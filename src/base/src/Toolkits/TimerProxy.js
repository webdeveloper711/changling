/*  Author  .   JIN */

function TimerProxy(){
    this.ctor();
}

var TAG = "<TimerProxy | ";

var _scheduler = cc.director.getScheduler();

var TIMER_STATUS = {
	INIT : "timer_init",
	DELAY : "timer_delay",
	RUNNING : "timer_running",
	FINISH : "timer_finished",
};

TimerProxy.prototype.ctor = function() {
    // body
    this._timers = {};

    var timer_init_, timer_delay_, timer_running_; //[[, timer_finish_//]]

    timer_init_ = function (timer, dt) {
        // body
        if (timer.delay) {
            timer._status = TIMER_STATUS.DELAY;
            timer._elapseDelay = timer.delay;
            return timer_delay_(timer, dt);
        }

        timer._status = TIMER_STATUS.RUNNING;
        timer._elapse = timer.interval;
        return timer_running_(timer, dt);
    }

    timer_delay_ = function (timer, dt) {
        // body
        timer._elapseDelay = timer._elapseDelay - dt;
        if (timer._elapseDelay > 0.0) {
            return;
        }

        timer._status = TIMER_STATUS.RUNNING;
        timer._elapse = timer.interval;
        // timer_running_(timer, dt)
    }

    timer_running_ = function (timer, dt) {
        // body
        timer._elapse = timer._elapse - dt;
        if (timer._elapse > 0.0) {
            return;
        }

        timer._elapse = timer.interval;
        timer._usedTimes = timer._usedTimes + 1;

        if (timer.callback) {
            timer.callback(timer.tag);
        }

        if (timer._usedTimes >= timer.times) {
            timer._status = TIMER_STATUS.FINISH;
            // return timer_finish_(timer, dt)
        }
    }

    //[[
    timer_finish_ = function (timer, dt) {
        // body
        if (this._timers[timer.tag]) {
            this._timers[timer.tag] = null;
        }
    }
    //]]

    var timer_validate_ = function () {
        // body
        for (k in this._timers) {
            var v = this._timers[k];
            if (v._status == TIMER_STATUS.FINISH) {
                this._timers[k] = null;
            }
        }
    }

    var timer_update_ = function (timer, dt) {
        // body
        if (timer._status == TIMER_STATUS.INIT) {
            return timer_init_(timer, dt);
        }

        if (timer._status == TIMER_STATUS.DELAY) {
            return timer_delay_(timer, dt);
        }

        if (timer._status == TIMER_STATUS.RUNNING) {
            return timer_running_(timer, dt);
        }

        if (timer._status == TIMER_STATUS.FINISH) {
            // nothing to do
            // return timer_finish_(timer, dt)
            return;
        }

        cc.log(TAG + "Timer_update_ - timer status error.");
    }

    var scheduleUpdate_ = function (dt) {
        // body
        for (k in this._timers) {
            var v = this._timers[k];
            // cc.log(TAG + "scheduleUpdate_ - called." + dt)
            timer_update_(v, dt);
        }

        timer_validate_();
    }

    /*//TODO: will fix!
       this._schedulerEntry = _scheduler.schedule(scheduleUpdate_, 0.0,false);
    */
    //TODO:?  this._schedulerEntry =  _scheduler.scheduleScriptFunc(scheduleUpdate_, 0.0, false);

};

TimerProxy.prototype.addTimer = function(timerTag, callback, interval, times, delay) {
    // body
    // appdf.assert(timerTag, "timerTag is error.");
    // appdf.assert(type(callback) == "function", "Timer callback error.");

    if (this._timers[timerTag]) {
        cc.log(TAG + "addTimer - timerTag is existed.");
        return false;
    }

    interval = interval || 0.0;
    interval = tonumber(interval);
    if (interval < 0.0) {
        interval = 0.0;
    }

    times = times || 1;
    times = math.ceil(tonumber(times));
    if (times == 0) {
        cc.log(TAG + "addTimer - times error with 0");
        return;
    }

    if (times < 0) {
        times = math.huge;
    }

    delay = delay || false;
    if (delay != false) {
        delay = tonumber(delay);
        if (delay < 0.0) {
            delay = false;
        }
    }

    this._timers[timerTag] = {
        tag: timerTag,

        interval: interval,
        times: times,
        callback: callback,
        delay: delay,

        _status: TIMER_STATUS.INIT,
        _elapse: 0,
        _usedTimes: 0,
        _elapseDelay: 0,
    }
    return true;
};

TimerProxy.prototype.removeTimer = function(timerTag) {
    // body
    if (this._timers[timerTag]) {
        this._timers[timerTag] = null;
    }
};

TimerProxy.prototype.hasTimer = function(timerTag) {
    // body
    if (this._timers[timerTag]) {
        return true;
    }

    return false;
};

TimerProxy.prototype.finalizer = function() {
    // body
    _scheduler.unscheduleScriptEntry(this._schedulerEntry);
};

var cc_exports_TimerProxy = cc_exports_TimerProxy || (new TimerProxy());