/*
    Author : Kil
    Date : 2017-12-2
 */

// cc.director = cc.Director._getInstance();

var appdf = appdf || {};

// 屏幕高宽 Screen width
appdf.WIDTH									    = 1334;
appdf.HEIGHT	    							= 750;

appdf.g_scaleY                                  = cc.director.getWinSize().height / appdf.HEIGHT; //Y坐标的缩放比例值 added ycc

appdf.BASE_SRC                                  = "base.src.";
appdf.CLIENT_SRC                                = "client.src.";
appdf.GAME_SRC                                  = "game.";
// 扩展目录
appdf.EXTERNAL_SRC							    = "client.src.external.";
// 通用定义目录
appdf.HEADER_SRC							    = "client.src.header.";
// 下载信息
appdf.DOWN_PRO_INFO							    = 1; // 下载进度
appdf.DOWN_COMPELETED						    = 3; // 下载结果
appdf.DOWN_ERROR_PATH						    = 4; // 路径出错
appdf.DOWN_ERROR_CREATEFILE					    = 5; // 文件创建出错
appdf.DOWN_ERROR_CREATEURL					    = 6; // 创建连接失败
appdf.DOWN_ERROR_NET		 				    = 7; // 下载失败

// 程序版本
appdf.BASE_C_VERSION                            = 0;// @app_version
// 资源版本
appdf.BASE_C_RESVERSION                         = 0;// @client_version
appdf.BASE_GAME                                 = [
    //{kind : 6,version : "0"},
    {kind : 391, version : 1}
];

// 项目ID
appdf.PORT_OBJECTID						        = 0;// 项目ID：用来在同一服务器上区分版本

// 登陆方式
appdf.LOGIN_CONFIG                              = 3;// 1:帐号 2:游客 4:微信

// 更新配置文件地址
appdf.IS_UPDATELOGON                            = 0;// 是否更新
appdf.URL_UPDATELOGON                           = "http://chengxingkeji.oss-cn-beijing.aliyuncs.com/Logon";// 更新地址
appdf.SP_UPDATELOGON                            = "serverpath/";// 保存目录
appdf.FILE_UPDATELOGON                          = "server_912.plist";// 保存文件

//链接URL Link URL
appdf.URL_REQUEST                               = "http://47.94.252.125:8091"; //@http_url//912测试平台

// 登陆IP
appdf.LOGONSERVER                               = "47.94.252.125";  // @http_url//912测试平台
//appdf.LOGONSERVER                               = "192.168.2.21";// @http_url//912测试平台

// 调试信息输出
appdf.OutLuaDebug                               = 1; // out_debug

appdf.print = function (str) {
    if (appdf.OutLuaDebug === 1) cc.log(str);
};

appdf.assert = function (condition, message) {
    message = message || 'Assertion failed';
    if (cc.sys.isNative) {
        if( !condition ) {
            var e = new Error('dummy');
            var stack = e.stack;
            cc.log("--- Assertion failed(" + message + ") ---");
            cc.log(stack);
        }
        //cc.assert(condition, message);
    } else{
        console.assert(condition, message);
    }
};


appdf.req = function (path) {
    // if (path && typeof(path) === "string") {
    //     return require(path);
    // } else {
    //     cc.log("require paht unknow");
    // }
};

// 字符分割
appdf.split = function (str, flag) {
    var tab = [];
    while (1) {

        var n = string.find(str, flag);
        if (n) {
            var first = str.substr(1, n - 1);
            str = str.substr(n + 1, str.length);
            tab.push(first);
        } else {
            tab.push(str);
            break;
        }
    }
    return tab;
};

//依据宽度截断字符
appdf.stringEllipsis = function (szText, sizeE,sizeCN,maxWidth) {
    //当前计算宽度
    var width = 0;
    //截断位置
    var lastpos = 0;
    //截断结果
    var szResult = "...";
    //完成判断
    var bOK = false;

    var i = 1;

    while (true) {
        var cur = szText.substr(i, i);
        var byte = string.byte(cur);
        if (byte == null) {
            break;
        }
        if (byte > 128) {
            if (width + sizeCN <= maxWidth - 3 * sizeE) {
                width = width + sizeCN;
                i = i + 3;
                lastpos = i + 2;
            } else {
                bOK = true;
                break;
            }
        } else if (byte != 32) {
            if (width + sizeE <= maxWidth - 3 * sizeE) {
                width = width + sizeE;
                i = i + 1;
                lastpos = i;
            } else {
                bOK = true;
                break;
            }
        } else {
            i = i + 1;
            lastpos = i;
        }
    }

    if (lastpos != 0) {
        szResult = szText.substr(1, lastpos);
        if (bOK)
            szResult = szResult + "...";
    }
    return szResult;
};

//打印table
appdf.printTable = function (dataBuffer) {
    if (!dataBuffer) {
        cc.log("printTable:dataBuffer is nil!");
        return;
    }
    if (typeof(dataBuffer) != "table") {
        cc.log("printTable:dataBuffer is not table!");
        return;
    }

    for (var i = 0; i < dataBuffer.length; i++) {
        var typeinfo = typeof(dataBuffer[i]);
        if (typeinfo == "table") {
            appdf.printTable(dataBuffer[i]);
        } else if (typeinfo == "userdata") {

        } else if (typeinfo == "boolean") {

        } else {
        }
    }
};

//HTTP获取json
appdf.onHttpJsonTable = function (url, method, params, callback) {
    var xhr = new cc.loader.getXMLHttpRequest();

    var bPost = ((method == "POST") || (method =="post"));

    if (!bPost) {
        if (params != null && params != "") {
            xhr.open(method, url + "?" + params);
        } else {
            xhr.open(method, url);
        }
        //xhr.setRequestHeader('Access-Control-Allow-Headers', '*');
        //xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
        xhr.send();
    } else {
        xhr.open(method, url);
        //xhr.setRequestHeader('Access-Control-Allow-Headers', '*');
        //xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
        xhr.send(params);
    }

    xhr.onreadystatechange = function () {
        var datatable;
        var response;
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                response = xhr.responseText;
                datatable = JSON.parse(response);
                cc.log("Http request success");
            } else {
                datatable = null;
                cc.log("Http request error");
            }
            callback(datatable, response);
        }
    };

    return true;
};
//创建版本
appdf.ValuetoVersion = function(value) {
    if (!value) {
        return {p: 0, m: 0, s: 0, b: 0};
    }

    var tmp;
    if (typeof value != "number") {
        tmp = Number(value);
    }
    else
        tmp = value;

    return {
        p: bit._rshift(bit._and(tmp, 0xFF000000), 24),
        m: bit._rshift(bit._and(tmp, 0x00FF0000), 16),
        s: bit._rshift(bit._and(tmp, 0x0000FF00), 8),
        b: bit._and(tmp, 0x000000FF)
    };
};

//创建颜色
appdf.ValueToColor = function ( r,g,b ) {
    r = r || 255;
    g = g || 255;
    b = b || 255;
    if (typeof(r) != "number")
        r = 255;
    if (typeof(g) != "number")
        g = 255;
    if (typeof(b) != "number")
        b = 255;
    var c = 0;
    c = bit._lshift(bit._and(0, 255), 24);
    c = bit._or(c, bit._lshift(bit._and(r, 255), 16));
    c = bit._or(c, bit._lshift(bit._and(g, 255), 8));
    c = bit._or(c, bit._and(b, 255));

    return c;
};

//版本值
appdf.VersionValue = function (p, m, s, b) {
    var v = 0;

    if (p != null) v = bit._or(v, bit._lshift(p, 24));
    if (m != null) v = bit._or(v, bit._lshift(m, 16));
    if (s != null) v = bit._or(v, bit._lshift(s, 8));
    if (b != null) v = bit._or(v, b);

    return v;
};

//根據名稱取node
appdf.getNodeByName = function (node,name) {
    var curNode = node.getChildByName(name);
    if (curNode)
        return curNode;
    else {
        var nodeTab = node.getChildren();
        if (nodeTab.length > 0) {
            for (i = 0; i < nodeTab.length; i++) {
                var result = appdf.getNodeByName(nodeTab[i], name);
                if (result)
                    return result;
            }
        }
    }
};

appdf.getSaveMD5String = function (_sp, _plist, _code, _length) {
    var tmpInfo = appdf.readByDecrypt( _code, _length);
    // var tmpInfo = appdf.readByDecrypt(_sp + _plist, _code);

    //检验
    if (tmpInfo != null && tmpInfo.length > _length) {
        var md5Test = tmpInfo.substr(1, _length);
        tmpInfo = tmpInfo(_length + 1, tmpInfo.length);
        if (md5Test != md5(tmpInfo)) {
            cc.log("test:" + md5Test + "#" + tmpInfo);
            tmpInfo = null;
        } else {
            tmpInfo = null;
        }

        if (tmpInfo != null)
            return tmpInfo;
        else
            return "";
    }
};

appdf.saveLogonServer =function  (logonServer) {
    var sp = appdf.SP_UPDATELOGON;
    var temp = md5(logonServer) + logonServer;
    saveByEncrypt(sp + appdf.FILE_UPDATELOGON, "code_0", temp);
};

appdf.getLogonServer = function () {
    var sp = appdf.SP_UPDATELOGON;
    return appdf.getSaveMD5String(sp, appdf.FILE_UPDATELOGON, "code_0", 32);
};

appdf.saveLogonRequest = function (url) {
    var sp = appdf.SP_UPDATELOGON;
    var temp = md5(url) + url;
    saveByEncrypt(sp + appdf.FILE_UPDATELOGON, "code_url", temp);
};

appdf.getLogonRequest = function () {
    var sp = appdf.SP_UPDATELOGON;
    return appdf.getSaveMD5String(sp, appdf.FILE_UPDATELOGON, "code_url", 32);
};

appdf.saveServerList = function (serverlist) {
    var sp = appdf.SP_UPDATELOGON;
    var count = serverlist.length;
    var temp = md5(count) + count;
    saveByEncrypt(sp + appdf.FILE_UPDATELOGON, "code_count", temp);

    for (i = 0; i < count; i++) {
        temp = md5(serverlist[i]) + serverlist[i];
        saveByEncrypt(sp + appdf.FILE_UPDATELOGON, "code_" + i, temp);
    }
};

appdf.getServerList = function () {
    var sp = appdf.SP_UPDATELOGON;
    var count = appdf.getSaveMD5String(sp, appdf.FILE_UPDATELOGON, "code_count", 32);
    var nCount = Number(count);

    var list = {};
    for (i = 0; i < nCount; i++) {
        list[i] = appdf.getSaveMD5String(sp, appdf.FILE_UPDATELOGON, "code_" + i, 32);
    }
    return list;
};

appdf.readByDecrypt = function ( _code, _length){
    var user_gameconfig = cc.sys.localStorage.getItem("user_gameconfig") != null ? cc.sys.localStorage.getItem("user_gameconfig") : [] ;
    var user_data = user_gameconfig[_code];
    if (user_data == null || user_data.length == 0){
        return "";
    }else{
        var str = "";
        for ( var i = 0 ; i < _length ; i++ ){
            str += String.fromCharCode(user_data[i]);
        }
        return str;
    }
};

appdf.saveByEncrypt = function (_code , szSaveInfo){
    var data = [];
    data[_code] = [];
    for ( var i = 0 ; i < szSaveInfo.length ; i++ ){
        data[_code][i] = szSaveInfo.charCodeAt(szSaveInfo.length-i-1);
    }
    cc.sys.localStorage.setItem("user_gameconfig",data);
};

function currentTime() {
    return Date.now();
}

appdf.getCookie = function(name) {
    var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
    if (arr = document.cookie.match(reg))
        return unescape(arr[2]);
    else
        return null;
};

appdf.GetQueryString = function(param,name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = param.match(reg);
    if (r != null) return unescape(r[2]); return null;
}