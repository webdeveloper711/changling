var Version = cc.Node.extend({
    ctor : function () {
        this._super();
        this.retain();
        if (cc.sys.isNative) {
            var sp = "";
            this._path = jsb.fileUtils.getWritablePath() + sp + "version.plist";
            this._versionInfo = jsb.fileUtils.getValueMapFromFile(this._path);

            cc.log("Version File Path : " + this._path);
        } else {
            this._versionInfo = cc.loader.getRes(res.version_plist);
        }
        ExternalFun.dump(this._versionInfo, "Version._versionInfo", 3);
        this._downUrl = null;
    },
    setVersion : function (version, kindid) {
        if (! kindid) {
            this._versionInfo["client"] = version;
        } else {
            var index = "game_" + kindid;
            this._versionInfo[index] = version;
        }
    },
    getVersion : function (kindid) {
        if (! kindid) {
            return this._versionInfo["client"];
        } else {
            var index = "game_" + kindid;
            return this._versionInfo[index];
        }
    },
    setResVersion : function (version, kindid) {
        if (! kindid) {
            this._versionInfo["res_client"] = version;
        } else {
            var index = "res_game_" + kindid;
            this._versionInfo[index] = version;
        }

        this.save();
    },
    getResVersion : function (kindid) {
        if (! kindid) {
            return this._versionInfo["res_client"];
        } else {
            var index = "res_game_" + kindid;
            return this._versionInfo[index];
        }
    },
    save : function () {
        if (cc.sys.isNative) {
            jsb.fileUtils.writeToFile(this._versionInfo, this._path);
        }
    }
});