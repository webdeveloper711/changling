
var bit = bit || {};
bit.init32 = function () {
    bit.data32 = [];
    for (var i = 1; i <= 32; i++) {
        bit.data32[i] = Math.pow(2, (32 - i));
    }
};
bit.init32();

bit.d2b = function (arg) {
    var tr = {};
    for (var i = 1; i <= 32; i++) {
        if (arg >= this.data32[i]) {
            tr[i] = 1;
            arg -= this.data32[i];
        } else {
            tr[i] = 0;
        }
    }
    return tr;
};

bit.b2d = function (arg) {
    var nr = 0;
    for (var i = 1; i <= 32; i++) {
        if (arg[i] == 1) nr += Math.pow(2, (32 - i));
    }
    return nr;
};

bit._xor = function (a, b) {
    var op1 = this.d2b(a);
    var op2 = this.d2b(b);
    var r = {};

    for (var i = 1; i <= 32; i++) {
        if (op1[i] == op2[i]) r[i] = 0;
        else r[i] = 1;
    }

    return this.b2d(r);
};

bit._and = function (a, b) {
    var op1 = this.d2b(a);
    var op2 = this.d2b(b);
    var r = {};

    for (var i = 1; i <= 32; i++) {
        if (op1[i] == 1 && op2[i] == 1) r[i] = 1;
        else r[i] = 0;
    }

    return this.b2d(r);
};

bit._or = function (a, b) {
    var op1 = this.d2b(a);
    var op2 = this.d2b(b);
    var r = {};

    for (var i = 1; i <= 32; i++) {
        if (op1[i] == 1 || op2[i] == 1) r[i] = 1;
        else r[i] = 0;
    }
    return this.b2d(r);
};

bit._not = function (a) {
    var op1 = this.d2b(a);
    var r = {};

    for (var i = 1; i <= 32; i++) {
        if (op1[i] == 1) r[i] = 0;
        else r[i] = 1;
    }

    return this.b2d(r);
};

bit._rshift = function (a, n) {
    var op1 = this.d2b(a);
    var r = this.d2b(0);

    if (n <32 && n >0) {
        for (var i = 1; i <= n; i++) {
            for (var j = 31; j >= 1; j--){
                op1[j + 1] = op1[j];
            }
            op1[1] = 0;
        }
    }
    r = op1;
    return this.b2d(r);
};

bit._lshift = function (a, n) {
    var op1 = this.d2b(a);
    var r = this.d2b(0);

    if (n <32 && n >0) {
        for (var i = 1; i <= n; i++) {
            for (var j = 1; j <= 31; j++){
                op1[j] = op1[j + 1];
            }
            op1[32] = 0;
        }
    }
    r = op1;
    return this.b2d(r);
};

bit.print = function (ta) {
    var sr = "";
    for (var i = 1; i <= 32; i++) {
        sr += ta[i];
    }
    cc.log("bit print" + sr);
};