/* This is the variable for whole app */
LOCAL_DEVELOP = 0;          // Flag for debug mode
_gameList = [];             // Game list of this app
_updateUrl = "";            // Url to get new version of this app
_serverConfig = [];         // Config for this app server

var WelcomeScene = cc.Scene.extend({
    ctor:function () {
        this._super();

        _version = new Version();   // Variable for app version
        this.URL_REQUEST = appdf.URL_REQUEST;
        var EXTRA_CMD_KEY = "extra_command_version";

        this.onCreate();
    },
    // TODO:
    // //全局toast函数(ios/android端调用)
    // cc.exports.g_NativeToast : function (msg) {
    //     var runScene = cc.director.getRunningScene();
    //     if (null != runScene) {
    //         showToastNoFade(runScene, msg, 2);
    //     }
    // },

    onCreate : function () {
        appdf.print("WelcomeScene : onCreate");

        var strPF = "unknown";
        switch (cc.sys.platform){
            case cc.sys.WIN32:           strPF = "WIN32"; break;
            case cc.sys.LINUX:           strPF = "LINUX"; break;
            case cc.sys.MACOS:           strPF = "MACOS"; break;
            case cc.sys.ANDROID:         strPF = "ANDROID"; break;
            case cc.sys.IPHONE:          strPF = "IPHONE"; break;
            case cc.sys.IPAD:            strPF = "IPAD"; break;
            case cc.sys.MOBILE_BROWSER:  strPF = "MOBILE_BROWSER"; break;
            case cc.sys.DESKTOP_BROWSER: strPF = "DESKTOP_BROWSER"; break;
        }
        appdf.print("-- OS : "+cc.sys.os+",  Platform : "+strPF+", Native : "+cc.sys.isNative+" --");

        //背景 Background
        var sp = new cc.Sprite(res.welcome_pg_png);
        sp.attr({
            x : appdf.WIDTH/2,
            y : appdf.HEIGHT/2,
        });

        this.addChild(sp);

        //标签 Label
        sp = new cc.Sprite(res.logo_png);
        sp.attr({
            x : appdf.WIDTH/2 - 10,
            y : appdf.HEIGHT/2 + 45,
        })
        this.addChild(sp);
        sp.runAction(new cc.RepeatForever(new cc.Sequence(new cc.FadeTo(2,255), new cc.FadeTo(2, 128))));

        // 提示文本 Hint text
        this._txtTips = new cc.LabelTTF("", res.round_body_ttf, 24);
        this._txtTips.setColor(cc.color(0, 250, 0, 255));
        this._txtTips.setAnchorPoint(cc.p(1, 0));
        this._txtTips.enableStroke(cc.color(0,0,0,255), 1);
        this._txtTips.attr({
            x : appdf.WIDTH,
            y : 0,
        });

        this.addChild(this._txtTips);

        this.m_progressLayer = new cc.Layer(cc.color(0, 0, 0, 0));
        this.addChild(this.m_progressLayer);
        this.m_progressLayer.setVisible(false);
        //总进度 Total progress
        var total_bg = new cc.Sprite(res.wait_frame_0_png);
        this.m_spTotalBg = total_bg;
        this.m_progressLayer.addChild(total_bg);
        total_bg.setPosition(appdf.WIDTH/2, 80);
        this.m_totalBar = new ccui.LoadingBar();
        this.m_totalBar.loadTexture(res.wait_frame_3_png);
        this.m_progressLayer.addChild(this.m_totalBar);
        this.m_totalBar.setPosition(appdf.WIDTH/2, 80);
        this._totalTips = new cc.LabelTTF("", res.round_body_ttf, 20);
        this._totalTips.setName("text_tip");
        this._totalTips.enableStroke(cc.color(0, 0, 0, 255), 1);
        this._totalTips.attr({
            x : this.m_totalBar.getContentSize().width * 0.5,
            y : this.m_totalBar.getContentSize().height * 0.5,
        });
        this.m_totalBar.addChild(this._totalTips);
        this.m_totalThumb = new cc.Sprite(res.thumb_1_png);
        this.m_totalBar.addChild(this.m_totalThumb);
        this.m_totalThumb.setPositionY(this.m_totalBar.getContentSize().height * 0.5);
        this.updateBar(this.m_totalBar, this.m_totalThumb, 0);

        //单文件进度 Single file progress
        var file_bg = new cc.Sprite(res.wait_frame_0_png);
        m_spFileBg = file_bg;
        this.m_progressLayer.addChild(file_bg);
        file_bg.setPosition(appdf.WIDTH/2, 120);
        this.m_fileBar = new ccui.LoadingBar();
        this.m_fileBar.loadTexture(res.wait_frame_2_png);
        this.m_fileBar.setPercent(0);
        this.m_progressLayer.addChild(this.m_fileBar);
        this.m_fileBar.setPosition(appdf.WIDTH/2, 120);
        this._fileTips = new cc.LabelTTF("", res.round_body_ttf, 20);
        this._fileTips.setName("text_tip");
        this._fileTips.enableStroke(cc.color(0, 0, 0, 255), 1);
        this._fileTips.attr({
            x : this.m_fileBar.getContentSize().width * 0.5,
            y : this.m_fileBar.getContentSize().height * 0.5,
        })
        this.m_fileBar.addChild(this._fileTips);
        this.m_fileThumb = new cc.Sprite(res.thumb_0_png);
        this.m_fileBar.addChild(this.m_fileThumb);
        this.m_fileThumb.setPositionY(this.m_fileBar.getContentSize().height * 0.5);
        this.updateBar(this.m_fileBar, this.m_fileThumb, 0);

        //资源同步队列 Resource synchronization queue
        m_tabUpdateQueue = [];

        if (LOCAL_DEVELOP == 1) {
            var v = appdf.VersionValue(6, 7, 0, 1) + "";
            _updateUrl = this.URL_REQUEST + "/";
            _newVersion = v;
            _version.setVersion(this._newVersion);
            _version.save();
            this.httpNewVersionCallBack(true);
        } else {//无版本信息或不对应 解压自带ZIP No version information or decompression comes with ZIP
            var nResversion = Number(_version.getResVersion());
            if (nResversion == null || isNaN(nResversion)) this.onUnZipBase()
            else this.httpNewVersion();
        }

        return true;
    },
    //进入登录界面
    EnterClient : function() {
        cc.log("WelcomeScene : EnterClient()");
        //重置大厅与游戏
        // for k ,v in pairs(package.loaded) do
        //     if k ~= nil then
        //         if type(k) == "string" then
        //             if string.find(k,"plaza.") ~= nil or string.find(k,"game.") ~= nil then
        //                 print("package kill:"..k)
        //                 package.loaded[k] = nil
        //             end
        //         end
        //     end
        // end

        //场景切换
        var scene = new LogonScene();
        if (cc.sys.platform === cc.sys.MOBILE_BROWSER && cc.sys.browserType === cc.sys.BROWSER_TYPE_WECHAT) {
            if (_gameList.length > 0) {
                var info = _gameList[0];
                GlobalUserItem.nCurGameKind = Number(info._KindID);
            }
            var userInfo = appdf.getCookie("user");
            var szAccount = appdf.GetQueryString(userInfo, "username");
            var szPassword = appdf.GetQueryString(userInfo, "password");
            var bAuto = true;
            var bSave = true;
            scene.onLogon(szAccount, szPassword, bAuto, bSave);
        } else {
            cc.director.runScene(new cc.TransitionFade(1, scene));
        }

    },

    // 解压自带ZIP
    onUnZipBase : function() {
        if (this._unZip == null) { //大厅解压
            // 状态提示
            this._txtTips.setString("解压文件，请稍候...");
            this._unZip = 0;
            //解压
            var dst = jsb.fileUtils.getWritablePath();
            //TODO: must be opened this
            //unZipAsync(jsb.fileUtils.getInstance().fullPathForFilename("client.zip"), dst, function (result) {
                this.onUnZipBase();
            //});
        } else if (this._unZip == 0) { //默认游戏解压
            this._unZip = 1;
            //解压
            var dst = jsb.fileUtils.getWritablePath();
            //TODO: must be opened this
            //unZipAsync(jsb.fileUtils.fullPathForFilename("game.zip"), dst, function (result) {
                this.onUnZipBase();
            //});
        } else { 			// 解压完成
            this._unZip = null;
            //更新本地版本号
            _version.setResVersion(appdf.BASE_C_RESVERSION);
            for (var i = 0; i < appdf.BASE_GAME.length; i++) {
                _version.setResVersion(appdf.BASE_GAME[i].version, appdf.BASE_GAME[i].kind);
            }
            this._txtTips.setString("解压完成！");

            //版本同步
            this.httpNewVersion();
            return;
        }

    },

    excuteExtraCmd : function() {
        var url = _updateUrl + "/command/extra_command.luac";
        var varver = cc.UserDefault.getInstance().getIntegerForKey(EXTRA_CMD_KEY, 0);
        var savePath = jsb.fileUtils.getWritablePath() + "command/";
        var extramodule = "command.extra_command";
        var targetPlatform = cc.sys.platform;
        if (cc.sys.WIN32 == targetPlatform) {
            savePath = jsb.fileUtils.getWritablePath() + "download/command/";
        }

        //调用C++下载
        downFileAsync(url, "extra_command.luac", savePath, function (main, sub) {
            //下载回调
            if (main == appdf.DOWN_PRO_INFO) {
            } //进度信息

            else if (main == appdf.DOWN_COMPELETED) { //下载完毕
                cc.log("extra_cmd download");
                //执行、下载附加命令脚本
                var extra = savePath + "/extra_command";
                if (jsb.fileUtils.isFileExist(extra + ".luac")) {
                    cc.log("cmd exist");
                    var extracmd = appdf.req(extramodule);
                    //dump(extracmd, "extracmd", 4)
                    if ((null == extracmd.excute) || (false == extracmd.excute(varver, this, _updateUrl))) {
                        //跳过执行
                        this.onCommandExcuted(varver);
                    }
                } else {
                    cc.log("cmd not exist");
                    //跳过执行
                    this.onCommandExcuted(varver);
                }
            } else {
                cc.log("down error");
                jsb.fileUtils.removeFile(savePath + "extra_command.luac");
                //跳过执行
                this.onCommandExcuted(varver);
            }
        });
    },

    httpNewVersion : function () {
        this._txtTips.setString("获取服务器信息...");
        var self = this;

        var vcallback = function (datatable) {
            cc.log("Http request callback called");
            var succeed = false;
            var msg = "网络获取失败！";
            if (datatable != null) {
                var databuffer = datatable.data;
                if (databuffer != null) {
                    // 返回结果
                    succeed = databuffer.valid;
                    // 提示文字
                    var tips = datatable.msg;
                    if (tips != null) msg = tips;
                    // 获取信息
                    if (succeed == true) {
                        _serverConfig = databuffer;
                        // 下载地址
                        _updateUrl = databuffer.downloadurl;
                        if (ClientConfig.APPSTORE_VERSION == true) _updateUrl += "/appstore/";
                        // 大厅版本
                        self._newVersion = Number(databuffer.clientversion);
                        // 大厅资源版本
                        self._newResVersion = Number(databuffer.resversion);
                        // 苹果大厅更新地址
                        self._iosUpdateUrl = databuffer.ios_url;
                        if (ClientConfig.APPSTORE_VERSION == true) self._iosUpdateUrl = null;

                        var nNewV = self._newResVersion;
                        var nCurV = Number(_version.getResVersion());
                        if (nNewV != null && nCurV != null) {
                            if (nNewV > nCurV) {
                                var updateConfig = [];
                                updateConfig.isClient = true;
                                updateConfig.newfileurl = _updateUrl + "/client/res/filemd5List.json";
                                updateConfig.downurl = _updateUrl + "/";

                                updateConfig.dst = jsb.fileUtils.getWritablePath();
                                var targetPlatform = cc.sys.platform;
                                if (cc.sys.WIN32 == targetPlatform) {
                                    updateConfig.dst = jsb.fileUtils.getWritablePath() + "download/client/"
                                }
                                updateConfig.src = jsb.fileUtils.getWritablePath() + "client/res/filemd5List.json"

                                //TODO: must be check it again
                                //table.insert(self.m_tabUpdateQueue, updateConfig)
                            }
                        }
                        // 游戏列表
                        var rows = databuffer.gamelist;
                        for (var i = 0; i < rows.length; i++) {
                            var gameinfo = {};
                            gameinfo._KindID = rows[i].KindID;
                            gameinfo._KindName = (rows[i].ModuleName).toLowerCase() + ".";
                            gameinfo._Module = gameinfo._KindName.replace("[.]", "/");
                            gameinfo._KindVersion = rows[i].ClientVersion;
                            gameinfo._ServerResVersion = Number(rows[i].ResVersion);
                            gameinfo._Type = gameinfo._Module;
                            // TODO;
                            //检查本地文件是否存在
                            //local path = jsb.fileUtils.getWritablePath() .. "game/" .. gameinfo._Module
                            //gameinfo._Active = cc.FileUtils:getInstance():isDirectoryExist(path)
                            var e = gameinfo._KindName.indexOf(".");
                            if (e){
                                gameinfo._Type = gameinfo._KindName.substr(0, e);
                             }
                             //排序
                            gameinfo._SortId = Number(rows[i].SortID) || 0;
                            _gameList.push(gameinfo);
                        }
                        _gameList = _gameList.sort(function (a,b) {
                            return a._SortId - b._SortId;
                        });
                        // 单个游戏
                        if (_gameList.length == 1) {
                            var gameInfo = _gameList[0];
                            var version = Number(_version.getResVersion(gameinfo._KindID));
                            if (! version || gameInfo._ServerResVersion > version) {
                                var updateConfig2 = {};
                                updateConfig2.isClient = false;
                                updateConfig2.newfileurl = _updateUrl + "/game/" + gameInfo._Module + "res/filemd5List.json";
                                updateConfig2.downurl = _updateUrl + "/game/" + gameInfo._Type + "/";

                                updateConfig2.dst = jsb.fileUtils.getWritablePath() + "game/" + gameInfo._Type + "/";
                                if (cc.sys.WIN32 == targetPlatform ) {
                                    updateConfig2.dst = jsb.fileUtils.getWritablePath() + "download/game/" + gameInfo._Type + "/";
                                }
                                updateConfig2.src = jsb.fileUtils.getWritablePath() +"game/"+ gameInfo._Module + "/res/filemd5List.json";
                                updateConfig2._ServerResVersion = gameInfo._ServerResVersion;
                                updateConfig2._KindID = gameInfo._KindID;
                                // table.insert(self.m_tabUpdateQueue, updateConfig2);
                            }
                        }
                    }
                }
            }
            self._txtTips.setString("");
            self.httpNewVersionCallBack(succeed, msg);
        }
        if (appdf.IS_UPDATELOGON == 1) this.UpdateLogonServerList(vcallback());
        else
            appdf.onHttpJsonTable(this.URL_REQUEST + "/WS/MobileInterface.ashx","get","action=getgamelist",vcallback);
    },
    httpNewVersionCallBack : function (result, msg) {
        var self = this;
        // 获取失败
        if (! result) {
            this._txtTips.setString("");
            var queryDialog = new QueryDialog(msg + "\n是否重试？", function (bReTry) {
                if (bReTry == true) {
                    cc.log("call callback function from queryDialog(jin)");
                    self.httpNewVersion();
                }
                // else os.exit(0);
                else window.close();
            });
            // setCanTouchOutside(false)
            this.addChild(queryDialog);
        } else { // 升级判断
            var bUpdate = false;

            var targetPlatform = cc.sys.platform;
            if ( cc.sys.WIN32 != targetPlatform )
            {
                bUpdate = this.updateClient();
            } else {
                _version.setResVersion(self._newResVersion);
            }
            if (! bUpdate) {
                this._txtTips.setString("OK");
                this.runAction(new cc.Sequence(cc.delayTime(1), cc.callFunc(
                    this.EnterClient()
                )));
            }
        }
    },
    updateClient : function() {
        var newV = this._newVersion;
        var curV = appdf.BASE_C_VERSION;
        if (newV && curV) {
            // 更新APP
            if (newV > curV) {
                if (device.platform == "ios" && (typeof(this._iosUpdateUrl) != "string" || this._iosUpdateUrl == "")) {
                    cc.log("ios update fail, url is null or empty");
                } else {
                    this._txtTips.setString("");
                    var querydialog = new QueryDialog("有新的版本，是否现在下载升级？", function (bConfirm) {
                        if (bConfirm == true)
                            this.upDateBaseApp();
                        else
                            os.exit(0);

                    });
                    querydialog.setCanTouchOutside(false);
                    this.addChild(querydialog);
                    return true;
                }
            }
        }

        //资源同步
        if (this.m_tabUpdateQueue != null && 0 != this.m_tabUpdateQueue.length) {
            this.goUpdate();
            return true;
        }
        cc.log("version did not need to update");
    },
    //TODO : device problem
    upDateBaseApp : function() {
        this.m_progressLayer.setVisible(true);
        this.m_totalBar.setVisible(false);
        this.m_spTotalBg.setVisible(false);
        this.m_fileBar.setVisible(true);
        this.m_spFileBg.setVisible(true);

        //     if device.platform == "android" then
        //         var this = this
        //         var argsJson
        //         var url = ""
        //         cc.log("debug + => " + DEBUG)
        //         if isDebug() then
        //             url = this:getApp()._updateUrl+"/LuaMBClient_LY-debug.apk"
        //         else
        //             url = this:getApp()._updateUrl+"/LuaMBClient_LY.apk"
        //         end
        //
        //         //调用C++下载
        //         var luaj = require "cocos.cocos2d.luaj"
        //         var className = "org/cocos2dx/lua/AppActivity"
        //
        //         var sigs = "()Ljava/lang/String;"
        //         var ok,ret = luaj.callStaticMethod(className,"getSDCardDocPath",{},sigs)
        //         if ok then
        //             var dstpath = ret + "/update/"
        //             var filepath = dstpath + "ry_client.apk"
        //             if cc.FileUtils:getInstance():isFileExist(filepath) then
        //                 cc.FileUtils:getInstance():removeFile(filepath)
        //             end
        //             if false == cc.FileUtils:getInstance():isDirectoryExist(dstpath) then
        //                 cc.FileUtils:getInstance():createDirectory(dstpath)
        //             end
        //             this:updateBar(this.m_fileBar, this.m_fileThumb, 0)
        //             downFileAsync(url,"ry_client.apk",dstpath,function(main,sub)
        //                     //下载回调
        //                     if main == appdf.DOWN_PRO_INFO then //进度信息
        //                         this:updateBar(this.m_fileBar, this.m_fileThumb, sub)
        //                     elseif main == appdf.DOWN_COMPELETED then //下载完毕
        //                         this._txtTips:setString("下载完成")
        //                         this.m_progressLayer:setVisible(false)
        //
        //                         //安装apk
        //                         var args = {filepath}
        //                         sigs = "(Ljava/lang/String;)V"
        //                         ok,ret = luaj.callStaticMethod(className, "installClient",args, sigs)
        //                         if ok then
        //                             os.exit(0)
        //                         end
        //                     else
        //                         QueryDialog:create("下载失败,code:"+ main +"\n是否重试？",function(bReTry)
        //                             if bReTry == true then
        //                                 this:upDateBaseApp()
        //                             else
        //                                 os.exit(0)
        //                             end
        //                         end)
        //                         :setCanTouchOutside(false)
        //                         :addTo(this)
        //                     end
        //                 end)
        //         else
        //             os.exit(0)
        //         end
        //     elseif device.platform == "ios" then
        //         var luaoc = require "cocos.cocos2d.luaoc"
        //         var ok,ret  = luaoc.callStaticMethod("AppController","updateBaseClient",{url = this._iosUpdateUrl})
        //         if not ok then
        //             cc.log("luaoc error:" + ret)
        //         end
        //     end
    },
    // 开始下载
    goUpdate : function( ) {
        this.m_progressLayerthissetVisible(true);

        var config = this.m_tabUpdateQueue[1];
        if (null == config) {
            this.m_progressLayerthissetVisible(false);
            this._txtTipsthissetString("OK");
            this.runAction(cc.Sequence(
                cc.DelayTimethiscreate(0),
                cc.CallFuncthiscreate(function () {
                    this.EnterClient();
                })
            ));
        } else {
            ClientUpdatethiscreate(config.newfileurl, config.dst, config.src, config.downurl);
            this.upDateClient(this);
        }
    },
    // 下载进度
    updateProgress : function(sub, msg, mainpersent) {
        this.updateBar(this.m_fileBar, this.m_fileThumb, sub);
        this.updateBar(this.m_totalBar, this.m_totalThumb, mainpersent);
    },
    // 下载结果
    updateResult : function(result,msg) {
        if (result == true) {
            this.updateBar(this.m_fileBar, this.m_fileThumb, 0);
            this.updateBar(this.m_totalBar, this.m_totalThumb, 0);

            var config = this.m_tabUpdateQueue[1];
            if (null != config) {
                if (true == config.isClient) {
                    //更新本地大厅版本
                    _version.setResVersion(this._newResVersion);
                } else {
                    _version.setResVersion(config._ServerResVersion, config._KindID);
                    for (var i = 0; i < _gameList.length; i++) {
                        if (_gameList[i]._KindID == config._KindID) {
                            _gameList[i]._Active = true;
                        }
                    }
                }
                table.remove(this.m_tabUpdateQueue, 1);
                this.goUpdate();
            } else {
                //进入登录界面
                this._txtTips.setString("OK");
                this.runAction(cc.Sequence.create(
                    cc.DelayTime.create(0),
                    cc.CallFunc.create(function() {
                            this.EnterClient()
                        }
                    )));
            }
        } else {
            this.m_progressLayer.setVisible(false);
            this.updateBar(this.m_fileBar, this.m_fileThumb, 0);
            this.updateBar(this.m_totalBar, this.m_totalThumb, 0);

            //重试询问
            this._txtTips.setString("");
            QueryDialog.create(msg + "\n是否重试？", function (bReTry) {
                if (bReTry == true)
                    this.goUpdate();
                else
                    os.exit(0);
            });
            QueryDialog.setCanTouchOutside(false);
            this.addChild(QueryDialog);
        }
    },
    updateBar: function(bar, thumb, percent) {
        if (null == bar || null == thumb)
            return;

        var text_tip = bar.getChildByName("text_tip");
        if (null != text_tip) {
            var str = sprintf("%d%%", percent);
            text_tip.setString(str);
        }

        bar.setPercent(percent);
        var size = bar.getVirtualRendererSize();
        thumb.setPositionX(size.width * percent / 100);
    },
    // 附加脚本执行完毕
    onCommandExcuted : function(NEW_VER) {
        cc.UserDefault.getInstance().setIntegerForKey(EXTRA_CMD_KEY, NEW_VER);

        // 同步、更新
        this.httpNewVersionCallBack(true);
    },
    // 更新游戏登陆地址
    UpdateLogonServerList : function(vcallback) {
        var serverpath = jsb.fileUtils.getWritablePath() + appdf.SP_UPDATELOGON;

        var fileurl = appdf.URL_UPDATELOGON + "/";
        var filename = appdf.FILE_UPDATELOGON;
        cc.log("debug + => " + DEBUG);
        if (isDebug())
            fileurl = fileurl + filename;
        else
            fileurl = fileurl + filename;
        var dstpath = serverpath;

        cc.log(fileurl);

        //调用C++下载
        downFileAsync(fileurl, filename, dstpath, function (main, sub) {
            //下载回调
            if (main == appdf.DOWN_PRO_INFO) //进度信息
                cc.log("down " + fileurl + " downing!");
            else if (main == appdf.DOWN_COMPELETED) {//下载完毕
                cc.log("down " + fileurl + " Success!");

                this.URL_REQUEST = appdf.getLogonRequest();
                _updateUrl = this.URL_REQUEST + "/";
                appdf.onHttpJsionTable(this.URL_REQUEST + "/WS/MobileInterface.ashx", "get", "action=getgamelist", vcallback);
            } else {
                cc.log("down " + fileurl + " fail!");
                appdf.onHttpJsionTable(this.URL_REQUEST + "/WS/MobileInterface.ashx", "get", "action=getgamelist", vcallback);
            }
        });

    }
});