// made by VS23
var PopWait = cc.Layer.extend({
    ctor: function (isTransparent) {
        if (isTransparent == true) {
            this._super(cc.color(0, 0, 0, 0));
        } else {
            this._super(cc.color(0, 0, 0, 125));
        }

        if (this.width === undefined || this.height === undefined) {
            this.setContentSize(appdf.WIDTH, appdf.HEIGHT);
        } else {
            this.setContentSize(this.width, this.height);
            this.setContentSize(this.width, this.height);
        }

        var onTouch = function (eventType, x, y) {
            return true;
        };
        // this.setTouchEnabled(true);
        // this.addTouchEventListener(onTouch);

        if (isTransparent != true) {
            var sp = new cc.Sprite(res.wait_round_png);
            sp.attr({
                x : appdf.WIDTH/2,
                y : appdf.HEIGHT/2,
            });

            this.addChild(sp);
            sp.runAction(new cc.RepeatForever(new cc.RotateBy(2, 360)));
        }

    },
    show: function (parent, message) {
        parent.addChild(this);
    },
    isShow: function(){
        return this._dismiss;
    },
    dismiss: function(){
        if (this._dismiss) {
            return;
        }
        this._dismiss = true;
        this.runAction(new cc.RemoveSelf());
    }
});