/*  Author  :   JIN */

var QueryDialog = cc.Layer.extend({

    // 默认字体大小
    DEF_TEXT_SIZE : 32,
    // UI标识
    DG_QUERY_EXIT : 2,
    BT_CANCEL     : 0,
    BT_CONFIRM    : 1,
    // 对话框类型
    QUERY_SURE    : 1,
    QUERY_SURE_CANCEL :2,

    ctor: function (msg, callback, txtsize, queryType) {
        this._super();
        cc.log('query dialog super called.');

        queryType = queryType || this.QUERY_SURE_CANCEL;
        this._callback = callback;
        this._canTouchOutside = true;

        var that = this;
        this.setContentSize(appdf.WIDTH, appdf.HEIGHT);
        this.attr({
            x:0,
            y:appdf.HEIGHT
        });

        //callback
        this.registerScriptHandler = function(eventtype){
            if(eventtype == "enterTransitionFinish"){
                this.onEnterTransitionFinish();
            } else if (eventType === "exitTransitionStart") {
                this.onExitTransitionStart();
            }
        };

        //key mornitoring
        var btcallback = function(ref, type){
            if(type == ccui.Widget.TOUCH_ENDED ) {
                that.onButtonClickedEvent(ref.getTag(), ref);
            }
        }

        //cancel display outside the area
        var onQueryExitTouch = function(eventType,x,y){
            if(!this._canTouchOutside){
                return true;
            }

            if(this._dismiss){
                return true;
            }

            if (eventType === "began"){
                var rect = that.getChildByTag(this.DG_QUERY_EXIT).getBoundingBox();
                if( false == cc.rectContainsPoint(rect,cc.p(x,y)) ){
                    this.dismiss();
                }
            }

            return true;
        }

        //this.setTouchEnabled();
        // this.registerScriptTouchHandler(onQueryExitTouch);

        var sp = new cc.Sprite(res.query_bg_png);
        sp.setTag(this.DG_QUERY_EXIT);
        sp.attr({
            x:appdf.WIDTH/2,
            y:appdf.HEIGHT/2
        });
        this.addChild(sp);

        if(this.QUERY_SURE === queryType){
            var bt = new ccui.Button(res.bt_query_confirm_0_png, res.bt_query_confirm_1_png);
            bt.setPosition(appdf.WIDTH/2,200);
            bt.setTag(this.BT_CONFIRM);
            bt.addTouchEventListener(btcallback);
            this.addChild(bt);
        } else {
            var bt1 = new ccui.Button(res.bt_query_confirm_0_png, res.bt_query_confirm_1_png);
            bt1.setPosition(appdf.WIDTH/2+169,270);
            bt1.setTag(this.BT_CONFIRM);
            bt1.addTouchEventListener(btcallback);
            this.addChild(bt1);

            var bt2 = new ccui.Button(res.bt_query_cancel_0_png, res.bt_query_cancel_1_png);
            bt2.setPosition(appdf.WIDTH/2-169,270);
            bt2.setTag(this.BT_CANCEL);
            bt2.addTouchEventListener(btcallback);
            this.addChild(bt2);
        }

        var sp1 = new cc.Sprite(res.sp_message_title_png);
            sp1.attr({
                x:670,
                y:550
            });
        this.addChild(sp1);

        var lb = new cc.LabelTTF(msg, res.round_body_ttf, this.DEF_TEXT_SIZE);
        lb.setColor(cc.color(160,86,47,255));
        lb.setAnchorPoint(cc.p(0.5,0.5));
        lb.setDimensions(600,180);
        lb.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        lb.setVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
        lb.attr({
            x:appdf.WIDTH/2,
            y:375
        });

        this.addChild(lb);

        this._dismiss = false;
        cc.log("run action 116");
        this.runAction( new cc.MoveTo(0.3,cc.p(0,0)));
    },


    onEnterTransitionFinish : function () {
        return this;
    },

    onExitTransitionStart : function () {
        this.unregisterScriptTouchHandler();
        return this;
    },

    setCanTouchOutside : function(canTouchOutside){
        this._canTouchOutside = canTouchOutside;
        return this;
    },

    onButtonClickedEvent :   function (tag, ref){
        cc.log("querydialog - onbuttonclickevent:"+tag);
        if(this._dismiss == true){
            return;
        }
        this.dismiss();
        if(this._callback){
            this._callback(tag == this.BT_CONFIRM);
        }
    },

    dismiss : function(){
        cc.log("querydialog  -  dismiss");
        this._dismiss = true;
        var self = this;
        this.stopAllActions();
        this.runAction(
            new cc.Sequence(
                new cc.MoveTo(0.3,cc.p(0,appdf.HEIGHT)),
                new cc.CallFunc(function () {
                    self.removeFromParent();
                })
            )
        );
    },

    dismiss2 : function(){
        this._dismiss = true;
        var that = this;
        this.stopAllActions();
        that.removeFromParent();
    }

});
