var showToast = function (context, message, delaytime, color) {
    if((context == null) || (message == null) || (delaytime < 1)) {
        return;
    }

    var msgtype = typeof message;
    if( msgtype === 'undefined' || msgtype === 'undefined' ) {
        return;
    }
    if (message === "") {
        return;
    }
    var showMessage = message;
    var bg = null; //context:getChildByName("toast_bg")
    var lab = null;
    if (bg) {
        bg.stopAllActions();
        lab = bg.getChildByName("toast_lab");
        if (! lab) {
            lab = new cc.LabelTTF(showMessage, res.round_body_ttf, 24, cc.size(930,0));
            lab.setName("toast_lab");
            if(! color) {
                lab.setColor(cc.color(255, 255, 255, 255));
            } else {
                lab.setColor(color);
            }
            bg.addChild(lab);
        }
        lab.stopAllActions();
        bg.setOpacity(255);
        lab.setOpacity(255);
        bg.runAction(new cc.sequence(new cc.delayTime(delaytime),
            new cc.Spawn(new cc.FadeTo(0.5, 0),
                new cc.CallFunc( function () {
                    lab.runAction(new cc.FadeTo(0.5, 0));
                })),
            new cc.RemoveSelf(true)));
    } else {
        bg = new ccui.ImageView(res.frame_1_png);
        bg.setOpacity(0);
        bg.setPosition(appdf.WIDTH / 2, appdf.HEIGHT / 2);
        context.addChild(bg);
        bg.setName("toast_bg");
        bg.setScale9Enabled(true);
        bg.runAction(new cc.Sequence(new cc.FadeTo(0.5, 255), new cc.DelayTime(delaytime),
            new cc.Spawn(new cc.FadeTo(0.5, 0),
                new cc.CallFunc(function () {
                    lab.runAction(new cc.FadeTo(0.5, 0));
                })),
            new cc.RemoveSelf(true)));

        lab = new cc.LabelTTF(showMessage, res.round_body_ttf , 24, cc.size(930,0));
        lab.setName("toast_lab");

        if(! color) {
            lab.setColor(cc.color(255,255,255,255));
        } else {
            lab.setColor(color);
        }
        bg.addChild(lab);
    }

    if(lab && bg) {
        lab.setString(showMessage);
        if(! color) {
            lab.setColor(cc.color(255, 255, 255, 255));
        } else {
            lab.setColor(color);
        }
        var labSize = lab.getContentSize();

        if(labSize.height < 30) {
            lab.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
            bg.setContentSize(cc.size(appdf.WIDTH, 64));
        } else {
            lab.setHorizontalAlignment(cc.TEXT_ALIGNMENT_LEFT);
            bg.setContentSize(cc.size(appdf.WIDTH, 64 + labSize.height));
        }
        lab.setPosition(appdf.WIDTH * 0.5, bg.getContentSize().height * 0.5);
    }
}

// 不渐变形式toast(fadeto方法，在安卓调用的时候会导致底图不显示)
var showToastNoFade = function(context,message, delaytime, color) {
    if((context == null) || (message == null) || (delaytime < 1)) {
        return;
    }
    var msgtype = typeof message;
    if( msgtype === 'undefined' || msgtype === 'undefined' ) {
        return;
    }
    if (message === "") {
        return;
    }
    var showMessage = message;

    var bg = context.getChildByName("toast_bg");
    var lab = null;
    if(bg) {
        bg.stopAllActions();
        lab = bg.getChildByName("toast_lab");
    } else {
        bg = new ccui.ImageView(res.frame_1_png);
        bg.setPosition(appdf.WIDTH / 2, appdf.HEIGHT / 2);
        context.addChild(bg);
        bg.setName("toast_bg");
        bg.setScale9Enabled(true);

        lab = new cc.LabelTTF(showMessage, res.round_body_ttf, 24, cc.size(930, 0));
        lab.setName("toast_lab");

        if(! color) {
            lab.setColor(cc.color(255,255,255,255));
        } else {
            lab.setColor(color);
        }
        bg.addChild(lab);
    }

    if((lab != null) && (bg != null)) {
        lab.setString(showMessage);
        if(! color) {
            lab.setColor(cc.color(255,255,255,255));
        } else {
            lab.setColor(color);
        }
        bg.runAction(new cc.Sequence(new cc.DelayTime(delaytime), new cc.RemoveSelf(true)));

        var labSize = lab.getContentSize();
        if(labSize.height < 30) {
            lab.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
            bg.setContentSize(cc.size(appdf.WIDTH, 64));
        } else {
            lab.setHorizontalAlignment(cc.TEXT_ALIGNMENT_LEFT);
            bg.setContentSize(cc.size(appdf.WIDTH, 64 + labSize.height));
        }
        lab.setPosition(appdf.WIDTH * 0.5, bg.getContentSize().height * 0.5);
    }
}