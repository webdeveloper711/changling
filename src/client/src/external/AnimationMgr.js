/* Author   :   JIN */

// 帧动画管理
function AnimationMgr() {
}

AnimationMgr.PLIST_RES = 1;
AnimationMgr.LOCAL_RES = 2;

AnimationMgr.prototype.getAnimationParam = function() {
    var returnValue = {
        m_strName : "",
        // 是否重置动画参数
        m_bResetParam : true,
        // 每帧持续时间
        m_fDelay : 0,
        // 是否恢复到第一帧
        m_bRestore : true
    };
    return returnValue;
};

// 加载帧动画
// @param[plist]    帧动画plist文件
AnimationMgr.prototype.loadAnimationFromFile = function( plist ) {
    cc.animationCache().addAnimations(plist);
};

// 加载帧动画
// @param[format]   帧文件名字format格式
// @param[start]    起始索引
// @param[count]    帧数量
// @param[key]      动画key
// @param[resType]  资源类型( 1.plist合图、2.单图)
AnimationMgr.prototype.loadAnimationFromFrame = function( format, start, count, key, resType) {
    var animation = cc.Animation.create();
    if (null == animation) {
        return;
    }
    resType = resType || AnimationMgr.PLIST_RES;

    var nBegin = start;
    var nEnd = start + count;
    for (var i = nBegin; i <= nEnd; i++) {
        var buf = sprintf(format, i);
        if (this.var_RES == resType) {
            if (jsb.fileUtils().isFileExist(buf)) {
                animation.addSpriteFrameWithFile(buf);
            }
        } else {
            var frame = new cc.SpriteFrameCache().getSpriteFrame(buf);
            if (null != frame) {
                animation.addSpriteFrame(frame);
            }
        }
    }

    cc.AnimationCache().addAnimation(animation, key);
};

// 移除动画
// @param[key]      动画key
AnimationMgr.prototype.removeCachedAnimation = function( key ) {
    cc.AnimationCache().removeAnimation(key);
};

// 播放动画
// @param[pNode]    动画播放节点
// @param[param]    动画参数
// @param[callBack] 回调
AnimationMgr.prototype.playAnimation = function( pNode, param, callBack) {
    var animation = cc.AnimationCache.getAnimation(param.m_strName);
    if (null == animation) {
        return;
    }
    // 设置参数
    if (param.m_bResetParam) {
        animation.setDelayPerUnit(param.m_fDelay);
        animation.setRestoreOriginalFrame(param.m_bRestore);
    }

    var act = new cc.Animate(animation);
    if (null == act) {
        return;
    }

    if (null != callBack) {
        pNode.runAction(new cc.Sequence(act, callBack));
    } else {
        pNode.runAction(act);
    }
};

// 获取动画
// @param[param]    动画参数
AnimationMgr.prototype.getAnimate = function( param ) {
    var animation = cc.AnimationCache.getAnimation(param.m_strName);
    if (null == animation) {
        return null;
    }

    // 设置参数
    if (param.m_bResetParam) {
        animation.setDelayPerUnit(param.m_fDelay);
        animation.setRestoreOriginalFrame(param.m_bRestore);
    }
    var act = new cc.Animate(animation);
    return act;
};
