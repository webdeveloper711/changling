/*  Author  .   Jin */

var m_tableTxtConfig = {};
//////
var emClipType =
[
    "kClipRunAct",
    "kClipReplace"
];

var ClipText = ccui.Layout.extend({

    onEnterTransitionDidFinish: function(){
        this.onEnterTransitionFinish();
    },

    ctor : function (visibleSize, str, fontName, fontSize, szType) {
        this._super();
        this._setAnchorPoint = this.setAnchorPoint;
        this.emClipType = ExternalFun.declarEnumWithTable(0, emClipType);
        this.m_szVisibleSize = visibleSize || cc.size(0,0);
        this.m_emClipType = szType;
        this.initText(str, fontName, fontSize);
    },
    
    // createClipText : function( visibleSize, str, fontName, fontSize, szType ) {
    //     // var text = this.ctor();
    //     var text = new ClipText();
    //     if (null != text) {
    //         text.m_szVisibleSize = visibleSize;
    //         text.m_emClipType = szType;
    //         text.initText(str, fontName, fontSize);
    //     }
    //     return text;
    // },
    
    // setAnchorPoint : function( anchor ) {
    //     cc.log("ClipText.setAnchorPoint.");
    //     this._setAnchorPoint(anchor);
    //     this.setTextAnchorPoint(anchor);
    // },
    
    setTextAnchorPoint : function( anchor ) {
        var pNode = this.m_text;
        if (null != pNode) {
            pNode.setAnchorPoint(anchor);
            this.updateMoveAction(pNode);
            var m_szVisibleSize = this.m_szVisibleSize;
            if (this.emClipType.kClipRunAct == this.m_emClipType) {
                if (pNode.getContentSize().width >= this.m_szVisibleSize.width) {
                    this.moveStr();
                }
            } else {
                pNode.setPosition(m_szVisibleSize.width * anchor.x, m_szVisibleSize.height * anchor.y);
            }
        }
    },
    
    setClipMode : function( mode , replaceStr) {
        this.m_emClipType = mode;
        this.m_replaceStr = replaceStr || "+.";
        var pNode = this.m_text;
    
        if (null == pNode) {
            return;
        }
    
        var anchor = pNode.getAnchorPoint();
        if (pNode.getContentSize().width >= this.m_szVisibleSize.width) {
            if (ClipText.emClipType.kClipRunAct == this.m_emClipType) {
                this.moveStr();
            } else {
                this.parseStr(this.m_strText);
            }
        }
    },
    
    setTextPosition : function( pos ) {
        if (null != this.m_text) {
            this.m_text.setPosition(pos);
            this.updateMoveAction(this.m_text);
        }
    },
    
    setString : function( str ) {
        this.m_strText = str;
        if (null == this.m_text) {
            return;
        }
        var m_text = this.m_text;
        m_text.setString(str);
        this.updateMoveAction(m_text);
    
        var anchor = m_text.getAnchorPoint();
        if (m_text.getContentSize().width >= this.m_szVisibleSize.width) {
            if (this.emClipType["kClipRunAct"] == this.m_emClipType) {
                this.moveStr();
            } else {
                this.parseStr(this.m_strText);
            }
        }
    },
    
    getString : function() {
        return this.m_strText;
    },
    
    getClipText : function() {
        return this.m_text;
    },
    
    setTextColor : function( color ) {
        if (null == this.m_text) {
            return;
        }
    
        this.m_text.setColor(color);
    },
    
    setTextFontSize : function( fSize ) {
        if (null == this.m_text) {
            return;
        }
    
        this.m_nFontSize = fSize;
        this.m_text.setFontSize(fSize);
    },
    
    initText : function( str, fontName, fontSize ) {
        //裁剪
        this.setClippingEnabled(true);
        this.setContentSize(this.m_szVisibleSize.width, this.m_szVisibleSize.height);
    
        //注册事件
    
        fontName = fontName || res.round_body_ttf;
        this.m_strFontName = fontName;
        fontSize = fontSize || 20;
        this.m_nFontSize = fontSize;
    
        this.m_strText = str;
        this.m_emClipType = this.m_emClipType || this.emClipType.kClipReplace;
    
        var text = new ccui.Text(str, fontName, fontSize);
        this.addChild(text);
        this.m_text = text;
    
        //更新移动动作
        this.updateMoveAction(text);
        //设置锚点
        this.setTextAnchorPoint(this.getAnchorPoint());
    
        if (text.getContentSize().width >= this.m_szVisibleSize.width) {
            if (this.emClipType.kClipReplace == this.m_emClipType) {
                this.parseStr(str);
            } else {
                this.moveStr();
            }
        }
    },

    //todo following touch event have to be fixed

    onTouchBegan : function (touch, event) {
        var pos = touch.getLocation();
        var child = this.m_text;
        if (null != child) {
            pos = child.convertToNodeSpace(pos);
            var rect = cc.rect(0, 0, child.getContentSize().width, child.getContentSize().height);

            if (cc.rectContainsPoint(rect, pos)) {
                return true;
            }
            return false;
        }
    },

    onTouchEnded : function (touch, event) {
        var child = this.m_text;
        if (null != child) {
            var runningAct = child.getNumberOfRunningActions();
            if (0 == runningAct) {
                var childSize = child.getContentSize();
                var m_szVisibleSize = this.m_szVisibleSize;
                if ((childSize.width >= m_szVisibleSize.width) && (this.emClipType.kClipRunAct == this.m_emClipType)) {
                    child.stopAllActions();
                    var anchor = child.getAnchorPoint();
                    child.setPosition(cc.p(m_szVisibleSize.width * anchor.x, m_szVisibleSize.height * anchor.y));
                    if (null != this.m_actMoveAct) {
                        child.runAction(this.m_actMoveAct);
                    }
                }
            }
        }
    },

    registerTouch : function(  ) {
        var onTouchBegan = function (touch, event) {
            var pos = touch.getLocation();
            var child = this.m_text;
            if (null != child) {
                pos = child.convertToNodeSpace(pos);
                var rect = cc.rect(0, 0, child.getContentSize().width, child.getContentSize().height);

                if (cc.rectContainsPoint(rect, pos)) {
                    return true;
                }
                return false;
            }
        }
    
        var onTouchEnded = function (touch, event) {
            var child = this.m_text;
            if (null != child) {
                var runningAct = child.getNumberOfRunningActions();
                if (0 == runningAct) {
                    var childSize = child.getContentSize();
                    var m_szVisibleSize = this.m_szVisibleSize;
                    if ((childSize.width >= m_szVisibleSize.width) && (this.emClipType.kClipRunAct == this.m_emClipType)) {
                        child.stopAllActions();
                        var anchor = child.getAnchorPoint();
                        child.setPosition(cc.p(m_szVisibleSize.width * anchor.x, m_szVisibleSize.height * anchor.y));
                        if (null != this.m_actMoveAct) {
                            child.runAction(this.m_actMoveAct);
                        }
                    }
                }
            }
        };
    
        var listener = new cc.EventListenerTouchOneByOne();
        this.listener = listener;
    
        listener.registerScriptHandler(onTouchBegan, cc.Handler.TOUCH_BEGAN);
        listener.registerScriptHandler(onTouchEnded, cc.Handler.TOUCH_ENDED);
        var eventDispatcher = this.getEventDispatcher();
        eventDispatcher.addEventListenerWithSceneGraphPriority(listener, this);
    },
    
    onExit : function(  ) {
        /*TODO: manually commented by vs23
        // var eventDispatcher = this.getEventDispatcher();
        // eventDispatcher.removeEventListener(this.listener);*/
    
        ExternalFun.SAFE_RETAIN(this.m_actMoveAct);
    },
    
    onEnterTransitionFinish : function(  ) {
        // this.registerTouch();
    },
    
    updateMoveAction : function( text ) {
        ExternalFun.SAFE_RELEASE(this.m_actMoveAct);
        var m_szVisibleSize = this.m_szVisibleSize;
    
        var anchor = text.getAnchorPoint();
        var textSize = text.getContentSize();
        var pos = cc.p(-textSize.width, text.getPositionY());
        var moveBy = new cc.MoveTo(5.0 + (textSize.width / m_szVisibleSize.width) * 2.0, pos);
        var delay = new cc.DelayTime(1.5);
        var back = new cc.CallFunc(function () {
            text.setPosition(cc.p(text.getContentSize().width * anchor.x + m_szVisibleSize.width, m_szVisibleSize.height * anchor.y));
        });
        var seq = new cc.Sequence(moveBy, delay, back);
        var repeatAct = new cc.Repeat(seq, 3);
        var backOrigin = new cc.MoveTo(3.0, cc.p(m_szVisibleSize.width * anchor.x, m_szVisibleSize.height * anchor.y));
        this.m_actMoveAct = new cc.Sequence(repeatAct, backOrigin);
    
        ExternalFun.SAFE_RETAIN(this.m_actMoveAct);
    },
    
    moveStr : function(  ) {
        var m_szVisibleSize = this.m_szVisibleSize;
        var text = this.m_text;
        if (null == text) {
            return;
        }
    
        var anchor = text.getAnchorPoint();
        text.stopAllActions();
        var pos = cc.p(text.getContentSize().width * anchor.x + m_szVisibleSize.width, m_szVisibleSize.height * anchor.y);
        text.setPosition(pos);
        text.runAction(this.m_actMoveAct);
    },
    
    parseStr : function( str ) {
        var nTotal = this.m_szVisibleSize.width - this.m_nFontSize;
    
        var config = this.getFontConfig(this.m_strFontName, this.m_nFontSize);
    
        var pStr = this.stringEllipsis(str, config, nTotal, this.m_replaceStr);
        this.m_text.setString(pStr);
    },
    
    //依据宽度截断字符
    stringEllipsis : function(szText, config, maxWidth, replaceStr) {
        replaceStr = replaceStr || "...";
        //当前计算宽度
        var width = 0;
        //截断结果
        var szResult = "...";
        //完成判断
        var bOK = false;
    
        var i = 0;
    
        while (true) {
            var cur = szText.substring(i, i);
    
            // var byte = string.byte(cur);
            var byte = cur.charCodeAt();
            if (byte == null) {
                break;
            }
            if (byte > 128) {
                if (width <= maxWidth - 3 * config.upperEnSize.width) {
                    width = width + config.cnSize.width;
                    i = i + 3;
                } else {
                    bOK = true;
                    break;
                }
            } else if (byte != 32) { //区分大小写和数字
                if (width <= maxWidth - 3 * config.upperEnSize.width) {
                    if (("A").charCodeAt() <= byte && byte <= ('Z').charCodeAt()) {
                        width = width + config.upperEnSize.width;
                    } else if (('a').charCodeAt() <= byte && byte <= ('z').charCodeAt()) {
                        width = width + config.lowerEnSize.width;
                    } else {
                        width = width + config.numSize.width;
                    }
                    i = i + 1;
                } else {
                    bOK = true;
                    break;
                }
            } else {
                i = i + 1;
            }
        }
    
        if (i != 0) {
            szResult = szText.substring(0, i - 1);
            if (bOK) {
                szResult = szResult + replaceStr;
            }
        }
        return szResult
    },
    
    //获取配置记录
    getFontConfig : function(fontfile, fontsize) {
        var strKey = sprintf("%s-%d", fontfile, fontsize);
        var config = m_tableTxtConfig[strKey];
        if (null == config) {
            config = {};
            var tmpEN = new cc.LabelTTF("A", fontfile, fontsize);
            var tmpCN = new cc.LabelTTF("网", fontfile, fontsize);
            var tmpen = new cc.LabelTTF("a", fontfile, fontsize);
            var tmpNu = new cc.LabelTTF("2", fontfile, fontsize);
            config.upperEnSize = tmpEN.getContentSize();
            config.cnSize = tmpCN.getContentSize();
            config.lowerEnSize = tmpen.getContentSize();
            config.numSize = tmpNu.getContentSize();
    
            m_tableTxtConfig[strKey] = config;
        }
        return config;
    },
    
    //计算字符串长度
    getLabelWidth : function(szText, fontfile, fontsize) {
        var begin = 0;
        var szLen = szText.length;
        var gsubSize = 1;
        var szWidth = 0;
        var config = this.getFontConfig(fontfile, fontsize);
    
        while (true) {
            if (begin >= szLen) {
                break;
            }
    
            var cur = szText.substring(begin, begin + 1);
            var byte = (cur).charCodeAt();
            if (byte == null) {
                break;
            }
            if (byte > 128) {
                szWidth = szWidth + config.cnSize.width;
                gsubSize = 3;
            } else if (byte != 32) { //区分大小写和数字
                if (('A').charCodeAt() <= byte && byte <= ('Z').charCodeAt()) {
                    szWidth = szWidth + config.upperEnSize.width;
                } else if (('a').charCodeAt() <= byte && byte <= ('z').charCodeAt()) {
                    szWidth = szWidth + config.lowerEnSize.width;
                } else {
                    szWidth = szWidth + config.numSize.width;
                }
                gsubSize = 1;
            } else {
                gsubSize = 1;
            }
            begin = begin + gsubSize;
        }
        return [szWidth, config];
    }
});


