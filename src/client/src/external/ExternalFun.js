/**
 * @author  .   JIN
 * reviewed by Jong 2017-12-02 12:35 AM
 * @Todo: Fix all string functions
 * */
 
// 通用扩展

var ExternalFun = {};

//枚举声明
ExternalFun.declarEnum = function( ENSTART, k ) {
    var enStart = 1;
    if (null != ENSTART) {
        enStart = ENSTART;
    }

    var args = [k];
    var enumValue = [];
    for (var i = 0; i < args.length; i++) {
        enumValue[args[i]] = enStart;
        enStart = enStart + 1;
    }

    return enumValue;
};

ExternalFun.declarEnumWithTable = function( ENSTART, keyTable ) {
    var enStart = 1;
    if (null != ENSTART) {
        enStart = ENSTART;
    }

    var args = keyTable;
    var enumValue = [];
    for (var i = 0; i < args.length; i++) {
        enumValue[args[i]] = enStart;
        enStart = enStart + 1;
    }

    return enumValue;
};

ExternalFun.SAFE_RELEASE = function( vars ){
	if (null != vars) {
        vars.release();
    }
};

ExternalFun.SAFE_RETAIN = function( vars ) {
    if (null != vars) {
        vars.retain();
    }
};

ExternalFun.enableBtn = function( btn, bEnable, bHide ){
	if (null == bEnable) {
        bEnable = false;
    }
	if (null == bHide) {
        bHide = false;
    }

	btn.setEnabled(bEnable);
	if (bEnable) {
        btn.setVisible(true);
        btn.setOpacity(255);
    } else {
        if (bHide) {
            btn.setVisible(false);
        } else {
            btn.setOpacity(125);
        }
    }
 };

//格式化长整形
ExternalFun.formatScore = function( llScore ) {
    return llScore;
    /*var str = string_formatNumberThousands(llScore);
    if (str.length >= 4) {
        str = string.sub(str, 1, -4);
        str = (string.gsub(str, ",", ""));
        return str;
    } else {
        return "";
    }*/
};

//无小数点 NumberThousands
ExternalFun.numberThousands = function( llScore ) {
    /*var str = string_formatNumberThousands(llScore);
    if (str.length >= 4) {
        return string.sub(str, 1, -4);
    } else {
        return "";
    }*/
    return string_formatNumberThousands(llScore, true);
};

var debug_mode = null;
//读取网络消息
ExternalFun.read_datahelper = function( param ) {
    if (debug_mode) {
        cc.log("read. " + param.strkey + " helper");
    }

    if (null != param.lentable) {
        var lentable = param.lentable;
        var depth = lentable.length;

        if (debug_mode) {
            cc.log("depth ==> " + depth);
        }

        var tmpT = [];
        for (var i = 0; i < depth; i++) {
            var entryLen = lentable[i];
            if (debug_mode) {
                cc.log("entryLen ==> " + entryLen);
            }

            var entryTable = [];
            for (var j = 0; j < entryLen; j++) {
                var entry = param.fun();
                if (debug_mode) {
                    if (typeof entry == "boolean") {
                        cc.log("value ==> " + (entry && "true" || "false"));
                    } else {
                        cc.log("value ==> " + entry);
                    }
                }
                entryTable.push(entry);
            }
            tmpT.push(entryTable);
        }

        return tmpT;
    } else {
        if (debug_mode) {
            var value = param.fun();
            if (typeof value == "boolean") {
                cc.log("value ==> " + (value && "true" || "false"))
            } else {
                cc.log("value ==> " + value);
            }
            return value;
        } else {
            return param.fun();
        }
    }
};

ExternalFun.readTableHelper = function( param ) {
    var templateTable = param.dTable || {};
    var strkey = param.strkey || "default";
    if (null != param.lentable) {
        var lentable = param.lentable;
        var depth = lentable.length;
        if (debug_mode) {
            cc.log("depth ==> " + depth);
        }

        var tmpT = [];
        for (var i = 0; i < depth; i++) {
            var entryLen = lentable[i];
            if (debug_mode) {
                cc.log("entryLen ==> " + entryLen);
            }

            var entryTable = [];
            for (var j = 0; j < entryLen; j++) {
                var entry = this.read_netdata(templateTable, param.buffer);
                if (debug_mode) {
                    dump(entry + " " + strkey + " ==> " + j);
                }
                entryTable.push(entry);
            }
            tmpT.push(entryTable);
        }

        return tmpT;
    } else {
        if (debug_mode) {
            var value = this.read_netdata(templateTable, param.buffer);
            cc.log(value + ":" + strkey);
            return value;
        } else {
            return this.read_netdata(templateTable, param.buffer);
        }
    }
};

/*
******
* 结构体描述
* Struct Description (keyTable)
*
* {k = "key", t = "type", s = len, l = []}
* k 表示字段名,对应C++结构体变量名
    field name
* t 表示字段类型,对应C++结构体变量类型
    field type
* s 针对string变量特有,描述长度
    length of string
* l 针对数组特有,描述数组长度,以table形式,一维数组表示为{N},N表示数组长度,多维数组表示为{N,N},N表示数组长度
    lengths of multi-dimension array
* d 针对table类型,即该字段为一个table类型,d表示该字段需要读取的table数据
    about table(associative array) type, indicates the field is table data
* ptr 针对数组,此时s必须为实际长度

** example
* 取数据的时候,针对一维数组,假如有字段描述为 {k = "a", t = "byte", l = {3}}
* 则表示为 变量a为一个byte型数组,长度为3
    a is byte array and its length is 3
* 取第一个值的方式为 a[0][0],第二个值a[0][1],依此类推
    To get first element, a[0][0], second is a[0][1], ...

* 取数据的时候,针对二维数组,假如有字段描述为 {k = "a", t = "byte", l = {3,3}}
* 则表示为 变量a为一个byte型二维数组,长度都为3
    a is 2D byte array, size is 3X3
* 则取第一个数组的第一个数据的方式为 a[0][0], 取第三个数组的第二个数据的方式为 a[2][1]
    get values as a[0][0], a[2][1], ...
******
]]
*/
/**
 * 读取网络消息 Read network data
 * @param {Array} keyTable
 * @param {Cmd_Data} dataBuffer
 * @returns {Array}
 */
ExternalFun.read_netdata = function( keyTable, dataBuffer ) {
    var cmd_table = [];
    for (var k in keyTable) {
        var keys = keyTable[k];

        //////
        //读取数据
        //类型
        var keyType = keys.t.toLowerCase();
        //键
        var key = keys.k;
        //长度
        var lenT = keys.l;
        var keyFun = null;
        if ("byte" == keyType) {
            keyFun = function () {
                return dataBuffer.readbyte();
            }
        } else if ("int" == keyType) {
            keyFun = function () {
                return dataBuffer.readint();
            }
        } else if ("word" == keyType) {
            keyFun = function () {
                return dataBuffer.readword();
            }
        } else if ("dword" == keyType) {
            keyFun = function () {
                return dataBuffer.readdword();
            }
        } else if ("score" == keyType) {
            keyFun = function () {
                return dataBuffer.readscore();
            }
        } else if ("string" == keyType) {
            if ( null != keys.s) {
                keyFun = function () {
                    return dataBuffer.readstring(keys.s);
                }
            } else {
                keyFun = function () {
                    return dataBuffer.readstring();
                }
            }
        } else if ("bool" == keyType) {
            keyFun = function () {
                return dataBuffer.readbool();
            }
        } else if ("table" == keyType) {
            cmd_table[key] = this.readTableHelper({
                dTable: keys["d"],
                lentable: lenT,
                buffer: dataBuffer,
                strkey: key
            });
        } else if ("double" == keyType) {
            keyFun = function () {
                return dataBuffer.readdouble();
            }
        } else if ("float" == keyType) {
            keyFun = function () {
                return dataBuffer.readfloat();
            }
        } else if ("short" == keyType) {
            keyFun = function () {
                return dataBuffer.readshort();
            }
        } else {
            cc.log("read_netdata error. key ==> %d type==> %d", key, keyType);
        }
        //cc.log("databuffer in ExternalFun : " + dataBuffer);
        if (null != keyFun) {
            cmd_table[key] = this.read_datahelper({
                strkey: key,
                lentable: lenT,
                fun: keyFun
            });
        }
    }


    return cmd_table;
};

//创建网络消息包
ExternalFun.create_netdata = function( keyTable ) {
    var len = 0;
    for (var i = 0; i < keyTable.length; i++) {
        var keys = keyTable[i];
        var keyType = keys["t"].toLowerCase();

        // 数组长度计算 Calculate array length
        var keyLen = 0;
        if ("byte" == keyType || "bool" == keyType) {
            keyLen = 1;
        } else if ("score" == keyType || "double" == keyType) {
            keyLen = 8;
        } else if ("word" == keyType || "short" == keyType) {
            keyLen = 2;
        } else if ("dword" == keyType || "int" == keyType || "float" == keyType) {
            keyLen = 4;
        } else if ("string" == keyType) {
            keyLen = keys["s"];
        } else if ("tchar" == keyType) {
            keyLen = keys["s"] * 2;
        } else if ("ptr" == keyType) {
            keyLen = keys["s"];
        } else {
            cc.log("error keytype ==> " + keyType);
        }

        len = len + keyLen;
    }
    cc.log("net len ==> " + len);
    return new Cmd_Data(len);
};

//导入包
ExternalFun.req_var = function( module_name ) {
    // cc.log("ExternalFun.req_var: "+module_name);
    // appdf.assert(false, "ExternalFun.req_var", 2);
    if ((null != module_name) && ("string" == typeof(module_name))) {
        return require(module_name);
    }
};

//加载界面根节点，设置缩放达到适配
ExternalFun.loadRootCSB = function( csbFile, parent ) {
    var rootlayer = new ccui.Layout();
    rootlayer.setContentSize(1335, 750); //这个是资源设计尺寸;
    rootlayer.setScale(yl.WIDTH / 1335);
    if (null != parent) {
        parent.addChild(rootlayer);
    }
    var csbnode = ccs.load(csbFile).node;
    rootlayer.addChild(csbnode);

    return {"rootlayer" : rootlayer, "csbnode" : csbnode};
};

//加载csb资源
ExternalFun.loadCSB = function(csbFile, parent) {
    var csbnode = ccs.load(csbFile).node;
    if (null != parent) {
        parent.addChild(csbnode);
    }
    return csbnode;
};

//加载 帧动画
ExternalFun.loadTimeLine = function( csbFile ) {
    return cc.CSLoader.createTimeline(csbFile);
};

//注册node事件
ExternalFun.registerNodeEvent = function( node ) {
    cc.log("ExternalFun.registerNodeEvent called!");
    if (null == node) {
        return;
    }
    var onNodeEvent = function (event) {
        if (event == "enter" && null != node.onEnter) {
            node.onEnter();
        } else if (event == "enterTransitionFinish" && null != node.onEnterTransitionFinish) {
            node.onEnterTransitionFinish();
        } else if (event == "exitTransitionStart" && null != node.onExitTransitionStart) {
            node.onExitTransitionStart();
        } else if (event == "exit" && null != node.onExit) {
            node.onExit();
        } else if (event == "cleanup" && null != node.onCleanup) {
            node.onCleanup();
        }
    };

    node.registerScriptHandler(onNodeEvent);
};

//注册touch事件
ExternalFun.registerTouchEvent = function( node, bSwallow ) {
    if (null == node) {
        return false;
    }
    var onNodeEvent = function (event) {
        if (event == "enter" && null != node.onEnter) {
            node.onEnter();
        } else if (event == "enterTransitionFinish") {
            //注册触摸
            var onTouchBegan = function (touch, event) {
                if (null == node.onTouchBegan) {
                    return false;
                }
                return node.onTouchBegan(touch, event);
            };

            var onTouchMoved = function (touch, event) {
                if (null != node.onTouchMoved) {
                    node.onTouchMoved(touch, event);
                }
            };

            var onTouchEnded = function (touch, event) {
                if (null != node.onTouchEnded) {
                    node.onTouchEnded(touch, event);
                }
            };

            var listener = cc.EventListenerTouchOneByOne()
            bSwallow = bSwallow || false;
            listener.setSwallowTouches(bSwallow);
            node._listener = listener;
            listener.registerScriptHandler(onTouchBegan, cc.Handler.TOUCH_BEGAN);
            listener.registerScriptHandler(onTouchMoved, cc.Handler.TOUCH_MOVED);
            listener.registerScriptHandler(onTouchEnded, cc.Handler.TOUCH_ENDED);
            var eventDispatcher = node.getEventDispatcher();
            eventDispatcher.addEventListenerWithSceneGraphPriority(listener, node);

            if (null != node.onEnterTransitionFinish) {
                node.onEnterTransitionFinish();
            }
        } else if ((event == "exitTransitionStart")
            && (null != node.onExitTransitionStart)) {
            node.onExitTransitionStart();
        } else if (event == "exit" && null != node.onExit) {
            if (null != node._listener) {
                var eventDispatcher = node.getEventDispatcher();
                eventDispatcher.removeEventListener(node._listener);
            }

            if (null != node.onExit) {
                node.onExit();
            }
        } else if (event == "cleanup" && null != node.onCleanup) {
            node.onCleanup();
        }
    };
    //node.registerScriptHandler(onNodeEvent);
    return true;
};

var filterLexicon = {};
//加载屏蔽词库
ExternalFun.loadLexicon = function( ) {
    var startTime = new Date();
    var str = cc.loader.getRes(res.Service_txt);
    // var str = cc.loader.getRes(res.badwords_txt);
    cc.log(str);
    // var str = jsb.fileUtils.getStringFromFile("public/badwords.txt");

    if ("{" != str.substring(0, 0) || "}" != str.substring(-1, -1)) {
        cc.log("[WARN] load lexicon error!!!");
        return;
    }
    str = "return" + str;
    //TODO:loadstring()?
    var fuc = loadstring(str);


    if (null != fuc && typeof fuc == "function") {
        filterLexicon = fuc();
    }

    var endTime = new Date();
    cc.log("load time ==> " + endTime - startTime);
};

// ExternalFun.loadLexicon();

//判断是否包含过滤词
ExternalFun.isContainBadWords = function( str ) {
    var startTime = new Date();

    cc.log("origin ==> " + str);
    //特殊字符过滤
    str = str.substring("[%w '|/?·`,;.~!@#$%^&*()-_。，、+]", "");
    cc.log("gsub ==> " + str);
    //是否直接为敏感字符
    var res = filterLexicon[str];
    //是否包含
    for (var k in filterLexicon) {
        var k = str.indexOf(k);
        // if (null != b || null != e) {
        if (k > -1) {
            res = true;
            break;
        }
    }

    var endTime = new Date();
    cc.log("excute time ==> " + endTime - startTime);

    return res != null;
};

//utf8字符串分割为单个字符
ExternalFun.utf8StringSplit = function( str ) {
    var strTable = [];
    for (uchar in str.match("[%z\1-\127\194-\244][\128-\191]*")) {
        strTable[strTable.length] = uchar;
    }
    return strTable;
};

ExternalFun.replaceAll = function(src, regex, replacement) {
    return src.replace(regex, replacement);
};

ExternalFun.cleanZero = function(s) {
    // 如果传入的是空串则继续返回空串
    if ("" == s) {
        return "";
    }

    // 字符串中存在多个'零'在一起的时候只读出一个'零'，并省略多余的单位

    var regex1 = ["零仟", "零佰", "零拾"];
    var regex2 = ["零亿", "零万", "零元"];
    var regex3 = ["亿", "万", "元"];
    var regex4 = ["零角", "零分"];

    // 第一轮转换把 "零仟", 零佰","零拾"等字符串替换成一个"零"
    for (var i = 0; i < 3; i++) {
        s = this.replaceAll(s, regex1[i], "零");
    }

    // 第二轮转换考虑 "零亿","零万","零元"等情况
    // "亿","万","元"这些单位有些情况是不能省的，需要保留下来
    for (i = 0; i < 3; i++) {
        // 当第一轮转换过后有可能有很多个零叠在一起
        // 要把很多个重复的零变成一个零
        s = this.replaceAll(s, "零零零", "零");
        s = this.replaceAll(s, "零零", "零");
        s = this.replaceAll(s, regex2[i], regex3[i]);
    }

    // 第三轮转换把"零角","零分"字符串省略
    for (i = 0; i < 2; i++) {
        s = this.replaceAll(s, regex4[i], "");
    }

    // 当"万"到"亿"之间全部是"零"的时候，忽略"亿万"单位，只保留一个"亿"
    s = this.replaceAll(s, "亿万", "亿")

    //去掉单位
    s = this.replaceAll(s, "元", "");
    return s;
};

//人民币阿拉伯数字转大写
ExternalFun.numberTransiform = function(strCount) {
    var big_num = ["零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖"];
    var big_mt = {
        __index: function () {
            return "";
        }
    };
    setmetatable(big_num, big_mt);
    var unit = ["元", "拾", "佰", "仟", "万",
        //拾万位到千万位
        "拾", "佰", "仟",
        //亿万位到万亿位
        "亿", "拾", "佰", "仟", "万",];
    var unit_mt = {
        __index: function () {
            return "";
        }
    };
    setmetatable(unit, unit_mt);
    var tmp_str = "";
    var len = strCount.length;
    for (var i = 0; i < len; i++) {
        tmp_str = tmp_str + big_num[strCount.charCodeAt(i) - 47] + unit[len - i + 1];
    }
    return this.cleanZero(tmp_str);
};

//播放音效 (根据性别不同播放不同的音效)
ExternalFun.playSoundEffect = function( path, useritem ) {
    var sound_path = path;
    if (null == useritem) {
        sound_path = "sound_res/" + path;
    } else {
        // 0.女/1.男
        var gender = useritem.cbGender;
        sound_path = sprintf("sound_res/%d/%s", gender, path);
    }
    if (GlobalUserItem.bVoiceAble) {
        cc.audioEngine.playEffect(sound_path, false);
    }
}

ExternalFun.playClickEffect = function( ) {
    if (GlobalUserItem.bVoiceAble) {
        cc.audioEngine.playEffect(cc.FileUtils.getInstance().fullPathForFilename("sound/Click.wav"), false);
    }
}

//播放背景音乐
ExternalFun.playBackgroudAudio = function( bgfile ) {
    var strfile = bgfile;
    if (null == bgfile) {
        strfile = "backgroud.wav";
    }
    strfile = "sound_res/" + strfile;
    if (GlobalUserItem.bSoundAble) {
        cc.audioEngine.playMusic(strfile, true);
    }
};

//播放大厅背景音乐
ExternalFun.playPlazzBackgroudAudio = function( ) {
    if (GlobalUserItem.bSoundAble) {
        cc.audioEngine.playMusic(res.audio_background01_mp3, true);
    }
};
/* Use array length of js instead of this */
// //中文长度计算(同步pc,中文长度为2)
// ExternalFun.stringLen = function(szText) {
//     var len = 0;
//     var i = 0;
//     while (true) {
//         //TODO:string.sub!!!
//         var cur = szText.substring(i, i+1);
//         var byte = cur.charCodeAt();
//         if (byte == null || isNaN(byte)) {
//             break;
//         }
//         if (byte > 128) {
//             i = i + 1;
//             len = len + 2;
//         } else {
//             i = i + 1;
//             len = len + 1;
//         }
//     }
//     return len;
// };

//webview 可见设置(已知在4s设备上设置可见会引发bug)
ExternalFun.visibleWebView = function(webview, visible) {
    if (null == webview) {
        return;
    }

    var target = cc.Application.getInstance().getTargetPlatform();
    if (target == cc.sys.iphone() || target == cc.sys.ipad()) {
        var size = cc.director().getOpenGLView().getFrameSize();
        var con = math.max(size.width, size.height);
        if (con != 960) {
            webview.setVisible(visible);
            return true;
        }
    } else {
        webview.setVisible(visible);
        return true;
    }
    return false;
};

// 过滤emoji表情
// 编码为 226 的emoji字符,不确定是否是某个中文字符
// [%z\48-\57\64-\126\226-\233][\128-\191] 正则匹配式去除了226
ExternalFun.filterEmoji = function(str) {
    var newstr = "";
    cc.log(str.charCodeAt());
    for (unchar in str.match("[%z\25-\57\64-\126\227-\240][\128-\191]*")) {
        newstr = newstr + unchar;
    }
    cc.log(newstr);
    return newstr;
};

// 判断是否包含emoji
// 编码为 226 的emoji字符,不确定是否是某个中文字符
ExternalFun.isContainEmoji = function(str){
    //TODO: create a native function for containEmoji //closed by lsh since error in Android
    // if (null != containEmoji) {
    //     return containEmoji(str);
    // }
    //TODO:string.utf8len()?
    // var origincount = string.utf8len(str);
	var origincount = str.length;//TODO:string.utf8len()
    cc.log("origin " + origincount);
	var count = 0;
	for (unchar in str.match("[%z\25-\57\64-\126\227-\240][\128-\191]*")){
        cc.log(unchar.charCodeAt());
        if (unchar.length < 4) {
            count = count + 1;
        }
    }
	cc.log("newcount " + count);
	return count != origincount;
};

var TouchFilter = cc.Layer.extend({
    ctor : function(showTime, autohide, msg) {
        this._super();
        showTime = showTime || 2;
        this.m_msgTime = showTime;
        if (autohide) {
            this.runAction(new cc.Sequence(new cc.DelayTime(showTime), new cc.RemoveSelf(true)));
        }
        this.m_filterMsg = msg;
    },

    onTouchBegan : function(touch, event) {
        return this.isVisible();
    },

    onTouchEnded : function(touch, event) {
         cc.log("TouchFilter.onTouchEnded");
         if (typeof(this.m_filterMsg) == "string" && "" != this.m_filterMsg) {
             showToast(this, this.m_filterMsg, this.m_msgTime);
         }
    }
});

var TOUCH_FILTER_NAME = "__touch_filter_node_name__";
//触摸过滤
ExternalFun.popupTouchFilter = function( showTime, autohide, msg, parent ) {
    var filter = new TouchFilter(showTime, autohide, msg);
    var runScene = parent || cc.director.getRunningScene();
    if (null != runScene) {
        var lastfilter = runScene.getChildByName(TOUCH_FILTER_NAME);
        if (null != lastfilter) {
            lastfilter.stopAllActions();
            lastfilter.removeFromParent();
        }
        if (null != filter) {
            filter.setName(TOUCH_FILTER_NAME);
            runScene.addChild(filter, yl.ZORDER.Z_FILTER_LAYER);
        }
    }
};

ExternalFun.dismissTouchFilter = function() {
    var runScene = cc.director.getRunningScene();
    if (null != runScene) {
        var filter = runScene.getChildByName(TOUCH_FILTER_NAME);
        if (null != filter) {
            filter.stopAllActions();
            filter.removeFromParent();
        }
    }
};

// eg. 10000 转 1.0万
ExternalFun.formatScoreText = function(score) {
    var scorestr = this.formatScore(score);
    if (score < 10000) {
        return scorestr;
    }

    if (score < 100000000) {
        scorestr = sprintf("%.2f万", score / 10000);
        return scorestr;
    }
    scorestr = sprintf("%.2f亿", score / 100000000);
    return scorestr;
};

// 随机ip地址
var external_ip_long =
[
	[ 607649792, 608174079 ], // 36.56.0.0-36.63.255.255
    [ 1038614528, 1039007743 ], // 61.232.0.0-61.237.255.255
    [ 1783627776, 1784676351 ], // 106.80.0.0-106.95.255.255
    [ 2035023872, 2035154943 ], // 121.76.0.0-121.77.255.255
    [ 2078801920, 2079064063 ], // 123.232.0.0-123.235.255.255
    [ -1950089216, -1948778497 ], // 139.196.0.0-139.215.255.255
    [ -1425539072, -1425014785 ], // 171.8.0.0-171.15.255.255
    [ -1236271104, -1235419137 ], // 182.80.0.0-182.92.255.255
    [ -770113536, -768606209 ], // 210.25.0.0-210.47.255.255
    [ -569376768, -564133889 ], // 222.16.0.0-222.95.255.255
];
ExternalFun.random_longip = function() {
    var rand_key = math.random(1, 10);
    var bengin_long = external_ip_long[rand_key][1] || 0;
    var end_long = external_ip_long[rand_key][2] || 0;
    return Math.random(bengin_long, end_long);
}

ExternalFun.long2ip = function( value ) {
    if (!value) {
        return {
            p: 0,
            m: 0,
            s: 0,
            b: 0
        };
    }
    if (null == bit) {
        cc.log("not support bit module");
        return {
            p: 0,
            m: 0,
            s: 0,
            b: 0
        };
    }
    var tmp;
    if (typeof value != "number") {
        tmp = Number(value);
    } else {
        tmp = value;
    }
    return {
        p: bit._rshift(bit._and(tmp, 0xFF000000), 24),
        m: bit._rshift(bit._and(tmp, 0x00FF0000), 16),
        s: bit._rshift(bit._and(tmp, 0x0000FF00), 8),
        b: bit._and(tmp, 0x000000FF)
    };
};

ExternalFun._dump = function(value, desciption, indent, nest) {
    var result = indent + "\"" + desciption + "\" = ";
    if( nest === 0 ) return result + "*MAX NESTING*";

    nest -= 1;

    if( typeof value === "object") {
        var newIndent = indent + "    ";
        result += "{\n";

        for(var k in value){
            result += this._dump(value[k], k, newIndent, nest) + "\n";
        }
        result += indent + "}\n";
    } else if( typeof value === "string"){
        result += "\"" + value + "\"";
    } else if( typeof value === "number"){
        result += value;
    } else if (typeof value === "boolean") {
        result += (value && "true" || "false");
    } else {
        result += "unknown type:" + typeof value;
    }

    return result;
}

ExternalFun.dump = function( value, desciption, nesting ) {
    var logStr = "";
    if( value == null ) logStr = desciption + " = null";
    else {
        if (typeof nesting !== "number") {
            nesting = 3;
        }
        logStr = this._dump(value, desciption, "- ", nesting+1 );
    }

    cc.log(logStr);
}