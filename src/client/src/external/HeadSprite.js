/*  Author  :   JIN  */

//游戏头像
var HeadSprite = cc.Sprite.extend({

    //注册事件
    onEnterTransitionDidFinish: function(){
        this.onEnterTransitionFinish();
    },

    ctor : function( ) {
        this._super();
        //缓存头像
        this.loadAllHeadFrame();
        this.m_spRender = null;
        this.m_downListener = null;
        this.m_resizeListener = null;

        this.m_headSize = 88;
        this.m_useritem = null;
        this.listener = null;
        this.m_bEnable = false;
        //是否头像
        this.m_bFrameEnable = false;
        //头像配置
        this.m_tabFrameParam = {};
    },

    checkData : function(useritem) {
        useritem = useritem || {};
        useritem.dwUserID = useritem.dwUserID || 0;
        useritem.dwCustomID = useritem.dwCustomID || 0;
        useritem.wFaceID = useritem.wFaceID || 0;
        if (useritem.wFaceID > 199) {
            useritem.wFaceID = 0;
        }
        useritem.cbMemberOrder = useritem.cbMemberOrder || 0;
    
        return useritem;
    },
    
    //创建普通玩家头像
    createNormal : function( useritem ,headSize) {
        if (null == useritem) {
            //return
        }
        useritem = this.checkData(useritem);
        var sp = new HeadSprite();
        sp.m_headSize = headSize;
        var spRender = sp.initHeadSprite(useritem);
        if (null != spRender) {
            sp.addChild(spRender);
            var thisSize = sp.getContentSize();
            spRender.setPosition(cc.p(thisSize.width * 0.5, thisSize.height * 0.5));
            return sp;
        }
    
        return null;
    },
    
    //创建裁剪玩家头像
    createClipHead : function( useritem, headSize, clippingfile ) {
        useritem = (new HeadSprite()).checkData(useritem);
    
        var sp = new HeadSprite();
        sp.m_headSize = headSize;
        var spRender = sp.initHeadSprite(useritem);
        if (null == spRender) {
            return null;
        }
    
        //创建裁剪
        var strClip = "head_mask.png";
        if (null != clippingfile) {
            strClip = clippingfile;
        }
        var clipSp = null;
        var frame = cc.spriteFrameCache.getSpriteFrame(strClip);
        if (null != frame) {
            clipSp = new cc.Sprite(frame);
        } else {
            clipSp = new cc.Sprite(strClip);
        }
        if (null != clipSp) {
            //裁剪
            var clip = new cc.ClippingNode();
            clip.setStencil(clipSp);
            clip.setAlphaThreshold(0);
            clip.addChild(spRender);
            var thisSize = sp.getContentSize();
            clip.setPosition(cc.p(thisSize.width * 0.5, thisSize.height * 0.5));
            sp.addChild(clip);
            return sp;
        }
    
        return null;
    },
    
    updateHead : function( useritem ) {
        if (null == useritem) {
            return;
        }
        this.m_useritem = useritem;
    
        //判断是否进入防作弊房间
        var bAntiCheat = GlobalUserItem.isAntiCheatValid(useritem.dwUserID);
    
        //更新头像框
        if (this.m_bFrameEnable && false == bAntiCheat) {
            //根据会员等级配置
            var vipIdx = this.m_useritem.cbMemberOrder || 0;
            var framestr = sprintf("sp_frame_%d_0.png", vipIdx);
            var deScale = 1.1;
    
            var framefile = this.m_tabFrameParam._framefile || framestr;
            var scaleRate = this.m_tabFrameParam._scaleRate || deScale;
            var zorder = this.m_tabFrameParam._zorder || 1;
            this.updateHeadFrame(framefile, scaleRate).setLocalZOrder(zorder);
        }
    
        if (null != useritem.dwCustomID && 0 != useritem.dwCustomID && false == bAntiCheat) {
            //判断是否有缓存
            var framename = useritem.dwUserID + "_custom_" + useritem.dwCustomID + ".ry";
            var frame = cc.spriteFrameCache.getSpriteFrame(framename);
            if (null != frame) {
                this.updateHeadByFrame(frame);
                return;
            }
        }
        //系统头像
        var faceid = useritem.wFaceID || 0;
        if (true == bAntiCheat) {
            faceid = 0;
        }
        var str = sprintf("Avatar%d.png", faceid);
        var frame = cc.spriteFrameCache.getSpriteFrame(str);
        if (null != frame) {
            this.m_spRender.setSpriteFrame(frame);
            this.setContentSize(this.m_spRender.getContentSize());
        }
        this.m_fScale = this.m_headSize / SYS_HEADSIZE;
        this.setScale(this.m_fScale);
    
        return sp;
    },
    
    updateHeadByFrame : function(frame){
        if (null == this.m_spRender) {
            this.m_spRender = new cc.Sprite(frame);
        } else {
            this.m_spRender.setSpriteFrame(frame);
        }
        cc.log("width " + this.m_spRender.getContentSize().width + " height " + this.m_spRender.getContentSize().height);
    
        cc.log("<DrawThirdFace> [initHeadSprite] [updateHeadByFrame]");
    
        this.setContentSize(this.m_spRender.getContentSize());
        this.m_fScale = this.m_headSize / SYS_HEADSIZE;
        this.setScale(this.m_fScale);
    },
    
    //允许个人信息弹窗/点击头像触摸事件
    registerInfoPop : function( bEnable, fun ) {
        this.m_bEnable = bEnable;
        this.m_fun = fun;
    
        if (bEnable) {
            //触摸事件
            this.registerTouch();
        } else {
            this.onExit();
        }
    },
    
    enableHeadFrame : function( bEnable, frameparam ) {
        if (null == this.m_useritem) {
            return;
        }
        this.m_bFrameEnable = bEnable;
        var bAntiCheat = GlobalUserItem.isAntiCheatValid(this.m_useritem.dwUserID);
    
        if (false == bEnable || bAntiCheat) {
            if (null != this.m_spFrame) {
                this.m_spFrame.removeFromParent();
                this.m_spFrame = null;
            }
            return;
        }
        var vipIdx = this.m_useritem.cbMemberOrder || 0;
    
        //根据会员等级配置
        var vipIdx = this.m_useritem.cbMemberOrder || 0;
        var framestr = sprintf("sp_frame_%d_0.png", vipIdx);
        var deScale = 1.1;
    
        frameparam = frameparam || {};
        this.m_tabFrameParam = frameparam;
        var framefile = frameparam._framefile || framestr;
        var scaleRate = frameparam._scaleRate || deScale;
        var zorder = frameparam._zorder || 1;
        var headSprite = this.updateHeadFrame(framefile, scaleRate);
        headSprite.setLocalZOrder(zorder);
    },
    
    //更新头像框
    updateHeadFrame : function(framefile, scaleRate){
        var spFrame = null;
        var frame = cc.spriteFrameCache.getSpriteFrame(framefile);
        if (null == frame) {
            spFrame = new cc.Sprite(framefile);
            frame = (spFrame != null) && spFrame.getSpriteFrame() || null;
        }
        if (null == frame) {
            return null;
        }
    
        if (null == this.m_spFrame) {
            var thisSize = this.getContentSize();
            this.m_spFrame = new cc.Sprite(frame);
            var positionRate = this.m_tabFrameParam._posPer || cc.p(0.5, 0.5);
            //var positionRate = this.m_tabFrameParam._posPer || cc.p(0.5, 0.64);
            this.m_spFrame.setPosition(thisSize.width * positionRate.x, thisSize.height * positionRate.y);
            this.addChild(this.m_spFrame);
        } else {
            this.m_spFrame.setSpriteFrame(frame);
        }
        this.m_spFrame.setScale(scaleRate);
        // noinspection JSAnnotator
        return this.m_spFrame;
    },
    
    initHeadSprite : function( useritem ) {
        this.m_useritem = useritem;
        var isThirdParty = useritem.bThirdPartyLogin || false;
    
        //系统头像
        var faceid = useritem.wFaceID || 0;
        cc.log("faceid"+faceid);
        //判断是否进入防作弊房间
        var bAntiCheat = GlobalUserItem.isAntiCheatValid(useritem.dwUserID);
        if (bAntiCheat) {
            //直接使用系统头像
            faceid = 0;
        }
        else if (isThirdParty && null != useritem.szThirdPartyUrl && (useritem.szThirdPartyUrl).length > 0) {
    
            cc.log("<DrawThirdFace> [initHeadSprite] ////////////////////-");
            cc.log("<DrawThirdFace> [initHeadSprite] ThirdPartyUrl." + useritem.szThirdPartyUrl);
    
            var name = useritem.szThirdPartyUrl.replace("[/..+-]", "");
            var filename = name + ".png"; // 目标文件
            var filename_T = name + "_T.png"; // 临时文件
    
            //判断是否有缓存或者本地文件
            var framename = filename;
            var path = jsb.fileUtils.getWritablePath() + "face/" + useritem.dwUserID;
            var filepath = path + "/" + filename;
            var bHave, spRender = this.haveCacheOrLocalFile(framename, filepath, false);
            if (bHave) {
                cc.log("<DrawThirdFace> [initHeadSprite] [bHave]");
                return spRender;
            } else {
    
                cc.log("<DrawThirdFace> [initHeadSprite] [No bHave]");
                //判断是否有旧头像
                var infofile = path + "/face.ry";
                if (jsb.fileUtils.isFileExist(infofile)) {
                    var oldfile = jsb.fileUtils.getStringFromFile(infofile);
                    try{
                        var datatable = JSON.parse(oldfile);
                        for (k in datatable) {
                            var v = datatable[k];
                            if (v != filename) {
                                cc.FileUtils.removeFile(path + "/" + v);
                            }
                        }

                    }catch(e){}
                }

                //网络请求
                var url = useritem.szThirdPartyUrl;


                cc.log("<DrawThirdFace> [initHeadSprite] [downloadFace] url." + url);
                cc.log("<DrawThirdFace> [initHeadSprite] [downloadFace] path." + path);
                cc.log("<DrawThirdFace> [initHeadSprite] [downloadFace] filenameT." + filename_T);
                cc.log("<DrawThirdFace> [initHeadSprite] [downloadFace] filename." + filename);


                this.downloadFace(url, path, filename_T, function (downloadfile) {

                    cc.log("<DrawThirdFace> [initHeadSprite] [downloadFace end] downloadfile." + downloadfile);

                    var thisfile = filename;
                    var thisfile_T = filename_T;
                    if (thisfile_T == downloadfile) {
                        //保存头像信息
                        var infotable = {};

                        //导入新的图片信息
                        //TODO: table.insert(infotable, downloadfile);
                        infotable.push(downloadfile);
                        var jsonStr = JSON.parse(infotable);
                        cc.FileUtils.writeStringToFile(jsonStr, infofile);

                        var filepath_old = path + "/" + thisfile_T;
                        var filepath_new = path + "/" + thisfile;

                        cc.log("<DrawThirdFace> [initHeadSprite] [downloadFace end] reSizeGivenFile." + filepath_old);

                        //处理图片大小
                        reSizeGivenFile(filepath_old, filepath_new, "g_FaceResizeListener", SYS_HEADSIZE);
                        var eventReSizeListener = function (event) {
                            cc.log("resize");
                            if (null == event.oldpath || null == event.newpath) {
                                return;
                            }

                            //是否是自己文件
                            if (event.newpath == filepath_new) {
                                var sp = cc.Sprite.create(event.newpath);
                                if (null == sp) {
                                    return;
                                }

                                //清理旧资源
                                var oldframe = cc.spriteFrameCache.getSpriteFrame(framename);
                                if (null != oldframe) {
                                    oldframe.release();
                                }

                                var customframe = new cc.Sprite(event.newpath).getSpriteFrame();
                                //缓存帧
                                cc.spriteFrameCache.addSpriteFrame(customframe, framename);
                                customframe.retain();
                                this.updateHeadByFrame(customframe);

                                //发送上传头像
                                var url = yl.HTTP_URL + "/WS/Account.ashx?action=uploadface";
                                var uploader = CurlAsset.createUploader(url, filepath_new);
                                if (null == uploader) {
                                    return;
                                }
                                var nres = uploader.addToFileForm("file", filepath_new, "image/png");
                                //用户标示
                                nres = uploader.addToForm("userID", useritem.dwUserID || "thrid");
                                //登陆时间差
                                var delta = tonumber(currentTime()) - tonumber(GlobalUserItem.LogonTime);
                                cc.log("time delta " + delta);
                                nres = uploader.addToForm("time", delta + "");
                                //客户端ip
                                var ip = MultiPlatform.getInstance().getClientIpAdress() || "192.168.1.1";
                                nres = uploader.addToForm("clientIP", ip);
                                //机器码
                                var machine = useritem.szMachine || "A501164B366ECFC9E249163873094D50";
                                nres = uploader.addToForm("machineID", machine);
                                //会话签名
                                nres = uploader.addToForm("signature", GlobalUserItem.getSignature(delta));
                                if (0 != nres) {
                                    return;
                                }
                                uploader.uploadFile(function (sender, ncode, msg) {
                                    //showToast(this.getParent(), "上传完成", 2)
                                    //cc.log(msg)
                                });
                            }
                        };
                        this.m_resizeListener = new cc.EventListenerCustom(FACERESIZE_LISTENER, eventReSizeListener);
                        this.getEventDispatcher().addEventListenerWithFixedPriority(this.m_resizeListener, 1);
                    }
                });
            }
        }
        else if (null != useritem.dwCustomID && 0 != useritem.dwCustomID) {
            //判断是否有缓存或者本地文件
            var framename = useritem.dwUserID + "_custom_" + useritem.dwCustomID + ".ry";
            var filepath = jsb.fileUtils.getWritablePath() + "face/" + useritem.dwUserID + "/" + framename;

            var bHave, spRender = this.haveCacheOrLocalFile(framename, filepath, true);
            if (bHave) {
                return spRender;
            } else {
                var path = jsb.fileUtils.getWritablePath() + "face/" + useritem.dwUserID;
                var filename = framename;
                //判断是否有旧头像
                var infofile = path + "/face.ry";
                if (jsb.fileUtils.isFileExist(infofile)) {
                    var oldfile = jsb.fileUtils.getStringFromFile(infofile);
                    var ok, datatable = pcall(function () {
                        return JSON.parse(oldfile);
                    });
                    if (ok && typeof datatable == "table") {
                        for (k in datatable) {
                            var v = datatable[k];
                            if (v != filename) {
                                var oldframe = cc.spriteFrameCache.getSpriteFrame(v);
                                if (null != oldframe) {
                                    oldframe.release();
                                }
                                cc.FileUtils.removeFile(path + "/" + v);
                            }
                        }
                    }
                }

                //网络请求
                var url = yl.HTTP_URL + "/WS/UserFace.ashx?customid=" + useritem.dwCustomID;
                this.downloadFace(url, path, filename, function (downloadfile) {
                    //判断是否是自己头像
                    var thisfile = filename;
                    if (thisfile == downloadfile) {
                        //保存头像信息
                        var infotable = {};
                        //导入新的图片信息
                        table.insert(infotable, downloadfile);
                        var jsonStr = JSON.parse(infotable);
                        cc.FileUtils.writeStringToFile(jsonStr, infofile);

                        var filepath = path + "/" + thisfile;
                        var customframe = createSpriteFrameWithBMPFile(filepath);
                        if (null != customframe) {
                            var oldframe = cc.spriteFrameCache.getSpriteFrame(framename);
                            if (null != oldframe) {
                                oldframe.release();
                            }

                            //缓存帧
                            cc.spriteFrameCache.addSpriteFrame(customframe, framename);
                            customframe.retain();
                            this.updateHeadByFrame(customframe);
                        }
                    }
                });
            }
        }

        var str = sprintf("Avatar%d.png", faceid);
        cc.log("Avatar : "+str);
        var frame = cc.spriteFrameCache.getSpriteFrame(str);
        if (null != frame) {
            this.m_spRender = new cc.Sprite(frame);
            // this.setContentSize(this.m_spRender.getContentSize());
            this.setContentSize(cc.size(96,96));
        }
        this.m_fScale = this.m_headSize / SYS_HEADSIZE;
        this.setScale(this.m_fScale);

        return this.m_spRender;
    },

    haveCacheOrLocalFile : function(framename, filepath, bmpfile) {
        //判断是否有缓存
        var frame = cc.spriteFrameCache.getSpriteFrame(framename);
        if (null != frame) {
            this.updateHeadByFrame(frame);
            return [true, this.m_spRender];
        } else {
            //判断是否有本地文件
            var path = filepath;
            if (cc.FileUtils.isFileExist(path)) {
                var customframe = null;
                if (bmpfile) {
                    customframe = createSpriteFrameWithBMPFile(path);
                } else {
                    var sp = cc.Sprite.create(path);
                    if (null != sp) {
                        customframe = sp.getSpriteFrame();
                    }
                }
                if (null != customframe) {
                    //缓存帧
                    var framename = this.m_useritem.dwUserID + "_custom_" + this.m_useritem.dwCustomID + ".ry";
                    cc.spriteFrameCache.addSpriteFrame(customframe, framename);
                    customframe.retain();
                    this.updateHeadByFrame(customframe);
                    return [true, this.m_spRender];
                }
            }
        }
        return false;
    },

    //下载头像
    downloadFace : function(url, path, filename, onDownLoadSuccess) {
        var downloader = CurlAsset.createDownloader("g_FaceDownloadListener", url);
        if (false == cc.FileUtils.isDirectoryExist(path)) {
            cc.FileUtils.createDirectory(path);
        }

        var eventCustomListener = function (event) {

            cc.log("<DrawThirdFace> [downloadFace end] eventCustomListener." + event.filename + " msg." + event.msg + " code." + event.code);

            if (null != event.filename && 0 == event.code) {
                if ((null != onDownLoadSuccess) && typeof onDownLoadSuccess == "function" && null != event.filename && typeof event.filename == "string") {

                    cc.log("<DrawThirdFace> [downloadFace end] onDownLoadSuccess." + event.filename);

                    onDownLoadSuccess(event.filename);
                }
            }
        }
        this.m_downListener = new cc.EventListenerCustom(FACEDOWNLOAD_LISTENER, eventCustomListener);
        this.getEventDispatcher().addEventListenerWithFixedPriority(this.m_downListener, 1);
        downloader.downloadFile(path, filename);
    },

    registerTouch : function( ) {
        var self = this;
        var customListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: false,
            onTouchBegan: function (touch, event) {
                return (self.isVisible() && self.isAncestorVisible(self) && self.m_bEnable);
            },
            onTouchEnded : function (touch, event) {
                var pos = touch.getLocation();
                pos = self.convertToNodeSpace(pos);
                var rec = cc.rect(0, 0, self.getContentSize().width, self.getContentSize().height);
                if (true == cc.rectContainsPoint(rec, pos)) {
                    if (null != self.m_fun) {
                        self.m_fun();
                    }
                }
            }
        });
        cc.eventManager.addListener(customListener, this);

        // var listener = cc.EventListenerTouchOneByOne.create();
        // this.listener = listener;
        // listener.registerScriptHandler(onTouchBegan, cc.Handler.EVENT_TOUCH_BEGAN);
        // listener.registerScriptHandler(onTouchEnded, cc.Handler.EVENT_TOUCH_ENDED);
        // var eventDispatcher = this.getEventDispatcher();
        // eventDispatcher.addEventListenerWithSceneGraphPriority(listener, this);
    },

    isAncestorVisible : function( child ) {
        if (null == child) {
            return true;
        }
        var parent = child.getParent();
        if (null != parent && false == parent.isVisible()) {
            return false;
        }
        return this.isAncestorVisible(parent);
    },

    onExit : function( ) {
        if (null != this.listener) {
            //todo event listener
            // var eventDispatcher = this.getEventDispatcher();
            // eventDispatcher.removeEventListener(this.listener);
            // this.listener = null;
        }

        if (null != this.m_downListener) {
            this.getEventDispatcher().removeEventListener(this.m_downListener);
            this.m_downListener = null;
        }

        if (null != this.m_resizeListener) {
            this.getEventDispatcher().removeEventListener(this.m_resizeListener);
            this.m_resizeListener = null;
        }
    },

    onEnterTransitionFinish : function() {
        if (this.m_bEnable && null == this.listener) {
            this.registerTouch();
        }
    },

    //缓存头像
    loadAllHeadFrame : function(  ) {
       //TODO: check when use ( jsb function)
       // if (false == cc.spriteFrameCache.isSpriteFramesWithFileLoaded("public/im_head.plist")) {
            //缓存头像
            cc.spriteFrameCache.addSpriteFrames(res.im_head_plist);
            //
            //手动retain所有的头像帧缓存、防止被释放
            // var dict = cc.FileUtils.getValueMapFromFile("public/im_head.plist");
            // var framesDict = dict["frames"];
            // if (null != framesDict && type(framesDict) == "table") {
            //     for (k in framesDict) {
            //         var v = framesDict[k];
            //         var frame = cc.spriteFrameCache.getInstance().getSpriteFrame(k);
            //         if (null != frame) {
            //             frame.retain();
            //         }
            //     }
            // }

            //缓存头像框
            cc.spriteFrameCache.addSpriteFrames(res.im_head_frame_plist);
            // dict = cc.FileUtils.getInstance().getValueMapFromFile("public/im_head_frame.plist");
            // framesDict = dict["frames"];
            // if (null != framesDict && type(framesDict) == "table") {
            //     for (k in framesDict) {
            //         var v = framesDict[k];
            //         var frame = cc.spriteFrameCache.getSpriteFrame(k);
            //         if (null != frame) {
            //             frame.retain();
            //         }
            //     }
            // }
        // }
    },

    unloadAllHead : function(  ) {
        var dict = jsb.fileUtils.getValueMapFromFile("public/im_head_frame.plist");
        var framesDict = dict["frames"];
        if (null != framesDict && typeof framesDict == "table") {
            for (k in framesDict) {
                var v = framesDict[k];
                var frame = cc.spriteFrameCache.getSpriteFrame(k);
                if (null != frame) {
                    frame.release();
                }
            }
        }
    
        cc.director().getTextureCache().removeUnusedTextures();
        cc.spriteFrameCache.removeUnusedSpriteFrames();
    },
    
    //获取系统头像数量
    getSysHeadCount : function(  ) {
        return 200;
    }

});

//自定义头像存储规则
// path/face/userid/custom_+customid.ry
//头像缓存规则
// uid_custom_cusomid

var FACEDOWNLOAD_LISTENER = "face_notify_down";
var FACERESIZE_LISTENER = "face_resize_notify";
//全局通知函数
cc_exports_g_FaceDownloadListener = function (ncode, msg, filename) {
    var event = new cc.EventCustom(FACEDOWNLOAD_LISTENER);
    event.code = ncode;
    event.msg = msg;
    event.filename = filename;

    cc.director().getEventDispatcher().dispatchEvent(event);
};

//
cc_exports_g_FaceResizeListener = function(oldpath, newpath) {
    var event = new cc.EventCustom(FACERESIZE_LISTENER);
    event.oldpath = oldpath;
    event.newpath = newpath;

    cc.director().getEventDispatcher().dispatchEvent(event);
};

var SYS_HEADSIZE = 88;