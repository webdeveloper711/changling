/*
  Author  :   JIN
*/


// var ExternalFun = require(appdf.EXTERNAL_SRC + "ExternalFun");
var g_var = ExternalFun.req_var;
var targetPlatform = cc.sys.platform;

//平台
var PLATFORM = [];
PLATFORM[cc.sys.ANDROID] = appdf.EXTERNAL_SRC + "platform.Bridge_android";
PLATFORM[cc.sys.IPHONE] = appdf.EXTERNAL_SRC + "platform.Bridge_ios";
PLATFORM[cc.sys.IPAD] = appdf.EXTERNAL_SRC + "platform.Bridge_ios";
PLATFORM[cc.sys.MACOS] = appdf.EXTERNAL_SRC + "platform.Bridge_ios";

function MultiPlatform() {
    this.sDefaultTitle = "";
    this.sDefaultContent = "";
    this.sDefaultUrl = "";
}

//实现单例
MultiPlatform._instance = null;
MultiPlatform.getInstance = function() {
    if (null == MultiPlatform._instance) {
        cc.log("new instance");
        MultiPlatform._instance = new MultiPlatform();
    }
    return MultiPlatform._instance;
};

MultiPlatform.prototype.getSupportPlatform = function()
{
    var plat = targetPlatform;
    //ios特殊处理
    if ((cc.sys.IPHONE == targetPlatform) || (cc.sys.IPAD == targetPlatform) || (cc.sys.MACOS == targetPlatform)) {
        plat = cc.sys.IPHONE;
    }

    return plat;
};

//获取设备id
MultiPlatform.prototype.getMachineId = function() {
    var plat = this.getSupportPlatform();

    // if (null != g_var(PLATFORM[plat]) && null != g_var(PLATFORM[plat]).getMachineId) {
    //     // return g_var(PLATFORM[plat]).getMachineId(); //TODO : closed by lsh
    //     return "A501164B366ECFC9E249163873094D51";
    // } else {
        cc.log("unknow platform ==> " + plat);
        return "A501164B366ECFC9E249163873094D51";
    // }
};

//获取设备ip
MultiPlatform.prototype.getClientIpAdress = function() {
    var plat = this.getSupportPlatform();

    // if (null != g_var(PLATFORM[plat]) && null != g_var(PLATFORM[plat]).getMachineId) {
    //     //return g_var(PLATFORM[plat]).getClientIpAdress(); //TODO : closed by lsh
    //     return "192.168.1.2";
    // } else {
    //     cc.log("unknow platform ==> " + plat);
        return "192.168.1.1";
    // }
};

//获取外部存储可写文档目录
MultiPlatform.prototype.getExtralDocPath = function() {
    var plat = this.getSupportPlatform();
    //TODO: must define device in init.js
    var path = jsb.fileUtils.getWritablePath();
    // if (null != g_var(PLATFORM[plat]) && null != g_var(PLATFORM[plat]).getExtralDocPath) {
    //     path = g_var(PLATFORM[plat]).getExtralDocPath();
    // } else {
    //     cc.log("undefined funtion || unknow platform ==> " + plat);
    // }
    //
    // if (false == cc.FileUtils.getInstance().isDirectoryExist(path)) {
    //     cc.FileUtils.createDirectory(path);
    // }
    return path;
};

// 选择图片
// callback 回调函数
// needClip 是否需要裁减图片
MultiPlatform.prototype.triggerPickImg = function( callback, needClip ) {
    var plat = this.getSupportPlatform();

    // if (null != g_var(PLATFORM[plat]) && null != g_var(PLATFORM[plat]).triggerPickImg) {
    //     g_var(PLATFORM[plat]).triggerPickImg(callback, needClip);
    // } else {
    //     cc.log("unknow platform ==> " + plat);
    // }
};

//配置第三方平台
MultiPlatform.prototype.thirdPartyConfig = function(thirdparty, configTab) {
    configTab = configTab || {};

    var plat = this.getSupportPlatform();
    ExternalFun.dump(PLATFORM[plat], "PLATFORM[plat]", 2);
    // if (null != g_var(PLATFORM[plat]) && null != g_var(PLATFORM[plat]).thirdPartyConfig) {
    //     //g_var(PLATFORM[plat]).thirdPartyConfig(thirdparty, configTab); //TODO : closed by lsh
    //     cc.log("platform ==> getSupportPlatform : " + plat);
    // } else {
    //     cc.log("unknow platform ==> " + plat);
    // }
};

//分享相关
MultiPlatform.prototype.configSocial = function(socialTab) {
    socialTab = socialTab || {};
    socialTab.title = socialTab.title || "";
    socialTab.content = socialTab.content || "";
    socialTab.url = socialTab.url || "";

    this.sDefaultTitle = socialTab.title;
    this.sDefaultContent = socialTab.content;
    this.sDefaultUrl = socialTab.url;

    var plat = this.getSupportPlatform();
    // if (null != g_var(PLATFORM[plat]) && null != g_var(PLATFORM[plat]).configSocial) {
    //     //g_var(PLATFORM[plat]).configSocial(socialTab); //TODO: closed by lsh
    //     cc.log("platform ==> configSocial:" + plat);
    // } else {
    //     cc.log("unknow platform ==> " + plat);
    // }
};

//第三方登陆
MultiPlatform.prototype.thirdPartyLogin = function(thirdparty, callback) {
    if (null == callback || typeof(callback) != "function") {
        return false, "need callback function";
    }

    // var plat = this.getSupportPlatform();
    // if (null != g_var(PLATFORM[plat]) && null != g_var(PLATFORM[plat]).thirdPartyLogin) {
    //     return g_var(PLATFORM[plat]).thirdPartyLogin(thirdparty, callback);
    // } else {
        var msg = "unknow platform ==> " + plat;
        cc.log(msg);
        return false, msg;
    // }
};

//分享
MultiPlatform.prototype.startShare = function(callback) {
    if (null == callback || typeof(callback) != "function") {
        return false, "need callback function";
    }
    // 判断微信配置
    if (yl.WeChat.AppID == "" || yl.WeChat.AppID == " ") {
        var runScene = cc.Director.getInstance().getRunningScene();
        if (null != runScene) {
            showToast(runScene, "分享失败, 错误代码." + yl.ShareErrorCode.NOT_CONFIG, 2, cc.c4b(250, 0, 0, 255));
        }
        return false, "not config wechat";
    }

    // var plat = this.getSupportPlatform();
    // if (null != g_var(PLATFORM[plat]) && null != g_var(PLATFORM[plat]).startShare) {
    //     return g_var(PLATFORM[plat]).startShare(callback);
    // } else {
        var msg = "unknow platform ==> " + plat;
        cc.log(msg);
        return false, msg;
    // }
};

//自定义分享
// imgOnly 值为字符串 "true" 表示只分享图片
MultiPlatform.prototype.customShare = function( callback, title, content, url, img, imgOnly ) {
    if (null == callback || typeof(callback) != "function") {
        return false, "need callback function";
    }
    // 判断微信配置
    if (yl.WeChat.AppID == "" || yl.WeChat.AppID == " ") {
        var runScene = cc.director().getRunningScene();
        if (null != runScene) {
            showToast(runScene, "分享失败, 错误代码." + yl.ShareErrorCode.NOT_CONFIG, 2, cc.c4b(250, 0, 0, 255));
        }
        return false, "not config wechat";
    }

    title = title || this.sDefaultTitle;
    content = content || this.sDefaultContent;
    img = img || "";
    url = url || this.sDefaultUrl;
    imgOnly = imgOnly || "false";

    // var plat = this.getSupportPlatform();
    // if (null != g_var(PLATFORM[plat]) && null != g_var(PLATFORM[plat]).customShare) {
    //     return g_var(PLATFORM[plat]).customShare(title, content, url, img, imgOnly, callback);
    // } else {
        var msg = "unknow platform ==> " + plat;
        cc.log(msg);
        return false, msg;
    // }
};

// 分享到指定平台
MultiPlatform.prototype.shareToTarget = function( target, callback, title, content, url, img, imgOnly ) {
    if (null == callback || typeof(callback) != "function") {
        return false, "need callback function";
    }
    // 判断微信配置
    if (yl.WeChat.AppID == "" || yl.WeChat.AppID == " ") {
        var runScene = cc.Director.getInstance().getRunningScene();
        if (null != runScene) {
            showToast(runScene, "分享失败, 错误代码." + yl.ShareErrorCode.NOT_CONFIG, 2, cc.c4b(250, 0, 0, 255));
        }
        return false, "not config wechat";
    }

    title = title || this.sDefaultTitle;
    content = content || this.sDefaultContent;
    img = img || "";
    url = url || this.sDefaultUrl;
    imgOnly = imgOnly || "false";

    // var plat = this.getSupportPlatform();
    // if (null != g_var(PLATFORM[plat]) && null != g_var(PLATFORM[plat]).shareToTarget) {
    //     return g_var(PLATFORM[plat]).shareToTarget(target, title, content, url, img, imgOnly, callback);
    // } else {
        var msg = "unknow platform ==> " + plat;
        cc.log(msg);
        return false, msg;
    // }
};

MultiPlatform.prototype.thirdPartyPay = function(thirdparty, payparamTab, callback) {
    if (null == callback || typeof(callback) != "function") {
        return false, "need callback function";
    }
    ;
    payparamTab = payparamTab || {};

    // var plat = this.getSupportPlatform();
    // if (null != g_var(PLATFORM[plat]) && null != g_var(PLATFORM[plat]).thirdPartyPay) {
    //     return g_var(PLATFORM[plat]).thirdPartyPay(thirdparty, payparamTab, callback);
    // } else {
        var msg = "unknow platform ==> " + plat;
        cc.log(msg);
        return false, msg;
    // }
};

//竣付通获取支付列表
MultiPlatform.prototype.getPayList = function(token, callback) {
    if (null == callback || typeof(callback) != "function") {
        return false, "need callback function";
    }

    // var plat = this.getSupportPlatform();
    // if (null != g_var(PLATFORM[plat]) && null != g_var(PLATFORM[plat]).getPayList) {
    //     return g_var(PLATFORM[plat]).getPayList(token, callback);
    // } else {
        var msg = "unknow platform ==> " + plat;
        cc.log(msg);
        callback("");
        return false, msg;
    // }
};

//第三方平台是否安装
MultiPlatform.prototype.isPlatformInstalled = function(thirdparty) {
    var plat = this.getSupportPlatform();
    // if (null != g_var(PLATFORM[plat]) && null != g_var(PLATFORM[plat]).isPlatformInstalled) {
    //     return g_var(PLATFORM[plat]).isPlatformInstalled(thirdparty);
    // } else {
        var msg = "unknow platform ==> " + plat;
        cc.log(msg);
        return false, msg;
    // }
};

//图片存储至系统相册
MultiPlatform.prototype.saveImgToSystemGallery = function(filepath, filename) {
    if (false == cc.FileUtils.getInstance().isFileExist(filepath)) {
        var msg = filepath + " not exist";
        cc.log(msg);
        return false, msg;
    }
    // var plat = this.getSupportPlatform();
    // if (null != g_var(PLATFORM[plat]) && null != g_var(PLATFORM[plat]).saveImgToSystemGallery) {
    //     return g_var(PLATFORM[plat]).saveImgToSystemGallery(filepath, filename);
    // } else {
        var msg = "unknow platform ==> " + plat;
        cc.log(msg);
        return false, msg;
    // }
};

// 录音权限判断
MultiPlatform.prototype.checkRecordPermission = function(){
    // var plat = this.getSupportPlatform();
    // if ( null != g_var(PLATFORM[plat]) && null != g_var(PLATFORM[plat]).checkRecordPermission ) {
		// return g_var(PLATFORM[plat]).checkRecordPermission( );
    // } else {
        var msg = "unknow platform ==> " + plat;
        cc.log(msg);
        return false, msg;
    // }
};

// 请求单次定位
MultiPlatform.prototype.requestLocation = function(callback) {
    callback = callback || -1;

    var plat = this.getSupportPlatform();
    // if (null != g_var(PLATFORM[plat]) && null != g_var(PLATFORM[plat]).requestLocation) {
    //     return g_var(PLATFORM[plat]).requestLocation(callback);
    // } else {
        var msg = "unknow platform ==> " + plat;
        cc.log(msg);
        if (typeof(callback) == "function") {
            callback("");
        }
        return false, msg;
    // }
};

// 计算距离
MultiPlatform.prototype.metersBetweenLocation = function( loParam ) {
    // var plat = this.getSupportPlatform();
    // if (null != g_var(PLATFORM[plat]) && null != g_var(PLATFORM[plat]).metersBetweenLocation) {
    //     return g_var(PLATFORM[plat]).metersBetweenLocation(loParam);
    // } else {
        var msg = "unknow platform ==> " + plat;
        cc.log(msg);
        return false, msg;
    // }
};

// 请求通讯录
MultiPlatform.prototype.requestContact = function(callback) {
    callback = callback || -1;

    // var plat = this.getSupportPlatform();
    // if (null != g_var(PLATFORM[plat]) && null != g_var(PLATFORM[plat]).requestContact) {
    //     return g_var(PLATFORM[plat]).requestContact(callback);
    // } else {
        var msg = "unknow platform ==> " + plat;
        cc.log(msg);
        return false, msg;
    // }
}

// 启动浏览器
MultiPlatform.prototype.openBrowser = function( url ) {
    url = url || yl.HTTP_URL;
    // var plat = this.getSupportPlatform();
    // if (null != g_var(PLATFORM[plat]) && null != g_var(PLATFORM[plat]).openBrowser) {
    //     return g_var(PLATFORM[plat]).openBrowser(url);
    // } else {
        var msg = "unknow platform ==> " + plat;
        cc.log(msg);
        return false, msg;
    // }
}


// 得到web端唤醒app请求的参数RoomID
MultiPlatform.prototype.GetWebRequestRoomID = function( ) {
    // var plat = this.getSupportPlatform();
    // if (null != g_var(PLATFORM[plat]) && null != g_var(PLATFORM[plat]).GetWebRequestRoomID) {
    //     return g_var(PLATFORM[plat]).GetWebRequestRoomID();
    // } else {
        var msg = "unknow platform ==> " + plat;
        cc.log(msg);
        return false, msg;
    // }
};

// 复制到剪贴板
MultiPlatform.prototype.copyToClipboard = function( msg ) {
    if (typeof(msg) != "string") {
        cc.log("复制内容非法");
        return 0, "复制内容非法";
    }
    // var plat = this.getSupportPlatform();
    // if (null != g_var(PLATFORM[plat]) && null != g_var(PLATFORM[plat]).copyToClipboard) {
    //     return g_var(PLATFORM[plat]).copyToClipboard(msg);
    // } else {
        var msg = "unknow platform ==> " + plat;
        cc.log(msg);
        return 0, msg;
    // }
};

