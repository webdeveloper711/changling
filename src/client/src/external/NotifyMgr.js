/*  Author  :   JIN */

//网络通知管理
function NotifyMgr () {
    this.ctor();
};
//实现单例
NotifyMgr._instance = null;

var MAX_CACHE_NOTIFY = 20;
NotifyMgr.getInstance = function() {
    if (null == this._instance) {
        this._instance = new NotifyMgr();
    }
    return this._instance;
};

NotifyMgr.prototype.ctor = function() {
    //通知管理
    this.m_tabNofityMgr = [];
    //通知列表
    this.m_tabListNotify = [];
    //未读通知列表
    this.m_tabUnreadNotify = [];
    //暂停通知列表
    this.m_tabSleepNotify = [];
    //通知id索引
    this.m_nNotifyId = 0;
};

//清理
NotifyMgr.prototype.clear = function() {
    //通知列表
    this.m_tabListNotify = [];
    //未读通知列表
    this.m_tabUnreadNotify = [];
    //暂停通知列表
    this.m_tabSleepNotify = [];
    //通知id索引
    this.m_nNotifyId = 0;
};

//
NotifyMgr.prototype.createNotify = function(main, sub) {
    var s = {
        "main" : main,
        "sub" : sub,
        "name" : "",
        "fun" : null,
        "pause" : false,
        "group" : ""
    };
    return s;
};

//红点name
var DOT_NODE_NAME = "___ry_notify_dot_name___";
// var DOT_NODE_NAME = "notify_dot_cell";
//通知红点
NotifyMgr.prototype.showNotify = function(node, msg, pos) {
    if (null == node) {
        return;
    }

    var dot = node.getChildByName(DOT_NODE_NAME);
    if (null == dot) {
        var frame = cc.spriteFrameCache.getSpriteFrame("sp_dot.png");
        if (null != frame) {
            dot = new cc.Sprite(frame);
            dot.setName(DOT_NODE_NAME);
            node.addChild(dot);

            var nodesize = node.getContentSize();
            var dotsize = dot.getContentSize();
            // pos = cc.p(nodesize.width - dotsize.width, nodesize.height - dotsize.height + 10);
            //pos = cc.p(nodesize.width, nodesize.height);
            dot.setPosition(pos);
        }
    }

    if (null != dot) {
        //前一个不相同通知
        if (null != dot.msg && dot.msg._id != msg._id) {
            this.markRead(dot.msg);
        }
        dot.msg = msg;
        dot.setVisible(true);
    }
};
//通知红点
NotifyMgr.prototype.hideNotify = function(node,markread) {
    if (null == node) {
        return;
    }
    markread = markread || false;
    var dot = node.getChildByName(DOT_NODE_NAME);
    if (null != dot) {
        dot.setVisible(false);
        if (true == markread && null != dot.msg) {
            this.markRead(dot.msg);
        }
    }
};

NotifyMgr.prototype.registerNotify = function(notify) {
    notify.main = notify.main || 0;
    notify.sub = notify.sub || 0;
    notify.name = notify.name || "";
    notify.group = notify.group || "";
    notify.fun = notify.fun || null;
    var pause = notify.pause || false;
    notify.active = !pause;
    notify.autoread = notify.autoread || false;

    var mNotify = this.getManagedNotify(notify.main, notify.sub, notify.name);
    if (null == mNotify) {
        cc.log("register main ==> " + notify.main + " sub ==> " + notify.sub + " name ==> " + notify.name);
        //ExternalFun.dump(this.m_tabNofityMgr, "NotifyMgr.m_tabNofityMgr", 6);
        var len = (this.m_tabNofityMgr).length;
        this.m_tabNofityMgr[len] = notify;
    } else {
        cc.log("update main ==> " + notify.main + " sub ==> " + notify.sub + " name ==> " + notify.name);
        mNotify = notify;
    }
};

//移除通知
NotifyMgr.prototype.unregisterNotify = function(main, sub, name) {
    var idx = 0;
    this.m_tabNofityMgr.forEach(function (v, k) {
        if (v.main == main && v.sub == sub && v.name == name) {
            idx = k;
            cc.log("unregister main ==> " + main + " sub ==> " + sub + " name ==> " + name);
        }
    });

    this.m_tabNofityMgr.forEach(function (v, k) {
        if (v.main == main && v.sub == sub && v.name == name) {
            idx = k;
            cc.log("unregister main ==> " + main + " sub ==> " + sub + " name ==> " + name);
        }
    });

    if (null != idx) {
        this.m_tabNofityMgr = this.m_tabNofityMgr.splice(idx,1);
    }
};

//标记暂停
NotifyMgr.prototype.pauseNotify = function(main, sub, name)
{
    var notify = this.getManagedNotify(main, sub, name);
    if (null != notify) {
        notify.active = false;
    }
};

//标记激活
NotifyMgr.prototype.resumeNotify = function(main, sub, name) {
    var notify = this.getManagedNotify(main, sub, name);
    if (null != notify) {
        notify.active = true;
    }
};

//标记通知已读
NotifyMgr.prototype.markRead = function(msg) {
    cc.log("mark read ==> " + msg._id);
    //异常处理
    msg._id = msg._id || -1;
    if (-1 == msg._id) {
        msg._bread = true;
    }

    this.m_tabUnreadNotify = [];
    for (var k in this.m_tabListNotify){
        var v = this.m_tabListNotify[k];
        if(v!=null){
            if(v.id == msg.id){
                this.m_tabListNotify[k] = null;
            } else {
                this.m_tabUnreadNotify.push(v);
            }
        }
    }
};

//获取未读通知列表
NotifyMgr.prototype.getUnreadNotifyList = function() {
    //dump(this.m_tabUnreadNotify, "this.m_tabUnreadNotify", 6)
    return this.m_tabUnreadNotify;
};

//更新未读通知列表
NotifyMgr.prototype.updateUnreadList = function() {
    this.m_tabUnreadNotify = [];
    for (k in (this.m_tabListNotify)) {
        var v = this.m_tabListNotify[k];
        if (null!= v && null != v._bread && false == v._bread) {
            this.m_tabUnreadNotify.push(v);
        } else {
            this.m_tabListNotify[k] = null;
            this.m_tabListNotify.splice(k,1);
        }
    }
};

//处理通知
NotifyMgr.prototype.excuteSleepNotfiy = function() {
    for( var k in this.m_tabSleepNotify){
        var v = this.m_tabSleepNotify[k];
        var notify = v.notify;
        var msg = v.msg;
        if (null != notify && typeof(notify.fun) == "function" && null != msg) {
            this.m_tabListNotify.push(msg);
            notify.fun(msg);
        }
    }
    this.m_tabSleepNotify = [];
    this.updateUnreadList();
};

//处理通知
NotifyMgr.prototype.excute = function(main, sub, param) {
    for(k in this.m_tabNofityMgr){
        var v = this.m_tabNofityMgr[k];
        if (v.main == main && v.sub == sub) {
            //dump(v, "v")
            var msg =
                {
                    '_id': this.getNotifyId(),
                    '_bread': false,
                    'main': main,
                    'sub': sub,
                    'name': v.name,
                    'param': param,
                    'group': v.group
                };
            if (true == v.active) {
                var bHandled = null;
                //活动状态
                if (typeof(v.fun) == "function") {
                    bHandled = v.fun(msg);
                }
                bHandled = bHandled || false;

                if (false == bHandled) {
                    /*table.insert(this.m_tabListNotify, msg);*/
                    this.m_tabListNotify.push(msg);
                    this.updateUnreadList();
                }
            } else {
                cc.log("cache notify. main ==> " + main + " sub ==> " + sub);
                this.m_tabSleepNotify.push({'notify': v, 'msg': msg});
                //控制容量
                if (this.m_tabSleepNotify.length > MAX_CACHE_NOTIFY) {
                    // table.remove(this.m_tabSleepNotify, 1);TODO:changed remove function into splice;
                    this.m_tabSleepNotify.splice(0, 1);
                }
            }
        }
    }
};

//判断是否在通知列表
NotifyMgr.prototype.getManagedNotify = function(main, sub, name) {
    cc.log("getmanagedNotify called!!!");
    for(var k in this.m_tabListNotify){
        var v = this.m_tabListNotify[k];
        if(v!=null){
            if(v.main == main && v.sub == sub && v.name == name) {
                return v;
            }
        } else {
            this.m_tabListNotify.splice(k,1);
        }

    }
    return null;
};

//获取通知id(标记管理)
NotifyMgr.prototype.getNotifyId = function() {
    var tmp = this.m_nNotifyId;
    this.m_nNotifyId = this.m_nNotifyId + 1;
    if (this.m_nNotifyId > yl.MAX_INT) {
        this.m_nNotifyId = 0;
    }
    return tmp;
};
