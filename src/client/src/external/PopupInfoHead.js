/*  Author : Jin */
var POP_LAYERNAME = "pop_headinfo_layer";
var POP_LAYERNAME_GAME = "pop_headinfo_layer_game";

var PopupInfoHead = cc.Node.extend({

    ctor : function( ) {
        this._super();
        //  ExternalFun.registerNodeEvent(this);
        this.m_head = null;
        this.m_bIsGamePop = true;
    },

    createNormal : function( useritem ,headSize){
        var sf = new PopupInfoHead();
        var head = (new HeadSprite()).createNormal(useritem, headSize);
        sf.m_useritem = useritem;
        sf.m_head = head;
        if (null != head) {
            sf.addChild(head);
        }
        return [sf, head];
    },

    createClipHead : function(useritem, headSize, clippingfile) {
        var sf = new PopupInfoHead();
        var head = (new HeadSprite()).createClipHead(useritem, headSize, clippingfile);
        sf.m_useritem = useritem;
        sf.m_head = head;
        if (null != head) {
            sf.addChild(head);
        }
        return [sf, head];
    },

    //设置是否是游戏界面弹窗
    setIsGamePop : function( v ) {
        this.m_bIsGamePop = v;
    },

    //更新头像
    updateHead : function(useritem) {
        this.m_useritem = useritem;
        if (null != this.m_head) {
            this.m_head.updateHead(useritem);
        }
    },

    enableInfoPop : function( bEnable, popPos, anr) {
        var self = this;
        var funCall = function () {
            self.onTouchHead();
        };
        this.m_popPos = popPos || this.getInfoLayerPos();
        this.m_popAnchor = anr || this.getInfoLayerAnchor();

        if (null != this.m_head) {
            this.m_head.registerInfoPop(bEnable, funCall);
        }

        if (bEnable) {

        } else {
            var infoLayer = null;
            if (this.m_bIsGamePop) {
                infoLayer = cc.director.getRunningScene().getChildByName(POP_LAYERNAME_GAME);
            } else {
                infoLayer = cc.director.getRunningScene().getChildByName(POP_LAYERNAME);
            }
            if (null != infoLayer) {
                infoLayer.removeFromParent();
            }
        }
    },

    //头像框
    enableHeadFrame : function( bEnable, frameparam ) {
        if (null != this.m_head) {
            this.m_head.enableHeadFrame(bEnable, frameparam);
        }
    },

    onTouchHead : function(  ) {
        var infoLayer = null;
        var name = "";
        if (this.m_bIsGamePop) {
            name = POP_LAYERNAME_GAME;
            infoLayer = cc.director.getRunningScene().getChildByName(POP_LAYERNAME_GAME)
        } else {
            name = POP_LAYERNAME;
            infoLayer = cc.director.getRunningScene().getChildByName(POP_LAYERNAME);
        }
        if (null == infoLayer) {
            infoLayer = new PopupInfoLayer(this, this.m_bIsGamePop);
            var runningScene = cc.director.getRunningScene();
            if (null != runningScene) {
                runningScene.addChild(infoLayer);
                infoLayer.setName(name);
            }
        }

        if (null != infoLayer && null != this.m_useritem) {
            infoLayer.showLayer(true);
            infoLayer.refresh(this.m_useritem, this.m_popPos, this.m_popAnchor);
        }
    },

    getInfoLayerPos : function(  ) {
        var size = cc.director.getWinSize();
        return cc.p(size.width * 0.11, size.height * 0.26);
    },

    getInfoLayerAnchor : function(  ) {
        return cc.p(0, 0);
    },

    onExit : function() {
        //强行移除layer，在关闭程序的时候会异常
        var infoLayer = null;
        if (this.m_bIsGamePop) {
            var name = POP_LAYERNAME_GAME;
            infoLayer = cc.director.getRunningScene().getChildByName(POP_LAYERNAME_GAME);
        } else {
            name = POP_LAYERNAME;
            infoLayer = cc.director.getRunningScene().getChildByName(POP_LAYERNAME);
        }
        if (null != infoLayer && null != infoLayer.hide) {
            infoLayer.hide();
        }
    }
   
});

// var ExternalFun = require(appdf.EXTERNAL_SRC + "ExternalFun")
// var HeadSprite = require(appdf.EXTERNAL_SRC + "HeadSprite")
// var PopupInfoLayer = require(appdf.EXTERNAL_SRC + "PopupInfoLayer")


