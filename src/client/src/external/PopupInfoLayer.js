/*	Author	:	JIN */

var PopupInfoLayer = cc.Layer.extend({
    ctor : function(viewParent, isGame) {
        //注册事件
        this._super();
        // ExternalFun.registerNodeEvent(this);
        var self = this;
        this.m_parent = viewParent;

        //加载csb资源
        isGame = isGame || false;
        var csbNode = null;
        if (true == isGame) {
            if (yl.SHOW_IP_ADDRESS) {
                csbNode = ExternalFun.loadCSB(res.GamePopInfo_json, this);
            } else {
                csbNode = ExternalFun.loadCSB(res.GamePopInfoNormal_json, this);
            }
        } else {
            if (yl.SHOW_IP_ADDRESS) {
                csbNode = ExternalFun.loadCSB(res.GamePopInfo_json, this);
            } else {
                csbNode = ExternalFun.loadCSB(res.GamePopInfoNormal_json, this);
            }
        }
        this.m_bIsGame = isGame;
        this.m_userInfo = {};

        //tmp.getPosition
        var bg_kuang = csbNode.getChildByName("bg_kuang");
        this.m_spBgKuang = bg_kuang;


        //关闭按钮
        var btn = bg_kuang.getChildByName("close_btn");
        btn.addTouchEventListener(function (ref, eventType) {
            if (eventType == ccui.Widget.TOUCH_ENDED) {
                self.hide();
            }
        });

        //vip玩家头像框
        var hekuang = bg_kuang.getChildByName("head_bg");
        hekuang.setPosition(110, 200);
        //this.m_headSize = hekuang.getContentSize().width
        this.m_headPos = {x: hekuang.getPositionX() - 24, y: hekuang.getPositionY() + 14};
        hekuang.removeFromParent();
        this.m_spHead = hekuang;
        //签名
        this.m_textSign = bg_kuang.getChildByName("sign_text");

        //vip等级
        this.m_textVip = bg_kuang.getChildByName("vip_text");

        //昵称
        tmp = bg_kuang.getChildByName("nick_text");
        this.m_clipNick = new ClipText(tmp.getContentSize(), "", res.round_body_ttf, 24);
        this.m_clipNick.setPosition(180, 250);
        bg_kuang.addChild(this.m_clipNick);
        this.m_clipNick.setAnchorPoint(cc.p(0.0, 0.5));
        this.m_clipNick.setTextColor(cc.color(86,87,86,255));

        tmp.removeFromParent();

        //id
        // this.m_textId = bg_kuang.getChildByName("id_text");
        // this.m_textId.setAnchorPoint(cc.p(0.0, 0.5));
        // this.m_textId.setColor(cc.color(99, 44, 21));
        // this.m_textId.setPosition(210, 215);
        // this.m_textId.setLocalZOrder(5);

        this.m_textId = new cc.LabelTTF("this is test id", res.round_body_ttf, 24);
        this.m_textId.setAnchorPoint(cc.p(0.0, 0.5));
        this.m_textId.setColor(cc.color(99, 44, 21));
        this.m_textId.setPosition(210, 215);
        bg_kuang.addChild(this.m_textId, 100);

        //元宝
        this.m_textIngot = bg_kuang.getChildByName("ingot_text");

        //游戏币
        this.m_textScore = bg_kuang.getChildByName("score_text");

        //游戏豆
        // this.m_textBean = bg_kuang.getChildByName("bean_text");
        this.m_textBean = new cc.LabelTTF("this is Bean text", res.round_body_ttf, 24);
        this.m_textBean.setAnchorPoint(cc.p(0.0, 0.5));
        this.m_textBean.setColor(cc.color(255, 244, 200));
        this.m_textBean.setPosition(110, 88);
        bg_kuang.addChild(this.m_textBean);

        // ip地址
        // this.m_textIp = bg_kuang.getChildByName("ip_text");
        // this.m_textIp.setVisible(yl.SHOW_IP_ADDRESS);
        // this.m_textIp.setAnchorPoint(cc.p(0.0, 0.5));
        // this.m_textIp.setColor(cc.color(99, 44, 21));
        // this.m_textIp.setPosition(210, 175);

        this.m_textIp = new cc.LabelTTF("this is test id", res.round_body_ttf, 24);
        this.m_textIp.setAnchorPoint(cc.p(0.0, 0.5));
        this.m_textIp.setColor(cc.color(99, 44, 21));
        this.m_textIp.setPosition(210, 175);
        bg_kuang.addChild(this.m_textIp, 100);

        // 距离
        this.m_textDistance = bg_kuang.getChildByName("distance_text");
        this.m_textDistance.setVisible(yl.SHOW_IP_ADDRESS);
        this.m_posDistance = cc.p(this.m_textDistance.getPositionX(), this.m_textDistance.getPositionY());
        this.m_clipDistance = new ClipText(cc.size(120, 30), "");
        this.m_clipDistance.setVisible(yl.SHOW_IP_ADDRESS);
        if (true == isGame) {
            this.m_clipDistance.setPosition(cc.p(200, 97));
        } else {
            this.m_clipDistance.setPosition(cc.p(254, 132));
        }
        bg_kuang.addChild(this.m_clipDistance);
        this.m_clipDistance.setAnchorPoint(cc.p(0, 0.5));
        this.m_clipDistance.setTextColor(cc.color(168, 83, 0, 255));

        //添加好友/好友申请/好友
        var btn = bg_kuang.getChildByName("addfriend_btn");
        var btnEvent = function (sender, eventType) {
            if (eventType == ccui.Widget.TOUCH_ENDED) {
                var addFriendTab = {};
                addFriendTab.dwUserID = GlobalUserItem.dwUserID;
                addFriendTab.dwFriendID = self.m_userInfo.dwUserID || -1;
                addFriendTab.cbGroupID = 0;
                var sendResult = function (isAction) {
                    sender.setEnabled(!isAction);
                    if (!isAction) {
                        sender.setOpacity(255);//设置不透明度
                        // showToast(self, "添加好友失败", 10);
                    } else {
                        sender.setOpacity(125);
                        // showToast(self, "添加好友成功", 10);
                    }
                };
                //添加好友
                FriendMgr.getInstance().sendAddFriend(addFriendTab, sendResult);
            }
        };
        btn.addTouchEventListener(btnEvent);
        this.m_btnAddFriend = btn;

        this.m_spTextBg = bg_kuang.getChildByName("sp_text_bg");

        //加载动画
        this.m_actShowAct = new cc.ScaleTo(0.2, 1.0);
        ExternalFun.SAFE_RETAIN(this.m_actShowAct);

        var scale = new cc.ScaleTo(0.2, 0.0001);
        var call = new cc.CallFunc(function () {
            self.setVisible(false);
            self.m_spBgKuang.setAnchorPoint(cc.p(0.5, 0.5));
        });
        this.m_actHideAct = new cc.Sequence(scale, call);
        ExternalFun.SAFE_RETAIN(this.m_actHideAct);

    },

    onNearUserInfo : function( event ) {
        var nearuser = event.msg;
        cc.log(nearuser, "nearuser", 6);
        if (null != nearuser) {
            // ip地址
            var ipTable = ExternalFun.long2ip(nearuser.dwClientAddr);
            var r1 = ipTable.b;
            var r2 = ipTable.s;
            var r3 = ipTable.m;
            var r4 = ipTable.p;
            if (null == r1 || null == r2 || null == r3 || null == r4) {
                this.m_userInfo.szIpAddress = "";
            } else {
                this.m_userInfo.szIpAddress = r1 + "." + r2 + "." + r3 + "." + r4;
            }
            this.m_textIp.setString(this.m_userInfo.szIpAddress);

            // 距离
            this.m_userInfo.dwDistance = nearuser.dwDistance;
            if (null != this.m_userInfo.dwDistance && 0 != this.m_userInfo.dwDistance) {
                if (this.m_userInfo.dwDistance > 1000) {
                    this.m_clipDistance.setString(sprintf("%.2f", this.m_userInfo.dwDistance / 1000));
                    this.m_clipDistance.setPosition(this.m_posDistance.x, this.m_posDistance.y);
                    var num = (this.m_userInfo.dwDistance / 1000 + "").length;
                    this.m_textDistance.setPosition(this.m_posDistance.x + num * 10, this.m_posDistance.y);
                    this.m_textDistance.setString("千米");
                } else {
                    this.m_clipDistance.setString("" + this.m_userInfo.dwDistance);
                    this.m_clipDistance.setPosition(this.m_posDistance.x, this.m_posDistance.y);
                    var num = (this.m_userInfo.dwDistance + "").length;
                    this.m_textDistance.setPosition(this.m_posDistance.x + num * 10, this.m_posDistance.y);
                    this.m_textDistance.setString("米");
                }
            } else {
                this.m_clipDistance.setString("");
                this.m_textDistance.setString("");
            }
        }

    },

    //通知消息回调
    onNotify : function(msg) {
        var bRes = false;
        if (null == msg) {
            return bRes;
        }
        if (msg.main == chat_cmd.MDM_GC_USER && msg.sub == chat_cmd.SUB_GC_RESPOND_NOTIFY) {
            if (msg.name == "faceinfo_friend_response") {
                bRes = true;
            }
        }

        return bRes;
    },

    showLayer : function( v ) {
        this.m_spBgKuang.setScale(0.0001);
        this.setVisible(v);
    },

    reSet : function(  ) {
        //头像
        if (null != this.m_spHead && null != this.m_spHead.getParent()) {
            this.m_spHead.removeFromParent();
        }

        this.m_textSign.setString("");
        this.m_textVip.setString("");
        this.m_clipNick.setString("");
        this.m_textId.setString("");
        this.m_textIngot.setString("");
        this.m_textScore.setString("");
        this.m_textBean.setString("");
        this.m_textIp.setString("");
        this.m_textDistance.setString("");
    },

    refresh : function( useritem, popPos ,anr) {
        this.reSet();
        if (null == useritem) {
            return;
        }
        this.m_userInfo = useritem;
        //判断是否进入防作弊房间
        var bAntiCheat = GlobalUserItem.isAntiCheatValid(useritem.dwUserID);

        var nick = useritem.szNickName || "";
        var dwGameID = useritem.dwGameID || 0;
        var ingot = useritem.lIngot || useritem.lUserIngot;
        ingot = ingot || 0;
        var vipIdx = useritem.cbMemberOrder || 0;
        var sign = useritem.szSign || "此人很懒，没有签名";

        var ipAdress = useritem.szIpAddress || "";
        if (null != useritem.dwIpAddress) {
            if (0 == useritem.dwIpAddress) {
                useritem.dwIpAddress = ExternalFun.random_longip();
            }
            var ipTable = ExternalFun.long2ip(useritem.dwIpAddress);
            var r1 = ipTable.b;
            var r2 = ipTable.s;
            var r3 = ipTable.m;
            var r4 = ipTable.p;
            if (null == r1 || null == r2 || null == r3 || null == r4) {
                useritem.szIpAddress = "";
            } else {
                useritem.szIpAddress = r1 + "." + r2 + "." + r3 + "." + r4;
            }
            ipAdress = useritem.szIpAddress;
        }
        var dwDistance = useritem.dwDistance;

        sign = (sign == "") && "此人很懒，没有签名" || sign;

        var bEnable = useritem.dwUserID != GlobalUserItem.dwUserID;
        var visible = useritem.dwUserID != GlobalUserItem.dwUserID;
        if (bAntiCheat) {
            nick = "游戏玩家";
            dwGameID = 0000;
            ingot = 0;
            vipIdx = 0;
            sign = "此人很懒，没有签名";

            bEnable = false;
        }

        //自己签名
        if (useritem.dwGameID == GlobalUserItem.dwGameID) {
            sign = GlobalUserItem.szSign;
            ipAdress = GlobalUserItem.szIpAdress;
        } else {
            if (!bAntiCheat && null == dwDistance) {
                // 请求玩家信息
                FriendMgr.getInstance().sendQueryUserLocation(useritem.dwUserID);
            }
        }
        //背景图片位置
        this.m_spBgKuang.setPosition(680, 350);

        this.m_spBgKuang.stopAllActions();
        this.m_spBgKuang.runAction(this.m_actShowAct);

        //头像位置
        var head = new HeadSprite().createNormal(useritem, 95);
        if (null != head) {
            head.setPosition(this.m_headPos["x"] + 2, this.m_headPos["y"] - 2);

            this.m_spBgKuang.addChild(head);
            this.m_spHead = head;
        }

        //签名
        this.m_textSign.setString(sign);
        this.m_textSign.setVisible(!bAntiCheat);
        this.m_textSign.setVisible(false);
        //vip
        this.m_textVip.setString(sprintf("%d", vipIdx));
        var vipSp = this.m_spBgKuang.getChildByName("vip_sprite");
        if (null == vipSp) {
            vipSp = new cc.Sprite(res.atlas_vipnumber_png);
            if (null != vipSp) {
                vipSp.setName("vip_sprite");
                vipSp.setAnchorPoint(cc.p(0, 0.5));
                vipSp.setPosition(this.m_textVip.getPosition());
                vipSp.setVisible(false);
                this.m_spBgKuang.addChild(vipSp);

            }
        }
        vipSp.setScale(0.9);
        vipSp.setTextureRect(cc.rect(28 * vipIdx, 0, 28, 26));

        //昵称
        this.m_clipNick.setString(nick);

        //id
        var str = sprintf("%d", dwGameID);
        this.m_textId.setString(str);


        //元宝
        str = ExternalFun.numberThousands(ingot);
        if ((str).length > 11) {
            // str = string.sub(str, 1, 11) + "...";
            str = str.substring(0, 10) + "...";
        }
        this.m_textIngot.setString(str);

        //游戏币 (ClientUserItem || GlobalUserItem)
        var score = useritem.lScore || useritem.lUserScore;
        score = score || 0;
        str = ExternalFun.numberThousands(score);
        if (str.length > 11) {
            // str = string.sub(str, 1, 11) + "...";
            str = str.substring(0, 10);
        }

        this.m_textScore.setString(str);

        //游戏豆
        var beans = useritem.dBeans || useritem.dUserBeans;
        beans = beans || 0;
        var str = ""+beans;
        if (str.length > 7) {
            str = str.substring(0, 6) + "...";
        }
        this.m_textBean.setString(str);

        // ip
        this.m_textIp.setString(ipAdress);
        this.m_textIp.setVisible(yl.SHOW_IP_ADDRESS && !bAntiCheat);

        // 距离
        if (null != this.m_userInfo.dwDistance && 0 != this.m_userInfo.dwDistance) {
            if (this.m_userInfo.dwDistance > 1000) {
                this.m_clipDistance.setString(sprintf("%.2f", this.m_userInfo.dwDistance / 1000));
                this.m_clipDistance.setPosition(this.m_posDistance.x, this.m_posDistance.y);
                var num = (this.m_userInfo.dwDistance / 1000 + "").length + 3;
                this.m_textDistance.setPosition(this.m_posDistance.x + num * 10, this.m_posDistance.y);
                this.m_textDistance.setString("千米");
            } else {
                this.m_clipDistance.setString("" + this.m_userInfo.dwDistance);
                this.m_clipDistance.setPosition(this.m_posDistance.x, this.m_posDistance.y);
                var num = (this.m_userInfo.dwDistance + "").length;
                this.m_textDistance.setPosition(this.m_posDistance.x + num * 10, this.m_posDistance.y);
                this.m_textDistance.setString("米");
            }
        } else {
            this.m_clipDistance.setString("");
            this.m_textDistance.setString("");
        }
        this.m_clipDistance.setVisible(yl.SHOW_IP_ADDRESS && !bAntiCheat);
        this.m_textDistance.setVisible(yl.SHOW_IP_ADDRESS && !bAntiCheat);

        //好友状态
        if (true == bEnable) {
            var friendinfo = FriendMgr.getInstance().getFriendInfoByID(useritem.dwUserID);
            var strfile = "";
            if (null != friendinfo) {
                bEnable = false;

                //已是好友
                if (this.m_bIsGame) {
                    strfile = "bt_isfriend_game.png";
                } else {
                    strfile = "bt_isfriend_plazz.png";
                }
            } else {
                //添加好友
                if (this.m_bIsGame) {
                    strfile = "bt_addfriend_game.png";
                } else {
                    strfile = "bt_addfriend_plazz.png";
                }
            }
            if ("" != strfile) {
                this.m_btnAddFriend.loadTextureNormal(strfile, ccui.Widget.PLIST_TEXTURE);
            }
        }

        this.m_btnAddFriend.setEnabled(bEnable);
        this.m_btnAddFriend.setVisible(visible);
        if (false == bEnable) {
            this.m_btnAddFriend.setOpacity(125);
        } else {
            this.m_btnAddFriend.setOpacity(255);
        }
    },

    onExit : function( ) {
        if (null != this.listener) {
            var eventDispatcher = this.getEventDispatcher();
            eventDispatcher.removeEventListener(this.listener);
            this.listener = null;
        }

        ExternalFun.SAFE_RELEASE(this.m_actShowAct);
        this.m_actShowAct = null;
        ExternalFun.SAFE_RELEASE(this.m_actHideAct);
        this.m_actHideAct = null;

        if (null != this.m_listener) {
            var eventDispatcher = this.getEventDispatcher();
            eventDispatcher.removeEventListener(this.m_listener);
            this.m_listener = null;
        }
    },

    onEnter : function( ) {
        this.registerTouch();

        // 信息通知
        //this.m_listener = new cc.EventCustom(yl.RY_NEARUSER_NOTIFY, this.onNearUserInfo(this));
        //cc.eventManager.dispatchEvent(this.m_listener);
        //cc.director().getEventDispatcher().addEventListenerWithSceneGraphPriority(this.m_listener, this);
    },

    registerTouch : function() {
        var self = this;
        var customListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan : function (touch, event) {
                cc.log("PopupInfoLayer : onTouchBegan");
                return this.isVisible();
            },

            onTouchMoved: function (touch, event) {
                cc.log("PopupInfoLayer : onTouchMoved");
                return true;
            },

            onTouchEnded : function (touch, event) {
                cc.log("PopupInfoLayer : onTouchEnded");
                var pos = touch.getLocation();
                var m_spBg = this.m_spBgKuang;
                pos = m_spBg.convertToNodeSpace(pos);
                var rec = new cc.rect(0, 0, m_spBg.getContentSize().width, m_spBg.getContentSize().height);
                if (false == cc.rectContainsPoint(rec, pos)) {
                    self.hide();
                }
            }
        });
        cc.eventManager.addListener(customListener,this);

        // var listener = cc.EventListenerTouchOneByOne();
        // this.listener = listener;
        // listener.setSwallowTouches(true);
        // listener.registerScriptHandler(onTouchBegan, ccui.TOUCH_BEGAN);
        // listener.registerScriptHandler(onTouchEnded, ccui.Widget.TOUCH_ENDED);
        //var eventDispatcher = this.getEventDispatcher();
        //eventDispatcher.addEventListenerWithSceneGraphPriority(listener, this);
    },

    hide : function() {
        this.m_spBgKuang.stopAllActions();
        this.m_spBgKuang.runAction(this.m_actHideAct);
    }

});


