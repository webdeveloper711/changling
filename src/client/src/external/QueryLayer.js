/*
    Author  .   JIN
*/

// var ExternalFun = appdf.req(appdf.EXTERNAL_SRC + "ExternalFun");
//TITLE类别（即调用的子界面信息）

////////////////////////////////////////
//按钮定义区域（即按钮的点击事件tag）
BTN_CLOSE                           = 1;     //关闭按钮
BTN_SURE                            = 2;     //确认按钮
BTN_CANCEL                          = 3;     //取消按钮



var QueryLayer = cc.Layer.extend({


    ////////////////////////////////////////
    //窗体类别（即调用的界面类型）

    ctor : function(scene, title, callback, param, single, sureType, background) {
        this._super(cc.color(0,0,0,0));
        //初始化数据
        if (null != scene) {
            this._scene = scene;
        } else {
            cc.log("没传值，没scene，开什么玩笑？会不会调用函数啊？");
            return null;
        }
        //调用哪个界面
        this._title = title || "提示消息";
        //回调函数，默认为显示点了哪个按钮
        if (null != callback) {
            this._callback = callback;
        } else {
            this._callback = function (button) {
                if (button == true) {
                    cc.log("点了确认");
                } else if (button == false) {
                    cc.log("点了取消");
                } else {
                    cc.log("点了关闭");
                }
            }
        }
        //参数设置
        this._param = param;
        //是否单独界面，默认为否
        this._single = single || false;
        //确认类型,默认为普通型
        this._sureType = sureType || QueryLayer.TYPE_NORMALTYPE;
        //背景遮罩显示，默认为不显示
        background = background || false;

        //当设置为单界面时，走onChangeShowMode()
        if (single == true) {
            var paramsingle = {};
            paramsingle.title = title;
            paramsingle.callback = callback;
            paramsingle.param = param;
            paramsingle.single = "Ready";
            paramsingle.sureType = sureType;
            paramsingle.background = background;
            this._scene.onChangeShowMode(yl.SCENE_QUERY, paramsingle);
            return null;
        }

        var tempLayer = ExternalFun.loadRootCSB(res.Query_json, this);
        var rootLayer = tempLayer.rootlayer;
        this.csbNode = tempLayer.csbnode;

        var self = this;
        //按键监听
        var btcallback = function (ref, type) {
            if (type == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };

        //关闭按钮
        this._close = this.csbNode.getChildByName("btn_close");
        if (sureType != QueryLayer.TYPE_NORMALTYPE && sureType != QueryLayer.TYPE_SPECIAL) {
            this._close.setVisible(false);
        } else {
            this._close.setVisible(true);
        }
        this._close.setTag(BTN_CLOSE);
        this._close.addTouchEventListener(btcallback);

        //确定按钮
        this._confrim = this.csbNode.getChildByName("btn_confrim");
        if (sureType == QueryLayer.TYPE_NOTIFY || sureType == QueryLayer.TYPE_ASKTYPE || sureType == QueryLayer.TYPE_SPECIAL) {
            this._confrim.setVisible(true);
            if (sureType == QueryLayer.TYPE_NOTIFY) {
                this._confrim.setPosition(667, 271);    //居中显示
            }
        } else {
            this._confrim.setVisible(false);
        }
        this._confrim.setTag(BTN_SURE);
        this._confrim.addTouchEventListener(btcallback);

        //取消按钮
        this._cancel = this.csbNode.getChildByName("btn_cancel");
        if (sureType == QueryLayer.TYPE_ASKTYPE || sureType == QueryLayer.TYPE_SPECIAL) {
            this._cancel.setVisible(true);
        } else {
            this._cancel.setVisible(false);
        }
        this._cancel.setTag(BTN_CANCEL);
        this._cancel.addTouchEventListener(btcallback);

        //遮罩
        this._bg = this.csbNode.getChildByName("bg_mask");
        if (true == background) {
            this._bg.setOpacity(100);
        } else {
            this._bg.setOpacity(0);
        }

        //当不是单独显示的时候，将界面加载到sence上
        if (single != "Ready") {
            //没背景的话来段动画，放松一下~
            if (false == background) {
                this.setAnchorPoint(cc.p(0.5, 0.5));
                this.setScale(0.00001);
                this.runAction(new cc.ScaleTo(0.2, 1.0))
            }
            this._scene.addChild(this);
        }

        //子界面
        this._txttitle = this.csbNode.getChildByName("bg_title");
        this._msg = this.csbNode.getChildByName("txt_notify");
        this._panel = this.csbNode.getChildByName("panel");

        // var targetPlatform = cc.sys.os;
        // var modulestr = this._title.replace(".", "/");
        // if (cc.sys.OS_WINDOWS == targetPlatform) {
        //     modulestr = modulestr + ".lua";
        // } else {
        //     modulestr = modulestr + ".luac"
        // }

        //if (cc.FileUtils.getInstance().isFileExist(modulestr)) {
            this._lay = new RoomCreateResult(this._scene, this._param, this);
            this._msg.setVisible(false);
            this._txttitle.setVisible(false);
            this._panel.addChild(this._lay);
        // } else {
        //     this._txttitle.setVisible(true);
        //     this._msg.setVisible(true);
        //     this._msg.setText(this._title);
        // }

    },

    exit : function() {
        if (this._lay) {
            this._lay.onExit();
            this._panel.removeAllChildren(true);
        }

        if ("Ready" == this._single) {
            this._scene.onKeyBack();
        } else {
            this.removeFromParent(true);
        }
    },

    onButtonClickedEvent : function(tag,ref) {
        if (tag == BTN_CLOSE) {
            this._callback();
            this.exit();
        } else if (tag == BTN_SURE) {
            this._callback(true);
            if (this._sureType == QueryLayer.TYPE_NOTIFY) {
                this.exit();
            }
        } else if (tag == BTN_CANCEL) {
            this._callback(false);
            if (this._sureType == QueryLayer.TYPE_ASKTYPE) {
                this.exit();
            }
        }

    }

});

QueryLayer.TITLE_OPTION              = "plaza.views.layer.other.OptionLayer";    // 设置界面（有关闭按钮，无确定和取消）

QueryLayer.TYPE_NORMALTYPE           = 1;     // 普通状态（有关闭按钮，无确定和取消）
QueryLayer.TYPE_NOTIFY               = 2;    // 通知状态（无关闭按钮，确定即为关闭，无取消）
QueryLayer.TYPE_ASKTYPE              = 6;     // 询问状态（无关闭按钮，有确定和取消）
QueryLayer.TYPE_SPECIAL              = 7;     // 特别状态（全都有，这玩意谁用啊！）

////////////////////////////////////////

