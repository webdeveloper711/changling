/*
author. Li
 */
var GameModel = cc.Layer.extend({
    ctor : function (frameEngine, scene) {
        this._super();

        // 初始化界面
        //ExternalFun.registerNodeEvent(this);
        this._scene = scene;
        this._gameFrame = frameEngine;

        //设置搜索路径
        this._gameKind = this.getGameKind();
        this._searchPath = "";
        this.setSearchPath();

        this._gameView = this.CreateView();
        this.OnInitGameEngine();
        this.m_bOnGame = false;
        this.m_cbGameStatus = -1;
        GlobalUserItem.bAutoConnect = true;
    },
    setSearchPath : function () {
        if (null == this._gameKind)
            return;
        var entergame = this._scene.getEnterGameInfo();
        if (null != entergame) {
            var modulestr = entergame._KindName.replace(".", "/");
            // this._searchPath = jsb.fileUtils.getWritablePath() + "game-cc/" + modulestr + "/res/";
            // jsb.fileUtils.addSearchPath(this._searchPath);
        }
    },
    reSetSearchPath : function () {
        //重置搜索路径
        // var oldPaths = jsb.fileUtils.getSearchPaths();
        // var newPaths = [];
        // for (k in oldPaths) {
        //     if (String(oldPaths[k]) !== String(this._searchPath))
        //         newPaths.push(oldPaths[k]);
        // }
        // jsb.fileUtils.setSearchPaths(newPaths);
    },
    addPrivateGameLayer : function( layer ) {
        if (null == layer) {
            return;
        }
        // if (null !== this._gameView.addToRootLayer)
        //     this._gameView.addChild(layer, this.priGameLayerZorder());
        // else {
            this._gameView.addChild(layer, this.priGameLayerZorder());
        //}
    },
    // 房卡信息层zorder
    priGameLayerZorder : function() {
        if (null !== this._gameView && null !== this._gameView.priGameLayerZorder)
            return this._gameView.priGameLayerZorder();
        return yl.MAX_INT;
    },
    onExit : function() {
        GlobalUserItem.bAutoConnect = true;
        this.reSetSearchPath();
    },
    onEnterTransitionFinish : function() {
    },
    //显示等待
    showPopWait : function() {
        if (this._scene && this._scene.showPopWait)
            this._scene.showPopWait();
    },
    
    //关闭等待
    dismissPopWait : function() {
        if (this._scene && this._scene.dismissPopWait)
            this._scene.dismissPopWait();
    },
    
    //初始化游戏数据
    OnInitGameEngine : function() {
        this._ClockFun = null;
        this._ClockID = yl.INVALID_ITEM;
        this._ClockTime = 0;
        this._ClockChair = yl.INVALID_CHAIR;
        this._ClockViewChair = yl.INVALID_CHAIR;
    },
    
    //重置框架
    OnResetGameEngine : function() {
        this.KillGameClock();
        this.m_bOnGame = false;
    },
    //退出询问
    onQueryExitGame : function() {
        if (PriRoom && true === GlobalUserItem.bPrivateRoom) {
            if (0 === PriRoom.getInstance().m_tabPriData.dwPlayCount
                && 0 === this.m_cbGameStatus)
                this._gameFrame.StandUp(1);
            //this._gameFrame.StandUp(1)
            PriRoom.getInstance().queryQuitGame(this.m_cbGameStatus);
        } else {
            if (this._queryDialog)
                return;
            var self = this;
            this._queryDialog = new QueryDialog("您要退出游戏么？", function (ok) {
                if (ok === true) {
                    //退出防作弊
                    self._gameFrame.setEnterAntiCheatRoom(false);
    
                    self.onExitTable();
                }
                self._queryDialog = null;
            });
            this._queryDialog.setCanTouchOutside(false);
            this.addChild(this._queryDialog);
        }
    },
    
    standUpAndQuit : function() {
    
    },
    // 退出桌子
    onExitTable : function() {
        this.stopAllActions();
        this.KillGameClock();
    
        var MeItem = this.GetMeUserItem();
        if (MeItem && MeItem.cbUserStatus > yl.US_FREE) {
            var wait = this._gameFrame.StandUp(1);
            if (wait != null) {
                this.showPopWait();
                return;
            }
        }
        this.dismissPopWait();
        this._scene.onKeyBack();
    },
    
    onExitRoom : function() {
        this._gameFrame.onCloseSocket();
        this.stopAllActions();
        this.KillGameClock();
        this.dismissPopWait();
        this._scene.onChangeShowMode(yl.SCENE_ROOMLIST);
    },
    
    // 返回键处理
    onKeyBack : function() {
        this.onQueryExitGame();
        return true;
    },

    // 获取自己椅子
    GetMeChairID : function() {
        return this._gameFrame.GetChairID();
        // return;
    },
    
    // 获取自己桌子
    GetMeTableID : function() {
        return this._gameFrame.GetTableID();
        // return;
    },
    
    // 获取自己
    GetMeUserItem : function() {
        return this._gameFrame.GetMeUserItem();
    },
    
    // 椅子号转视图位置,注意椅子号从0~nChairCount-1,返回的视图位置从1~nChairCount
    SwitchViewChairID : function(chair) {
        var viewid = yl.INVALID_CHAIR;
        var nChairCount = this._gameFrame.getChairCount();
        var nChairID = this.GetMeChairID();
        if (chair !== yl.INVALID_CHAIR && chair < nChairCount)
            viewid = math_mod(chair + Math.floor(nChairCount * 3 / 2) - nChairID, nChairCount) + 1;
        return viewid;
    },
    
    // 是否合法视图id
    IsValidViewID : function( viewId ) {
        var nChairCount = this._gameFrame.getChairCount();
        return (viewId >= 0) && (viewId < nChairCount);
    },
    
    // 设置计时器
    SetGameClock : function(chair,id,time) {
        if (!this._ClockFun) {
            this._ClockFun = this.schedule(this.OnClockUpdata, 1, false);
        }
        this._ClockChair = chair;
        this._ClockID = id;
        this._ClockTime = time;
        this._ClockViewChair = this.SwitchViewChairID(chair);
        this.OnUpdataClockView();
    },
    GetClockViewID : function() {
        return this._ClockViewChair;
    },

    // 关闭计时器
    KillGameClock : function(notView) {
        cc.log("KillGameClock");
        this._ClockID = yl.INVALID_ITEM;
        this._ClockTime = 0;
        this._ClockChair = yl.INVALID_CHAIR;
        this._ClockViewChair = yl.INVALID_CHAIR;
        if (this._ClockFun) {
            //注销时钟
            cc.director.getScheduler().unscheduleScriptEntry(this._ClockFun);
            this._ClockFun = null;
        }
        if (null !=notView)
            this.OnUpdataClockView();
    },
    
    //计时器更新
    OnClockUpdata : function() {
        if (this._ClockID != yl.INVALID_ITEM) {
            this._ClockTime = this._ClockTime - 1;
            var result = this.OnEventGameClockInfo(this._ClockChair, this._ClockTime, this._ClockID);
            if (result === true || this._ClockTime < 1)
                this.KillGameClock();
        }
        this.OnUpdataClockView();
    },
    //更新计时器显示
    OnUpdataClockView : function() {
        if (this._gameView && this._gameView.OnUpdataClockView)
            this._gameView.OnUpdataClockView(this._ClockViewChair, this._ClockTime);
    },

    //用户状态
    onEventUserStatus : function(useritem,newstatus,oldstatus) {
        if (!this._gameView || !this._gameView.OnUpdateUser)
            return;
        var MyTable = this.GetMeTableID();
        var MyChair = this.GetMeChairID();

        if (null == MyTable || MyTable == yl.INVALID_TABLE)
            return;

        //旧的清除
        if (oldstatus.wTableID == MyTable) {
            var viewid = this.SwitchViewChairID(oldstatus.wChairID);
            if (null != viewid && viewid !== yl.INVALID_CHAIR) {
                this._gameView.OnUpdateUser(viewid, null, useritem.cbUserStatus === yl.US_FREE);
                if (PriRoom)
                    PriRoom.getInstance().onEventUserState(viewid, useritem, true);
            }
        }

        //更新新状态
        if (newstatus.wTableID == MyTable) {
            var viewid = this.SwitchViewChairID(newstatus.wChairID);
            if (null != viewid && viewid !== yl.INVALID_CHAIR) {
                this._gameView.OnUpdateUser(viewid, useritem);
                if (PriRoom)
                    PriRoom.getInstance().onEventUserState(viewid, useritem, false);
            }
        }

    },

    //用户积分
    onEventUserScore : function(useritem) {
        if (!this._gameView || !this._gameView.OnUpdateUser)
            return;
        var MyTable = this.GetMeTableID();

        if (null == MyTable || MyTable == yl.INVALID_TABLE)
            return;

        if (MyTable === useritem.wTableID) {
            var viewid = this.SwitchViewChairID(useritem.wChairID);
            if (null != viewid && viewid !== yl.INVALID_CHAIR)
                this._gameView.OnUpdateUser(viewid, useritem);
        }
    },

    //用户进入
    onEventUserEnter : function(tableid,chairid,useritem) {

        if (!this._gameView || !this._gameView.OnUpdateUser)
            return;
        var MyTable = this.GetMeTableID();

        if (null == MyTable || MyTable == yl.INVALID_TABLE)
            return;

        if (MyTable == tableid) {
            var viewid = this.SwitchViewChairID(chairid);
            if (null != viewid && viewid !== yl.INVALID_CHAIR) {
                this._gameView.OnUpdateUser(viewid, useritem);

                if (PriRoom)
                    PriRoom.getInstance().onEventUserState(viewid, useritem, false);
            }
        }
    },

    //发送准备
    SendUserReady : function(dataBuffer) {
        this._gameFrame.SendUserReady(dataBuffer);
    },

    //发送数据
    SendData : function(sub,dataBuffer) {
        if (this._gameFrame) {
            dataBuffer.setcmdinfo(yl.MDM_GF_GAME, sub);
            return this._gameFrame.sendSocketData(dataBuffer);
        }

        return false;
    },

    //是否观看
    IsLookonMode : function() {

    },

    //播放音效
    PlaySound : function(path) {
        if (GlobalUserItem.bSoundAble ) {
            cc.audioEngine.playEffect(path);
        }
    },

    //获取gamekind
    getGameKind : function() {
        return null;
    },

    // 创建场景
    CreateView : function() {
        // body
    },

    // 场景消息
    onEventGameScene : function(cbGameStatus,dataBuffer){

    },

    // 游戏消息
    onEventGameMessage : function(sub,dataBuffer){
        // body
    },

    // 计时器响应
    OnEventGameClockInfo : function(chair,time,clockid){
        // body
    },

    // 私人房解散响应
    // useritem 用户数据
    // cmd_table CMD_GR_RequestReply(回复数据包)
    // 返回是否自定义处理
    onCancellApply : function(useritem, cmd_table){
        cc.log("base onCancellApply");
        return false;
    },

    // 私人房解散结果
    // 返回是否自定义处理
    onCancelResult : function( cmd_table ){
        cc.log("base onCancelResult");
        return false;
    },

    // 桌子坐下人数
    onGetSitUserNum : function(){
        cc.log("base get sit user number");
        return 0;
    },

    // 根据chairid获取玩家信息
    getUserInfoByChairID : function( chairid ){

    }
});




