/*
    author : Kil
    date : 2017-12-6
 */
var GameRoomLayerModel = cc.Layer.extend({
    ctor : function () {
        this._super();
    },
    getTableParam : function () {
        return [null, null];
    },
    getShowCount : function () {
        return 3;
    }
});
