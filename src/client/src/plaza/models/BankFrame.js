/*
* author: Ri Chung Hyok
* Define BankFrame class
**/

// var BaseFrame = appdf.req(appdf.CLIENT_SRC+"plaza.models-cc-cc.BaseFrame");
// var BankFrame = class("BankFrame",BaseFrame);
// var logincmd = appdf.req(appdf.HEADER_SRC + "CMD_LogonServer");
// var game_cmd = appdf.req(appdf.HEADER_SRC + "CMD_GameServer");
//var ExternalFun = require(appdf.EXTERNAL_SRC + "ExternalFun");

function BankFrame(view, callback) {
    BaseFrame.call(this, view, callback);
}

BankFrame.prototype = Object.create(BaseFrame.prototype);
BankFrame.prototype.constructor = BankFrame;

// 银行刷新
BankFrame.OP_BANK_REFRESH = 0;
// 银行存款
BankFrame.OP_SAVE_SCORE = 1;
// 银行取款
BankFrame.OP_TAKE_SCORE = 2;
// 银行赠送
BankFrame.OP_SEND_SCORE = 3;
// 银行开通
BankFrame.OP_ENABLE_BANK = 4;
// 银行资料
BankFrame.OP_GET_BANKINFO = 5;
// 查询用户
BankFrame.OP_QUERY_USER = 6;
//  长连接开通银行
BankFrame.OP_ENABLE_BANK_GAME = 7;

// 连接结果
BankFrame.prototype.onConnectCompeleted = function() {
    cc.log("BankFrame.prototype.onConnectCompeleted oprateCode=" + this._oprateCode);

    if (this._oprateCode == BankFrame.OP_BANK_REFRESH)			// 刷新
        this.sendFlushScore();
    else if (this._oprateCode == BankFrame.OP_SAVE_SCORE)			// 存入
        this.sendSaveScore();
    else if (this._oprateCode == BankFrame.OP_TAKE_SCORE) 		// 取出
        this.sendTakeScore();
    else if (this._oprateCode == BankFrame.OP_SEND_SCORE)			// 赠送
        this.sendTransferScore();
    else if (this._oprateCode == BankFrame.OP_ENABLE_BANK)  		// 开通
        this.sendEnableBank();
    else if (this._oprateCode == BankFrame.OP_GET_BANKINFO) 		// 获取银行信息
        this.sendGetBankInfo();
    else if (this._oprateCode == BankFrame.OP_QUERY_USER)
        this.sendQueryUserInfo();
    else {
        this.onCloseSocket();
        if (null !== this._callBack) {
            this._callBack(-1, "未知操作模式！");
        }
    }

};

// 网络信息(短连接)
BankFrame.prototype.onSocketEvent = function(main,sub,pData){
    var bCloseSocket = true;
    if (main === logincmd.MDM_GP_USER_SERVICE) { // 用户服务
        if (sub === logincmd.SUB_GP_USER_INSURE_INFO)
            this.onSubGetBankInfo(pData);
        else if (sub === logincmd.SUB_GP_USER_INSURE_SUCCESS)
            this.onSubInsureSuccess(pData);
        else if (sub === logincmd.SUB_GP_USER_INSURE_FAILURE)
            this.onSubInsureFailue(pData);
        else if (sub === logincmd.SUB_GP_USER_INSURE_ENABLE_RESULT) {
            this.onSubEnableBankResult(pData);
            bCloseSocket = false;
        }
        else if (sub === logincmd.SUB_GP_QUERY_USER_INFO_RESULT) {
        }

        else {
            var message = "未知命令码：" + main + "-" + sub;  //string.format("未知命令码：%d-%d", main, sub);
            if (null !== this._callBack)
                this._callBack(-1, message);
        }
    }

    if (main === logincmd.MDM_GP_USER_SERVICE && sub === logincmd.SUB_GP_QUERY_USER_INFO_RESULT)
        this.onUserInfoResult(pData);
    else {
        if (bCloseSocket) this.onCloseSocket();
    }
};


// 网络消息(长连接)
BankFrame.prototype.onGameSocketEvent = function(main,sub,pData) {
    if (main === game_cmd.MDM_GR_INSURE) {
        cc.log("GameSocket Bank #" + main + "# #" + sub + "#");
        if (sub === game_cmd.SUB_GR_USER_INSURE_INFO) { 				// 银行资料
            var cmd_table = ExternalFun.read_netdata(game_cmd.CMD_GR_S_UserInsureInfo, pData);
            GlobalUserItem.lUserScore = cmd_table.lUserScore;
            GlobalUserItem.lUserInsure = cmd_table.lUserInsure;
            if (null !== this._callBack)
                this._callBack(BankFrame.OP_GET_BANKINFO, cmd_table);
            // 通知更新
            var eventListener = cc.EventCustom.new(yl.RY_USERINFO_NOTIFY);
            eventListener.obj = yl.RY_MSG_USERWEALTH;
            cc.director.getEventDispatcher().dispatchEvent(eventListener);
        }
        else if (sub === game_cmd.SUB_GR_USER_INSURE_SUCCESS) { 			// 银行成功
            var cmd_table = ExternalFun.read_netdata(game_cmd.CMD_GR_S_UserInsureSuccess, pData);
            GlobalUserItem.lUserScore = cmd_table.lUserScore;
            GlobalUserItem.lUserInsure = cmd_table.lUserInsure;
            // 通知更新
            var eventListener = cc.EventCustom.new(yl.RY_USERINFO_NOTIFY);
            eventListener.obj = yl.RY_MSG_USERWEALTH;
            cc.director.getEventDispatcher().dispatchEvent(eventListener);

            if (null !== this._callBack)
                this._callBack(1, "操作成功！");
        }
        else if (sub === game_cmd.SUB_GR_USER_INSURE_FAILURE) {			// 银行失败
            var cmd_table = ExternalFun.read_netdata(game_cmd.CMD_GR_S_UserInsureFailure, pData);
            if (null !== this._callBack)
                this._callBack(-1, cmd_table.szDescribeString);
        }
        else if (sub === game_cmd.SUB_GR_USER_TRANSFER_USER_INFO) { 		// 用户资料
            var cmd_table = ExternalFun.read_netdata(game_cmd.CMD_GR_S_UserTransferUserInfo, pData);
            this._tabTarget.opTargetAcconts = cmd_table.szAccounts;
            this._tabTarget.opTargetID = cmd_table.dwTargerUserID;

            var buffer = ExternalFun.create_netdata(game_cmd.CMD_GP_C_TransferScoreRequest);
            buffer.setcmdinfo(game_cmd.MDM_GR_INSURE, game_cmd.SUB_GR_TRANSFER_SCORE_REQUEST);
            buffer.pushbyte(game_cmd.SUB_GR_TRANSFER_SCORE_REQUEST);
            buffer.pushscore(this._lOperateScore);
            buffer.pushstring(cmd_table.szAccounts, yl.LEN_ACCOUNTS);
            buffer.pushstring(md5(this._szPassword), yl.LEN_PASSWORD);
            buffer.pushstring("", yl.LEN_TRANS_REMARK);
            this._oprateCode = BankFrame.OP_SEND_SCORE;

            if (!this._gameFrame.sendSocketData(buffer))
                this._callBack(-1, "发送转账失败！");
        }
        else if (sub === game_cmd.SUB_GR_USER_INSURE_ENABLE_RESULT) { 	// 开通结果
            var cmd_table = ExternalFun.read_netdata(game_cmd.CMD_GR_S_UserInsureEnableResult, pData);
            cc.log(cmd_table+"CMD_GR_S_UserInsureEnableResult"+6);

            GlobalUserItem.cbInsureEnabled = cmd_table.cbInsureEnabled;
            if (null !== this._callBack)
                this._callBack(BankFrame.OP_ENABLE_BANK_GAME, cmd_table.szDescribeString);
        }
    }
};


/**
 *
 * @param {Cmd_Data} pData
 */
BankFrame.prototype.onSubEnableBankResult = function(pData) {
    GlobalUserItem.cbInsureEnabled = pData.readbyte();
    var szTipString = pData.readstring();
    if (null !== this._callBack)
        this._callBack(2, szTipString);
};

/**
 *
 * @param {Cmd_Data} pData
 */
BankFrame.prototype.onSubInsureSuccess = function(pData) {
    var dwUserID = pData.readdword();
    if (dwUserID === GlobalUserItem.dwUserID) {
        GlobalUserItem.lUserScore = GlobalUserItem.readScore(pData);
        GlobalUserItem.lUserInsure = GlobalUserItem.readScore(pData);
        if (null !== this._callBack)
            this._callBack(1, "操作成功！");

        // 通知更新
        var eventListener = cc.EventCustom.new(yl.RY_USERINFO_NOTIFY);
        eventListener.obj = yl.RY_MSG_USERWEALTH;
        cc.director.getEventDispatcher().dispatchEvent(eventListener);
    }
};

/**
 *
 * @param {Cmd_Data} pData
 */
BankFrame.prototype.onSubInsureFailue = function(pData) {
    var lError = pData.readint();
    var szError = pData.readstring();
    if (null !== this._callBack)
        this._callBack(-1, szError);
};

/**
 * 获取到银行资料
 * @param {Cmd_Data} pData
 */
BankFrame.prototype.onSubGetBankInfo = function( pData ) {
    var cmdtable = ExternalFun.read_netdata(logincmd.CMD_GP_UserInsureInfo, pData);
    GlobalUserItem.lUserScore = cmdtable.lUserScore;
    GlobalUserItem.lUserInsure = cmdtable.lUserInsure;
    if (null !== this._callBack)
        this._callBack(BankFrame.OP_GET_BANKINFO, cmdtable);
    // 通知更新
    var eventListener = cc.EventCustom.new(yl.RY_USERINFO_NOTIFY);
    eventListener.obj = yl.RY_MSG_USERWEALTH;
    cc.director.getEventDispatcher().dispatchEvent(eventListener);
};

/**
 *
 * @param {Cmd_Data} pData
 */
BankFrame.prototype.onUserInfoResult = function(pData) {
    var cmdtable = ExternalFun.read_netdata(logincmd.CMD_GP_UserTransferUserInfo, pData);
    this._target = cmdtable.szAccounts;
    this._tabTarget.opTargetAcconts = cmdtable.szAccounts;
    this._tabTarget.opTargetID = cmdtable.dwTargetGameID;

    this._oprateCode = BankFrame.OP_SEND_SCORE;
    this.sendTransferScore();
};

// 开通
BankFrame.prototype.sendEnableBank = function() {
    var EnableBank = new Cmd_Data(202);
    var password = md5(GlobalUserItem.szPassword);
    var bankpass = md5(this._szPassword);
    var machine = GlobalUserItem.szMachine;
    EnableBank.setcmdinfo(yl.MDM_GP_USER_SERVICE, yl.SUB_GP_USER_ENABLE_INSURE);
    EnableBank.pushdword(GlobalUserItem.dwUserID);
    EnableBank.pushstring(password, 33);
    EnableBank.pushstring(bankpass, 33);
    EnableBank.pushstring(machine, 33);

    // 发送失败
    if (!this.sendSocketData(EnableBank) && null !== this._callBack)
        this._callBack(-1, "发送开通失败！");
};

// 刷新
BankFrame.prototype.sendFlushScore = function() {
    var FlushData = new Cmd_Data(37);
    FlushData.setcmdinfo(yl.MDM_GP_USER_SERVICE, yl.SUB_GP_QUERY_INSURE_INFO);
    FlushData.pushdword(GlobalUserItem.dwUserID);
    FlushData.pushstring(GlobalUserItem.szPassword, 33);

    // 发送失败
    if (!this.sendSocketData(FlushData) && null !== this._callBack)
        this._callBack(-1, "发送查询失败！");
};

// 存入
BankFrame.prototype.sendSaveScore = function() {
    var SaveData = new Cmd_Data(78);
    SaveData.setcmdinfo(yl.MDM_GP_USER_SERVICE, yl.SUB_GP_USER_SAVE_SCORE);
    SaveData.pushdword(GlobalUserItem.dwUserID);
    SaveData.pushscore(this._lOperateScore);
    cc.log("this._lOperateScore = " + this._lOperateScore);
    SaveData.pushstring(GlobalUserItem.szMachine, 33);
    // 发送失败
    if (!this.sendSocketData(SaveData) && null !== this._callBack)
        this._callBack(-1, "发送存款失败！");
};

// 取出
BankFrame.prototype.sendTakeScore = function() {
    var TakeData = new Cmd_Data(210 - 66);
    TakeData.setcmdinfo(yl.MDM_GP_USER_SERVICE, yl.SUB_GP_USER_TAKE_SCORE);
    TakeData.pushdword(GlobalUserItem.dwUserID);
    TakeData.pushscore(this._lOperateScore);
    TakeData.pushstring(md5(this._szPassword), 33);
    //  TakeData:pushstring(GlobalUserItem.szDynamicPass,33)
    TakeData.pushstring(GlobalUserItem.szMachine, 33);
    // 发送失败
    if (!this.sendSocketData(TakeData) && null !== this._callBack)
        this._callBack(-1, "发送取款失败！");
};

// 发送赠送
BankFrame.prototype.sendTransferScore = function() {
    var TransferScore = new Cmd_Data(272);
    TransferScore.setcmdinfo(yl.MDM_GP_USER_SERVICE, yl.SUB_GR_USER_TRANSFER_SCORE);
    TransferScore.pushdword(GlobalUserItem.dwUserID);
    TransferScore.pushscore(this._lOperateScore);
    TransferScore.pushstring(md5(this._szPassword), 33);
    TransferScore.pushstring(this._target, 32);
    TransferScore.pushstring(GlobalUserItem.szMachine, 33);
    TransferScore.pushstring("", 32);

    if (!this.sendSocketData(TransferScore) && null !== this._callBack)
        this._callBack(-1, "发送赠送失败！");
};

// 发送查询银行数据
BankFrame.prototype.sendGetBankInfo = function() {
    var cmd = new Cmd_Data(70);
    cmd.setcmdinfo(logincmd.MDM_GP_USER_SERVICE, logincmd.SUB_GP_QUERY_INSURE_INFO);
    cmd.pushdword(GlobalUserItem.dwUserID);
    cmd.pushstring(md5(GlobalUserItem.szPassword), yl.LEN_PASSWORD);

    if (!this.sendSocketData(cmd) && null !== this._callBack)
        this._callBack(-1, "发送查询失败！");
};

// 发送查询用户信息
BankFrame.prototype.sendQueryUserInfo = function() {
    this._oprateCode = BankFrame.OP_GET_BANKINFO;
    if (null !== this._gameFrame && this._gameFrame.isSocketServer()) {
        var buffer = ExternalFun.create_netdata(game_cmd.CMD_GR_C_QueryInsureInfoRequest);
        buffer.setcmdinfo(game_cmd.MDM_GR_INSURE, game_cmd.SUB_GR_QUERY_INSURE_INFO);
        buffer.pushbyte(game_cmd.SUB_GR_QUERY_INSURE_INFO);
        buffer.pushstring(md5(GlobalUserItem.szPassword), yl.LEN_PASSWORD);
        if (!this._gameFrame.sendSocketData(buffer))
            this._callBack(-1, "发送查询失败！")
    }
    else {
        if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null !== this._callBack)
            this._callBack(-1, "建立连接失败！");
    }
};
