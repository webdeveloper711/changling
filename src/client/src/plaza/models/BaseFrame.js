/*
* author: Jong
* Define BaseFrame class
**/

/**
 *
 * @param view
 * @param callback
 * @constructor
 */
function BaseFrame(view, callback) {
    this._viewFrame = view;
    this._threadid  = null;
    this._socket    = null;
    this._callBack = callback;
    // 游戏长连接
    this._gameFrame = null;
    this.m_tabCacheMsg = [];
}

BaseFrame.prototype.setCallback = function(callback) {
    this._callBack = callback;
};

BaseFrame.prototype.setViewFrame = function(viewFrame) {
    this._viewFrame = viewFrame;
};

BaseFrame.prototype.setSocketEvent = function(socketEvent) {
    this._socketEvent = socketEvent;
};

BaseFrame.prototype.getViewFrame = function() {
    return this._viewFrame;
};

BaseFrame.prototype.isSocketServer = function() {
    return this._socket !== null && this._threadid !== null;
};

var targetPlatform = cc.sys.platform;

// 网络错误
BaseFrame.prototype.onSocketError = function(pData) {
    cc.log(this.constructor.name + " >> onSocketError:");
    if (this._threadid === null) {
        return;
    }

    cc.log(this.constructor.name + " >> onSocketError >> _threadid="+this._threadid);
    if( this._threadid ) {
        var cachemsg = JSON.stringify(this.m_tabCacheMsg);
        if (cachemsg !== null || cachemsg !== undefined) {
            if (cc.sys.DESKTOP_BROWSER == targetPlatform) {
                // TODO: LogAsset:getInstance():logData(cachemsg or "",true)
                cc.log(cachemsg || "");
            }
            else {
                // buglyReportLuaException(cachemsg or "", debug.traceback())
                throw cachemsg || "";
            }
        }
    }

    this.onCloseSocket();

    if  (this._callBack) {
        if (!pData) {
            this._callBack(-1, "网络断开！");
        }
        else if (typeof pData === "string") {
            this._callBack(-1, pData);
        }
        else {
            var errorcode = pData.readword();
            if (errorcode == null || errorcode === undefined)
                this._callBack(-1, "网络断开！");
            else if (errorcode == 6)
                this._callBack(-1, "长时间无响应，网络断开！");
            else if (errorcode == 3) {
                this._callBack(-1, "网络连接超时, 请重试!");
                // 切换地址 Replace address
                if (yl.SERVER_LIST[yl.CURRENT_INDEX]) {
                    yl.LOGONSERVER = yl.SERVER_LIST[yl.CURRENT_INDEX];
                }
                yl.CURRENT_INDEX = yl.CURRENT_INDEX + 1;
                if (yl.CURRENT_INDEX > yl.TOTAL_COUNT) {
                    yl.CURRENT_INDEX = 1;
                }
            }
            else {
                this._callBack(-1, "网络错误，code：" + errorcode);
            }
        }
    }
};

// 启动网络
BaseFrame.prototype.onCreateSocket = function(szUrl, nPort) {
    cc.log(this.constructor.name + " >> onCreateSocket:");
    // 已存在连接 Already connected
	if (this._socket) {
        cc.log("base frame >> already socked is connected");
        return false;
    }

	// 创建连接 Create connection
	var self = this;
	this._szServerUrl = szUrl;
	this._nServerPort = nPort;
	this._SocketFun = function(pData) {
        self.onSocketCallBack(pData);
    };
	this._socket = ClientSocket.createSocket(this._SocketFun);
	this._socket.setwaittime(0);
	if (this._socket.connectSocket(this._szServerUrl, this._nServerPort, yl.VALIDATE)) {
        this._threadid = 0;
        return true;
    }
	else { // 创建失败
        this.onCloseSocket();
        return false;
    }
};

// 网络消息回调
BaseFrame.prototype.onSocketCallBack = function(pData) {
    cc.log(this.constructor.name + " >> onSocketCallBack:");
    // 无效数据 Data is null
	if  (!pData) {
        return;
    }
	if (! this._callBack) {
        cc.log("base frame no callback");
        this.onCloseSocket();
        return;
    }

	// 连接命令 Connection command
	var main = pData.getmain();
	var sub =pData.getsub();
	cc.log("onSocketCallBack main:" + main + "#sub:" + sub);
	if (main == yl.MAIN_SOCKET_INFO) { 		// 网络状态 Network status
        if (sub == yl.SUB_SOCKET_CONNECT) {
            this._threadid = 1;
            this.onConnectCompeleted();
        }
        else if (sub == yl.SUB_SOCKET_ERROR) {	// 网络错误
            if (this._threadid) {
                this.onSocketError(pData);
            }
            else {
                this.onSocketError("网络错误，webSocket Code：105");
            }
        }
        else {
            this.onCloseSocket();
        }
    }
	else {
        if (1 == this._threadid) { // 网络数据
            this.onSocketEvent(main, sub, pData);
        }
    }
};

// 关闭网络
BaseFrame.prototype.onCloseSocket = function() {
    cc.log(this.constructor.name + " >> onCloseSocket:");
    //appdf.assert(false, "for the debugging");

    if (this._socket) {
        this._socket.releaseSocket();
        this._socket = null;
    }
	if (this._threadid) {
        this._threadid = null;
    }
	this._SocketFun = null;
};

/**
 * 发送数据
 * @param {Cmd_Data} pData
 * @returns {boolean}
 */
BaseFrame.prototype.sendSocketData = function(pData) {
    if (this._socket == null) {
        this._callBack(-1);
        return false;
    }

    cc.log(this.constructor.name + " >> sendSocketData main:" + pData.getmain() + ",sub:" + pData.getsub());

    var tabCache = [];
    tabCache["main"] = pData.getmain();
    tabCache["sub"] = pData.getsub();
    tabCache["len"] = pData.getlen();
    tabCache["kindid"] = GlobalUserItem.nCurGameKind;

    ExternalFun.dump(tabCache, "pData", 6);
    //appdf.assert(false, "pData.main Error");
    this.m_tabCacheMsg.push(tabCache);
	if (this.m_tabCacheMsg.length > 5) {
        this.m_tabCacheMsg.shift();
        this.m_tabCacheMsg.pop();
    }
	if (! this._socket.sendData(pData)) {
        this.onSocketError("发送数据失败！");
        return false;
    }
	return true;
};

// 连接OK
BaseFrame.prototype.onConnectCompeleted = function() {
    cc.log("warn BaseFrame-onConnectResult-");
};

// 网络信息(短连接)
BaseFrame.prototype.onSocketEvent = function(main, sub, pData) {
    cc.log("warn BaseFrame-onSocketData-" + main + "-" + sub);
};

// 网络消息(长连接)
BaseFrame.prototype.onGameSocketEvent = function(main, sub, pData) {
    cc.log("warn BaseFrame-onGameSocketEvent-" + main + "-" + sub);
};