/**
* @author: Ri Chung Hyok
* Define CheckinFrame class
 * 
 * reviewed by Jong 2017-12-02 02:10 AM
**/

// var BaseFrame = appdf.req(appdf.CLIENT_SRC+"plaza.models-cc-cc.BaseFrame")
// var CheckinFrame = class("CheckinFrame",BaseFrame)

function CheckinFrame(view, callback) {
    BaseFrame.call(this, view, callback);
}

CheckinFrame.prototype = Object.create(BaseFrame.prototype);
CheckinFrame.prototype.constructor = CheckinFrame;

CheckinFrame.QUERYCHECKIN = 0;
CheckinFrame.CHECKINDONE = 1;
CheckinFrame.GETMEMBERGIFT = 2;
CheckinFrame.QUERYMEMBERGIFT = 3;
CheckinFrame.BASEENSURETAKE = 4;
CheckinFrame.BASEENSUREQUERY = 5;

//连接结果
CheckinFrame.prototype.onConnectCompeleted = function() {

    cc.log("============Checkin onConnectCompeleted============");
    cc.log("CheckinFrame.prototype.onConnectCompeleted oprateCode=" + this._oprateCode);

    if (this._oprateCode === CheckinFrame.QUERYCHECKIN)			//查询
        this.sendCheckinQuery();
    else if (this._oprateCode === CheckinFrame.CHECKINDONE)		//签到
        this.sendCheckinDone();
    else if (this._oprateCode === CheckinFrame.GETMEMBERGIFT)		//领取会员礼包
        this.sendGetMemberGift();
    else if (this._oprateCode === CheckinFrame.QUERYMEMBERGIFT) 	//查询会员礼包
        this.sendCheckMemberGift();
    else if (this._oprateCode === CheckinFrame.BASEENSURETAKE) 	//领取低保
        this.sendBaseEnsureTake();
    else if (this._oprateCode === CheckinFrame.BASEENSUREQUERY) 	//低保参数查询
        this.sendBaseEnsureLoad();
    else {
        this.onCloseSocket();
        if (null !== this._callBack)
            this._callBack(-1, "未知操作模式！");
    }
};

//网络信息
CheckinFrame.prototype.onSocketEvent = function(main,sub,pData) {
    cc.log("============Checkin onSocketEvent============");
    cc.log("*socket event:" + main + "#" + sub);
    var bRes = false;
    if (main === yl.MDM_GP_USER_SERVICE) { //用户服务
        if (sub === yl.SUB_GP_CHECKIN_INFO) 						//查询签到
            bRes = this.onSubCheckinInfo(pData);
        else if (sub === yl.SUB_GP_CHECKIN_RESULT) 			//签到结果
            bRes = this.onSubCheckinResult(pData);
        else if (sub === yl.SUB_GP_MEMBER_QUERY_INFO_RESULT) 		//查询礼包结果
            this.onSubCheckMemberGift(pData);
        else if (sub === yl.SUB_GP_MEMBER_DAY_GIFT_RESULT) 		//领取礼包结果
            bRes = this.onSubGetMemberGift(pData);
        else if (sub === yl.SUB_GP_MEMBER_DAY_PRESENT_RESULT)
            this.onSubGetVipPresent(pData);
        else if (sub === yl.SUB_GP_BASEENSURE_RESULT) 				//领取低保结果
            this.onSubBaseEnsureResult(pData);
        else if (sub === yl.SUB_GP_BASEENSURE_PARAMETER) 			//低保参数
            this.onSubBaseEnsureParam(pData);
        else {
            var message = "未知命令码："+main+"-"+sub;  //string.format("未知命令码：%d-%d", main, sub);
            if (null !== this._callBack)
                this._callBack(-1, message);
        }
    }

    if (!bRes)
        this.onCloseSocket();
};

CheckinFrame.prototype.onSubCheckinInfo = function(pData) {
    cc.log("============CheckinFrame.onSubCheckinInfo============");
    GlobalUserItem.wSeriesDate = pData.readword();
    cc.log("wSeriesDate-" + GlobalUserItem.wSeriesDate);
    GlobalUserItem.bTodayChecked = pData.readbool();
    GlobalUserItem.bQueryCheckInData = true;
    for (var i = 0; i < yl.LEN_WEEK; i++) {
        GlobalUserItem.lRewardGold[i] = GlobalUserItem.readScore(pData);
        cc.log("lRewardGold[" + i + "]-" + GlobalUserItem.lRewardGold[i]);
    }

    if (GlobalUserItem.bTodayChecked) {
        //非会员标记当日已签到
        if (GlobalUserItem.cbMemberOrder === 0)
            GlobalUserItem.setTodayCheckIn();
    }

    if (null !== this._callBack)
        return this._callBack(1);
};

CheckinFrame.prototype.onSubCheckinResult = function(pData) {
    // CMD_GP_CheckInResult
    cc.log("============CheckinFrame.onSubCheckinResult============");
    GlobalUserItem.bTodayChecked = pData.readbool();
    var lscore = GlobalUserItem.readScore(pData);
    var szTip = pData.readstring();
    GlobalUserItem.bQueryCheckInData = true;
    if (GlobalUserItem.bTodayChecked === true) {
        GlobalUserItem.lUserScore = lscore;
        GlobalUserItem.wSeriesDate = GlobalUserItem.wSeriesDate + 1;

        //非会员标记当日已签到
        if (GlobalUserItem.cbMemberOrder === 0)
            GlobalUserItem.setTodayCheckIn();
    }

    //通知更新
    var eventListener = cc.EventCustom.new(yl.RY_USERINFO_NOTIFY);
    eventListener.obj = yl.RY_MSG_USERWEALTH;
    cc.director.getEventDispatcher().dispatchEvent(eventListener);

    if (null !== this._callBack)
        return this._callBack(10, szTip);
};

CheckinFrame.prototype.onSubCheckMemberGift = function(pData) {

    cc.log("============ CheckinFrame.onSubCheckMemberGift ============");
    var bPresent = pData.readbool();
    var bGift = pData.readbool();
    var giftCount = pData.readdword();
    cc.log("gift count " + giftCount);

    var gifts = [];
    for (var i = 0 ; i < giftCount; i++) {
        var item = {};
        item.count = pData.readword();
        item.id = pData.readword();

        //手机端筛选
        //if item.id > 100)
        gifts.push(item);
        //end
    }
    // 加入会员奖励
    item = {};
    item.count = GlobalUserItem.MemberList[GlobalUserItem.cbMemberOrder]._present || 0;
    cc.log("奖励 " + item.count);
    if (item.count) {
        item.id = "money";
        gifts.push(item);
    }

    if (null !== this._callBack) {
        if (bGift)
            this._callBack(CheckinFrame.QUERYMEMBERGIFT, gifts, true);
        else {
            if (GlobalUserItem.cbMemberOrder !== 0)
                GlobalUserItem.setTodayCheckIn();
            this._callBack(CheckinFrame.QUERYMEMBERGIFT, gifts, false);
        }
    }
};

CheckinFrame.prototype.onSubGetMemberGift = function(pData) {
    cc.log("============ CheckinFrame.onSubGetMemberGift ============");

    var bSuccessed = pData.readbool();
    var szTip = pData.readstring();

    cc.log("Gift tips " + szTip);
    if (null !== this._callBack)
        return this._callBack(CheckinFrame.GETMEMBERGIFT, "");
    return false;
};

// 会员送金结果
// CMD_GP_MemberDayPresentResult
CheckinFrame.prototype.onSubGetVipPresent = function(pData) {
    cc.log("============ CheckinFrame.onSubGetVipPresent ============");
    var bSuccessed = pData.readbool();
    var score = GlobalUserItem.readScore(pData);
    var szTip = pData.readstring();
    if (bSuccessed) {
        GlobalUserItem.lUserScore = score;
        //通知更新
        var eventListener = cc.EventCustom.new(yl.RY_USERINFO_NOTIFY);
        eventListener.obj = yl.RY_MSG_USERWEALTH;
        cc.director.getEventDispatcher().dispatchEvent(eventListener);
    }
    cc.log("Present tips " + szTip);
};

CheckinFrame.prototype.onSubBaseEnsureResult = function(pData) {
    //CMD_GP_BaseEnsureResult
    cc.log("============CheckinFrame.onSubBaseEnsureResult============");
    var bSuccess = pData.readbool();
    var lscore = GlobalUserItem.readScore(pData);
    var szTip = pData.readstring();

    if (true === bSuccess) {
        GlobalUserItem.lUserScore = lscore;
        //通知更新
        var eventListener = cc.EventCustom.new(yl.RY_USERINFO_NOTIFY);
        eventListener.obj = yl.RY_MSG_USERWEALTH;
        cc.director.getEventDispatcher().dispatchEvent(eventListener);
    }
    if (null !== this._callBack)
        this._callBack(2, szTip);
};

// 低保参数
CheckinFrame.prototype.onSubBaseEnsureParam = function( pData ) {
    // CMD_GP_BaseEnsureParamter
    cc.log("============CheckinFrame.onSubBaseEnsureParam============");
    var tab = [];
    tab.condition = GlobalUserItem.readScore(pData);
    tab.amount = GlobalUserItem.readScore(pData);
    tab.times = pData.readbyte();
    if (null !== this._callBack)
        this._callBack(CheckinFrame.BASEENSUREQUERY, tab);
};

//查询签到
CheckinFrame.prototype.sendCheckinQuery = function(){
    cc.log("sendCheckinQuery");
    var CheckinQuery = new Cmd_Data(70);
    CheckinQuery.setcmdinfo(yl.MDM_GP_USER_SERVICE, yl.SUB_GP_CHECKIN_QUERY);
    var password = md5(GlobalUserItem.szPassword);
    CheckinQuery.pushdword(GlobalUserItem.dwUserID);
    CheckinQuery.pushstring(password, 33);

    //发送失败
    if (!this.sendSocketData(CheckinQuery) && null !== this._callBack)
        this._callBack(-1, "发送查询失败！");
};

//执行签到
CheckinFrame.prototype.sendCheckinDone = function() {
    var CheckinDone = new Cmd_Data(136);
    CheckinDone.setcmdinfo(yl.MDM_GP_USER_SERVICE, yl.SUB_GP_CHECKIN_DONE);
    var password = md5(GlobalUserItem.szPassword);
    CheckinDone.pushdword(GlobalUserItem.dwUserID);
    CheckinDone.pushstring(password, 33);
    CheckinDone.pushstring(GlobalUserItem.szMachine, 33);

    //发送失败
    if (!this.sendSocketData(CheckinDone) && null !== this._callBack)
        this._callBack(-1, "发送签到失败！");
};

//查询礼包
CheckinFrame.prototype.sendCheckMemberGift = function() {
    var CheckMemberGift = new Cmd_Data(136);
    CheckMemberGift.setcmdinfo(yl.MDM_GP_USER_SERVICE, yl.SUB_GP_MEMBER_QUERY_INFO);
    CheckMemberGift.pushdword(GlobalUserItem.dwUserID);
    CheckMemberGift.pushstring(md5(GlobalUserItem.szPassword), yl.LEN_PASSWORD);
    CheckMemberGift.pushstring(GlobalUserItem.szMachine, yl.LEN_MACHINE_ID);

    //发送失败
    if (!this.sendSocketData(CheckMemberGift) && null !== this._callBack)
        this._callBack(-1, "发送查询礼包失败！");
};

//获取礼包
CheckinFrame.prototype.sendGetMemberGift = function() {
    var GetMemberGift = new Cmd_Data(136);
    GetMemberGift.setcmdinfo(yl.MDM_GP_USER_SERVICE, yl.SUB_GP_MEMBER_DAY_GIFT);
    GetMemberGift.pushdword(GlobalUserItem.dwUserID);
    GetMemberGift.pushstring(md5(GlobalUserItem.szPassword), yl.LEN_PASSWORD);
    GetMemberGift.pushstring(GlobalUserItem.szMachine, yl.LEN_MACHINE_ID);

    //发送失败
    if (!this.sendSocketData(GetMemberGift) && null !== this._callBack)
        this._callBack(-1, "发送获取礼包失败！");
};

//获取送金
CheckinFrame.prototype.sendGetVipPresend = function() {
    var dataBuffer = new Cmd_Data(136);
    dataBuffer.setcmdinfo(yl.MDM_GP_USER_SERVICE, yl.SUB_GP_MEMBER_DAY_PRESENT);
    dataBuffer.pushdword(GlobalUserItem.dwUserID);
    dataBuffer.pushstring(md5(GlobalUserItem.szPassword), yl.LEN_PASSWORD);
    dataBuffer.pushstring(GlobalUserItem.szMachine, yl.LEN_MACHINE_ID);

    //发送失败
    if (!this.sendSocketData(dataBuffer) && null !== this._callBack)
        this._callBack(-1, "发送会员送金失败！");
};

//领取低保
CheckinFrame.prototype.sendBaseEnsureTake = function() {
    var BaseEnsureTake = new Cmd_Data(136);
    BaseEnsureTake.setcmdinfo(yl.MDM_GP_USER_SERVICE, yl.SUB_GP_BASEENSURE_TAKE);

    BaseEnsureTake.pushdword(GlobalUserItem.dwUserID);
    BaseEnsureTake.pushstring(md5(GlobalUserItem.szPassword), yl.LEN_PASSWORD);
    BaseEnsureTake.pushstring(GlobalUserItem.szMachine, yl.LEN_MACHINE_ID);

    //发送失败
    if (!this.sendSocketData(BaseEnsureTake) && null !== this._callBack)
        this._callBack(-1, "发送领取低保失败！");
};

// 查询低保
CheckinFrame.prototype.sendBaseEnsureLoad = function() {
    var databuffer = new Cmd_Data(0);
    databuffer.setcmdinfo(yl.MDM_GP_USER_SERVICE, yl.SUB_GP_BASEENSURE_LOAD);
    //发送失败
    if (!this.sendSocketData(databuffer) && null !== this._callBack)
        this._callBack(-1, "发送加载低保失败！");
};

//查询签到
CheckinFrame.prototype.onCheckinQuery = function() {
    //操作记录
    this._oprateCode = CheckinFrame.QUERYCHECKIN;
    if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null !== this._callBack)
        this._callBack(-1, "建立连接失败！");
};

//执行签到
CheckinFrame.prototype.onCheckinDone = function() {
    //操作记录
    this._oprateCode = CheckinFrame.CHECKINDONE;
    if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null !== this._callBack)
        this._callBack(-1, "建立连接失败！");
};

//查询礼包
CheckinFrame.prototype.onCheckMemberGift = function() {
    //操作记录
    this._oprateCode = CheckinFrame.QUERYMEMBERGIFT;
    if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null !== this._callBack)
        this._callBack(-1, "建立连接失败！")
};

//获取礼包
CheckinFrame.prototype.onGetMemberGift = function() {
    //操作记录
    this._oprateCode = CheckinFrame.GETMEMBERGIFT;
    if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null !== this._callBack)
        this._callBack(-1, "建立连接失败！");
};
//领取低保
CheckinFrame.prototype.onBaseEnsureTake = function() {
    //操作记录
    this._oprateCode = CheckinFrame.BASEENSURETAKE;
    if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null !== this._callBack)
        this._callBack(-1, "建立连接失败！");
};

// 查询低保
CheckinFrame.prototype.onBaseEnsureLoad = function() {
    //操作记录
    this._oprateCode = CheckinFrame.BASEENSUREQUERY;
    if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null !== this._callBack)
        this._callBack(-1, "建立连接失败！");
};
