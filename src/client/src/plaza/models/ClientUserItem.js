/*
* author: Jong
 */

function ClientUserItem() {
    this.dwGameID		= 0;
	this.dwUserID		= 0;

	this.wFaceID 		= 0;
	this.dwCustomID		= 0;

	this.cbGender		= yl.GENDER_MANKIND;
	this.cbMemberOrder	= 0;

	this.wTableID		= yl.INVALID_TABLE;
	this.wChairID		= yl.INVALID_CHAIR;
	this.cbUserStatus 	= 0;

	this.lScore 			= 0;
	this.lIngot 			= 0;
    this.lRoomCrad 			= 0;
	this.dBeans 			= 0;
	this.lGrade				= 0;
	this.lInsure			= 0;

	this.dwWinCount		= 0;
	this.dwLostCount	= 0;
	this.dwDrawCount	= 0;
	this.dwFleeCount	= 0;
	this.dwExperience	= 0;

	this.szNickName = "";
}

ClientUserItem.prototype.testlog = function() {
    cc.log("ClientUserItem*******************************************");
    cc.log("dwGameID=" + this.dwGameID);
    cc.log("dwUserID=" + this.dwUserID);

    cc.log("wFaceID=" + this.wFaceID);
    cc.log("dwCustomID=" + this.dwCustomID);

    cc.log("cbGender=" + this.cbGender);
    cc.log("cbMemberOrder=" + this.cbMemberOrder);

    cc.log("wTableID=" + this.wTableID);
    cc.log("wChairID=" + this.wChairID);
    cc.log("cbUserStatus=" + this.cbUserStatus);

    cc.log("lScore=" + this.lScore);
    cc.log("lIngot=" + this.lIngot);
    cc.log("lRoomCrad=" + this.lRoomCrad);
    cc.log("dBeans=" + this.dBeans);

    cc.log("dwWinCount=" + this.dwWinCount);
    cc.log("dwLostCount=" + this.dwLostCount);
    cc.log("dwDrawCount=" + this.dwDrawCount);
    cc.log("dwFleeCount=" + this.dwFleeCount);
    cc.log("dwExperience=" + this.dwExperience);

    cc.log("szNickName=" + this.szNickName);
    cc.log("*********************************************************");
};