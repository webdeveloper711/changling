
FriendMgr.instance = null;

//好友文件存放路径
var FRIEND_PATH = /*jsb.fileUtils.getWritablePath() + */"friend";
//我的好友
var MYFRIEND_FILE_PATH = FRIEND_PATH;
//聊天信息本地存储最大数量
var MAX_CHAT_CACHE = 20;
function FriendMgr(){
}
FriendMgr.getInstance = function() {
    if (this.instance == null)
        this.instance = this.create();
    return this.instance;
};

FriendMgr.create = function() {
    var obj = new FriendMgr();
    obj.init();
    return obj;
};

FriendMgr.prototype.init = function() {

    //界面层
    this.m_viewLayer = null;

    this.m_Delegate = null;
    if (this.m_Socket)
        this.m_Socket.relaseSocket();

    this.m_Socket = null;
    this.m_CallBack = null;
    this.m_FriendTab = [];

    //加载框
    this._popWait = null;

    //断线自动重连次数
    this.m_nAutoLoginCount = 5;

    //聊天缓存
    this.m_tabChatCache = [];
    this.m_tabChatCache[GlobalUserItem.dwUserID] = [];
    //通知缓存
    this.m_tabNoticeCache = [];
    this.m_tabNoticeCache[GlobalUserItem.dwUserID] = [];
    //配置好友本地文件存储目录
    /*if("Windows"!=cc.sys.os){
        if (false == jsb.fileUtils().isDirectoryExist(FRIEND_PATH))
            jsb.fileUtils().createDirectory(FRIEND_PATH);
    }*/

    //配置自己好友文件存储目录
    MYFRIEND_FILE_PATH = FRIEND_PATH  + "_"+ GlobalUserItem.dwUserID;
    /*if(cc.sys.os!=cc.sys.OS_WINDOWS){
        if (false == jsb.fileUtils().isDirectoryExist(MYFRIEND_FILE_PATH))
            jsb.fileUtils().createDirectory(MYFRIEND_FILE_PATH);
    }*/
    /*if(!localStorage.getItem(MYFRIEND_FILE_PATH)){
        localStorage.setItem(MYFRIEND_FILE_PATH, "");
    }*/
    //数据缓存队列(用户缓存网络异常时未发送的数据)
    this.m_sendCache = [];
    //最后收到的喇叭消息
    this.m_lastTrumpetData = [];

    //注册监听
    this.registerNotifyList();
    //是否请求更新坐标
    this.m_bRequestUpdateLocation = false;
    //是否请求附近好友
    this.m_bQueryNearUserInfo = false;
};

FriendMgr.prototype.registerNotifyList = function() {

    //聊天
    var self = this;
    var notify = NotifyMgr.getInstance().createNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_USER_CHAT_NOTIFY);
    notify.name = "friend_chat";
    notify.fun = this.onNotify(notify);
    notify.pause = true;
    NotifyMgr.getInstance().registerNotify(notify);
    //申请好友通知
    var notify2 = NotifyMgr.getInstance().createNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_APPLYFOR_NOTIFY);
    notify2.name = "friend_apply";
    notify2.fun = this.onNotify(notify2);
    notify2.pause = true;
    NotifyMgr.getInstance().registerNotify(notify2);
    //回应通知
    var notify3 = NotifyMgr.getInstance().createNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_RESPOND_NOTIFY);
    notify3.name = "friend_response";
    notify3.fun = this.onNotify(notify3);
    notify3.pause = true;
    NotifyMgr.getInstance().registerNotify(notify3);
    //邀请通知
    var notify4 = NotifyMgr.getInstance().createNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_INVITE_GAME_NOTIFY);
    notify4.name = "friend_invite";
    notify4.fun = this.onNotify(notify4);
    notify4.pause = true;
    NotifyMgr.getInstance().registerNotify(notify4);
    //私人房邀请
    var notify5 = NotifyMgr.getInstance().createNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_INVITE_PERSONAL_NOTIFY);
    notify5.name = "pri_friend_invite";
    notify5.fun = this.onNotify(notify5);
    notify5.pause = true;
    NotifyMgr.getInstance().registerNotify(notify5);
    //分享通知
    var notify6 = NotifyMgr.getInstance().createNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_USER_SHARE_NOTIFY);
    notify6.name = "friend_share";
    notify6.fun = this.onNotify(notify6);
    notify6.pause = true;
    NotifyMgr.getInstance().registerNotify(notify6);
};

FriendMgr.prototype.setCallBackDelegate = function(delegate,callback) {
    //this.m_Delegate = delegate
    //this.m_CallBack = callback;
};

FriendMgr.prototype.setViewLayer = function(layer) {
    this.m_viewLayer = layer;
};

//通知消息回调
FriendMgr.prototype.onNotify = function(msg) {
    if (null != this.m_viewLayer && null != this.m_viewLayer.onNotify)
        return this.m_viewLayer.onNotify(msg);
};

//网络消息回调
FriendMgr.prototype.onSocketCallBack = function(pData) {
    //无效数据
    if (pData == null || null == this.m_Socket)
        return;
    //连接命令
    var main = pData.getmain();
    var sub = pData.getsub();
    //作为消息通知的参数
    var netdata = null;

    if (main == yl.MAIN_SOCKET_INFO) { 		//网络状态
        if (sub == yl.SUB_SOCKET_CONNECT)
            this.onConnectCompeleted();
        else if (sub == yl.SUB_SOCKET_ERROR)	//网络错误
            this.onSocketError(pData);
        else
            this.onCloseSocket();
    }
    else {
        //////////////////
        ////-单独数据处理
        //////////////////
        if (main == chat_cmd.MDM_GC_LOGON) {
            if (sub == chat_cmd.SUB_S_USER_FRIEND) { //好友消息
                this.onSocketEvent(main, sub, pData);
                return;
            }
        }
        else if (main == chat_cmd.MDM_GC_USER) {
            if (sub == chat_cmd.SUB_GC_USER_STATUS_NOTIFY) {  //好友上线、下线
                this.onSocketEvent(main, sub, pData);
                return;
            }
        }

        if (this.m_Delegate) {
            if (this.m_CallBack)
                this.m_CallBack(this.m_Delegate, main, sub, pData);
            else
                netdata = this.m_Delegate.onSocketEvent(main, sub, pData);
        }
        else
            netdata = this.onSocketEvent(main, sub, pData);

    }

    //通知处理
    NotifyMgr.getInstance().excute(main, sub, netdata);
};

//连接结果
FriendMgr.prototype.onConnectCompeleted = function() {
    this.m_Socket.setdelaytime(0);
    this.m_Socket.setwaittime(0.5);

    //发送登录
    this.login();
};

FriendMgr.prototype.getLocationInfo = function() {
    MultiPlatform.getInstance().requestLocation(function (result) {
        if (typeof(result) == "string" && "" != result) {
            GlobalUserItem.tabCoordinate.lo = 181.0;
            GlobalUserItem.tabCoordinate.la = 91.0;
			try{
                tabCoordinate = JSON.parse(result);
                var berror = tabCoordinate["berror"] || false;
                if (berror) {
                    var msg = tabCoordinate["msg"] || "";
                    var runScene = cc.director.getRunningScene();
                    if (null != runScene) {
                        //showToast(runScene, msg, 2)
                        this.dismissPopWait();
                    }
                    else {
                        //经度
                        var lo = Number(tabCoordinate["longitude"]) || 181.0;
                        //纬度
                        var la = Number(tabCoordinate["latitude"]) || 91.0;
                        GlobalUserItem.tabCoordinate.lo = lo;
                        GlobalUserItem.tabCoordinate.la = la;
                    }

                    if (!GlobalUserItem.bUpdateCoordinate && this.m_bRequestUpdateLocation) {
                        this.m_bRequestUpdateLocation = false;
                        this.updateLocation();
                    }
                }
			}catch(e){
                this.dismissPopWait();
			}
        }
    });
    this.dismissPopWait();//I added this line to remove popwait so remove after complete logic
};

//更新位置
FriendMgr.prototype.updateLocation = function() {
    this.m_bRequestUpdateLocation = true;
    if (GlobalUserItem.noCoordinateData())
        this.getLocationInfo();
    else
    //更新坐标
        this.sendUpdateCoordinate(GlobalUserItem.tabCoordinate.lo, GlobalUserItem.tabCoordinate.la);
};

//设置sleep
FriendMgr.prototype.setWaitTime = function(arg) {
    if (null != this.m_Socket)
        this.m_Socket.setwaittime(arg);
};

//网络错误
FriendMgr.prototype.onSocketError = function(pData) {
    this.onCloseSocket();
    if (!pData) {
        cc.log("网络断开！");
    }
    else if (typeof(pData) == "string") {
        cc.log(pData);
    }
    else {
        var errorcode = pData.readword();
        if (errorcode == null)
            cc.log("网络断开！");
        else if (errorcode == 3)
            cc.log("网络连接超时, 请重试!");
        //切换地址
        if (null != yl.SERVER_LIST[yl.CURRENT_INDEX])
            yl.LOGONSERVER = yl.SERVER_LIST[yl.CURRENT_INDEX];
        yl.CURRENT_INDEX = yl.CURRENT_INDEX + 1;
        if (yl.CURRENT_INDEX > yl.TOTAL_COUNT)
            yl.CURRENT_INDEX = 1;
        else
            cc.log("网络中断，请检查您的网络是否通畅！   " + errorcode);

    }
};

//网络信息
FriendMgr.prototype.onSocketEvent = function(main,sub,pData) {
    cc.log("FriendMgr.onSocketEvent * socket event:" + main + "#" + sub);
    //作为消息通知的参数
    var netdata = null;
    //this.dismissPopWait()

    if (main == chat_cmd.MDM_GC_LOGON) {				//用户信息
        if (sub == chat_cmd.SUB_S_USER_FRIEND) {  			//用户好友
            var wFriendCount = pData.readword();
            // appdf.assert(pData.getlen() != wFriendCount*102 +2,"//chat_cmd.tagClientFriendInfo-结构不匹配-");
            var getTab = [];
            for (var i = 0; i < wFriendCount ; i++) {
                getTab[i] = ExternalFun.read_netdata(chat_cmd.tagClientFriendInfo, pData);
            }
            //如果好友列表中已經存在該用戶，更新該用戶信息
            for (var i = 0; i < getTab.length; i++) {
                var isExit = false;
                for (var n = 0; n < this.m_FriendTab.length; n++) {
                    if (getTab[i].dwUserID == this.m_FriendTab[n].dwUserID) {
                        isExit = true;
                        this.m_FriendTab[n] = getTab[i];
                        // for (k in getTab[i]) {
                        //     this.m_FriendTab[n].k = getTab[i].k;
                        //     break;
                        // }
                    }
                }

                if (!isExit)
                    // this.m_FriendTab.push(getTab[i]);
                    this.m_FriendTab[this.m_FriendTab.length] = getTab[i];
            }
            for(var i=0;i<this.m_FriendTab.length;i++){
                for(var j=i;j<this.m_FriendTab.length;j++){
                    if(this.m_FriendTab[i].cbMainStatus<this.m_FriendTab[j].cbMainStatus){
                        var temp = this.m_FriendTab[i];
                        this.m_FriendTab[i] = this.m_FriendTab[j];
                        this.m_FriendTab[j] = temp;
                    }
                }
            }
            //刷新好友列表
            if (null != this.m_viewLayer && null != this.m_viewLayer.refreshFriendList)
                this.m_viewLayer.refreshFriendList();
        } else if (sub == chat_cmd.SUB_GC_LOGON_SUCCESS) {
            this.updateLocation();
        }
    } else if (main == chat_cmd.MDM_GC_USER) {	//用户命令
        netdata = this.onFriendMessage(sub, pData);

        this.popCacheMessage();
        return netdata;
    }
};

FriendMgr.prototype.onFriendMessage = function(sub,pData) {
    var netdata = null;

    if (sub == chat_cmd.SUB_GC_USER_STATUS_NOTIFY) {			//好友状态(上下线)
        var userStatus = ExternalFun.read_netdata(chat_cmd.CMD_GC_UserOnlineStatusNotify, pData);
        var userInfo = null;
        for (k in this.m_FriendTab) {
            if (this.m_FriendTab[k].dwUserID == userStatus.dwUserID) {
                cc.log("好友状态 ==> " + userStatus.cbMainStatus);
                this.m_FriendTab[k].cbMainStatus = userStatus.cbMainStatus;
                netdata = userStatus;
                userInfo = this.m_FriendTab[k];
            }
        }
        // table.sort(this.m_FriendTab, function(a, b)
        //     return a.cbMainStatus > b.cbMainStatus
        // end)
        //刷新好友列表
        if (null != this.m_viewLayer && null != this.m_viewLayer.refreshFriendList)
            this.m_viewLayer.refreshFriendList();
    }
    else if (sub == chat_cmd.SUB_GC_GAME_STATUS_NOTIFY) {  		//游戏状态
        var userStatus = ExternalFun.read_netdata(chat_cmd.CMD_GC_UserGameStatusNotify, pData);
        var userInfo = null;
        for (k in this.m_FriendTab) {
            if (this.m_FriendTab[k].dwUserID == userStatus.dwUserID)
                cc.log("游戏状态 ==> " + userStatus.cbGameStatus);
            this.m_FriendTab[k].cbGameStatus = userStatus.cbGameStatus;
            this.m_FriendTab[k].wServerID = userStatus.wServerID;
            this.m_FriendTab[k].wTableID = userStatus.wTableID;
            this.m_FriendTab[k].wKindID = userStatus.wKindID;
            this.m_FriendTab[k].szServerName = userStatus.szServerName;
            userInfo = this.m_FriendTab[k];
            break;
        }
        if (null != userInfo) {
            if (null != this.m_viewLayer && null != this.m_viewLayer.refreshFriendState)
                this.m_viewLayer.refreshFriendState(userInfo, true);
        }
    }
    else if (sub == chat_cmd.SUB_GC_USER_CHAT_NOTIFY) {		//聊天通知
        var notify = ExternalFun.read_netdata(chat_cmd.CMD_GC_UserChat, pData);
        cc.log(notify + "=============聊天消息==============");
        this.insertUserMsg(notify.dwSenderID, notify.dwSenderID, notify.dwTargetUserID, notify.szMessageContent);
        netdata = notify;

        if (null != this.m_viewLayer && null != this.m_viewLayer.messageNotify)
            this.m_viewLayer.messageNotify(notify);
    }
    else if (sub == chat_cmd.SUB_GC_APPLYFOR_NOTIFY) { 			//申请通知
        cc.log("InsetFriendNotify1");
        var notify = ExternalFun.read_netdata(chat_cmd.CMD_GC_ApplyForNotify, pData);
        this.insertFriendNotify(1, notify);
        // var msg = NotifyMgr.getInstance().createNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_APPLYFOR_NOTIFY);
        // NotifyMgr.getInstance().showNotify(this.m_viewLayer.m_btnMsgNotice, msg, cc.p(210, 90));
        if (null != this.m_viewLayer && null != this.m_viewLayer.updateNotifyList){
            this.m_viewLayer.updateNotifyList();
            var msg = NotifyMgr.getInstance().createNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_APPLYFOR_NOTIFY);
            this.m_viewLayer.onNotify(msg);
        } else {
            var runScene = cc.director.getRunningScene();
            var msg = NotifyMgr.getInstance().createNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_APPLYFOR_NOTIFY);
            NotifyMgr.getInstance().showNotify(runScene.m_btnFriend, msg, cc.p(57, 70));
        }
        cc.log("view : " + this.m_viewLayer);
    }
    else if (sub == chat_cmd.SUB_GC_RESPOND_NOTIFY) { 			//回应通知
        cc.log("InsertFriendNotify2");
        var szNotifyContent = pData.readstring();
        this.insertFriendNotify(2, szNotifyContent);
        // var msg = NotifyMgr.getInstance().createNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_RESPOND_NOTIFY);
        // NotifyMgr.getInstance().showNotify(this.m_viewLayer.m_btnMsgNotice, msg, cc.p(210, 90));
        if (null != this.m_viewLayer && null != this.m_viewLayer.updateNotifyList){
            this.m_viewLayer.updateNotifyList();
            var msg = NotifyMgr.getInstance().createNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_RESPOND_NOTIFY);
            this.m_viewLayer.onNotify(msg);
        } else {
            var runScene = cc.director.getRunningScene();
            var msg = NotifyMgr.getInstance().createNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_RESPOND_NOTIFY);
            NotifyMgr.getInstance().showNotify(runScene.m_btnFriend, msg, cc.p(57, 70));
        }
        cc.log("View : " + this.m_viewLayer);
    }
    else if (sub == chat_cmd.SUB_GC_SEARCH_USER_RESULT) { 		//查找通知/查找结果
        //chat_cmd.CMD_GC_SearchByGameIDResult
        var cbUserCount = pData.readbyte();
        var userTab = {};
        for (var i = 0; i < cbUserCount; i++) {
            userTab[i] = ExternalFun.read_netdata(chat_cmd.tagClientFriendInfo, pData);
        }
        if (null != this.m_viewLayer && null != this.m_viewLayer.searchResult)
            this.m_viewLayer.searchResult(userTab);
    }
    else if (sub == chat_cmd.SUB_GC_INVITE_GAME_NOTIFY) { 		//邀请通知
        var notify = ExternalFun.read_netdata(chat_cmd.CMD_GC_InviteGameNotify, pData);
        cc.log(notify + "SUB_GC_INVITE_GAME_NOTIFY" + 3);
        this.insertUserMsg(notify.dwSenderID, notify.dwSenderID, GlobalUserItem.dwUserID, notify.szInviteMsg);
        netdata = notify;
        if (null != this.m_viewLayer && null != this.m_viewLayer.messageNotify)
            this.m_viewLayer.messageNotify(notify);
        if (null != this.m_query && null != this.m_query.getParent())
            this.m_query.removeFromParent();
        var runScene = cc.director.getRunningScene();
        if (null != runScene) {
            var list = this.getFriendList();
            var nick = "";
            for (index in list) {
                if (list[index].dwUserID == notify.dwSenderID) {
                    nick = list[index].szNickName;
                    break;
                }
            }
            var msgTab = {};
            msgTab.main = "您的好友 " + nick + " @您一起游戏";
            msgTab.sub = notify.szInviteMsg;
            this.m_query = FriendQueryDialog.create(msgTab, function (ok) {
            }, null, FriendQueryDialog.QUERY_SURE)
                .setCanTouchOutside(false)
                .addTo(runScene, yl.ZORDER.Z_INVITE_DLG);
        }
    }
    else if (sub == chat_cmd.SUB_GC_INVITE_PERSONAL_NOTIFY) { //私人房邀请通知
        var notify = ExternalFun.read_netdata(chat_cmd.CMD_GC_InvitePersonalGameNotify, pData);
        cc.log(notify + "SUB_GC_INVITE_GAME_NOTIFY" + 3);
        this.insertUserMsg(notify.dwSenderID, notify.dwSenderID, GlobalUserItem.dwUserID, notify.szInviteMsg);
        netdata = notify;
        if (null != this.m_viewLayer && null != this.m_viewLayer.messageNotify)
            this.m_viewLayer.messageNotify(notify);
        if (null != this.m_query && null != this.m_query.getParent())
            this.m_query.removeFromParent();
        var runScene = cc.director.getRunningScene();
        if (null != runScene) {
            var list = this.getFriendList();
            var nick = "";
            for (k in list) {
                if (list[k].dwUserID == notify.dwSenderID) {
                    nick = list[k].szNickName;
                    break;
                }
            }
            var msgTab = {};
            msgTab.main = "您的好友 " + nick + " @您一起约战";
            msgTab.sub = notify.szInviteMsg;
            this.m_query = FriendQueryDialog.create(msgTab, function (ok) {
                if (ok) {
                    showToast(runScene, "房间ID复制成功!", 1);
                    GlobalUserItem.szCopyRoomId = sprintf("%06d", notify.dwServerNumber);
                }
                this.m_query = null;
            }, null, FriendQueryDialog.QUERY_SURE_CANCEL)
                .setCanTouchOutside(false)
                .addTo(runScene, yl.ZORDER.Z_INVITE_DLG);
        }
    }
    else if (sub == chat_cmd.SUB_GC_TRUMPET_NOTIFY) {			//喇叭通知
        var data_tab = ExternalFun.read_netdata(chat_cmd.CMD_GC_Trumpet_S, pData);
        netdata = data_tab;
    }
    else if (sub == chat_cmd.SUB_GC_DELETE_FRIEND_NOTIFY) { 	//删除通知
        var data_tab = ExternalFun.read_netdata(chat_cmd.CMD_GC_DeleteFriendNotify, pData);
        var idx = null;
        for (k in this.m_FriendTab) {
            if (this.m_FriendTab[k].dwUserID == data_tab.dwFriendUserID) {
                idx = k;
                break;
            }
        }
        if (null != idx)
            this.m_FriendTab.splice(idx,1);

        //刷新好友列表
        if (null != this.m_viewLayer && null != this.m_viewLayer.refreshFriendList)
            this.m_viewLayer.refreshFriendList();
    }
    else if (sub == chat_cmd.SUB_GC_MODIFY_FRIEND_NOTIFY) {
    } 	//好友修改结果

    else if (sub == chat_cmd.SUB_GC_MODIFY_GROUP_NOTIFY) {
    }		//用户组修改结果

    else if (sub == chat_cmd.SUB_GC_UPDATE_COORDINATE_NOTIFY) { //更新坐标
        var cmd_table = ExternalFun.read_netdata(chat_cmd.CMD_GC_Update_CoordinateNotify, pData);
        cc.log(cmd_table + "CMD_GC_Update_CoordinateNotify" + 6);
        GlobalUserItem.bUpdateCoordinate = true;
        if (this.m_bQueryNearUserInfo) {
            this.m_bQueryNearUserInfo = false;
            this.sendQueryNearUser();
        }
    }
    else if (sub == chat_cmd.SUB_GC_GET_NEARUSER_RESULT) {		//附近结果
        var cmd_table = ExternalFun.read_netdata(chat_cmd.CMD_GC_Get_NearuserResult, pData);
        var tablist = {};
        for (var i = 0; i < cmd_table.cbUserCount - 1; i++) {
            var nearuser = ExternalFun.read_netdata(chat_cmd.tagNearUserInfo, pData);
            //过滤自己
            if (nearuser.dwUserID != GlobalUserItem.dwUserID)
                tablist.push(nearuser);
        }
        //刷新列表
        if (null != this.m_viewLayer && null != this.m_viewLayer.refreshNearFriendList)
            this.m_viewLayer.refreshNearFriendList(tablist);
    }
    else if (sub == chat_cmd.SUB_GC_QUERY_NEARUSER_RESULT) { 	//查询结果
        var cmd_table = ExternalFun.read_netdata(chat_cmd.CMD_GC_Query_NearuserResult, pData);
        cc.log(cmd_table + "CMD_GC_Query_NearuserResult" + 6);
        if (1 == cmd_table.cbUserCount) {
            //通知更新
            // var eventListener = cc.EventCustom().new(yl.RY_NEARUSER_NOTIFY);
            // eventListener.msg = cmd_table.NearUserInfo;
            // cc.director.getEventDispatcher().dispatchEvent(eventListener);

            var eventListener = new cc.EventCustom(yl.RY_NEARUSER_NOTIFY);
            eventListener.msg = cmd_table.NearUserInfo;
            cc.eventManager.dispatchEvent(eventListener);
        }
    }
    else if (sub == chat_cmd.SUB_GC_QUERY_DISTANCE_RESULT) {
        var cmd_table = ExternalFun.read_netdata(chat_cmd.CMD_GC_Query_NearuserResult, pData);
        cc.log(cmd_table + "CMD_GC_Query_NearuserResult" + 6)
        if (1 == cmd_table.cbUserCount) {
            //通知更新
            var eventListener = cc.EventCustom.new(yl.RY_DISTANCE);
            eventListener.msg = cmd_table.NearUserInfo;
            cc.director.getEventDispatcher().dispatchEvent(eventListener);
        }
    }
    else if (sub == chat_cmd.SUB_GC_QUERY_NEARUSER_ECHO
        || sub == chat_cmd.SUB_GC_UPDATE_COORDINATE_ECHO) {  	//坐标更新反馈
        var data_tab = ExternalFun.read_netdata(chat_cmd.CMD_GC_ECHO, pData);
        cc.log(data_tab.szDescribeString);
        this.dismissPopWait();
    }
    else if (sub == chat_cmd.SUB_GC_USER_SHARE_NOTIFY) {		//分享通知
        var notify = ExternalFun.read_netdata(chat_cmd.CMD_GC_UserShareNotify, pData);
        cc.log(notify + "notify" + 6);
        this.insertUserMsg(notify.dwSenderID, notify.dwSenderID, GlobalUserItem.dwUserID, notify.szMessageContent);
        netdata = notify;
        //下载图片
        if (typeof notify.szShareImageAddr == "string" && "" != notify.szShareImageAddr) {
            var path = this.downloadShareImage(notify.szShareImageAddr);
            this.insertUserMsg(notify.dwSenderID, notify.dwSenderID, GlobalUserItem.dwUserID, path, true);
        }
        if (null != this.m_viewLayer && null != this.m_viewLayer.messageNotify)
            this.m_viewLayer.messageNotify(notify);
    }
    else 														//所有反馈处理/操作结果处理
        this.showEcho(sub, pData);

    return netdata;
};

//显示反馈
FriendMgr.prototype.showEcho = function(sub,pData) {
    if (sub == chat_cmd.SUB_GC_OPERATE_SUCCESS) { 			//操作成功
        var data_tab = ExternalFun.read_netdata(chat_cmd.CMD_GC_OperateSuccess, pData);
        cc.log(data_tab + "success");
    }
    else if (sub == chat_cmd.SUB_GP_OPERATE_FAILURE) { 		//操作失败
        var data_tab = ExternalFun.read_netdata(chat_cmd.CMD_GC_OperateFailure, pData);
        cc.log(data_tab + "fail");
        var runScene = cc.director.getRunningScene();
        if (null != runScene)
            showToast(runScene, data_tab.szDescribeString, 2);
        else 													//通用反馈
            this.showFriendEcho(sub, pData);
    }
};

FriendMgr.prototype.showFriendEcho = function(sub, pData) {
    var data_tab = ExternalFun.read_netdata(chat_cmd.CMD_GC_ECHO, pData);
    cc.log(data_tab.szDescribeString);
    if (data_tab.lErrorCode != 0 && null != this.m_viewLayer)
        showToast(this.m_viewLayer, data_tab.szDescribeString, 2);

    //更新喇叭库存
    if (sub == chat_cmd.SUB_GC_TRUMPET_ECHO && data_tab.lErrorCode == 0) {
        GlobalUserItem.nLargeTrumpetCount = GlobalUserItem.nLargeTrumpetCount - 1;
        GlobalUserItem.nLargeTrumpetCount = (GlobalUserItem.nLargeTrumpetCount < 0) && 0 || GlobalUserItem.nLargeTrumpetCount;

        //通知数量变更
        var event = cc.EventCustom.new(yl.TRUMPET_COUNT_UPDATE_NOTIFY);
        cc.director.getEventDispatcher().dispatchEvent(event);
    }
};

FriendMgr.prototype.contentAndLogin = function() {
    var self = this;
    this.m_Socket = ClientSocket.createSocket(function (pData) {
        self.onSocketCallBack(pData);
    });

    if (this.m_Socket.connectSocket(yl.LOGONSERVER, yl.FRIENDPORT, yl.VALIDATE)) {
        //todo
    }
};

//好友登陆
FriendMgr.prototype.login = function() {
    var friendData = new Cmd_Data();
    friendData.setcmdinfo(chat_cmd.MDM_GC_LOGON, chat_cmd.SUB_GC_MB_LOGON_USERID);
    friendData.pushdword(GlobalUserItem.dwUserID);
    friendData.pushstring(md5(GlobalUserItem.szPassword).toUpperCase(), yl.LEN_PASSWORD);
    friendData.pushstring("手机型号", chat_cmd.LEN_PHONE_MODE);

    if (!this.sendSocketData(friendData)) {
        cc.log("FriendMgr.login 登录好友系统失败！！！");
        return;
    }

    //配置自己好友文件存储目录 TODO : Kil
    // MYFRIEND_FILE_PATH = FRIEND_PATH + GlobalUserItem.dwUserID + "/";
    // if (false == jsb.fileUtils().isDirectoryExist(MYFRIEND_FILE_PATH))
    //     jsb.fileUtils().createDirectory(MYFRIEND_FILE_PATH);
};

//发送聊天
FriendMgr.prototype.sendMessageFriend = function(userTab) {
    //cc.log (userTab, "===== FriendMgr.sendMessageFriend======");

    var sendMsgData = new Cmd_Data();
    sendMsgData.setcmdinfo(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_USER_CHAT);
    sendMsgData.pushdword(userTab.dwUserID);
    sendMsgData.pushdword(userTab.dwTargetUserID);
    sendMsgData.pushdword(userTab.dwFontColor);
    sendMsgData.pushbyte(userTab.cbFontSize);
    sendMsgData.pushbyte(userTab.cbFontAttri);
    sendMsgData.pushstring(userTab.szFontName, chat_cmd.LEN_FONT_NAME);
    sendMsgData.pushstring(userTab.szMessageContent, chat_cmd.LEN_MESSAGE_CONTENT);

    if (!this.sendSocketData(sendMsgData))
        cc.log("发送聊天消息失败！");
    else {
        this.insertUserMsg(userTab.dwTargetUserID, userTab.dwUserID, userTab.dwTargetUserID, userTab.szMessageContent);

        if (null != this.m_viewLayer && null != this.m_viewLayer.messageNotify)
            this.m_viewLayer.messageNotify(notify);
    }
};

//添加好友/申请好友
FriendMgr.prototype.sendAddFriend = function(userTab,func) {
    var addFriendData = new Cmd_Data();
    addFriendData.setcmdinfo(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_APPLYFOR_FRIEND);
    addFriendData.pushdword(userTab.dwUserID);
    addFriendData.pushdword(userTab.dwFriendID);
    addFriendData.pushbyte(userTab.cbGroupID);

    if (!this.sendSocketData(addFriendData)) {
        cc.log("发送添加好友失败！");
        func(false);
    }
    else
        func(true);
};

//回应好友/好友回应
FriendMgr.prototype.sendRespondFriend = function(userTab,notifyId) {
    cc.log(userTab, "=========");

    var respondData = new Cmd_Data();
    respondData.setcmdinfo(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_RESPOND_FRIEND);
    respondData.pushdword(userTab.dwUserID);
    respondData.pushdword(userTab.dwRequestID);
    respondData.pushbyte(0);
    respondData.pushbyte(0);
    respondData.pushbool(userTab.bAccepted);

    if (!this.sendSocketData(respondData))
        cc.log("发送回应好友失败！");
    else {
        this.markFriendNotifyRead(notifyId);
        if (null != this.m_viewLayer && null != this.m_viewLayer.updateNotifyList)
            this.m_viewLayer.updateNotifyList();
    }
};

//查找好友/查找用户
FriendMgr.prototype.sendSearchFriend = function(dwuserid) {
    cc.log("查找 " + dwuserid);
    var searchData = ExternalFun.create_netdata(chat_cmd.CMD_GC_SearchByGameID); //Cmd_Data:create(chat_cmd.CMD_GC_SearchByGameID);
    searchData.setcmdinfo(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_SEARCH_USER);
    searchData.pushdword(dwuserid);

    if (!this.sendSocketData(searchData))
        cc.log("发送查找好友失败！");
};

//邀请游戏
FriendMgr.prototype.sendInviteGame = function(msgTab) {
    var sendMsgData = ExternalFun.create_netdata(chat_cmd.CMD_GC_InviteGame);
    sendMsgData.setcmdinfo(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_INVITE_GAME);
    sendMsgData.pushdword(GlobalUserItem.dwUserID);
    sendMsgData.pushdword(msgTab.dwInvitedUserID);
    sendMsgData.pushword(msgTab.wKindID);
    sendMsgData.pushword(msgTab.wServerID);
    sendMsgData.pushword(msgTab.wTableID);
    sendMsgData.pushstring(msgTab.szInviteMsg, 128);

    if (!this.sendSocketData(sendMsgData)) {
        cc.log("发送邀请失败！");
        // sendMsgData.retain();
        this.m_sendCache.push(sendMsgData);
    }
    else
    //内容
        this.insertUserMsg(msgTab.dwInvitedUserID, GlobalUserItem.dwUserID, msgTab.dwInvitedUserID, msgTab.szInviteMsg);
};

//私人房邀请游戏
FriendMgr.prototype.sendInvitePrivateGame = function(msgTab) {
    var sendMsgData = ExternalFun.create_netdata(chat_cmd.CMD_GC_InvitePersonalGame);
    sendMsgData.setcmdinfo(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_INVITE_PERSONAL);
    sendMsgData.pushdword(GlobalUserItem.dwUserID);
    sendMsgData.pushdword(msgTab.dwInvitedUserID);
    sendMsgData.pushword(msgTab.wKindID);
    sendMsgData.pushdword(msgTab.wServerNumber);
    sendMsgData.pushword(msgTab.wTableID);
    sendMsgData.pushstring(msgTab.szInviteMsg, 128);

    if (!this.sendSocketData(sendMsgData)) {
        cc.log("发送邀请失败！");
        // sendMsgData.retain();
        this.m_sendCache.push(sendMsgData);
    }
    else {
        //内容
        this.insertUserMsg(msgTab.dwInvitedUserID, GlobalUserItem.dwUserID, msgTab.dwInvitedUserID, msgTab.szInviteMsg);
        return true;
    }
};

//发送喇叭/用户喇叭
FriendMgr.prototype.sendTrupmet = function( msgTab ) {
    cc.log(msgTab, "===== FriendMgr.sendTrupmet======");

    var sendMsgData = ExternalFun.create_netdata(chat_cmd.CMD_GC_Trumpet_C);
    sendMsgData.setcmdinfo(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_TRUMPET);
    sendMsgData.pushword(306);
    sendMsgData.pushdword(msgTab.dwSenderID);
    sendMsgData.pushdword(msgTab.dwFontColor);
    sendMsgData.pushstring(msgTab.szNickName, yl.LEN_NICKNAME);
    sendMsgData.pushstring(msgTab.szMessageContent, chat_cmd.LEN_MESSAGE_CONTENT);

    if (!this.sendSocketData(sendMsgData)) {
        cc.log("发送喇叭失败！");
        // sendMsgData.retain();
        this.m_sendCache.push(sendMsgData);
    }
};

//删除好友
FriendMgr.prototype.sendDeleteFriend = function(friendid, groupid) {
    var sendMsgData = ExternalFun.create_netdata(chat_cmd.CMD_GC_DeleteFriend);
    sendMsgData.setcmdinfo(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_DELETE_FRIEND);
    sendMsgData.pushdword(GlobalUserItem.dwUserID);
    sendMsgData.pushdword(friendid);
    sendMsgData.pushbyte(groupid);

    if (!this.sendSocketData(sendMsgData)) {
        cc.log("发送删除失败！");
        // sendMsgData.retain();
        this.m_sendCache.push(sendMsgData);
    }
};
//发送更新坐标
FriendMgr.prototype.sendUpdateCoordinate = function(lLongitude, lLatitude) {
    var sendMsgData = ExternalFun.create_netdata(chat_cmd.CMD_GC_Update_Coordinate);
    sendMsgData.setcmdinfo(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_UPDATE_COORDINATE);
    sendMsgData.pushdword(GlobalUserItem.dwUserID);
    sendMsgData.pushdouble(lLongitude);
    sendMsgData.pushdouble(lLatitude);

    if (!this.sendSocketData(sendMsgData)) {
        cc.log("发送更新坐标失败！");
        // sendMsgData.retain();
        this.m_sendCache.push(sendMsgData);
    }
};

//查询附近好友
FriendMgr.prototype.queryNearUser = function() {
    this.m_bRequestUpdateLocation = true;
    this.m_bQueryNearUserInfo = true;
    GlobalUserItem.bUpdateCoordinate = false;
    this.showPopWait();
    this.getLocationInfo();
};

//发送查询附近
FriendMgr.prototype.sendQueryNearUser = function() {
    var sendMsgData = ExternalFun.create_netdata(chat_cmd.CMD_GC_Get_Nearuser);
    sendMsgData.setcmdinfo(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_GET_NEARUSER);
    sendMsgData.pushdword(GlobalUserItem.dwUserID);
    sendMsgData.pushdouble(GlobalUserItem.tabCoordinate.lo);
    sendMsgData.pushdouble(GlobalUserItem.tabCoordinate.la);
    if (!this.sendSocketData(sendMsgData)) {
        cc.log("发送查询附近失败！");
        // sendMsgData.retain();
        this.m_sendCache.push(sendMsgData);
    }
};

//发送指定用户查询
FriendMgr.prototype.sendQueryUserLocation = function( dwTargetUserID ) {
    cc.log("FriendMgr.sendQueryUserLocation");
    var sendMsgData = ExternalFun.create_netdata(chat_cmd.CMD_GC_Query_Nearuser);
    sendMsgData.setcmdinfo(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_QUERY_NEARUSER);
    sendMsgData.pushdword(GlobalUserItem.dwUserID);
    sendMsgData.pushdword(dwTargetUserID);
    if (!this.sendSocketData(sendMsgData)) {
        cc.log("发送查询位置失败！");
        // sendMsgData.retain();
        this.m_sendCache.push(sendMsgData);
    }
};
//发送指定用户查询距离
FriendMgr.prototype.sendQueryUserDistance = function( dwTargetUserID ) {
    cc.log("FriendMgr.sendQueryUserDistance");
    var sendMsgData = ExternalFun.create_netdata(chat_cmd.CMD_GC_Query_Nearuser);
    sendMsgData.setcmdinfo(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_QUERY_DISTANCE);
    sendMsgData.pushdword(GlobalUserItem.dwUserID);
    sendMsgData.pushdword(dwTargetUserID);
    if (!this.sendSocketData(sendMsgData)) {
        cc.log("发送查询位置失败！");
        // sendMsgData.retain();
        this.m_sendCache.push(sendMsgData);
    }
};

//发送分享
FriendMgr.prototype.sendShareMessage = function(msgTab) {
    var sendMsgData = ExternalFun.create_netdata(chat_cmd.CMD_GC_UserShare);
    sendMsgData.setcmdinfo(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_USER_SHARE);
    sendMsgData.pushdword(GlobalUserItem.dwUserID);
    sendMsgData.pushdword(msgTab.dwSharedUserID);
    sendMsgData.pushstring(msgTab.szShareImageAddr, chat_cmd.LEN_MESSAGE_CONTENT);
    sendMsgData.pushstring(msgTab.szMessageContent, chat_cmd.LEN_MESSAGE_CONTENT);

    if (!this.sendSocketData(sendMsgData)) {
        cc.log("发送分享失败！");
        //sendMsgData.retain()
        //table.insert(this.m_sendCache, sendMsgData)
        return false;
    }
    else {
        //内容
        this.insertUserMsg(msgTab.dwSharedUserID, GlobalUserItem.dwUserID, msgTab.dwSharedUserID, msgTab.szMessageContent);
        //图片
        this.insertUserMsg(msgTab.dwSharedUserID, GlobalUserItem.dwUserID, msgTab.dwSharedUserID, msgTab.szImagePath, true);
        return true;
    }
};

//发送数据
FriendMgr.prototype.sendSocketData = function(pData) {
    if (this.m_Socket == null)
        return false;
    //this.showPopWait()
    if (!this.m_Socket.sendData(pData)) {
        //this.dismissPopWait()
        this.onCloseSocket();
        return false;
    }
    return true;
};

//关闭网络
FriendMgr.prototype.onCloseSocket = function() {
    if (this.m_Socket) {
        // TODO :
        // this.m_Socket.relaseSocket();
        this.m_Socket = null;
    }

    //控制自动连接次数
    if (this.m_nAutoLoginCount > 0) {
        cc.log("auto connect");
        this.m_nAutoLoginCount = this.m_nAutoLoginCount - 1;
        this.m_nAutoLoginCount = (this.m_nAutoLoginCount >= 0) && this.m_nAutoLoginCount || 0;
        this.contentAndLogin();
    }
    else
        cc.log("auto connect ==> 0");
};

//判断是否有网络连接
FriendMgr.prototype.isConnected = function(){
	return null != this.m_Socket;
};

//重置并进行网络连接
FriendMgr.prototype.reSetAndLogin = function() {
    //聊天缓存
    this.m_tabChatCache[GlobalUserItem.dwUserID] = [];
    //通知缓存
    this.m_tabNoticeCache[GlobalUserItem.dwUserID] = [];

    this.m_nAutoLoginCount = 5;
    this.contentAndLogin();
    //获取位置
    this.getLocationInfo();
};

//重置并断开网络连接
FriendMgr.prototype.reSetAndDisconnect = function() {
    this.m_nAutoLoginCount = 0;
    this.onCloseSocket();
    //清空好友数据
    this.m_FriendTab = [];
    this.setCallBackDelegate(null, null);
    //清空缓存
    this.m_sendCache = [];
    GlobalUserItem.bUpdateCoordinate = false;
    this.m_bRequestUpdateLocation = false;
    this.m_bQueryNearUserInfo = false;
};

//处理缓存未发送消息
FriendMgr.prototype.popCacheMessage = function() {
    //处理未发送的缓存数据
    for (k in this.m_sendCache) {
        if (this.sendSocketData(v)) {
            cc.log("cache send");
            this.m_sendCache[k] = null;
        }
    }
};

////////////////
////好友数据处理
////////////////
//缓存游戏通知
FriendMgr.prototype.insertFriendNotify = function(Type,notifyTab) {
    // localStorage.setItem(MYFRIEND_FILE_PATH, "[]");

    var notifyObj;
    if(Type==2){
        notifyObj = notifyTab;
    } else {
        notifyObj = {};
        for(k in notifyTab){
            var v = notifyTab[k];
            notifyObj[k] = v;
        }
    }

    if(localStorage.getItem(MYFRIEND_FILE_PATH)) {
        var data = localStorage.getItem(MYFRIEND_FILE_PATH);
        var isExist = false;
        // try{
        	var dataTab = JSON.parse(data);
            for (var k = 0;k < dataTab.length;k++) {
                if (Type == dataTab[k].notifyType
                    && notifyObj.dwRequestID == dataTab[k].notify.dwRequestID
                    && (!dataTab[k].bRead)) {
                    isExist = true;
                    break;
                }
            }
		// }catch(e){
            //dataTab = [];
            if (!isExist) {
                //1:申请好友通知  2:回应通知  3：邀请通知
                dataTab.push({notifyType:Type, notify : notifyObj, notifyId : (new Date()).getTime(), bRead : false});

                //控制容量
                if (dataTab.length > MAX_CHAT_CACHE)
                	dataTab.splice(1,1);
                this.m_tabNoticeCache[GlobalUserItem.dwUserID] = dataTab;
                localStorage.setItem(MYFRIEND_FILE_PATH, JSON.stringify(dataTab));
                /* cc.fileUtils().writeStringToFile(JSON.stringify(dataTab), filePath);*/
            }
    } else {
        this.m_tabNoticeCache[GlobalUserItem.dwUserID] = [{notifyType:Type, notify : notifyObj, notifyId : (new Date()).getTime(), bRead : false}];
        localStorage.setItem(MYFRIEND_FILE_PATH, JSON.stringify(this.m_tabNoticeCache[GlobalUserItem.dwUserID]));
    }

};

//获取消息通知
FriendMgr.prototype.getFriendNotify = function(){
	var filePath = MYFRIEND_FILE_PATH + "FriendNotify.ry";
	if (null == this.m_tabNoticeCache[GlobalUserItem.dwUserID] ||
		this.m_tabNoticeCache[GlobalUserItem.dwUserID].length == 0) {

        this.m_tabNoticeCache[GlobalUserItem.dwUserID] = [];
            /*if (jsb.fileUtils().isFileExist(filePath)) {
                var data = jsb.fileUtils().getStringFromFile(filePath);*/
        if(localStorage.getItem(MYFRIEND_FILE_PATH)){
            var data = localStorage.getItem(MYFRIEND_FILE_PATH);
            var ok = false;
            try {
                var dataTab = JSON.parse(data);
                ok = true;
                this.m_tabNoticeCache[GlobalUserItem.dwUserID] = dataTab;
            }
            catch (err) {

            }
            if (true == ok)
                this.m_tabNoticeCache[GlobalUserItem.dwUserID] = dataTab;
        }

    }
	// table.sort(this.m_tabNoticeCache[GlobalUserItem.dwUserID], function(a, b)
	// 	if a.bRead == true && b.bRead == true)
	// 		return a.notifyId < b.notifyId
	// 	else
	// 		return (a.bRead == true)
	// 	end
	// end)
    cc.log("< === FriendNotify list in FriendMgr ===> : " + this.m_tabNoticeCache[GlobalUserItem.dwUserID]);
	if(this.m_tabNoticeCache[GlobalUserItem.dwUserID] == null){
        this.m_tabNoticeCache[GlobalUserItem.dwUserID] = [];
    }
    for(var i=0;i<this.m_tabNoticeCache[GlobalUserItem.dwUserID].length;i++){
        for(var j=i;j<this.m_tabNoticeCache[GlobalUserItem.dwUserID].length;j++){
            if(this.m_tabNoticeCache[GlobalUserItem.dwUserID][i].bRead == true && this.m_tabNoticeCache[GlobalUserItem.dwUserID][j].bRead == true) {
                if (this.m_tabNoticeCache[GlobalUserItem.dwUserID][i].notifyId > this.m_tabNoticeCache[GlobalUserItem.dwUserID][j].notifyId) {
                    var temp = this.m_tabNoticeCache[GlobalUserItem.dwUserID][i];
                    this.m_tabNoticeCache[GlobalUserItem.dwUserID][i] = this.m_tabNoticeCache[GlobalUserItem.dwUserID][j];
                    this.m_tabNoticeCache[GlobalUserItem.dwUserID][j] = temp;
                }
            }else{

            }
        }
    }
	return this.m_tabNoticeCache[GlobalUserItem.dwUserID];
};
//获取未读通知
FriendMgr.prototype.getUnReadNotify = function() {
    var list = this.getFriendNotify();
    var unread = [];
    for (var i = 0; i < list.length; i++) {
        if (null == list[i].bRead || false == list[i].bRead)
            unread.push(list[i]);
    }
    return unread;
};

//刪除消息通知
FriendMgr.prototype.deleteFriendNotify = function(notifyId) {
    var filePath = MYFRIEND_FILE_PATH + "FriendNotify.ry";

    if(localStorage.getItem(MYFRIEND_FILE_PATH)){
        var data = localStorage.getItem(MYFRIEND_FILE_PATH);
		try{
        	var dataTab = JSON.parse(data);
            var idx = 1;
            for (k in dataTab) {
                if (dataTab[k].notifyId == notifyId) {
                    idx = k;
                    break;
                }
            }
            //todo remove
			dataTab.splice(idx,1);
            this.m_tabNoticeCache[GlobalUserItem.dwUserID] = dataTab;
            // jsb.fileUtils().writeStringToFile(JSON.stringify(dataTab), filePath);
            localStorage.setItem(MYFRIEND_FILE_PATH, JSON.stringify(dataTab));
		}catch(e){

		}
    }
};

//标记消息已读
FriendMgr.prototype.markFriendNotifyRead = function(notifyId) {
    var filePath = MYFRIEND_FILE_PATH + "FriendNotify.ry";

    if(localStorage.getItem(MYFRIEND_FILE_PATH)){
        var data = localStorage.getItem(MYFRIEND_FILE_PATH);
        //todo pcall
        try{
            var dataTab = JSON.parse(data);
            for (k in dataTab) {
                var kkk = dataTab[k];
                var notifyIdofkkk = dataTab[k].notifyId;
                if (dataTab[k].notifyId == notifyId) {
                    dataTab[k].bRead = true;
                }

                //break
            }
            //table.remove(dataTab,index)
            this.m_tabNoticeCache[GlobalUserItem.dwUserID] = dataTab;
            // jsb.fileUtils().writeStringToFile(JSON.stringify(dataTab), filePath);
            localStorage.setItem(MYFRIEND_FILE_PATH, JSON.stringify(dataTab));
        }catch(e){
        }
    }
};

//删除好友
FriendMgr.prototype.deleteFriend = function(index) {
    this.m_FriendTab.splice(index,1);
    if (this.m_Delegate) {
        if (this.m_Delegate.refreshFriendList)
            this.m_Delegate.refreshFriendList();
    }
};

//获取缓存好友列表
FriendMgr.prototype.getFriendList = function() {
    return this.m_FriendTab || [];
};

//获取好友
FriendMgr.prototype.getFriendByID = function( dwID ) {
    for (k in this.m_FriendTab) {
        if (this.m_FriendTab[k].dwUserID == dwID)
            return this.m_FriendTab[k]
    }
    return null;
};

//获取好友信息
FriendMgr.prototype.getFriendInfoByID = function(dwUserID) {
    if (null == dwUserID)
        return;
    for (k in this.m_FriendTab) {
        if (this.m_FriendTab[k].dwUserID == dwUserID)
            return this.m_FriendTab[k];
    }
    return null;
};

//插入一条聊天信息
FriendMgr.prototype.insertUserMsg = function(userID, sendID, targetID, content, bImage) {
    var filePath = MYFRIEND_FILE_PATH + userID.toString() + ".ry";

    if (jsb.fileUtils().isFileExist(filePath)) {
        var data = jsb.fileUtils().getStringFromFile(filePath);
        try{
            var dataTab = JSON.parse(data);
            bImage = bImage || false;
            dataTab.push({dwSenderID : sendID, dwTargetUserID : targetID, szMessageContent : content, bImage : bImage});

            //控制容量
            if (dataTab.length > MAX_CHAT_CACHE) {
                var msg = dataTab[1];
                if (msg.bImage) {
                    cc.log("FriendMgr. removeChatImage");
                    jsb.fileUtils().removeFile(msg.szMessageContent);
                }
				dataTab.splice(1,1);
            }
            this.m_tabChatCache[GlobalUserItem.dwUserID][userID] = dataTab;
            jsb.fileUtils().writeStringToFile(JSON.stringify(dataTab), filePath);
            return;
		}catch(e){}
    }
    this.m_tabChatCache[GlobalUserItem.dwUserID][userID] = [{dwSenderID : sendID,dwTargetUserID : userID ,szMessageContent :content}];
    jsb.fileUtils().writeStringToFile(JSON.stringify(this.m_tabChatCache[GlobalUserItem.dwUserID][userID]), filePath);
};

//获取用户聊天信息记录
FriendMgr.prototype.getUserRecordMsg = function(userID) {
    if (null == this.m_tabChatCache[GlobalUserItem.dwUserID][userID] || 0 == this.m_tabChatCache[GlobalUserItem.dwUserID][userID].length) {
        var filePath = MYFRIEND_FILE_PATH + userID.toString() + ".ry";
        this.m_tabChatCache[GlobalUserItem.dwUserID][userID] = {};
        if (jsb.fileUtils().isFileExist(filePath)) {
            var data = jsb.fileUtils().getStringFromFile(filePath);
            try {
                var dataTab = JSON.parse(data);
                this.m_tabChatCache[GlobalUserItem.dwUserID][userID] = dataTab;
            }catch(e){}
        }
        return this.m_tabChatCache[GlobalUserItem.dwUserID][userID];
    }
};

//显示等待
FriendMgr.prototype.showPopWait = function(msg) {
    var runScene = cc.director.getRunningScene();
    if (null == this._popWait && null != runScene) {
        this._popWait = new PopWait();
        this._popWait.show(runScene, msg);
    }
};

//关闭等待
FriendMgr.prototype.dismissPopWait = function() {
    if (this._popWait) {
        this._popWait.dismiss();
        this._popWait = null;
    }
};

var IMAGE_DOWNLOAD_NOTIFY = "friend_image_download_notify";
//全局通知函数
var cc_exports_g_imageDownloadListener = function (ncode, msg, filename) {
    cc.log(msg);
    var event = cc.EventCustom.new(yl.RY_IMAGE_DOWNLOAD_NOTIFY);
    event.code = ncode;
    event.msg = msg;
    event.filename = filename;

    cc.director.getEventDispatcher().dispatchEvent(event);
};
//下载分享图片
FriendMgr.prototype.downloadShareImage = function( imageurl ) {
    var path = jsb.fileUtils.getWritablePath();
    var downloader = CurlAsset.createDownloader("g_imageDownloadListener", imageurl);
    if (false == jsb.fileUtils().isDirectoryExist(path))
        jsb.fileUtils().createDirectory(path);
    downloader.downloadFile(path, "/shareimage.png");
    return path + "/shareimage.png";
};

//TODO: from function insertUserMsg don't changed fileUtils yet! notice21
