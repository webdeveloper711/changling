/*
author. Li
 */
// var BaseFrame = appdf.req(appdf.CLIENT_SRC+"plaza.models-cc-cc.BaseFrame");
// var GameFrameEngine = class("GameFrameEngine",BaseFrame);
//
// var UserItem = appdf.req(appdf.CLIENT_SRC+"plaza.models-cc-cc.ClientUserItem");
// var game_cmd = appdf.req(appdf.HEADER_SRC + "CMD_GameServer");
// var ExternalFun = appdf.req(appdf.EXTERNAL_SRC + "ExternalFun");
// var GameChatLayer = appdf.req(appdf.CLIENT_SRC+"plaza.views.layer.game-cc.GameChatLayer");

function GameFrameEngine(view, callback) {
    BaseFrame.call(this, view, callback);
    this._kindID = 0;
    this._kindVersion = 0;

    // 短连接服务
    this._shotFrame = null;
    // 语音队列
    this._tabVoiceMsgQueue = {};
    this._bPlayVoiceRecord = false;
}

GameFrameEngine.prototype = Object.create(BaseFrame.prototype);
GameFrameEngine.prototype.constructor = GameFrameEngine;


GameFrameEngine.prototype.setKindInfo = function(id,version) {
    this._kindID = id;
    this._kindVersion = version;
    return this;
};

GameFrameEngine.prototype.onInitData = function() {

    //房间信息 以后转移
    this._wTableCount = 0;
    this._wChairCount = 0;
    this._wServerType = 0;
    this._dwServerRule = 0;
    this._UserList = [];
    this._tableUserList = [];
    this._tableStatus = [];
    this._delayEnter = false;

    this._wTableID = yl.INVALID_TABLE;
    this._wChairID = yl.INVALID_CHAIR;
    this._cbTableLock = 0;
    this._cbGameStatus = 0;
    this._cbAllowLookon = 0;
    this.bChangeDesk = false;
    this.bEnterAntiCheatRoom = false;		//进入防作弊房间
    GlobalUserItem.bWaitQuit = false;		// 退出等待
    this._tabVoiceMsgQueue = [];
    this._bPlayVoiceRecord = false;
    this._nPlayVoiceId = null;
};

GameFrameEngine.prototype.setEnterAntiCheatRoom = function( bEnter ) {
    this.bEnterAntiCheatRoom = bEnter;
};

//连接房间
GameFrameEngine.prototype.onLogonRoom = function(){
	this._roomInfo = GlobalUserItem.GetRoomInfo();

	if ( null == this._roomInfo && null!= this._callBack){
        this._callBack(-1, "获取房间信息失败！");
        return;
    }

    var ServerAddr = appdf.LOGONSERVER;
	// TODO: Currently Proxy server is running on only LOGONSERVER, will check with pangyu later
    /*if (this._roomInfo.szServerAddr == "127.0.0.1") {
        ServerAddr = appdf.LOGONSERVER;
    }
    else{
        ServerAddr = this._roomInfo.szServerAddr;
    }*/
    cc.log("登录房间：" + ServerAddr + "#" + (this._roomInfo.wServerPort + 1000));
    if (!this.onCreateSocket(ServerAddr, (this._roomInfo.wServerPort + 1000)) && null != this._callBack) {
        this._callBack(-1, "建立连接失败！");
    }
};
//连接结果
GameFrameEngine.prototype.onConnectCompeleted = function() {
    if (null != this._socket)
        this._socket.setdelaytime(0);
    cc.log("============GameFrameEngine onConnectCompeleted============");

    var dataBuffer = new Cmd_Data(213);

    //初始化参数
    this.onInitData();

    dataBuffer.setcmdinfo(yl.MDM_GR_LOGON, yl.SUB_GR_LOGON_MOBILE);
    dataBuffer.pushword(this._kindID);
    dataBuffer.pushdword(this._kindVersion);

    dataBuffer.pushbyte(yl.DEVICE_TYPE);
    dataBuffer.pushword(0x0011);
    dataBuffer.pushword(255);

    dataBuffer.pushdword(GlobalUserItem.dwUserID);
    dataBuffer.pushstring(GlobalUserItem.szDynamicPass, 33);
    dataBuffer.pushstring(GlobalUserItem.szRoomPasswd, 33);
    dataBuffer.pushstring(GlobalUserItem.szMachine, 33);

    if (!this.sendSocketData(dataBuffer) && null != this._callBack)
        this._callBack(-1, "发送登录失败！");
};

//网络信息
GameFrameEngine.prototype.onSocketEvent = function(main,sub,dataBuffer) {
    //登录信息
    cc.log("=============GameFrameEngin onsocketevent called ============" + "main is " + main + "  sub is " + sub  );
    if (main == yl.MDM_GR_LOGON)
        this.onSocketLogonEvent(sub, dataBuffer);
    //配置信息
    else if (main == yl.MDM_GR_CONFIG)
        this.onSocketConfigEvent(sub, dataBuffer);
    //用户信息
    else if (main == yl.MDM_GR_USER)
        this.onSocketUserEvent(sub, dataBuffer);
    //状态信息
    else if (main == yl.MDM_GR_STATUS)
        this.onSocketStatusEvent(sub, dataBuffer);
    else if (main == yl.MDM_GF_FRAME)
        this.onSocketFrameEvent(sub, dataBuffer);
    else if (main == yl.MDM_GF_GAME) {
        if (this._viewFrame && this._viewFrame.onEventGameMessage)
            this._viewFrame.onEventGameMessage(sub, dataBuffer);
    }
    else if (main == game_cmd.MDM_GR_INSURE) {
        if (this._viewFrame && this._viewFrame.onSocketInsureEvent)
            this._viewFrame.onSocketInsureEvent(sub, dataBuffer);

        // 短连接服务
        if (null != this._shotFrame && null != this._shotFrame.onGameSocketEvent)
            this._shotFrame.onGameSocketEvent(main, sub, dataBuffer);
        //else if( main == game_cmd.MDM_GR_TASK 
        // || main == game_cmd.MDM_GR_PROPERTY 
        // then
        // 短连接服务
        // if (null!= this._shotFrame && null!= this._shotFrame.onGameSocketEvent)
        // 	this._shotFrame.onGameSocketEvent(main,sub,dataBuffer)
        // end]]
    }
    else {
        // 短连接服务
        if (null != this._shotFrame && null != this._shotFrame.onGameSocketEvent)
            this._shotFrame.onGameSocketEvent(main, sub, dataBuffer);
        // 私人房
        if (PriRoom)
            PriRoom.getInstance().getNetFrame().onGameSocketEvent(main, sub, dataBuffer);
    }
};

GameFrameEngine.prototype.onSocketLogonEvent = function(sub,dataBuffer) {
    //登录完成
    if (sub == game_cmd.SUB_GR_LOGON_FINISH)
        this.onSocketLogonFinish();
    // 登录成功
    else if (sub == game_cmd.SUB_GR_LOGON_SUCCESS) {
        var cmd_table = ExternalFun.read_netdata(game_cmd.CMD_GR_LogonSuccess, dataBuffer);
        cc.log(cmd_table + "CMD_GR_LogonSuccess" + 4);
    }
    //登录失败
    else if (sub == game_cmd.SUB_GR_LOGON_FAILURE) {
        var errorCode = dataBuffer.readint();
        var msg = dataBuffer.readstring();
        cc.log("登录房间失败." + errorCode + "#" + msg);
        this.onCloseSocket();
        if (null != this._callBack)
            this._callBack(-1, "登录房间失败." + errorCode + "#" + msg);
    }
    //升级提示
    else if (sub == game_cmd.SUB_GR_UPDATE_NOTIFY) {
        if (null != this._callBack)
            this._callBack(-1, "版本信息错误");
    }
};

//登录完成
GameFrameEngine.prototype.onSocketLogonFinish = function() {
    if (this._delayEnter == true)
        return;

    var myUserItem = this.GetMeUserItem();
    if (null == myUserItem && null != this._callBack) {
        this._callBack(-1, "获取自己信息失败！");
        return;
    }
    if (GlobalUserItem.bPrivateRoom && PriRoom)
        PriRoom.getInstance().onLoginPriRoomFinish();
    else {
        if (this._wTableID != yl.INVALID_TABLE) {
            if (this._viewFrame && this._viewFrame.onEnterTable)
                this._viewFrame.onEnterTable();
            //showToast(this._viewFrame,"找到游戏桌子，正在获取场景中+.",1)
            this.SendGameOption();
        }
        else {
        }
        //[[if (this._viewFrame && this._viewFrame.onEnterRoom)
        // 	this._viewFrame.onEnterRoom()
        // end]]
    }
};

//房间配置
GameFrameEngine.prototype.onSocketConfigEvent = function(sub,dataBuffer) {
    //房间配置
    if (sub == yl.SUB_GR_CONFIG_SERVER) {
        this._wTableCount = dataBuffer.readword();
        this._wChairCount = dataBuffer.readword();
        this._wServerType = dataBuffer.readword();
        this._dwServerRule = dataBuffer.readdword();
        GlobalUserItem.dwServerRule = this._dwServerRule;

        //是否进入防作弊
        this.setEnterAntiCheatRoom(GlobalUserItem.isAntiCheat());
        cc.log("房间配置[table." + this._wTableCount + "][chair." + this._wChairCount + "][type." + this._wServerType + "][rule." + this._dwServerRule + "]");
        //配置完成
    }
    else{
        if (sub == yl.SUB_GR_CONFIG_FINISH) {
        }
    }
};

GameFrameEngine.prototype.GetTableCount = function() {
    return this._wTableCount;
};

GameFrameEngine.prototype.GetChairCount = function() {
    return this._wChairCount;
};

GameFrameEngine.prototype.GetServerType = function() {
    return this._wServerType;
};

GameFrameEngine.prototype.GetServerRule = function() {
    return this._dwServerRule;
};

//房间取款准许
GameFrameEngine.prototype.OnRoomAllowBankTake = function() {
    return bit._and(this._dwServerRule, 0x00010000) != 0;
};

//房间存款准许
GameFrameEngine.prototype.OnRoomAllowBankSave = function() {
    return bit._and(this._dwServerRule, 0x00040000) != 0;
};

//游戏取款准许
GameFrameEngine.prototype.OnGameAllowBankTake = function() {
    return bit._and(this._dwServerRule, 0x00020000) != 0;
};
//游戏存款准许
GameFrameEngine.prototype.OnGameAllowBankSave = function() {
    return bit._and(this._dwServerRule, 0x00080000) != 0;
};

GameFrameEngine.prototype.IsAllowAvertCheatMode = function() {
    return bit._and(this._dwServerRule, yl.SR_ALLOW_AVERT_CHEAT_MODE) != 0;
};

//是否更新大厅金币
GameFrameEngine.prototype.IsAllowPlazzScoreChange = function() {
    return (this._wServerType != yl.GAME_GENRE_SCORE) && (this._wServerType != yl.GAME_GENRE_EDUCATE);
};

//游戏赠送准许
GameFrameEngine.prototype.OnGameAllowBankTransfer = function() {
    return false;
};

//用户信息
GameFrameEngine.prototype.onSocketUserEvent = function(sub,dataBuffer) {
    //等待分配
    if (sub == game_cmd.SUB_GR_USER_WAIT_DISTRIBUTE)
    //showToast(this._viewFrame, "正在进行分组,请稍后+.", 3)
        cc.log("正在进行分组,请稍后+.");
    //用户进入
    else if (sub == yl.SUB_GR_USER_ENTER)
        this.onSocketUserEnter(dataBuffer);
    //用户积分
    else if (sub == yl.SUB_GR_USER_SCORE)
        this.onSocketUserScore(dataBuffer);
    //用户状态
    else if (sub == yl.SUB_GR_USER_STATUS)
        this.onSocketUserStatus(dataBuffer);
    //请求失败
    else if (sub == yl.SUB_GR_REQUEST_FAILURE)
        this.onSocketReQuestFailure(dataBuffer);
};
//用户进入
GameFrameEngine.prototype.onSocketUserEnter = function(dataBuffer) {
    var userItem = new ClientUserItem();

    userItem.dwGameID = dataBuffer.readdword();
    userItem.dwUserID = dataBuffer.readdword();

    //自己判断
    var bMythisInfo = (userItem.dwUserID == GlobalUserItem.dwUserID);

    //非法过滤
    if (null == this._UserList[GlobalUserItem.dwUserID]) {
        if (bMythisInfo == false) {
            cc.log("还未有自己信息，不处理其他用户信息");
            return;
        }
    }
    else {
        if (bMythisInfo == true) {
            cc.log("GameFrameEngine.prototype.onSocketUserEnter 已有自己信息，不再次处理自己信息");
            return;
        }
    }

    //读取信息
    userItem.wFaceID = dataBuffer.readword();
    userItem.dwCustomID = dataBuffer.readdword();

    userItem.cbGender = dataBuffer.readbyte();
    userItem.cbMemberOrder = dataBuffer.readbyte();

    userItem.wTableID = dataBuffer.readword();
    userItem.wChairID = dataBuffer.readword();
    userItem.cbUserStatus = dataBuffer.readbyte();

    userItem.lScore = dataBuffer.readscore();
    userItem.lIngot = dataBuffer.readscore();
    userItem.lRoomCrad = dataBuffer.readscore();
    userItem.dBeans = dataBuffer.readdouble();

    userItem.dwWinCount = dataBuffer.readdword();
    userItem.dwLostCount = dataBuffer.readdword();
    userItem.dwDrawCount = dataBuffer.readdword();
    userItem.dwFleeCount = dataBuffer.readdword();
    userItem.dwExperience = dataBuffer.readdword();
    userItem.lIntegralCount = dataBuffer.readscore();
    userItem.dwAgentID = dataBuffer.readdword();
    userItem.dwIpAddress = dataBuffer.readdword(); // ip地址
    userItem.dwDistance = null;					 // 距离

    var curlen = dataBuffer.getcurlen();
    var datalen = dataBuffer.getlen();
    var tmpSize;
    var tmpCmd;
    while (curlen < datalen) {
        tmpSize = dataBuffer.readword();
        tmpCmd = dataBuffer.readword();
        if (tmpSize == null || tmpCmd == null)
            break;
        if (tmpCmd == yl.DTP_GR_NICK_NAME) {
            userItem.szNickName = dataBuffer.readstring(tmpSize / 2);

            if (null == userItem.szNickName || (this.IsAllowAvertCheatMode() == true && userItem.dwUserID != GlobalUserItem.dwUserID))
                userItem.szNickName = "游戏玩家";
        }
        else if (tmpCmd == yl.DTP_GR_UNDER_WRITE) {
            userItem.szSign = dataBuffer.readstring(tmpSize / 2);
            if (null == userItem.szSign || (this.IsAllowAvertCheatMode() == true && userItem.dwUserID != GlobalUserItem.dwUserID))
                userItem.szSign = "此人很懒，没有签名";
        }
        else if (tmpCmd == 0)
            break;
        else {
            for (var i = 0; i < tmpSize; i++) {
                if (dataBuffer.readbyte() == null)
                    break;
            }
        }
        curlen = dataBuffer.getcurlen();
    }
    cc.log("GameFrameEngine enter ==> " + userItem.szNickName + userItem.dwIpAddress + userItem.dwDistance);

    // userItem.testlog()

    //添加/更新到缓存
    var bAdded;
    var item = this._UserList[userItem.dwUserID];
    if (item != null) {
        item.dwGameID = userItem.dwGameID;
        item.lScore = userItem.lScore;
        item.lIngot = userItem.lIngot;
        item.dBeans = userItem.dBeans;
        item.wFaceID = userItem.wFaceID;
        item.dwCustomID = userItem.dwCustomID;
        item.cbGender = userItem.cbGender;
        item.cbMemberOrder = userItem.cbMemberOrder;
        item.wTableID = userItem.wTableID;
        item.wChairID = userItem.wChairID;
        item.cbUserStatus = userItem.cbUserStatus;
        item.dwWinCount = userItem.dwWinCount;
        item.dwLostCount = userItem.dwLostCount;
        item.dwDrawCount = userItem.dwDrawCount;
        item.dwFleeCount = userItem.dwFleeCount;
        item.dwExperience = userItem.dwExperience;
        item.szNickName = userItem.szNickName;
        bAdded = true;
    }

    if (!bAdded)
        this._UserList[userItem.dwUserID] = userItem;

    //记录自己桌椅号
    if (userItem.dwUserID == GlobalUserItem.dwUserID) {
        this._wTableID = userItem.wTableID;
        this._wChairID = userItem.wChairID;
    }

    if (userItem.wTableID != yl.INVALID_TABLE && userItem.cbUserStatus != yl.US_LOOKON) {
        this.onUpDataTableUser(userItem.wTableID, userItem.wChairID, userItem);

        if (this._viewFrame && this._viewFrame.onEventUserEnter)
            this._viewFrame.onEventUserEnter(userItem.wTableID, userItem.wChairID, userItem);
    }

    if (bMythisInfo == true && this._delayEnter == true)
        this._delayEnter = false;
    //this.onSocketLogonFinish()

};
//用户积分
GameFrameEngine.prototype.onSocketUserScore = function(dataBuffer) {

    var dwUserID = dataBuffer.readdword();

    var item = this._UserList[dwUserID];
    if (item != null) {
        //更新数据
        item.lScore = dataBuffer.readscore();
        item.dBeans = dataBuffer.readdouble();

        item.dwWinCount = dataBuffer.readdword();
        item.dwLostCount = dataBuffer.readdword();
        item.dwDrawCount = dataBuffer.readdword();
        item.dwFleeCount = dataBuffer.readdword();

        item.dwExperience = dataBuffer.readdword();

        cc.log("更新用户[" + dwUserID + "][" + item.szNickName + "][" + item.lScore + "]");

        //自己信息
        if (item.dwUserID == GlobalUserItem.dwUserID && this.IsAllowPlazzScoreChange()) {
            cc.log("更新金币");
            GlobalUserItem.lUserScore = item.lScore;
            GlobalUserItem.dUserBeans = item.dBeans;
        }

        //通知更新界面
        if (this._wTableID != yl.INVALID_TABLE && this._viewFrame && this._viewFrame.onEventUserScore)
            this._viewFrame.onEventUserScore(item);
    }
};

//用户状态
GameFrameEngine.prototype.onSocketUserStatus = function(dataBuffer) {

    //读取信息
    var dwUserID = dataBuffer.readdword();
    var newstatus = [];
    newstatus.wTableID = dataBuffer.readword();
    newstatus.wChairID = dataBuffer.readword();
    newstatus.cbUserStatus = dataBuffer.readbyte();

    //过滤观看
    if (newstatus.cbUserStatus == yl.US_LOOKON)
        return;

    //获取自己
    var myUserItem = this.GetMeUserItem();

    //未找到自己
    if (null == myUserItem) {
        cc.log(" GameFrameEngine.prototype.未找到自己, 查询自己信息 ");
        if (newstatus.wTableID != yl.INVALID_TABLE) {
            this._delayEnter = true;
            this.QueryUserInfo(newstatus.wTableID, newstatus.wChairID);
            return;
        }

        //非法信息
        this.onCloseSocket();
        if (null != this._callBack)
            this._callBack(-1, "用户信息获取不正确,请重新登录！");
        return;
    }

    //自己判断
    var bMythisInfo = (dwUserID == myUserItem.dwUserID);

    var useritem = this._UserList[dwUserID];

    //找不到用户
    if (useritem == null) {
        //当前桌子用户
        if (newstatus.wTableID != yl.INVALID_TABLE) {
            //虚拟信息
            var newitem = new ClientUserItem();
            newitem.szNickName = "游戏玩家";
            newitem.dwUserID = dwUserID;
            newitem.cbUserStatus = cbUserStatus;
            newitem.wTableID = newstatus.wTableID;
            newitem.wChairID = newstatus.wChairID;

            this._UserList[dwUserID] = newitem;
            this.onUpDataTableUser(newitem.wTableID, newitem.wChairID, newitem);
            //发送查询
            this.QueryUserInfo(newstatus.wTableID, newstatus.wChairID);
        }
        return;
    }

    // 记录旧状态
    var oldstatus = [];
    oldstatus.wTableID = useritem.wTableID;
    oldstatus.wChairID = useritem.wChairID;
    oldstatus.cbUserStatus = useritem.cbUserStatus;
    //更新信息
    useritem.cbUserStatus = newstatus.cbUserStatus;
    useritem.wTableID = newstatus.wTableID;
    useritem.wChairID = newstatus.wChairID;

    //清除旧桌子椅子记录
    if (oldstatus.wTableID != yl.INVALID_TABLE) {
        //新旧桌子不同 新旧椅子不同
        if ((oldstatus.wTableID != newstatus.wTableID) || (oldstatus.wChairID != newstatus.wChairID))
            this.onUpDataTableUser(oldstatus.wTableID, oldstatus.wChairID, null);
    }
    //新桌子记录
    if (newstatus.wTableID != yl.INVALID_TABLE)
        this.onUpDataTableUser(newstatus.wTableID, newstatus.wChairID, useritem);

    //自己状态
    if (bMythisInfo == true) {

        this._wTableID = newstatus.wTableID;
        this._wChairID = newstatus.wChairID;
        //离开
        if (newstatus.cbUserStatus == yl.US_NULL) {
            cc.log("自己离开");
            if (this._viewFrame && this._viewFrame.onExitRoom && !GlobalUserItem.bWaitQuit)
                this._viewFrame.onExitRoom();
        }
        //起立
        else if (newstatus.cbUserStatus == yl.US_FREE && oldstatus.cbUserStatus > yl.US_FREE) {
            cc.log("自己起立");
            if (this._viewFrame && this._viewFrame.onExitTable && !GlobalUserItem.bWaitQuit) {
                if (this.bEnterAntiCheatRoom) {
                    cc.log("防作弊换桌");
                    this.OnResetGameEngine();
                }
                else if (!this.bChangeDesk)
                    this._viewFrame.onExitTable();
                else {
                    this.bChangeDesk = false;
                    this.OnResetGameEngine();
                }
            }
        }
        //坐下
        else if (newstatus.cbUserStatus > yl.US_FREE && oldstatus.cbUserStatus < yl.US_SIT) {
            cc.log("自己坐下");
            this.bChangeDesk = false;
            if (this._viewFrame && this._viewFrame.onEnterTable) {
                this._viewFrame.onEnterTable();
            }
            this.SendGameOption();
            if (this._viewFrame && this._viewFrame.onEventUserStatus)
                this._viewFrame.onEventUserStatus(useritem, newstatus, oldstatus);
        }
        else if (newstatus.wTableID != yl.INVALID_TABLE && this.bChangeDesk == true) {
            cc.log("换位");
            if (this._viewFrame && this._viewFrame.onEnterTable)
                this._viewFrame.onEnterTable();
            //showToast(this._viewFrame,"找到游戏桌子，正在获取场景中+.",1)
            this.SendGameOption();
            if (this._viewFrame && this._viewFrame.onEventUserStatus)
                this._viewFrame.onEventUserStatus(useritem, newstatus, oldstatus);
        }
        else {
            cc.log("自己新状态." + newstatus.cbUserStatus);
            if (this._viewFrame && this._viewFrame.onEventUserStatus)
                this._viewFrame.onEventUserStatus(useritem, newstatus, oldstatus);
        }
    }
    //他人状态
    else {
        //更新用户
        if (oldstatus.wTableID != yl.INVALID_TABLE || newstatus.wTableID != yl.INVALID_TABLE) {
            if (this._viewFrame && this._viewFrame.onEventUserStatus)
                this._viewFrame.onEventUserStatus(useritem, newstatus, oldstatus);
        }
        //删除用户
        if (newstatus.cbUserStatus == yl.US_NULL)
            this.onRemoveUser(dwUserID);
    }
};

//请求失败
GameFrameEngine.prototype.onSocketReQuestFailure = function(dataBuffer) {
    var cmdtable = ExternalFun.read_netdata(game_cmd.CMD_GR_RequestFailure, dataBuffer);

    if (this._viewFrame && this._viewFrame.onReQueryFailure)
        this._viewFrame.onReQueryFailure(cmdtable.lErrorCode, cmdtable.szDescribeString);
    else {
        cc.log(cmdtable, "onSocketReQuestFailure", 6);
        cc.log("not viewframe || onReQueryFailure is null");
    }

    if (this.bChangeDesk == true) {
        this.bChangeDesk = false;
        if (this._viewFrame && this._viewFrame.onExitTable && !GlobalUserItem.bWaitQuit)
            this._viewFrame.onExitTable();
    }
    // 清理锁表
    GlobalUserItem.dwLockServerID = 0;
    GlobalUserItem.dwLockKindID = 0;
};

//状态信息
GameFrameEngine.prototype.onSocketStatusEvent = function(sub,dataBuffer) {
    if (sub == yl.SUB_GR_TABLE_INFO) {
        cc.log("SUB_GR_TABLE_INFO");
        var wTableCount = dataBuffer.readword();
        for (i = 0; i < wTableCount; i++) {
            this._tableStatus[i] = [];
            this._tableStatus[i].cbTableLock = dataBuffer.readbyte();
            this._tableStatus[i].cbPlayStatus = dataBuffer.readbyte();
            this._tableStatus[i].lCellScore = dataBuffer.readint();
        }
        if (! GlobalUserItem.bPrivateRoom && ! GlobalUserItem.bMatch) {
            if (this._viewFrame && this._viewFrame.onEnterRoom)
                this._viewFrame.onEnterRoom();
        }
        if (this._viewFrame && this._viewFrame.onGetTableInfo)
            this._viewFrame.onGetTableInfo();
    }
    else if (sub == yl.SUB_GR_TABLE_STATUS) {	//桌子状态
        var wTableID = dataBuffer.readword() + 1;
        this._tableStatus[wTableID] = [];
        this._tableStatus[wTableID].cbTableLock = dataBuffer.readbyte();
        this._tableStatus[wTableID].cbPlayStatus = dataBuffer.readbyte();
        this._tableStatus[wTableID].lCellScore = dataBuffer.readint();

        cc.log("SUB_GR_TABLE_STATUS ==> " + wTableID);
        if (this._viewFrame && this._viewFrame.upDataTableStatus)
            this._viewFrame.upDataTableStatus(wTableID);
    }
};

//框架信息
GameFrameEngine.prototype.onSocketFrameEvent = function(sub,dataBuffer) {
    //游戏状态
    if (sub == yl.SUB_GF_GAME_STATUS) {
        this._cbGameStatus = dataBuffer.readword();
        //this._cbAllowLookon = dataBuffer.readword();
    }
    //游戏场景
    else if (sub == yl.SUB_GF_GAME_SCENE) {
        if (this._viewFrame && this._viewFrame.onEventGameScene)
            this._viewFrame.onEventGameScene(this._cbGameStatus, dataBuffer);
        else {
            cc.log("game scene did !respon");
            if (null == this._viewFrame)
                cc.log("viewframe is nl");
            else
                cc.log("onEventGameScene is ni viewframe is" + this._viewFrame.getTag());
        }
    }
    //系统消息
    else if (sub == yl.SUB_GF_SYSTEM_MESSAGE)
        this.onSocketSystemMessage(dataBuffer);
    //动作消息
    else if (sub == yl.SUB_GF_ACTION_MESSAGE)
        this.onSocketActionMessage(dataBuffer);
    //用户聊天
    else if (sub == game_cmd.SUB_GF_USER_CHAT) {
        var chat = ExternalFun.read_netdata(game_cmd.CMD_GF_S_UserChat, dataBuffer);
        //获取玩家昵称
        var useritem = this._UserList[chat.dwSendUserID];
        if (null == useritem)
            return;
        if (this._wTableID == yl.INVALID_CHAIR || this._wTableID != useritem.wTableID)
            return;

        chat.szNick = useritem.szNickName;

        new GameChatLayer().addChatRecordWith(chat);

        if (null != this._viewFrame && null != this._viewFrame.onUserChat) {
            // 播放声音
            var idx = new GameChatLayer().compareWithText(chat.szChatString);
            if (null != idx) {
                var sound_path = "src/client/res/sound/" + useritem.cbGender + "_" + idx + ".wav";
                if (GlobalUserItem.bSoundAble)
                    cc.audioEngine.playEffect(sound_path,false);
                    // cc.audioEngine.playEffect(cc.FileUtils.getInstance().fullPathForFilename(sound_path), false);
            }
            this._viewFrame.onUserChat(chat, useritem.wChairID);
        }
    }
    //用户表情
    else if (sub == game_cmd.SUB_GF_USER_EXPRESSION) {
        var expression = ExternalFun.read_netdata(game_cmd.CMD_GF_S_UserExpression, dataBuffer);
        //获取玩家昵称
        var useritem = this._UserList[expression.dwSendUserID];

        if (null == useritem)
            return;
        if (this._wTableID == yl.INVALID_CHAIR || this._wTableID != useritem.wTableID)
            return;

        expression.szNick = useritem.szNickName;

        GameChatLayer.addChatRecordWith(expression, true);
        if (null != this._viewFrame && null != this._viewFrame.onUserExpression)
            this._viewFrame.onUserExpression(expression, useritem.wChairID);
    }
    // 用户语音
    else if (sub == game_cmd.SUB_GF_USER_VOICE) {
        AudioRecorder.getInstance().saveRecordFile(dataBuffer, function (uid, tid, spath) {
            var msgTab = [];
            msgTab.uid = uid;
            msgTab.tid = tid;
            msgTab.spath = spath;
            this.tabVoiceMsgQueue.push(msgTab);

            this.popVocieMsg();
        });
    }
};

//系统消息
GameFrameEngine.prototype.onSocketSystemMessage = function(dataBuffer) {
    var wType = dataBuffer.readword();
    var wLength = dataBuffer.readword();
    var szString = dataBuffer.readstring();
    cc.log("系统消息#" + wType + "#" + szString);
    var bCloseRoom = bit._and(wType, yl.SMT_CLOSE_ROOM);
    var bCloseGame = bit._and(wType, yl.SMT_CLOSE_GAME);
    var bCloseLink = bit._and(wType, yl.SMT_CLOSE_LINK);
    if (this._viewFrame)
    //showToast(this._viewFrame,szString,2,cc.c3b(250,0,0))
    cc.log("bCloseRoom ==> "+bCloseRoom);
    cc.log("bCloseGame ==> "+bCloseGame);
    cc.log("bCloseLink ==> "+bCloseLink);
    if (bCloseRoom != 0 || bCloseGame != 0 || bCloseLink != 0) {
        if (515 == wType || 501 == wType) {
            if (this._viewFrame && this._viewFrame.onSystemMessage)
                this._viewFrame.onSystemMessage(wType, szString);
        }
        else {
            this.setEnterAntiCheatRoom(false);
            if (this._viewFrame && this._viewFrame.onExitRoom && !GlobalUserItem.bWaitQuit)
                this._viewFrame.onExitRoom();
            else
                this.onCloseSocket();
        }
    }
};

//系统动作
GameFrameEngine.prototype.onSocketActionMessage = function(dataBuffer) {
    var wType = dataBuffer.readword();
    var wLength = dataBuffer.readword();
    var nButtonType = dataBuffer.readint();
    var szString = dataBuffer.readstring();
    cc.log("系统动作#" + wType + "#" + szString);

    var bCloseRoom = bit._and(wType, yl.SMT_CLOSE_ROOM);
    var bCloseGame = bit._and(wType, yl.SMT_CLOSE_GAME);
    var bCloseLink = bit._and(wType, yl.SMT_CLOSE_LINK);

    if (this._viewFrame)
    //showToast(this._viewFrame,szString,2,cc.c3b(250,0,0))
        if (bCloseRoom != 0 || bCloseGame != 0 || bCloseLink != 0) {
            this.setEnterAntiCheatRoom(false);
            if (this._viewFrame && this._viewFrame.onExitRoom && !GlobalUserItem.bWaitQuit)
                this._viewFrame.onExitRoom();
            else
                this.onCloseSocket();
        }
};


//更新桌椅用户
GameFrameEngine.prototype.onUpDataTableUser = function(tableid,chairid,useritem) {
    var id = tableid;
    var idex = chairid;
    if (null == this._tableUserList[id])
        this._tableUserList[id] = [];
    if (useritem)
        this._tableUserList[id][idex] = useritem.dwUserID;
    else
        this._tableUserList[id][idex] = null;
};
//获取桌子用户
GameFrameEngine.prototype.getTableUserItem = function(tableid,chairid) {
    var id = tableid;
    var idex = chairid;
    if (this._tableUserList[id]) {
        var userid = this._tableUserList[id][idex];
        if (userid)
            return this._UserList[userid];
    }
    return null;
};

GameFrameEngine.prototype.getTableInfo = function(index) {
    if (index > 0)
        return this._tableStatus[index];
};

//获取自己游戏信息
GameFrameEngine.prototype.GetMeUserItem = function() {
    return this._UserList[GlobalUserItem.dwUserID];
};

//获取游戏状态
GameFrameEngine.prototype.GetGameStatus = function() {
    return this._cbGameStatus;
};

//设置游戏状态
GameFrameEngine.prototype.SetGameStatus = function(cbGameStatus) {
    this._cbGameStatus = cbGameStatus;
};

//获取桌子ID
GameFrameEngine.prototype.GetTableID = function() {
    return this._wTableID;
};

//获取椅子ID
GameFrameEngine.prototype.GetChairID = function() {
    return this._wChairID;
};

//移除用户
GameFrameEngine.prototype.onRemoveUser = function(dwUserID) {
    this._UserList[dwUserID] = null;
};

//坐下请求
GameFrameEngine.prototype.SitDown = function(table ,chair,password) {
    var dataBuffer = new Cmd_Data(70);
    dataBuffer.setcmdinfo(yl.MDM_GR_USER, yl.SUB_GR_USER_SITDOWN);
    dataBuffer.pushword(table);
    dataBuffer.pushword(chair);
    this._reqTable = table;
    this._reqChair = chair;
    if (password)
        dataBuffer.pushstring(password, yl.LEN_PASSWORD);

    //记录坐下信息
    if (null != GlobalUserItem.m_tabEnterGame && typeof(GlobalUserItem.m_tabEnterGame) != undefined) {
        cc.log("update game info");
        GlobalUserItem.m_tabEnterGame.nSitTable = table;
        GlobalUserItem.m_tabEnterGame.nSitChair = chair;
    }
    return this.sendSocketData(dataBuffer);
};

//查询用户
GameFrameEngine.prototype.QueryUserInfo = function(table ,chair) {
    var dataBuffer = new Cmd_Data(4);
    dataBuffer.setcmdinfo(yl.MDM_GR_USER, yl.SUB_GR_USER_CHAIR_INFO_REQ);
    dataBuffer.pushword(table);
    dataBuffer.pushword(chair);
    return this.sendSocketData(dataBuffer);
};

//换位请求
GameFrameEngine.prototype.QueryChangeDesk = function() {
    this.bChangeDesk = true;
    var dataBuffer = new Cmd_Data(0);
    dataBuffer.setcmdinfo(yl.MDM_GR_USER, yl.SUB_GR_USER_CHAIR_REQ);
    return this.sendSocketData(dataBuffer);
};

//起立请求
GameFrameEngine.prototype.StandUp = function(bForce) {
    var dataBuffer = new Cmd_Data(5);
    dataBuffer.setcmdinfo(yl.MDM_GR_USER, yl.SUB_GR_USER_STANDUP);
    dataBuffer.pushword(this.GetTableID());
    dataBuffer.pushword(this.GetChairID());
    dataBuffer.pushbyte(!bForce && 0 || 1);
    return this.sendSocketData(dataBuffer);
};

//发送准备
GameFrameEngine.prototype.SendUserReady = function(dataBuffer) {
    var userReady = dataBuffer;
    if (!userReady)
        userReady = new Cmd_Data(0);
    userReady.setcmdinfo(yl.MDM_GF_FRAME, yl.SUB_GF_USER_READY);
    return this.sendSocketData(userReady);
};

//场景规则
GameFrameEngine.prototype.SendGameOption = function() {
    var dataBuffer = new Cmd_Data(9);
    dataBuffer.setcmdinfo(yl.MDM_GF_FRAME, yl.SUB_GF_GAME_OPTION);
    dataBuffer.pushbyte(0);
    dataBuffer.pushdword(appdf.VersionValue(6, 7, 0, 1));
    dataBuffer.pushdword(this._kindVersion);
    return this.sendSocketData(dataBuffer);
};

//加密桌子
GameFrameEngine.prototype.SendEncrypt = function(pass) {
    var passlen = pass.length * 2; //14//(ExternalFun.stringLen(pass)) * 2
    cc.log("passlen ==> " + passlen);
    var len = passlen + 4 + 13;//(sizeof game_cmd.CMD_GR_UserRule)
    cc.log("len ==> " + len);
    var cmddata = new Cmd_Data(len);
    cmddata.setcmdinfo(game_cmd.MDM_GR_USER, game_cmd.SUB_GR_USER_RULE);
    cmddata.pushbyte(0);
    cmddata.pushword(0);
    cmddata.pushword(0);
    cmddata.pushint(0);
    cmddata.pushint(0);
    cmddata.pushword(passlen);
    cmddata.pushword(game_cmd.DTP_GR_TABLE_PASSWORD);
    cmddata.pushstring(pass, passlen / 2);

    return this.sendSocketData(cmddata);
};

//发送文本聊天 game_cmd.CMD_GF_C_UserChat
//[msg] 聊天内容
//[tagetUser] 目标用户
GameFrameEngine.prototype.sendTextChat = function( msg, tagetUser , color) {
    if (typeof msg != "string") {
        cc.log("聊天内容异常");
        return [false, "聊天内容异常!"];
    }
        //敏感词判断
    if (true == ExternalFun.isContainBadWords(msg)) {
        cc.log("聊天内容包含敏感词汇");
        return [false, "聊天内容包含敏感词汇!"];
    }
    msg = msg + "\0";

    tagetUser = tagetUser || yl.INVALID_USERID;
    color = color || 16777215;                  //appdf.ValueToColor( 255,255,255 )
    var msgLen = msg.length;
    var defineLen = yl.LEN_USER_CHAT * 2;

    var cmddata = new Cmd_Data(266 - defineLen + msgLen * 2);
    cmddata.setcmdinfo(game_cmd.MDM_GF_FRAME, game_cmd.SUB_GF_USER_CHAT);
    cmddata.pushword(msgLen);
    cmddata.pushdword(color);
    cmddata.pushdword(tagetUser);
    cmddata.pushstring(msg, msgLen);

    return this.sendSocketData(cmddata);
};

//发送表情聊天 game_cmd.CMD_GF_C_UserExpressio
//[idx] 表情图片索引
//[tagetUser] 目标用户
GameFrameEngine.prototype.sendBrowChat = function( idx, tagetUser ) {
    tagetUser = tagetUser || yl.INVALID_USERID;

    var cmddata = new Cmd_Data(6);
    cmddata.setcmdinfo(game_cmd.MDM_GF_FRAME, game_cmd.SUB_GF_USER_EXPRESSION);
    cmddata.pushword(idx);
    cmddata.pushdword(tagetUser);

    return this.sendSocketData(cmddata);
};

GameFrameEngine.prototype.OnResetGameEngine = function() {
    if (this._viewFrame && this._viewFrame.OnResetGameEngine)
        this._viewFrame.OnResetGameEngine();
};

GameFrameEngine.prototype.popVocieMsg = function(){
    if (this._bPlayVoiceRecord) {
        return;
    }
    var self = this;
    var msgTab = this._tabVoiceMsgQueue[1];
    if (typeof msgTab == "table") {
        var uid = msgTab.uid;
        var spath = msgTab.spath;

        //获取玩家
        var useritem = this._UserList[uid];
        if (null != useritem) {
            // 录音开始
            if (null != this._viewFrame && null != this._viewFrame.onUserVoiceStart)
                this._viewFrame.onUserVoiceStart(useritem, spath);

            this._nPlayVoiceId = VoiceRecorderKit.startPlayVoice(spath);
            AudioRecorder.getInstance().setFinishCallBack(this._nPlayVoiceId, function (voiceid, filename) {
                cc.log("play over " + filename);
                VoiceRecorderKit.finishPlayVoice();
                self._bPlayVoiceRecord = false;
                self._nPlayVoiceId = null;

                // 录音结束
                if (null != self._viewFrame && null != self._viewFrame.onUserVoiceEnded)
                    self._viewFrame.onUserVoiceEnded(useritem, spath);
                this.setPlayingVoice(false);
            });
            this._bPlayVoiceRecord = true;
        }
    }
    this.tabVoiceMsgQueue.splice(1, 1);
};

GameFrameEngine.prototype.setPlayingVoice = function( bPlaying ) {
    this._bPlayVoiceRecord = bPlaying;
    if (false == bPlaying)
        this.popVocieMsg();
};

GameFrameEngine.prototype.clearVoiceQueue = function() {
    this._tabVoiceMsgQueue = [];
    this._bPlayVoiceRecord = false;
    if (null != this._nPlayVoiceId) {
        cc.audioEngine.stop(this._nPlayVoiceId);
        VoiceRecorderKit.finishPlayVoice();
        this._nPlayVoiceId = null;
    }
    if (null != AudioRecorder.getInstance().clear)
        AudioRecorder.getInstance().clear();
};