
function GameServerItem(pData) {
    this.wKindID = 0;
    this.wNodeID = 0;
    this.wSortID = 0;
    this.wServerID = 0;
    this.wServerKind = 0;
    this.wServerType = 0;
    this.wServerLevel = 0;
    this.wServerPort = 0;

    this.lCellScore = 0;
    this.cbEnterMember = 0;
    this.lEnterScore = 0;

    this.dwServerRule = 0;
    this.dwOnLineCount = 0;
    this.dwAndroidCount = 0;
    this.dwFullCount = 0;

    this.szServerAddr = "";
    this.szServerName = "";
}


GameServerItem.prototype.onInit = function(pData) {
    if (pData === null) {
        cc.log("GameServerItem-onInit-null");
        return;
    }

    this.wKindID = pData.readword();			// 名称索引
    this.wNodeID = pData.readword();				// 节点索引
    this.wSortID = pData.readword();				// 排序索引
    this.wServerID = pData.readword();			// 房间索引
    this.wServerKind = pData.readword();			// 房间类型
    this.wServerType = pData.readword();			// 房间类型
    this.wServerLevel = pData.readword();			// 房间等级
    this.wServerPort = pData.readword();			// 房间端口

    this.lCellScore = pData.readscore();			// 单元积分
    this.cbEnterMember = pData.readbyte(); 							// 进入会员
    this.lEnterScore = pData.readscore();			// 进入积分

    this.dwServerRule = pData.readdword();		// 房间规则
    this.dwOnLineCount = pData.readdword();		// 在线人数
    this.dwAndroidCount = pData.readdword();		// 机器人数
    this.dwFullCount = pData.readdword();		// 满员人数

    this.szServerAddr = pData.readstring(32);	// 房间地址
    this.szServerName = pData.readstring(32); 	// 房间名称
    this.dwSurportType = pData.readdword(); 		// 支持类型
    this.wTableCount = pData.readword(); 		// 桌子数目

    // self:testlog()
    return this;
};

GameServerItem.prototype.testlog = function() {
    cc.log("**************************************************");
    // dump(self, "GameServerItem", 6)
    cc.log("**************************************************");
};

GameServerItem.prototype.readScore = function(dataBuffer) {
    /*if (this._int64 === null || this._int64 === undefined) {
        this._int64 = Integer64.new().addTo(self);
    }
    dataBuffer.readscore(this._int64);
    return this._int64.getvalue();*/
    return dataBuffer.readscore();
};