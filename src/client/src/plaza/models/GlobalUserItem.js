//var logincmd = appdf.req(appdf.HEADER_SRC  +  "CMD_LogonServer");
var GlobalUserItem = GlobalUserItem || {};

// 原始游戏列表
GlobalUserItem.m_tabOriginGameList = {};
GlobalUserItem.bEnableRoomCard = false; 	//  激活房卡功能

// 重置数据
GlobalUserItem.reSetData = function() {
    GlobalUserItem.bVistor = null;
    GlobalUserItem.bWeChat = false;
    GlobalUserItem.dwGameID = 0;
    GlobalUserItem.dwUserID = 0;

    GlobalUserItem.dwExperience = 0;
    GlobalUserItem.dwLoveLiness = 0;

    GlobalUserItem.szAccount = "";
    GlobalUserItem.szPassword = "";
    GlobalUserItem.szMachine = "";
    GlobalUserItem.szMobilePhone = "";
    GlobalUserItem.szRoomPasswd = "";

    GlobalUserItem.szNickName = "";
    GlobalUserItem.szInsurePass = "";
    GlobalUserItem.szDynamicPass = "";
    //  个人信息附加包
    GlobalUserItem.szSign = "此人很懒，没有签名";
    GlobalUserItem.szSpreaderAccount = "";		//  推广员
    GlobalUserItem.szQQNumber = ""; 		    //  qq号码
    GlobalUserItem.szEmailAddress = "";		    //  邮箱地址
    GlobalUserItem.szSeatPhone = ""; 		    //  座机
    GlobalUserItem.szMobilePhone = ""; 		    //  手机
    GlobalUserItem.szTrueName = "";	            //  真实姓名
    GlobalUserItem.szAddress = ""; 		        //  联系地址
    GlobalUserItem.szPassportID = ""; 		    //  身份ID
    //  个人信息附加包

    GlobalUserItem.lUserScore = 0;						// 用户金币
    GlobalUserItem.lUserIngot = 0;						// 用户元宝
    GlobalUserItem.lUserInsure = 0;						// 银行存款
    GlobalUserItem.dUserBeans = 0.00; 					// 游戏豆
    GlobalUserItem.cbInsureEnabled = 0;
    GlobalUserItem.nLargeTrumpetCount = 0;						// 大喇叭数量

    GlobalUserItem.cbGender = 0;
    GlobalUserItem.cbMemberOrder = 0;
    GlobalUserItem.MemberOverDate = 0;
    GlobalUserItem.MemberList = [];

    GlobalUserItem.wFaceID = 0;
    GlobalUserItem.dwCustomID = 0;

    GlobalUserItem.dwStationID = 0;

    GlobalUserItem.nCurGameKind = 122;
    GlobalUserItem.szCurGameName = "";
    GlobalUserItem.roomlist = [];
    GlobalUserItem.tasklist = [];
    GlobalUserItem.wTaskCount = 0;

    GlobalUserItem.nCurRoomIndex = -1;

    GlobalUserItem.nGameResType = 0;

    GlobalUserItem.bVoiceAble = true;
    GlobalUserItem.bSoundAble = true;

    GlobalUserItem.nSound = 100;
    GlobalUserItem.nMusic = 100;
    GlobalUserItem.bShake = true;

    GlobalUserItem.szHelp = null;

    GlobalUserItem.bAutoLogon = false;
    GlobalUserItem.bSavePassword = false;
    GlobalUserItem.bHasLogon = false;  	// 已经登录过
    GlobalUserItem.bVisitor = false;

    GlobalUserItem.LogonTime = 0;

    GlobalUserItem.wCurrLevelID = 0;
    GlobalUserItem.dwExperience = 0;
    GlobalUserItem.dwUpgradeExperience = 0;
    GlobalUserItem.lUpgradeRewardGold = 0;
    GlobalUserItem.lUpgradeRewardIngot = 0;

    GlobalUserItem.wSeriesDate = 0; 		// 连续日期
    GlobalUserItem.bTodayChecked = false; 	// 今日签到
    GlobalUserItem.lRewardGold = [0, 0, 0, 0, 0, 0, 0];	// 金币数目
    GlobalUserItem.buyItem = null;
    GlobalUserItem.useItem = [];

    GlobalUserItem.szThirdPartyUrl = "";		// 第三方头像url
    GlobalUserItem.bThirdPartyLogin = false;
    GlobalUserItem.thirdPartyData = {};		// 第三方登陆数据
    GlobalUserItem.m_tabEnterGame = null; 		// 保存进入游戏的数据
    GlobalUserItem.dwServerRule = 0;			// 房间规则
    GlobalUserItem.bEnterGame = false; 	// 进入游戏
    GlobalUserItem.bIsAngentAccount = false; 	// 是否是代理商帐号

    GlobalUserItem.bQueryCheckInData = false;

    GlobalUserItem.nTableFreeCount = 0;		// 转盘免费次数
    GlobalUserItem.nShareSend = 0;		// 每日首充赠送

    GlobalUserItem.bJftPay = false;
    GlobalUserItem.szSpreaderURL = null; 		// 普通分享链接
    GlobalUserItem.szWXSpreaderURL = null; 		// 微信分享链接

    GlobalUserItem.tabShopCache = {}; 		// 商店信息缓存
    GlobalUserItem.tabRankCache = {};		// 排行信息缓存
    GlobalUserItem.bFilterTask = false; 	// 是否过滤任务 (只显示单个游戏任务)

    GlobalUserItem.bPrivateRoom = false; 	//  私人房

    GlobalUserItem.lRoomCard = 0;		//  房卡数量
    GlobalUserItem.dwLockServerID = 0;		//  锁定房间
    GlobalUserItem.dwLockKindID = 0; 		//  锁定游戏
    GlobalUserItem.bWaitQuit = false; 	//  等待退出
    GlobalUserItem.bAutoConnect = true; 		//  是否自动断线重连(游戏切换到主页再切换回)

    GlobalUserItem.cbLockMachine = 0;		//  是否锁定设备
    GlobalUserItem.szIpAdress = "";		//  ip地址
    GlobalUserItem.tabCoordinate = {lo: 360.0, la: 360.0};		//  坐标
    GlobalUserItem.bUpdateCoordinate = false;	//  是否更新坐标
    GlobalUserItem.tabDayTaskCache = {}; 		//  每日必做列表
    GlobalUserItem.nInviteSend = 0;			//  邀请奖励
    GlobalUserItem.bEnableCheckIn = false; 	//  激活签到
    GlobalUserItem.bEnableTask = false; 	//  激活任务
    GlobalUserItem.bEnableEveryDay = false; 	//  激活每日任务开关

    GlobalUserItem.szCopyRoomId = "";		//  复制房间id

    GlobalUserItem.bMatch = false; 	//  比赛
    GlobalUserItem.tabIntroduceCache = {}; 		//  玩法介绍
};

// 读取配置
GlobalUserItem.LoadData = function() {
    // 声音设置
    GlobalUserItem.bVoiceAble = cc.sys.localStorage.getItem("vocieable") != null ? cc.sys.localStorage.getItem("vocieable") : true ;
    GlobalUserItem.bSoundAble = cc.sys.localStorage.getItem("soundable") != null ? (cc.sys.localStorage.getItem("soundable")!="false"? true: false) : true ;
    // 音量设置
    GlobalUserItem.nSound = cc.sys.localStorage.getItem("soundvalue") != null ? cc.sys.localStorage.getItem("soundvalue") : 100 ;
    GlobalUserItem.nMusic = cc.sys.localStorage.getItem("musicvalue") != null ? cc.sys.localStorage.getItem("musicvalue") : 100 ;
    // 震动设置
    GlobalUserItem.bShake = cc.sys.localStorage.getItem("shakeable") != null ? cc.sys.localStorage.getItem("shakeable") : true ;
    // 自动登录
    GlobalUserItem.bAutoLogon = cc.sys.localStorage.getItem("autologon") != null ? cc.sys.localStorage.getItem("autologon") : false ;

    if (GlobalUserItem.bSoundAble)
        cc.audioEngine.setMusicVolume(GlobalUserItem.nMusic / 100.0);
    else
        cc.audioEngine.setMusicVolume(0);

    if (GlobalUserItem.bVoiceAble)
        cc.audioEngine.setEffectsVolume(GlobalUserItem.nSound / 100.00);
    else
        cc.audioEngine.setEffectsVolume(0);

    // 账号密码
    var sp = "";

        GlobalUserItem.szAccount = appdf.getSaveMD5String(sp, "user_gameconfig.plist", "code_1", 32);
    if (GlobalUserItem.szAccount != "") {
        GlobalUserItem.szPassword = appdf.getSaveMD5String(sp, "user_gameconfig.plist", "code_2", 32);
        GlobalUserItem.bSavePassword = GlobalUserItem.szPassword != "";
    }
    else {
        GlobalUserItem.szAccount = "";
        GlobalUserItem.szPassword = "";
        GlobalUserItem.bAutoLogon = false;
        GlobalUserItem.bSavePassword = false;
    }

    GlobalUserItem.wCurrLevelID = 0;
    GlobalUserItem.dwExperience = 0;
    GlobalUserItem.dwUpgradeExperience = 0;
    GlobalUserItem.lUpgradeRewardGold = 0;
    GlobalUserItem.lUpgradeRewardIngot = 0;
}

GlobalUserItem.reSetData();

// 保存配置参数
GlobalUserItem.onSaveAccountConfig = function(save_type) {
    var sp = "";
    var szSaveInfo = "";

    if (save_type != null && save_type == 3) {
        if (GlobalUserItem.thirdPartyData != null) {
            szSaveInfo = "";
            if (GlobalUserItem.thirdPartyData.szAccount != null && GlobalUserItem.thirdPartyData.szAccount.length > 0) {
                szSaveInfo = md5(GlobalUserItem.thirdPartyData.szAccount) + GlobalUserItem.thirdPartyData.szAccount;
            }
            appdf.saveByEncrypt("code_3", szSaveInfo);
            // appdf.saveByEncrypt(sp + "user_gameconfig.plist", "code_3", szSaveInfo);

            szSaveInfo = "";
            if (GlobalUserItem.thirdPartyData.szNick != null && GlobalUserItem.thirdPartyData.szNick.length > 0) {
                szSaveInfo = md5(GlobalUserItem.thirdPartyData.szNick) + GlobalUserItem.thirdPartyData.szNick;
            }
            appdf.saveByEncrypt("code_4", szSaveInfo);
            // saveByEncrypt(sp + "user_gameconfig.plist", "code_4", szSaveInfo);

            szSaveInfo = "";
            if (GlobalUserItem.thirdPartyData.cbGender != null && GlobalUserItem.thirdPartyData.cbGender >= 0) {
                var sGender = "" + GlobalUserItem.thirdPartyData.cbGender;   //string.format("%d",GlobalUserItem.thirdPartyData.cbGender)
                szSaveInfo = md5(sGender) + sGender;
            }
            appdf.saveByEncrypt("code_5", szSaveInfo);
            // saveByEncrypt(sp + "user_gameconfig.plist", "code_5", szSaveInfo);
        }
    }
    else {
        szSaveInfo = "";
        if (GlobalUserItem.szAccount != null && GlobalUserItem.szAccount.length > 0) {
            szSaveInfo = md5(GlobalUserItem.szAccount) + GlobalUserItem.szAccount;
        }
        appdf.saveByEncrypt("code_3", szSaveInfo);
        // saveByEncrypt(sp + "user_gameconfig.plist", "code_1", szSaveInfo);

        szSaveInfo = "";
        if (GlobalUserItem.bSavePassword && GlobalUserItem.szPassword != null && GlobalUserItem.szPassword.length > 0) {
            szSaveInfo = md5(GlobalUserItem.szPassword) + GlobalUserItem.szPassword;
        }
        appdf.saveByEncrypt("code_2", szSaveInfo);
        // saveByEncrypt(sp + "user_gameconfig.plist", "code_2", szSaveInfo);
    }

    cc.sys.localStorage.setItem("autologon", GlobalUserItem.bAutoLogon);
};

GlobalUserItem.setShakeAble = function(able) {
    GlobalUserItem.bShake = able;
    cc.sys.localStorage.setItem("shakeable", GlobalUserItem.bShake);
};

GlobalUserItem.setSoundAble = function(able) {
    GlobalUserItem.bSoundAble = able;
    if (able)
        cc.audioEngine.setMusicVolume(GlobalUserItem.nMusic / 100.0);
    else {
        cc.audioEngine.setMusicVolume(0);
        cc.audioEngine.stopMusic(); // 暂停音乐
    }
    cc.sys.localStorage.setItem("soundable", GlobalUserItem.bSoundAble);
};

GlobalUserItem.setVoiceAble = function(able) {
    GlobalUserItem.bVoiceAble = able;
    if (able)
        cc.audioEngine.setEffectsVolume(GlobalUserItem.nSound / 100.00);
    else
        cc.audioEngine.setEffectsVolume(0);
    cc.sys.localStorage.setItem("vocieable", GlobalUserItem.bVoiceAble);
};

GlobalUserItem.setMusicVolume = function(music) {
    var tmp = music;
    if (tmp > 100)
        tmp = 100;
    else if (tmp < 0)
        tmp = 0;

    cc.audioEngine.setMusicVolume(tmp / 100.0);
    GlobalUserItem.nMusic = tmp;
    cc.sys.localStorage.setItem("musicvalue", GlobalUserItem.nMusic);
};

GlobalUserItem.setEffectsVolume = function(sound) {
    var tmp = sound;
    if (tmp > 100)
        tmp = 100;
    else if (tmp < 0)
        tmp = 0;

    cc.audioEngine.setEffectsVolume(tmp / 100.00);
    GlobalUserItem.nSound = tmp;
    cc.sys.localStorage.setItem("soundvalue", GlobalUserItem.nSound);
};

// 查询房间数目(普通房間)
GlobalUserItem.GetGameRoomCount = function(nKindID) {
    var checkKind;
    if (!nKindID)
        checkKind = GlobalUserItem.nCurGameKind;
    else
        checkKind = Number(nKindID);

    for (i = 0; i < GlobalUserItem.roomlist.length; i++) {
        var list = GlobalUserItem.roomlist[i];
        if (Number(list[0]) == checkKind) {
            if (!list[1])
                break;

            var nCount = 0;
            var serverList = list[1];

            serverList.forEach(function (v, k) {
                if (v.wServerType != yl.GAME_GENRE_PERSONAL) nCount++;
            });

            return nCount;
        }
    }
    return 0;
};

// 获取房间信息(普通房间)
GlobalUserItem.GetGameRoomInfo = function(wServerID, nKindID) {
    var checkKind;
    if (!nKindID)
        checkKind = GlobalUserItem.nCurGameKind;
    else
        checkKind = Number(nKindID);

    for (i = 0; i < GlobalUserItem.roomlist.length; i ++) {
        var list = GlobalUserItem.roomlist[i];
        if (Number(list[0]) == checkKind) {
            if (!list[1])
                break;

            var nCount = 0;
            var serverList = list[1];
            serverList.forEach(function (v, k) {
                if (v.wServerID == wServerID && v.wServerType != yl.GAME_GENRE_PERSONAL && v.wServerType != yl.GAME_GENRE_MATCH)
                    return v;
            });
        }
    }
    return null;
};

// 查询房间信息
GlobalUserItem.GetRoomInfo = function(index,nKindID) {
    var checkKind;
    if (nKindID == undefined)
        checkKind = GlobalUserItem.nCurGameKind;
    else
        checkKind = Number(nKindID);

    if (checkKind == undefined) {
        cc.log("not checkKind");
        return null;
    }

    var roomIndex = index;
    if (roomIndex == undefined) roomIndex = GlobalUserItem.nCurRoomIndex;

    if (roomIndex == undefined) {
        cc.log("not roomIndex");
        return null;
    }
    if (roomIndex < 0) {
        cc.log("roomIndex < 0");
        return null;
    }
    for (i = 0; i < GlobalUserItem.roomlist.length; i++) {
        var list = GlobalUserItem.roomlist[i];
        if (Number(list[0]) == Number(checkKind)) {
            var listinfo = list[1];
            if (!listinfo) {
                cc.log("not listinfo");
                return null;
            }
            if (roomIndex >= listinfo.length) {
                cc.log("roomIndex > #listinfo");
                return null;
            }
            return listinfo[roomIndex];
        }
    }
};

//  非房卡房間號
GlobalUserItem.normalRoomIndex = function(nKindID) {
    if (!nKindID) {
        cc.log("not kindid");
        return null;
    }
    for (i = 0; i < GlobalUserItem.roomlist.length; i++) {
        var list = GlobalUserItem.roomlist[i];
        if (Number(list[0]) == Number(nKindID)) {
            var listinfo = list[1];
            if (!listinfo) {
                cc.log("not listinfo");
                return null;
            }
            listinfo.forEach(function (v, k) {
                if (v.wServerType != yl.GAME_GENRE_PERSONAL) return v._nRoomIndex;
            });
            return null;
        }
    }
};

// 查询房间数目
GlobalUserItem.GetRoomCount = function(nKindID) {
    var checkKind;
    if (!nKindID)
        checkKind = GlobalUserItem.nCurGameKind;
    else
        checkKind = Number(nKindID);

    for (i = 0; i < GlobalUserItem.roomlist.length; i++) {
        var list = GlobalUserItem.roomlist[i];
        if (Number(list[0]) == checkKind) {
            if (!list[1])
                break;

            var nCount = 0;
            var serverList = list[1];
            if( serverList != null){
                nCount = serverList.length;
            }
            return nCount;
        }
    }
    return 0;
};

// 加载用户信息
GlobalUserItem.onLoadData = function(pData) {
    if (pData === null) {
        cc.log("GlobalUserItem-LoadData-null");
        return;
    }
    // 登录时间
    GlobalUserItem.LogonTime = currentTime();

    GlobalUserItem.wFaceID = pData.readword();
    GlobalUserItem.cbGender = pData.readbyte();
    GlobalUserItem.dwCustomID = pData.readdword();
    GlobalUserItem.dwUserID = pData.readdword();
    GlobalUserItem.dwGameID = pData.readdword();
    GlobalUserItem.dwExperience = pData.readdword();
    GlobalUserItem.dwLoveLiness = GlobalUserItem.readScore(pData); // pData.readdword()
    GlobalUserItem.szAccount = pData.readstring(32);
    GlobalUserItem.szNickName = pData.readstring(32);
    GlobalUserItem.szDynamicPass = pData.readstring(33);
    GlobalUserItem.lUserScore = GlobalUserItem.readScore(pData);
    GlobalUserItem.lUserIngot = GlobalUserItem.readScore(pData);
    GlobalUserItem.lUserInsure = GlobalUserItem.readScore(pData);
    GlobalUserItem.dUserBeans = pData.readdouble();
    GlobalUserItem.cbInsureEnabled = pData.readbyte();
    var bAngent = pData.readbyte() || 0;
    GlobalUserItem.bIsAngentAccount = (bAngent == 1);
    GlobalUserItem.cbLockMachine = pData.readbyte();
    GlobalUserItem.lRoomCard = GlobalUserItem.readScore(pData);
    GlobalUserItem.dwLockServerID = pData.readdword();
    GlobalUserItem.dwLockKindID = pData.readdword();
    cc.log("lock server " + GlobalUserItem.dwLockServerID);
    cc.log("lock kind " + GlobalUserItem.dwLockKindID);

    var curlen = pData.getcurlen();
    var datalen = pData.getlen();

    cc.log("*** curlen-" + curlen);
    cc.log("*** datalen-" + datalen);

    var tmpSize;
    var tmpCmd;
    while (curlen < datalen) {
        tmpSize = pData.readword();
        tmpCmd = pData.readword();
        if (!tmpSize || !tmpCmd) {
            break;
        }

        cc.log("*** tmpSize-" + tmpSize);
        cc.log("*** tmpCmd-" + tmpCmd);
        if (tmpCmd == yl.DTP_GP_UI_UNDER_WRITE) {
            GlobalUserItem.szSign = pData.readstring(tmpSize / 2);
            if (!GlobalUserItem.szSign) {
                GlobalUserItem.szSign = "此人很懒，没有签名";
            }
        }
        else if (tmpCmd == yl.DTP_GP_MEMBER_INFO) {
            GlobalUserItem.cbMemberOrder = pData.readbyte();
            for (i = 0; i < 8; i++)
                cc.log("systemtime-" + pData.readword());
        }
        else if (tmpCmd == logincmd.DTP_GP_UI_QQ) {
            GlobalUserItem.szQQNumber = pData.readstring(tmpSize / 2) || "";
            cc.log("qq " + GlobalUserItem.szQQNumber);
        }
        else if (tmpCmd == logincmd.DTP_GP_UI_EMAIL) {
            GlobalUserItem.szEmailAddress = pData.readstring(tmpSize / 2) || "";
            cc.log("email " + GlobalUserItem.szEmailAddress);
        }
        else if (tmpCmd == logincmd.DTP_GP_UI_SEAT_PHONE) {
            GlobalUserItem.szSeatPhone = pData.readstring(tmpSize / 2) || "";
            cc.log("szSeatPhone " + GlobalUserItem.szSeatPhone);
        }
        else if (tmpCmd == logincmd.DTP_GP_UI_MOBILE_PHONE) {
            GlobalUserItem.szMobilePhone = pData.readstring(tmpSize / 2) || "";
            cc.log("szMobilePhone " + GlobalUserItem.szMobilePhone);
        }
        else if (tmpCmd == logincmd.DTP_GP_UI_COMPELLATION) {
            GlobalUserItem.szTrueName = pData.readstring(tmpSize / 2) || "";
            cc.log("szTrueName " + GlobalUserItem.szTrueName);
        }
        else if (tmpCmd == logincmd.DTP_GP_UI_DWELLING_PLACE) {
            GlobalUserItem.szAddress = pData.readstring(tmpSize / 2) || "";
            cc.log("szAddress " + GlobalUserItem.szAddress);
        }
        else if (tmpCmd == logincmd.DTP_GP_UI_PASSPORTID) {
            GlobalUserItem.szPassportID = pData.readstring(tmpSize / 2) || "";
            cc.log("szPassportID " + GlobalUserItem.szPassportID);
        }
        else if (tmpCmd == logincmd.DTP_GP_UI_SPREADER) {
            GlobalUserItem.szSpreaderAccount = pData.readstring(tmpSize / 2) || "";
            cc.log("szSpreaderAccount " + GlobalUserItem.szSpreaderAccount);
        }
        else if (tmpCmd == 0)
            break;
        else {
            for (i = 0; i < tmpSize; i++) {
                if (!pData.readbyte())
                    break;
            }
        }
        curlen = pData.getcurlen();
    }

    GlobalUserItem.testlog();
}

GlobalUserItem.testlog = function() {
    cc.log("**************************************************");
    // dump(self, "GlobalUserItem", 6)
    cc.log("**************************************************");
};

/**
 *
 * @param {Cmd_Data} dataBuffer
 * @returns {*|number}
 */
GlobalUserItem.readScore = function(dataBuffer) {
    /* original code
    if (typeof this._int64 == 'undefined') {
        this._int64 = Integer64.new().retain();
    }
    dataBuffer.readscore(this._int64);
    return self._int64.getvalue();*/
    this._int64 = dataBuffer.readscore();
    return this._int64;
};

GlobalUserItem.getSignature = function(times) {
    var timevalue = times;
    cc.log("timevalue-" + timevalue);
    var timestr = "" + timevalue;
    var pstr = "" + GlobalUserItem.dwUserID;
    pstr = pstr + GlobalUserItem.szDynamicPass + timestr + "RYSyncLoginKey";
    pstr = md5(pstr).toUpperCase();

    cc.log("signature-" + pstr);
    return pstr;
};

GlobalUserItem.getDateNumber = function(datestr) {
    var index = datestr.search("%(");
    var strname = "";
    var dwnum = "";
    if (index > -1) {
        dwnum = datestr.substring(index + 1, -1);
        strname = datestr.substring(0, index - 1);
    }

    index = dwnum.search("%)");
    if (index > -1) {
        dwnum = dwnum.substring(0, index - 1);
    }
    return dwnum;
};

// 是否是防作弊
GlobalUserItem.isAntiCheat = function() {
    return (bit._and(GlobalUserItem.dwServerRule, yl.SR_ALLOW_AVERT_CHEAT_MODE) != 0);
};

// 防作弊是否有效(是否进入了游戏)
GlobalUserItem.isAntiCheatValid = function(userid) {
    if (false == GlobalUserItem.bEnterGame)
        return false;

    // 自己排除
    if (userid == GlobalUserItem.dwUserID)
        return false;

    return GlobalUserItem.isAntiCheat();
};

GlobalUserItem.todayCheck = function(date) {
    if (null == date)
        return false;

    var curDate = new Date();
    var checkDate = new Date(date);
    return curDate.getFullYear() == checkDate.getFullYear() && curDate.getMonth() == checkDate.getMonth() && curDate.getDate() == checkDate.getDate();
};

GlobalUserItem.setTodayFirstAction = function(key, value) {
    cc.sys.localStorage.setItem(key, value  +  "");
    cc.sys.localStorage.clear();
	// cc.UserDefault.getInstance().flush();
};

// 当日首次签到
GlobalUserItem.isFirstCheckIn = function() {
    var everyDayCheck = cc.sys.localStorage.getItem(GlobalUserItem.dwUserID + "everyDayCheck") != null ? cc.sys.localStorage.getItem(GlobalUserItem.dwUserID + "everyDayCheck") : null;
    // cc.log(everyDayCheck)
    if ("nil" !== everyDayCheck) {
        var n = Number(everyDayCheck);
        return !GlobalUserItem.todayCheck(n);
    }
    return true;
};

GlobalUserItem.setTodayCheckIn = function() {
    if (GlobalUserItem.isFirstCheckIn()) {
        GlobalUserItem.setTodayFirstAction(GlobalUserItem.dwUserID + "everyDayCheck", (new Date()).getTime());
    }
};

// 当日首次充值
GlobalUserItem.isFirstPay = function() {
    var everyDayPay = cc.sys.localStorage.getItem(GlobalUserItem.dwUserID + "everyDayPay") != null ? cc.sys.localStorage.getItem(GlobalUserItem.dwUserID + "everyDayPay") : null ;
    if ("nil" !== everyDayPay) {
        var n = Number(everyDayPay);
        return !GlobalUserItem.todayCheck(n);
    }
    return true;
};

GlobalUserItem.setTodayPay = function() {
    if (GlobalUserItem.isFirstPay()) {
        GlobalUserItem.setTodayFirstAction(GlobalUserItem.dwUserID + "everyDayPay", (new Date()).getTime());
    }
};

// 当日首次分享
GlobalUserItem.isFirstShare = function() {
    var everyDayShare = cc.sys.localStorage.getItem(GlobalUserItem.dwUserID + "everyDayShare") != null ? cc.sys.localStorage.getItem(GlobalUserItem.dwUserID + "everyDayShare") : null ;
    if ("nil" != everyDayShare) {
        var n = Number(everyDayShare);
        return !GlobalUserItem.todayCheck(n);
    }
    return true;
};

GlobalUserItem.setTodayShare = function() {
    if (GlobalUserItem.isFirstShare()) {
        GlobalUserItem.setTodayFirstAction(GlobalUserItem.dwUserID + "everyDayShare", (new Date()).getTime());
    }
}

// 当日首次转盘
GlobalUserItem.isFirstTable = function() {
    var everyDayTable = cc.sys.localStorage.getItem(GlobalUserItem.dwUserID + "everyDayTable") != null ? cc.sys.localStorage.getItem(GlobalUserItem.dwUserID + "everyDayTable") : null ;
    if ("nil" != everyDayTable) {
        var n = Number(everyDayTable);
        return !GlobalUserItem.todayCheck(n);
    }
    return true;
};

GlobalUserItem.setTodayTable = function() {
    if (GlobalUserItem.isFirstTable()) {
        GlobalUserItem.setTodayFirstAction(GlobalUserItem.dwUserID + "everyDayTable", (new Date()).getTime());
    }
};

//  当日首页广告
GlobalUserItem.isShowAdNotice = function() {

//   pangyu_20170710_隐藏当日首页广告
    return false;
// 	var everyDayAdNotice = cc.UserDefault:getInstance():getStringForKey(GlobalUserItem.dwUserID  +  "everyDayNoAdNotice", "nil")
// 	if "nil" != everyDayAdNotice 
// 		var n = Number(everyDayAdNotice)
// 		return not GlobalUserItem.todayCheck(n)
// 	end
// 	return true
};

GlobalUserItem.setTodayNoAdNotice = function( noAds ) {
    if (noAds)
        GlobalUserItem.setTodayFirstAction(GlobalUserItem.dwUserID + "everyDayNoAdNotice", (new Date()).getTime());
    else
        GlobalUserItem.setTodayFirstAction(GlobalUserItem.dwUserID + "everyDayNoAdNotice", "nil");
};

// 判断是否是代理商帐号
GlobalUserItem.isAngentAccount = function(nottip) {
    nottip = nottip || false;
    if (GlobalUserItem.bIsAngentAccount) {
        var runScene = cc.director.getRunningScene();
        if (null != runScene && !nottip) {
            showToast(runScene, "您是代理商帐号，无法操作！", 2);
        }
        return true;
    }
    return false;
};

// 设置是否绑定账号
GlobalUserItem.setBindingAccount = function() {
    cc.sys.localStorage.setItem("isBingdingAccount", true);
};

// 获取是否绑定账号
GlobalUserItem.getBindingAccount = function() {
    // return new cc.UserDefault.getBoolForKey("isBingdingAccount", false);
    return cc.sys.localStorage.getItem("isBindingAccount");
};

// 判断是否能修改信息
GlobalUserItem.notEditAble = function(nottip) {
    nottip = nottip || false;
    if (GlobalUserItem.bWeChat) {
        var runScene = cc.director.getRunningScene();
        if (null != runScene && !nottip) {
            var str = "";
            if (GlobalUserItem.bVistor)
                str = "游客登录无法修改信息";
            else if (GlobalUserItem.bWeChat)
                str = "微信登录无法修改信息";

            showToast(runScene, str, 2);
        }
        return true;
    }
    return false;
};

//  无定位数据
GlobalUserItem.noCoordinateData = function () {
    if (null == GlobalUserItem.tabCoordinate || null == GlobalUserItem.tabCoordinate.la || 360.0 == GlobalUserItem.tabCoordinate.la || null == GlobalUserItem.tabCoordinate.lo || 360.0 == GlobalUserItem.tabCoordinate.lo)
        return true;
    return false;
};