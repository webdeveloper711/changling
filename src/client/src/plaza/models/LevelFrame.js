/**
 * @author ?
 * Reviewed by Jong: 2017-12-02
 */

// var BaseFrame = appdf.req(appdf.CLIENT_SRC+"plaza.models.BaseFrame")
// var LevelFrame = class("LevelFrame",BaseFrame)

function LevelFrame(view, callback) {
    BaseFrame.call(this, view, callback);
}

LevelFrame.prototype = Object.create(BaseFrame.prototype);
LevelFrame.prototype.constructor = LevelFrame;

LevelFrame.prototype.onConnectCompeleted = function() {
    if (this._operateCode === 0)
        this.sendLoadLevel();
};

LevelFrame.prototype.onLoadLevel = function() {
    this._operateCode = 0;
    if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null !== this._callBack)
        this._callBack(-1, "建立连接失败！");
};

LevelFrame.prototype.sendLoadLevel = function() {
    var GrowLevelQueryInfo = new Cmd_Data(136);
    GrowLevelQueryInfo.setcmdinfo(yl.MDM_GP_USER_SERVICE, yl.SUB_GP_GROWLEVEL_QUERY);
    GrowLevelQueryInfo.pushdword(GlobalUserItem.dwUserID);
    GrowLevelQueryInfo.pushstring((md5(GlobalUserItem.szPassword)).toUpperCase(), yl.LEN_MD5);
    GrowLevelQueryInfo.pushstring(GlobalUserItem.szMachine, yl.LEN_MACHINE_ID);
    //发送失败
    if (!this.sendSocketData(GrowLevelQueryInfo) && null !== this._callBack)
        this._callBack(-1, "发送等级查询失败！");
};

LevelFrame.prototype.onSocketEvent = function(main,sub,pData) {
    cc.log("LevelFrame.prototype.onSocketEvent " + main + " " + sub);
    if (main === yl.MDM_GP_USER_SERVICE) { //用户服务
        if (sub === yl.SUB_GP_GROWLEVEL_PARAMETER)
        // 等级参数
            this.onSubGrowLevelParameter(pData);
        else if (sub === yl.SUB_GP_GROWLEVEL_UPGRADE)
        // 用户升级
            this.onSubGrowLevelUpgrade(pData);
    }
};

LevelFrame.prototype.onSubGrowLevelParameter = function(pData) {
    GlobalUserItem.wCurrLevelID = pData.readword();
    GlobalUserItem.dwExperience = pData.readdword();
    GlobalUserItem.dwUpgradeExperience = pData.readdword();
    GlobalUserItem.lUpgradeRewardGold = GlobalUserItem.readScore(pData);
    GlobalUserItem.lUpgradeRewardIngot = GlobalUserItem.readScore(pData);

    this.onCloseSocket();
    if (null !== this._callBack)
        this._callBack(1);
};

LevelFrame.prototype.onSubGrowLevelUpgrade = function(pData) {
    var score = GlobalUserItem.readScore(pData);
    var ingot = GlobalUserItem.readScore(pData);
    var tips = pData.readstring();

    GlobalUserItem.lUserScore = score;
    GlobalUserItem.lUserIngot = ingot;
    //通知更新
    var eventListener = cc.EventCustom.new(yl.RY_USERINFO_NOTIFY);
    eventListener.obj = yl.RY_MSG_USERWEALTH;
    cc.director.getEventDispatcher().dispatchEvent(eventListener);

    if (null !== this._callBack)
        this._callBack(2, tips);
};
