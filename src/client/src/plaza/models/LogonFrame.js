/*
author: Li
登录模块
 */

function LogonFrame(view,callback) {
    BaseFrame.call(this, view, callback);
    this._plazaVersion = appdf.VersionValue(6, 7, 0, 1);
    this._stationID = yl.STATION_ID;
    var targetPlatform = cc.sys.platform;   //TODO: this is must be cc.sys.platform
    var tmp = yl.DEVICE_TYPE_LIST[targetPlatform];
    this._deviceType = tmp || yl.DEVICE_TYPE;
    this._szMachine = MultiPlatform.getInstance().getMachineId();

    this.m_angentServerList = [];
    this._tempAllRoom = [];
    this._szMobilePhone = "0123456789";
}

LogonFrame.prototype = Object.create(BaseFrame.prototype);
LogonFrame.prototype.constructor = LogonFrame;

LogonFrame.ACCOUNTS_LOGON = 0;        //账号登陆
LogonFrame.REGISTER_LOGON = 1;        //注册登陆
LogonFrame.VISTOR_LOGON   = 2;        //游客登陆
LogonFrame.WECHAT_LOGON   = 3;        //微信登陆

//连接结果
LogonFrame.prototype.onConnectCompeleted = function() {
    if (this._logonMode === LogonFrame.ACCOUNTS_LOGON)
        this.sendLogon();
    else if (this._logonMode === LogonFrame.REGISTER_LOGON)
        this.sendRegister();
    else if (this._logonMode === LogonFrame.VISTOR_LOGON)
        this.sendVisitor();
    else if (this._logonMode === LogonFrame.WECHAT_LOGON)
        this.sendThirdPartyLogin();
    else {
        this.onCloseSocket();
        if (null !== this._callBack)
            this._callBack(-1, "未知登录模式！");
    }
};

//网络信息
LogonFrame.prototype.onSocketEvent = function(main,sub,pData) {
    if ((main === yl.MDM_MB_LOGON) || (main === yl.MDM_GP_LOGON)) //登录命令
        this.onSubLogonEvent(sub, pData);
    else if (main === yl.MDM_MB_SERVER_LIST) //房间列表
        this.onRoomListEvent(sub, pData);
};

//玩家信息
LogonFrame.prototype.onSubLogonEvent = function(sub,pData)
{
    var sub = pData.getsub();
    //登录成功读取用户信息
    if (sub === yl.SUB_MB_LOGON_SUCCESS) {
        cc.log("=========== 登录成功 ===========");
        GlobalUserItem.szMachine = this._szMachine;
        GlobalUserItem.onLoadData(pData);
        GlobalUserItem.szIpAdress = MultiPlatform.getInstance().getClientIpAdress() || "";

        //重置房间
        GlobalUserItem.roomlist = [];
        if (PriRoom)
            PriRoom.getInstance().m_tabPriModeGame = [];
        this._tempAllRoom = [];
    }
    //会员信息
    else if (sub === yl.SUB_GP_MEMBER_PARAMETER_RESULT) {
        var count = pData.readword();
        GlobalUserItem.MemberList = [];
        for (i = 0; i < count; i++) {
            var item = [];
            item._order = pData.readbyte();
            item._name = pData.readstring(16);
            item._right = pData.readdword();
            item._task = pData.readdword();
            item._shop = pData.readdword();
            item._insure = pData.readdword();
            item._present = pData.readdword();
            item._gift = pData.readdword();
            GlobalUserItem.MemberList[item._order] = item;

            //dump(item, "会员信息", 6)
        }
    }
    //登录失败
    else if (sub === yl.SUB_MB_LOGON_FAILURE) {
        cc.log("=========== 登录失败 ===========");
        // CMD_MB_LogonFailure
        var cmdtable = ExternalFun.read_netdata(logincmd.CMD_MB_LogonFailure, pData);
        if (10 === cmdtable.lResultCode) {
            GlobalUserItem.setBindingAccount();
            if (null !== this._callBack)
                this._callBack(10, cmdtable.szDescribeString);
        }
        this.onCloseSocket();
        if (null !== this._callBack)
            this._callBack(-1, cmdtable.szDescribeString);
    }
    //更新APP
    else if (sub === yl.SUB_MB_UPDATE_NOTIFY) {
        var cbMustUpdate = pData.readbyte();
        var cbAdviceUpdate = pData.readbyte();
        var dwCurrentVersion = pData.readdword();
        cc.log("update_notify:" + cbMustUpdate + "#" + cbAdviceUpdate + "#" + dwCurrentVersion);
        var tmpV = appdf.ValuetoVersion(dwCurrentVersion);
        cc.log(tmpV.p + "." + tmpV.m + "." + tmpV.s + "." + tmpV.b);
        this.onCloseSocket();
        if (null !== this._callBack)
            this._callBack(-1, "版本信息错误！");
    }
};

//房间信息
LogonFrame.prototype.onRoomListEvent = function(sub,pData) {
    if (sub === yl.SUB_MB_LIST_FINISH) {	//列表完成
        this.onCloseSocket();
        if (this._logonMode === LogonFrame.ACCOUNTS_LOGON) {
            GlobalUserItem.szAccount = this._szAccount;
            GlobalUserItem.szPassword = this._szPassword;
            GlobalUserItem.bVistor = false;
            GlobalUserItem.bWeChat = false;
        }
        else if (this._logonMode === LogonFrame.REGISTER_LOGON) {
            GlobalUserItem.szAccount = this._szRegAccount;
            GlobalUserItem.szPassword = this._szRegPassword;
            GlobalUserItem.bVistor = false;
            GlobalUserItem.bWeChat = false;
        }
        else if (this._logonMode === LogonFrame.VISTOR_LOGON) {						//游客登录
            GlobalUserItem.bVistor = true;
            GlobalUserItem.bWeChat = false;
        }
        else if (this._logonMode === LogonFrame.WECHAT_LOGON) { 						//微信登陆
            GlobalUserItem.bVistor = false;
            GlobalUserItem.bWeChat = true;
        }
        // 整理列表
        for (k in this._tempAllRoom) {

            // table.sort(v, function(a, b)
            // 	return a.wSortID < b.wSortID
            // end)
            for (i = 0; i < this._tempAllRoom[k].length; i++) {
                for (var j = i; j < this._tempAllRoom[k].length; j++) {
                    if (this._tempAllRoom[k][i].wSortID > this._tempAllRoom[k][j].wSortID) {
                        var temp = this._tempAllRoom[k][i];
                        this._tempAllRoom[k][i] = this._tempAllRoom[k][j];
                        this._tempAllRoom[k][j] = temp;
                    }
                }
            }

            for (var i = 0; i < this._tempAllRoom[k].length; i++) {
                this._tempAllRoom[k][i]._nRoomIndex = i;
            }
            var roomlist = [];
            //记录游戏ID
            roomlist.push(k);
            roomlist.push(this._tempAllRoom[k]);
            if (PriRoom)
                PriRoom.getInstance().m_tabPriRoomList[k] = this._tempAllRoom[k];
            //加入缓存
            GlobalUserItem.roomlist.push(roomlist);
        }

        //登录完成
        this.onCloseSocket(); //无状态
        if (null !== this._callBack)
            this._callBack(1);
    }
    else if (sub === yl.SUB_MB_LIST_SERVER)	//列表数据
        this.onSubRoomListInfo(pData);
    else if (sub === yl.SUB_MB_AGENT_KIND) 	//代理列表
        this.onSubAngentListInfo(pData);
};

//房间列表
LogonFrame.prototype.onSubRoomListInfo = function(pData) {
    //计算房间数目
    var len = pData.getlen();
    cc.log("=============== onSubRoomListInfo ================");
    cc.log("onSubRoomListInfo:" + len);
    if ((len - Math.floor(len / yl.LEN_GAME_SERVER_ITEM) * yl.LEN_GAME_SERVER_ITEM) !== 0) {
        cc.log("roomlist_len_error" + len);
        return;
    }
    var itemcount = Math.floor(len / yl.LEN_GAME_SERVER_ITEM);
    cc.log("=============== onSubRoomListInfo ================ " + itemcount);

    //读取房间信息
    for (i = 0; i < itemcount; i++) {
        var item = new GameServerItem().onInit(pData);
        if (!item)
            break;
        //dump(item, "item", 3)
        if (null == this._tempAllRoom[item.wKindID])
            this._tempAllRoom[item.wKindID] = [];

        if (item.wServerType == yl.GAME_GENRE_PERSONAL) {
            if (GlobalUserItem.bEnableRoomCard) {
                if (PriRoom)
                    PriRoom.getInstance().m_tabPriModeGame[item.wKindID] = true;
                this._tempAllRoom[item.wKindID].push(item);
            }
        }
        else
            this._tempAllRoom[item.wKindID].push(item);
        if (item.wServerType == yl.GAME_GENRE_MATCH && MatchRoom)
            MatchRoom.getInstance().m_tabMatchModeGame[item.wKindID] = true;
    }
};

LogonFrame.prototype.onSubAngentListInfo = function(pData) {
    this.m_angentServerList = [];
    //计算数目
    var len = pData.getlen();
    cc.log("=============== onSubAngentListInfo ================");
    cc.log("onSubAngentListInfo:" + len);
    if ((len - Math.floor(len / 4) * 4) !== 0) {
        cc.log("angentlist_len_error" + len);
        return;
    }
    cc.log("=============== onSubAngentListInfo ================");
    var itemcount = Math.floor(len / 4);

    //读取房间信息
    for (i = 0; i < itemcount; i++) {
        var kind = pData.readword();
        var sort = pData.readword();

        this.m_angentServerList[kind] = {KindID: kind, SortID: sort};
    }
};

//账号登陆
LogonFrame.prototype.onLogonByAccount = function(szAccount,szPassword) {
    //数据保存
    this._szAccount = szAccount;        //"asd7002";
    this._szPassword = szPassword;      //md5("asd7002")
    this._szMobilePhone = "0123456789";
    //记录模式
    this._logonMode = LogonFrame.ACCOUNTS_LOGON;
    if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null !== this._callBack) {
        this._callBack(-1, "建立连接失败！");
        return false;
    }
    return true;
};

//游客登陆
LogonFrame.prototype.onLogonByVisitor = function() {
    //记录模式
    this._logonMode = LogonFrame.VISTOR_LOGON;
    if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null != this._callBack) {
        this._callBack(-1, "建立连接失败！");
        return false;
    }
    return true;
};

//微信登陆
LogonFrame.prototype.onLoginByThirdParty = function(szAccount, szNick, cbgender, platform) {
    //数据保存
    this._szAccount = szAccount;
    this._szNickName = szNick;
    this._cbLoginGender = cbgender;
    this._cbPlatform = platform;

    //记录模式
    this._logonMode = LogonFrame.WECHAT_LOGON;
    if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null != this._callBack) {
        this._callBack(-1, "建立连接失败！");
        return false;
    }
    return true;
};

//注册
LogonFrame.prototype.onRegister = function(szAccount,szPassword,cbGender,szSpreader) {
    //数据保存
    this._szRegAccount = szAccount;
    this._szRegPassword = szPassword;
    this._cbRegGender = cbGender;
    this._szMobilePhone = "0123456789";
    this._szSpreader = szSpreader;

    //记录模式
    this._logonMode = LogonFrame.REGISTER_LOGON;
    if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null !== this._callBack) {
        this._callBack(-1, "建立连接失败！");
        return false;
    }
    return true;
};

LogonFrame.prototype.sendLogon = function() {
    var LogonData = new Cmd_Data(235);
    var view = new Uint8Array(LogonData.m_pBuffer);

    cc.log("send logon:account-" + this._szAccount + " password-" + this._szPassword);
    LogonData.setcmdinfo(yl.MDM_MB_LOGON, yl.SUB_MB_LOGON_ACCOUNTS);
    LogonData.pushword(yl.INVALID_WORD);
    LogonData.pushdword(this._plazaVersion);
    LogonData.pushbyte(this._deviceType);

    LogonData.pushstring((md5(this._szPassword)).toUpperCase(), yl.LEN_MD5);

    LogonData.pushstring(this._szAccount.toUpperCase(), yl.LEN_ACCOUNTS);
    LogonData.pushstring(this._szMachine, yl.LEN_MACHINE_ID);
    LogonData.pushstring(this._szMobilePhone, yl.LEN_MOBILE_PHONE);

    //发送失败
    if (!this.sendSocketData(LogonData) && null !== this._callBack)
        this._callBack(-1, "发送登录失败！");
};

LogonFrame.prototype.sendVisitor = function() {
    var VisitorData = new Cmd_Data(97);
    VisitorData.setcmdinfo(yl.MDM_MB_LOGON, yl.SUB_MB_LOGON_VISITOR);

    VisitorData.pushword(yl.INVALID_WORD);
    VisitorData.pushdword(this._plazaVersion);
    VisitorData.pushbyte(this._deviceType);

    this._szMachine = MultiPlatform.getInstance().getMachineId();
    VisitorData.pushstring(this._szMachine, yl.LEN_MACHINE_ID);
    VisitorData.pushstring(this._szMobilePhone, yl.LEN_MOBILE_PHONE);

    //发送失败
    if (!this.sendSocketData(VisitorData) && null !== this._callBack)
        this._callBack(-1, "发送游客登录失败！");
};

LogonFrame.prototype.sendThirdPartyLogin = function() {
    var cmddata = ExternalFun.create_netdata(logincmd.CMD_MB_LogonOtherPlatform);
    cmddata.setcmdinfo(logincmd.MDM_MB_LOGON, logincmd.SUB_MB_LOGON_OTHERPLATFORM);

    cmddata.pushword(yl.INVALID_WORD);
    cmddata.pushdword(this._plazaVersion);
    cmddata.pushbyte(this._deviceType);
    cmddata.pushbyte(this._cbLoginGender);
    cmddata.pushbyte(this._cbPlatform);
    cmddata.pushstring(this._szAccount, 33); //LEN_USER_UIN
    cmddata.pushstring(this._szNickName, yl.LEN_NICKNAME);
    cmddata.pushstring(this._szNickName, yl.LEN_COMPELLATION);
    cmddata.pushstring(this._szMachine, yl.LEN_MACHINE_ID);
    cmddata.pushstring(this._szMobilePhone, yl.LEN_MOBILE_PHONE);

    if (!this.sendSocketData(cmddata) && null !== this._callBack)
        this._callBack(-1, "发送微信登录失败！");
};

LogonFrame.prototype.sendRegister = function() {
    var RegisterData = new Cmd_Data(358);
    RegisterData.setcmdinfo(yl.MDM_MB_LOGON, yl.SUB_MB_REGISTER_ACCOUNTS);

    RegisterData.pushword(yl.INVALID_WORD);
    RegisterData.pushdword(this._plazaVersion);
    RegisterData.pushbyte(this._deviceType);

    RegisterData.pushstring((md5(this._szRegPassword)).toUpperCase(), yl.LEN_MD5);

    RegisterData.pushword(1);
    RegisterData.pushbyte(this._cbRegGender);

    RegisterData.pushstring(this._szRegAccount, yl.LEN_ACCOUNTS);
    RegisterData.pushstring(this._szRegAccount, yl.LEN_NICKNAME);

    RegisterData.pushdword(Number(this._szSpreader));

    RegisterData.pushstring(this._szMachine, yl.LEN_MD5);
    RegisterData.pushstring(this._szMobilePhone, yl.LEN_MOBILE_PHONE);

    //发送失败
    if (!this.sendSocketData(RegisterData) && null !== this._callBack)
        this._callBack(-1, "发送注册失败！");
};