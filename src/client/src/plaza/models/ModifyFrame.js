// var BaseFrame = appdf.req(appdf.CLIENT_SRC+"plaza.models.BaseFrame")
// var ModifyFrame = class("ModifyFrame",BaseFrame)

function ModifyFrame(view, callback) {
    BaseFrame.call(this, view, callback);
}

ModifyFrame.prototype = Object.create(BaseFrame.prototype);
ModifyFrame.prototype.constructor = ModifyFrame;

// var loginCMD = appdf.req(appdf.HEADER_SRC + "CMD_LogonServer")
// var ExternalFun = appdf.req(appdf.EXTERNAL_SRC + "ExternalFun")

//修改系统头像
var MODIFY_SYSTEM_FACE = 4;
//注册绑定
var REGISTE_BINDING = 5;
// 绑定设备
var BIND_MACHINE = 6;
ModifyFrame.BIND_MACHINE = BIND_MACHINE;
// 查询个人信息
var QUERY_USERINFO = 7;
// 填写推广员
var INPUT_SPREADER = 8;
ModifyFrame.INPUT_SPREADER = INPUT_SPREADER;
// 实名认证
var INPUT_TRUEINFO = 9;

//连接结果
ModifyFrame.prototype.onConnectCompeleted = function() {

    if (this._oprateCode == 0)	//修改信息
        this.sendModifyUserInfo();
    else if (this._oprateCode == 1)	//修改登录密码
        this.sendModifyLogonPass();
    else if (this._oprateCode == 2) 	//修改银行密码
        this.sendModifyBankPass();
    else if (this._oprateCode == 3) 	//账号绑定
        this.sendAccountBinding();
    else if (this._oprateCode == MODIFY_SYSTEM_FACE) 	//修改系统头像
        this.sendModifySystemFace();
    else if (this._oprateCode == REGISTE_BINDING) //注册绑定
        this.sendAccountRegisteBinding();
    else if (this._oprateCode == BIND_MACHINE) //绑定设备
        this.sendBindMachine();
    else if (this._oprateCode == QUERY_USERINFO) // 查询信息
        this.sendQueryUserInfo();
    else if (this._oprateCode == INPUT_SPREADER) // 填写推广员
        this.sendInputSpreader();
    else if (this._oprateCode == INPUT_TRUEINFO) // 实名认证
        this.sendModifyTrueUserInfo();
    else {
        this.onCloseSocket();
        if (null != this._callBack)
            this._callBack(-1, "未知操作模式！");
    }
};

//网络信息(短连接)
ModifyFrame.prototype.onSocketEvent = function(main,sub,pData) {
    cc.log("ModifyFrame onSocketEvent"+sub);
    cc.log("ModifyFrame onSocketEvent"+yl.MDM_GP_USER_SERVICE);
    if (main == yl.MDM_GP_USER_SERVICE) { //用户服务
        if (sub == yl.SUB_GP_OPERATE_SUCCESS)
            this.onSubOperateSuccess(pData);
        else if (sub == yl.SUB_GP_OPERATE_FAILURE)
            this.onSubOperateFailure(pData);
        else if (sub == yl.SUB_GP_USER_FACE_INFO) 	//修改头像
            this.onSubUserFaceInfoResult(pData);
        else if (sub == yl.SUB_GP_USER_INDIVIDUAL)
            this.onSubQueryUserInfoResult(pData);
    }
    this.onCloseSocket();
};

//网络消息(长连接)
ModifyFrame.prototype.onGameSocketEvent = function(main,sub,pData) {

};

ModifyFrame.prototype.onSubOperateSuccess = function(pData) {
    var lResultCode = pData.readint();
    var szDescribe = pData.readstring();
    if (this._oprateCode == 1) {
        GlobalUserItem.szPassword = this._szLogonNew;
        //保存数据
        GlobalUserItem.onSaveAccountConfig();
    }
    else if (this._oprateCode == 0) {
        GlobalUserItem.cbGender = this._cbGender;
        GlobalUserItem.szNickName = this._szNickname;
        GlobalUserItem.szSign = this._szSign;
    }
    else if (this._oprateCode == 3) {
        if (null != this._callBack)
            this._callBack(2, szDescribe);
        return;
    }
    else if (this._oprateCode == REGISTE_BINDING) {
        if (null != this._callBack)
            this._callBack(2, szDescribe);
        return;
    }
    else if (this._oprateCode == BIND_MACHINE) {
        if (null != this._callBack)
            this._callBack(BIND_MACHINE, szDescribe);
        return;
    }
    else if (this._oprateCode == INPUT_SPREADER) {
        GlobalUserItem.szSpreaderAccount = this._SpreaderID
        if (null != this._callBack)
            this._callBack(INPUT_SPREADER, szDescribe);
        return;
    }
    if (null != this._callBack)
        this._callBack(1, szDescribe);
};

ModifyFrame.prototype.onSubOperateFailure = function(pData) {
    var lResultCode = pData.readint();
    var szDescribe = pData.readstring();
    if (null != this._callBack)
        this._callBack(-1, szDescribe);
};

ModifyFrame.prototype.onSubUserFaceInfoResult = function( pData ) {
    var wFaceId = pData.readword();
    var dwCustomId = pData.readdword();

    GlobalUserItem.wFaceID = wFaceId;
    GlobalUserItem.dwCustomID = dwCustomId;

    if (null != this._callBack)
        this._callBack(yl.SUB_GP_USER_FACE_INFO, "头像修改成功");
};

ModifyFrame.prototype.onSubQueryUserInfoResult = function( pData ) {
    var cmd_table = ExternalFun.read_netdata(logincmd.CMD_GP_UserIndividual, pData);
    // 附加包
    var curlen = pData.getcurlen();
    var datalen = pData.getlen();
    cc.log("*** curlen-" + curlen);
    cc.log("*** datalen-" + datalen);

    var tmpSize = 0;
    var tmpCmd = 0;
    while (curlen < datalen) {
        tmpSize = pData.readword();
        tmpCmd = pData.readword();
        if (!tmpSize || !tmpCmd)
            break;
        cc.log("*** tmpSize-" + tmpSize);
        cc.log("*** tmpCmd-" + tmpCmd);

        if (tmpCmd == logincmd.DTP_GP_UI_SPREADER) // 推广
            GlobalUserItem.szSpreaderAccount = pData.readstring(tmpSize / 2) || "";
        else if (tmpCmd == logincmd.DTP_GP_UI_QQ) {
            GlobalUserItem.szQQNumber = pData.readstring(tmpSize / 2) || "";
            cc.log("qq " + GlobalUserItem.szQQNumber);
        }
        else if (tmpCmd == logincmd.DTP_GP_UI_EMAIL) {
            GlobalUserItem.szEmailAddress = pData.readstring(tmpSize / 2) || "";
            cc.log("email " + GlobalUserItem.szEmailAddress);
        }
        else if (tmpCmd == logincmd.DTP_GP_UI_SEAT_PHONE) {
            GlobalUserItem.szSeatPhone = pData.readstring(tmpSize / 2) || "";
            cc.log("szSeatPhone " + GlobalUserItem.szSeatPhone);
        }
        else if (tmpCmd == logincmd.DTP_GP_UI_MOBILE_PHONE) {
            GlobalUserItem.szMobilePhone = pData.readstring(tmpSize / 2) || "";
            cc.log("szMobilePhone " + GlobalUserItem.szMobilePhone);
        }
        else if (tmpCmd == logincmd.DTP_GP_UI_COMPELLATION) {
            GlobalUserItem.szTrueName = pData.readstring(tmpSize / 2) || "";
            cc.log("szTrueName " + GlobalUserItem.szTrueName);
        }
        else if (tmpCmd == logincmd.DTP_GP_UI_DWELLING_PLACE) {
            GlobalUserItem.szAddress = pData.readstring(tmpSize / 2) || "";
            cc.log("szAddress " + GlobalUserItem.szAddress);
        }
        else if (tmpCmd == logincmd.DTP_GP_UI_PASSPORTID) {
            GlobalUserItem.szPassportID = pData.readstring(tmpSize / 2) || "";
            cc.log("szPassportID " + GlobalUserItem.szPassportID);
        }
        else if (tmpCmd == logincmd.DTP_GP_UI_SPREADER) {
            GlobalUserItem.szSpreaderAccount = pData.readstring(tmpSize / 2) || "";
            cc.log("szSpreaderAccount " + GlobalUserItem.szSpreaderAccount);
        }
        else if (tmpCmd == 0)
            break;
        else {
            for (var i = 0; i < tmpSize; i++) {
                if (!pData.readbyte())
                    break;
            }
        }
        curlen = pData.getcurlen();
    }
    if (null != this._callBack)
        this._callBack(logincmd.SUB_GP_USER_INDIVIDUAL);
};

ModifyFrame.prototype.sendModifyUserInfo = function() {
    var ModifyUserInfo = new Cmd_Data(817);

    ModifyUserInfo.setcmdinfo(yl.MDM_GP_USER_SERVICE, yl.SUB_GP_MODIFY_INDIVIDUAL);
    ModifyUserInfo.pushbyte(this._cbGender);
    ModifyUserInfo.pushdword(GlobalUserItem.dwUserID);
    ModifyUserInfo.pushstring(md5(GlobalUserItem.szPassword), 33);

    //////
    //附加信息
    // 昵称
    ModifyUserInfo.pushword(64);
    ModifyUserInfo.pushword(yl.DTP_GP_UI_NICKNAME);
    ModifyUserInfo.pushstring(this._szNickname, yl.LEN_NICKNAME);
    // 签名
    ModifyUserInfo.pushword(64);
    ModifyUserInfo.pushword(yl.DTP_GP_MODIFY_UNDER_WRITE);
    ModifyUserInfo.pushstring(this._szSign, yl.LEN_UNDER_WRITE);
    // qq
    ModifyUserInfo.pushword(32);
    ModifyUserInfo.pushword(logincmd.DTP_GP_UI_QQ);
    ModifyUserInfo.pushstring(GlobalUserItem.szQQNumber, yl.LEN_QQ);
    // email
    ModifyUserInfo.pushword(66);
    ModifyUserInfo.pushword(logincmd.DTP_GP_UI_EMAIL);
    ModifyUserInfo.pushstring(GlobalUserItem.szEmailAddress, yl.LEN_EMAIL);
    // 座机
    ModifyUserInfo.pushword(66);
    ModifyUserInfo.pushword(logincmd.DTP_GP_UI_SEAT_PHONE);
    ModifyUserInfo.pushstring(GlobalUserItem.szSeatPhone, yl.LEN_SEAT_PHONE);
    // 手机
    ModifyUserInfo.pushword(24);
    ModifyUserInfo.pushword(logincmd.DTP_GP_UI_MOBILE_PHONE);
    ModifyUserInfo.pushstring(GlobalUserItem.szMobilePhone, yl.LEN_MOBILE_PHONE);
    // 真实姓名
    ModifyUserInfo.pushword(32);
    ModifyUserInfo.pushword(logincmd.DTP_GP_UI_COMPELLATION);
    ModifyUserInfo.pushstring(GlobalUserItem.szTrueName, yl.LEN_COMPELLATION);
    // 联系地址
    ModifyUserInfo.pushword(256);
    ModifyUserInfo.pushword(logincmd.DTP_GP_UI_DWELLING_PLACE);
    ModifyUserInfo.pushstring(GlobalUserItem.szAddress, yl.LEN_DWELLING_PLACE);
    // 身份证
    ModifyUserInfo.pushword(38);
    ModifyUserInfo.pushword(logincmd.DTP_GP_UI_PASSPORTID);
    ModifyUserInfo.pushstring(GlobalUserItem.szPassportID, yl.LEN_PASS_PORT_ID);
    //附加信息
    //////

    if (!this.sendSocketData(ModifyUserInfo) && null != this._callBack)
        this._callBack(-1, "发送修改资料失败！");
};

ModifyFrame.prototype.sendModifyLogonPass = function() {
    var ModifyLogonPass = new Cmd_Data(136);
    ModifyLogonPass.setcmdinfo(yl.MDM_GP_USER_SERVICE, yl.SUB_GP_MODIFY_LOGON_PASS);

    ModifyLogonPass.pushdword(GlobalUserItem.dwUserID);
    ModifyLogonPass.pushstring(md5(this._szLogonNew), 33);
    ModifyLogonPass.pushstring(md5(this._szLogonOld), 33);

    if (!this.sendSocketData(ModifyLogonPass) && null != this._callBack)
        this._callBack(-1, "发送修改登录失败！");
};

ModifyFrame.prototype.sendModifyBankPass = function() {
    var ModifyInsurePass = new Cmd_Data(136);

    ModifyInsurePass.setcmdinfo(yl.MDM_GP_USER_SERVICE, yl.SUB_GP_MODIFY_INSURE_PASS);

    ModifyInsurePass.pushdword(GlobalUserItem.dwUserID);
    ModifyInsurePass.pushstring(md5(this._szBankNew), 33);
    ModifyInsurePass.pushstring(md5(this._szBankOld), 33);

    if (!this.sendSocketData(ModifyInsurePass) && null != this._callBack)
        this._callBack(-1, "发送修改银行失败！");
};

//CMD_GP_AccountBind_Exists
ModifyFrame.prototype.sendAccountBinding = function() {
    var AccountBinding = new Cmd_Data(266) //4+66+66+64+66

    AccountBinding.setcmdinfo(yl.MDM_GP_USER_SERVICE, yl.SUB_GP_ACCOUNT_BINDING_EXISTS);

    AccountBinding.pushdword(GlobalUserItem.dwUserID);
    AccountBinding.pushstring(md5(GlobalUserItem.szPassword), 33);
    AccountBinding.pushstring(this._szMachine, yl.LEN_MACHINE_ID);

    AccountBinding.pushstring(this._szAccount, 32);
    AccountBinding.pushstring(this._szPassword, 33);

    if (!this.sendSocketData(AccountBinding) && null != this._callBack)
        this._callBack(-1, "发送绑定账号失败！");
};

//CMD_GP_AccountBind
ModifyFrame.prototype.sendAccountRegisteBinding = function() {
    var AccountBinding = new Cmd_Data(331); //4+66+66+64+66+64+1
    AccountBinding.setcmdinfo(yl.MDM_GP_USER_SERVICE, yl.SUB_GP_ACCOUNT_BINDING);

    AccountBinding.pushdword(GlobalUserItem.dwUserID);
    AccountBinding.pushstring(md5(GlobalUserItem.szPassword), 33);
    AccountBinding.pushstring(this._szMachine, yl.LEN_MACHINE_ID);
    var targetPlatform = cc.Application.getInstance().getTargetPlatform();
    var tmp = yl.DEVICE_TYPE_LIST[targetPlatform];
    var deviceType = tmp || yl.DEVICE_TYPE;
    AccountBinding.pushbyte(deviceType);

    AccountBinding.pushstring(this._szAccount, 32);
    AccountBinding.pushstring(this._szPassword, 33);
    AccountBinding.pushstring(this._szSpreader, yl.LEN_ACCOUNTS);

    if (!this.sendSocketData(AccountBinding) && null != this._callBack)
        this._callBack(-1, "发送绑定账号失败！");
};

//发送修改头像
ModifyFrame.prototype.sendModifySystemFace = function( ) {
    var sysmodify = ExternalFun.create_netdata(logincmd.CMD_GP_SystemFaceInfo);
    sysmodify.setcmdinfo(yl.MDM_GP_USER_SERVICE, yl.SUB_GP_SYSTEM_FACE_INFO);
    sysmodify.pushword(this._wFaceId);
    sysmodify.pushdword(this._dwUserId);
    sysmodify.pushstring(md5(GlobalUserItem.szPassword), yl.LEN_PASSWORD);
    sysmodify.pushstring(this._szMachine, yl.LEN_MACHINE_ID);

    if (!this.sendSocketData(sysmodify) && null != this._callBack)
        this._callBack(-1, "修改用户头像失败！");
};

// 发送绑定设备
ModifyFrame.prototype.sendBindMachine = function() {
    var buffer = ExternalFun.create_netdata(logincmd.CMD_GP_ModifyMachine);
    buffer.setcmdinfo(logincmd.MDM_GP_USER_SERVICE, logincmd.SUB_GP_MODIFY_MACHINE);
    buffer.pushbyte(this._cbBind);
    buffer.pushdword(GlobalUserItem.dwUserID);
    buffer.pushstring(md5(this._strpw), yl.LEN_MD5);
    buffer.pushstring(this._szMachine, yl.LEN_MACHINE_ID);

    if (!this.sendSocketData(buffer) && null != this._callBack)
        this._callBack(-1, "绑定设备失败！");
};

// 发送查询信息
ModifyFrame.prototype.sendQueryUserInfo = function() {
    var buffer = ExternalFun.create_netdata(logincmd.CMD_GP_QueryIndividual);
    buffer.setcmdinfo(logincmd.MDM_GP_USER_SERVICE, logincmd.SUB_GP_QUERY_INDIVIDUAL);
    buffer.pushdword(GlobalUserItem.dwUserID);
    buffer.pushstring(md5(GlobalUserItem.szPassword), yl.LEN_MD5);

    if (!this.sendSocketData(buffer) && null != this._callBack)
        this._callBack(-1, "查询信息失败！");
};

// 发送填推广员
ModifyFrame.prototype.sendInputSpreader = function() {
    var ModifyUserInfo = new Cmd_Data(817);
    ModifyUserInfo.setcmdinfo(yl.MDM_GP_USER_SERVICE, yl.SUB_GP_MODIFY_INDIVIDUAL);
    ModifyUserInfo.pushbyte(GlobalUserItem.cbGender);
    ModifyUserInfo.pushdword(GlobalUserItem.dwUserID);
    ModifyUserInfo.pushstring(md5(GlobalUserItem.szPassword), 33);

    //////
    //附加信息
    // 昵称
    ModifyUserInfo.pushword(64);
    ModifyUserInfo.pushword(yl.DTP_GP_UI_NICKNAME);
    ModifyUserInfo.pushstring(GlobalUserItem.szNickName, yl.LEN_NICKNAME);
    // 签名
    ModifyUserInfo.pushword(64);
    ModifyUserInfo.pushword(yl.DTP_GP_MODIFY_UNDER_WRITE);
    ModifyUserInfo.pushstring(GlobalUserItem.szSign, yl.LEN_UNDER_WRITE);
    // qq
    ModifyUserInfo.pushword(32);
    ModifyUserInfo.pushword(logincmd.DTP_GP_UI_QQ);
    ModifyUserInfo.pushstring(GlobalUserItem.szQQNumber, yl.LEN_QQ);
    // email
    ModifyUserInfo.pushword(66);
    ModifyUserInfo.pushword(logincmd.DTP_GP_UI_EMAIL);
    ModifyUserInfo.pushstring(GlobalUserItem.szEmailAddress, yl.LEN_EMAIL);
    // 座机
    ModifyUserInfo.pushword(66);
    ModifyUserInfo.pushword(logincmd.DTP_GP_UI_SEAT_PHONE);
    ModifyUserInfo.pushstring(GlobalUserItem.szSeatPhone, yl.LEN_SEAT_PHONE);
    // 手机
    ModifyUserInfo.pushword(24);
    ModifyUserInfo.pushword(logincmd.DTP_GP_UI_MOBILE_PHONE);
    ModifyUserInfo.pushstring(GlobalUserItem.szMobilePhone, yl.LEN_MOBILE_PHONE);
    // 真实姓名
    ModifyUserInfo.pushword(32);
    ModifyUserInfo.pushword(logincmd.DTP_GP_UI_COMPELLATION);
    ModifyUserInfo.pushstring(GlobalUserItem.szTrueName, yl.LEN_COMPELLATION);
    // 联系地址
    ModifyUserInfo.pushword(256);
    ModifyUserInfo.pushword(logincmd.DTP_GP_UI_DWELLING_PLACE);
    ModifyUserInfo.pushstring(GlobalUserItem.szAddress, yl.LEN_DWELLING_PLACE);
    // 身份证
    ModifyUserInfo.pushword(38);
    ModifyUserInfo.pushword(logincmd.DTP_GP_UI_PASSPORTID);
    ModifyUserInfo.pushstring(GlobalUserItem.szPassportID, yl.LEN_PASS_PORT_ID);
    // 推广员
    ModifyUserInfo.pushword(64);
    ModifyUserInfo.pushword(logincmd.DTP_GP_UI_SPREADER);
    ModifyUserInfo.pushstring(this._SpreaderID, 32);
    //附加信息
    //////

    if (!this.sendSocketData(ModifyUserInfo) && null != this._callBack)
        this._callBack(-1, "发送修改资料失败！");
};

ModifyFrame.prototype.onModifyUserInfo = function(cbGender,szNickname,szSign) {
    this._oprateCode = 0;
    this._cbGender = cbGender;

    GlobalUserItem.cbGender = this._cbGender;    /////////ADD
    if (szNickname == null)
        this._szNickname = GlobalUserItem.szNickName;
    else
        this._szNickname = szNickname;

    this._szSign = szSign;

    if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null != this._callBack) {
        this._callBack(-1, "建立连接失败！");
        return false;
    }
    return true;
};

//发送实名认证
ModifyFrame.prototype.sendModifyTrueUserInfo = function() {

    GlobalUserItem.szTrueName = this._szTrueName;
    GlobalUserItem.szPassportID = this._szIdentityCard;

    var ModifyLogonTrueInfo = new Cmd_Data(140);
    ModifyLogonTrueInfo.setcmdinfo(yl.MDM_GP_USER_SERVICE, yl.SUB_GP_REAL_AUTH_QUERY);

    ModifyLogonTrueInfo.pushdword(GlobalUserItem.dwUserID);
    ModifyLogonTrueInfo.pushstring(md5(GlobalUserItem.szPassword), 33);

    // 真实姓名
    ModifyLogonTrueInfo.pushstring(GlobalUserItem.szTrueName, yl.LEN_COMPELLATION);
    // 身份证
    ModifyLogonTrueInfo.pushstring(GlobalUserItem.szPassportID, yl.LEN_PASS_PORT_ID);

    if (!this.sendSocketData(ModifyLogonTrueInfo) && null != this._callBack)
        this._callBack(-1, "发送修改登录失败！");
};
//建立实名认证链接
ModifyFrame.prototype.onModifyTrueUserInfo = function(szTrueName,szIdentityCard) {
    this._oprateCode = INPUT_TRUEINFO;
    this._szIdentityCard = szIdentityCard;
    this._szTrueName = szTrueName;
    if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null != this._callBack) {
        this._callBack(-1, "建立连接失败！");
        return false;
    }
    return true;
};

ModifyFrame.prototype.onModifyLogonPass = function(szOld,szNew) {
    this._oprateCode = 1;
    this._szLogonOld = szOld;
    this._szLogonNew = szNew;
    if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null != this._callBack) {
        this._callBack(-1, "建立连接失败！");
        return false;
    }
    return true;
};
ModifyFrame.prototype.onModifyBankPass = function(szOld,szNew) {
    this._oprateCode = 2;
    this._szBankOld = szOld;
    this._szBankNew = szNew;
    if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null != this._callBack) {
        this._callBack(-1, "建立连接失败！");
        return false;
    }
    return true;
};

ModifyFrame.prototype.onAccountBinding = function(szAccount,szPassword) {
    //数据保存
    this._szAccount = szAccount;
    this._szPassword = szPassword;
    this._szMachine = GlobalUserItem.szMachine;

    //记录模式
    this._oprateCode = 3;
    if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null != this._callBack) {
        this._callBack(-1, "建立连接失败！");
        return false;
    }
    return true;
};

ModifyFrame.prototype.onAccountRegisterBinding = function(szAccount, szPassword, szSpreader) {
    //数据保存
    this._szAccount = szAccount;
    this._szPassword = szPassword;
    this._szMachine = GlobalUserItem.szMachine;
    this._szSpreader = szSpreader;

    //记录模式
    this._oprateCode = REGISTE_BINDING;
    if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null != this._callBack) {
        this._callBack(-1, "建立连接失败！");
        return false;
    }
    return true;
};

ModifyFrame.prototype.onModifySystemHead = function( wFaceId ) {
    //数据保存
    this._wFaceId = wFaceId;
    this._dwUserId = GlobalUserItem.dwUserID;
    this._szMachine = GlobalUserItem.szMachine;

    //记录模式
    this._oprateCode = MODIFY_SYSTEM_FACE;
    if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null != this._callBack) {
        this._callBack(-1, "建立连接失败！");
        return false;
    }
    return true;
};

// 机器绑定
ModifyFrame.prototype.onBindingMachine = function( cbBind, strpw ) {
    // 数据保存
    this._cbBind = cbBind;
    this._strpw = strpw;
    this._szMachine = GlobalUserItem.szMachine;

    //记录模式
    this._oprateCode = BIND_MACHINE;
    if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null != this._callBack) {
        this._callBack(-1, "建立连接失败！");
        return false;
    }
    return true;
};

// 信息查询
ModifyFrame.prototype.onQueryUserInfo = function() {
    //记录模式
    this._oprateCode = QUERY_USERINFO;
    if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null != this._callBack) {
        this._callBack(-1, "建立连接失败！");
        return false;
    }
    return true;
};

// 发送填写推广员
ModifyFrame.prototype.onBindSpreader = function( nSpreaderID ) {
    this._SpreaderID = nSpreaderID;
    //记录模式
    this._oprateCode = INPUT_SPREADER;
    if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null != this._callBack) {
        this._callBack(-1, "建立连接失败！");
        return false;
    }
    return true;
};
