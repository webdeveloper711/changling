/*
author: Li
商城接口
2016_07_05 Ravioyla
 */


// var BaseFrame = appdf.req(appdf.CLIENT_SRC+"plaza.models-cc-cc.BaseFrame")
// var ShopDetailFrame = class("ShopDetailFrame",BaseFrame)
// var logincmd = appdf.req(appdf.HEADER_SRC + "CMD_LogonServer")
// var game_cmd = appdf.req(appdf.HEADER_SRC + "CMD_GameServer")
// var ExternalFun = appdf.req(appdf.EXTERNAL_SRC + "ExternalFun")

function ShopDetailFrame(view, callback) {
    BaseFrame.call(this, view, callback);
}

ShopDetailFrame.prototype = Object.create(BaseFrame.prototype);
ShopDetailFrame.prototype.constructor = ShopDetailFrame;
//连接结果
ShopDetailFrame.prototype.onConnectCompeleted = function() {

    cc.log("============ShopDetail onConnectCompeleted============");
    cc.log("ConnectCompeleted oprateCode=" + this._oprateCode);

    if (this._oprateCode == 0)	//购买
        this.sendPropertyBuy();
    else if (this._oprateCode == 1)	//使用
        this.sendPropertyUse();
    else if (this._oprateCode == 2)	//背包查询
        this.sendQueryBag();
    else if (this._oprateCode == 3)	//赠送
        this.sendPropertyTrans();
    else if (this._oprateCode == 4)
        this.sendQuerySend();			//获取赠送
    else {
        cc.log("this.onCloseSocket() 1");
        this.onCloseSocket();
        this._callBack(-1, "未知操作模式！");
    }
//    // 查询背包
//	this.onQueryBag()

};

//网络信息(短连接)
ShopDetailFrame.prototype.onSocketEvent = function(main,sub,pData) {
    cc.log("============ ShopDetail onSocketEvent ============");
    cc.log("socket event:" + main + "#" + sub);

    if (main == yl.MDM_GP_PROPERTY) { //道具命令
        if (sub == yl.SUB_GP_PROPERTY_BUY_RESULT) { //购买
            var buySuccess = this.onSubPropertyBuyResult(pData);
            if (this._use == 1 && buySuccess) {
                this._use = 0;
                if (false == this._callBack(yl.SUB_GP_PROPERTY_BUY_RESULT, tonumber(this._id))) {
                    this.sendPropertyUseNoConnect(this._id, this._itemCount);
                    return;
                }
            }
        }
        else if (sub == yl.SUB_GP_PROPERTY_USE_RESULT) 		//使用
            this.onSubPropertyUseResult(pData);
        else if (sub == yl.SUB_GP_PROPERTY_FAILURE) 			//失败
            this.onSubPropertyFailure(pData);
        else if (sub == yl.SUB_GP_QUERY_BACKPACKET_RESULT) 	//背包查询
            this.onSubQueryBag(pData);
        else if (sub == yl.SUB_GP_PROPERTY_PRESENT_RESULT) 	//赠送
            this.onSubPropertyTransResult(pData);
        else if (sub == yl.SUB_GP_GET_SEND_PRESENT_RESULT) {	//获取赠送
            this.onSubQuerySend(pData);
            //查询背包
            this.sendQueryBag();
            return;
        }
        else {
            var message = string.format("未知命令码：%d-%d", main, sub);
            this._callBack(-1, message);
        }
    }

    this._use = 0;
    this.onCloseSocket();
};

//网络消息(长连接)
ShopDetailFrame.prototype.onGameSocketEvent = function(main,sub,pData) {
    if (main == game_cmd.MDM_GR_PROPERTY) {
        cc.log("GameSocket ShopDetail #" + main + "# #" + sub + "#");
        if (sub == game_cmd.SUB_GR_GAME_PROPERTY_BUY_RESULT) {			// 道具购买
            var buySuccess = this.onSubPropertyBuyResult(pData);
            if (this._use == 1 && buySuccess) {
                this._use = 0;
                if (false == this._callBack(yl.SUB_GP_PROPERTY_BUY_RESULT, tonumber(this._id)))
                    this.onPropertyUse(this._id, this._itemCount);
            }
            this._use = 0;
        }
        else if (sub == game_cmd.SUB_GR_PROPERTY_USE_RESULT) {				// 道具使用
            var cmd_table = ExternalFun.read_netdata(game_cmd.CMD_GR_S_PropertyUse, pData);
            cc.log(cmd_table + "CMD_GR_S_PropertyUse" + 5);

            GlobalUserItem.lUserScore = cmd_table.Score;
            var tmpOrder = cmd_table.cbMemberOrder;
            if (tmpOrder > GlobalUserItem.cbMemberOrder)
                GlobalUserItem.cbMemberOrder = tmpOrder;
            if (null != this._callBack)
                this._callBack(2, cmd_table.szNotifyContent);
            //通知更新
            var eventListener = cc.EventCustom.new(yl.RY_USERINFO_NOTIFY);
            eventListener.obj = yl.RY_MSG_USERWEALTH;
            cc.director.getEventDispatcher().dispatchEvent(eventListener);
        }
        else if (sub == game_cmd.SUB_GR_PROPERTY_FAILURE) { 				// 道具失败
            var cmd_table = ExternalFun.read_netdata(game_cmd.CMD_GR_PropertyFailure, pData);
            cc.log(cmd_table, "CMD_GR_PropertyFailure", 5);
            if (null != this._callBack)
                this._callBack(0, cmd_table.szDescribeString);
        }
        else if (sub == game_cmd.SUB_GR_PROPERTY_BACKPACK_RESULT) 		// 背包查询
            this.onSubQueryBag(pData);
        else if (sub == game_cmd.SUB_GR_PROPERTY_PRESENT_RESULT) 			// 道具赠送
            this.onSubPropertyTransResult(pData);
        else if (sub == game_cmd.SUB_GR_GET_SEND_PRESENT_RESULT) 			// 获取赠送
        // 获取赠送

        // 查询背包
            this.onQueryBag();
        else if (sub == game_cmd.SUB_GR_GAME_PROPERTY_FAILURE)
            this.onSubPropertyFailure(pData);
    }
};

// CMD_LogonServer CMD_GP_S_PropertySuccess
ShopDetailFrame.prototype.onSubPropertyBuyResult = function(pData) {
    cc.log("============ ShopDetailFrame.prototype.onSubPropertyBuyResult ============");
    var cmdtable = ExternalFun.read_netdata(logincmd.CMD_GP_S_PropertySuccess, pData);

    //判断是大喇叭
    if (cmdtable.dwPropID == yl.LARGE_TRUMPET)
        GlobalUserItem.nLargeTrumpetCount = GlobalUserItem.nLargeTrumpetCount + cmdtable.dwPropCount;

    //更新用户银行金币
    GlobalUserItem.lUserInsure = cmdtable.lInsureScore;
    //更新元宝
    GlobalUserItem.lUserIngot = cmdtable.lUserMedal;
    //更新游戏豆
    GlobalUserItem.dUserBeans = cmdtable.dCash;

    //通知更新
    var eventListener = cc.EventCustom.new(yl.RY_USERINFO_NOTIFY);
    eventListener.obj = yl.RY_MSG_USERWEALTH;
    cc.director.getEventDispatcher().dispatchEvent(eventListener);

    var tips = cmdtable.szNotifyContent;
    if (1 == this._use)
        tips = null;
    if (null != this._callBack)
        this._callBack(1, tips);

    return true;
};

ShopDetailFrame.prototype.onSubPropertyUseResult = function(pData) {
    cc.log("============ ShopDetailFrame.prototype.onSubPropertyUseResult ============");
    // CMD_GP_S_PropertyUse
    var userID = pData.readdword();
    var recvUserID = pData.readdword();
    var propID = pData.readdword();
    var multiple = pData.readdword();
    var propCount = pData.readdword();
    var remainderPropCount = pData.readdword();

    GlobalUserItem.lUserScore = GlobalUserItem.readScore(pData);

    cc.log("*propCount-" + propCount);
    cc.log("*lSendLoveLiness-" + GlobalUserItem.readScore(pData));
    cc.log("*lRecvLoveLiness-" + GlobalUserItem.readScore(pData));
    cc.log("*lUseResultsGold-" + GlobalUserItem.readScore(pData));

    var propKind = pData.readdword();
    var userTime = pData.readdword();//GlobalUserItem.readScore(pData)
    var resultValidTime = pData.readdword();
    var handleCode = pData.readdword();
    var szName = pData.readstring(16);
    var tmpOrder = pData.readbyte();
    if (tmpOrder > GlobalUserItem.cbMemberOrder)
        GlobalUserItem.cbMemberOrder = tmpOrder;

    var szTip = pData.readstring();

    if (null != this._callBack)
        this._callBack(2, szTip);
    // 通知更新
    var eventListener = cc.EventCustom.new(yl.RY_USERINFO_NOTIFY);
    eventListener.obj = yl.RY_MSG_USERWEALTH;
    cc.director.getEventDispatcher().dispatchEvent(eventListener);
};

ShopDetailFrame.prototype.onSubPropertyFailure = function(pData) {
    cc.log("============ ShopDetailFrame.prototype.onSubPropertyFailure ============");
    var code = pData.readdword();
    var szTip = pData.readstring();

    if (null != this._callBack)
        this._callBack(0, szTip);
};

// CMD_LogonServer CMD_GP_S_BackpackProperty
ShopDetailFrame.prototype.onSubQueryBag = function(pData) {
    cc.log("============ ShopDetailFrame.prototype.onSubQueryBag ============");
    var userID = pData.readdword();
    var status = pData.readdword();
    var count = pData.readdword();
    var list = [];
    for (i = 0; i < count; i++) {
        var item = [];
        //道具信息
        item._count = pData.readint();
        //game_cmd.tagPropertyInfo
        item._index = pData.readword();
        item._kind = pData.readword();
        item._shop = pData.readword();
        item._area = pData.readword();
        item._service = pData.readword();
        item._commend = pData.readword();
        item._multiple = pData.readword();
        item._cbSuportMobile = pData.readbyte();
        //销售价格
        item._gold = GlobalUserItem.readScore(pData);
        item._bean = pData.readdouble();
        item._ingot = GlobalUserItem.readScore(pData);
        item._loveliness = GlobalUserItem.readScore(pData);
        //赠送内容
        item._sendLoveliness = GlobalUserItem.readScore(pData);
        item._recvLoveliness = GlobalUserItem.readScore(pData);
        item._resultGold = GlobalUserItem.readScore(pData);

        item._name = pData.readstring(32) || "";
        item._info = pData.readstring(128) || "";

        list.push(item);
        //如果是大喇叭
        if (item._index == yl.LARGE_TRUMPET)
            GlobalUserItem.nLargeTrumpetCount = item._count;
    }

    if (null != this._callBack)
        this._callBack(yl.SUB_GP_QUERY_BACKPACKET_RESULT, list);
};

ShopDetailFrame.prototype.onSubPropertyTransResult = function(pData) {
    cc.log("============ ShopDetailFrame.prototype.onSubPropertyTransResult ============");
    var userID = pData.readdword();
    var recvGameID = pData.readdword();
    var propID = pData.readdword();

    var propCount = pData.readword();
    var wType = pData.readword();

    var szName = pData.readstring(16);
    var nHandleCode = pData.readint();
    var szTip = pData.readstring();

    cc.log("////////////////////////////");
    cc.log("*userid-" + userID);
    cc.log("*recvGameID-" + recvGameID);
    cc.log("*propID-" + propID);
    cc.log("*propCount-" + propCount);
    cc.log("*type-" + wType);
    cc.log("*name-" + szName);
    cc.log("*code-" + nHandleCode);
    cc.log("*tip-" + szTip);

    if (null != this._callBack)
        this._callBack(2, szTip);
};

ShopDetailFrame.prototype.onSubQuerySend = function(pData) {
    var wCount = pData.readword();
    cc.log("count ==> " + wCount);
    // for i = 1, wCount do
    // 	var presend = ExternalFun.read_netdata(logincmd.SendPresent, pData)
    // 	cc.log(presend, "pData", 6)
    // end
};

//道具购买
ShopDetailFrame.prototype.sendPropertyBuy = function() {
    var PropertyBuy = new Cmd_Data(145);
    PropertyBuy.setcmdinfo(yl.MDM_GP_PROPERTY, yl.SUB_GP_PROPERTY_BUY);

    PropertyBuy.pushdword(GlobalUserItem.dwUserID);
    PropertyBuy.pushdword(this._id);
    PropertyBuy.pushdword(this._itemCount);
    PropertyBuy.pushbyte(this._consumeType);
    PropertyBuy.pushstring(md5(GlobalUserItem.szPassword), 33);
    PropertyBuy.pushstring(GlobalUserItem.szMachine, yl.LEN_MACHINE_ID);

    //发送失败
    if (!this.sendSocketData(PropertyBuy) && null != this._callBack)
        this._callBack(-1, "发送道具购买失败！");
};

//道具使用(非连接)
ShopDetailFrame.prototype.sendPropertyUseNoConnect = function(id,count) {
    cc.log("============ ShopDetailFrame.prototype.sendPropertyUseNoConnect ============");
    var PropertyUse = new Cmd_Data(14);
    PropertyUse.setcmdinfo(yl.MDM_GP_PROPERTY, yl.SUB_GP_PROPERTY_USE);
    PropertyUse.pushdword(GlobalUserItem.dwUserID);
    PropertyUse.pushdword(GlobalUserItem.dwUserID);
    PropertyUse.pushdword(id);
    PropertyUse.pushword(count);

    //发送失败
    if (!this.sendSocketData(PropertyUse) && null != this._callBack)
        this._callBack(-1, "发送道具使用失败！");
};


//道具使用(正常)
ShopDetailFrame.prototype.sendPropertyUse = function() {
    var PropertyUse = new Cmd_Data(14);
    PropertyUse.setcmdinfo(yl.MDM_GP_PROPERTY, yl.SUB_GP_PROPERTY_USE);
    PropertyUse.pushdword(GlobalUserItem.dwUserID);
    PropertyUse.pushdword(GlobalUserItem.dwUserID);
    PropertyUse.pushdword(this._id);
    PropertyUse.pushword(this._itemCount);

    //发送失败
    if (!this.sendSocketData(PropertyUse) && null != this._callBack)
        this._callBack(-1, "发送道具使用失败！");
};

//获取背包
ShopDetailFrame.prototype.sendQueryBag = function() {
    var QueryBag = new Cmd_Data(8);
    QueryBag.setcmdinfo(yl.MDM_GP_PROPERTY, yl.SUB_GP_QUERY_BACKPACKET);
    QueryBag.pushdword(GlobalUserItem.dwUserID);
    QueryBag.pushdword(0);

    //发送失败
    if (!this.sendSocketData(QueryBag) && null != this._callBack)
        this._callBack(-1, "发送背包查询失败！");
};

//获取赠送
ShopDetailFrame.prototype.sendQuerySend = function() {
    // CMD_GP_C_GetSendPresent
    var QueryBag = new Cmd_Data(70);
    QueryBag.setcmdinfo(yl.MDM_GP_PROPERTY, yl.SUB_GP_GET_SEND_PRESENT);
    QueryBag.pushdword(GlobalUserItem.dwUserID);
    QueryBag.pushstring(md5(GlobalUserItem.szPassword), 33);

    //发送失败
    if (!this.sendSocketData(QueryBag) && null != this._callBack)
        this._callBack(-1, "发送赠送查询失败！");
};

//赠送
ShopDetailFrame.prototype.sendPropertyTrans = function() {
    var PropertyTrans = new Cmd_Data(48);
    PropertyTrans.setcmdinfo(yl.MDM_GP_PROPERTY, yl.SUB_GP_PROPERTY_PRESENT);

    PropertyTrans.pushdword(GlobalUserItem.dwUserID);
    PropertyTrans.pushdword(this._recvid);
    PropertyTrans.pushdword(this._id);

    PropertyTrans.pushword(this._count);
    PropertyTrans.pushword(this._type);

    PropertyTrans.pushstring(this._nickname);

    cc.log("//////////////////////////////-");
    cc.log("*recvid-" + this._recvid);
    cc.log("*id-" + this._id);
    cc.log("*count-" + this._count);
    cc.log("*nickname-" + this._nickname);

    //发送失败
    if (!this.sendSocketData(PropertyTrans) && null != this._callBack)
        this._callBack(-1, "发送赠送失败！");
};

//道具购买
ShopDetailFrame.prototype.onPropertyBuy = function(type,count,id,use) {
    this._consumeType = type;
    this._itemCount = count;
    this._id = id;
    this._use = use;

    if (null != this._gameFrame && this._gameFrame.isSocketServer()) {
        var buffer = ExternalFun.create_netdata(game_cmd.CMD_GR_C_GamePropertyBuy);
        buffer.setcmdinfo(game_cmd.MDM_GR_PROPERTY, game_cmd.SUB_GR_GAME_PROPERTY_BUY);
        buffer.pushdword(GlobalUserItem.dwUserID);
        buffer.pushdword(this._id);
        buffer.pushdword(this._itemCount);
        buffer.pushbyte(this._consumeType);
        buffer.pushstring(md5(GlobalUserItem.szPassword), 33);
        buffer.pushstring(GlobalUserItem.szMachine, yl.LEN_MACHINE_ID);

        if (!this._gameFrame.sendSocketData(buffer))
            this._callBack(-1, "发送购买失败！");
    }
    else {
        //操作记录
        this._oprateCode = 0;
        if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null != this._callBack)
            this._callBack(-1, "建立连接失败！");
    }
};

//道具使用
ShopDetailFrame.prototype.onPropertyUse = function(id,count) {
    this._itemCount = count;
    this._id = id;

    if (null != this._gameFrame && this._gameFrame.isSocketServer()) {
        var buffer = ExternalFun.create_netdata(game_cmd.CMD_GR_C_PropertyUse);
        buffer.setcmdinfo(game_cmd.MDM_GR_PROPERTY, game_cmd.SUB_GR_PROPERTY_USE);
        buffer.pushdword(GlobalUserItem.dwUserID);
        buffer.pushdword(GlobalUserItem.dwUserID);
        buffer.pushdword(this._id);
        buffer.pushword(this._itemCount);
        if (!this._gameFrame.sendSocketData(buffer))
            this._callBack(-1, "发送使用失败！");
    }
    else {
        //操作记录
        this._oprateCode = 1;
        if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null != this._callBack)
            this._callBack(-1, "建立连接失败！");
    }
};

//获取背包
ShopDetailFrame.prototype.onQueryBag = function() {
    if (null != this._gameFrame && this._gameFrame.isSocketServer()) {
        var buffer = ExternalFun.create_netdata(game_cmd.CMD_GR_C_BackpackProperty);
        buffer.setcmdinfo(game_cmd.MDM_GR_PROPERTY, game_cmd.SUB_GR_PROPERTY_BACKPACK);
        buffer.pushdword(GlobalUserItem.dwUserID);
        buffer.pushstring(md5(GlobalUserItem.szPassword), yl.LEN_PASSWORD);
        if (!this._gameFrame.sendSocketData(buffer))
            this._callBack(-1, "发送查询失败！");
    }
    else {
        //操作记录
        this._oprateCode = 2;

        if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null != this._callBack)
            this._callBack(-1, "建立连接失败！");
    }
};

//赠送
ShopDetailFrame.prototype.onPropertyTrans = function(itemid,type,recvid,nickname,count) {
    this._id = itemid;
    this._type = type;
    this._recvid = recvid;
    this._nickname = nickname;
    this._count = count;

    if (null != this._gameFrame && this._gameFrame.isSocketServer()) {
        var buffer = ExternalFun.create_netdata(game_cmd.CMD_GR_C_PropertyPresent);
        buffer.setcmdinfo(game_cmd.MDM_GR_PROPERTY, game_cmd.SUB_GR_PROPERTY_PRESENT);
        buffer.pushdword(GlobalUserItem.dwUserID);
        buffer.pushdword(this._recvid);
        buffer.pushdword(this._id);
        buffer.pushword(this._count);
        buffer.pushword(this._type);
        buffer.pushstring(this._nickname);
        if (!this._gameFrame.sendSocketData(buffer))
            this._callBack(-1, "发送赠送失败！");
    }
    else {
        //操作记录
        this._oprateCode = 3;
        if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null != this._callBack)
            this._callBack(-1, "建立连接失败！");
    }
};

//获取赠送
ShopDetailFrame.prototype.onQuerySend = function() {
    if (null != this._gameFrame && this._gameFrame.isSocketServer()) {
        var buffer = ExternalFun.create_netdata(game_cmd.CMD_GR_C_GetSendPresent);
        buffer.setcmdinfo(game_cmd.MDM_GR_PROPERTY, game_cmd.SUB_GR_GET_SEND_PRESENT);
        buffer.pushdword(GlobalUserItem.dwUserID);
        buffer.pushdword(0);
        if (!this._gameFrame.sendSocketData(buffer))
            this._callBack(-1, "发送查询失败！");
    }
    else {
        //操作记录
        this._oprateCode = 4;

        if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null != this._callBack)
            this._callBack(-1, "建立连接失败！");
    }
};
