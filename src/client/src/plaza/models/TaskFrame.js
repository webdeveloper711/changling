/*
author: Li
手游任务接口
 */

function TaskFrame(view, callback) {
    BaseFrame.call(this, view, callback);

    this.m_tabTaskInfo = {};
    this.m_tabTaskInfo.tasklist = [];
}
TaskFrame.prototype = Object.create(BaseFrame.prototype);
TaskFrame.prototype.constructor = TaskFrame;

TaskFrame.prototype.getTaskInfo = function() {
    return this.m_tabTaskInfo;
};

TaskFrame.prototype.getTastList= function(){
	return this.m_tabTaskInfo.tasklist;
};

TaskFrame.prototype.updateTask = function(wTaskID, command){
	var newStatus = yl.TASK_STATUS_UNFINISH;
	if (command == yl.SUB_GP_TASK_TAKE)
		newStatus = yl.TASK_STATUS_UNFINISH;
	else if(command == yl.SUB_GP_TASK_GIVEUP)
		newStatus = yl.TASK_STATUS_WAIT;
	else if(command == yl.TASK_STATUS_SUCCESS)
		newStatus = yl.TASK_STATUS_REWARD;

    for (k in this.m_tabTaskInfo.tasklist) {
        if (this.m_tabTaskInfo.tasklist[k].wTaskID == this._wTaskID) {
            this.m_tabTaskInfo.tasklist[k].cbTaskStatus = newStatus;
            this.m_tabTaskInfo.tasklist[k].wTaskLevel = this.getLevel(newStatus);
            break;
        }
    }
	
	//任务排序
	this.sortTaskList();
};

TaskFrame.prototype.sortTaskList = function(){
	//任务排序
    this.m_tabTaskInfo.tasklist.sort(function(a,b) {
        if (a.wTaskLevel != b.wTaskLevel)
            return a.wTaskLevel > b.wTaskLevel;
        else
            return a.wTaskID < b.wTaskID;
    });
};



//连接结果
TaskFrame.prototype.onConnectCompeleted = function() {
    cc.log("============Task onConnectCompeleted============");
    cc.log("TaskFrame.prototype.onConnectCompeleted oprateCode=" + this._oprateCode);
    cc.log("============Task onConnectCompeleted============");

    if (this._oprateCode == 0)	//刷新
        this.sendTaskLoad();
    else if (this._oprateCode == 1)	//放弃
        this.sendTaskGiveup();
    else if (this._oprateCode == 2)   //领取
        this.sendTaskTake();
    else if (this._oprateCode == 3)	//奖励
        this.sendTaskReward();
    else {
        this.onCloseSocket();
        this._callBack(-1, "未知操作模式！");
    }
};

//网络信息(短连接)
TaskFrame.prototype.onSocketEvent = function(main,sub,pData) {
    cc.log("============Task onSocketEvent============");
    cc.log("socket event:" + main + "#" + sub);
    cc.log("============Task onSocketEvent============");
    var bConnect = false;
    if (main == yl.MDM_GP_USER_SERVICE) {		//用户服务
        if (sub == yl.SUB_GP_TASK_LIST) 					//全部任务
            this.onSubTaskList(pData, GlobalUserItem.bFilterTask);
        else if (sub == yl.SUB_GP_TASK_INFO) 				//任务信息
            this.onSubTaskInfo(pData);
        else if (sub == yl.SUB_GP_TASK_RESULT) 			//任务结果
            bConnect = this.onSubTaskResult(pData);
        else if (sub == yl.SUB_GP_TASK_GIVEUP_RESULT) {
        } 	//放弃结果

        else {
            var message = "未知命令码："+main+"-"+sub;
            this._callBack(-1, message);
        }
    }
    if (sub != yl.SUB_GP_TASK_LIST && !bConnect)
        this.onCloseSocket();
};

//网络消息(长连接)
TaskFrame.prototype.onGameSocketEvent = function(main,sub,pData) {
    if (main == game_cmd.MDM_GR_TASK) {
        if (sub == game_cmd.SUB_GR_TASK_INFO) 				//任务信息
            this.onSubTaskInfo(pData);
        else if (sub == game_cmd.SUB_GR_TASK_FINISH) {
        } 			//任务完成

        else if (sub == game_cmd.SUB_GR_TASK_LIST) 			//任务列表
            this.onSubTaskList(pData, true);
        else if (sub == game_cmd.SUB_GR_TASK_RESULT) 			//任务结果
            this.onSubTaskResult(pData);
        else if (sub == game_cmd.SUB_GR_TASK_GIVEUP_RESULT) {
        }	//放弃结果

    }
};

TaskFrame.prototype.onSubTaskList = function(pData, filterKind) {
    filterKind = filterKind || false;
    //计算任务数目
    var itemcount = pData.readword();
    cc.log("itemcount - " + itemcount);
    this.m_tabTaskInfo = {};
    this.m_tabTaskInfo.wTaskCount = itemcount;

    //记录任务信息
    var tasklist = [];
    //读取任务信息
    for (i = 0; i < itemcount; i++) {
        var len = pData.readword();
        var item = null;
        if (i < itemcount)
            item = new TaskItem().onInit(pData, len);
        else
            item = new TaskItem().onInit(pData, 0);

        if (!item)
            break;

        item.wTaskLevel = this.getLevel(item.cbTaskStatus);
        if (filterKind) {
            if (item.wKindID == GlobalUserItem.nCurGameKind)
                tasklist.push(item);
        }
        else
            tasklist.push(item);
    }
    this.m_tabTaskInfo.tasklist = tasklist;
    //任务排序
    this.sortTaskList();

    if (null != this._callBack)
        this._callBack(-1);
};

TaskFrame.prototype.onSubTaskInfo = function(pData) {
    var len = pData.getlen();
    //任务数目
    var itemcount = pData.readword();

    var bNotify = false;
    //进度赋值
    for (i = 0; i < itemcount; i++) {
        var wTaskID = pData.readword();
        var wTaskProgress = pData.readword();
        var cbTaskStatus = pData.readbyte();

        for (k in this.m_tabTaskInfo.tasklist) {
            if (this.m_tabTaskInfo.tasklist[k].wTaskID == wTaskID) {
                this.m_tabTaskInfo.tasklist[k].wTaskID.wTaskProgress = wTaskProgress;
                this.m_tabTaskInfo.tasklist[k].wTaskID.cbTaskStatus = cbTaskStatus;
                this.m_tabTaskInfo.tasklist[k].wTaskID.wTaskLevel = this.getLevel(cbTaskStatus);
            }
        }

        if (cbTaskStatus == yl.TASK_STATUS_SUCCESS)
            bNotify = true;
    }

    //任务排序
    this.sortTaskList();

    if (null != this._callBack)
        this._callBack(1);

    if (bNotify)
    //通知处理 (长短连接统一用一个消息)
        NotifyMgr.getInstance().excute(yl.MDM_GP_USER_SERVICE, yl.SUB_GP_TASK_INFO, null);
};

TaskFrame.prototype.onSubTaskResult = function(pData) {
    //logincmd.CMD_GP_TaskResult
    var bSuccessed = pData.readbool();
    var wCommandID = pData.readword();
    var score = GlobalUserItem.readScore(pData);
    var ingot = GlobalUserItem.readScore(pData);
    //领取奖励更新
    if (wCommandID == logincmd.SUB_GP_TASK_REWARD && true == bSuccessed) {
        GlobalUserItem.lUserScore = score;
        GlobalUserItem.lUserIngot = ingot;

        //通知更新
        var eventListener = cc.EventCustom.new(yl.RY_USERINFO_NOTIFY);
        eventListener.obj = yl.RY_MSG_USERWEALTH;
        cc.director.getEventDispatcher().dispatchEvent(eventListener);
    }
    var szTip = pData.readstring();

    var bRes = false;
    if (null != this._callBack) {
        if (bSuccessed == true)
            bRes = this._callBack(2, szTip);
        else if (bSuccessed == false)
            this._callBack(3, szTip);
    }
    return bRes;
};

TaskFrame.prototype.onSubTaskGiveupResult = function(pData) {

};

//加载任务
TaskFrame.prototype.sendTaskLoad = function() {
    var TaskLoad = new Cmd_Data(70);
    TaskLoad.setcmdinfo(yl.MDM_GP_USER_SERVICE, yl.SUB_GP_TASK_LOAD);
    var password = md5(GlobalUserItem.szPassword);
    TaskLoad.pushdword(GlobalUserItem.dwUserID);
    TaskLoad.pushstring(password, 33);

    //发送失败
    if (!this.sendSocketData(TaskLoad) && null != this._callBack)
        this._callBack(-1, "发送开通失败！");
};

//放弃任务
TaskFrame.prototype.sendTaskGiveup = function() {
    var TaskGiveup = new Cmd_Data(138);
    TaskGiveup.setcmdinfo(yl.MDM_GP_USER_SERVICE, yl.SUB_GP_TASK_GIVEUP);
    var password = md5(GlobalUserItem.szPassword);
    TaskGiveup.pushword(this._wTaskID);
    TaskGiveup.pushdword(GlobalUserItem.dwUserID);
    TaskGiveup.pushstring(password, 33);
    TaskGiveup.pushstring(GlobalUserItem.szMachine, 33);

    //发送失败
    if (!this.sendSocketData(TaskGiveup) && null != this._callBack)
        this._callBack(-1, "发送放弃失败！");
};

//领取任务
TaskFrame.prototype.sendTaskTake = function() {
    var TaskTake = new Cmd_Data(138);
    TaskTake.setcmdinfo(yl.MDM_GP_USER_SERVICE, yl.SUB_GP_TASK_TAKE);
    var password = md5(GlobalUserItem.szPassword);
    TaskTake.pushword(this._wTaskID);
    TaskTake.pushdword(GlobalUserItem.dwUserID);
    TaskTake.pushstring(password, 33);
    TaskTake.pushstring(GlobalUserItem.szMachine, 33);

    //发送失败
    if (!this.sendSocketData(TaskTake) && null != this._callBack)
        this._callBack(-1, "发送领取失败！");
};

//领取奖励
TaskFrame.prototype.sendTaskReward = function() {
    var TaskReward = new Cmd_Data(138);
    TaskReward.setcmdinfo(yl.MDM_GP_USER_SERVICE, yl.SUB_GP_TASK_REWARD);
    var password = md5(GlobalUserItem.szPassword);
    TaskReward.pushword(this._wTaskID);
    TaskReward.pushdword(GlobalUserItem.dwUserID);
    TaskReward.pushstring(password, 33);
    TaskReward.pushstring(GlobalUserItem.szMachine, 33);

    //发送失败
    if (!this.sendSocketData(TaskReward) && null != this._callBack)
        this._callBack(-1, "发送领取奖励失败！");
};

//加载任务
TaskFrame.prototype.onTaskLoad = function() {
    if (null != this._gameFrame && this._gameFrame.isSocketServer()) {
        var buffer = ExternalFun.create_netdata(game_cmd.CMD_GR_C_LoadTaskInfo);
        buffer.setcmdinfo(game_cmd.MDM_GR_TASK, game_cmd.SUB_GR_TASK_LOAD_INFO);
        buffer.pushdword(GlobalUserItem.dwUserID);
        buffer.pushstring(md5(GlobalUserItem.szPassword), yl.LEN_PASSWORD);
        if (!this._gameFrame.sendSocketData(buffer))
            this._callBack(-1, "发送加载任务失败！");
    }
    else {
        //操作记录
        this._oprateCode = 0;
        if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null != this._callBack)
            this._callBack(-1, "建立连接失败！");
    }
};

//放弃任务
TaskFrame.prototype.onTaskGiveup = function(wTaskID) {
    this._wTaskID = wTaskID;
    if (null != this._gameFrame && this._gameFrame.isSocketServer()) {
        var buffer = ExternalFun.create_netdata(game_cmd.CMD_GR_C_TakeGiveUp);
        buffer.setcmdinfo(game_cmd.MDM_GR_TASK, game_cmd.SUB_GR_TASK_GIVEUP);
        buffer.pushword(wTaskID);
        buffer.pushdword(GlobalUserItem.dwUserID);
        buffer.pushstring(md5(GlobalUserItem.szPassword), yl.LEN_PASSWORD);
        buffer.pushstring(GlobalUserItem.szMachine, yl.LEN_MACHINE_ID);
        if (!this._gameFrame.sendSocketData(buffer))
            this._callBack(-1, "发送放弃任务失败！");
    }
    else {
        //操作记录
        this._oprateCode = 1;

        if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null != this._callBack)
            this._callBack(-1, "建立连接失败！");
    }
};

//领取任务
TaskFrame.prototype.onTaskTake = function(wTaskID) {
    this._wTaskID = wTaskID;
    if (null != this._gameFrame && this._gameFrame.isSocketServer()) {
        var buffer = ExternalFun.create_netdata(game_cmd.CMD_GR_C_TakeTask);
        buffer.setcmdinfo(game_cmd.MDM_GR_TASK, game_cmd.SUB_GR_TASK_TAKE);
        buffer.pushword(wTaskID);
        buffer.pushdword(GlobalUserItem.dwUserID);
        buffer.pushstring(md5(GlobalUserItem.szPassword), yl.LEN_PASSWORD);
        buffer.pushstring(GlobalUserItem.szMachine, yl.LEN_MACHINE_ID);
        if (!this._gameFrame.sendSocketData(buffer))
            this._callBack(-1, "发送领取任务失败！");
    }
    else {
        //操作记录
        this._oprateCode = 2;

        if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null != this._callBack)
            this._callBack(-1, "建立连接失败！");
    }
};

//领取奖励
TaskFrame.prototype.onTaskReward = function(wTaskID) {
    this._wTaskID = wTaskID;
    if (null != this._gameFrame && this._gameFrame.isSocketServer()) {
        var buffer = ExternalFun.create_netdata(game_cmd.CMD_GR_C_TaskReward);
        buffer.setcmdinfo(game_cmd.MDM_GR_TASK, game_cmd.SUB_GR_TASK_REWARD);
        buffer.pushword(wTaskID);
        buffer.pushdword(GlobalUserItem.dwUserID);
        buffer.pushstring(md5(GlobalUserItem.szPassword), yl.LEN_PASSWORD);
        buffer.pushstring(GlobalUserItem.szMachine, yl.LEN_MACHINE_ID);
        if (!this._gameFrame.sendSocketData(buffer))
            this._callBack(-1, "发送领取奖励失败！");
    }
    else {
        //操作记录
        this._oprateCode = 3;

        if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null != this._callBack)
            this._callBack(-1, "建立连接失败！");
    }
};

//获取优先级
TaskFrame.prototype.getLevel = function(wStatus) {
    //优先级赋值
    var wLevel = 1;
    if (wStatus == yl.TASK_STATUS_UNFINISH)
        wLevel = 3;
    else if (wStatus == yl.TASK_STATUS_REWARD)
        wLevel = 0;
    else if (wStatus == yl.TASK_STATUS_FAILED)
        wLevel = 2;
    else if (wStatus == yl.TASK_STATUS_SUCCESS)
        wLevel = 1;
    else if (wStatus == yl.TASK_STATUS_WAIT)
        wLevel = 4;

    return wLevel;
};
