/*
author: Li
手游任务单项
 */
	

// local TaskItem = class("TaskItem")

function TaskItem(pData) {

    //基本信息
    this.wTaskID = 0;				//任务标识
    this.wTaskType = 0;				//任务类型
    this.wTaskObject = 0;				//任务目标
    this.cbPlayerType = 0;				//玩家类型
    this.wKindID = 0;				//类型标识
    this.dwTimeLimit = 0;				//时间限制

    //奖励信息
    this.lStandardAwardGold = 0;				//奖励金币
    this.lStandardAwardMedal = 0;				//奖励奖牌
    this.lMemberAwardGold = 0;				//奖励金币
    this.lMemberAwardMedal = 0;				//奖励奖牌

    //描述信息
    this.szTaskName = "";				//任务名称
    this.szTaskDescribe = "";			//任务描述

    //进度信息
    this.wTaskProgress = 0;				//任务进度
    this.cbTaskStatus = yl.TASK_STATUS_WAIT;				//任务状态

}

TaskItem.prototype.onInit = function(pData,len) {
    if (pData == null) {
        cc.log("TaskItem-onInit-null");
        return;
    }
    // tagTaskParameter


    this.wTaskID = pData.readword();		//任务标识
    this.wTaskType = pData.readword();		//任务类型
    this.wTaskObject = pData.readword();		//任务目标
    this.cbPlayerType = pData.readbyte();		//玩家类型
    this.wKindID = pData.readword();		//类型标识
    this.dwTimeLimit = pData.readdword();		//时间限制

    this.lStandardAwardGold = pData.readscore();		//奖励金币
    this.lStandardAwardMedal = pData.readscore();		//奖励奖牌
    this.lMemberAwardGold = pData.readscore();		//奖励金币
    this.lMemberAwardMedal = pData.readscore();		//奖励奖牌

    this.szTaskName = pData.readstring(64);			//任务名称
    //cc.log("task len ==> " + len)
    if (len == 0) {
        cc.log("describe length - " + len);
        this.szTaskDescribe = pData.readstring();	//任务描述
    }
    else
        this.szTaskDescribe = pData.readstring((len - 173) / 2);	//任务描述
    return this;
};

TaskItem.prototype.readScore = function(dataBuffer) {
    // if (this._int64 == null)
    //     this._int64 = Integer64.new().addTo(this);
    // dataBuffer.readscore(this._int64);
    // return this._int64.getvalue();
    return dataBuffer.readscore();
};
