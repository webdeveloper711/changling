/*
    author : kil
    date :  2017-12-4
 */
LAYER_CREATEPRIROOME = 1002;
var KIND_GAME = "400";

var HELP_LAYER_NAME = "__introduce_help_layer__";
var HELP_BTN_NAME = "__introduce_help_button__";
var VOICE_BTN_NAME = "__voice_record_button__";
var VOICE_LAYER_NAME = "__voice_record_layer__";


var BT_CLOSE_DLG = 1;
var BT_CONFIRM = 2;

var ClientScene = cc.Scene.extend({
    BT_EXIT 		: 1,
    BT_CONFIG 		: 2,
    BT_USER 		: 3,
    BT_SHOP_ENTITY  : 4,    // 实体
    BT_SHOP			: 6,
    BT_ACTIVITY		: 7,
    BT_RECORD		: 8,
    BT_BANK			: 9,
    BT_BAG			: 10,
    BT_FRIEND		: 11,
    BT_TASK			: 12,
    BT_RANK			: 13,
    BT_QUICKSTART	: 14,
    BT_SHOP_BEAN    : 15,    // 游戏豆
    BT_BOX			: 16,
    BT_CHECKIN		: 17,
    BT_TRUMPET		: 18,
    BT_PERSON		: 19,
    BT_ACTIVE	    : 21,
    BT_HELP			: 22,
    BT_SHARE        : 25,
    BT_NOTICE       : 26,
    BT_INGOT        : 27,
    BT_ZHANJI       : 28,
    BT_CLOSE        : 29,
    BT_EXTENSION    : 30,
    BT_VERIFIED     : 31,
    ctor : function () {
        this._super();
        this.onCreate();
    },
    onEnterTransitionDidFinish : function () {
        this.onEnterTransitionFinish();
    },
    // 进入场景而且过渡动画结束时候触发。
    onEnterTransitionFinish : function() {
        this.registerNotifyList();
        if (!GlobalUserItem.bIsAngentAccount)
        //查询赠送
            this._shopDetailFrame.onQuerySend();
        else {
            if (null != this.m_touchFilter) {
                this.m_touchFilter.dismiss();
                this.m_touchFilter = null;
            }
        }

        //根据会员等级确定裁剪资源
        var vipIdx = GlobalUserItem.cbMemberOrder || 0;
        //裁切头像
        var head = new HeadSprite().createNormal(GlobalUserItem, 69);
        if (null != head) {
            head.setPosition(cc.p(110, 50));
            this._AreaTop.addChild(head);
            this._head = head;

            var frameFile = null;
            var scaleRate = null;
            if (vipIdx == 0) {
                frameFile = "sp_frame_0_0_0.png";
                scaleRate = null;
            }

            this._head.enableHeadFrame(true, {_framefile : frameFile, _scaleRate : scaleRate});
        }

        // TODO : KIL
        // this.m_actBoxAni = ExternalFun.loadTimeLine("plaza/BoxAni.csb");
        // ExternalFun.SAFE_RETAIN(this.m_actBoxAni);
        // this._btBox.stopAllActions();
        // this.m_actBoxAni.gotoFrameAndPlay(0, true);
        // this._btBox.runAction(this.m_actBoxAni);
        //请求公告
        this.requestNotice();
        return this;
    },

    onExitTransitionDidStart : function () {
        this.onExitTransitionStart();
    },

    // 退出场景而且开始过渡动画时候触发。
    onExitTransitionStart : function() {
        // this._sceneLayer.unregisterScriptKeypadHandler();

        return this;
    },

    onExit : function() {
        if (this._gameFrame.isSocketServer()) {
            this._gameFrame.onCloseSocket();
        }
        this.disconnectFrame();

        ExternalFun.SAFE_RELEASE(this.m_actBoxAni);
        this.m_actBoxAni = null;
        ExternalFun.SAFE_RELEASE(this.m_actStartBtnAni);
        this.m_actStartBtnAni = null;
        ExternalFun.SAFE_RELEASE(this.m_actChargeBtnAni);
        this.m_actChargeBtnAni = null;
        ExternalFun.SAFE_RELEASE(this.m_actCoinAni);
        this.m_actCoinAni = null;

        // TODO ;
        // this.releasePublicRes();
        this.removeListener();

        // this.unregisterNotify();
        // removebackgroundcallback(); // TODO: AppDelegate method // AppDelegate::getAppInstance()->setBackgroundListener(0);

        if (PriRoom)
            PriRoom.getInstance().onExitPlaza();

        return this;
    },


    // 初始化界面
    onCreate : function() {
        cc.log("ClientScene.onCreate");
        this.m_listener = null;
        this.cachePublicRes();
        this.m_actBoxAni = null;
        this.m_actStartBtnAni = null;
        this.m_actChargeBtnAni = null;
        var self = this;

        //保存进入的游戏记录信息
        GlobalUserItem.m_tabEnterGame = null;
        //上一个场景
        this.m_nPreTag = null;
        //喇叭发送界面
        this.m_trumpetLayer = null;
        GlobalUserItem.bHasLogon = true;
        // TODO:
        this._gameFrame = new GameFrameEngine(this,function (code,result) {
            self.onRoomCallBack(code, result);
        });

        if (PriRoom)
            PriRoom.getInstance().onEnterPlaza(this, this._gameFrame);

        // 背景
        this._bg = new ccui.ImageView(res.background_png);
        this._bg.setPosition(yl.WIDTH / 2, yl.HEIGHT / 2);
        this.addChild(this._bg);

        var btcallback = function(sender, target) {
            if (target == ccui.Widget.TOUCH_ENDED) {
                cc.log("this is clientscene btncallback");
                self.onButtonClickedEvent(sender.getTag(), sender);
            }
        };

        this._sceneRecord = [];

        this._sceneLayer = new cc.Layer();
        this._sceneLayer.setContentSize(yl.WIDTH,yl.HEIGHT);
        this.addChild(this._sceneLayer);
        // TODO :
        //     //返回键事件
        //     this._sceneLayer.registerScriptKeypadHandler(function(event) {
        //         if (event == "backClicked") {
        //             if (this._popWait == null) {
        //                 if (this._sceneRecord.length > 0) {
        //                     var cur_layer = this._sceneLayer.getChildByTag(this._sceneRecord[this._sceneRecord.length]);
        //                     if (cur_layer && cur_layer.onKeyBack) {
        //                         if (cur_layer.onKeyBack() == true) {
        //                             return;
        //                         }
        //                     }
        //                 }
        //                 this.onKeyBack();
        //             }
        //         }
        //     });
        //     this._sceneLayer.setKeyboardEnabled(true);

        //加载csb资源
        var tempLayer = ExternalFun.loadRootCSB(res.PlazzLayer_json, this);
        var rootLayer = tempLayer.rootlayer;
        var csbNode = tempLayer.csbnode;
        this.m_plazaLayer = csbNode;


        this.m_touchFilter = new PopWait();
        this.m_touchFilter.show(this,"请稍候！");
        cc.log("this is test : " + typeof this.m_touchFilter);
        // 定时关闭
        this.m_touchFilter.runAction(new cc.Sequence(new cc.DelayTime(3),
            new cc.CallFunc(function() {
                if (null != self.m_touchFilter) {
                    self.m_touchFilter.dismiss();
                    self.m_touchFilter = null;
                }
                // 网络断开
                self.disconnectFrame();
            })
            )
        );

        //顶部区域
        var areaTop = csbNode.getChildByName("top_bg");
        this._AreaTop = areaTop;

        if (areaTop != null) {
            //昵称
            this._nikename = areaTop.getChildByName("t_nikename");
            this._userID = areaTop.getChildByName("txt_ID");
            this._userBeanNum = areaTop.getChildByName("txt_beanNum");

            //宝箱
            this.m_btnBox = areaTop.getChildByName("btn_box");
            this.m_btnBox.setTag(this.BT_BOX);
            this.m_btnBox.setVisible(false);
            this.m_btnBox.addTouchEventListener(btcallback);
            var box = ExternalFun.loadCSB(res.BoxAni_json, areaTop);
            box.setPosition(this.m_btnBox.getPosition());
            this._btBox = box;

            //查看信息
            this._btPersonInfo = areaTop.getChildByName("bt_person");
            this._btPersonInfo.setTag(this.BT_PERSON);
            this._btPersonInfo.addTouchEventListener(btcallback);

            //金币
            this._gold = areaTop.getChildByName("atlas_coin");
            var btn = areaTop.getChildByName("btn_take");
            btn.setTag(this.BT_SHOP);
            btn.addTouchEventListener(btcallback);
            btn.setVisible(true);

            //元宝
            this._ingot = areaTop.getChildByName("atlas_ingot");
            btn = areaTop.getChildByName("btn_exchange");
            btn.setTag(this.BT_SHOP_ENTITY);
            btn.addTouchEventListener(btcallback);
            btn.setVisible(true);

            //游戏豆
            this._bean = areaTop.getChildByName("atlas_bean");
            btn = areaTop.getChildByName("btn_charge");
            btn.setTag(this.BT_SHOP_BEAN);
            btn.addTouchEventListener(btcallback);

            //战绩
            btn = areaTop.getChildByName("btn_zhanji");
            btn.setTag(this.BT_ZHANJI);
            btn.setVisible(false);
            btn.addTouchEventListener(btcallback);

            //推广
            btn = areaTop.getChildByName("btn_extension");
            btn.setTag(this.BT_EXTENSION);
            btn.addTouchEventListener(btcallback);

            //实名认证
            btn = areaTop.getChildByName("btn_realName");
            btn.setTag(this.BT_VERIFIED);
            btn.addTouchEventListener(btcallback);

            //设置
            btn = areaTop.getChildByName("btn_set");
            btn.setTag(this.BT_CONFIG);
            btn.addTouchEventListener(btcallback);
            this._btConfig = btn;

            //分享
            btn = areaTop.getChildByName("btn_share");
            btn.setTag(this.BT_SHARE);
            btn.addTouchEventListener(btcallback);
            btn.setVisible(false);
            this._btEveryday = btn;

            //返回登陆
            btn = areaTop.getChildByName("btn_close");
            btn.setTag(this.BT_CLOSE);
            btn.addTouchEventListener(btcallback);
            this._btClose = btn;

            //返回
            this._btExit = areaTop.getChildByName("btn_return");
            this._btExit.setTag(this.BT_EXIT);
            this._btExit.setPosition(cc.p(this._btConfig.getPositionX(), this._btConfig.getPositionY()));
            this._btExit.addTouchEventListener(btcallback);

            //公告
            btn = areaTop.getChildByName("btn_gonggao");
            btn.setTag(this.BT_NOTICE);
            btn.setVisible(false);
            btn.addTouchEventListener(btcallback);

        }

        //底部区域
        var areaBottom = csbNode.getChildByName("bottom_bg");
        this._AreaBottom = areaBottom;

        if (areaBottom != null) {
            //等级信息及进度条
            this._level = areaBottom.getChildByName("atlas_levels");
            this._level.setString(GlobalUserItem.wCurrLevelID + "");
            this._levelpro = areaBottom.getChildByName("bar_progress");
            this._levelpro.setPercent(0);

            //好友
            btn = areaBottom.getChildByName("btn_friend");
            btn.setTag(this.BT_FRIEND);
            btn.addTouchEventListener(btcallback);
            this.m_btnFriend = btn;

            //客服
            btn = areaBottom.getChildByName("btn_help");
            btn.setTag(this.BT_HELP);
            btn.addTouchEventListener(btcallback);

            //商城
            btn = areaBottom.getChildByName("btn_shop");
            btn.setTag(this.BT_SHOP);
            btn.addTouchEventListener(btcallback);

            //排行
            btn = areaBottom.getChildByName("btn_rank");
            btn.setTag(this.BT_RANK);
            btn.addTouchEventListener(btcallback);

            //活动
            btn = areaBottom.getChildByName("btn_Active");
            btn.setTag(this.BT_ACTIVE);
            btn.addTouchEventListener(btcallback);

            //背包
            btn = areaBottom.getChildByName("btn_bag");
            btn.setTag(this.BT_BAG);
            btn.setVisible(false);
            btn.addTouchEventListener(btcallback);
            this.m_btnBag = btn;

            //快速开始
            btn = areaBottom.getChildByName("image_start");
            btn.setTouchEnabled(true);
            this.m_btnQuickStart = btn;
            btn.setTag(this.BT_QUICKSTART);
            btn.setVisible(false);
            btn.addTouchEventListener(btcallback);
            var quickStart = ExternalFun.loadCSB(res.StartBtnAni_json, areaBottom);
            quickStart.setPosition(btn.getPosition());
            this.m_quickStart = quickStart;

            //元宝
            btn = areaBottom.getChildByName("btn_bag_0");
            btn.setTag(this.BT_INGOT);
            btn.setVisible(false);
            btn.addTouchEventListener(btcallback);

            //任务
            btn = areaBottom.getChildByName("btn_task");
            btn.setTag(this.BT_TASK);
            btn.addTouchEventListener(btcallback);
            this.m_btnTask = btn;
        }

        //喇叭
        this._notify = csbNode.getChildByName("sp_trumpet_bg");
        this._notify.setVisible(false);
        btn = this._notify.getChildByName("btn_trumpet");
        btn.setTag(this.BT_TRUMPET);
        btn.setVisible(false);
        btn.addTouchEventListener(btcallback);

        var stencil  = new cc.Sprite();
        stencil.setAnchorPoint(cc.p(0, 0.5));
        stencil.setTextureRect(cc.rect(0, 0, 627, 50));
        this._notifyClip = new cc.ClippingNode(stencil);
        this._notifyClip.setAnchorPoint(cc.p(0, 0.5));
        this._notifyClip.setInverted(false);
        this._notifyClip.setPosition(350, 20);
        this._notify.addChild(this._notifyClip);

        this._notifyText = new cc.LabelTTF("", res.round_body_ttf, 24);
        this._notifyClip.addChild(this._notifyText);
        this._notifyText.setColor(cc.color(255, 191, 123, 255));
        this._notifyText.setAnchorPoint(cc.p(0, 0.5));
        this._notifyText.enableStroke(cc.color(79, 48, 35, 255), 1);

        this.m_tabInfoTips = [];
        this._tipIndex = 1;
        this.m_nNotifyId = 0;
        // 系统公告列表
        this.m_tabSystemNotice = [];
        this._sysIndex = 1;
        // 公告是否运行
        this.m_bNotifyRunning = false;

        this.m_bSingleGameMode = false;
        if  (yl.SINGLE_GAME_MODOLE && _gameList.length == 1) {
            //默认使用第一个游戏 TODO : check index
            var index = 0;
            for (i = 0; i < _gameList.length; i++) {
                if (_gameList[i]._KindID == KIND_GAME) { //查找营口麻将
                    index = i;
                    break;
                }
            }

            var entergame = _gameList[index];
            if (null != entergame) {
                this.m_bSingleGameMode = true;
                this.updateEnterGameInfo(entergame);
                GlobalUserItem.nCurGameKind = Number(entergame._KindID);

                GlobalUserItem.bFilterTask = true;
                if (MatchRoom != undefined && true == MatchRoom.getInstance().isCurrentGameOpenMatch(GlobalUserItem.nCurGameKind)) {
                    if (PriRoom && false == PriRoom.getInstance().isCurrentGameOpenPri(GlobalUserItem.nCurGameKind) &&
                        0 == GlobalUserItem.GetGameRoomCount(GlobalUserItem.nCurGameKind)) {
                        this.onChangeShowMode(MatchRoom.LAYTAG.MATCH_ROOMLIST);
                    } else {
                        this.onChangeShowMode(MatchRoom.LAYTAG.MATCH_TYPELIST);
                    }
                } else if (PriRoom != undefined && true == PriRoom.getInstance().isCurrentGameOpenPri(GlobalUserItem.nCurGameKind)) {
                    this.onChangeShowMode(PriRoom.LAYTAG.LAYER_ROOMLIST);
                } else {
                    this.onChangeShowMode(yl.SCENE_ROOMLIST);
                }

                this._bg.loadTexture("src/client/res/plaza/backgroud_plazz.png");
            } else {
                this.onChangeShowMode(yl.SCENE_GAMELIST);
            }

        } else {
            this.onChangeShowMode(yl.SCENE_GAMELIST);
        }

        var shopDetail = function(result, msg) {
            if (result == yl.SUB_GP_QUERY_BACKPACKET_RESULT) {
                self._shopDetailFrame = null;
            }

            // 是否处理锁表
            var bHandleLockGame = true;
            if (PriRoom) {
                // 是否锁表、是否更新游戏、是否锁私人房
                var lockGame, updateGame, lockPriGame = PriRoom.getInstance().onEnterPlazaFinish();
                if (lockGame) {
                    if (! updateGame && ! lockPriGame) {
                        bHandleLockGame = false;
                    }
                    if (null != self._checkInFrame) {
                        self._checkInFrame.onCloseSocket();
                        self._checkInFrame = null;
                    }

                    if (null != self.m_touchFilter) {
                        self.m_touchFilter.dismiss();
                        self.m_touchFilter = null;
                    }
                } else {
                    // 任务信息查询
                    self.queryTaskInfo();
                }
            } else {
                // 任务信息查询
                self.queryTaskInfo();
            }

            if (! bHandleLockGame) {
                // 普通房锁表登陆
                cc.log("ClinetScene normal lock login");
                var lockRoom = GlobalUserItem.GetGameRoomInfo(GlobalUserItem.dwLockServerID);
                if (GlobalUserItem.dwLockKindID == GlobalUserItem.nCurGameKind && null != lockRoom) {
                    GlobalUserItem.nCurRoomIndex = lockRoom._nRoomIndex;
                    self.onStartGame();
                }
            }
        };

        this._shopDetailFrame = new ShopDetailFrame(this, shopDetail);

        var checkInInfo = function(result, msg, subMessage) {
            var bRes = false;
            if (result == 1) {
                if (false == GlobalUserItem.bTodayChecked) {
                    self.onChangeShowMode(yl.SCENE_CHECKIN, null , function() {
                        // 广告显示在签到界面之上
                        if (GlobalUserItem.isShowAdNotice()) {
                            var webview = new WebViewLayer(self);
                            var runScene = cc.director.getRunningScene();
                            if (null != runScene) {
                                runScene.addChild(webview, yl.ZORDER.Z_AD_WEBVIEW);
                            }
                        }
                    });
                    self._checkInFrame = null;
                }else if (GlobalUserItem.cbMemberOrder != 0) {
                    self._checkInFrame.sendCheckMemberGift();
                    bRes = true;
                } else {
                    // 显示广告
                    if (GlobalUserItem.isShowAdNotice()) {
                        var webview = new WebViewLayer(self);
                        var runScene = cc.director.getRunningScene();
                        if (null != runScene) {
                            runScene.addChild(webview, yl.ZORDER.Z_AD_WEBVIEW);
                        }
                    }
                    self._checkInFrame = null;
                }
            } else if (result == self._checkInFrame.QUERYMEMBERGIFT) {
                self._checkInFrame = null;
            }
            if (null != self._checkInFrame && self._checkInFrame.QUERYMEMBERGIFT == result) {
                if (true == subMessage) {
                    self.onChangeShowMode(yl.SCENE_CHECKIN);
                } else {
                    // 显示广告
                    if (GlobalUserItem.isShowAdNotice()) {
                        var webview = new WebViewLayer(self);
                        var runScene = cc.director.getRunningScene();
                        if (null != runScene) {
                            runScene.addChild(webview, yl.ZORDER.Z_AD_WEBVIEW);
                        }
                    }
                }
                self._checkInFrame = null;
            }

            if (null == self._checkInFrame) {
                if (null != self.m_touchFilter) {
                    self.m_touchFilter.dismiss();
                    self.m_touchFilter = null;
                }
            }
            return bRes;
        };
        this._checkInFrame = new CheckinFrame(this, checkInInfo);
        this.m_bFirstQueryCheckIn = true;

        // setbackgroundcallback(function (bEnter) {
        //     if (type(this.onBackgroundCallBack) == "function") {
        //         this.onBackgroundCallBack(bEnter)
        //     }
        // });

        this.initListener();

        //快速开始
        this.m_bQuickStart = false;

        //游戏喇叭列表
        this.m_gameTrumpetList = {};
        this.m_spGameTrumpetBg = null;

        // 回退
         this.m_bEnableKeyBack = true;

        // 金币、金币动画
        this.m_nodeCoinAni = null;
        this.m_actCoinAni = null;
    },

    registerNotifyList : function() {
        // 代理帐号不显示
        if (GlobalUserItem.bIsAngentAccount) {
            return;
        }
        NotifyMgr.getInstance().excuteSleepNotfiy();
        var tmp = FriendMgr.getInstance().getUnReadNotify();
        if (tmp.length > 0) {
            NotifyMgr.getInstance().excute(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_APPLYFOR_NOTIFY, null);
            //聊天
            var notify = NotifyMgr.getInstance().createNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_USER_CHAT_NOTIFY);
            notify.name = "client_friend_chat";
            notify.group = "client_friend";
            notify.fun = this.onNotify(notify);
            NotifyMgr.getInstance().registerNotify(notify);
            //申请好友通知
            var notify2 = NotifyMgr.getInstance().createNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_APPLYFOR_NOTIFY);
            notify2.name = "client_friend_apply";
            notify2.group = "client_friend";
            notify2.fun = this.onNotify(notify);
            NotifyMgr.getInstance().registerNotify(notify2);
            //回应通知
            var notify3 = NotifyMgr.getInstance().createNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_RESPOND_NOTIFY);
            notify3.name = "client_friend_response";
            notify3.group = "client_friend";
            notify3.fun = this.onNotify(notify);
            NotifyMgr.getInstance().registerNotify(notify3);
            //邀请通知
            var notify4 = NotifyMgr.getInstance().createNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_INVITE_GAME_NOTIFY);
            notify4.name = "client_friend_invite";
            notify4.group = "client_friend";
            notify4.fun = this.onNotify(notify);
            NotifyMgr.getInstance().registerNotify(notify4);
            //私人房邀请
            var notify5 = NotifyMgr.getInstance().createNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_INVITE_PERSONAL_NOTIFY);
            notify5.name = "client_pri_friend_invite";
            notify5.fun = this.onNotify(notify);
            notify5.group = "client_friend";
            NotifyMgr.getInstance().registerNotify(notify5);
            //分享通知
            var notify6 = NotifyMgr.getInstance().createNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_USER_SHARE_NOTIFY);
            notify6.name = "client_friend_share";
            notify6.fun = this.onNotify(notify);
            notify6.group = "client_friend";
            NotifyMgr.getInstance().registerNotify(notify6);

            //喇叭通知
            var notify7 = NotifyMgr.getInstance().createNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_TRUMPET_NOTIFY);
            notify7.name = "trumpet";
            notify7.group = "client_trumpet";
            notify7.fun = this.onNotify(notify);
            NotifyMgr.getInstance().registerNotify(notify7);

            // 任务
            var notify8 = NotifyMgr.getInstance().createNotify(yl.MDM_GP_USER_SERVICE, yl.SUB_GP_TASK_INFO);
            notify8.name = "client_task_info";
            notify8.group = "client_task";
            notify8.fun = this.onNotify(notify);
            NotifyMgr.getInstance().registerNotify(notify8);
        }
    },

    unregisterNotify : function() {
        // 代理帐号不显示
        if (GlobalUserItem.bIsAngentAccount) {
            return;
        }

        NotifyMgr.getInstance().unregisterNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_USER_CHAT_NOTIFY, "client_friend_chat");
        NotifyMgr.getInstance().unregisterNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_APPLYFOR_NOTIFY, "client_friend_apply");
        NotifyMgr.getInstance().unregisterNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_RESPOND_NOTIFY, "client_friend_response");
        NotifyMgr.getInstance().unregisterNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_INVITE_GAME_NOTIFY, "client_friend_invite");
        NotifyMgr.getInstance().unregisterNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_USER_SHARE_NOTIFY, "client_friend_share");
        NotifyMgr.getInstance().unregisterNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_INVITE_PERSONAL_NOTIFY, "client_pri_friend_invite");
        NotifyMgr.getInstance().unregisterNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_TRUMPET_NOTIFY, "trumpet");
        NotifyMgr.getInstance().unregisterNotify(yl.MDM_GP_USER_SERVICE, yl.SUB_GP_TASK_INFO, "client_task_info");
    },

    onBackgroundCallBack : function(bEnter) {
        if (!bEnter) {
            cc.log("onBackgroundCallBack not bEnter");
            var curScene = this._sceneRecord[this._sceneRecord.length-1];
            if (curScene == yl.SCENE_GAME) {

            }
            if (curScene == yl.SCENE_ROOM) {
                this.onKeyBack();
            }

            if (null != this._gameFrame && this._gameFrame.isSocketServer() && GlobalUserItem.bAutoConnect) {
                this._gameFrame.onCloseSocket();
            }

            this.disconnectFrame();

            //关闭好友服务器
            FriendMgr.getInstance().reSetAndDisconnect();

            this.dismissPopWait();

            // 关闭介绍
            var runScene = cc.director.getRunningScene();
            if (null != runScene && null != runScene.getChildByName(HELP_LAYER_NAME)) {
                runScene.removeChildByName(HELP_LAYER_NAME);
            }
        }
        else {
            cc.log("onBackgroundCallBack  bEnter");
            if (this._sceneRecord.length > 0) {
                var curScene = this._sceneRecord[this._sceneRecord.length-1];
                if (curScene == yl.SCENE_GAME) {
                    if (this._gameFrame.isSocketServer() == false && GlobalUserItem.bAutoConnect) {
                        this._gameFrame.OnResetGameEngine();
                        this.onStartGame();
                    }
                }
            }
            //连接好友服务器
            FriendMgr.getInstance().reSetAndLogin();

            //查询财富
            if (GlobalUserItem.bJftPay) {
                //通知查询
                var eventListener = new cc.EventCustom(yl.RY_JFTPAY_NOTIFY);
                cc.eventManager.dispatchEvent(eventListener);
            }
        }
    },

    onRoomCallBack : function (code,message) {
        cc.log("onRoomCallBack." + code);
        if (message) {
            showToast(this, message, 1);
        }
        if (code == -1) {
            this.dismissPopWait();
            var curScene = this._sceneRecord[this._sceneRecord.length-1];
            if (curScene == yl.SCENE_ROOM || curScene == yl.SCENE_GAME) {
                cc.log("onRoomCallBack curscene is " + curScene);
                var curScene = this._sceneLayer.getChildByTag(curScene);
                if (curScene && curScene.onExitRoom) {
                    curScene.onExitRoom();
                } else {
                    this.onChangeShowMode(yl.SCENE_ROOMLIST);
                }
            }
        }
    },

    onReQueryFailure : function(code, msg) {
        this.dismissPopWait();
        if (null != msg && typeof(msg) == "string") {
            showToast(this, msg, 2);
        }
    },

    onEnterRoom : function() {
        cc.log("client onEnterRoom");
        this.dismissPopWait();
        // 防作弊房间
        if (GlobalUserItem.isAntiCheat()) {
            cc.log("防作弊");
            if (this._gameFrame.SitDown(yl.INVALID_TABLE, yl.INVALID_CHAIR)) {
                this.showPopWait();
            }
        }

        //如果是快速游戏
        var entergame = this.getEnterGameInfo();
        var self = this;
        if (this.m_bQuickStart && null != entergame) {
            this.m_bQuickStart = false;
            var t = yl.INVALID_TABLE;
            var c = yl.INVALID_CHAIR;
            // 找桌
            var bGet = false;
            for( var j=0; j<this._gameFrame._tableStatus.length; j++) {
                var v = this._gameFrame._tableStatus[j];
                if (v.cbTableLock == 0 && v.cbPlayerType == 0) {
                    var st = j;
                    var chaircount = self._gameFrame._wChairCount;
                    for (var i = 0; i < chaircount; i++) {
                        var sc = i;
                        if (null == self._gameFrame.getTableUserItem(st, sc)) {
                            t = st;
                            c = sc;
                            bGet = true;
                            break;
                        }
                    }
                }
                if (bGet) {
                    return true;
                }
            }
            cc.log(" fast enter " + t + " ## " + c);
            if (this._gameFrame.SitDown(t, c)) {
                this.showPopWait();
            }
            else {
                //自定义房间界面处理登陆成功消息
                var entergame = this.getEnterGameInfo();
                if (null != entergame) {
                    var modulestr = entergame._KindName.replace("%.", "/");
                    var targetPlatform = cc.sys.platform;
                    var customRoomFile = "";
                    if ((cc.sys.WIN32 === targetPlatform) || (cc.sys.MOBILE_BROWSER === targetPlatform) || (cc.sys.DESKTOP_BROWSER === targetPlatform)) {
                        customRoomFile = "game-cc/" + modulestr + "src/views/GameRoomListLayer.js";
                    } else {
                        customRoomFile = "game-cc/" + modulestr + "src/views/GameRoomListLayer.js";   //GameRoomListLayer.luac
                    }
                    if (cc.FileUtils.getInstance().isFileExist(customRoomFile)) {
                        if (appdf.req(customRoomFile).onEnterRoom(this._gameFrame)) {
                            this.showPopWait();
                        } else {
                            //断网、退出房间
                            if (null != this._gameFrame) {
                                this._gameFrame.onCloseSocket();
                                GlobalUserItem.nCurRoomIndex = -1;
                            }
                        }
                    }
                }

                if (this._sceneRecord.length > 0) {
                    // TODO : check index
                    if (this._sceneRecord[this._sceneRecord.length - 1] == yl.SCENE_GAME) {
                        this.onChangeShowMode();
                    } else if (this._sceneRecord[this._sceneRecord.length] == yl.SCENE_ROOM) {
                        this._gameFrame.setViewFrame(this._sceneLayer.getChildByTag(yl.SCENE_ROOM));
                    } else {
                        this.onChangeShowMode(yl.SCENE_ROOM, this.m_bQuickStart);
                        this.m_bQuickStart = false;
                    }
                }
            }
        }
    },

    onEnterTable : function() {
        cc.log("ClientScene onEnterTable");

        if (PriRoom && GlobalUserItem.bPrivateRoom) {
            // 动作记录
            PriRoom.getInstance().m_nLoginAction = PriRoom.L_ACTION.ACT_ENTERTABLE;
        }
        // TODO : check index
        var tag = this._sceneRecord[this._sceneRecord.length - 1];
        if (tag == yl.SCENE_GAME) {
            this._gameFrame.setViewFrame(this._sceneLayer.getChildByTag(yl.SCENE_GAME));
        } else {
            this.onChangeShowMode(yl.SCENE_GAME);
        }
    },

    //启动游戏
    onStartGame : function () {
        var entergame = this.getEnterGameInfo();
        if (null == entergame) {
            showToast(this, "游戏信息获取失败", 3);
        }
        this.getEnterGameInfo().nEnterRoomIndex = GlobalUserItem.nCurRoomIndex;
        if (null != this.m_touchFilter) {
            this.m_touchFilter.dismiss();
            this.m_touchFilter = null;
        }

        this.showPopWait();
        this._gameFrame.onInitData();
        this._gameFrame.setKindInfo(GlobalUserItem.nCurGameKind, entergame._KindVersion);
        // TODO : check index
        var curScene = this._sceneRecord[this._sceneRecord.length - 1];
        this._gameFrame.setViewFrame(this);
        this._gameFrame.onCloseSocket();
        this._gameFrame.onLogonRoom();
    },

    onCleanPackage : function(name) {
        // if (! name) {
        // end
        // for k ,v in pairs(package.loaded) do
        // 	if k != null {
        // 		if type(k) == "string" {
        // 			if string.find(k,name) != null || string.find(k,name) != null {
        // 				cc.log("package kill." + k)
        // 				package.loaded[k] = null
        // 			end
        // 		end
        // 	end
        // end
    },

    onLevelCallBack : function(result,msg) {
        if (typeof(msg) == "string" && "" != msg) {
            showToast(this, msg, 2);
        }

        this.dismissPopWait();
        this._level.setString(GlobalUserItem.wCurrLevelID + "");

        if (GlobalUserItem.dwUpgradeExperience > 0) {
            var scalex = GlobalUserItem.dwExperience / GlobalUserItem.dwUpgradeExperience;
            if (scalex > 1) {
                scalex = 1;
            }
            this._levelpro.setPercent(100 * scalex);
        } else {
            this._levelpro.setPercent(1);
        }

        if (1 == result) {
            if (null != this._levelFrame && this._levelFrame.isSocketServer()) {
                this._levelFrame.onCloseSocket();
                this._levelFrame = null;
            }
        }
    },

    onUserInfoChange : function( event ) {
        cc.log("//////////userinfo change notify////////////");

        var msgWhat = event.obj;

        if (null != msgWhat && msgWhat == yl.RY_MSG_USERHEAD) {
            //更新头像
            if (null != this._head) {
                this._head.updateHead(GlobalUserItem);
            }
        }

        if (null != msgWhat && msgWhat == yl.RY_MSG_USERWEALTH) {
            //更新财富
            this.updateInfomation();
        }
    },

    initListener : function() {
        var self = this;
        this.m_listener = cc.EventListener.create({
            event: cc.EventListener.CUSTOM,
            eventName: yl.RY_USERINFO_NOTIFY,
            callback: function(event) {
                self.onUserInfoChange(event);
            }
        });
        cc.eventManager.addListener(this.m_listener, 1);

        // this.m_listener = new cc.EventListenerCustom(yl.RY_USERINFO_NOTIFY, handler(this, this.onUserInfoChange));
        // cc.director.getEventDispatcher().addEventListenerWithSceneGraphPriority(this.m_listener, this);
    },

    removeListener : function( ) {
        if (null != this.m_listener) {
            cc.eventManager.removeCustomListeners(this.m_listener);
            this.m_listener = null;
        }
    },

    onKeyBack : function () {
        cc.log("onKeyBack");
        if (!this.m_bEnableKeyBack) {
            return;
        }
        // TODO : check index
        var curScene = this._sceneRecord[this._sceneRecord.length - 1];
        cc.log("SCENE_VERIFIED" + yl.SCENE_VERIFIED);

        cc.log("curScene" + this._sceneRecord);
        if (curScene) {
            if (curScene == yl.SCENE_ROOM) {
                this._gameFrame.onCloseSocket();
                GlobalUserItem.nCurRoomIndex = -1;
            } else if (curScene == yl.SCENE_GAMELIST || curScene == yl.SCENE_CREATROOM) {
                if (PriRoom) {
                    PriRoom.getInstance().exitRoom();
                }
            }
        }
        this.onChangeShowMode();
    },

    //跑马灯更新
    onChangeNotify : function(msg) {
        this._notifyText.stopAllActions();
        if (!msg || !msg.str || msg.str.length == 0) {
            this._notifyText.setString("");
            this.m_bNotifyRunning = false;
            this._tipIndex = 1;
            this._sysIndex = 1;
            return;
        }

        this.m_bNotifyRunning = true;
        var msgcolor = msg.color || cc.color(255, 191, 123, 255);
        this._notifyText.setVisible(false);
        this._notifyText.setString(msg.str);
        this._notifyText.setTextColor(msgcolor);

        if (true == msg.autoremove) {
            msg.showcount = msg.showcount || 0;
            msg.showcount = msg.showcount - 1;
            if (msg.showcount <= 0) {
                this.removeNoticeById(msg.id);
            }
        }

        var self = this;
        var tmpWidth = this._notifyText.getContentSize().width;
        this._notifyText.runAction(
            new cc.Sequence(
                new cc.CallFunc(function () {
                    self._notifyText.setPosition(yl.WIDTH - 200, 0);
                    self._notifyText.setVisible(true);
                }),
                cc.setPositionTo.create(16 + (tmpWidth / 172), cc.p(0 - tmpWidth, 0)),
                new cc.CallFunc(function () {
                    var tipsSize = 0;
                    var tips = {};
                    var index = 1;
                    if (0 != self.m_tabInfoTips.length) {
                        // 喇叭等
                        var tmp = self._tipIndex + 1;
                        if (tmp > self.m_tabInfoTips.length) {
                            tmp = 1;
                        }
                        self._tipIndex = tmp;
                        self.onChangeNotify(self.m_tabInfoTips[self._tipIndex]);
                    } else {
                        // 系统公告
                        var tmp = self._sysIndex + 1;
                        if (tmp > self.m_tabSystemNotice.length) {
                            tmp = 1;
                        }
                        self._sysIndex = tmp;
                        self.onChangeNotify(self.m_tabSystemNotice[self._sysIndex]);
                    }
                })
            )
        );
    },
    GPSNoFindText : function() {
        showToast(this, "获取定位信息失败，您无法进入到游戏房间内！", 2);
    },
    ExitClient : function() {
        // this._sceneLayer.setKeyboardEnabled(false);
        GlobalUserItem.nCurRoomIndex = -1;
        this.updateEnterGameInfo(null);
        var scene = new LogonScene();
        cc.director.runScene(new cc.TransitionFade(1, scene));

        GlobalUserItem.reSetData();
        //读取配置
        GlobalUserItem.LoadData();
        //断开好友服务器
        FriendMgr.getInstance().reSetAndDisconnect();
        //通知管理
        NotifyMgr.getInstance().clear();
        // 私人房数据
        if (PriRoom) {
            PriRoom.getInstance().reSet();
        }
        // 回放
        // if (GameVideo) {
            // GameVideo.getInstance().reSet(); TODO
        // }
    },

    //按钮事件
    onButtonClickedEvent : function(tag, ref) {
        cc.log("ClientScene : onButtonClickedEvent : tag = " + tag);
        if (tag == this.BT_EXIT) {
            this.onKeyBack();
        }
        else if (tag == this.BT_QUICKSTART) { //No need in changling
            if (GlobalUserItem.isAngentAccount()) {
                return;
            }
            //判断当前场景
            // TODO : check index
            var curScene = this._sceneRecord[this._sceneRecord.length - 1];
            if (PriRoom && ! PriRoom.enableQuickStart(curScene)) {
                return;
            }

            //默认使用第一个游戏
            var entergame = this.getEnterGameInfo();
            if (null == entergame) {
                entergame = _gameList[0];
                this.updateEnterGameInfo(entergame);
            }
            if (null == entergame) {
                cc.log("未找到游戏信息");
                return;
            }
            //快速开始
            this.m_bQuickStart = true;

            //游戏列表
            if (curScene == yl.SCENE_GAMELIST) {
                this.quickStartGame();
            } else if (curScene == yl.SCENE_ROOMLIST) { 			//游戏房间列表
                GlobalUserItem.nCurRoomIndex = entergame.nEnterRoomIndex || GlobalUserItem.normalRoomIndex(entergame._KindID);
                var roominfo = GlobalUserItem.GetRoomInfo(GlobalUserItem.nCurRoomIndex);
                if (null == roominfo) {
                    showToast(this, "房间信息获取失败！", 2);
                    return;
                }
                if (roominfo.wServerType == yl.GAME_GENRE_PERSONAL) {
                    //showToast(this, "房卡房间不支持快速开始！", 2)
                    return;
                }
                if (this.roomEnterCheck()) {
                    //进入房间
                    this.onStartGame();
                }
            } else if (curScene == yl.SCENE_ROOM) { 				//房间桌子列表
                //坐下
                this.onEnterRoom();
            }
        }
        else {
            if (tag != this.BT_CONFIG && tag != this.BT_PERSON && tag != this.BT_SHARE && tag != this.BT_NOTICE && tag != this.BT_CLOSE) {
                if (GlobalUserItem.isAngentAccount()) {
                    return;
                }
            }

            if (this._sceneRecord.length > 0) {
                curScene = this._sceneRecord[this._sceneRecord.length - 1];
                if (curScene == yl.SCENE_ROOM) {
                    if (tag == this.BT_QUICKSTART) { //自动找位
                        var room = this._sceneLayer.getChildByTag(curScene);
                        if (room) {
                            room.onQuickStart();
                        }
                        return;
                    }
                }
            }
            if (tag == this.BT_CONFIG) {
                this.onChangeShowMode(yl.SCENE_OPTION);
            } else if (tag == this.BT_SHARE) {
                this.onChangeShowMode(yl.SCENE_SHARE);
            } else if (tag == this.BT_CLOSE) {
                this.ExitClient();
            } else if (tag == this.BT_NOTICE) {
                //请求公告
                this.requestNotice();
                this.onChangeShowMode(yl.SCENE_NOTICE);
            } else if (tag == this.BT_USER) {
                this.onChangeShowMode(yl.SCENE_USERINFO);
            } else if (tag == this.BT_BANK) {
                // 当前金币场
                var rom = GlobalUserItem.GetRoomInfo();
                if (null != rom) {
                    if (rom.wServerType != yl.GAME_GENRE_GOLD) {
                        showToast(this, "当前房间禁止操作银行!", 2);
                        return;
                    }
                }
                this.onChangeShowMode(yl.SCENE_BANK);
            } else if (tag == this.BT_RANK) {
                var param = this.m_nPreTag;
                this.onChangeShowMode(yl.SCENE_RANKINGLIST, param)
            } else if (tag == this.BT_TASK) {
                if (false == GlobalUserItem.bEnableTask) {
                    showToast(this, "当前功能暂未开放,敬请期待!", 2);
                    return;
                }
                NotifyMgr.getInstance().hideNotify(this.m_btnTask);
                this.onChangeShowMode(yl.SCENE_TASK);
            } else if (this.cur_Scene == yl.SCENE_BANKRECORD) {
                this.onChangeShowMode(yl.SCENE_BANK);
            } else if (tag == this.BT_CHECKIN) {
                this.onChangeShowMode(yl.SCENE_CHECKIN);
            }else if (tag == this.BT_SHOP_ENTITY) {
                this.onChangeShowMode(yl.SCENE_SHOP, ShopLayer.CBT_ENTITY);
            }else if (tag == this.BT_SHOP_BEAN) {
                this.onChangeShowMode(yl.SCENE_SHOP, ShopLayer.CBT_BEAN);
            }else if ((tag == this.BT_BOX) || (tag == this.BT_ACTIVE)) {
                this.onChangeShowMode(yl.SCENE_EVERYDAY);
            }else if (tag == this.BT_EXTENSION) {
                this.onChangeShowMode(yl.SCENE_EXTENSION);
            }else if (tag == this.BT_BAG) {
                this.onChangeShowMode(yl.SCENE_BAG);
            }else if (tag == this.BT_FRIEND) {
                NotifyMgr.getInstance().hideNotify(this.m_btnFriend);
                this.onChangeShowMode(yl.SCENE_FRIEND);
            }else if (tag == this.BT_TRUMPET) {
                this.getTrumpetSendLayer();
            }else if (tag == this.BT_PERSON) {
                this.onChangeShowMode(yl.SCENE_USERINFO);
            }else if (tag == this.BT_SHOP) {
                this.onChangeShowMode(yl.SCENE_SHOP);
            }else if (tag == this.BT_INGOT) {
                this.onChangeShowMode(yl.SCENE_INGOT);
            }else if (tag == this.BT_ZHANJI) {
                this.onChangeShowMode(PriRoom.LAYTAG.LAYER_COMBATLOG, null, null, 1);
                //this.onChangeShowMode(PriRoom.LAYTAG.LAYER_CREATEPRIROOME,null,null,1)
            }else if (tag == this.BT_HELP) {
                this.onChangeShowMode(yl.SCENE_FEEDBACK);
            }else if (tag == this.BT_VERIFIED) {
                this.onChangeShowMode(yl.SCENE_VERIFIED);
            } else {
                showToast(this, "功能尚未开放，敬请期待！", 2);
            }
        }
    },

    //切换页面
    onChangeShowMode : function(nTag, param, transitionCallBack) {
        cc.log("nTag = " + nTag);
        var tag = nTag;
        var curtag; 			//当前页面ID
        var bIn; 				//进入判断
        //当前页面
        if (this._sceneRecord.length > 0) {
            curtag = this._sceneRecord[this._sceneRecord.length - 1];
        }
        ExternalFun.dismissTouchFilter();
        // TODO : check index
        if ((2 <= this._sceneRecord.length) && (curtag == this._sceneRecord[this._sceneRecord.length - 2])) {
            this._sceneRecord.splice(this._sceneRecord.length - 1, 1);
            return;
        }

        //退出判断
        if (tag == null || tag == undefined) {
            //返回登录
            if (this._sceneRecord.length < 2) {
                //todo strange code
                // this.ExitClient();
                return;
            }
            //清除记录
            var cur = this._sceneRecord[this._sceneRecord.length - 1];
            // this._sceneRecord[this._sceneRecord.length - 1] = null;
            this._sceneRecord.pop();
            //上一页面
            tag = this._sceneRecord[this._sceneRecord.length - 1];
            //当前为游戏界面
            if (cur == yl.SCENE_GAME) {
                //防作弊房间
                if (GlobalUserItem.isAntiCheat()) {
                    tag = yl.SCENE_ROOMLIST;
                    var bHaveRoomList = false;
                    var tmpRecord = [];
                    for (i = 0; i < this._sceneRecord.length; i++) {
                        if (this._sceneRecord[i] != yl.SCENE_ROOM) {
                            tmpRecord.push(this._sceneRecord[i]);
                        }

                        if (this._sceneRecord[i] == yl.SCENE_ROOMLIST) {
                            bHaveRoomList = true;
                        }
                    }
                    if (false == bHaveRoomList) {
                        // TODO : check index
                        tmpRecord[tmpRecord.length] = yl.SCENE_ROOMLIST;
                    }
                    this._sceneRecord = tmpRecord;

                    this._gameFrame.onCloseSocket();
                    // 私人房
                } else if (GlobalUserItem.bPrivateRoom) {
                    if (PriRoom) {
                        tag = yl.SCENE_GAMELIST;
                        // 清理记录
                        this._sceneRecord = [];
                        if (!this.m_bSingleGameMode) {
                            // 非单游戏模式, 保存游戏列表界面记录
                            // TODO : check index
                            this._sceneRecord[0] = yl.SCENE_GAMELIST;
                        }
                        // TODO : check index
                        this._sceneRecord[this._sceneRecord.length] = tag;
                        PriRoom.getInstance().exitGame();
                    }
                    this._gameFrame.onCloseSocket();
                    //网络已经关闭 回退到房间列表
                } else if (this._gameFrame.isSocketServer() != true) {
                    tag = yl.SCENE_ROOMLIST;
                    bHaveRoomList = false;
                    tmpRecord = [];
                    for (var i = 0; i < this._sceneRecord; i++) {
                        if (this._sceneRecord[i] != yl.SCENE_ROOM) {
                            tmpRecord.push(this._sceneRecord[i]);
                        }

                        if (this._sceneRecord[i] == yl.SCENE_ROOMLIST) {
                            bHaveRoomList = true;
                        }
                    }
                    if (false == bHaveRoomList) {
                        // TODO : check index
                        tmpRecord[tmpRecord.length] = yl.SCENE_ROOMLIST;
                    }
                    this._sceneRecord = tmpRecord;
                } else if (tag != yl.SCENE_ROOM) { //回退到房间桌子界面
                    // TODO : check index
                    this._sceneRecord[this._sceneRecord.length] = yl.SCENE_ROOM;
                    tag = yl.SCENE_ROOM;
                }

                // 任务查询
                this.queryTaskInfo();
                // 游戏币查询
                this.queryUserScoreInfo();

                // 游戏喇叭关闭
                if (null != this.m_spGameTrumpetBg) {
                    this.m_spGameTrumpetBg.stopAllActions();
                    this.m_spGameTrumpetBg.removeFromParent();
                }
                // 玩法按钮
                if (this.getChildByName(HELP_BTN_NAME)) {
                    this.removeChildByName(HELP_BTN_NAME);
                }
                // 移除语音按钮
                if (this.getChildByName(VOICE_BTN_NAME)) {
                    this.removeChildByName(VOICE_BTN_NAME);
                }
                // 移除语音
                this.cancelVoiceRecord();
                // TODO : check function vs object
                if ((null != this._gameFrame) && typeof(this._gameFrame.clearVoiceQueue) == "object") {
                    this._gameFrame.clearVoiceQueue();
                }
                // 关闭介绍
                var runScene = cc.director.getRunningScene();
                if ((null != runScene) && (null != runScene.getChildByName(HELP_LAYER_NAME))) {
                    runScene.removeChildByName(HELP_LAYER_NAME);
                }
            }
        } else {
            //查找已有
            var oldIndex;
            for (i = 0; i < this._sceneRecord.length; i++) {
                if (this._sceneRecord[i] == tag) {
                    // TODO : check index
                    oldIndex = i + 1;
                    break;
                }
            }

            if (oldIndex == null || oldIndex == undefined) { //新界面
                bIn = true; //进入判断
                // TODO : check index
                this._sceneRecord[this._sceneRecord.length] = tag; //记录ID
            } else {
                //重复过滤
                if (oldIndex == this._sceneRecord.length) {
                    return;
                }

                //回退至已有记录
                // TODO : check index
                for (i = this._sceneRecord.length - 1; i > oldIndex; i--) {
                    this._sceneRecord[i].pop();
                }
            }
        }
        //上一个页面
        this.m_nPreTag = this._sceneRecord[this._sceneRecord.length - 1];

        //当前页面
        if (curtag != null) {
            var cur_layer = this._sceneLayer.getChildByTag(curtag);
            if (cur_layer) {
                cur_layer.stopAllActions();
                //游戏界面不触发切换动画
                if (tag == yl.SCENE_GAME || curtag == yl.SCENE_GAME) {
                    cur_layer.removeFromParent();
                    ExternalFun.playPlazzBackgroudAudio();
                } else {
                    //动画判断
                    var curAni;
                    if (!bIn) {
                        curAni = new cc.MoveTo(0.3, cc.p(yl.WIDTH, 0)); //退出动画
                    } else {
                        curAni = new cc.MoveTo(0.3, cc.p(-yl.WIDTH, 0)); //返回动画
                    }
                    cur_layer.runAction(new cc.Sequence(curAni, new cc.RemoveSelf(true)));
                }
            }
        }

        //目标页面
        var dst_layer = this.getTagLayer(tag, param);

        if (dst_layer != null) {
            //游戏界面不触发切换动画
            if (tag == yl.SCENE_GAME) {
                //this._sceneLayer.setKeyboardEnabled(false);
                this._sceneLayer.addChild(dst_layer);
                if (dst_layer.onSceneAniFinish) {
                    dst_layer.onSceneAniFinish();
                }
                //this._sceneLayer.setKeyboardEnabled(true);
            } else {
                //触摸过滤
               // ExternalFun.popupTouchFilter();
                // this._sceneLayer.setKeyboardEnabled(false);
                if (!bIn) {
                    dst_layer.setPosition(-yl.WIDTH, 0);
                } else {
                    dst_layer.setPosition(yl.WIDTH, 0);
                }

                this._sceneLayer.addChild(dst_layer);
                dst_layer.stopAllActions();
                dst_layer.runAction(new cc.Sequence(
                    new cc.MoveTo(0.3, cc.p(0, 0)),
                    new cc.CallFunc(function () {
                        if (dst_layer.onSceneAniFinish) {
                            dst_layer.onSceneAniFinish();
                        }
                       // this._sceneLayer.setKeyboardEnabled(true);
                        ExternalFun.dismissTouchFilter();
                        if (typeof(transitionCallBack) == "function") {
                            transitionCallBack();
                        }
                    })
                    )
                )
            }
        }
        else {
            cc.log("dst_layer is null");
            this.ExitClient();
            return;
        }
        if (tag == yl.SCENE_GAME || tag == yl.SCENE_ROOM) {
            this._gameFrame.setViewFrame(dst_layer);
        }

        this._AreaBottom.stopAllActions();
        this._AreaTop.stopAllActions();

        if (tag == yl.SCENE_GAMELIST || tag == yl.SCENE_ROOM || tag == PriRoom.LAYTAG.LAYER_ROOMLIST) {
            this._AreaTop.runAction(new cc.MoveTo(0.3, cc.p(667, 750)));
            this._AreaBottom.runAction(new cc.MoveTo(0.3, cc.p(667, 0)));
            this._AreaTop.setVisible(true);
            this._AreaBottom.setVisible(true);
            this._btClose.setVisible(true);
            this._btConfig.setVisible(true);
        }
        else if (tag == yl.SCENE_ROOMLIST || tag == yl.SCENE_BOINLIST) {
            this._AreaTop.runAction(new cc.MoveTo(0.3, cc.p(667, 750)));
            this._AreaBottom.runAction(new cc.MoveTo(0.3, cc.p(667, -170)));
            this._AreaTop.setVisible(true);
            this._AreaBottom.setVisible(false);
            this._btExit.setVisible(true);
            this._btExit.setEnabled(true);
            this._head.setVisible(true);
            this._btConfig.setVisible(false);
        }
        else {
            cc.log("onChangeShowMode:_btConfig:" + this._btConfig);
            this._btConfig.setVisible(true);
            if (PriRoom && PriRoom.haveBottomTop(tag)) {
                if (this._AreaBottom.getPositionY() < 0) {
                    cc.log("this._AreaBottom.getPositionY() < 0");
                    this._AreaTop.setPosition(cc.p(667, 900));
                    this._AreaBottom.setPosition(cc.p(667, -170));
                    this._AreaTop.setVisible(false);
                    this._AreaBottom.setVisible(false);
                    this._btClose.setVisible(false);
                } else {
                    cc.log("this._AreaBottom.getPositionY() >= 0");
                    this._AreaTop.setPosition(cc.p(667, 900));
                    this._AreaBottom.setPosition(cc.p(667, -170));
                    this._AreaTop.setVisible(false);
                    this._AreaBottom.setVisible(false);
                    this._btClose.setVisible(false);
                }
            } else {
                if (tag == yl.SCENE_GAME) {
                    this._AreaTop.setPosition(cc.p(667, 900));
                    this._AreaBottom.setPosition(cc.p(667, -170));
                    this._AreaTop.setVisible(false);
                    this._AreaBottom.setVisible(false);
                    this._btClose.setVisible(false);
                } else {

                    this._AreaTop.runAction(new cc.MoveTo(0.3, cc.p(667, 900)));
                    this._AreaBottom.runAction(new cc.MoveTo(0.3, cc.p(667, -170)));
                    this._AreaTop.setVisible(false);
                    this._AreaBottom.setVisible(false);
                    this._btClose.setVisible(false);
                }
            }
        }


        var bRoomList = true;
        var bEnableBackBtn = false;
        if (PriRoom) {
            bRoomList = PriRoom.isRoomListLayer(tag);
            bEnableBackBtn = PriRoom.enableBackBtn(tag);
        } else {
            bRoomList = (tag == yl.SCENE_ROOMLIST);
        }


        if (bRoomList) {
            var temp = true;
            // 单游戏
            if (this.m_bSingleGameMode) {
                if (MatchRoom && true == MatchRoom.getInstance().isCurrentGameOpenMatch(GlobalUserItem.nCurGameKind)) {
                    temp = true;
                    if (PriRoom && true == PriRoom.getInstance().isCurrentGameOpenPri(GlobalUserItem.nCurGameKind)) {
                        if (PriRoom.isRoomListLayer(tag)) {
                            // 普通房列表
                            temp = false;
                        }
                    }
                    if (tag == MatchRoom.LAYTAG.MATCH_ROOMLIST) {
                        if (PriRoom && false == PriRoom.getInstance().isCurrentGameOpenPri(GlobalUserItem.nCurGameKind)
                            && 0 == GlobalUserItem.GetGameRoomCount(GlobalUserItem.nCurGameKind)) {
                            temp = true;
                        } else {
                            temp = false;
                        }
                        // 普通房列表

                    }
                } else if (PriRoom && true == PriRoom.getInstance().isCurrentGameOpenPri(GlobalUserItem.nCurGameKind)) {
                    // 私人房单游戏
                    // 私人房列表
                    temp = true;
                    if (tag == yl.SCENE_ROOMLIST) {
                        // 普通房列表
                        temp = false;
                    }
                } else if (tag == yl.SCENE_ROOMLIST) {
                    // 普通房间
                    temp = true;
                } else {
                    temp = false;
                }
            } else {
                temp = false;
            }
            this._btExit.setVisible(!temp);
            this._btExit.setEnabled(!temp);
            this._btPersonInfo.setEnabled(!temp);
            if (null != this._head) {
                this._head.setVisible(!temp);
            }
            this._bg.loadTexture("src/client/res/plaza/backgroud_plazz.png");
        } else if (tag == yl.SCENE_ROOM) {
            this._btExit.setVisible(true);
            this._btExit.setEnabled(true);
            this._btPersonInfo.setEnabled(true);
            this._btConfig.setVisible(false);
            if (null != this._head) {
                this._head.setVisible(true);
            }
            this._bg.loadTexture("src/client/res/plaza/backgroud_plazz.png");
        } else if (tag == yl.SCENE_GAMELIST) {
            this._btExit.setVisible(false);
            this._btExit.setEnabled(false);
            if (null != this._head) {
                this._head.setVisible(true);
            }
            this._btPersonInfo.setEnabled(true);
            this._bg.loadTexture(res.background_png);
        } else if (bEnableBackBtn) {
            this._btExit.setVisible(true);
            this._btExit.setEnabled(true);
            this._btPersonInfo.setEnabled(false);

            if (null != this._head) {
                this._head.setVisible(false);
            }
        } else {
            this._bg.loadTexture("src/client/res/plaza/backgroud_plazz.png");
        }
        //控制宝箱、喇叭显示
        var infoShow = (tag == yl.SCENE_GAMELIST || bRoomList );//or tag == yl.SCENE_ROOM)

        this._notify.setVisible(infoShow);

        this._btBox.setVisible(GlobalUserItem.bEnableEveryDay && infoShow);
        this.m_btnBox.setVisible(GlobalUserItem.bEnableEveryDay && infoShow);
        if (true == infoShow) {
            if (null != this.m_actBoxAni) {
                this._btBox.stopAllActions();
                this.m_actBoxAni.gotoFrameAndPlay(0, true);
                this._btBox.runAction(this.m_actBoxAni);
            }
        }

        //更新金币
        infoShow = (tag == yl.SCENE_GAMELIST || tag == yl.SCENE_ROOMLIST || tag == yl.SCENE_ROOM || bRoomList);
        if (true == infoShow) {
            this.updateInfomation();
        }

        if (tag == yl.SCENE_GAME) {
            this._btBox.stopAllActions();
        }

        //游戏信息
        GlobalUserItem.bEnterGame = ( tag == yl.SCENE_GAME );
        if (tag == yl.SCENE_ROOMLIST) {
            GlobalUserItem.dwServerRule = 0;
        }
    },

    text_Scheduler : function(args) {

        if (Inttext >= 1) {
            return;
        }
        var item = {};
        item.str = "用户"  +  Inttext  +  "说."  +    "11111111111111111".replace("\n", "");
        item.color = cc.color(255,191,123,255);
        item.autoremove = true;
        item.showcount = 1;
        item.bNotice = false;
        item.id = this.getNoticeId();
        Inttext = Inttext + 1;
        this.addNotice(item);

    },
    //获取页面
    getTagLayer : function(tag, param) {
        var dst;
        if (tag == yl.SCENE_GAMELIST) {
            dst = new GameTopLayer(this);
        } else if (tag == yl.SCENE_BOINLIST) {
            dst = new GameListLayer(_gameList);
        } else if (tag == yl.SCENE_ROOMLIST) {
            //是否有自定义房间列表 TODO : KIL
            var entergame = this.getEnterGameInfo();
            if (null != entergame) {
                var modulestr = entergame._KindName.replace("%.", "/");
                var targetPlatform = cc.sys.platform;
                var customRoomFile = "";
                if (cc.sys.WIN32 == targetPlatform) {
                    customRoomFile = "game/" + modulestr + "src/views/GameRoomListLayer.lua";
                } else {
                    customRoomFile = "game/" + modulestr + "src/views/GameRoomListLayer.luac";
                }
                if (jsb.fileUtils.isFileExist(customRoomFile)) {
                    dst = appdf.req(customRoomFile).create(this, this._gameFrame, param);
                }
            }
            if (null == dst) {
                dst = new RoomListLayer(this, param);
            }
        } else if (tag == yl.SCENE_USERINFO) {
            dst = new UserInfoLayer(this);
        } else if (tag == yl.SCENE_OPTION) {
            dst = new OptionLayer(this);
        } else if (tag == yl.SCENE_VERIFIED) {
            dst = new VerifiedLayer(this);
        } else if (tag == yl.SCENE_QUERY) {
            dst = new QueryLayer(this, param.title, param.callback, param.param, param.single, param.sureType, param.background);
        } else if (tag == yl.SCENE_SHARE) {
            dst = new ShareLayer(this);
        } else if (tag == yl.SCENE_NOTICE) {
            dst = new NoticeLayer(this);
        } else if (tag == yl.SCENE_BANK) {
            dst = new BankLayer(this, this._gameFrame);
        } else if (tag == yl.SCENE_RANKINGLIST) {
            dst = new RankingListLayer(this, param);
        } else if (tag == yl.SCENE_TASK) {
            dst = new TaskLayer(this, this._gameFrame);
        } else if (tag == yl.SCENE_ROOM) {
            dst = new RoomLayer(this._gameFrame, this, param);
        } else if (tag == yl.SCENE_GAME) {
            // TODO : KIL
            entergame = this.getEnterGameInfo();
            if (null != entergame) {
                modulestr = entergame._KindName;
                // var gameScene = appdf.req(appdf.GAME_SRC + modulestr + "src.views.GameLayer");
                // if (gameScene) {
                //     dst = gameScene.create(this._gameFrame, this);
                // }
                dst = new GameLayer(this._gameFrame, this);
                if (PriRoom && null != dst && true == GlobalUserItem.bPrivateRoom) {
                    PriRoom.getInstance().enterGame(dst, this);
                }
            } else {
                cc.log("游戏记录错误");
            }
        } else if (tag == yl.SCENE_CHECKIN) {
            dst = new CheckinLayer(this);
        } else if (tag == yl.SCENE_BANKRECORD) {
            dst = new BankRecordLayer(this);
        } else if (tag == yl.SCENE_ORDERRECORD) {
            dst = new OrderRecordLayer(this);
        } else if (tag == yl.SCENE_EVERYDAY) {
            dst = new EveryDayLayer(this);
        } else if (tag == yl.SCENE_EXTENSION) {
            dst = new ExtensionLayer(this);
        } else if (tag == yl.SCENE_AGENT) {
            dst = new AgentLayer(this);
        } else if (tag == yl.SCENE_SHOP) {
            dst = new ShopLayer(this, param);
        } else if (tag == yl.SCENE_SHOPDETAIL) {
            dst = new ShopDetailLayer(this, this._gameFrame);
        } else if (tag == yl.SCENE_BAG) {
            dst = new BagLayer(this, this._gameFrame);
        } else if (tag == yl.SCENE_INGOT) {
            dst = new IngotLayer(this, param);
        } else if (tag == yl.SCENE_BAGDETAIL) {
            dst = new BagDetailLayer(this, this._gameFrame);
        } else if (tag == yl.SCENE_BAGTRANS) {
            dst = new BagTransLayer(this, this._gameFrame);
        } else if (tag == yl.SCENE_BINDING) {
            dst = new BindingLayer(this);
        } else if (tag == yl.SCENE_FRIEND) {
            dst = new FriendLayer(this);
        } else if (tag == yl.SCENE_MODIFY) {
            dst = new ModifyPasswdLayer(this);
        } else if (tag == yl.SCENE_TABLE) {
            dst = new TableLayer(this);
        } else if (tag == yl.SCENE_FEEDBACK) {
            dst = new FeedbackLayer(this);
        } else if (tag == yl.SCENE_FEEDBACKLIST) {
            dst = new FeedbackLayer.createFeedbackList(this);
        } else if (tag == yl.SCENE_FAQ) {
            dst = new FaqLayer(this);
        } else if (tag == yl.SCENE_BINDINGREG) {
            dst = new BindingRegisterLayer(this);
        } else if (tag == yl.SCENE_CREATROOM) {
            dst = new CreatGameRoomLayer(this);
        } else if (PriRoom) {
            dst = PriRoom.getInstance().getTagLayer(tag, param, this);
        }
        if (dst) {
            dst.setTag(tag);
        }
        return dst;
    },

    //显示等待
    showPopWait : function (isTransparent) {
        if (!this._popWait) {
            this._popWait = new PopWait(isTransparent);
            this._popWait.show(this, "请稍候！");
            this._popWait.setLocalZOrder(yl.MAX_INT);
        }
    },

    //关闭等待
    dismissPopWait : function() {
        if (this._popWait) {
            this._popWait.dismiss();
            this._popWait = null;
        }
    },

    //更新进入游戏记录
    updateEnterGameInfo : function ( info ) {
        GlobalUserItem.m_tabEnterGame = info;
    },

    getEnterGameInfo : function () {
        return GlobalUserItem.m_tabEnterGame;
    },

    //获取游戏信息
    getGameInfo : function(wKindID) {
        for( var i=0; i<_gameList.length; i++) {
            if (Number(_gameList[i]._KindID) == Number(wKindID)) {
                return _gameList[i];
            }
        }
        return null;
    },

    //获取喇叭发送界面
    getTrumpetSendLayer : function () {
        if (null == this.m_trumpetLayer) {
            this.m_trumpetLayer = new TrumpetSendLayer(this);
            this.m_plazaLayer.addChild(this.m_trumpetLayer);
        }
        this.m_trumpetLayer.showLayer(true);
    },

    getSceneRecord : function ( ) {
        return this._sceneRecord;
    },

    updateInfomation : function ( ) {
        var str = (GlobalUserItem.lUserScore + "").replace(".", "/");
        if (str.length > 11) {
            str = str.find(1, 11) + "...";
        }
        this._gold.setString(str);
        this._gold.setVisible(true);
        str = (GlobalUserItem.dUserBeans + "").replace(".", "/");
        if (str.length > 11) {
            str = str.find(1, 11) + "...";
        }
        this._bean.setString(str);
        this._bean.setVisible(true);
        str = (GlobalUserItem.lUserIngot + "").replace(".", "/");
        if (str.length > 11) {
            str = str.find(1, 11) + "...";
        }
        this._ingot.setString(str);
        this._ingot.setVisible(true);

        str = GlobalUserItem.szNickName;
        if (str.length > 10) {
            str = str.find(1, 10) + "...";
        }
        var test = str.toUpperCase();
        this._nikename.setString(str.toUpperCase());

        str = GlobalUserItem.dUserBeans;
        if (str.length > 20) {
            str = str.find(1, 6) + "...";
        }
        this._userBeanNum.setString(str);

        str = GlobalUserItem.dwGameID;
        if (str.length > 20) {
            str = str.find(1, 20) + "...";
        }
        this._userID.setString(str);
    },

    //缓存公共资源
    cachePublicRes : function ( ) {
        cc.spriteFrameCache.addSpriteFrames(res.public_plist);
        if (cc.sys.isNative) {
            var dict = jsb.fileUtils.getValueMapFromFile(res.public_plist);

            var framesDict = dict["frames"];

            if (null != framesDict && typeof framesDict == "object") {
                for(var k in framesDict){
                    // cc.log("framesDict.key:"+k);
                    // ExternalFun.dump(framesDict[k], "framesDict.value");
                    var frame = cc.spriteFrameCache.getSpriteFrame(k);
                    if (null != frame) {
                        frame.retain();
                    }
                }
            }
        }

        cc.spriteFrameCache.addSpriteFrames(res.plaza_plist);
        if (cc.sys.isNative) {
            dict = jsb.fileUtils.getValueMapFromFile(res.plaza_plist);
            framesDict = dict["frames"];

            if (null != framesDict && typeof framesDict == "object") {
                for(var k in framesDict){
                    // cc.log("framesDict.key"+k);
                    // ExternalFun.dump(framesDict[k], "framesDict.value");
                    var frame = cc.spriteFrameCache.getSpriteFrame(k);
                    if (null != frame) {
                        frame.retain();
                    }
                }
            }
        }
    },

    //释放公共资源
    releasePublicRes : function ( ) {
        var dict = jsb.fileUtils.getValueMapFromFile(res.public_plist);
        var framesDict = dict["frames"];
        if (null != framesDict) {
            for(var k in framesDict){
                // cc.log("framesDict.key"+k);
                // ExternalFun.dump(framesDict[k], "framesDict.value");
                var frame = cc.spriteFrameCache.getSpriteFrame(k);
                if (null != frame && frame.getReferenceCount() > 0) {
                    frame.release();
                }
            }
        }

        dict = jsb.fileUtils.getValueMapFromFile(res.plaza_plist);
        framesDict = dict["frames"];
        if (null != framesDict) {
            for(var k in framesDict){
                // cc.log("framesDict.key"+k);
                // ExternalFun.dump(framesDict[k], "framesDict.value");
                var frame = cc.spriteFrameCache.getSpriteFrame(k);
                if (null != frame && frame.getReferenceCount() > 0) {
                    frame.release();
                }
            }
        }
        cc.spriteFrameCache.removeSpriteFramesFromFile(res.public_plist);
        cc.director.getTextureCache().removeTextureForKey("public.png");
        cc.spriteFrameCache.removeSpriteFramesFromFile(res.plaza_plist);
        cc.director.getTextureCache().removeTextureForKey("plaza.png");
        cc.director.getTextureCache().removeUnusedTextures();
        cc.spriteFrameCache.removeUnusedSpriteFrames();
    },

    //输出日志
    logData : function (msg, addExtral) {
        addExtral = addExtral || false;
        var logtable = {};
        var entergame = this.getEnterGameInfo();
        if (null != entergame) {
            logtable.name = entergame._KindName;
            logtable.id = entergame._KindID;
        }
        logtable.msg = msg;
        var jsonStr = JSON.parse(logtable);
        LogAsset.getInstance().logData(jsonStr, true);
    },

    //快速登录逻辑
    quickStartGame : function () {
        //进入游戏房间/进入第一个桌子
        var gamelist = this._sceneLayer.getChildByTag(yl.SCENE_GAMELIST);
        var gameinfo = this.getEnterGameInfo();
        if (null != gamelist && null != gameinfo) {
            //进入游戏第一个房间
            GlobalUserItem.nCurRoomIndex = gameinfo.nEnterRoomIndex;
            if (null == GlobalUserItem.nCurRoomIndex || -1 == GlobalUserItem.nCurRoomIndex) {
                GlobalUserItem.nCurRoomIndex = GlobalUserItem.normalRoomIndex(gameinfo._KindID);
            }
            if (!GlobalUserItem.nCurRoomIndex) {
                showToast(this, "未找到房间信息!", 2);
                return;
            }

            //获取更新
            if (!this.updateGame()) {
                //获取房间信息
                var roomCount = GlobalUserItem.GetRoomCount(gameinfo._KindID);
                if (!roomCount || 0 == roomCount) {
                    //gamelist.onLoadGameList(gameinfo._KindID)
                    cc.log("quickStartGame 房间列表为空");
                }
                GlobalUserItem.nCurGameKind = Number(gameinfo._KindID);
                GlobalUserItem.szCurGameName = gameinfo._KindName;

                var roominfo = GlobalUserItem.GetRoomInfo(GlobalUserItem.nCurRoomIndex);
                if (roominfo.wServerType == yl.GAME_GENRE_PERSONAL) {
                    //showToast(this, "房卡房间不支持快速开始！", 2)
                    return;
                }
                if (this.roomEnterCheck()) {
                    //启动游戏
                    this.onStartGame();
                }
            }
        }
    },

    // 游戏更新处理
    updateGame : function(dwKindID) {
        var gameinfo = this.getEnterGameInfo();
        if (null != dwKindID) {
            gameinfo = this.getGameInfo(dwKindID);
        }
        if (null == gameinfo) {
            return false;
        }
        var gamelist = this._sceneLayer.getChildByTag(yl.SCENE_GAMELIST);
        if (null == gamelist) {
            return false;
        }

        //获取更新
        var version = _version.getResVersion(gameinfo._KindID);
        cc.log("gameinfo._ServerResVersion:"+gameinfo._ServerResVersion);
        cc.log("Current Game version:"+version);
        if ( version==null || gameinfo._ServerResVersion > version) {
            //gamelist.updateGame(gameinfo, gameinfo.gameIndex);    //TODO: must be opened. temporarily closed by lsh
            return true;
        }
        return false;
    },

    // 链接游戏
    loadGameList : function (dwKindID) {
        var gameinfo = this.getEnterGameInfo();
        ExternalFun.dump(gameinfo, "this.getEnterGameInfo",1);
        if (null != dwKindID) {
            gameinfo = this.getGameInfo(dwKindID);
        }
        ExternalFun.dump(gameinfo, "this.getEnterGameInfo",1);
        if (null == gameinfo) {
            return false;
        }
        var gamelist = this._sceneLayer.getChildByTag(yl.SCENE_GAMELIST);
        if (null == gamelist) {
            return false;
        }

        this.updateEnterGameInfo(gameinfo);
        var roomCount = GlobalUserItem.GetRoomCount(gameinfo._KindID);
        if (!roomCount || 0 == roomCount) {
            //gamelist.onLoadGameList(gameinfo._KindID)
            cc.log("loadGameList 房间列表为空");
            return true;
        }
        return false;
    },

    roomEnterCheck : function () {
        var roominfo = GlobalUserItem.GetRoomInfo(GlobalUserItem.nCurRoomIndex);
        var self = this;
        // 密码
        if (bit._and(roominfo.wServerKind, yl.SERVER_GENRE_PASSWD) != 0) {
            this.m_bEnableKeyBack = false;
            this.createPasswordEdit("请输入房间密码", function (pass) {
                self.m_bEnableKeyBack = true;
                GlobalUserItem.szRoomPasswd = pass;
                self.onStartGame();
            }, "src/client/res/RoomList/sp_pwroom_title.png");
            return false;
        }

        // 比赛
        if (bit._and(roominfo.wServerType, yl.GAME_GENRE_MATCH) != 0) {
            showToast(this, "暂不支持比赛房间！", 1);
            return false;
        }
        return true;
    },

    //网络通知
    onNotify : function(msg) {
        var bHandled = false;
        var main = msg.main || 0;
        var sub = msg.sub || 0;
        var name = msg.name || "";
        var param = msg.param;
        var group = msg.group;

        if (group == "client_trumpet" && typeof(msg.param) != undefined) {
            //喇叭消息获取		//
            var item = {};
            item.str = msg.param.szNickName + "说." + msg.param.szMessageContent.replace("\n", "");
            item.color = cc.c4b(255, 191, 123, 255);
            item.autoremove = true;
            item.showcount = 1;
            item.bNotice = false;
            item.id = this.getNoticeId();
            this.addNotice(item);
            bHandled = true;

            //当前场景为游戏
            curScene = this._sceneRecord[this._sceneRecord.length];
            if (curScene == yl.SCENE_GAME) {
                this.m_gameTrumpetList.push(item.str);

                var chat = {};
                chat.szNick = msg.param.szNickName;
                chat.szChatString = msg.param.szMessageContent;
                GameChatLayer.addChatRecordWith(chat);

                if (null == this.m_spGameTrumpetBg) {
                    var frame = cc.spriteFrameCache.getSpriteFrame("sp_trumpet_bg.png");
                    if (null != frame) {
                        var trumpetBg = new cc.Sprite(frame);
                        var runScene = cc.director.getRunningScene();
                        if (null != runScene && null != trumpetBg) {
                            this.m_spGameTrumpetBg = trumpetBg;
                            trumpetBg.setScaleX(0.0001);
                            runScene.addChild(trumpetBg);
                            var trumpetsize = trumpetBg.getContentSize();
                            trumpetBg.setPosition(appdf.WIDTH * 0.5, appdf.HEIGHT - trumpetsize.height * 0.5);
                            var stencil = new cc.Sprite();
                            stencil.setAnchorPoint(cc.p(0, 0.5));
                            stencil.setTextureRect(cc.rect(0, 0, 700, 50));
                            var notifyClip = new cc.ClippingNode(stencil);
                            notifyClip.setAnchorPoint(cc.p(0, 0.5));
                            notifyClip.setInverted(false);
                            notifyClip.setPosition(50, 30);
                            notifyClip.addChild(trumpetBg);
                            var notifyText = new cc.LabelTTF("", "fonts/round_body.ttf", 24);
                            notifyClip.addChild(notifyText);
                            notifyText.setTextColor(cc.color(255, 191, 123, 255));
                            notifyText.setAnchorPoint(cc.p(0, 0.5));
                            notifyText.enableOutline(cc.color(79, 48, 35, 255), 1);
                            notifyText.setPosition(700, 0);
                            this.m_spGameTrumpetBg.trumpetText = notifyText;

                            trumpetBg.runAction(cc.Sequence(cc.ScaleTo(0.5, 1.0), cc.CallFunc(function () {
                                this.onGameTrumpet();
                            })));
                        }
                    }
                }
            } else {
                this.m_gameTrumpetList = {};
            }
        } else if (group == "client_task") { 		// 任务
            bHandled = true;
            //当前场景非任务
            curScene = this._sceneRecord[this._sceneRecord.length];
            if (curScene != yl.SCENE_TASK && true == GlobalUserItem.bEnableTask) {
                NotifyMgr.getInstance().showNotify(this.m_btnTask, msg, cc.p(57, 70));
            }
        } else if (group == "client_friend") {
            bHandled = true;
            var curScene = this._sceneRecord[this._sceneRecord.length];
            if (curScene != yl.SCENE_FRIEND) {
                NotifyMgr.getInstance().showNotify(this.m_btnFriend, msg, cc.p(57, 70));
            }
        }
        return bHandled;
    },

    onGameTrumpet : function () {
        if (null != this.m_spGameTrumpetBg && 0 != this.m_gameTrumpetList.length) {
            var str = this.m_gameTrumpetList[1];
            table.remove(this.m_gameTrumpetList, 1);
            var text = this.m_spGameTrumpetBg.trumpetText;
            if (null != text) {
                text.setString(str);
                text.setPosition(cc.p(700, 0));
                text.stopAllActions();
                var tmpWidth = text.getContentSize().width;
                var self = this;
                text.runAction(new cc.Sequence(new cc.MoveTo(16 + (tmpWidth / 172), cc.p(0 - text.getContentSize().width, 0)), cc.CallFunc.create(function () {
                    if (0 != self.m_gameTrumpetList) {
                        this.onGameTrumpet();
                    } else {
                        self.m_spGameTrumpetBg.runAction(new cc.Sequence(new cc.ScaleTo(0.5, 0.0001, 1.0), new cc.CallFunc(function () {
                            self.m_spGameTrumpetBg.removeFromParent();
                            self.m_spGameTrumpetBg = null
                        })));
                    }
                })));
            }
        }
    },

    //请求公告
    requestNotice : function () {
        var self = this;
        var url = yl.HTTP_URL + "/WS/MobileInterface.ashx?action=GetMobileRollNotice";
        appdf.onHttpJsonTable(url, "GET", "", function (jstable, jsdata) {
            if (jstable != undefined) {
                var data = jstable["data"];
                var msg = jstable["msg"];
                if (data != undefined && data != null) {
                    var valid = data["valid"];
                    if (null != valid && true == valid) {
                        var list = data["notice"];
                        if (list != undefined && list != null) {
                            var listSize = list.length;
                            self.m_nNoticeCount = listSize;
                            for (i = 0; i < listSize; i++) {
                                var item = [];
                                item.str = list[i].content || "";
                                item.id = self.getNoticeId();
                                item.color = cc.color(255, 191, 123, 255);
                                item.autoremove = false;
                                item.showcount = 0;
                                self.m_tabSystemNotice.push(item);
                            }
                            self.onChangeNotify(self.m_tabSystemNotice[self._sysIndex]);
                        }
                    }
                }
                if (null != msg) {
                    showToast(self, msg, 3);
                }
            }
        });
    },

    addNotice : function (item) {
        if (null == item) {
            return;
        }
        this.m_tabInfoTips.push(item);
        if (!this.m_bNotifyRunning) {
            this.onChangeNotify(this.m_tabInfoTips[this._tipIndex]);
        }
    },

    removeNoticeById : function (id) {
        if (null == id) {
            return;
        }

        var idx = null;
        for (i = 0; i < this.m_tabInfoTips.length; i++) {
            if (null != this.m_tabInfoTips[i].id && this.m_tabInfoTips[i].id == id) {
                idx = i;
                break;
            }
        }

        if (null != idx) {
            this.m_tabInfoTips.splice(idx, 1);
        }
    },

    getNoticeId : function () {
        var tmp = this.m_nNotifyId;
        this.m_nNotifyId = this.m_nNotifyId + 1;
        return tmp;
    },

    createPasswordEdit : function (placeholder, confirmFun, titleFile) {
        var runScene = cc.director.getRunningScene();
        var layout = new ccui.Layout();
        layout.setContentSize(cc.size(appdf.WIDTH, appdf.HEIGHT));
        layout.setTouchEnabled(true);
        layout.setSwallowTouches(true);
        runScene.addChild(layout);

        var bg = display.newSprite("plaza/plaza_query_bg.png");
        bg.setPosition(appdf.WIDTH / 2, appdf.HEIGHT / 2);
        layout.addChild(bg);
        var bg_size = bg.getContentSize();

        //编辑框
        var editpass = new ccui.EditBox(cc.size(490, 67), "RoomList/sp_pwedit_bg.png");
        editpass.setPosition(bg_size.width * 0.5, 220);
        editpass.setAnchorPoint(cc.p(0.5, 0.5));
        editpass.setFontName("fonts/round_body.ttf");
        editpass.setPlaceholderFontName("fonts/round_body.ttf");
        editpass.setPlaceholderFontColor(cc.c3b(225, 179, 152));
        editpass.setFontColor(cc.c3b(225, 179, 152));
        editpass.setFontSize(16);
        editpass.setPlaceholderFontSize(24);
        editpass.setMaxLength(32);
        editpass.setInputFlag(cc.EDITBOX_INPUT_FLAG_PASSWORD);
        editpass.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        editpass.setPlaceHolder(placeholder);
        editpass.setFontColor(cc.c4b(225, 179, 152, 255));
        bg.addChild(editpass);

        var self = this;
        var btnEvent = function (sender, eventType) {
            if (eventType == ccui.Widget.TOUCH_ENDED) {
                var tag = sender.getTag();
                if (self.BT_CLOSE_DLG == tag) {
                    layout.removeFromParent();
                    self.m_bEnableKeyBack = true;
                } else if (BT_CONFIRM == tag) {
                    var editText = editpass.getText().replace(" ", "");
                    if (editText.length < 1) {
                        showToast(runScene, "密码不能为空", 2);
                        return;
                    }

                    if (typeof confirmFun == "function") {
                        confirmFun(editText);
                    }
                    layout.removeFromParent();
                }
            }
        };

        titleFile = titleFile || "RoomList/sp_pwtitle_room.png";
        //标题
        var sp = new cc.Sprite(titleFile);
        bg.addChild(sp);
        sp.setPosition(bg_size.width * 0.5, bg_size.height - 30);

        var b1 = new ccui.Button("General/bt_cancel_0.png", "General/bt_cancel_1.png");
        b1.setPosition(bg_size.width * 0.5 - 190, 100);
        b1.setTag(BT_CLOSE_DLG);
        bg.addChild(b1);
        b1.addTouchEventListener(btnEvent);

        var b2 = new ccui.Button("General/bt_confirm_0.png", "General/bt_confirm_1.png");
        b2.setPosition(bg_size.width * 0.5 + 180, 100);
        b2.setTag(BT_CONFIRM);
        bg.addChild(b2);
        b.addTouchEventListener(btnEvent);
    },

    queryUserScoreInfo : function (queryCallBack) {
        var ostime = new Date().getTime();
        var url = yl.HTTP_URL + "/WS/MobileInterface.ashx";

        this.showPopWait();
        var self = this;
        appdf.onHttpJsonTable(url, "GET", "action=GetScoreInfo&userid=" + GlobalUserItem.dwUserID + "&time=" + ostime + "&signature=" + GlobalUserItem.getSignature(ostime), function (sjstable, sjsdata) {
            self.dismissPopWait();
            //dump(sjstable, "sjstable", 5);
            if (sjstable != null) {
                var data = sjstable["data"];
                if (data != undefined && data != null) {
                    var valid = data["valid"];
                    if (true == valid) {
                        var score = Number(data["Score"]) || 0;
                        var bean = Number(data["Currency"]) || 0;
                        var ingot = Number(data["UserMedal"]) || 0;
                        var roomcard = Number(data["RoomCard"]) || 0;

                        var needupdate = false;
                        if (score != GlobalUserItem.lUserScore
                            || bean != GlobalUserItem.dUserBeans
                            || ingot != GlobalUserItem.lUserIngot
                            || roomcard != GlobalUserItem.lRoomCard) {
                            GlobalUserItem.dUserBeans = bean;
                            GlobalUserItem.lUserScore = score;
                            GlobalUserItem.lUserIngot = ingot;
                            GlobalUserItem.lRoomCard = roomcard;
                            needupdate = true;
                        }
                        if (needupdate) {
                            cc.log("update score");
                            //通知更新
                            var eventListener = new cc.EventCustom(yl.RY_USERINFO_NOTIFY);
                            eventListener.obj = yl.RY_MSG_USERWEALTH;
                            cc.eventManager.dispatchEvent(eventListener);
                        }
                        if (typeof(queryCallBack) == "function") {
                            queryCallBack(needupdate);
                        }
                    }
                }
            }
        });
    },

    queryTaskInfo : function () {
        var self = this;
        var taskResult = function (result, msg) {
            if (result == 1) { // 获取到 SUB_GP_TASK_INFO
                if (self.m_bFirstQueryCheckIn && null != self._checkInFrame) {
                    self.m_bFirstQueryCheckIn = false;
                    if (true == GlobalUserItem.bEnableCheckIn) {
                        //签到页面
                        self._checkInFrame.onCheckinQuery();
                    } else {
                        if (null != self.m_touchFilter) {
                            self.m_touchFilter.dismiss();
                            self.m_touchFilter = null;
                        }
                        // 显示广告
                        if (GlobalUserItem.isShowAdNotice()) {
                            var webview = new WebViewLayer(this);
                            var runScene = cc.director.getRunningScene();
                            if (null != runScene) {
                                runScene.addChild(webview, yl.ZORDER.Z_AD_WEBVIEW);
                            }
                        }
                    }
                }
                self.queryLevelInfo();
                if (null != self._taskFrame._gameFrame) {
                    self._taskFrame._gameFrame._shotFrame = null;
                    self._taskFrame._gameFrame = null;
                }
                self._taskFrame = null;
            }
        };
        this._taskFrame = new TaskFrame(this, taskResult);
        this._taskFrame._gameFrame = this._gameFrame;
        if (null != this._gameFrame) {
            this._gameFrame._shotFrame = this._taskFrame;
        }
        //this._taskFrame.onTaskLoad();  // closed by lsh. This is en error
    },

    queryLevelInfo : function () {
        var self = this;
        var levelCallBack = function (result, msg) {
            self.onLevelCallBack(result, msg);
        };
        this._levelFrame = new LevelFrame(this, levelCallBack);
        this._levelFrame.onLoadLevel();
    },

    disconnectFrame : function () {
        if ((null != this._shopDetailFrame) && (this._shopDetailFrame.isSocketServer())) {
            this._shopDetailFrame.onCloseSocket();
            this._shopDetailFrame = null;
        }

        if ((null != this._levelFrame) && (this._levelFrame.isSocketServer())) {
            this._levelFrame.onCloseSocket();
            this._levelFrame = null;
        }

        if ((null != this._checkInFrame) && (this._checkInFrame.isSocketServer())) {
            this._checkInFrame.onCloseSocket();
            this._checkInFrame = null;
        }

        if ((null != this._taskFrame) && (this._taskFrame.isSocketServer())) {
            this._taskFrame.onCloseSocket();

            if (null != this._taskFrame._gameFrame) {
                this._taskFrame._gameFrame._shotFrame = null;
                this._taskFrame._gameFrame = null;
            }
            this._taskFrame = null;
        }
    },

    coinDropDownAni : function ( funC ) {
        var self = this;
        var runScene = cc.director.getRunningScene();
        if (null == runScene) {
            return;
        }
        ExternalFun.popupTouchFilter(1, false);
        if (null == this.m_nodeCoinAni) {
            this.m_nodeCoinAni = ExternalFun.loadCSB("plaza/CoinAni.csb", runScene);
            this.m_nodeCoinAni.setPosition(30, yl.HEIGHT + 30);

            this.m_actCoinAni = ExternalFun.loadTimeLine("plaza/CoinAni.csb");
            ExternalFun.SAFE_RETAIN(this.m_actCoinAni);
        }
        var onFrameEvent = function (frame) {
            if (null == frame) {
                return;
            }
            var str = frame.getEvent();
            cc.log("frame event ==> " + str);
            if (str == "drop_over") {
                self.m_nodeCoinAni.setVisible(false);
                self.m_nodeCoinAni.stopAllActions();
                if (typeof(funC) == "function") {
                    funC();
                }
                ExternalFun.dismissTouchFilter();
            }
        };
        this.m_actCoinAni.setFrameEventCallFunc(onFrameEvent);

        var child = runScene.getChildren() || {};
        var childCount = child.length;
        this.m_nodeCoinAni.setVisible(true);
        this.m_nodeCoinAni.setvarZOrder(childCount + 1);
        this.m_nodeCoinAni.stopAllActions();
        this.m_actCoinAni.gotoFrameAndPlay(0, false);
        this.m_nodeCoinAni.runAction(this.m_actCoinAni);
    },

    popTargetShare : function ( callback ) {
        var lay = new TargetShareLayer(callback);
        this.addChild(lay);
        //TODO by Han
       // lay.setLocalZOrder(yl.ZORDER.Z_TARGET_SHARE);
        lay.setLocalZOrder(1);
    },

    createHelpBtn : function (pos, zorder, url, parent) {
        parent = parent || this;
        zorder = zorder || yl.ZORDER.Z_HELP_BUTTON;
        url = url || yl.HTTP_URL;
        var self = this;
        var btncallback = function (ref, tType) {
            if (tType == ccui.TouchEventType.ended) {
                self.popHelpLayer(url, zorder);
            }
        };
        pos = pos || cc.p(100, 100);
        var btn = new ccui.Button("pub_btn_introduce_0.png", "pub_btn_introduce_1.png", "pub_btn_introduce_0.png", UI_TEX_TYPE_PLIST);
        btn.setPosition(pos);
        btn.setName(HELP_BTN_NAME);
        btn.addChild(parent);
        btn.setvarZOrder(zorder);
        btn.addTouchEventListener(btncallback);
    },

    popHelpLayer : function ( url, zorder) {
        zorder = zorder || yl.ZORDER.Z_HELP_WEBVIEW;
        //var IntroduceLayer = appdf.req(appdf.CLIENT_SRC + "plaza.views.layer.plaza.IntroduceLayer");
        var lay = new IntroduceLayer(this, url);
        lay.setName(HELP_LAYER_NAME);
        var runScene = cc.director.getRunningScene();
        if (null != runScene) {
            runScene.addChild(lay);
            lay.setvarZOrder(zorder);
        }
    },

    createHelpBtn2 : function(pos, zorder, nKindId, nType, parent) {
        parent = parent || this;
        zorder = zorder || yl.ZORDER.Z_HELP_BUTTON;
        url = url || yl.HTTP_URL;

        var self = this;
        var btncallback = function (ref, tType) {
            if (tType == ccui.TouchEventType.ended) {
                self.popHelpLayer2(nKindId, nType, zorder);
            }
        };
        pos = pos || cc.p(100, 100);
        var btn = new ccui.Button("pub_btn_introduce_0.png", "pub_btn_introduce_1.png", "pub_btn_introduce_0.png", UI_TEX_TYPE_PLIST);
        btn.setPosition(pos);
        btn.setName(HELP_BTN_NAME);
        btn.addChild(parent);
        btn.setvarZOrder(zorder);
        btn.addTouchEventListener(btncallback);
    },

    popHelpLayer2 : function ( nKindId, nType, nZorder) {
        nZorder = nZorder || yl.ZORDER.Z_HELP_WEBVIEW;
        //var IntroduceLayer = appdf.req(appdf.CLIENT_SRC + "plaza.views.layer.plaza.IntroduceLayer");
        var lay = new IntroduceLayer().createLayer(this, nKindId, nType);
        if (null != lay) {
            lay.setName(HELP_LAYER_NAME);
            var runScene = cc.director.getRunningScene();
            if (null != runScene) {
                runScene.addChild(lay);
                lay.setvarZOrder(nZorder);
            }
        }
    },

    createVoiceBtn :function (pos, zorder, parent) {
        var self = this;
        parent = parent || this;
        zorder = zorder || yl.ZORDER.Z_VOICE_BUTTON;
        var btncallback = function (ref, tType) {
            if (tType == ccui.TouchEventType.began) {
                self.startVoiceRecord();
            } else if (tType == ccui.TouchEventType.ended
                || tType == ccui.TouchEventType.canceled) {
                self.stopVoiceRecord();
            }
        };
        pos = pos || cc.p(150, 150);
        var btn = new ccui.Button("btn_voice_chat_0.png", "btn_voice_chat_1.png", "btn_voice_chat_0.png", UI_TEX_TYPE_PLIST);
        btn.setPosition(pos);
        btn.setName(VOICE_BTN_NAME);
        btn.addChild(parent);
        btn.setvarZOrder(zorder);
        btn.addTouchEventListener(btncallback);
    },

    startVoiceRecord : function () {
        //防作弊不聊天
        if (GlobalUserItem.isAntiCheat()) {
            var runScene = cc.director.getRunningScene();
            showToast(runScene, "防作弊房间禁止聊天", 3);
            return;
        }

        var lay = VoiceRecorderKit.createRecorderLayer(this, this._gameFrame);
        if (null != lay) {
            lay.setName(VOICE_LAYER_NAME);
            this.addChild(lay);
        }
    },

    stopVoiceRecord : function () {
        var voiceLayer = this.getChildByName(VOICE_LAYER_NAME);
        if (null != voiceLayer) {
            voiceLayer.removeRecorde();
        }
    },

    cancelVoiceRecord : function () {
        var voiceLayer = this.getChildByName(VOICE_LAYER_NAME);
        if (null != voiceLayer) {
            voiceLayer.cancelVoiceRecord();
        }
    },

    // 好友邀请
    inviteFriend : function (inviteFriend, gameKind, serverId, tableId, inviteMsg) {
        var tab = {};
        tab.dwInvitedUserID = inviteFriend;
        tab.wKindID = gameKind;
        tab.wServerID = serverId;
        tab.wTableID = tableId;
        tab.szInviteMsg = inviteMsg;
        FriendMgr.getInstance().sendInviteGame(tab);
    },

    // 好友截图分享
    imageShareToFriend : function ( toFriendId, imagepath, shareMsg ) {
        var param = imagepath;
        var runScene = cc.director.getRunningScene();
        if (jsb.fileUtils.isFileExist(param)) {
            this.showPopWait();
            //发送上传头像
            var url = yl.HTTP_URL + "/WS/Account.ashx?action=uploadshareimage";
            var uploader = CurlAsset.createUploader(url, param);
            if (null == uploader) {
                showToast(this, "分享图上传异常", 2);
                return;
            }
            var nres = uploader.addChildFileForm("file", param, "image/png");
            //用户标示
            nres = uploader.addChildForm("userID", GlobalUserItem.dwUserID);
            //分享用户
            nres = uploader.addChildForm("suserID", toFriendId);
            //登陆时间差
            var delta = tonumber(currentTime()) - Number(GlobalUserItem.LogonTime);
            cc.log("time delta " + delta);
            nres = uploader.addChildForm("time", delta + "");
            //客户端ip
            var ip = MultiPlatform.getInstance().getClientIpAdress() || "192.168.1.1";
            nres = uploader.addChildForm("clientIP", ip);
            //机器码
            var machine = GlobalUserItem.szMachine || "A501164B366ECFC9E249163873094D50";
            nres = uploader.addChildForm("machineID", machine);
            //会话签名
            nres = uploader.addChildForm("signature", GlobalUserItem.getSignature(delta));
            if (0 != nres) {
                showToast(this, "上传表单提交异常,error code ==> " + nres, 2);
                return;
            }

            uploader.uploadFile(function (sender, ncode, msg) {
                var logtable = {};
                logtable.msg = msg;
                logtable.ncode = ncode;
                var jsonStr = JSON.parse(logtable);
                LogAsset.getInstance().logData(jsonStr, true);

                // var ok, msgTab = pcall(function()
                // 	return cjson.decode(msg)
                // end)
                if (ok) {
                    var dataTab = msgTab["data"];
                    if (typeof(dataTab) == "table") {
                        var address = "";
                        if (true == dataTab["valid"]) {
                            address = dataTab["ShareUrl"] || "";
                        }
                        var tab = {};
                        tab.dwSharedUserID = toFriendId;
                        tab.szShareImageAddr = address;
                        tab.szMessageContent = shareMsg;
                        tab.szImagePath = imagepath;
                        if (FriendMgr.getInstance().sendShareMessage(tab)) {
                            showToast(runScene, "分享成功!", 2);
                        }
                    }
                } else {

                }
                this.dismissPopWait();
            });
        } else {
            showToast(runScene, "您要分享的图片不存在, 请重试", 2);
        }
    },
    getNoticeListToNoticeLayer : function () {
        return this.m_tabSystemNotice;
    }

});
