var targetPlatform = cc.sys.platform;
var privatemgr = "";
if (cc.sys.WIN32 == targetPlatform)
	privatemgr = "client/src/privatemode/plaza/src/models/PriRoom.lua";
else
	privatemgr = "client/src/privatemode/plaza/src/models/PriRoom.luac";

// if (jsb.fileUtils.isFileExist(privatemgr)) {
//     if (!PriRoom) {
//         new PriRoom();
//     }
// }

var FIRST_LOGIN = true;

var LogonScene = cc.Scene.extend({

    BT_EXIT 			: 1,
    DG_QUERYEXIT 	    : 2,

	ctor: function () {
		this._super();
		this.onceExcute();
		this.onCreate();
    },

    // TODO : 全局处理lua错误
    // cc.exports.g_LuaErrorHandle : function () {
    //     cc.exports.bHandlePopErrorMsg = true;
    //     if (isDebug()) {
    //         cc.log("debug return");
    //         return true;
    //     } else {
    //         cc.log("release return");
    //         return false;
    //     }
    // },

    // //加载配置
    onceExcute : function() {
        //文件日志
        // LogAsset.init(MultiPlatform.getExtralDocPath(), true, true);
        //配置微信
        MultiPlatform.getInstance().thirdPartyConfig(yl.ThirdParty.WECHAT, yl.WeChat);
        //配置支付宝
        MultiPlatform.getInstance().thirdPartyConfig(yl.ThirdParty.ALIPAY, yl.AliPay);
        //配置竣付通
        MultiPlatform.getInstance().thirdPartyConfig(yl.ThirdParty.JFT, yl.JFT);
        //配置分享
        MultiPlatform.getInstance().configSocial(yl.SocialShare);
        //配置高德
        MultiPlatform.getInstance().thirdPartyConfig(yl.ThirdParty.AMAP, yl.AMAP);
    },


    onEnterTransitionDidFinish : function () {
        this.onEnterTransitionFinish();
    },
    // 进入场景而且过渡动画结束时候触发。
    onEnterTransitionFinish : function() {
        if (null != this._logonView) {
            this._logonView.onReLoadUser();
        }
        // 默认登陆游戏信息
        if (_gameList.length > 0) {
            var info = _gameList[0];
            GlobalUserItem.nCurGameKind = Number(info._KindID);
        }

        if (FIRST_LOGIN) {
            FIRST_LOGIN = false;

            if (GlobalUserItem.bAutoLogon) {
                GlobalUserItem.bVisitor = false;
                if (yl.AUTO_LOGIN) {
                    this.onLogon(GlobalUserItem.szAccount, GlobalUserItem.szPassword, true, true);
                }
            }
            GlobalUserItem.m_tabOriginGameList = _gameList;
        }
        return this;
    },

    onExitTransitionDidStart : function () {
        this.onExitTransitionStart();
    },
    // 退出场景而且开始过渡动画时候触发。
    onExitTransitionStart : function() {
        // this._backLayer.unregisterScriptKeypadHandler();
        // this._backLayer.setKeyboardEnabled(false);
        // return this;
    },

    onExit : function () {
        if(this._logonFrame.isSocketServer()) {
            this._logonFrame.onCloseSocket();
        }
    },

    // 初始化界面
	onCreate : function() {
        appdf.print("LogonScene.onCreate");

        var self = this;

        if (appdf.IS_UPDATELOGON == 1) {
            yl.LOGONSERVER = appdf.getLogonServer();
            yl.URL_REQUEST = appdf.getLogonRequest();
            yl.HTTP_URL = appdf.getLogonRequest();
            yl.SERVER_LIST = appdf.getServerList();
            yl.TOTAL_COUNT = yl.SERVER_LIST.length;
        }
        var  btcallback = function(ref, type) {
            if (type == ccui.Widget.TOUCH_ENDED) {
                this.onButtonClickedEvent(ref.getTag(), ref);
            }
        };

        var logonCallBack = function (result,message) {
            self.onLogonCallBack(result, message);
        };

        this._logonFrame = new LogonFrame(this,logonCallBack);

        this._backLayer = new cc.Layer();
        this.addChild(this._backLayer);

        // TODO :
        // //返回键事件
        // this._backLayer.registerScriptKeypadHandler(function(event)
        // 	if event == "backClicked") {
        // 		if this._popWait == null) {
        // 			this.onButtonClickedEvent(LogonScene.BT_EXIT)
        // 		end
        // 	end
        // end)
        // this._backLayer.setKeyboardEnabled(true)

        this._topLayer = new cc.Layer();
        this.addChild(this._topLayer);

        //背景
        var s = new cc.Sprite(res.background_png);
        s.setPosition(yl.WIDTH / 2, yl.HEIGHT / 2);
        this._backLayer.addChild(s);

        //平台logo
        s = new cc.Sprite(res.logon_logo_png);
        s.setPosition(yl.WIDTH / 2, yl.HEIGHT / 2 + 150);
        this._backLayer.addChild(s);
        s.runAction(new cc.MoveTo(0.3, cc.p(yl.WIDTH / 2, yl.HEIGHT / 2 + 45)));

        this._txtTips = new cc.LabelTTF("同步服务器信息中...", "fonts/round_body.ttf", 24)
        this._txtTips.setColor(cc.color(0,250,0,255));
        this._txtTips.setAnchorPoint(cc.p(1,0));
        this._txtTips.setVisible(false);
        this._txtTips.enableStroke(cc.color(0,0,0,255), 1);
        this._txtTips.setPosition(yl.WIDTH,0);
        this._topLayer.addChild(this._txtTips);

        //读取配置
        GlobalUserItem.LoadData();

        //背景音乐
        ExternalFun.playPlazzBackgroudAudio();

        // 激活房卡
        GlobalUserItem.bEnableRoomCard = (_serverConfig["isOpenCard"] == 0);
        //创建登录界面
        this._logonView = new LogonView(_serverConfig);
        this._logonView.setPosition(0,0);
		this._backLayer.addChild(this._logonView);
    },

    //按钮事件
    onButtonClickedEvent : function(tag,ref) {
        //退出按钮
        var self = this;
        if (tag == this.BT_EXIT) {
            /* why below two lines needed? (jong)
            var a = Integer64.new();
            cc.log(a.getstring());*/

            if (this.getChildByTag(LogonScene.DG_QUERYEXIT)) {
                return;
            }
            var q = new QueryDialog("确认退出APP吗？", function (ok) {
                if (ok == true) {
                    os.exit(0);
                }
            });
            q.setTag(LogonScene.DG_QUERYEXIT);
            this.addChild(q);
        }
    },

    //显示登录 bCheckAutoLogon-是否自动登录
    onShowLogon : function(bCheckAutoLogon) {
        var self = this;

        // 取消注册界面
        if (this._registerView != null) {
            this._registerView.runAction(new cc.MoveTo(0.3, cc.p(yl.WIDTH, 0)));
        }

        // 取消用户条款界面
        if (this._serviceView != null) {
            this._serviceView.runAction(new cc.MoveTo(0.3, cc.p(yl.WIDTH, 0)));
        }

        if (null == this._logonView) {
            this._logonView = new LogonView();
            this._logonView.setPosition(yl.WIDTH, 0);
            this._backLayer.addChild(this._logonView);
        }

        //自动登录判断
        bCheckAutoLogon = false;
        if (! bCheckAutoLogon) {
            this._logonView.runAction(
                new cc.Sequence(
                    new cc.MoveTo(0.3, cc.p(0, 0)),
                    new cc.CallFunc(function () {
                            self._logonView.onReLoadUser();
                        }
                    )));
        } else {
            this._logonView.runAction(
                new cc.Sequence(
                    new cc.MoveTo(0.3, cc.p(0, 0)),
                    new cc.CallFunc(function () {
                        self._logonView.onReLoadUser();
                        var szAccount = GlobalUserItem.szAccount;
                        var szPassword = GlobalUserItem.szPassword;
                        this.onLogon(szAccount, szPassword, true, true);
                    })
                ));

        }
    },

    //显示注册界面
    onShowRegister : function() {
        //取消登录界面
        if (this._logonView != null) {
            this._logonView.runAction(new cc.MoveTo(0.3, cc.p(yl.WIDTH, 0)));
        }

        //取消用户条款界面
        if (this._serviceView != null) {
            this._serviceView.runAction(new cc.MoveTo(0.3, cc.p(yl.WIDTH, 0)));
        }

        //创建注册界面
        if (this._registerView == null) {
            this._registerView = new RegisterView();
            this._registerView.setPosition(-yl.WIDTH, 0);
            // this._registerView.setPosition(yl.WIDTH / 2, yl.HEIGHT / 2);
            this._backLayer.addChild(this._registerView);
        } else {
            this._registerView.stopAllActions();
        }
        this._registerView.runAction(new cc.MoveTo(0.3, cc.p(0, 0)));

        cc.log("onshowRegister");
    },

    //显示用户条款界面
    onShowService : function() {
        //取消注册界面
        if (this._registerView != null) {
            this._registerView.runAction(new cc.MoveTo(0.3, cc.p(yl.WIDTH, 0)));
        }

        //取消登录界面
        if (this._logonView != null) {
            this._logonView.runAction(new cc.MoveTo(0.3, cc.p(yl.WIDTH, 0)));
        }

        if (this._serviceView == null) {
            this._serviceView = new ServiceLayer();
            this._serviceView.setPosition(yl.WIDTH, 0);
            this._backLayer.addChild(this._serviceView);
        } else {
            this._serviceView.stopAllActions();
        }
        this._serviceView.runAction(new cc.MoveTo(0.3, cc.p(0, 0)));
    },

    // 登录大厅
    onLogon : function(szAccount,szPassword,bSave,bAuto) {
	    cc.log("onLogon function called");
        //保存登陆地址
        if (yl.SAVE_LOGONSERVER == 1) {
            appdf.saveLogonServer(yl.LOGONSERVER);
            appdf.saveLogonRequest(appdf.URL_REQUEST);
            appdf.saveServerList(yl.SERVER_LIST);

            return;
        }

        //读取登陆地址
        if (yl.SAVE_LOGONSERVER == 2) {
            var sp = "";
            this.logon_1 = appdf.getLogonServer();
            this.logon_Request = appdf.getLogonRequest();
            this.logon_s = appdf.getServerList();
            var logon_count = this.logon_s.length();
            return;
        }

        //输入检测
        if (szAccount == null || szPassword == null || bSave == null || bAuto == null) {
            return;
        }
        var len = szAccount.length;
        if (len < 6 || len > 31) {
            showToast(this, "游戏帐号必须为6~31个字符，请重新输入!", 2, cc.color(250, 0, 0, 255));
            return;
        }

        len = szPassword.length;
        if (len < 6) {
            showToast(this, "密码必须大于6个字符，请重新输入！", 2, cc.color(250, 0, 0, 255));
            return;
        }

        //参数记录
        this._szAccount = szAccount;
        this._szPassword = szPassword;
        this._bAuto = bAuto;
        this._bSave = bSave;

        //调用登录
        this.showPopWait();
        this._Operate = 0;
        this._logonFrame.onLogonByAccount(szAccount, szPassword);
    },

    //游客登录
    onVisitor : function() {
        //调用登录
        this.showPopWait();
        this._Operate = 2;
        this._logonFrame.onLogonByVisitor();
    },

    //微信登陆
    thirdPartyLogin : function(plat,bSave,bAuto) {
        var sp = "";
        this._tThirdData = {};

        var self = this;

        //直接登陆判断
        var account = appdf.getSaveMD5String(sp, "user_gameconfig.plist", "code_3", 32);
        var nick = appdf.getSaveMD5String(sp, "user_gameconfig.plist", "code_4", 32);
        var gender = appdf.getSaveMD5String(sp, "user_gameconfig.plist", "code_5", 32);
        var platform = yl.PLATFORM_LIST[plat];
        if (account != "" && nick != "" && gender != "") {
            gender = Number(gender);
            this.showPopWait();
            this._Operate = 3;
            this._tThirdData =
                {
                    szAccount: account,
                    szNick: nick,
                    cbGender: gender,
                    platform: yl.PLATFORM_LIST[plat],
                };
            this._bAuto = bAuto;
            this._bSave = bSave;
            this._logonFrame.onLoginByThirdParty(this._tThirdData.szAccount, this._tThirdData.szNick, this._tThirdData.cbGender, this._tThirdData.platform);
        } else {
            this.showPopWait();
            this.runAction(new cc.Sequence(new cc.DelayTime(5), new cc.CallFunc(function () {
                this.dismissPopWait();
            })));

            var loginCallBack = function (param) {
                this.dismissPopWait();
                if (typeof (param) == "string" && string.len(param) > 0) {
                    // var ok, datatable = pcall(function()
                    //         return cjson.decode(param)
                    // end)
                    var datatable = JSON.parse(param);
                    if (ok && typeof datatable == "table") {
                        var account = datatable["unionid"] || "";
                        var nick = datatable["screen_name"] || "";
                        this._szHeadUrl = datatable["profile_image_url"] || "";
                        var gender = datatable["gender"] || "0";
                        gender = Number(gender);
                        self.showPopWait();
                        this._Operate = 3;
                        this._tThirdData =
                            {
                                szAccount: account,
                                szNick: nick,
                                cbGender: gender,
                                platform: yl.PLATFORM_LIST[plat],
                            };

                        self._bAuto = bAuto;
                        self._bSave = bSave;

                        self._logonFrame.onLoginByThirdParty(this._tThirdData.szAccount, this._tThirdData.szNick, this._tThirdData.cbGender, this._tThirdData.platform);
                    }
                }
            }

            MultiPlatform.thirdPartyLogin(plat, loginCallBack);
        }
    },

    //注册账号
    onRegister : function(szAccount,szPassword,szRePassword,bAgreement,szSpreader) {
        //输入检测
        if (szAccount == null || szPassword == null || szRePassword == null) {
            return;
        }

        if (bAgreement == false) {
            showToast(this, "请先阅读并同意《游戏中心服务条款》！", 2, cc.color(250, 0, 0, 255));
            return;
        }
        var len = szAccount.length;//#szAccount
        if (len < 6 || len > 31) {
            showToast(this, "游戏帐号必须为6~31个字符，请重新输入！", 2, cc.color(250, 0, 0, 255));
            return;
        }

        // //判断emoji TODO : AppDelegate.ccp file
        // if (ExternalFun.isContainEmoji(szAccount)) {
        //     showToast(this, "帐号包含非法字符,请重试", 2);
        //     return;
        // }
        //
        // //判断是否有非法字符
        // if (true == ExternalFun.isContainBadWords(szAccount)) {
        //     showToast(this, "帐号中包含敏感字符,不能注册", 2);
        //     return;
        // }

        len = szPassword.length;
        if (len < 6 || len > 26) {
            showToast(this, "密码必须为6~26个字符，请重新输入！", 2, cc.color(250, 0, 0, 255));
            return;
        }

        if (szPassword != szRePassword) {
            showToast(this, "二次输入密码不一致，请重新输入！", 2, cc.color(250, 0, 0, 255));
            return;
        }

        // 与帐号不同
        if (szPassword.toLowerCase() == szAccount.toLowerCase()) {
            showToast(this, "密码不能与帐号相同，请重新输入！", 2, cc.color(250, 0, 0, 255));
            return;
        }

        //参数记录
        this._szAccount = szAccount;
        this._szPassword = szPassword;
        this._bAuto = true;
        this._bSave = true;
        this._gender = Math.random(1);
        //调用注册
        this.showPopWait();
        this._Operate = 1;
        this._logonFrame.onRegister(szAccount, szPassword, this._gender, szSpreader);
    },
    //登录注册回调
    onLogonCallBack: function(result, message) {
        cc.log("result : " + result);
        cc.log("message : " + message);
        if (result == 1) { //成功
            //本地保存
            if (this._Operate == 2) { 					//游客登录
                GlobalUserItem.bAutoLogon = false;
                GlobalUserItem.bSavePassword = false;
                GlobalUserItem.szPassword = "WHYK@foxuc.cn";
            }
            else if (this._Operate == 3) { 				//微信登陆
                GlobalUserItem.szThirdPartyUrl = this._szHeadUrl;
                GlobalUserItem.szPassword = "WHYK@foxuc.cn";
                GlobalUserItem.bThirdPartyLogin = true;
                GlobalUserItem.thirdPartyData = this._tThirdData;
                GlobalUserItem.bAutoLogon = this._bAuto;
                GlobalUserItem.bSavePassword = this._bSave;
                GlobalUserItem.onSaveAccountConfig(this._Operate);
            }
            else {
                GlobalUserItem.bAutoLogon = this._bAuto;
                GlobalUserItem.bSavePassword = this._bSave;
                GlobalUserItem.onSaveAccountConfig();
            }

            if (yl.HTTP_SUPPORT) {
                var ostime = Math.floor(new Date().getTime() / 1000);
                var self = this;
                appdf.onHttpJsonTable(yl.HTTP_URL + "/WS/MobileInterface.ashx", "GET", "action=GetMobileShareConfig&userid=" + GlobalUserItem.dwUserID + "&time=" + ostime + "&signature=" + GlobalUserItem.getSignature(ostime), function (jstable, jsdata) {
                    var msg = null;
                    if (jstable) {
                        var data = jstable.data;
                        msg = jstable.msg;
                        if (data) {
                            var valid = data.valid;
                            if (valid) {
                                var count = data.FreeCount || 0;
                                GlobalUserItem.nTableFreeCount = Number(count);
                                var sharesend = data.SharePresent || 0;
                                GlobalUserItem.nShareSend = Number(sharesend);

                                //推广链接
                                GlobalUserItem.szSpreaderURL = data.SpreaderUrl;
                                if (null == GlobalUserItem.szSpreaderURL || "" == GlobalUserItem.szSpreaderURL) {
                                    GlobalUserItem.szSpreaderURL = yl.HTTP_URL + "/Mobile/Register.aspx";
                                }
                                else {
                                    GlobalUserItem.szSpreaderURL = GlobalUserItem.szSpreaderURL.replace(" ", "");
                                }
                                // 微信平台推广链接
                                GlobalUserItem.szWXSpreaderURL = data.WxSpreaderUrl;
                                if (null == GlobalUserItem.szWXSpreaderURL || "" == GlobalUserItem.szWXSpreaderURL) {
                                    GlobalUserItem.szWXSpreaderURL = yl.HTTP_URL + "/Mobile/Register.aspx";
                                }
                                else {
                                    GlobalUserItem.szWXSpreaderURL = GlobalUserItem.szWXSpreaderURL.replace(" ", "");
                                }

                                //整理代理游戏列表
                                if (self._logonFrame.m_angentServerList.length > 0) {
                                    self.arrangeGameList(self._logonFrame.m_angentServerList);
                                }
                                else {
                                    _gameList = GlobalUserItem.m_tabOriginGameList;
                                }

                                // 每日必做列表
                                GlobalUserItem.tabDayTaskCache = [];
                                var dayTask = data.DayTask;
                                cc.log("dataTask = " + typeof dayTask);
                                if (dayTask) {
                                    for (var k in  dayTask) {
                                        if (Number(dayTask[k]) == 0) {
                                            GlobalUserItem.tabDayTaskCache[k] = 1;
                                            GlobalUserItem.bEnableEveryDay = true;
                                        }
                                    }
                                }
                                GlobalUserItem.bEnableCheckIn = (GlobalUserItem.tabDayTaskCache["Field1"] != null);
                                GlobalUserItem.bEnableTask = (GlobalUserItem.tabDayTaskCache["Field6"] != null);

                                // 邀请送金
                                var sendcount = data["RegGold"];
                                GlobalUserItem.nInviteSend = Number(sendcount) || 0;

                                // 检查资源
                                if (!self.updateGameResource()) {
                                    self.enterClient();
                                }
                                return;
                            }
                        }
                    }
                    self.dismissPopWait();
                    var str = "游戏登陆异常"
                    if (typeof msg == "string") {
                        str = str + "." + msg
                    }
                    showToast(self, str, 3, cc.color(250, 0, 0));
                });
            } else {
                //整理代理游戏列表
                if (this._logonFrame.m_angentServerList.length > 0) {
                    this.arrangeGameList(this._logonFrame.m_angentServerList);
                }
                else {
                    _gameList = GlobalUserItem.m_tabOriginGameList;
                }

                // 检查资源
                if (!this.updateGameResource()) {
                    this.enterClient();
                }
            }
        } else if (result == -1) { //失败
            this.dismissPopWait();
            if (typeof message == "string" && message != "") {
                showToast(this._topLayer, message, 2, cc.color(250, 0, 0, 255));
            }
        }
        else if (result == 10) { //重复绑定
            if (this._logonView != null && null != this._logonView.refreshBtnList) {
                this._logonView.refreshBtnList();
            }
        }
    },

    //显示等待
    showPopWait : function() {
        if (!this._popWait) {
            this._popWait = new PopWait();
            this._popWait.show(this._topLayer, "请稍候！");
        }
    },

    //关闭等待
    dismissPopWait: function() {
        if (this._popWait != null) {
            this._popWait.dismiss();
            this._popWait = null;
        }
    },
    //整理游戏列表
    arrangeGameList : function(serverList) {
        var originList = GlobalUserItem.m_tabOriginGameList;
        var newList = [];
        originList.forEach(function (v, k) {
            var serverGame = serverList[Number(v._KindID)];
            if (null != serverGame) {
                v._SortId = serverGame.SortID
                newList.push(v);
            }
        });
        newList = newList.sort(function(a, b) {
            return a._SortId > b._SortId;
        });
        _gameList = newList;
    },

    enterClient : function() {
        appdf.print("LogonScene.enterClient");
        //进入游戏列表
        var scene = new ClientScene();
        cc.director.runScene(new cc.TransitionFade(1, scene));
        FriendMgr.getInstance().reSetAndLogin();
    },

    updateGameResource : function() {
        // 单游戏、且未同步最新资源
        if (1 == _gameList.length) {
            var updategame = _gameList[0];
            var kindid = Number(updategame._KindID);
            var version = Number(_version.getResVersion(updategame._KindID));
            if (!version || updategame._ServerResVersion > version) {
                cc.log("update single game");
                var targetPlatform = cc.sys.platform;
                if (cc.sys.WIN32 == targetPlatform) {
                    cc.log("LogonScene win32 跳过更新");
                    _version.setResVersion(updategame._ServerResVersion, updategame._KindID);
                    return false;
                }

                //进入更新场景
                _updategame = updategame;
                var scene = new UpdateScene();
                cc.director.runScene(new cc.TransitionFade(1, scene));
                return true;
            }
        }
        return false;
    }

});
