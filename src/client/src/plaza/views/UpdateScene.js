/*  Author  :   JIN */

// var Update = appdf.req(appdf.BASE_SRC + "app.controllers.ClientUpdate");
// var QueryDialog = appdf.req("base.src.app.views.layer.other.QueryDialog");

var UpdateScene = cc.Scene.extend({
    // 初始化界面
    ctor : function () {
        this._super();
        cc.log("onCreate updateScene.");
        this.m_tabUpdateGame = _updategame;

        //背景
        var newbasepath = jsb.fileUtils.getWritablePath() + "/baseupdate/";
        var bgfile = newbasepath + "base/res/background.png";
        var sp = cc.Sprite.create(bgfile);
        if (null == sp) {
            sp = new cc.Sprite("background.png");
        }
        if (null != sp) {
            sp.setPosition(appdf.WIDTH / 2, appdf.HEIGHT / 2);
            this.addChild(sp);
        }

        //标签
        var tipfile = newbasepath + "base/res/logo.png";
        if (false == jsb.fileUtils.isFileExist(tipfile)) {
            tipfile = "logo.png";
        }
        sp = new cc.Sprite(tipfile);
        if (null == sp)
            sp = new cc.Sprite("logo.png");

        if (null != sp) {
            sp.setPosition(appdf.WIDTH / 2, appdf.HEIGHT / 2 + 100);
            this.addChild(sp);
            sp.runAction(cc.RepeatForever.create(new cc.Sequence(cc.FadeTo.create(2, 255), cc.FadeTo.create(2, 128))));
        }

        //slogan
        var sloganfile = newbasepath + "base/res/logo_text_00.png";
        if (false == jsb.fileUtils.isFileExist(sloganfile)) {
            sloganfile = "logo_text_00.png";
        }
        sp = new cc.Sprite(sloganfile);
        if (null == sp) {
            sp = new cc.Sprite("logo_text_00.png");
        }
        if (null != sp) {
            sp = new cc.Sprite(sloganfile);
            sp.setPosition(appdf.WIDTH / 2, 200);
            this.addChild(sp);
        }
        //sp.setVisible(false)
        //提示文本
        this._txtTips = cc.LabelTTF("", "fonts/round_body.ttf", 24);
        this._txtTips.setColor(cc.color(0, 250, 0, 255));
        this._txtTips.setAnchorPoint(cc.p(1, 0));
        this._txtTips.enableOutline(cc.color(0, 0, 0, 255), 1);
        this._txtTips.setPosition(appdf.WIDTH, 0);
        this.addChild(this._txtTips);

        this.m_progressLayer = cc.Layer(cc.color(0, 0, 0, 0));
        this.addChild(this.m_progressLayer);
        this.m_progressLayer.setVisible(false);
        //总进度
        var total_bg = new cc.Sprite("wait_frame_0.png");
        this.m_spTotalBg = total_bg;
        this.m_progressLayer.addChild(total_bg);
        total_bg.setPosition(appdf.WIDTH / 2, 80);
        this.m_totalBar = new ccui.LoadingBar();
        this.m_totalBar.loadTexture("wait_frame_3.png");
        this.m_progressLayer.addChild(this.m_totalBar);
        this.m_totalBar.setPosition(appdf.WIDTH / 2, 80);
        this._totalTips = new cc.LabelTTF("", "fonts/round_body.ttf", 20);
        this._totalTips.setName("text_tip");
        this._totalTips.enableOutline(cc.color(0, 0, 0, 255), 1);
        this._totalTips.setPosition(this.m_totalBar.getContentSize().width * 0.5, this.m_totalBar.getContentSize().height * 0.5);
        this._totalTips.addChild(this.m_totalBar);
        this.m_totalThumb = new cc.Sprite("thumb_1.png");
        this.m_totalBar.addChild(this.m_totalThumb);
        this.m_totalThumb.setPositionY(this.m_totalBar.getContentSize().height * 0.5);
        // this.updateBar(this.m_totalBar, this.m_totalThumb, 0);

        //单文件进度
        var file_bg = new cc.Sprite("wait_frame_0.png");
        this.m_spFileBg = file_bg;
        this.m_progressLayer.addChild(file_bg);
        file_bg.setPosition(appdf.WIDTH / 2, 120);
        this.m_fileBar = new ccui.LoadingBar();
        this.m_fileBar.loadTexture("wait_frame_2.png");
        this.m_fileBar.setPercent(0);
        this.m_progressLayer.addChild(this.m_fileBar);
        this.m_fileBar.setPosition(appdf.WIDTH / 2, 120);
        this._fileTips = cc.LabelTTF("", "fonts/round_body.ttf", 20);
        this._fileTips.setName("text_tip");
        this._fileTips.enableOutline(cc.color(0, 0, 0, 255), 1);
        this._fileTips.move(this.m_fileBar.getContentSize().width * 0.5, this.m_fileBar.getContentSize().height * 0.5);
        this.m_fileBar.addChild(this._fileTips);
        this.m_fileThumb = cc.Sprite.create("thumb_0.png");
        this.m_fileBar.addChild(this.m_fileThumb);
        this.m_fileThumb.setPositionY(this.m_fileBar.getContentSize().height * 0.5);
        this.updateBar(this.m_fileBar, this.m_fileThumb, 0);
    },

    // 进入场景而且过渡动画结束时候触发。
    onEnterTransitionFinish : function () {
        this.goUpdate();
    },

    goUpdate : function () {
        this.m_progressLayer.setVisible(true);
        updategame = this.m_tabUpdateGame;

        //更新参数
        var newfileurl = _updateUrl + "/game/" + updategame._Module + "/res/filemd5List.json";
        var dst = jsb.fileUtils.getWritablePath() + "game/" + updategame._Type + "/";
        var targetPlatform = cc.sys.platform;
        if (cc.sys.WIN32 == targetPlatform) {
            dst = jsb.fileUtils.getWritablePath() + "download/game/" + updategame._Type + "/";
        }

        var src = jsb.fileUtils.getWritablePath() + "game/" + updategame._Module + "/res/filemd5List.json";
        var downurl = _updateUrl + "/game/" + updategame._Type + "/";

        //创建更新
        // Update.create(newfileurl, dst, src, downurl)
        var updatectrl = new ClientUpdate(newfileurl, dst, src, downurl);
        updatectrl.updateClient(this);
    },

    //更新进度
    updateProgress : function (sub, msg, mainpersent) {
        this.updateBar(this.m_fileBar, this.m_fileThumb, sub);
        this.updateBar(this.m_totalBar, this.m_totalThumb, mainpersent);
    },

    //更新结果
    updateResult : function (result,msg) {
        if (null != this.m_spDownloadCycle) {
            this.m_spDownloadCycle.stopAllActions();
            this.m_spDownloadCycle.setVisible(false);
        }

        if (result == true) {
            this._txtTips.setString("OK");
            _version.setResVersion(this.m_tabUpdateGame._ServerResVersion, this.m_tabUpdateGame._KindID);
            //进入游戏列表
            this.getApp().enterSceneEx(appdf.CLIENT_SRC + "plaza.views.ClientScene", "FADE", 1);
            FriendMgr.getInstance().reSetAndLogin();
        } else {
            this.m_progressLayer.setVisible(false);
            this.updateBar(this.m_fileBar, this.m_fileThumb, 0);
            this.updateBar(this.m_totalBar, this.m_totalThumb, 0);

            //重试询问
            this._txtTips.setString("");
            QueryDialog.create(msg + "\n是否重试？", function (bReTry) {
                if (bReTry == true) {
                    this.goUpdate();
                } else {
                    os.exit(0);
                }
            })
            QueryDialog.setCanTouchOutside(false)
            this.addChild(QueryDialog)
        }
    },

    updateBar : function (bar, thumb, percent) {
        if (null == bar || null == thumb) {
            return;
        }
        var text_tip = bar.getChildByName("text_tip");
        if (null != text_tip) {
            var str = string.format("%d%%", percent);
            text_tip.setString(str);
        }

        bar.setPercent(percent);
        var size = bar.getVirtualRendererSize();
        thumb.setPositionX(size.width * percent / 100);
    }

});



