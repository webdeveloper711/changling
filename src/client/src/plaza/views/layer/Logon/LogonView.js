var LogonView = cc.Layer.extend({

    ctor : function (serverConfig) {
        this._super();
        cc.log("LogonView:onCreate");

        var self = this;
        this.setContentSize(yl.WIDTH,yl.HEIGHT);
        //ExternalFun.registerTouchEvent(this)

        var btcallback = function(sender, target) {
            if (target == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(sender.getTag(), sender);
            }
        };
        var cbtlistener = function (sender,eventType) {
            self.onSelectedEvent(sender, eventType);
        };

        var editHanlder = function ( name, sender ) {
            self.onEditEvent(name, sender);
        };

        var offset = cc.p(0, 90);

        // 帐号提示
        this.m_spAccount = new cc.Sprite(res.account_text_png);
        this.m_spAccount.setPosition(466, 381 + offset.y);
        this.m_spAccount.setAnchorPoint(cc.p(1, 0.5));
        this.addChild(this.m_spAccount);

        // 账号输入
        this.edit_Account = new cc.EditBox(cc.size(490, 51), new cc.Scale9Sprite(res.text_field_frame_png));
        this.edit_Account.setPosition(yl.WIDTH/2 + 100, 381 + offset.y);
        this.edit_Account.setAnchorPoint(cc.p(0.5, 0.5));
        this.edit_Account.setFont(res.round_body_ttf, 24);
        this.edit_Account.setPlaceholderFont(res.round_body_ttf, 24);
        //this.edit_Account.setFontSize(24);
        //this.edit_Account.setPlaceholderFontSize(24);
        this.edit_Account.setMaxLength(31);
        this.edit_Account.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        this.addChild(this.edit_Account);
        // this.edit_Account.registerScriptEditBoxHandler(editHanlder);

        // 密码提示
        this.m_spPasswd = new cc.Sprite(res.password_text_png);
        this.m_spPasswd.setPosition(466, 300 + offset.y);
        this.m_spPasswd.setAnchorPoint(cc.p(1, 0.5));
        this.addChild(this.m_spPasswd);

        // 密码输入
        this.edit_Password = new cc.EditBox(cc.size(490, 51), new cc.Scale9Sprite(res.text_field_frame_png));
        this.edit_Password.setPosition(yl.WIDTH/2 + 100, 300 + offset.y);
        this.edit_Password.setAnchorPoint(cc.p(0.5, 0.5));
        this.edit_Password.setFont(res.round_body_ttf, 24);
        this.edit_Password.setPlaceholderFont(res.round_body_ttf, 24);
        // this.edit_Password.setFontSize(24);
        // this.edit_Password.setPlaceholderFontSize(24);
        this.edit_Password.setMaxLength(26);
        this.edit_Password.setInputFlag(cc.EDITBOX_INPUT_FLAG_PASSWORD);
        this.edit_Password.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        this.addChild(this.edit_Password);

        this.m_btnShowPasswd = new ccui.Button(res.login_btn_hidepw_0_png, res.login_btn_hidepw_1_png);
        this.m_btnShowPasswd.setTag(LogonView.BT_SHOWPW);
        this.m_btnShowPasswd.setPosition(850 + 100, 295 +offset.y);
        this.addChild(this.m_btnShowPasswd);
        // this.m_btnFgPasswd.addChilduchEventListener(btcallback);
        this.m_btnShowPasswd.setSwallowTouches(true);
        this.m_btnShowPasswd.setLocalZOrder(1);

        this.m_btnShowPasswd.setVisible(false);
        this.m_btnShowPasswd.setEnabled(false);

        // 忘记密码
        this.m_btnFgPasswd = new ccui.Button(res.btn_login_fgpw_png);
        this.m_btnFgPasswd.setTag(LogonView.BT_FGPW);
        this.m_btnFgPasswd.setPosition(900,330);
        this.addChild(this.m_btnFgPasswd);
        // this.m_btnFgPasswd.addChilduchEventListener(btcallback);

        // 记住密码
        this.cbt_Record = new ccui.CheckBox(res.rem_password_button_png, res.choose_button_png);
        this.cbt_Record.setPosition(515, 165);
        this.cbt_Record.setSelected(GlobalUserItem.bSavePassword);
        this.cbt_Record.setTag(LogonView.CBT_RECORD);
        this.addChild(this.cbt_Record);

        this.cbt_Record.setVisible(false);
        this.cbt_Record.setEnabled(false);

        // 自动登录
        this.cbt_Auto = new ccui.CheckBox(res.cbt_auto_0_png, res.cbt_auto_1_png);
        this.cbt_Auto.setPosition(700 - 93, 245);
        this.cbt_Auto.setSelected(GlobalUserItem.bSavePassword);
        this.cbt_Auto.setTag(LogonView.CBT_AUTO);
        this.addChild(this.cbt_Auto);

        // 注册按钮
        this.m_btnRegister = new ccui.Button(res.regist_button_png);
        this.m_btnRegister.setTag(LogonView.BT_REGISTER);
        this.m_btnRegister.setPosition(640, 330);
        this.addChild(this.m_btnRegister);
        this.m_btnRegister.addTouchEventListener(btcallback);

        // 用户协议
        this.m_btnAgreement = new ccui.Button(res.bt_regist_agreement_png);
        this.m_btnAgreement.setTag(LogonView.BT_AGREEMENT);
        this.m_btnAgreement.setPosition(700, 150);
        this.addChild(this.m_btnAgreement);
        this.m_btnAgreement.addTouchEventListener(btcallback);

        // 同意用户协议
        this.cbt_Agreement = new ccui.CheckBox(res.priland_check_dot_0_png,res.priland_check_dot_1_png);
        this.cbt_Agreement.setPosition(500,150);
        this.cbt_Agreement.setSelected(true);
        this.cbt_Agreement.setTag(LogonView.CBT_AGREEMENT);
        this.addChild(this.cbt_Agreement);

        //账号登录
        var btn = new ccui.Button(res.logon_button_0_png, res.logon_button_1_png, res.logon_button_2_png);
        btn.setTag(LogonView.BT_LOGON);
        btn.setPosition(cc.p(0,0));
        btn.setName("btn_1");
        this.addChild(btn);
        btn.addTouchEventListener(btcallback);

        //游客登录
        btn = new ccui.Button(res.visitor_button_0_png, res.visitor_button_1_png, res.visitor_button_2_png);
        btn.setTag(LogonView.BT_VISITOR);
        btn.setPosition(new cc.p(0,0));
        btn.setEnabled(false);
        btn.setVisible(false);
        btn.setName("btn_2");
        this.addChild(btn);
        btn.addTouchEventListener(btcallback);

        //微信登陆
        btn = new ccui.Button(res.thrid_part_wx_0_png, res.thrid_part_wx_1_png, res.thrid_part_wx_2_png);
        btn.setTag(LogonView.BT_WECHAT);
        btn.setPosition(cc.p(0,0));
        btn.setVisible(false);
        btn.setEnabled(false);
        btn.setName("btn_3");
        this.addChild(btn);
        // btn.addChilduchEventListener(btcallback);

        m_serverConfig = serverConfig || {};
        this.refreshBtnList();
    },
    refreshBtnList : function( ) {
        for (i = 0; i < 3; i++) {
            var btn = this.getChildByName("btn_" + i);
            if (btn != null) {
                btn.setVisible(false);
                btn.setEnabled(false);
            }
        }

        var btnpos = [
            [cc.p(667, 230), cc.p(0, 0), cc.p(0, 0)],
            [cc.p(463, 230), cc.p(868, 230), cc.p(0, 0)],
            [cc.p(372, 230), cc.p(667, 230), cc.p(962, 230)]
        ];

        // 登陆限定
        var loginConfig = appdf.LOGIN_CONFIG; //this.m_serverConfig.moblieLogonMode or 0

        var btnlist = [];
        if (1 == bit._and(loginConfig, 1)) {
            btnlist.push("btn_1");
        } else {
            // 隐藏帐号输入信息
            this.hideAccountInfo();
        }

        if (null == GlobalUserItem.getBindingAccount() && (2 == bit._and(loginConfig, 2))) {
            btnlist.push("btn_2");
        }

        // var enableWeChat = this.m_serverConfig.wxLogon || 1;
        if (4 == bit._and(loginConfig, 4)) {
            btnlist.push("btn_3");
        }

        var poslist = btnpos[btnlist.length -1];
        for (var i = 0; i < btnlist.length; i++) {
            var tmp = this.getChildByName(btnlist[i]);
            if (null != tmp) {
                tmp.setEnabled(true);
                tmp.setVisible(true);

                var pos = poslist[i];
                if (null != pos)
                    tmp.setPosition(pos);
            }
        }
    },
    onEditEvent : function(name, editbox) {
        if ("changed" == name) {
            if (editbox.getText() != GlobalUserItem.szAccount) {

            }
        }
    },
    onReLoadUser : function() {
        if (GlobalUserItem.szAccount != null && GlobalUserItem.szAccount != "")
            this.edit_Account.string = GlobalUserItem.szAccount.toUpperCase();
        else
            this.edit_Account.setPlaceHolder("请输入您的游戏帐号");

        if (GlobalUserItem.szPassword != null && GlobalUserItem.szPassword != "")
            this.edit_Password.setText(GlobalUserItem.szPassword);
        else
            this.edit_Password.setPlaceHolder("请输入您的游戏密码");
    },
    onButtonClickedEvent : function(tag, ref) {
        if (tag == LogonView.BT_REGISTER) {
            GlobalUserItem.bVisitor = false;
            this.getParent().getParent().onShowRegister();

        } else if (tag == LogonView.BT_VISITOR) {     //游客登陆

            var bAuto = this.cbt_Agreement.isSelected();
            if (bAuto == false) {
                showToast(this, "请阅读并同意用户服务条款", 2);
                return;
            }
            GlobalUserItem.bVisitor = true;
            this.getParent().getParent().onVisitor();

        } else if (tag == LogonView.BT_AGREEMENT) {
            this.getParent().getParent().onShowService();

        } else if (tag == LogonView.BT_LOGON) {   //账号登陆
            var bAuto = this.cbt_Agreement.isSelected();
            if (bAuto == false) {
                showToast(this, "请阅读并同意用户服务条款", 2);
                return;
            }
            GlobalUserItem.bVisitor = false;
            var szAccount = this.edit_Account.string.replace(" ", "");
            var szPassword = this.edit_Password.string.replace(" ", "");
            var bAuto = this.getChildByTag(LogonView.CBT_RECORD).isSelected();
            var bSave = this.getChildByTag(LogonView.CBT_RECORD).isSelected();
            this.getParent().getParent().onLogon(szAccount, szPassword, bSave, bAuto);

        } else if (tag == LogonView.BT_THIRDPARTY) {
            this.m_spThirdParty.setVisible(true);

        } else if (tag == LogonView.BT_WECHAT) {      //微信登陆
            var bAuto = this.cbt_Agreement.isSelected();
            if (bAuto == false) {
                showToast(this, "请阅读并同意用户服务条款", 2);
                return;
            }
            //平台判定
            var targetPlatform = cc.sys.platform;
            if ((cc.sys.IPHONE == targetPlatform) || (cc.sys.IPAD == targetPlatform) || (cc.sys.ANDROID == targetPlatform)) {
                this.getParent().getParent().thirdPartyLogin(yl.ThirdParty.WECHAT, yl.THIRDLOGIN_SAVE, yl.AUTO_LOGIN);
            } else {
                showToast(this, "不支持的登录平台 ==> " + targetPlatform, 2);
            }

        } else if (tag == LogonView.BT_FGPW) {
            MultiPlatform.getInstance().openBrowser(yl.HTTP_URL + "/Mobile/RetrievePassword.aspx");

        } else if (tag == LogonView.BT_SHOWPW) {
            this.m_btnShowPasswd.loadTextureDisabled("Logon/login_btn_showpw_0.png");
            this.m_btnShowPasswd.loadTextureNormal("Logon/login_btn_showpw_0.png");
            this.m_btnShowPasswd.loadTexturePressed("Logon/login_btn_showpw_1.png");
            this.m_btnShowPasswd.setTag(LogonView.BT_HIDEPW);

            var txt = this.edit_Password.getText();
            this.edit_Password.removeFromParent();
            //密码输入	
            this.edit_Password = new ccui.EditBox(cc.size(490, 67), "Logon/text_field_frame.png");
            this.edit_Password.setPosition(yl.WIDTH / 2, 280);
            this.edit_Password.setAnchorPoint(cc.p(0.5, 0.5));
            this.edit_Password.setFontName(res.round_body_ttf);
            this.edit_Password.setPlaceholderFontName(res.round_body_ttf);
            this.edit_Password.setFontSize(24);
            this.edit_Password.setPlaceholderFontSize(24);
            this.edit_Password.setMaxLength(26);
            this.edit_Password.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
            this.addChild(this.edit_Password);
            this.edit_Password.setText(txt);

        } else if (tag == LogonView.BT_HIDEPW) {
            this.m_btnShowPasswd.loadTextureDisabled("Logon/login_btn_hidepw_0.png");
            this.m_btnShowPasswd.loadTextureNormal("Logon/login_btn_hidepw_0.png");
            this.m_btnShowPasswd.loadTexturePressed("Logon/login_btn_hidepw_1.png");
            this.m_btnShowPasswd.setTag(LogonView.BT_SHOWPW);

            var txt = this.edit_Password.getText();
            this.edit_Password.removeFromParent();
            //密码输入	
            this.edit_Password = ccui.EditBox.create(cc.size(490, 67), "Logon/text_field_frame.png");
            this.edit_Password.setPosition(yl.WIDTH / 2, 280);
            this.edit_Password.setAnchorPoint(cc.p(0.5, 0.5));
            this.edit_Password.setFontName(res.round_body_ttf);
            this.edit_Password.setPlaceholderFontName(res.round_body_ttf);
            this.edit_Password.setFontSize(24);
            this.edit_Password.setPlaceholderFontSize(24);
            this.edit_Password.setMaxLength(26);
            this.edit_Password.setInputFlag(cc.EDITBOX_INPUT_FLAG_PASSWORD);
            this.edit_Password.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
            this.addChild(this.edit_Password);
            this.edit_Password.setText(txt);
        }
    },
    onTouchBegan : function(touch, event) {
        return self.isVisible();
    },
    
    onTouchEnded : function(touch, event) {
        var pos = touch.getLocation();
        var m_spBg = self.m_spThirdParty;
        pos = m_spBg.convertToNodeSpace(pos);
        var rec = cc.rect(0, 0, m_spBg.getContentSize().width, m_spBg.getContentSize().height);
        if (false == cc.rectContainsPoint(rec, pos))
            self.m_spThirdParty.setVisible(false);
    },
    
    hideAccountInfo : function() {
        self.m_spAccount.setVisible(false);
        //账号输入
        self.edit_Account.setVisible(false);
        self.edit_Account.setEnabled(false);

        self.m_spPasswd.setVisible(false);
        // 密码输入
        self.edit_Password.setVisible(false);
        self.edit_Password.setEnabled(false);

        // 查看密码
        self.m_btnShowPasswd.setVisible(false);
        self.m_btnShowPasswd.setEnabled(false);

        // 忘记密码
        self.m_btnFgPasswd.setVisible(false);
        self.m_btnFgPasswd.setEnabled(false);

        // 记住密码
        self.cbt_Record.setVisible(false);
        self.cbt_Record.setEnabled(false);

        // 注册账号
        self.m_btnRegister.setVisible(false);
        self.m_btnRegister.setEnabled(false);

        // 自动登录
        yl.AUTO_LOGIN = false;
    }
});

LogonView.BT_LOGON = 1;                  //登陆
LogonView.BT_REGISTER = 2;               //注册
LogonView.CBT_RECORD = 3;                //记住密码
LogonView.CBT_AUTO = 4;                  //自动登录
LogonView.BT_VISITOR = 5;                //游客登录
LogonView.BT_WEIBO = 6;                  //微博
LogonView.BT_QQ	= 7;                     //QQ
LogonView.BT_THIRDPARTY	= 8;             //第三方登陆
LogonView.BT_WECHAT	= 9;                 //微信登陆
LogonView.BT_FGPW = 10; 	                // 忘记密码
LogonView.BT_SHOWPW = 11;                // 显示密码
LogonView.BT_HIDEPW = 12;                // 隐藏密码
LogonView.BT_AGREEMENT = 13;             //用户协议
LogonView.CBT_AGREEMENT = 14;            //同意用户协议

