var RegisterView = cc.Layer.extend({

    BT_REGISTER     : 1,
    BT_RETURN	    : 2,
    BT_AGREEMENT    : 3,
    CBT_AGREEMENT   : 4,

    bAgreement      : true,

    ctor : function () {
        this._super();
        var self = this;
        this.setContentSize(yl.WIDTH, yl.HEIGHT);

        cc.spriteFrameCache.addSpriteFrames(res.public_plist);

        var  btcallback = function(sender, target) {
            if (target == ccui.Widget.TOUCH_ENDED)
                self.onButtonClickedEvent(sender.getTag(), sender);
        };

        // 背景
        var background = new cc.Sprite(res.regist_background_png);
        background.setPosition(yl.WIDTH/2, yl.HEIGHT/2);
        this.addChild(background);

        // Top背景
        var frame = cc.spriteFrameCache.getSpriteFrame("sp_top_bg.png");
        if (frame != null) {
            var sp = new cc.Sprite(frame);
            sp.setPosition(yl.WIDTH/2, yl.HEIGHT-51);
            this.addChild(sp);
        }

        // Top标题
        var title = new cc.Sprite(res.title_regist_png);
        title.setPosition(yl.WIDTH/2, yl.HEIGHT-51);
        this.addChild(title);

        //Top返回
        var btnBack = new ccui.Button(res.bt_return_0_png, res.bt_return_1_png);
        btnBack.setTag(this.BT_RETURN);
        btnBack.setPosition(75, yl.HEIGHT - 51);
        this.addChild(btnBack);
        btnBack.addTouchEventListener(btcallback);

        var offset = cc.p(0, -100);

        // 帐号提示
        var iconRegistTip1 = new cc.Sprite(res.icon_regist_tip_png);
        iconRegistTip1.setPosition(293, 540 + offset.y);
        this.addChild(iconRegistTip1);

        var textAccount1 = new cc.Sprite(res.text_regist_account_png);
        textAccount1.setPosition(392, 540 + offset.y);
        this.addChild(textAccount1);

        // 账号输入
        this.edit_Account = new cc.EditBox(cc.size(490, 67), new cc.Scale9Sprite(res.text_field_regist_png));
        this.edit_Account.setPosition(730, 540 + offset.y);
        this.edit_Account.setAnchorPoint(cc.p(0.5, 0.5));
        this.edit_Account.setFontName(res.round_body_ttf);
        this.edit_Account.setPlaceholderFontName(res.round_body_ttf);
        this.edit_Account.setFontSize(24);
        this.edit_Account.setPlaceholderFontSize(24);
        this.edit_Account.setMaxLength(31);
        this.edit_Account.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        this.edit_Account.setPlaceHolder("6-31位字符");
        this.addChild(this.edit_Account);

        // 密码提示
        var iconRegistTip2 = new cc.Sprite(res.icon_regist_tip_png);
        iconRegistTip2.setPosition(293, 450 + offset.y);
        this.addChild(iconRegistTip2);

        var textAccount2 = new cc.Sprite(res.text_regist_password_png);
        textAccount2.setPosition(392, 450 + offset.y);
        this.addChild(textAccount2);

        // 密码输入
        this.edit_Password = new cc.EditBox(cc.size(490, 67), new cc.Scale9Sprite(res.text_field_regist_png));
        this.edit_Password.setPosition(730, 450 + offset.y);
        this.edit_Password.setAnchorPoint(cc.p(0.5, 0.5));
        this.edit_Password.setFontName(res.round_body_ttf);
        this.edit_Password.setPlaceholderFontName(res.round_body_ttf);
        this.edit_Password.setFontSize(24);
        this.edit_Password.setPlaceholderFontSize(24);
        this.edit_Password.setMaxLength(26);
        this.edit_Password.setInputFlag(cc.EDITBOX_INPUT_FLAG_PASSWORD);
        this.edit_Password.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        this.edit_Password.setPlaceHolder("6-26位英文字母，数字，下划线组合");
        this.addChild(this.edit_Password);

        // 确认密码提示
        var iconRegistTip3 = new cc.Sprite(res.icon_regist_tip_png);
        iconRegistTip3.setPosition(293, 358 + offset.y);
        this.addChild(iconRegistTip3);

        var textAccount3 = new cc.Sprite(res.text_regist_confirm_png);
        textAccount3.setPosition(392, 358 + offset.y);
        this.addChild(textAccount3);

        // 确认密码输入
        this.edit_RePassword = new cc.EditBox(cc.size(490, 67), new cc.Scale9Sprite(res.text_field_regist_png));
        this.edit_RePassword.setPosition(730, 358 + offset.y);
        this.edit_RePassword.setAnchorPoint(cc.p(0.5, 0.5));
        this.edit_RePassword.setFontName(res.round_body_ttf);
        this.edit_RePassword.setPlaceholderFontName(res.round_body_ttf);
        this.edit_RePassword.setFontSize(24);
        this.edit_RePassword.setPlaceholderFontSize(24);
        this.edit_RePassword.setMaxLength(26);
        this.edit_RePassword.setInputFlag(cc.EDITBOX_INPUT_FLAG_PASSWORD);
        this.edit_RePassword.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        this.edit_RePassword.setPlaceHolder("6-26位英文字母，数字，下划线组合");
        this.addChild(this.edit_RePassword);

        // 推广员
        this.edit_Spreader = new cc.EditBox(cc.size(490,67), new ccui.Scale9Sprite(res.text_field_regist_png));
        this.edit_Spreader.setPosition(730,268);
        this.edit_Spreader.setAnchorPoint(cc.p(0.5,0.5));
        this.edit_Spreader.setFontName("fonts/round_body.ttf");
        this.edit_Spreader.setPlaceholderFontName("fonts/round_body.ttf");
        this.edit_Spreader.setFontSize(24);
        this.edit_Spreader.setPlaceholderFontSize(24);
        this.edit_Spreader.setMaxLength(32);
        this.edit_Spreader.setInputMode(cc.EDITBOX_INPUT_MODE_NUMERIC); //.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE)
        this.edit_Spreader.setPlaceHolder("请输入推广员ID");
        this.addChild(this.edit_Spreader);


        this.edit_Spreader.setVisible(false);
        // this.edit_Spreader.setEnabled(false);

        // 条款协议
        this.cbt_Agreement = new ccui.CheckBox(res.choose_regist_0_png,"",res.choose_regist_1_png,"","");
        this.cbt_Agreement.setPosition(510,183);
        this.cbt_Agreement.setSelected(this.bAgreement);
        this.cbt_Agreement.setTag(this.CBT_AGREEMENT);
        this.addChild(this.cbt_Agreement);

        this.cbt_Agreement.setVisible(false);
        this.cbt_Agreement.setEnabled(false);

        var btnRegist = new ccui.Button(res.bt_regist_0_png);
        btnRegist.setTag(this.BT_REGISTER);
        btnRegist.setPosition(yl.WIDTH/2, 180);
        this.addChild(btnRegist);
        btnRegist.addTouchEventListener(btcallback);
    },
    onButtonClickedEvent : function (tag, ref) {
        if (tag == this.BT_RETURN) {
            this.getParent().getParent().onShowLogon();
        } else if (tag == this.BT_AGREEMENT) {
            this.getParent().getParent().onShowService();
        } else if (tag == this.BT_REGISTER) {
            // 判断 非 数字、字母、下划线、中文 的帐号
            var szAccount = this.edit_Account.getString();
            var pattern = "^[a-zA-Z0-9_\\u3400-\\uFAFF]{6,}$";
            var filter = szAccount.match(pattern);
            cc.log("filter : " + filter);
            if (filter == null) {
                showToast(this, "帐号包含非法字符, 请重试!", 1);
                return;
            }
            szAccount = szAccount.replace(" ", "");
            var szPassword = this.edit_Password.getString().replace(" ", "");
            var szRePassword = this.edit_RePassword.getString().replace(" ", "");
            var bAgreement = this.getChildByTag(this.CBT_AGREEMENT).isSelected();
            var szSpreader = this.edit_Spreader.getString().replace(" ", "");
            this.getParent().getParent().onRegister(szAccount,szPassword,szRePassword,bAgreement,szSpreader);
        }
    },
    setAgreement : function (bAgree) {
        this.cbt_Agreement.setSelected(bAgree);
    }
});


