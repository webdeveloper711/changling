/*Author : JIN */
// var PopupInfoHead = appdf.req(appdf.EXTERNAL_SRC + "PopupInfoHead");
// var QueryDialog = appdf.req(appdf.BASE_SRC+"app.views.layer.other.QueryDialog");
// var ExternalFun = appdf.req(appdf.EXTERNAL_SRC + "ExternalFun");
// var chat_cmd = appdf.req(appdf.HEADER_SRC+"CMD_ChatServer");
// var NotifyMgr = appdf.req(appdf.EXTERNAL_SRC + "NotifyMgr");

//global
cc.SCROLLVIEW_SCRIPT_SCROLL = 0;
cc.SCROLLVIEW_SCRIPT_ZOOM   = 1;
cc.TABLECELL_TOUCHED        = 2;
cc.TABLECELL_HIGH_LIGHT     = 3;
cc.TABLECELL_UNHIGH_LIGHT   = 4;
cc.TABLECELL_WILL_RECYCLE   = 5;
cc.TABLECELL_SIZE_FOR_INDEX = 6;
cc.TABLECELL_SIZE_AT_INDEX  = 7;
cc.NUMBER_OF_CELLS_IN_TABLEVIEW = 8;

var FriendChatList = {

    //maximum label text width
    MAX_LINE_WIDTH : 320,
    ctor:function (view, dwUserID, superLayer) {
        this.m_ChatLayer = view;
        this.m_Super = superLayer;

        this.m_FriendTableView = null;
        this.m_ChatTableView = null;
        this.m_UserNickLab = null;
        this.m_dwCurrentFriendId = dwUserID;

        this.m_nFriendCount = (FriendMgr.getFriendList()).length;

        var curFriend = FriendMgr.getFriendByID(dwUserID);
        if(curFriend!=null)
        {
            this.chatTab = FriendMgr.getUserRecordMsg(curFriend.dwUserID);
            this.chatTab = this.sortTable(this.chatTab);

            //set the current chat object
            if(this.m_Super.setCurrentChatUser!=null)
            {
                this.m_Super.setCurrentChatUser(curFriend.dwUserID);
            }
        }

        cc.log("FriendChatList currentuser ==> " + curFriend.szNickName);
        this.createChatList(this.m_ChatLayer);

        var LabInput = appdf.getNodeByName(view,"LabInput");
        var EditMsg = appdf.getNodeByName(view, "SendMsgEdit");
        var BtnSendMsg = appdf.getNodeByName(view, "BtnSend");

        if(EditMsg) {
            var EditBoxSize = cc.size(LabInput.getContentSize().width+10, LabInput.getContentSize().height+10);
            EditMsg = new cc.EditBox(EditBoxSize, res.dikuang20_png);
            EditMsg.setFontSize(30);
            EditMsg.setPlaceholderFontSize(30);
            EditMsg.setFontName(res.round_body_ttf);
            EditMsg.setFontColor(cc.Color(206,211,255));
            EditMsg.setPlaceHolder("请输入要发送的消息文字");
            EditMsg.setPlaceholderFontName(res.round_body_ttf);
            EditMsg.setPlaceholderFontColor(cc.Color(206,211,255));
            EditMsg.setMaxLength(32);
            EditMsg.setReturnType(cc.KEYBOARD_RETURNTYPE_DONE);
            EditMsg.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
            EditMsg.setPosition(LabInput.getPosition());
            EditMsg.setName("sendMsgEdit");
            LabInput.getParent().addChild(EditMsg);
        }

        if(LabInput) {
            labInput.removeFromParent();
        }

        //send button
        BtnSendMsg.addTouchEventListener(function(ref, tType){
            if(tType === ccui.Widget.TOUCH_ENDED){
                var curFriend = FriendMgr.getInstance().getFriendById(this.m_dwCurrentFriendId);
                if(curFriend == null){
                    return;
                }

                //judge offline
                if(curFriend.cbMainStatus == chat_cmd.FRIEND_US_OFFLINE){
                    showToast(this.m_ChatLayer, "好友已离线!", 3);
                    return;
                }
                var content = EditMsg.getText();
                if(content.length<1){
                    showToast(this.m_ChatLayer,"聊天内容不能为空", 3);
                } else {
                    //sensitive word filtering
                    if(ExternalFun.isContainBadWords(content)==true){
                        showToast(this.m_ChatLayer,"聊天内容包含敏感词汇!", 3);
                    }

                    var sendTab = {};
                    sendTab.dwUserID = GlobalUserItem.dwUserID;
                    sendTab.dwTargetUserId = curFriend.dwUserID;
                    sendTab.dwFontColor = 0;
                    sendTab.cbFontSize = 0;
                    sendTab.cbFontAttri =0;
                    sendTab.szFontName = "微软雅黑";
                    sendTab.szMessageContent = content;

                    FriendMgr.getInstance().sendMessageFriend(sendTab);
                    EditMsg.setText("");
                }
            }
        });
    },

    createChatList : function(view){
        var FriendListTopBar = appdf.getNodeByName(view, "Image_3");
        var FriendChatListTopBar = appdf.getNodeByName(view, "Image_5");

        var FriendListSize = FriendListTopBar.getContentSize();
        var ChatListSize = FriendChatListTopBar.getContentSize();
        var ChatLayerSize = view.getContentSize();

        this.m_UserNickLab = appdf.getNodeByName(view, "LabNick");
        var curFriend = FriendMgr.getInstance().getFriendById(this.m_dwCurrentFriendId);
        var nickname = "";
        if(curFriend != null){
            nickname = curFriend.szNickname;
        }

        this.m_UserNickLab.setString(nickname);

        //Friends list
        if(this.m_FriendTableView) {
            this.m_FriendTableView = new cc.TableView(cc.size(FriendListSize.width,ChatLayerSize.height-125));
            this.m_FriendTableView.setColor(cc.Color(158,200,200));
                this.m_FriendTableView.setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL);
                this.m_FriendTableView.setPosition(cc.p(FriendListTopBar.getPositionX()-FriendListSize.width/2-5,30));
            this.m_FriendTableView.setDelegate();

            view.addChild(this.m_FriendTableView);
            this.m_FriendTableView.registerScriptHandler(handler(this,this.tableCellTouched),cc.TABLECELL_TOUCHED);
            this.m_FriendTableView.registerScriptHandler(handler(this,this.cellSizeForTable),cc.TABLECELL_SIZE_FOR_INDEX);
            this.m_FriendTableView.registerScriptHandler(handler(this,this.tableCellAtIndex()),cc.TABLECELL_SIZE_AT_INDEX);
            this.m_FriendTableView.registerScriptHandler(handler(this,this.numberOfCellsInTableView()),cc.NUMBER_OF_CELLS_IN_TABLEVIEW);

        }

        //current friend chat list 当前好友聊天列表
        if(this.m_ChatTableView == null){
            this.m_ChatTableView = new cc.TableView(cc.size(ChatListSize.width, ChatLayerSize.height-200));
            this.m_ChatTableView.setColor(cc.Color(255,255,255));
            this.m_ChatTableView.setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL);
            this.m_ChatTableView.setPosition(cc.p(FriendChatListTopBar.getPositionX()-ChatListSize.width/2,110));
            this.m_ChatTableView.setDelegate();

            view.addChild(this.m_ChatTableView);
            this.m_ChatTableView.registerScriptHandler(handler(this,this.tableCellTouched),cc.TABLECELL_TOUCHED);
            this.m_ChatTableView.registerScriptHandler(handler(this,this.cellSizeForTable),cc.TABLECELL_SIZE_FOR_INDEX);
            this.m_ChatTableView.registerScriptHandler(handler(this,this.tableCellAtIndex()),cc.TABLECELL_SIZE_AT_INDEX);
            this.m_ChatTableView.registerScriptHandler(handler(this,this.numberOfCellsInTableView()),cc.NUMBER_OF_CELLS_IN_TABLEVIEW);

        }

        this.m_FriendTableView.reloadData();
        this.m_ChatTableView.reloadData();
        this.moveToLastRow();
        this.enableChatListTouch();

    },

    tableCellTouched : function(table, cell)
    {
        if(table == this.m_FriendTableView){
            this.m_dwCurrentFriendId = cell.getTag();
            var curFriend = FriendMgr.getInstance().getFriendById(this.m_dwCurrentFriendId);
            if( curFriend == null) {
                return;
            }
            cc.log("FriendChatList:tableCellTouched ==>" + curFriend.szNickName);
            var dwUserID = curFriend.dwUserID;
            this.m_dwCurrentFriendId = dwUserID;

            //set the current chat object
            if( this.m_Super.setCurrentChatUser != null ){
                this.m_Super.setCurrentChatUser(dwUserID);
            }

            //hide notification
            var node = this.getNotifyNode(dwUserID);
            if( node!=null ){
                NotifyMgr.getInstance().hideNotify(node, true);
            }

            this.chatTab = FriendMgr.getInstance().getUserRecordMsg(dwUserID);
            this.chatTab = this.sortTable(this.chatTab);

            this.m_UserNickLab.setString(curFriend.szNickName);
            this.m_FriendTableView.reloadData();
            this.m_ChatTableView.reloadData();
            this.moveToLastRow();
            this.enableChatListTouch();

        }
    },

    cellSizeForTable : function(table, idx) {
        var FriendListTopBar = appdf.getNodeByName(this.m_ChatLayer, "Image_3");
        var FriendChatListTopBar = appdf.getNodeByName(this.m_ChatLayer,"Image_5");
        var FriendListSize = FriendListTopBar.getContentSize();

        if( table == this.m_FriendTableView ) {
            return FriendListSize.width, 68;
        } else if ( table == this.m_ChatTableView ) {
            var ChatListSize = FriendChatListTopBar.getContentSize();
            var curUserTab = this.chatTab[idx+1];
            if( true == curUserTab.bImage ) {
                return 220,130;
            } else {
                var labelSize = this.calculationLableSize(curUserTab.szMessageContent);
                if( labelSize.height < 30 ) {
                    labelSize.height = 40;
                }
                return ChatListSize.width, labelSize.height+30;
            }
        }
    },

    tableCellAtIndex : function(table, idx) {
        var cell = table.dequeueCell();

        if(table == this.m_FriendTableView ) {
            var curUserTab = FriendMgr.getInstance().getFriendList()[this.m_nFriendCount - idx];
            if(curUserTab == null ) {
                var tvc = new cc.TableViewCell();
                return tvc;
            }

            var defaultCell = null;
            if( cell == null ) {
                cell = new cc.TableViewCell();

                defaultCell = new cc.Sprite(res.dikuang10_png);
                defaultCell.setName('defaultCell');
                defaultCell.setAnchorPoint(cc.p(0,0));
                cell.addChild(defaultCell);

                var defaultCellSize = defaultCell.getContentSize();

                //avatar
                var head = PopupInfoHead.createClipHead(curUserTab, 56);
                head.setPosition(cc.p(100,defaultCellSize.height/2));
                //according to membership level to determine the picture frame
                head.enableHeadFrame(false);
                head.setName('head');
                defaultCell.addChild(head);

                //nickname
                var nickName = new cc.LabelTTF(curUserTab.szNickName, res.round_body_ttf, 20, cc.size(130,40),cc.TEXT_ALIGNMENT_LEFT);
                nickName.setName('nickName');
                nickName.setFontFillColor(cc.color(41,82,146));
                nickName.setAnchorPoint(cc.p(0,0.5));
                nickName.setHorizontalAlignment(cc.TEXT_ALIGNMENT_LEFT);
                nickName.setVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
                nickName.setPosition(cc.p(135, defaultCellSize.height/2));
                defaultCell.addChild(nickName);

                //instructions
                var spUser = new cc.Sprite(res.dikuang17_png);
                spUser.setName('spUser');
                spUser.setAnchorPoint(cc.p(1,0.5));
                spUser.setPosition(cc.p(defaultCellSize.width, defaultCellSize.height/2));

                spUser.setVisible(this.m_dwCurrentFriendId == curUserTab.dwUserID || false);
                defaultCell.addChild(spUser);

                var start = new cc.Sprite(res.tutiao3_png);
                start.setPosition(cc.p(20,defaultCellSize.height/2));
                defaultCell.addChild(start);

                var lv = new cc.Sprite(res.biaoti3_png);
                lv.setPosition(cc.p(50,defaultCellSize.height/2));
                defaultCell.addChild(lv);

                var userStatus = null;
                if(curUserTab.cbMainStatus == chat_cmd.FRIEND_US_OFFLINE) {
                        userStatus = new cc.Sprite(res.biaoti5_png);
                } else {
                    userStatus = new cc.Sprite(res.biaoti4_png);
                }

                if( userStatus != null ) {
                    userStatus.setName("userStatus");
                    userStatus.setAnchorPoint(cc.p(1,0.5));
                    userStatus.setPosition(cc.p(defaultCellSize.width-10,defaultCellSize.height/2));
                    defaultCell.addChild(userStatus);
                }
            } else {
                defaultCell = cell.getChildByName("defaultCell");
                var nickName = defaultCell.getChildByName("nickName");
                var head = defaultCell.getChildByName("head");
                var userStatus = defaultCell.getChildByName("userStatus");
                var spUser = defaultCell.getChildByName('spUser');
                spUser.setVisible(this.m_dwCurrentFriendId == curUserTab.dwUserID || false);
                nickName.setString(curUserTab.szNickName);

                if(curUserTab.cbMainStatus==chat_cmd.FRIEND_US_OFFLINE){
                    var sp = new cc.Sprite(res.biaoti5_png);
                    userStatus.setSpriteFrame(sp.getSpriteFrame());
                } else {
                    var sp = new cc.Sprite(res.biaoti4_png);
                    userStatus.setSpriteFrame(sp.getSpriteFrame());
                }
                cell.setTag(curUserTab.dwUserID);

                //获取未读通知列表 Get unread notifications list
                var unread = NotifyMgr.getInstance().getUnreadNotifyList();
                //隐藏红点 Hide the red dot
                NotifyMgr.getInstance().hideNotify(defaultCell, false);
                //通知红点 notify red dot
                for(k in unread) {
                    var v = unread[k];
                    if(typeof(v.param)=="table"){
                        var sendUser = v.param.dwSenderID;
                        if(sendUser != null && sendUser == curUserTab.dwUserID){
                            NotifyMgr.getInstance().showNotify(defaultCell, v);
                            break;
                        }
                    }
                }
            }
        } else if(table == this.m_ChatTableView ) {
            var curUserTab = this.chatTab[idx+1];
            if(cell == null ) {
                cell =  new cc.TableViewCell();
            } else {
                cell.removeAllChildren();
            }
            var defaultCell = null;
            if(curUserTab.dwSenderID == GlobalUserItem.dwUserID) {
                defaultCell = new cc.Scale9Sprite(res.dikuang16_png, cc.rect(0,0,200,56),cc.rect(20,20,100,5));
            } else {
                defaultCell = new cc.Scale9Sprite(res.dikuang15_png, cc.rect(0,0,200,56),cc.rect(40,20,100,5));
            }

            defaultCell.setAnchorPoint(cc.p(0,0));
            defaultCell.setPosition(cc.p(0,0));
            defaultCell.setName("defaultName");
            cell.addChild(defaultCell);

            //avatar
            var head = PopupInfoHead.createClipHead(curUserTab, 56);
            //根据会员等级确定头像框
            head.enableHeadFrame(false);
            cell.addChild(head);

            //图片内容 image content
            if(curUserTab.bImage == true) {
                if(cc.fileUtils().isFileExist(curUserTab.szMessageContent)){
                    var image = new ccui.ImageView(curUserTab.szMessageContent);
                    image.setScale9Enabled(true);
                    image.setName("share_image");
                    image.setContentSize(cc.size(200,100));
                    defaultCell.addChild(image);
                    image.setTouchEnabled(true);
                    //image.setSwallowTouches(false);
                    image.addTouchEventListener(function(ref, tType){
                       if(tType == ccui.Widget.TOUCH_ENDED){
                           var runScene = cc.director().getRunningScene();
                           if(runScene!=null) {
                               var mask = new ccui.Layout();
                               mask.setContentSize(yl.WIDTH, yl.HEIGHT);
                               mask.setBackGroundColor(cc.Color(0,0,0));
                               mask.setBackGroundColorOpacity(200);
                               mask.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
                               var scaleImage = new ccui.ImageView(curUserTab.szMessageContent);
                               scaleImage.setScale(0.000001);
                               scaleImage.setScale9Enabled(true);
                               //scaleImage.setTouchEnabled(true);
                               //scaleImage.setContentSize(1335, 750);
                               mask.addTouchEventListener(function (ref, tType) {
                                   if(tType == ccui.Widget.TOUCH_ENDED){
                                       scaleImage.stopAllActions();
                                       scaleImage.runAction(cc.Sequence(cc.ScaleTo(0.1,0.01),cc.callFunc(function () {
                                            mask.removeFromParent();
                                       })));
                                   }
                               });
                               scaleImage.setPosition(cc.p(1335*0.5,750*0.5));
                               mask.addChild(scaleImage);
                               runScene.addChild(mask);
                               scaleImage.stopAllActions();
                               var conSize = scaleImage.getContentSize();
                               scaleImage.runAction(cc.Sequence(cc.ScaleTo(0.1,700/conSize.height),cc.CallFunc(function () {
                                   mask.setTouchEnabled(true);
                               })));
                           }
                       }
                    });
                    if(curUserTab.dwSenderID == GlobalUserItem.dwUserID) {
                        image.setPosition(cc.p(107,55));
                    } else {
                        image.setPosition(cc.p(113,55));
                    }
                } else {
                    var mask = new cc.LayerColor(cc.Color(0,0,0,125),200,100);
                    mask.ignoreAnchorPointForPosition(false);
                    mask.setAnchorPoint(cc.p(0.5,0.5));
                    if(curUserTab.dwSenderID==GlobalUserItem.dwUserID){
                        mask.setPosition(cc.p(107,55));
                    } else {
                        mask.setPosition(cc.p(113,55));
                    }
                    defaultCell.addChild(mask);

                    //旋转 rotate
                    var cycle = new cc.Sprite(res.spinner_circle_png);
                    if(cycle != null) {
                        cycle.setPosition(cc.p(113,55));
                        defaultCell.addChild(cycle);
                        cycle.stopAllActions();
                        cycle.runAction(cc.Sequence(cc.Repeat(cc.RotateBy(1.0,360),10),cc.RemoveSelf(true)));
                    }
                }
                defaultCell.setContentSize(220,110);
            } else {
                var labelSize = this.calculationLableSize(curUserTab.szMessageContent);
                var content = new cc.LabelTTF("1",res.round_body_ttf, 20);
                content.setDimensions(cc.size(this.MAX_LINE_WIDTH,0));
                content.setHorizontalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
                content.setName("content");
                defaultCell.addChild(content);
                content.setString(curUserTab.szMessageContent);
                defaultCell.setContentSize(cc.size(labelSize.width+20,labelSize.height+20));
                content.setPosition(cc.p(defaultCell.getContentSize().width/2,defaultCell.getContentSize().height/2))
            }

            var tmpHeight = defaultCell.getContentSize().height>56?defaultCell.getContentSize().height:56;
            if (curUserTab.dwSenderID == GlobalUserItem.dwUserID) {
                head.setPosition(cc.p(860,0));
                defaultCell.setAnchorPoint(cc.p(1,0));
                defaultCell.setPosition(cc.p(825,0));
            } else {
                head.setPosition(cc.p(30,0));
                defaultCell.setAnchorPoint(cc.p(1,0));
                defaultCell.setPosition(cc.p(70,0));
            }
            head.setPositionY(tmpHeight*0.5);
        }
        return cell;
    },

    numberOfCellIsInTableView : function (table) {
        if( table == this.m_FriendTableView ) {
            return FriendMgr.getInstance().getFriendList().length;
        } else if (table == this.m_ChatTableView) {
            return this.chatTab.length;
        }
    },

    removeTableView     :       function () {
        this.m_FriendTableView.removeFromParent();
        this.m_ChatTableView.removeFromParent();
    },

    refreshChatList     :       function () {
        this.m_nFriendCount = FriendMgr.getInstance().getFriendList().length;
        var friendlist = FriendMgr.getInstance().getFriendList();
        cc.log("refreshFriend friendcount :  " + this.m_nFriendCount);
        cc.log("refreshFriend friendlist: " + friendlist);
        if(FriendMgr.getInstance().getFriendByID(this.m_dwCurrentFriendId)){
            if(this.m_nFriendCount>0){
                this.m_dwCurrentFriendId = friendList[1].dwUserID;
            } else {
                this.m_dwCurrentFriendId = null;
            }
        }

        if(this.m_FriendTableView != null) {
            this.m_FriendTableView.reloadData();
        }

        if(this.m_ChatTableView!=null) {
            this.m_ChatTableView.reloadData();
            this.moveToLastRow();
            this.enableChatListTouch();
        }
    },

    calculationLableSize    :       function (content) {
        var label = new cc.LabelTTF("1","Arial",20);
        label.setDimensions(cc.size(this.MAX_LINE_WIDTH,0));
        label.setHorizontalAlignment(cc.TEXT_ALIGNMENT_LEFT);
        label.setVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
        label.setString(content);

        var contentSize = label.getContentSize();
        return contentSize;
    },

    //新消息通知 New message notification
    messageNotify   :   function(msgTab){
        var curFriend = FriendMgr.getInstance().getFriendByID(this.m_dwCurrentFriendId);
        if(curFriend == null ) {
            return;
        }
        this.chatTab = FriendMgr.getInstance().getUserRecordMsg(curFriend.dwUserID);

        this.chatTab = this.sortTable(this.chatTab);

        this.m_ChatTableView.reloadData();
        this.moveToLastRow();
        this.enableChatListTouch();
    },

    //聊天table滚动到最后一行 chat table scrolls to the last line
    moveToLastRow   :   function () {
        this.m_ChatTableView.setContentOffset(cc.p(0,0),false);
    },

    //倒叙输出Table  Flashback output table
    sortTable   :   function(table) {
      var sortTab = {};
      if(table.length>0) {
          for(var i = 0;i<table.length;i++){
              sortTab[i] = table[table.length-i-1];
          }
          return sortTab;
      }
      return {};
    },

    //获取通知红点显示node   Get notified red dot shows node
    getNotifyNode   :   function (dwUserID) {
        var cell = this.getFriendCell(dwUserID);
        if(cell != null ) {
            return cell.getChildByName("defaultCell");
        }
        return null;
    },

    //好友 cell    friends cell
    getFriendCell   :   function (dwUserID) {
        var container = this.m_FriendTableView.getContainer();
        if(container != null ) {
            return container.getChildByTag(dwUserID);
        }
    },

    //刷新好友cell   refresh friend cell
    refreshFriendState  :   function (userInfo) {
        var cell = this.getFriendCell(userInfo.dwUserID);
        if(cell != null){
            var defaultCell = cell.getChildByName("defaultCell");
            if(defaultCell == null) {
                return;
            }
            var userStatus = defaultCell.getChildByName("userStatus");
            if(userStatus == null ) {
                return;
            }

            if(userInfo.cbMainStatus == chat_cmd.Friend_US_OFFLINE) {
                var sp = new cc.Sprite(res.biaoti5_png);
                userStatus.setSpriteFrame(sp.getSpriteFrame());
            } else {
                var sp = new cc.Sprite(res.biaoti4_png);
                userStatus.setSpriteFrame(sp.getSpriteFrame());
            }
        }
    },

    //
    enableChatListTouch :   function () {
        var container = this.m_ChatTableView.getContainer();
        if(container != null ) {
            this.m_ChatTableView.setTouchEnabled((container.getContentSize().height >= this.m_ChatTableView.getViewSize().height));

        }
    }


}
