/*Author :  JIN */
// var chat_cmd = appdf.req(appdf.HEADER_SRC+"CMD_ChatServer");
// var ClipText = appdf.req(appdf.EXTERNAL_SRC + "ClipText");
// var MultiPlatform = appdf.req(appdf.EXTERNAL_SRC + "MultiPlatform");
// var ExternalFun = appdf.req(appdf.EXTERNAL_SRC + "ExternalFun");

var BT_FRIENDLIST       =   101;
var BT_ADDFRIEND        =   102;
var BT_MESSAGENOTIRY    =   103;
var NEAR_FRIEND_LIST    =   "__near_friend_list_layer__";

var FriendLayer = cc.Layer.extend({

   ctor :   function (scene) {
       this._super();
       cc.log("FriendLayer oncreated.");
       /*TODO: used cc.LayerColor background opacity*/
       var friendLayer = new cc.LayerColor(cc.color(0, 0, 0, 125));
       this.addChild(friendLayer);
       this.onCreate(scene);
   },

    onEnterTransitionFinish     :   function () {
        if(FriendMgr.getInstance().isConnected() == false) {
            FriendMgr.getInstance().reSetAndLogin();
        }
        this.initFriendListener();
        //激活通知  activate notification
        NotifyMgr.getInstance().resumeNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_USER_CHAT_NOTIFY, "friend_chat");
        NotifyMgr.getInstance().resumeNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_APPLYFOR_NOTIFY, "friend_apply");
        NotifyMgr.getInstance().resumeNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_RESPOND_NOTIFY, "friend_response");
        NotifyMgr.getInstance().resumeNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_INVITE_GAME_NOTIFY, "friend_invite");
        NotifyMgr.getInstance().resumeNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_USER_SHARE_NOTIFY, "friend_share");
        NotifyMgr.getInstance().resumeNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_INVITE_PERSONAL_NOTIFY, "pri_friend_invite");

        //处理通知 processing notification
        NotifyMgr.getInstance().excuteSleepNotfiy();

        //判断是否有未读消息通知(针对消息通知界面)Determine whether there is unread message notification (for message notification interface)
        var tmp = FriendMgr.getInstance().getUnReadNotify();
        if(tmp.length>0) {
            NotifyMgr.getInstance().excute(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_APPLYFOR_NOTIFY, null);
            FriendMgr.getInstance().registerNotifyList();
        }
        return this;
    },

    //退出场景而且开始过渡动画时候触发 Exit the scene and trigger transition animation
    onExitTransitionStart   :   function () {
        //暂停通知  Suspension notice
        NotifyMgr.getInstance().pauseNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_USER_CHAT_NOTIFY, "friend_chat");
        NotifyMgr.getInstance().pauseNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_APPLYFOR_NOTIFY, "friend_apply");
        NotifyMgr.getInstance().pauseNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_RESPOND_NOTIFY, "friend_response");
        NotifyMgr.getInstance().pauseNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_INVITE_GAME_NOTIFY, "friend_invite");
        NotifyMgr.getInstance().pauseNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_USER_SHARE_NOTIFY, "friend_share");
        NotifyMgr.getInstance().pauseNotify(chat_cmd.MDM_GC_USER, chat_cmd.SUB_GC_INVITE_PERSONAL_NOTIFYpri_friend_invite, "pri_friend_invite");
        FriendMgr.getInstance().setViewLayer(null);
        //FriendMgr.getInstance().setCallBackDelegate(null);
        this.removeFriendListener();
        return this;
    },

    onNotify    :   function (msg) {
        var bHandled = false;
        if(msg.main === chat_cmd.MDM_GC_USER) {
            if( msg.sub === chat_cmd.SUB_GC_USER_CHAT_NOTIFY
                    || msg.sub === chat_cmd.SUB_GC_INVITE_GAME_NOTIFY
                    || msg.sub === chat_cmd.SUB_GC_USER_SHARE_NOTIFY
                    || msg.sub === chat_cmd.SUB_GC_INVITE_PERSONAL_NOTIFY )
            {
                //确保不在好友列表界面  Make sure you are not in the buddy list interface
                if(this.m_nSelect != BT_FRIENDLIST ) {
                    NotifyMgr.getInstance().showNotify(this.m_btnFriendList, msg, cc.p(210,90));
                }

                //单独解析 dwSenderID 为发送者id   Resolve dwSenderID separately as sender id
                if(typeof(msg.param) === "table") {
                    var sendUser = msg.param.dwSenderID;
                    if(sendUser == null) {
                        return false;
                    }
                    if( this.m_bIsChatLayer === false ) {
                        //不在聊天界面
                        bHandled = false;
                    } else {
                        //是否当前聊天对象
                        bHandled = (sendUser === this.m_dwCurrentFriendId);
                    }
                    if(bHandled == false) {
                        var friendcell = this.getFriendlistCell(sendUser);
                        if(friendcell != null ) {
                            var chatbtn = friendcell.getChatBtn();
                            NotifyMgr.getInstance().showNotify(chatbtn, msg);
                        }
                    }

                    //聊天界面  chat interface
                    if(this.m_bIsChatLayer == true ) {
                        var node = this.getChatFriendCellNotifyNode(sendUser);
                        if(node!=null) {
                            NotifyMgr.getInstance().showNotify(node, msg);
                        }
                    }
                }
            } else if (msg.sub == chat_cmd.SUB_GC_APPLYFOR_NOTIFY || msg.sub == chat_cmd.SUB_GC_RESPOND_NOTIFY ) {
                //Non-notification interface
                if(this.m_nSelect != BT_MESSAGENOTIRY ) {
                    NotifyMgr.getInstance().showNotify(this.m_btnMsgNotice, msg, cc.p(210, 90));
                }
                bHandled = true;
            }
        }
        return bHandled;
    },

    onCreate    :   function (scene) {
        ExternalFun.registerNodeEvent(this);
        FriendMgr.getInstance().setViewLayer(this);

        //是否在聊天界面  whether the chat interface
        this.m_bIsChatLayer = false;
        //当前聊天对象 the current chat object
        this.m_dwCurrentUserId = -1;
        //好友列表按钮 Friend list button
        this.m_btnFriendList = null;
        this.m_btnMsgNotice = null;

        var that = this;
        this._scene = scene;
        this.m_TableData = {};
        this.m_nFriendCount = 0;
        this.m_chatListManager = null;
        this.m_listener = null;
        this.m_downListener = null;
        this.m_nSelect = BT_FRIENDLIST;

        this.Layer = ccs.load(res.FriendLayer_json).node;
        this.addChild(this.Layer);
        this.initWithButton();
        this.initFriendList();
    },

    onEnterTransitionDidFinish : function () {
        this.onEnterTransitionFinish();
    },

    onExitTransitionDidStart : function () {
        this.onExitTransitionStart();
    },

    registerScriptHandler : function (onNodeEvent) {
       // this.registerScriptHandler(onNodeEvent);
    },

    // 初始化按钮 initialize button
    initWithButton  :   function () {
        this._btcallback = function (ref, tType) {
            if(tType = ccui.Widget.TOUCH_ENDED) {
                this.onButtonClickedEvent(ref.getTag(),ref);
            }
        };

        var FriendBg = this.Layer.getChildByName("FriendBg");
        this.m_friendBg = FriendBg;

        //无好友提示 no friend prompt
        this.m_nofriend = appdf.getNodeByName(this.m_friendBg, "tips_nofriend");
        var FriendAddLayer = this.Layer.getChildByName("FriendAddLayer");
        this.m_friendAddLayer = FriendAddLayer;
        var FriendChatlayer = this.Layer.getChildByName("FriendChatLayer");
        this.m_nFriendChatLayer = FriendChatlayer;
        var FriendNotifyLayer = appdf.getNodeByName(this.Layer, "FriendNotifyLayer");
        this.m_friendNotifyLayer = FriendNotifyLayer;

        var ListUserSearch = appdf.getNodeByName(FriendAddLayer, "ListUserSearch");
        ListUserSearch.setScrollBarEnabled(false);

        FriendAddLayer.setTouchEnabled(true);
        FriendChatlayer.setTouchEnabled(true);
        FriendNotifyLayer.setTouchEnabled(true);
        FriendBg.setTouchEnabled(true);
        this.m_friendNotifyLayer.setVisible(false);
        var that = this;
        //返回按钮 Back button
        var BcakBtn = appdf.getNodeByName(FriendBg,"BackBtn");
        BcakBtn.addTouchEventListener(function (ref, tType) {

            if(tType == ccui.Widget.TOUCH_ENDED) {
                if(FriendChatlayer.isVisible()){
                    FriendChatlayer.setVisible(false);

                    //重置当前保存信息  Reset the current saved information
                    this.setIsChatLayer(false);
                    this.setCurrentChatUser(-1);

                    if(this.m_chatListManager!=null){
                        this.m_chatListManager.removeTableView();
                        this.m_chatListManager = null;
                    }
                }  else {
                    that._scene.onKeyBack();
                }
            }
        });

        //好友列表  friend list
        var BtnList = appdf.getNodeByName(FriendBg, "BtnList");
        BtnList.addTouchEventListener(function (ref, tType) {
            if(tType == ccui.Widget.TOUCH_ENDED) {
                NotifyMgr.getInstance().hideNotify(that.m_btnFriendList);
                FriendAddLayer.setVisible(false);
                ref.setEnabled(false);
                FriendNotifyLayer.setVisible(false);
                var BtnAddFriend = appdf.getNodeByName(FriendBg,"BtnAddFriend");
                BtnAddFriend.setEnabled(true);
                var BtnMsgNotice = appdf.getNodeByName(FriendBg, "BtnMsgNotice");
                BtnMsgNotice.setEnabled(true);

                that.m_nofriend.setTouchEnabled(false);
                that.m_nofriend.setVisible(false);
                if(that.m_friendUserList!=null){
                    that.m_friendUserList.setTouchEnabled(true);
                    that.m_friendUserList.setVisible(true);
                }
                that.refreshFriendList();
            }
        });
        this.m_btnFriendList = BtnList;

        //添加好友 add friend
        var BtnAddFriend = appdf.getNodeByName(FriendBg, "BtnAddFriend");
        this.m_btnAddFriend = BtnAddFriend;

        BtnAddFriend.addTouchEventListener(function (ref, tType) {
           if(tType == ccui.Widget.TOUCH_ENDED){
               that.m_nofriend.setTouchEnabled(false);
               that.m_nofriend.setVisible(false);
               ListUserSearch.removeAllItems();
               that.onClickAddFriend();
           }
        });

        //消息通知 notitication
        var BtnMsgNotice = appdf.getNodeByName(FriendBg, "BtnMsgNotice");
        BtnMsgNotice.addTouchEventListener(function (ref, tType) {
            if(tType == ccui.Widget.TOUCH_ENDED) {
                that.m_nSelect = BT_MESSAGENOTIRY;
                if(that.m_friendUserList != null){
                    that.m_friendUserList.setTouchEnabled(false);
                    that.m_friendUserList.setVisible(false);
                }

                NotifyMgr.getInstance().hideNotify(that.m_btnMsgNotice, true);

                that.m_nofriend.setTouchEnabled(false);
                that.m_nofriend.setVisible(false);

                FriendAddLayer.setVisible(false);
                ref.setEnabled(false);
                FriendNotifyLayer.setVisible(true);
                var BtnList = appdf.getNodeByName(FriendBg,"BtnList");
                BtnList.setEnabled(true);
                var BtnAddFriend = appdf.getNodeByName(FriendBg, "BtnAddFriend");
                BtnAddFriend.setEnabled(true);
                that.updateNotifyList();
            }
        });
        this.m_btnMsgNotice = BtnMsgNotice;

        //查找按钮  Find button
        var searchBtn = appdf.getNodeByName(FriendAddLayer, "BtnSearch");
        searchBtn.addTouchEventListener(function (ref, tType) {
           if(tType == ccui.Widget.TOUCH_ENDED) {
               if(that.m_editId == null) {
                   showToast(that, "输入不能为空", 2, cc.color(255, 255, 255, 255));
               }

               // var content = Number(that.m_editId.string);
               var content = that.m_editId.string;
               if(content==null) {
                   showToast(that, "请输入合法的ID", 2, cc.color(255, 255, 255, 255));
                   return;
               }

               if((content).length<1){
                   showToast(that,"输入不能为空",2, cc.color(255, 255, 255, 255));
                   return;
               } else {
                   content = Number(content);
                   FriendMgr.getInstance().sendSearchFriend(content);
               }
           }
        });
        BtnList.setEnabled(false);
        FriendAddLayer.setVisible(false);
        this.refreshFriendList();

        //附近的人 Peaple nearby
        var btn = FriendAddLayer.getChildByName("BtnNearBy");
        btn.addTouchEventListener(function (ref, tType) {
           if(tType == ccui.Widget.TOUCH_ENDED) {
               FriendMgr.getInstance().queryNearUser();
               var near = new NearFriendListLayer();
               that.addChild(near);
               near.setName(NEAR_FRIEND_LIST);
           }
        });

        //通讯录 address book
        btn = FriendAddLayer.getChildByName("BtnTelList");
        btn.addTouchEventListener(function (ref, tType) {
           if(tType == ccui.Widget.TOUCH_ENDED) {
               var sharecall = function (isok) {
                   if(typeof(isok)=="string" && isok =="true") {
                       showToast(that, "分享完成", 2);
                   }
               };
               var url = GlobalUserItem.szSpreaderURL || yl.HTTP_URL;
               var msg = "亲爱的好友，我最近玩了一款超好玩的游戏，玩法超级多，内容超级精彩，快来加入我，和我一起精彩游戏吧！下载地址：" + url;
               MultiPlatform.getInstance().shareToTarget(yl.ThirdParty.SMS, sharecall, "好友分享", msg);

           }
        });

        //微信 WeChat
        btn = FriendAddLayer.getChildByName("BtnWechat");
        btn.addTouchEventListener(function (ref, tType) {
           if(tType == ccui.Widget.TOUCH_ENDED) {
               var sharecall = function (isok) {
                   if(typeof(isok)=="string" && isok == "true") {
                       showToast(that,"分享完成", 2);
                   }
               };
               var url = GlobalUserItem.szWXSpreaderURL || yl.HTTP_URL;
               MultiPlatform.getInstance().shareToTarget(yl.ThirdParty.WECHAT, sharecall, "有人@你一起玩游戏！\", \"你的好友正在玩游戏！玩法超多超精彩！快来打败他！", url, "");
           }
        });
    },

    //初始化好友列表  initialize buddy list
    initFriendList  :   function () {
        var userListBg = appdf.getNodeByName(this.Layer, "UserListBg");
        var userListBgSize = userListBg.getContentSize();
        userListBg.removeAllChildren();
        this.m_TableData = FriendMgr.getInstance().getFriendList();
        this.m_nFriendCount = this.m_TableData.length;

        var tableView = new cc.TableView(this, cc.size(userListBgSize.width-40, userListBgSize.height));
        tableView.setName("FriendUserList");
        tableView.setColor(cc.color(108,97,91));
        tableView.setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL);
        tableView.setPosition(cc.p(20,0));
        this.m_friendUserList = tableView;
        tableView.setDelegate(this);
        userListBg.addChild(tableView);
        // tableView.registerScriptHandler(handler(this,this.tableCellTouched),cc.TABLECELL_TOUCHED);
        // tableView.registerScriptHandler(handler(this,this.cellSizeForTable),cc.TABLECELL_SIZE_FOR_INDEX);
        // tableView.registerScriptHandler(handler(this,this.tableCellAtIndex),cc.TABLECELL_SIZE_AT_INDEX);
        // tableView.registerScriptHandler(handler(this,this.numberOfCellsInTableView),cc.NUMBER_OF_CELLS_IN_TABLEVIEW);

        this.refreshFriendList();
    },

    initFriendListener  :   function () {
        // this.m_listener = cc.EventListenerCustom(yl.RY_FRIEND_NOTIFY, handler(this,this.onFriendInfo));
        // cc.director.getEventDispatcher().addEventListenerWithSceneGraphPriority(this.m_listener, this);
        var eventCustomListener = function(event) {
            if(event.filename!= null && event.code == 0) {
                if(typeof(event.filename)=="string" && cc.FileUtils.getInstance().isFileExist(event.filename)) {
                    cc.log("刷新图片");
                    if(this.m_chatListManager != null && this.m_chatListManager.messageNotify != null) {
                        this.m_chatListManager.messageNotify();
                    }
                }
            }
        };
        // this.m_downListener = cc.EventListenerCustom(yl.RY_IMAGE_DOWNLOAD_NOTIFY, eventCustomListener);
        // this.getEventDispatcher().addEventListenerWithFixedPriority(this.m_downListener, 1);
    },

    onClickAddFriend  :  function () {
        this.m_nSelect = BT_ADDFRIEND;
        if (this.m_friendUserList != null) {
            this.m_friendUserList.setTouchEnabled(false);
            this.m_friendUserList.setVisible(false);
        }
        this.m_friendAddLayer.setVisible(true);
        this.m_btnAddFriend.setEnabled(false);
        this.m_friendNotifyLayer.setVisible(false);

        this.m_nofriend.setTouchEnabled(false);
        this.m_nofriend.setVisible(false);

        var BtnList = appdf.getNodeByName(this.m_friendBg,"BtnList");
        BtnList.setEnabled(true);
        var  BtnMsgNotice = appdf.getNodeByName(this.m_friendBg,"BtnMsgNotice");
        BtnMsgNotice.setEnabled(true);

        var InputTextBg = appdf.getNodeByName(this.m_friendAddLayer,"InputTextBg");
        /*if (!appdf.getNodeByName(this.m_friendAddLayer,"EditBoxSearchIput")) { // TODO:error can't do if condition*/
            var EditID = new cc.EditBox(InputTextBg.getContentSize(),new cc.Scale9Sprite(res.dikuang20_png));
            EditID.setFontSize(30);
            EditID.setPlaceholderFontSize(30);
            // EditID.setPlaceHolder("请填写对方的游戏ID号:");
            EditID.setFontColor(new cc.Color(108, 97, 91));
            EditID.setFontName(res.round_body_ttf);
            EditID.setPlaceholderFontName(res.round_body_ttf);
            EditID.setPlaceholderFontColor(new cc.Color(108, 97, 91));
            EditID.setMaxLength(32);
            EditID.setReturnType(cc.KEYBOARD_RETURNTYPE_DONE);
            EditID.setInputMode(cc.EDITBOX_INPUT_MODE_NUMERIC);
            EditID.setPosition(InputTextBg.getPosition());
            EditID.setName("EditBoxSearchIput");
            this.m_friendAddLayer.addChild(EditID);
            this.m_editId = EditID;
        /*}*/
        InputTextBg.setVisible(false);
    },

    onFriendInfo : function(event) {
      var msgWhat = event.obj;
      if(msgWhat != null && yl.RY_MSG_FRIENDDEL == msgWhat ) {
          this.refreshFriendList();
      }
    },

    removeFriendListener : function(){
       if(this.m_listener != null ) {
           cc.director().getEventDispatcher().removeEventListener(this.m_listener);
       }

       if(this.m_downListener != null) {
           this.getEventDispatcher().removeEventListener(this.m_downListener);
           this.m_downListener = null;
       }
    },

    getFriendListCell : function (userid) {
        if(userid==null || userid == (-1)) {
            return null;
        }

        var container = this.m_friendUserList.getContainer();
        if(container != null) {
            var cell = container.getChildByTag(userid);
            if(cell != null ) {
                return cell.getChildByName("friendCell")
            }
        }
        return null;
    },

    //获取好友界面好友 get buddy interfacefriend
    getChatFriendCell : function(dwUserID){
        if(null == dwUserID && -1 == dwUserID){
            return null;
        }
        if(null != this.m_chatListManager && this.m_chatListManager.getFriendCell() != null){
            return this.m_chatListManager.getFriendCell(dwUserID);
        }
        return null;
    },

    //获取好友界面好友红点显示 get friend interface freind red dot display
    getChatFriendCellNotifyNode : function(dwUserID, ingoreSelf){
        //当前聊天对象 get current caht object
        if(dwUserID == this.m_dwCurrentUserId) {
            return null;
        }
        if(this.m_cahtListManager != null && this.m_chatListManager.getNotifyNode() != null ) {
            return this.m_chatListManager.getNotifyNode(dwUserID);
        }
        return null;
    },

    refreshFriendList   :   function(){
        this.m_TableData = FriendMgr.getInstance().getFriendList();
        this.m_nFriendCount = this.m_TableData.length;
        var userListBg = appdf.getNodeByName(this.Layer, "UserListBg");
        tableView = appdf.getNodeByName(userListBg,"FriendUserList");
        if(tableView == undefined ) {
            var conSize = userListBg.getContentSize();
            var tableView = new cc.TableView(this, conSize);
            tableView.setName("FriendUserList");
            tableView.setColor(cc.color(108,97,91));
            tableView.setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL);
            tableView.setPosition(cc.p(20,0));
            this.m_friendUserList = tableView;
            userListBg.addChild(this.m_friendUserList);
            tableView.reloadData();
        } else {
            tableView.reloadData();
        }
        // tableView.reloadData();
        var child = userListBg.getChildByName("tips_name");
        userListBg.removeChild(child, true);
        var that = this;
        if(this.m_nFriendCount == 0) {
            if(this.m_nofriend != null) {
                this.m_friendNotifyLayer.setVisible(false);
                this.m_nofriend.setTouchEnabled(true);
                this.m_nofriend.setVisible(true);
            }
            var button = appdf.getNodeByName(this.m_nofriend, "BtnAddFriend");
            button.addTouchEventListener(function (ref, tType) {
                if(tType == ccui.Widget.TOUCH_ENDED) {
                    that.onClickAddFriend();
                }
            });
        }

        if(this.m_chatListManager) {
            this.m_chatListManager.refreshChatList();
        }
    },

    tableCellSizeForIndex : function(view, idx) {
        return {width : 962, height: 90};
    },

    //刷新指定好友游戏状态 refresh specified friend game status
    refreshFriendState  :   function (userInfo, isGameState ) {
        isGameState = isGameState || false;
        //刷新好友列表 refresh friend list
        var friendCell = this.getFriendListCell(userInfo.dwUserID);
        if(friendCell!=null && friendCell.refreshFriendState != null ) {
            friendCell.refreshFriendState(userInfo);
        }
        if(isGameState == false ){
            if(this.m_chatListManager!=null && this.m_chatListManager.refreshFriendState != null) {
                return this.m_chatListManager.refreshFriendState(userInfo);
            }
        }
    },

    messageNotify   :   function(notify){
        if(this.m_chatListManager!=null && this.m_chatListManager.messageNotify!=null) {
            this.m_chatListManager.messageNotify(notify);
        }
    },

    updateNotifyList    :   function(){
        var notifyTab = FriendMgr.getInstance().getFriendNotify();
        cc.log(notifyTab+"====== 当前数据 =====");
        var FriendNotifyLayer = appdf.getNodeByName(this.Layer,"FriendNotifyLayer");
        var NotifyList = appdf.getNodeByName(FriendNotifyLayer,"NotifyList");
        var NotifyListSize = NotifyList.getContentSize();
        NotifyList.removeAllItems();
        if(notifyTab.length == 0) {
            NotifyList.setVisible(false);
            var tipLab = new cc.LabelTTF("暂无消息通知",res.round_body_ttf, 30);
            tipLab.setPosition(cc.p(NotifyListSize.width*0.5, NotifyListSize.height*0.5));
            FriendNotifyLayer.addChild(tipLab);
            var removeTipLab = function () {
                tipLab.removeFromParent();
            }
            tipLab.runAction(new cc.Sequence( new cc.DelayTime(2),new cc.CallFunc(removeTipLab())));
        } else {
            NotifyList.setVisible(true);
            for (var i=notifyTab.length-1;i>=0;i--){
                var curTab = notifyTab[i];

                var notifyItem = new ccui.ImageView(res.dikuang5_png);
                notifyItem.setScale9Enabled(true);
                notifyItem.setAnchorPoint(cc.p(0,0));
                notifyItem.setPosition(0,0);
                notifyItem.setContentSize(NotifyListSize.width, 80);
                notifyItem.setTag(i);

                //1:申请好友通知  2:回应通知  3：邀请通知  1: Request Good friend notification 2: Meeting notification 3:Notice of acceptance
                if(curTab.notifyType == 1) {
                    var tip = new cc.LabelTTF(curTab.notify.szNickName+"请求成为您的好友",res.round_body_ttf,30,cc.TEXT_ALIGNMENT_LEFT);
                    tip.setColor(cc.color(108,97,91,255));
                    tip.setAnchorPoint(cc.p(0,0.5));
                    tip.setPosition(cc.p(20,notifyItem.getContentSize().height/2));
                    notifyItem.addChild(tip);

                    if(!curTab.bRead){
                        var sureBtn = new ccui.Button(res.anniu21_png, res.anniu22_png);
                        sureBtn.setAnchorPoint(cc.p(0,0.5));
                        sureBtn.setPosition(cc.p(500,notifyItem.getContentSize().height/2));
                        notifyItem.addChild(sureBtn);
                        sureBtn.addTouchEventListener(function (ref, tType) {
                            if(tType == ccui.Widget.TOUCH_ENDED) {
                                var thisTab = notifyTab[ref.getParent().getTag()];
                                var sendTab = {};
                                sendTab.dwUserID = GlobalUserItem.dwUserID;
                                sendTab.dwRequestID = thisTab.notify.dwRequestID;
                                sendTab.bAccepted = true;
                                NotifyMgr.getInstance().hideNotify(ref.getParent(),true);
                                FriendMgr.getInstance().sendRespondFriend(sendTab,thisTab.notifyId);
                            }
                        });
                        var cancleBtn = new ccui.Button(res.anniu23_png);
                        cancleBtn.setAnchorPoint(cc.p(0,0.5));
                        cancleBtn.setPosition((cc.p(630,notifyItem.getContentSize().height/2)));
                        notifyItem.addChild(cancleBtn);
                        cancleBtn.addTouchEventListener(function (ref, tType) {
                           if(tType == ccui.Widget.TOUCH_ENDED){
                               var thisTab = notifyTab[ref.getParent().getTag()];
                               var sendTab = {};
                               sendTab.dwUserID = GlobalUserItem.dwUserID;
                               sendTab.dwRequestID = thisTab.notify.dwRequestID;
                               sendTab.bAccepted = false;
                               NotifyMgr.getInstance().hideNotify(ref.getParent(),true);
                               FriendMgr.getInstance().sendRespondFriend(sendTab, thisTab.notifyId);
                           }
                        });
                    }
                } else if (curTab.notifyType == 2){
                    var tip = new cc.LabelTTF(curTab.notify, res.round_body_ttf,30, cc.size(600,0),cc.TEXT_ALIGNMENT_LEFT);
                    tip.setColor(cc.color(108,97,91,255));
                    tip.setAnchorPoint(cc.p(0,0.5));
                    tip.setPosition(cc.p(20,notifyItem.getContentSize().height/2));
                    notifyItem.addChild(tip);
                    if(!curTab.bRead){
                        notifyItem.setTouchEnabled(true);
                        var self = this;
                        notifyItem.addTouchEventListener(function (ref, tType) {
                           if(tType == ccui.Widget.TOUCH_ENDED){
                               var thisTab = notifyTab[ref.getTag()];
                               FriendMgr.getInstance().markFriendNotifyRead(thisTab.notifyId);
                               // ref.removeChildByName("notify_dot_cell");
                               var notify_dot = ref.getChildByName("notify_dot_cell");
                               notify_dot.removeFromParent(true);
                               self.updateNotifyList();
                           }
                        });
                    }
                } else if(curTab.notifyType == 3) {
                    var str = "" + curTab.notify.dwInviteUserID + "在" + curTab.notify.wServerID + "房间"+ curTab.notify.wTableID + "号桌" + curTab.notify.szInviteMsg;
                    var tip = new cc.LabelTTF(str, res.round_body_ttf, 30, cc.size(600,0),cc.TEXT_ALIGNMENT_LEFT);
                    tip.setTextColor(cc.Color(108,97,91, 255));
                    tip.setAnchorPoint(cc.p(0,0.5));
                    tip.setPosition(cc.p(20,notifyItem.getContentSize().height/2));
                    notifyItem.addChild(tip);

                    if(!cutTab.bRead){
                        var sureBtn = new ccui.Button(res.anniu21_png, res.anniu22_png);
                        sureBtn.setAnchorPoint(cc.p(0,0.5));
                        sureBtn.setTag(i+400);
                        sureBtn.setPosition(cc.p(650,notifyItem.getContentSize().height/2));
                        notifyItem.addChild(sureBtn);
                        sureBtn.addTouchEventListener(function (ref, tType) {
                            if(tType == ccui.Widget.TOUCH_ENDED){
                                var notifyList = FriendMgr.getInstance().getFriendNotify();
                                var index = sureBtn.getTag() - 400;
                                cc.log("同意进入某房间==========");
                            }
                        });

                        var cancleBtn = new ccui.Button(res.anniu23_png);
                        cancleBtn.setAnchorPoint(cc.p(0,0.5));
                        cancleBtn.setTag(i+800);
                        cancleBtn.setPosition(cc.p(800,notifyItem.getContentSize().height/2));
                        notifyItem.addChild(cancleBtn);
                        cancleBtn.addTouchEventListener(function (ref, tType) {
                           if(tType == ccui.Widget.TOUCH_ENDED){
                               cc.log("不同意进入某房间==========");
                           }
                        });
                    }
                }

                // TODO: insert image in resouce folder
                var frame = cc.spriteFrameCache.getSpriteFrame("sp_dot.png");
                if(frame != null && !curTab.bRead) {
                    dot = new cc.Sprite(frame);
                    dot.setName("notify_dot_cell");
                    notifyItem.addChild(dot);

                    var nodesize = notifyItem.getContentSize();
                    var dotsize = dot.getContentSize();
                    var pos = cc.p(nodesize.width - dotsize.width*0.5, nodesize.height-dotsize.height*0.5);
                    dot.setPosition(pos);
                }

                var onTouchBegan = function(touch, event){
                    var locationInNode = notifyItem.convertTonodeSpace(touch.getLocation());
                    var size = notifyItem.getContentSize();
                    var rect = cc.rect(0,0,size.width, size.height);
                    if(cc.rectContainsPoint(rect,locationInNode)) {
                        return true;
                    }
                    return false;
                };

                var onTouchMoved = function (touch, event) {
                  var delta = touch.getDelta();
                  var deltax = delta.x;
                  var deltay = delta.y;
                };

                var onTouchEnded = function (touch, event) {
                    notifyItem.bMoveRight = null;
                }

                /*var m_Listener = cc.EventListenerTouchOneByOne();

                m_Listener.registerScriptHandler(onTouchBegan, cc.TOUCH_BEGAN);
                m_Listener.registerScriptHandler(onTouchMoved, cc.TOUCH_MOVED);
                m_Listener.registerScriptHandler(onTouchEnded, cc.TOUCH_ENDED);

                var disptcher = new cc.director().getEventDispatcher();
                dispacther.addEventListenerWithSceneGraphPriority(m_Listener, notifyItem);

                var onNodeEvent = function(event){
                    if(event == "exit") {
                        cc.log("node itme remove");
                        var eventDispatcher = notifyItem.getEventDispatcher();
                        eventDispatcher.removeEventListener(m_Listener);
                    }
                };
                notifyItem.registerScriptHandler(onNodeEvent);*/
                //TODO: where is pushbackcustom item
                    NotifyList.pushBackCustomItem(notifyItem);
            }
        }
    },

    onDeleteFriend  :   function(){
        var eventListener = cc.EventCustom(yl.RY_FRIEND_NOTIFY);
        eventListener.obj = yl.RY_MSG_FRIENDDEL;
        cc.director().getEventDispatcher().dispatchEvent(eventListener);
    },

    searchResult    :   function(userTab) {
        var FriendAddLayer = appdf.getNodeByName(this.Layer, "FriendAddLayer");
        var ListUserSearch = appdf.getNodeByName(FriendAddLayer, "ListUserSearch");
        var ListUserSearchSize = ListUserSearch.getContentSize();
        cc.log("add Friend userTab = " + userTab);
        if (userTab == null) {
            showToast(self, "找不到该用户!!!", 2);
            //        local tipLab = cc.Label:createWithTTF("找不到该用户!!!","fonts/round_body.ttf",30);
            //        tipLab:setPosition(cc.p(ListUserSearchSize.width/2,ListUserSearchSize.height/2));
            //        ListUserSearch:addChild(tipLab);
            //        tipLab:setTextColor(cc.color(41, 82, 146, 255));
            //        local function removeTipLab()
            //            tipLab:removeFromParent();
            //        end
            //        tipLab:runAction(cc.Sequence:create(cc.DelayTime:create(2),cc.CallFunc:create(removeTipLab)));
        } else {
            if (null != this.m_editId) {
                this.m_editId.setString("");
            }
            ListUserSearch.removeAllItems();

            for (var i in userTab) {
                var userInfoTab = userTab[i];
                var userItem = new ccui.ImageView(res.dikuang5_png)//ccui.Layout:create();
                userItem.setScale9Enabled(true);
                userItem.setAnchorPoint(cc.p(0, 0.5));
                userItem.setPosition(cc.p(65, 30));
                userItem.setContentSize(ListUserSearchSize.width, 120);

                //头像
                var head = (new PopupInfoHead()).createNormal(userInfoTab, 60)[0];
                head.setPosition(cc.p(100, 60));
                userItem.addChild(head);
                head.setIsGamePop(false);
                //根据会员等级确定头像框
                // head:enableHeadFrame(true)
                //head:enableInfoPop(true, cc.p(420, 120), cc.p(0.5, 0.5))

                //头像框
                var head_frame = new cc.Sprite(res.friendhd_clip_png);
                head_frame.setAnchorPoint(cc.p(0, 0.5));
                head_frame.setPosition(cc.p(65, 60));
                userItem.addChild(head_frame);

                var yPos = 60;
                var nickName = new ClipText(cc.size(150, 30), userInfoTab.szNickName, res.round_body_ttf, 30);
                nickName.setName("nickName");
                nickName.setTextColor(cc.color(108, 97, 91, 255));
                nickName.setAnchorPoint(cc.p(0, 0.5));
                nickName.setPosition(cc.p(200, yPos + 20));
                userItem.addChild(nickName);

                var userid = new ClipText(cc.size(150, 30), "ID:" + userInfoTab.dwGameID, res.round_body_ttf, 30);
                userid.setName("userid");
                userid.setTextColor(cc.color(108, 97, 91, 255));
                userid.setAnchorPoint(cc.p(0, 0.5));
                userid.setPosition(cc.p(200, yPos - 20));
                userItem.addChild(userid);

               var start = new cc.Sprite.create(res.tutiao2_png);
               start.setAnchorPoint(cc.p(0,0.5));
               start.setPosition(cc.p(360,yPos));
               userItem.addChild(start);

               var lv = new cc.Sprite(res.biaoti2_png);
               lv.setAnchorPoint(cc.p(0,0.5));
               lv.setPosition(cc.p(415,yPos));
               userItem.addChild(lv);

               var lValue = new cc.LabelAtlas(""+(userInfoTab.cbMemberOrder),res.number_friend_1_png,16,20,0);
               lValue.setAnchorPoint(cc.p(0,0.5));
               lValue.setPosition(cc.p(470,yPos));
               lValue.setName("lValue");
               userItem.addChild(lValue);

                var friendTab = FriendMgr.getInstance().getFriendList();
                //查询好友列表是否存在该用户
                var userIsExit = false;
                for (var i in friendTab) {
                    var curUser = friendTab[i];
                    if (curUser.dwUserID == userInfoTab.dwUserID) {
                        //好友列表存在该好友
                        userIsExit = true;
                        break;
                    }
                }
                //判断是否是自己
                var isMe = userInfoTab.dwUserID == GlobalUserItem.dwUserID;

                var actionBtn = null;
                if (true == isMe) {
                } else if (userIsExit) {
                    actionBtn = new ccui.Button(res.anniu20_png);
                } else {
                    actionBtn = new ccui.Button(res.anniu14_png, res.anniu15_png, res.anniu15_1_png);
                    actionBtn.setTag(i + 1000);
                    actionBtn.addTouchEventListener(function (ref, tType) {
                        if (tType == ccui.Widget.TOUCH_ENDED) {
                            var addFriendTab = {};
                            addFriendTab.dwUserID = GlobalUserItem.dwUserID;
                            addFriendTab.dwFriendID = userInfoTab.dwUserID;
                            addFriendTab.cbGroupID = 0;
                            var sendResult = function (isAction) {
                                //ref:setVisible(not isAction)
                                ref.setEnabled(false);
                            };
                            //添加好友
                            cc.log("click add friend button" + addFriendTab.dwUserID + "sendresult : " + sendResult);
                            FriendMgr.getInstance().sendAddFriend(addFriendTab, sendResult);
                        }
                    });
                }
                if (null != actionBtn) {
                    actionBtn.setAnchorPoint(cc.p(1, 0.5));
                    actionBtn.setPosition(cc.p(750, 60));
                    userItem.addChild(actionBtn);
                }

                ListUserSearch.pushBackCustomItem(userItem);
            }
        }
    },

    tableCellTouched    :   function(table,cell) {
        cc.log("cell touched at index: " + cell.getIdx());
    },

    cellSizeForTable    :   function(table,idx) {
        return 962, 90;
    },

    tableCellAtIndex    :   function(table, idx) {
        var cell = table.dequeueCell();
        var userInfoTab = this.m_TableData[this.m_nFriendCount - 1 - idx];

        var friendCell = null;
        //获取未读通知列表
        var unread = NotifyMgr.getInstance().getUnreadNotifyList()
        if (null == cell) {
            cell = new cc.TableViewCell();
            friendCell = new FriendListCell(userInfoTab.dwUserID, this);
            friendCell.setName("friendCell");
            cell.addChild(friendCell);
        } else {
            friendCell = cell.getChildByName("friendCell");
            var head = appdf.getNodeByName(cell, "head");
            var nickName = appdf.getNodeByName(cell, "nickName");
            var lValue = appdf.getNodeByName(cell, "lValue");

            lValue.setString(""+(userInfoTab.wGrowLevel));
            nickName.setString(userInfoTab.szNickName);
            head.updateHead(userInfoTab);
        }

        if (null != friendCell) {
            friendCell.setCellStatusNormal();
            var chatbtn = friendCell.getChatBtn();
            if (null != chatbtn) {
                //隐藏红点
                NotifyMgr.getInstance().hideNotify(chatbtn, false);

                //通知红点
                for (k in unread) {
                    var v = unread[k];
                    if (typeof(v.param) == "table") {
                        var sendUser = v.param.dwSenderID;
                        if (null != sendUser && sendUser == userInfoTab.dwUserID) {
                            NotifyMgr.getInstance().showNotify(chatbtn, v);
                            break;
                        }
                    }
                }
                chatbtn.setTag(userInfoTab.dwUserID);
            }

            var userStatusLab = appdf.getNodeByName(cell, "userStatusLab");

            //好友状态
            friendCell.refreshFriendState(userInfoTab);
        }

        cell.setTag(userInfoTab.dwUserID);
        return cell;
    },

    numberOfCellsInTableView    :   function(table) {
        return this.m_nFriendCount;
    },

    setIsChatLayer  : function(v) {
        this.m_bIsChatLayer = v;
    },

    setCurrentChatUser  :   function(dwUserID) {
        cc.log("current user ==> " + dwUserID);
        //关闭通知
        var friendcell = self.getFriendListCell(dwUserID);
        if (null != friendcell) {
            var chatbtn = friendcell.getChatBtn();
            NotifyMgr.getInstance().hideNotify(chatbtn, true);
        }
        this.m_dwCurrentUserId = dwUserID;
    },

    refreshNearFriendList   :   function( tablist ) {
        var near = self.getChildByName(NEAR_FRIEND_LIST);
        if (null != near && null != near.onRefreshList) {
            near.onRefreshList(tablist);
        }
    },

    onKeyBack   :   function() {
        return !this.m_bIsChatLayer
    }
});

//
// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new FriendLayer();
//         this.addChild(layer);
//     }
// });