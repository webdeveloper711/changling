/*Author : Jin*/
// var PopupInfoHead = appdf.req(appdf.EXTERNAL_SRC + "PopupInfoHead");
// var NotifyMgr = appdf.req(appdf.EXTERNAL_SRC + "NotifyMgr");
// var chat_cmd = appdf.req(appdf.HEADER_SRC+"CMD_ChatServer");
// var ClipText = require(appdf.EXTERNAL_SRC + "ClipText");

var FriendListCell = cc.Sprite.extend({
    ctor    :   function (dwID, superView) {
        this._super(res.dikuang5_png);
        cc.log("FriendListCell ctor called!!!");
        this.setAnchorPoint(cc.p(0, 0));
        this.setPosition(cc.p(0, 0));
        this.m_StartPos = cc.p(0, 0);//触摸起始点
        this.m_MoveDistance = 30;
        this.m_Super = superView;
        this.m_SuperView = superView.Layer;
        //聊天按钮
        this.m_btnChat = null;
        //用户状态
        this.m_clipUserStatus = null;
        var userInfo = FriendMgr.getInstance().getFriendByID(dwID);
        cc.log("FriendDetailInfo : " + userInfo);
        if (null == userInfo) {
            return;
        }

        var deleBtn = new ccui.Button(res.anniu11_png, res.anniu11_png, res.anniu11_png);
        deleBtn.addTouchEventListener(function (ref, tType) {
            if (tType == ccui.Widget.TOUCH_ENDED) {
                var friendid = userInfo.dwUserID;
                cc.log("==============删除好友============== " + userInfo.szNickName + friendid);
                FriendMgr.getInstance().sendDeleteFriend(friendid, 0);
            }
        });
        deleBtn.setAnchorPoint(cc.p(1, 0.5));
        deleBtn.setPosition(cc.p(this.getContentSize().width, this.getContentSize().height / 2));
        this.m_btnDele = deleBtn;
        this.m_btnDele.setEnabled(false);
        this.addChild(deleBtn);

        this.m_ContentSprite = new cc.Sprite(res.dikuang5_png);
        this.m_ContentSprite.setAnchorPoint(cc.p(0, 0));
        this.m_ContentSprite.setPosition(cc.p(0, 0));
        this.m_ContentSprite.setVisible(true);
        this.addChild(this.m_ContentSprite);

        this.createCell(userInfo, this.m_ContentSprite, this.m_SuperView);
        var onTouchBegan = function (touch, event) {
            var locationInNode = this.convertToNodeSpace(touch.getLocation());
            var size = this.getContentSize();
            var rect = cc.rect(0, 0, size.width, size.height);
            if (cc.rectContainsPoint(rect, locationInNode)) {
                this.m_StartPos = locationInNode;
                return true;
            }
            return false;
        };

        var onTouchMoved = function (touch, event) {
            var delta = touch.getDelta();
            var deltax = delta.x;
            var deltay = delta.y;
        };

        var onTouchEnded = function (touch, event) {

        };
        /*TODO: followings are jsb functions
        // this.m_Listener = new cc.EventListenerTouchOneByOne();
        // //this.m_Listener.setSwallowTouches(true)
        // this.m_Listener.registerScriptHandler(onTouchBegan, cc.TOUCH_BEGAN);
        // this.m_Listener.registerScriptHandler(onTouchMoved, cc.TOUCH_MOVED);
        // this.m_Listener.registerScriptHandler(onTouchEnded, cc.TOUCH_ENDED);

        // var dispacther = cc.director.getEventDispatcher();
        // dispacther.addEventListenerWithSceneGraphPriority(this.m_Listener, this.m_ContentSprite);*/
    },

    createCell : function(userInfo,contentView,superView) {
        var defaultCellSize = this.getContentSize();

        // var head = PopupInfoHead.createClipHead(userInfo, 80)
        var head = (new PopupInfoHead()).createNormal(userInfo, 60)[0];
        head.setPosition(cc.p(65, 44));
        head.setIsGamePop(false);
        //根据会员等级确定头像框
        // head.enableHeadFrame(true)
        // head.enableInfoPop(true, cc.p(420, 120), cc.p(0.5, 0.5))
        head.setName("head");
        contentView.addChild(head);

        //头像框
        var head_frame = new cc.Sprite(res.friendhd_clip_png);
        head_frame.setAnchorPoint(cc.p(0, 0.5));
        head_frame.setPosition(cc.p(30, 44));
        contentView.addChild(head_frame);

        var yPos = defaultCellSize.height * 0.7;
        var nickName = new ClipText(cc.size(150, 30), userInfo.szNickName, res.round_body_ttf, 30);
        nickName.setName("nickName");
        nickName.setTextColor(cc.color(108, 97, 91, 255));
        nickName.setAnchorPoint(cc.p(0, 0.5));
        nickName.setPosition(cc.p(150, yPos));
        contentView.addChild(nickName);

        var start = new cc.Sprite(res.tutiao2_png);
        start.setAnchorPoint(cc.p(0, 0.5));
        start.setPosition(cc.p(360, yPos - 20));
        contentView.addChild(start);

        var lv = new cc.Sprite(res.biaoti2_png);
        lv.setAnchorPoint(cc.p(0, 0.5));
        lv.setPosition(cc.p(415, yPos - 20));
        contentView.addChild(lv);

        var lValue = new cc.LabelAtlas(""+(userInfo.wGrowLevel), res.number_friend_1_png, 20, 26, "0");
        lValue.setAnchorPoint(cc.p(0, 0.5));
        lValue.setPosition(cc.p(450, yPos - 20));
        lValue.setName("lValue");
        contentView.addChild(lValue);

        var chatBtn = new ccui.Button(res.anniu9_png, res.anniu9_png, res.anniu9_png);
        chatBtn.setVisible(false);
        chatBtn.setName("chatBtn");
        chatBtn.addTouchEventListener(function (ref, tType) {
            if (tType == ccui.Widget.TOUCH_ENDED) {
                //关闭通知
                NotifyMgr.getInstance().hideNotify(chatBtn, true);

                //标记在聊天界面
                if (null != this.m_Super.setIsChatLayer) {
                    this.m_Super.setIsChatLayer(false);
                }
                var chatuser = FriendMgr.getInstance().getFriendByID(chatBtn.getTag());
                if (null == chatuser) {
                    return;
                }

                //聊天
                var FriendChatLayer = appdf.getNodeByName(superView, "FriendChatLayer");
                FriendChatLayer.setVisible(false);
                //加載好友列表与聊天列表
                this.m_Super.m_chatListManager = FriendChatList.create(FriendChatLayer, chatuser.dwUserID, this.m_Super);
            }
        });
        chatBtn.setAnchorPoint(cc.p(1, 0.5));
        chatBtn.setPosition(cc.p(750, 60));
        contentView.addChild(chatBtn);
        this.m_btnChat = chatBtn;

        var userStatusLab = new ClipText(cc.size(350, 25), "", res.round_body_ttf, 25);
        userStatusLab.setName("userStatusLab");
        userStatusLab.setTextColor(cc.color(88, 94, 101, 255));
        userStatusLab.setAnchorPoint(cc.p(0, 0.5));
        userStatusLab.setPosition(cc.p(180, defaultCellSize.height * 0.3));
        contentView.addChild(userStatusLab);
        this.m_clipUserStatus = userStatusLab;

        if (userInfo.cbMainStatus == chat_cmd.FRIEND_US_OFFLINE) {
            //离线
            userStatusLab.setString("离线");
            chatBtn.setVisible(false);
            chatBtn.setEnabled(false);
        } else {
            //在线
            userStatusLab.setString("在线");
            chatBtn.setVisible(false);
            chatBtn.setEnabled(false);
        }
    },
    setCellStatusNormal : function() {
        this.m_ContentSprite.setPosition(cc.p(0, 0));

        this.m_btnChat.setVisible(false);
    },
    getChatBtn : function() {
        return this.m_btnChat;
    },
    refreshFriendState : function(userInfoTab)
    {
        //好友状态
        if (userInfoTab.cbMainStatus == chat_cmd.FRIEND_US_OFFLINE) {
            //离线
            this.m_btnChat.setVisible(false);
            this.m_btnChat.setEnabled(false);
            this.m_clipUserStatus.setString("离线");
        } else {
            //上线
            this.m_btnChat.setVisible(false);
            this.m_btnChat.setEnabled(false);

            //dump(userInfoTab, "userInfoTab")
            //游戏状态(游戏中)
            if (userInfoTab.cbGameStatus >= yl.US_FREE) {
                if (null != userInfoTab.szServerName) {
                    var str = "";
                    if (userInfoTab.cbGameStatus >= yl.US_SIT) {
                        str = "在" + userInfoTab.szServerName + (userInfoTab.wTableID) + "号桌";
                    } else {
                        str = "在" + userInfoTab.szServerName;
                    }
                    this.m_clipUserStatus.setString(str);
                }
                else {
                    this.m_clipUserStatus.setString("在线")
                }
            }
        }
    }
});

// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new FriendListCell(222,this);
//         this.addChild(layer);
//     }
// });