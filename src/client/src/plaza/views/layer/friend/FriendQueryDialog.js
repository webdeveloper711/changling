/*Author . JIN */

var FriendQueryDialog = cc.Layer.extend({
    DG_QUERY_EXIT   :  2,
    BT_CANCEL       :  0,
    BT_CONFIRM      :  1,

    // 对话框类型
    QUERY_SURE          : 1,
    QUERY_SURE_CANCEL   : 2,

    //默认字体大小
    DEF_TEXT_SIZE   : 32,

    ctor : function (msgTab, callback, txtsize, queyrType) {
        this._super();
        cc.log("friendQueryDialog called!");
        var queryType = queryType || this.QUERY_SURE_CANCEL;
        this._callback = callback;
        this._canTouchOutside = true;

        var that = this;
        this.setContentSize(appdf.WIDTH, appdf.HEIGHT);
        // this.setPosition(0, appdf.HEIGHT);

        /*TODO:registerScriptHandler?
        //回调函数
        this.registerScriptHandler(function (eventType) {
            if (eventType == "enterTransitionFinish") {    // 进入场景而且过渡动画结束时候触发。
                that.onEnterTransitionFinish();
            } else if (eventType == "exitTransitionStart") {  // 退出场景而且开始过渡动画时候触发。
                that.onExitTransitionStart();
            }
        });*/

        //按键监听
        var btcallback = function (ref, type) {
            if (type == ccui.Widget.TOUCH_ENDED) {
                that.onButtonClickedEvent(ref.getTag(), ref);
            }
        };

        //区域外取消显示
        var onQueryExitTouch = function (eventType, x, y) {
            if (!this._canTouchOutside) {
                return true;
            }

            if (this._dismiss == true) {
                return true;
            }

            if (eventType == "began") {
                var rect = that.getChildByTag(this.DG_QUERY_EXIT).getBoundingBox();
                if (cc.rectContainsPoint(rect, cc.p(x, y)) == false) {
                    this.dismiss();
                }
            }
            return true;
        }
        /*this.setTouchEnabled(true);*/
        /*TODO: registerScriptTouchHandler?
        this.registerScriptTouchHandler(onQueryExitTouch);*/

        var newsp = new cc.Sprite(res.query_bg_png);
        newsp.setTag(this.DG_QUERY_EXIT);
        newsp.setPosition(appdf.WIDTH / 2, appdf.HEIGHT / 2);
        this.addChild(newsp);


        if (this.QUERY_SURE == queryType) {
            var btn = new ccui.Button(res.bt_query_confirm_0_png, res.bt_query_confirm_1_png);
            btn.setPosition(appdf.WIDTH / 2, 200);
            btn.setTag(this.BT_CONFIRM);
            this.addChild(btn);
            btn.addTouchEventListener(btcallback);
        } else {
            btn1 = new ccui.Button(res.bt_query_confirm_0_png, res.bt_query_confirm_1_png);
            btn1.setPosition(appdf.WIDTH / 2 + 169, 200);
            btn1.setTag(this.BT_CONFIRM);
            this.addChild(btn1);
            btn1.addTouchEventListener(btcallback);

            btn2 = new ccui.Button(res.bt_query_cancel_0_png, res.bt_query_cancel_1_png);
            btn2.setPosition(appdf.WIDTH / 2 - 169, 200);
            btn2.setTag(this.BT_CANCEL);
            this.addChild(btn2);
            btn2.addTouchEventListener(btcallback);
        }

        var labelTTF = new cc.LabelTTF("提示", res.round_body_ttf, 36);
        labelTTF.setColor(cc.color(255, 221, 65, 255));
        labelTTF.setAnchorPoint(cc.p(0.5, 0.5));
        labelTTF.setDimensions(600, 120);
        labelTTF.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        labelTTF.setVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
        labelTTF.setPosition(appdf.WIDTH / 2, 545);
        this.addChild(labelTTF);

        var mainMsg = (msgTab.main) || "";
        var mainText = new cc.LabelTTF(mainMsg, res.round_body_ttf, !txtsize && this.DEF_TEXT_SIZE || txtsize);
        mainText.setColor(cc.color(255, 255, 255, 255));
        mainText.setAnchorPoint(cc.p(0.5, 0.5));
        mainText.setDimensions(600, 0);
        mainText.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        mainText.setVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
        mainText.setPosition(appdf.WIDTH / 2, 395);
        this.addChild(mainText);
        var subMsg = msgTab.sub || "";
        if ("" != subMsg) {
            var con = mainText.getContentSize();
            var yPos = 365 - con.height * 0.5;
            var labelTTF = new cc.LabelTTF(subMsg, res.round_body_ttf, !txtsize && FriendQueryDialog.DEF_TEXT_SIZE || txtsize);
            labelTTF.setTextColor(cc.color(255, 255, 255, 255));
            labelTTF.setAnchorPoint(cc.p(0.5, 0.5));
            labelTTF.setDimensions(600, 0);
            labelTTF.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
            labelTTF.setVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
            labelTTF.setPosition(appdf.WIDTH / 2, yPos);
            this.addChild(labelTTF);
        }
        this._dismiss = false;
        // this.runAction(new cc.MoveTo(0.3, cc.p(0, 0)));
    },


    // 进入场景而且过渡动画结束时候触发。
    onEnterTransitionFinish : function() {
        return this;
    },

    // 退出场景而且开始过渡动画时候触发。
    onExitTransitionStart : function() {
        this.unregisterScriptTouchHandler();
        return this;
    },

    //窗外触碰
    setCanTouchOutside : function(canTouchOutside) {
        this._canTouchOutside = canTouchOutside;
        return this;
    },

    //按键点击
    onButtonClickedEvent : function(tag,ref) {
        if (this._dismiss == true) {
            return;
        }
        //取消显示
        this.dismiss();
        //通知回调
        if (this._callback) {
            this._callback(tag === this.BT_CONFIRM);
        }
    },
    //取消消失
    dismiss : function(){
        this._dismiss = true;
        var that = this;
        cc.log(that);
        this.stopAllActions();
        this.runAction(
            new cc.Sequence(
                new cc.MoveTo(0.3,cc.p(0,appdf.HEIGHT)),
                new cc.CallFunc(function(){
                    new cc.removeSelf(true);
                })
            ));
    }

});

// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new FriendQueryDialog({});
//         this.addChild(layer);
//     }
// });

