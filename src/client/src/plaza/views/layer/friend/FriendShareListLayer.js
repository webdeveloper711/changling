//
// Author. zhong
// Date. 2016-12-29 19.30.40
//
// 好友分享列表
// var ExternalFun = appdf.req(appdf.EXTERNAL_SRC + "ExternalFun");
// var PopupInfoHead = appdf.req(appdf.EXTERNAL_SRC + "PopupInfoHead");
// var ClipText = appdf.req(appdf.EXTERNAL_SRC + "ClipText");

var TAG_MASK = 101;
var BTN_CANCELSHARE = 102;

var FriendShareListLayer = cc.Layer.extend({
    ctor : function (callback) {
      this._super();
      cc.log("FriendShareListLayer created!!!");
        this.m_callback = callback;
        // 加载csb资源
        var rc = ExternalFun.loadRootCSB(res.FriendShareListLayer_json, this);
        var rootLayer = rc.rootlayer;
        var csbNode = rc.csbnode;
        var self = this;
        var touchFunC = function (ref, tType) {
            if (tType == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };

        // 遮罩
        var mask = csbNode.getChildByName("Panel_1");
        mask.setTag(TAG_MASK);
        mask.addTouchEventListener(touchFunC);

        // 底板
        var image_bg = csbNode.getChildByName("image_bg");
        image_bg.setTouchEnabled(true);
        image_bg.setSwallowTouches(true);
        image_bg.setScale(0.00001);
        this.m_imageBg = image_bg;

        var content = image_bg.getChildByName("content");
        var contentsize = content.getContentSize();
        this.m_fSix = contentsize.width / 6;
        // 列表
        this._listView = new cc.TableView(this, contentsize);
        this._listView.setDirection(cc.SCROLLVIEW_DIRECTION_HORIZONTAL);
        this._listView.setPosition(content.getPosition());
        this._listView.setDelegate(this);
        image_bg.addChild(this._listView);
        /*
        this._listView.registerScriptHandler(handler(this, this.tableCellTouched), cc.TABLECELL_TOUCHED);
        this._listView.registerScriptHandler(handler(this, this.cellSizeForTable), cc.TABLECELL_SIZE_FOR_INDEX);
        this._listView.registerScriptHandler(handler(this, this.tableCellAtIndex), cc.TABLECELL_SIZE_AT_INDEX);
        this._listView.registerScriptHandler(handler(this, this.numberOfCellsInTableView), cc.NUMBER_OF_CELLS_IN_TABLEVIEW);
        */
        this._listView.reloadData();
        content.removeFromParent();

        // 取消分享
        var btn = image_bg.getChildByName("btn_cancelshare");
        btn.setTag(BTN_CANCELSHARE);
        btn.addTouchEventListener(touchFunC);

        // 加载动画
        image_bg.runAction( new cc.ScaleTo(0.2, 1.0));
    },

    tableCellTouched : function(view, cell){

    },

    cellSizeForTable : function(view, idx){
        return this.m_fSix, 250;
    },

    tableCellSizeForIndex : function(view, idx) {
        return {width : this.m_fSix, height: 90};
    },

    numberOfCellsInTableView : function(view) {
        return (FriendMgr.getInstance().getFriendList()).length;
    },

    tableCellAtIndex : function(view, idx) {
        var cell = view.dequeueCell();
        if (!cell) {
            cell = new cc.TableViewCell();
        } else {
            cell.removeAllChildren();
        }

        // 好友数据
        var frienddata = FriendMgr.getInstance().getFriendList()[idx];
        // 头像
        var head = (new PopupInfoHead()).createClipHead(frienddata, 100, res.friendhd_clip_png)[0];
        head.setPosition(cc.p(74, 165));
        head.setIsGamePop(false);
        var str = "";
        if (frienddata.cbMainStatus == 0) {// chat_cmd.FRIEND_US_OFFLINE
            str = "离线";
            // 灰度图
            if (null != head && null != head.m_head && null != head.m_head.m_spRender) {
                //TODO: convertToGraySprite()?
               // cc.Layout.convertToGraySprite(head.m_head.m_spRender);
                head.setColor(cc.color(200,200,200,50));
            }
        } else if (frienddata.cbMainStatus == 1) {
            str = "在线";
        }
        // 在线
        var statusLab = new cc.LabelTTF(str, res.round_body_ttf, 20);
        statusLab.setPosition(74, 40);

        // 底图
        var bg = new ccui.ImageView(res.sp_friend_cellbg_png);
        bg.setTouchEnabled(true);
        bg.setSwallowTouches(false);
        bg.addTouchEventListener(function (ref, tType) {
            if (tType == ccui.Widget.TOUCH_ENDED) {
                var frienddata = FriendMgr.getInstance().getFriendList()[idx + 1];
                if (typeof this.m_callback == "function" && null != frienddata) {
                    if (frienddata.cbMainStatus != 1) {
                        showToast(this, "好友已离线, 暂时无法发送数据", 2);
                        // 灰度图
                        if (null != head && null != head.m_head && null != head.m_head.m_spRender) {
                            convertToGraySprite(head.m_head.m_spRender);
                        }
                        statusLab.setString("离线");
                        return;
                    }
                    if ("离线" == statusLab.getString()) {
                        statusLab.setString("在线");
                        if (null != head && null != head.m_head && null != head.m_head.m_spRender) {
                            convertToNormalSprite(head.m_head.m_spRender)
                        }
                    }
                    this.m_callback(frienddata);
                }
            }
        });
        bg.setPosition(this.m_fSix * 0.5, view.getViewSize().height * 0.5);
        cell.addChild(bg);
        bg.addChild(head);
        bg.addChild(statusLab);

        // 昵称
        var nick = new ClipText(cc.size(120, 30), frienddata.szNickName);
        nick.setAnchorPoint(cc.p(0.5, 0.5));
        nick.setPosition(cc.p(74, 80));
        bg.addChild(nick);

        return cell;
    },


    onButtonClickedEvent : function(tag, ref) {
        cc.log("onButttonClickEvent called.");
        if (TAG_MASK == tag) {
            this.hide();
        } else if (BTN_CANCELSHARE == tag) {
            this.hide();
        }
    },

    hide : function(){
        var scale1 = new cc.ScaleTo(0.2, 0.0001);
        var self = this;
        var call1 = new cc.CallFunc(function() {
            self.removeFromParent(true);
        });
        this.m_imageBg.runAction(new cc.Sequence(scale1, call1));
    }
});



//
// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new FriendShareListLayer();
//         this.addChild(layer);
//     }
// });