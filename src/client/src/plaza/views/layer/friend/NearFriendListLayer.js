/*
Author : JIN
*/

// 附近好友列表
// var ExternalFun = appdf.req(appdf.EXTERNAL_SRC + "ExternalFun");
// var PopupInfoHead = appdf.req(appdf.EXTERNAL_SRC + "PopupInfoHead");
// var ClipText = appdf.req(appdf.EXTERNAL_SRC + "ClipText");
// var QueryDialog = appdf.req("app.views.layer.other.QueryDialog");
//var MultiPlatform = appdf.req(appdf.EXTERNAL_SRC + "MultiPlatform");

var TAG_MASK = 101;
var BTN_CLOSE = 102;

var NearFriendListLayer = cc.Layer.extend({
    ctor: function () {
        this._super();
        this.m_tabList = [];
        // 加载csb资源
        var rootLayer, csbNode, nearFriendListLayerJson = ExternalFun.loadRootCSB(res.NearFriendListLayer_json, this);
        rootLayer = nearFriendListLayerJson.rootlayer;
        csbNode = nearFriendListLayerJson.csbnode;
        var that = this;
        var touchFunC = function (ref, tType) {
            if (tType == ccui.Widget.TOUCH_ENDED) {
                that.onButtonClickedEvent(ref.getTag(), ref);
            }
        };

        // 遮罩
        var mask = csbNode.getChildByName("Panel_1");
        mask.setTag(TAG_MASK);
        mask.addTouchEventListener(touchFunC);

        // 底板
        var image_bg = csbNode.getChildByName("image_bg");
        cc.log("image_bg:" + image_bg);
        image_bg.setTouchEnabled(true);
        image_bg.setSwallowTouches(true);
        image_bg.setScale(0.00001);
        this.m_imageBg = image_bg;

        var content = image_bg.getChildByName("content");
        var contentsize = content.getContentSize();
        this.m_fSix = contentsize.width;
        // 列表
        this._listView = new cc.TableView(this,contentsize);
        this._listView.setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL);
        this._listView.setPosition(100, 70);
        this._listView.setDelegate();
        image_bg.addChild(this._listView);
        /* TODO:registerscripthandler?
        this._listView.registerScriptHandler(handler(this, this.tableCellTouched), cc.TABLECELL_TOUCHED);
        this._listView.registerScriptHandler(handler(this, this.cellSizeForTable), cc.TABLECELL_SIZE_FOR_INDEX);
        this._listView.registerScriptHandler(handler(this, this.tableCellAtIndex), cc.TABLECELL_SIZE_AT_INDEX);
        this._listView.registerScriptHandler(handler(this, this.numberOfCellsInTableView), cc.NUMBER_OF_CELLS_IN_TABLEVIEW);*/

        this._listView.reloadData();
        content.removeFromParent();

        // 关闭按钮
        var btn = image_bg.getChildByName("btn_close");
        btn.setTag(BTN_CLOSE);
        btn.addTouchEventListener(touchFunC);

        // 加载动画
        image_bg.runAction(new cc.ScaleTo(0.2, 1.0));
    },

    tableCellTouched : function(view, cell){

    },

    cellSizeForTable : function(view, idx) {
        return this.m_fSix, 90;
    },

    numberOfCellsInTableView : function(view) {
        return this.m_tabList.length;
    },

    tableCellAtIndex : function(view, idx) {
        var cell = view.dequeueCell();
        if (!cell) {
            cell = new cc.TableViewCell();
        } else {
            cell.removeAllChildren();
        }
        // 好友数据
        var frienddata = this.m_tabList[idx + 1];
        if (null == frienddata) {
            return cell;
        }

        //底框条
        var frame = cc.Sprite(res.dikuang_back_png);
        frame.setPosition(cc.p(430, 40));
        cell.addChild(frame);

        // 按钮
        var btn = ccui.Button(res.btn_join_friend_1_png, res.btn_join_friend_2_png);
        btn.setTouchEnabled(true);
        btn.setSwallowTouches(false);
        btn.setPosition(cc.p(1250, 40));
        btn.addTouchEventListener(function (ref, tType) {
            if (tType == ccui.Widget.TOUCH_ENDED) {
                var friendinfo = FriendMgr.getInstance().getFriendInfoByID(frienddata.dwUserID);
                if (null == friendinfo) {
                    var query = QueryDialog.create("是否发送添加好友申请?", function (ok) {
                        if (ok) {
                            //TODO: {} object?
                            var addFriendTab = {};
                            addFriendTab.dwUserID = GlobalUserItem.dwUserID;
                            addFriendTab.dwFriendID = frienddata.dwUserID;
                            addFriendTab.cbGroupID = 0;
                            var sendResult = function (isAction) {
                                if (isAction) {
                                    showToast(this, "已发送好友申请", 2);
                                }
                            }
                            //添加好友
                            FriendMgr.getInstance().sendAddFriend(addFriendTab, sendResult);
                        }
                    });
                    query.setCanTouchOutside(false)
                    this.addChild(query);
                } else {
                    showToast(this, "已经是好友", 2);
                }
            }
        });
        btn.setPosition(this.m_fSix * 0.5 + 340, view.getViewSize().height * 0.5 - 160);
        cell.addChild(btn);

        //等级
        sp1 = cc.Sprite(res.tutiao2_png);
        sp1.setPosition(270, 40);
        cell.addChild(sp1);

        sp2 = cc.Sprite(res.biaoti2_png);
        sp2.setPosition(320, 40);
        cell.addChild(sp2);

        this._level = cc.LabelAtlas(GlobalUserItem.wCurrLevelID + "", "Friend/number_friend_1.png", 20, 26, string.byte("0"));
        this._level.setPosition(340, 40);
        this._level.setAnchorPoint(cc.p(0, 0.5));
        cell.addChild(this._level);
        this._level.setVisible(true);

        // 头像
    //    var head = PopupInfoHead.createClipHead(frienddata, 70, "Friend/friendhd_clip.png")
    //    head.setPosition(cc.p(74,165))
    //    head.setIsGamePop(false)
    //    cell.addChild(head)
        var head = PopupInfoHead.createNormal(frienddata, 60);
        head.setPosition(cc.p(65, 38));
        head.setIsGamePop(false);
        cell.addChild(head);
        //头像框
        var head_frame = cc.Sprite.create(res.friendhd_clip_png);
        head_frame.setAnchorPoint(cc.p(0, 0.5));
        head_frame.setPosition(cc.p(30, 38));
        cell.addChild(head_frame);

        // 昵称
        var szNick = frienddata.szNickName || "";
        var nick = ClipText.createClipText(cc.size(120, 30), szNick);
        nick.setAnchorPoint(cc.p(0.5, 0.5));
        nick.setPosition(cc.p(170, 40));
        nick.setTextColor(cc.c4b(108, 97, 98, 255));
        cell.addChild(nick);

        // 距离
    //    var dis = frienddata.dwDistance || 0
    //    var dLongitude = frienddata.dLongitude
    //    var dLatitude = frienddata.dLatitude
    //    //[[var tab = {}
    //    tab.myLatitude = GlobalUserItem.tabCoordinate.la
    //    tab.myLongitude =  GlobalUserItem.tabCoordinate.lo
    //    tab.otherLatitude = dLatitude
    //    tab.otherLongitude =  dLongitude
    //    dump(MultiPlatform.getInstance().metersBetweenLocation(tab), "MultiPlatform.getInstance().metersBetweenLocation(tab)", 6)
    //    print("NearFriendListLayer ==> ", frienddata.dwDistance)]]
    //    if dLongitude != 0 && dLatitude != 0 then
    //        var szDis = ""
    //        var szUnit = ""
    //        if frienddata.dwDistance > 1000 then
    //            szDis = string.format("%.2f", frienddata.dwDistance / 1000)
    //            szUnit = "千米"
    //        else
    //            szDis = "" + frienddata.dwDistance
    //            szUnit = "米"
    //        end

    //        // 距离
    //        var clipdis = ClipText.createClipText(cc.size(120, 30), szDis)
    //        clipdis.setAnchorPoint(cc.p(1, 0.5))
    //        clipdis.setPosition(cc.p(96, 40))
    //        cell.addChild(clipdis)

    //        // 单位
    ////        var txt = cc.Label.createWithTTF(szUnit, "fonts/round_body.ttf", 20)
    ////        bg.addChild(txt)
    ////        txt.setAnchorPoint(cc.p(0, 0.5))
    ////        txt.setPosition(96,40)
    //    end

        return cell;
    },

    onRefreshList : function( tablist ) {
        if (typeof tablist == "table") {
            this.m_tabList = tablist;
            this._listView.reloadData();
        }
    },

    onButtonClickedEvent : function(tag, ref){
        if (TAG_MASK == tag) {
            this.hide();
        } else if (BTN_CLOSE == tag) {
            this.hide();
        }
    },

    hide : function() {
        var scale1 = new cc.ScaleTo(0.2, 0.0001);
        var that = this;
        var call1 = new cc.CallFunc(function () {
            that.removeFromParent(this);
        });
        this.m_imageBg.runAction(new cc.Sequence(scale1, call1));
    }
});

// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new NearFriendListLayer();
//         this.addChild(layer);
//     }
// });
