/*Author    .   JIN */

// var ExternalFun = appdf.req(appdf.EXTERNAL_SRC + "ExternalFun");
// var QueryDialog   = appdf.req("app.views.layer.other.QueryDialog");
// var Shop = appdf.req(appdf.CLIENT_SRC+"plaza.views.layer.plaza.ShopLayer");

// var targetPlatform = cc.Application.getTargetPlatform();
var targetPlatform = cc.sys.platform;
var BT_CLOSE = 101;
var BT_CANCEL = 102;
var BT_SURE = 103;

var TrumpetSendLayer = cc.Layer.extend({
   ctor : function (scene) {
       this._super(cc.color(0,0,0,125));
       // cc.spriteFrameCache.addSpriteFrames(res.public_plist);
       cc.log("TrumpetSendLayer called!!!");
       this._scene = scene;
       //注册触摸事件
       /*
       TODO:? registerTouchEvent
       ExternalFun.registerTouchEvent(this, true);
       */
       var public_plist = cc.loader.getRes(res.public_plist);
       //加载csb资源
       var rc = ExternalFun.loadRootCSB(res.TrumpetSendLayer_json, this);
       var rootLayer = rc.rootlayer;
       var csbNode = rc.csbnode;

       this.m_spBg = csbNode.getChildByName("sp_bg");
       if (targetPlatform == cc.sys.ANDROID) {
           this.m_spBg.setScale(0.000001);
       } else {
           this.m_spBg.setScale(1.0);
       }
       var spbg = this.m_spBg;

       //喇叭数量
       this.m_altasTrumpetNum = spbg.getChildByName("atlas_left");

       //编辑区
       var tmp = spbg.getChildByName("edit_bg");
       if (targetPlatform == cc.sys.IPHONE || targetPlatform == cc.sys.IPAD) {
           //TODO: UI_TEX_TYPE_PLIST?  ccui.TextureResType.plistType
           var editbox = new cc.EditBox(cc.size(tmp.getContentSize().width - 10, 60), public_plist['blank.png']);
           editbox.setPosition(tmp.getPosition());
           editbox.setPlaceHolder("请输入喇叭消息");
           editbox.setMaxLength(64);
           spbg.addChild(editbox);
           editbox.setContentSize(cc.size(tmp.getContentSize().width - 10, tmp.getContentSize().height - 10));
           editbox.setPlaceholderFont(res.round_body_ttf, 24);
           editbox.setFont(res.round_body_ttf, 24);
           this.m_editTrumpet = editbox;
       } else {
           //TODO: UI_TEX_TYPE_PLIST?
           var editbox = new cc.EditBox(cc.size(tmp.getContentSize().width - 10, tmp.getContentSize().height - 10), public_plist['blank.png']);
           editbox.setPosition(tmp.getPosition());
           editbox.setPlaceHolder("请输入喇叭消息");
           editbox.setPlaceholderFont(res.round_body_ttf, 24);
           editbox.setFont(res.round_body_ttf, 24);
           editbox.setMaxLength(64);
           spbg.addChild(editbox);
           this.m_editTrumpet = editbox;
       }
       var self = this;
       var btncallback = function (ref, tType) {
           if (tType == ccui.Widget.TOUCH_ENDED) {
               self.onButtonClickedEvent(ref.getTag(), ref);
           }
       };
       var btn = spbg.getChildByName("btn_close");
       btn.setTag(BT_CLOSE);
       btn.addTouchEventListener(btncallback);

       btn = spbg.getChildByName("btn_cancel");
       btn.setTag(BT_CANCEL);
       btn.addTouchEventListener(btncallback);

       btn = spbg.getChildByName("btn_sure");
       btn.setTag(BT_SURE);
       btn.addTouchEventListener(btncallback);
       //加载动画
       var call = new cc.CallFunc(function () {
           self.setVisible(true);
           self.m_altasTrumpetNum.setString("" + GlobalUserItem.nLargeTrumpetCount);
       });
       var scale = new cc.ScaleTo(0.2, 1.0);
       this.m_actShowAct = new cc.Sequence(call, scale);
       ExternalFun.SAFE_RETAIN(this.m_actShowAct);

       var scale1 = new cc.ScaleTo(0.2, 0.0001);
       var call1 = new cc.CallFunc(function () {
           self.setVisible(false);
           self.m_editTrumpet.setText("");
       });
       this.m_actHideAct = new cc.Sequence(scale1, call1);
       ExternalFun.SAFE_RETAIN(this.m_actHideAct);

       // this.setVisible(false);
   },

    showLayer : function( va ){
        var ani = null;
        if (va) {
            ani = this.m_actShowAct;
        } else {
            ani = this.m_actHideAct;
        }

        if (null != ani) {
            this.m_spBg.stopAllActions();
            if (targetPlatform == cc.sys.ANDROID) {
                this.m_spBg.runAction(ani);
            } else {
                this.setVisible(va);
                if (va) {
                    this.m_altasTrumpetNum.setString("" + GlobalUserItem.nLargeTrumpetCount);
                }
            }
        }
    },

    onButtonClickedEvent : function( tag, sender ) {
        cc.log("onButtonClickEvent called!!!");
        if (BT_CLOSE == tag) {
            this.showLayer(false);
        } else if (BT_SURE == tag) {
            if (GlobalUserItem.nLargeTrumpetCount < 1) {
                this.showLayer(false);
                var query = new QueryDialog("您的喇叭数量不足，是否前往商城购买！", function (ok) {
                    if (ok == true) {
                        this._scene.onChangeShowMode(yl.SCENE_SHOP, Shop.CBT_PROPERTY);
                    }
                    query = null;
                }).setCanTouchOutside(false);
                this._scene.addChild(query);
                return;
            }

            var content = this.m_editTrumpet.getText();
            if (content.length < 1) {
                showToast(this, "喇叭内容不能为空", 3);
            } else {
                //判断emoji
                if (ExternalFun.isContainEmoji(content)) {
                    showToast(this, "喇叭内容包含非法字符,请重试", 2);
                    return;
                }

                //敏感词过滤
                if (true == ExternalFun.isContainBadWords(content)) {
                    showToast(this, "喇叭内容包含敏感词汇!", 3);
                    return;
                }

                var msgTab = {};
                msgTab.dwSenderID = GlobalUserItem.dwUserID;
                msgTab.szNickName = GlobalUserItem.szNickName;
                msgTab.dwFontColor = 16777215;
                msgTab.cbFontSize = 0;
                msgTab.cbFontAttri = 0;
                msgTab.szFontName = "微软雅黑";
                msgTab.szMessageContent = content;

                //喇叭使用后数量控制在使用结果处理
                FriendMgr.getInstance().sendTrupmet(msgTab);

                this.showLayer(false);
                //this.m_editTrumpet.setText("")
            }
        } else if (BT_CANCEL == tag) {
            this.showLayer(false);
        }
    },


    onExit : function() {
        ExternalFun.SAFE_RELEASE(this.m_actShowAct);
        this.m_actShowAct = null;
        ExternalFun.SAFE_RELEASE(this.m_actHideAct);
        this.m_actHideAct = null;
    },

    onTouchBegan : function(touch, event) {
        return this.isVisible();
    },

    onTouchEnded : function(touch, event){
        var pos = touch.getLocation();
        var m_spBg = this.m_spBg;
        pos = m_spBg.convertToNodeSpace(pos);
        var rec = cc.rect(0, 0, m_spBg.getContentSize().width, m_spBg.getContentSize().height);
        if (false == cc.rectContainsPoint(rec, pos)) {
            //this.showLayer(false)
        }
    },

    showPopWait : function() {
        this._scene.showPopWait();
    },

    dismissPopWait : function() {
        this._scene.dismissPopWait();
    }

});


// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new TrumpetSendLayer(this);
//         this.addChild(layer);
//     }
// });