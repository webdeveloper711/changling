/*Author    :   JIN*/
var targetPlatform = cc.sys.platform;

//关闭
var BT_CLOSE = 101;
//发送聊天
var BT_SEND = 102;
//文本聊天
var CBT_TEXT = 201;
//表情聊天
var CBT_BROW = 202;
//聊天记录
var CBT_RECORD = 203;
var CHAT_ITEM_NAME = "game_chat_item";
var CHAT_BROW_PANEL = "game_brow_panel";
var GAMECHAT_LISTENER = "gamechat_listener";
//表情tableview宽度
var LEN_BROWVIEW_WIDTH = 480;
//tableview宽度
var LEN_TABLEVIEW_WIDTH = 600;
//聊天文本大小
var SIZE_CHAT_LABEL = 26;
//聊天内容宽度
var LEN_CHAT_WIDTH = 600;

//聊天记录
var chatRecord = [];
//文本聊天内容
var textChatConfig = [];
var idxTable = {};

//聊天展现层
var GameChatLayer = cc.Layer.extend({
    ctor : function (netframe, csbfile) {
        this._super();
        if (null == netframe) {
            cc.log("net frame can't be null");
            return;
        }
        this.m_netframe = netframe;
        this.loadTextChat();

        /*TODO:
        //注册触摸事件
        ExternalFun.registerTouchEvent(this, true);*/
        csbfile = csbfile || res.GameChatLayer_json;
        //加载csb资源
        var rc = ExternalFun.loadRootCSB(csbfile, this);
        cc.spriteFrameCache.addSpriteFrames(res.public_plist);

        rootLayer = rc.rootlayer;
        csbNode = rc.csbnode;
        var chat_bg = csbNode.getChildByName("chat_bg");
        this.m_spChatBg = chat_bg;

        //内容区域
        this.m_spChatArea = chat_bg.getChildByName("chat_area");
        this.m_spChatArea.setScale(1);
        this.m_spChatArea.setContentSize(420, 350);
        var self = this;
        var btncallback = function (ref, tType) {
            if (tType == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };
        //关闭按钮
        var btn = chat_bg.getChildByName("btn_close");
        btn.setTag(BT_CLOSE);
        btn.addTouchEventListener(btncallback);
        btn.setVisible(false);
        //发送按钮
        btn = chat_bg.getChildByName("btn_send");
        btn.setTag(BT_SEND);
        btn.addTouchEventListener(btncallback);

        //编辑框
        var tmp = chat_bg.getChildByName("edit_frame");
        var size = tmp.getContentSize();
        ExternalFun.dump(size, "tmp.getContentSize()");
        //TODO: ? UI_TEX_TYPE_PLIST
        var editbox = new cc.EditBox(cc.size(size.width - 10, size.height - 10), new cc.Scale9Sprite());
        editbox.setPosition(tmp.getPosition());
        editbox.setFontColor(cc.color(0, 0, 0));
        editbox.setAnchorPoint(cc.p(0.5, 0.5));
        editbox.setFont(res.round_body_ttf, 30);
        editbox.setPlaceholderFont(res.round_body_ttf, 30);
        editbox.setMaxLength(32);
        editbox.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        //.setVisible(false)
        //.setPlaceHolder("请输入聊天信息")
        chat_bg.addChild(editbox);
        this.m_editChat = editbox;
        this.m_bEditChat = false;

        var checkEvent = function (sender, eventType) {
            self.onCheckBoxClickEvent(sender, eventType);
        };

        this.m_nSelect = CBT_TEXT;
        //文本聊天按钮
        var cbt = chat_bg.getChildByName("text_check");

        cbt.setTag(CBT_TEXT);
        cbt.addEventListener(checkEvent);
        cbt.setSelected(true);
        cbt.setVisible(false);
        this.m_checkText = cbt;
        this.m_spChatArea.setVisible(false);

        //历史记录按钮
        cbt = chat_bg.getChildByName("record_check");
        cbt.setTag(CBT_RECORD);
        cbt.addEventListener(checkEvent);
        cbt.setSelected(false);
        cbt.setVisible(false);
        this.m_checkRecord = cbt;
        //内容
        this.m_textTableView = null;
        this.m_browTableView = null;
        this.m_recordTableView = null;
        //默认显示
        this.showTextChat(true);
        this.setVisible(false);

        //ios设置父节点缩放值，会导致editbox显示异常
        if (targetPlatform == cc.sys.ANDROID) {
            this.m_spChatBg.setScale(0.000001);
        }
        //动作
        var call = new cc.CallFunc(function () {
            self.setVisible(true);
            if (self.m_nSelect == CBT_RECORD && null != self.m_recordTableView) {
                self.m_recordTableView.reloadData();
                self.moveRecordToLastRow();
            }
        });

        var sca = new cc.ScaleTo(0.2, 1.0);
        var call1 = new cc.CallFunc(function () {
            if (targetPlatform != cc.sys.ANDROID) {
                self.m_editChat.setVisible(true);
            }
            self.m_editChat.setEnabled(true);
        });
        this.m_actShow = new cc.Sequence(call, sca, call1);
        ExternalFun.SAFE_RETAIN(this.m_actShow);

        var call2 = new cc.CallFunc(function () {
            if (targetPlatform != cc.sys.ANDROID) {
                self.m_editChat.setVisible(false);
            }
            self.m_editChat.setEnabled(false);
        });
        var sca2 = new cc.ScaleTo(0.2, 0.000001);
        var call3 = new cc.CallFunc(function () {
            self.setVisible(false);
        });
        this.m_actHide = new cc.Sequence(call2, sca2, call3);
        ExternalFun.SAFE_RETAIN(this.m_actHide);

        var customListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                return self.isVisible();
            },
            onTouchEnded : function (touch, event) {
                var pos = touch.getLocation();
                var m_spBg = self.m_spChatBg;
                pos = m_spBg.convertToNodeSpace(pos);
                var rec = cc.rect(0, 0, m_spBg.getContentSize().width, m_spBg.getContentSize().height);
                if (false == cc.rectContainsPoint(rec, pos) && false == self.m_bEditChat) {
                    self.showGameChat(false);
                }

                if (true == self.m_bEditChat) {
                    self.m_bEditChat = false;
                }
            }
        });
        cc.eventManager.addListener(customListener, this);

        //todo: cc.EventListenerCustom into EventLisener
        this.m_listener = cc.EventListener.create({
            event: cc.EventListener.CUSTOM,
            eventName: GAMECHAT_LISTENER,
            callback: function (target, event) {
                if (self.m_nSelect == CBT_RECORD && null != self.m_recordTableView) {
                    self.m_recordTableView.reloadData();
                    self.moveRecordToLastRow();
                }
            }
        });
        cc.eventManager.addListener(this.m_listener, this);
    },

    getRecordConfig : function() {
        // cc.spriteFrameCache.addSpriteFrames(clientRes.brow_plist);
        // cc.spriteFrameCache.addSpriteFrames(clientRes.plaza_plist);
        return {bBrow: false, strChat: "", nIdx: 0, szNick: ""};
    },

    loadTextChat : function(){
       /* var str = cc.FileUtils.getStringFromFile(res.chat_text_json);
        // var ok, datatable = pcall(function()
        // 		       return cjson.decode(str)
        // 		    end)

        if (true == ok && type(datatable) == "table") {
            for (k, v in pairs(datatable)) {
                var record = {};
                record.bBrow = false;
                record.strChat = v;
                textChatConfig[k] = record;
            }
        } else {
            cc.log("load text chat error!");
        }
       */
       var str = cc.loader.getRes(res.chat_text_json);
       for (var i = 0; i < str.length; i++) {
           var record ={};
           record.bBrow = false;
           record.strChat = str[i];
           textChatConfig[i] = record;
       }
    },
    // 默认短语对比
    compareWithText : function(str){
        // for (k in textChatConfig) {
        //     var v = textChatConfig[k];
        //     if (v.strChat == str) {
        //         return (k - 1);
        //     }
        // }
        for(k in textChatConfig){
            if(textChatConfig[k].strChat == str ) {
                return k;
            }
        }
        return null;
    },

    showGameChat : function( bShow ) {
        cc.log("showGameChat >> bShow:"+bShow);
        //防作弊不聊天
        if (GlobalUserItem.isAntiCheat()) {
            var runScene = cc.director.getRunningScene();
            showToast(runScene, "防作弊房间禁止聊天", 3);
            return;
        }
        var ani = null;
        ani = bShow?this.m_actShow:this.m_actHide;
        if (null != ani) {
            this.m_spChatBg.stopAllActions();
            if (targetPlatform == cc.sys.ANDROID) {
                this.m_spChatBg.runAction(ani);
            } else {
                this.setVisible(bShow);
                if (this.m_nSelect == CBT_RECORD && null != this.m_recordTableView) {
                    this.m_recordTableView.reloadData();
                    this.moveRecordToLastRow();
                }
            }
        }
    },

    showTextChat : function(bShow) {
        if (bShow) {
            if (null == this.m_textTableView) {
                this.m_textTableView = this.getDataTableView(cc.size(600, 400));
                this.m_spChatBg.addChild(this.m_textTableView);
            }
            this.m_textTableView.reloadData();
        }

        if (null != this.m_textTableView) {
            this.m_textTableView.setVisible(bShow);
        }
    },

    showRecord : function(bShow) {
        if (bShow) {
            if (null == this.m_recordTableView) {
                this.m_recordTableView = this.getDataTableView(cc.size(600, 240));
                this.m_spChatBg.addChild(this.m_recordTableView);
            }
            this.m_recordTableView.reloadData();
            this.moveRecordToLastRow();
        }

        if (null != this.m_recordTableView) {
            this.m_recordTableView.setVisible(bShow);
        }
    },

    moveRecordToLastRow : function() {
        var container = this.m_recordTableView.getContainer();
        var needToLastRow = false;
        if (null != container) {
            needToLastRow = (container.getContentSize().height >= this.m_recordTableView.getViewSize().height);
            //this.m_recordTableView.setTouchEnabled(needToLastRow)
        }
        if (needToLastRow) {
            this.m_recordTableView.setContentOffset(cc.p(0, 0), false);
        }
    },

    getDataTableView : function(size) {
        //tableview
        var m_tableView = new cc.TableView(this,size);
        m_tableView.setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL);
        //聊天框位置
        m_tableView.setPosition(cc.p(-65, 70));
        m_tableView.setDelegate(this);
        m_tableView.setScale(1);
        return m_tableView;
    },

    onButtonClickedEvent : function( tag, sender ) {
        if (BT_CLOSE == tag) {
            this.showGameChat(false);
        } else if (BT_SEND == tag) {
            var chatstr = this.m_editChat.getString();
            cc.log("SEND BUTTON CLICKED"+chatstr);
            // chatstr = chatstr.replace(" ", "");
            if (chatstr.length > 128) {
                showToast(this, "聊天内容过长", 2);
                return;
            }
            // //判断emoji
            // if (ExternalFun.isContainEmoji(chatstr)) {
            //     showToast(this, "聊天内容包含非法字符,请重试", 2);
            //     return;
            // }
            //
            // //敏感词过滤
            // if (true == ExternalFun.isContainBadWords(chatstr)) {
            //     showToast(this, "聊天内容包含敏感词汇!", 2);
            //     return;
            // }

            if ("" != chatstr) {
                var res = this.sendTextChat(chatstr);
                var valid = res[0];
                var msg = res[1];
                if (false == valid && typeof msg == "string" && "" != msg) {
                    showToast(this, msg, 2);
                } else {
                    this.m_editChat.setString("");
                }
            }
        }
    },

    onCheckBoxClickEvent : function(sender, eventType) {
        if (null == sender) {
            return;
        }

        var tag = sender.getTag();
        if (this.m_nSelect == tag) {
            sender.setSelected(true);
            return;
        }
        this.m_nSelect = tag;
        this.m_editChat.setText("");

        var bShowText = false;
        var bShowRecord = false;
        if (CBT_TEXT == tag) {
            bShowText = true;
            bShowRecord = false;
        } else if (CBT_RECORD == tag) {
            bShowText = false;
            bShowRecord = true;
        }

        this.m_checkText.setSelected(bShowText);
        this.m_checkRecord.setSelected(bShowRecord);
        this.showTextChat(bShowText);
        this.showRecord(bShowRecord);
        this.m_spChatArea.setVisible(bShowRecord);
    },

    onEnterTransitionFinish : function() {

    },

    onEditEvent : function(event,editbox) {
        var src = editbox.getText();
        if (event == "began") {
            this.m_bEditChat = src.length != 0;
        } else if (event == "changed") {
            this.m_bEditChat = src.length != 0;
        }
    },

    onExit : function() {
        chatRecord = [];

        ExternalFun.SAFE_RELEASE(this.m_actShow);
        this.m_actShow = null;
        ExternalFun.SAFE_RELEASE(this.m_actHide);
        this.m_actHide = null;

        if (null != this.m_listener) {
            cc.eventManager.removeCustomListeners(this.m_listener);
            //cc.director.getEventDispatcher().removeEventListener(this.m_listener);
        }
    },

    isTouchBrowEnable : function() {
        return (CBT_BROW == this.m_nSelect);
    },

    //发送文本聊
    sendTextChat : function(msg) {
        if (null != this.m_netframe && null != this.m_netframe.sendTextChat) {
            return this.m_netframe.sendTextChat(msg);
        }
        return [false, ""];
    },

    //发送表情聊天
    sendBrowChat : function(idx) {
        if (null != this.m_netframe && null != this.m_netframe.sendBrowChat) {
            return this.m_netframe.sendBrowChat(idx);
        }
        return [false, ""];
    },

    //添加聊天记录
    addChatRecord : function(rec) {
        rec = rec || GameChatLayer.getRecordConfig();
        chatRecord.push(rec);
        // chatRecord.insert(0, rec);

        //通知
        var eventListener = new cc.EventCustom(GAMECHAT_LISTENER);
        cc.eventManager.dispatchEvent(eventListener);

        // var eventListener = cc.EventCustom(GAMECHAT_LISTENER);
        // cc.director().getEventDispatcher().dispatchEvent(eventListener);
    },

    addChatRecordWith : function(cmdtable, bBrow) {
        var rec = this.getRecordConfig();
        rec.bBrow = bBrow;
        rec.szNick = cmdtable.szNick;
        if (bBrow) {
            rec.nIdx = cmdtable.wItemIndex;
        } else {
            rec.strChat = cmdtable.szChatString;
        }

        this.addChatRecord(rec);
    },

    getTextChatSize : function(str) {
        var tmp = cc.LabelTTF(str, clientRes.round_body, SIZE_CHAT_LABEL, cc.size(LEN_CHAT_WIDTH, 050));
        var tmpsize = tmp.getContentSize();
        tmp.setString("网");
        return [LEN_TABLEVIEW_WIDTH, tmpsize.height + 15, tmp.getContentSize().height];
    },

    //tableview
    cellSizeForTable : function( view, idx ) {
        if (CBT_TEXT == this.m_nSelect) {
            var record = textChatConfig[idx + 1];
            if (null != record) {
                return cc.size(900, 60);
            }
            return cc.size(0, 0);
        } else if (CBT_RECORD == this.m_nSelect) {
            var record = chatRecord[idx + 1];
            if (null != record) {
                if (true == record.bBrow) {
                    return cc.size(LEN_TABLEVIEW_WIDTH, 80);
                } else {
                    var result = this.getTextChatSize(record.strChat);
                    var wi = result[0];
                    var he = result[1];
                    var si = result[2];
                    return cc.size(wi, he + si);
                }
            }
            return cc.size(0, 0);
        }
        return cc.size(0, 0);
    },

    tableCellSizeForIndex : function (view, idx) {
        if (CBT_TEXT == this.m_nSelect) {
            var record = textChatConfig[idx];
            if (null != record) {
                return cc.size(900, 60);
            }
            return cc.size(0, 0);
        } else if (CBT_RECORD == this.m_nSelect) {
            var record = chatRecord[idx];
            if (null != record) {
                if (true == record.bBrow) {
                    return cc.size(LEN_TABLEVIEW_WIDTH, 80);
                } else {
                    var result = this.getTextChatSize(record.strChat);
                    var wi = result[0];
                    var he = result[1];
                    var si = result[2];
                    return cc.size(wi, he + si);
                }
            }
            return cc.size(0, 0);
        }
        return cc.size(0, 0);
        // return cc.size(100, 60);
    },

    numberOfCellsInTableView : function( view ) {
        if (CBT_TEXT == this.m_nSelect) {
            return textChatConfig.length;
        } else if (CBT_RECORD == this.m_nSelect) {
            return chatRecord.length;
        }
        return 0;
    },

    tableCellAtIndex : function(view, idx) {
        var self = this;
        var cell = view.dequeueCell();
        var dataitem = null;

        if (null == cell) {
            cell = new cc.TableViewCell();
            dataitem = this.getDataItemAt(view, idx);
            if (null != dataitem) {
                dataitem.setName(CHAT_ITEM_NAME);
                cell.addChild(dataitem);
            }
        } else {
            dataitem = cell.getChildByName(CHAT_ITEM_NAME);
        }
        if (null != dataitem){
            this.refreshDataItemAt(view, dataitem, idx);
        }

        cell.setTag(idx);

        var btn = new ccui.Button(res.chat_item_framebg_png, res.chat_item_framebg_png);
        btn.setTouchEnabled(true);
        btn.setSwallowTouches(false);
        // btn.setScale(3,1.3);
        btn.addTouchEventListener(function (ref, tType) {
            if (tType == ccui.Widget.TOUCH_ENDED) {
                cc.log(cell.getIdx()+"'s CELL IS SELECTED");
                if (CBT_TEXT == self.m_nSelect) {
                    var idx = cell.getIdx();
                    if (null != textChatConfig[idx]) {
                        self.sendTextChat(textChatConfig[idx].strChat);
                    }
                }
            }
        });
        btn.setPosition(view.getViewSize().width * 0.5-3, view.getViewSize().height * 0.5 - 173);
        btn.setOpacity(0);
        cell.addChild(btn);

        return cell;
    },

    // tableCellTouched : function(view, cell) {
    //     cc.log(cell.getIdx()+"'s CELL IS SELECTED");
    //     if (CBT_TEXT == this.m_nSelect) {
    //         var idx = cell.getIdx();
    //         if (null != textChatConfig[idx]) {
    //             this.sendTextChat(textChatConfig[idx].strChat);
    //         }
    //     }
    // },

    getDataItemAt : function(view, idx) {
        if (CBT_TEXT == this.m_nSelect) {
            var chatitem = new ChatItem(false);
            chatitem.setName(CHAT_ITEM_NAME);
            return chatitem;
        } else if (CBT_RECORD == this.m_nSelect) {
            chatitem = new ChatItem(true);
            chatitem.setName(CHAT_ITEM_NAME);
            return chatitem;
        }
        return null;
    },

    refreshDataItemAt : function(view, item, idx) {
        if (null == item) {
            return;
        }

        if (CBT_TEXT == this.m_nSelect) {
            var record = textChatConfig[idx];
            if (null != record) {
                item.refreshTextItem(record.strChat);
            }
        } else if (CBT_RECORD == this.m_nSelect) {
            record = chatRecord[idx];
            if (null != record) {
                item.refreshRecordItem(record.strChat, record.szNick);
            }
        }

        var size = item.getContentSize();
        item.setPosition(size.width * 0.5, 0);
    }
});

//聊天内容
var ChatItem = cc.Node.extend({
    ctor : function (bRecord) {
        this._super();
        cc.spriteFrameCache.addSpriteFrames(res.plaza_plist);
        if (!bRecord) {
            var frame = cc.spriteFrameCache.getSpriteFrame('chat_item_framebg.png');
            if (null != frame) {
                // this.m_spLine = new cc.SpriteFrame(frame);
                this.m_spLine = new cc.Sprite(frame);
                cc.log("m_spLine : " + this.m_spLine);
                //文字在聊天底框的位置
                this.m_spLine.setPosition(-4, this.m_spLine.getContentSize().height * 0.5);
                this.m_spLine.setScale(1);
                this.addChild(this.m_spLine);
            }
        }

        this.m_labelChat = null;
        this.m_clipUserName = null;
    },

    refreshTextItem : function(str){
        if (str.length>14) str = str.substring(0,14)+"...";
        if (null == this.m_labelChat) {
            // this.m_labelChat = new ClipText(cc.size(400, 50), str, res.yahei_body_ttf, 26);
            this.m_labelChat = new cc.LabelTTF(str, res.yahei_body_ttf, 26, cc.size(400, 50), cc.TEXT_ALIGNMENT_LEFT);
            this.m_labelChat.setAnchorPoint(cc.p(0, 0.5));
            this.m_labelChat.setPosition(-200, this.m_spLine.getContentSize().height * 0.5-12);
            // this.m_labelChat.setTextColor(cc.color(228, 254, 254, 255));
            this.m_labelChat.setFontFillColor(cc.color(228, 254, 254, 255));

            this.addChild(this.m_labelChat);
        } else {
            this.m_labelChat.setString(str);
        }
        // var labSize = this.m_labelChat.getContentSize();
        this.setContentSize(cc.size(LEN_TABLEVIEW_WIDTH, 50 + 15));
    },

    refreshRecordItem : function(str,sendusernick) {
        if (str.length>14) str = str.substring(0,14)+"...";
        if (null == this.m_labelChat) {
            this.m_labelChat = cc.LabelTTF(str, res.round_body_ttf, 20, cc.size(LEN_CHAT_WIDTH, 0), cc.TEXT_ALIGNMENT_LEFT);
            this.m_labelChat.setAnchorPoint(cc.p(0.5, 0));
            this.m_labelChat.setPositionY(5);
            this.m_labelChat.setFontFillColor(cc.color(91, 22, 4));
            this.addChild(this.m_labelChat);
        } else {
            this.m_labelChat.setString(str);
        }
        var labSize = this.m_labelChat.getContentSize();
        this.addSendUser(labSize.height + 10, sendusernick);
        if (null != sendusernick && typeof sendusernick == "string") {
            labSize.height = labSize.height + this.m_clipUserName.getContentSize().height + 5;
        }
        this.setContentSize(cc.size(LEN_TABLEVIEW_WIDTH, labSize.height + 15));
    },

    addSendUser : function(posHeight,sendusernick) {
        if (null != sendusernick && typeof sendusernick == "string") {
            if (null == this.m_clipUserName) {
                this.m_clipUserName = ClipText.createClipText(cc.size(200, 20), sendusernick + ".", clientRes.round_body, 20);
                this.m_clipUserName.setTextColor(cc.color(36, 236, 255));
                this.m_clipUserName.setAnchorPoint(cc.p(0, 0.5));
                this.m_clipUserName.setPositionX(-LEN_CHAT_WIDTH * 0.5);
                this.addChild(this.m_clipUserName);
            } else {
                this.m_clipUserName.setString(sendusernick + ".");
            }

            this.m_clipUserName.setPositionY(posHeight + this.m_clipUserName.getContentSize().height * 0.5);
        } else {
            if (null != this.m_clipUserName) {
                this.m_clipUserName.setVisible(false);
            }
        }
    }
});
/*
//表情面板
var BrowPanel = new cc.Node.extend(function () {
   var browPanel = new cc.Node();
   return browPanel;
});*/

/*
GameChatLayer.loadTextChat();
*/

// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new GameChatLayer(this);
//         this.addChild(layer);
//     }
// });