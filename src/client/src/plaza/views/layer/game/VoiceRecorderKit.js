/*Author    :   JIN */


// 语音录音
var VoiceRecorderKit = VoiceRecorderKit || {};
// 录音间隔
VoiceRecorderKit.nInterval = 0;
// 是否在录音
VoiceRecorderKit.bRecordVoice = false;

//var MultiPlatform = appdf.req(appdf.EXTERNAL_SRC + "MultiPlatform");
//var game_cmd = appdf.req(appdf.HEADER_SRC + "CMD_GameServer");
var RECORD_STATE = true;
//var ExternalFun = appdf.req(appdf.EXTERNAL_SRC + "ExternalFun");
var MAX_RECORD_TIME = 20;
var RECORD_SAVE_PATH = /*jsb.fileUtils.getWritablePath()*/ + "/saverec/";

var VoiceRecorderLayer = cc.Layer.extend({
    ctor : function( parent, gameFrame ) {
        this._super();
        this.parent = parent;
        // 加载csb资源
        var rootLayer, csbNode, rc = ExternalFun.loadRootCSB(res.VoiceRecordLayer_json, this);
        rootLayer = rc.rootlayer;
        csbNode = rc.csbnode;
        /*
        this.m_actAni = ExternalFun.loadTimeLine(res.VoiceRecordLayer_json);
        */
        /*
            this.m_actAni.gotoFrameAndPlay(0, true);
            this.runAction(this.m_actAni);
        */

        // 剩余时间
        this.m_txtLeftTips = csbNode.getChildByName("txt_tips");
        this.m_txtLeftTips.setString("剩余录音时间." + MAX_RECORD_TIME + "秒");

        // 移除文件
        // cc.fileUtils().removeFile(RECORD_SAVE_PATH + "record.mp3");
        RECORD_STATE = true;
        // 权限检查
        if (RECORD_STATE) {
            cc.audioEngine.setMusicVolume(0.01);
            // AudioRecorder.getInstance().startRecord("record.mp3");
        }
        this.m_nRecordTime = 0;
        this._gameFrame = gameFrame;
        this.m_bCancelRecord = false;
        VoiceRecorderKit.bRecordVoice = true;

        if (null == this.m_scheduler) {
            // this.m_scheduler = cc.scheduler.scheduleScriptFunc(handler(this, this.onCountDown), 1.0, false);
            this.m_scheduler = new cc.Scheduler(this.onCountDown(this), 1.0, false);
        }
    },

    onCountDown : function(dt) {
        this.m_nRecordTime = this.m_nRecordTime + 1;
        var left = MAX_RECORD_TIME - this.m_nRecordTime;
        this.m_txtLeftTips.setString("剩余录音时间." + left + "秒");
        if (0 == left) {
            this.removeRecorde();
        }
    },


    removeRecorde : function() {
        VoiceRecorderKit.bRecordVoice = false;
        if (null != this.m_scheduler) {
            scheduler.unscheduleScriptEntry(this.m_scheduler);
            this.m_scheduler = null;
        }
        if (this.m_nRecordTime < 1) {
            showToast(this.parent, "录音的时间过短，请重试", 1);
            AudioRecorder.getInstance().cancelRecord();
            cc.audioEngine.setMusicVolume(GlobalUserItem.nMusic / 100.0);
            this.removeFromParent();
            return;
        }

        if (RECORD_STATE) {
            if (!this.m_bCancelRecord) {
                AudioRecorder.getInstance().endRecord();
                cc.audioEngine.setMusicVolume(GlobalUserItem.nMusic / 100.0);

                var tabBuffer = [AudioRecorder.getInstance().createSendBuffer()];
                var nBufferCount = tabBuffer.length;
                cc.log("录音包 ==> " + nBufferCount);
                for (var i = 0; i < nBufferCount; i++) {
                    var buffer = tabBuffer[i];
                    if (null != buffer) {
                        cc.log(" 发送录音 ", i);
                        buffer.setcmdinfo(game_cmd.MDM_GF_FRAME, game_cmd.SUB_GF_USER_VOICE);
                        if (null != this._gameFrame) {
                            this._gameFrame.sendSocketData(buffer);
                        }
                    }
                }
            }
        } else {
            AudioRecorder.getInstance().cancelRecord();
            cc.audioEngine.setMusicVolume(GlobalUserItem.nMusic / 100.0);
        }
        this.removeFromParent();
    },


    cancelVoiceRecord : function() {
        this.bRecordVoice = false;
        if (null != this.m_scheduler) {
            scheduler.unscheduleScriptEntry(this.m_scheduler);
            this.m_scheduler = null;
        }
        this.m_txtLeftTips.setString("取消录音");
        AudioRecorder.getInstance().cancelRecord();
        cc.audioEngine.setMusicVolume(GlobalUserItem.nMusic / 100.0);
        this.m_bCancelRecord = true;
        this.removeFromParent();
    },
});

//全局函数(ios/android端调用)
cc_exports_g_NativeRecord = function (msg) {
    if (msg == "record error") {
        var runScene = cc.director().getRunningScene();
        if (null != runScene) {
            showToastNoFade(runScene, "访问麦克风失败或录音异常,请检查录音权限!", 2);
        }
        RECORD_STATE = false;
    }
};

// var scheduler = cc.director().getScheduler();

VoiceRecorderKit.init = function() {
    // 配置路径
    AudioRecorder.getInstance().init(jsb.fileUtils.getWritablePath() + "/saverec/", jsb.fileUtils.getWritablePath() + "/downrec/");
};

VoiceRecorderKit.createRecorderLayer = function( parent, gameFrame ) {
    // 录音间隔
    var lasttime = VoiceRecorderKit.nInterval;
    //TODO: hour or day? Date().get !!!!! need fixing
    var curtime = new Date();
    if (curtime - lasttime < 2) {
        var runScene = cc.director().getRunningScene();
        if (null != runScene) {
            showToastNoFade(runScene, "录音间隔太短, 请稍后", 1);
        }
        return null;
    }

    // 权限请求 TODO: UserDefault?
    var bRequest = cc.UserDefault.getInstance().getBoolForKey("recordpermissionreq", false);
    if (false == bRequest) {
        cc.log("###2");
        // 尝试请求
        AudioRecorder.getInstance().startRecord("record.mp3");
        AudioRecorder.getInstance().cancelRecord();
        cc.UserDefault.getInstance().setBoolForKey("recordpermissionreq", true);
        VoiceRecorderKit.nInterval = new Date();
        return null;
    }

    cc.log("###3");
    // 权限检查(ios端有效)
    if (false == MultiPlatform.getInstance().checkRecordPermission()) {
        var runScene = cc.director().getRunningScene();
        if (null != runScene) {
            showToastNoFade(runScene, "当前未获得麦克风权限,无法进行语音聊天!", 2);
        }
        VoiceRecorderKit.nInterval = new Date();
        return null;
    }
    cc.log("###4");
    VoiceRecorderKit.nInterval = new Date();
    return VoiceRecorderLayer.create(parent, gameFrame);
};

/*TODO:
// VoiceRecorderKit.init();*/

VoiceRecorderKit.startPlayVoice = function( spath ) {
    cc.audioEngine.setMusicVolume(0.01);
    cc.audioEngine.setVoiceVolume(1.0);
    return cc.audioEngine.playVoice(spath);
}

VoiceRecorderKit.finishPlayVoice = function() {
    if (!VoiceRecorderKit.bRecordVoice) {
        cc.audioEngine.setMusicVolume(GlobalUserItem.nMusic / 100.0);
        cc.audioEngine.setVoiceVolume(0);
    }
}

// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new VoiceRecorderLayer(this);
//         this.addChild(layer);
//     }
// });
