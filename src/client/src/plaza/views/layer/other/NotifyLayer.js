// var ExternalFun = appdf.req(appdf.EXTERNAL_SRC + "ExternalFun");
// var WebViewLayer = appdf.CLIENT_SRC + "plaza.views.layer.plaza.WebViewLayer";
// appdf.req(appdf.CLIENT_SRC+"plaza.models.FriendMgr");
// var NotifyMgr = appdf.req(appdf.EXTERNAL_SRC + "NotifyMgr");
// var ModifyFrame = appdf.req(appdf.CLIENT_SRC+"plaza.models.ModifyFrame");
// var MultiPlatform = appdf.req(appdf.EXTERNAL_SRC + "MultiPlatform");

var g_var = ExternalFun.req_var;

var NoticeLayer = cc.Layer.extend({
    BT_EXIT			: 1,
    PRO_WIDTH		: yl.WIDTH,
    ctor : function (scene) {
        this._super();
        cc.log("NoticeLayer created!");
        this._scene = scene;
        this.setContentSize(yl.WIDTH, yl.HEIGHT);

        //加载csb资源
        var rootLayer, csbNode, rc = ExternalFun.loadRootCSB(res.noticeScene_json, this);
        rootLayer = rc.rootlayer;
        csbNode = rc.csbnode;
        this.m_csbNode = csbNode;

        var self = this;
        //按钮事件
        var btncallback = function (ref, type) {
            if (type == ccui.TOUCH_ENDED) {
                that.onButtonClickedEvent(ref.getTag(), ref);
            }
        };

        //容器层
        var panel_1 = csbNode.getChildByName("panel_1");

        //退出按钮
        this.m_btClose = panel_1.getChildByName("bt_close");
        this.m_btClose.setTag(NoticeLayer.BT_EXIT);
        this.m_btClose.addTouchEventListener(btncallback);

        //初始化列表内容
        this.setNoticeFormSystemNotice();
        var content = this.m_csbNode.getChildByName("panel_noticeTxt");
        // 滑动列表
        var tableView = new cc.TableView(this, content.getContentSize());
        tableView.setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL);
        tableView.setPosition(content.getPosition());
        //tableView.setPosition(cc.p(content.getPositionX(),content.getPositionY()))
        tableView.setDelegate();
        /*tableView.registerScriptHandler(this.cellSizeForTable, cc.TABLECELL_SIZE_FOR_INDEX);
        tableView.registerScriptHandler(handler(this, this.tableCellAtIndex), cc.TABLECELL_SIZE_AT_INDEX);
        tableView.registerScriptHandler(handler(this, this.numberOfCellsInTableView), cc.NUMBER_OF_CELLS_IN_TABLEVIEW);*/
        this.m_csbNode.addChild(tableView);
        this.m_tableView = tableView;
        content.removeFromParent();
        // this.m_tableView.reloadData();
    },

    onButtonClickedEvent : function ( tag, sender ) {
        cc.log("onButtonClickEvent called!!!");
        if (NoticeLayer.BT_EXIT == tag) {
            this._scene.onKeyBack();
        } else {
            cc.log("shareLayer按钮点击事件报错");
        }
    },

    cellSizeForTable : function (view, idx) {
        return 800, 80;
    },

    tableCellAtIndex : function (view, idx) {
        var cell = view.dequeueCell();
        if (!cell)
            cell = new cc.TableViewCell();
        else
            cell.removeAllChildren();


        var tabData = {};
        tabData.strValue = this.m_strChatVoiceGame[idx + 1];
        tabData.tag = idx + 1;
        var item = this.createRecordItem(tabData);
        item.setPosition(view.getViewSize().width * 0.5, 25);
        cell.addChild(item);

        return cell;
    },

    numberOfCellsInTableView : function (view) {
        return this.m_strChatVoiceGame.length;
    },
    // 创建记录
    createRecordItem : function (tabData) {

        var item = new ccui.Widget();
        item.setContentSize(cc.size(800, 80));
        item.setTag(tabData.tag);

        // 聊天内容
        var str = tabData.strValue;
        var strValue = new cc.LabelTTF(str, "fonts/round_body.ttf", 30);
        strValue.setColor(cc.color(124, 74, 20));
        strValue.setAnchorPoint(0, 0.5);
        item.addChild(strValue);
        strValue.setPosition(20, 40);

        return item;
    },

   setNoticeFormSystemNotice : function () {
        this.m_strChatVoiceGame = {};
       /*
        this.m_strChatVoiceGame = this._scene.getNoticeListToNoticeLayer();*/

    },
    
    tableCellSizeForIndex : function () {
        
    }

});

    // var testScene = cc.Scene.extend({
    //     onEnter : function () {
    //         this._super();
    //         var layer = new NoticeLayer(this);
    //         this.addChild(layer);
    //     }
    // });
