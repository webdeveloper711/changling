

// var ExternalFun = appdf.req(appdf.EXTERNAL_SRC + "ExternalFun");
// var g_var = ExternalFun.req_var;
// var WebViewLayer = appdf.CLIENT_SRC + "plaza.views.layer.plaza.WebViewLayer";
// appdf.req(appdf.CLIENT_SRC+"plaza.models.FriendMgr");
// var NotifyMgr = appdf.req(appdf.EXTERNAL_SRC + "NotifyMgr");
// var ModifyFrame = appdf.req(appdf.CLIENT_SRC+"plaza.models.ModifyFrame");

var TAG_MASK = 101;
var BTN_CLOSE = 102;

var OptionLayer = cc.Layer.extend({

    ctor : function (scene) {
        this._super(cc.color(0,0,0,0));
        this._scene = scene;
        cc.log("create OptionLayer");
       this.setContentSize(yl.WIDTH, yl.HEIGHT);
        var self = this;
        var cbtlistener = function (sender, eventType) {
            self.onSelectedEvent(sender.getTag(), sender, eventType);
        };

        var btcallback = function (ref, type) {
            if (type == ccui.Widget.TOUCH_ENDED){
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };

        var areaWidth = yl.WIDTH;
        var areaHeight = yl.HEIGHT;
        cc.spriteFrameCache.addSpriteFrames(res.option_background_png);
        //黑色背景
        var frame = cc.spriteFrameCache.getSpriteFrame(res.option_background_png);
        if (null != frame) {
            var sp = cc.Sprite(frame);
            sp.setPosition(yl.WIDTH / 2, yl.HEIGHT / 2);
            this.addChild(sp);
            sp.setVisible(false)
        }

        //显示灰色背景
        var s1 = new cc.Sprite(res.option_background_png);
        s1.setPosition(yl.WIDTH / 2, yl.HEIGHT / 2);
        this.addChild(s1);
        //显示背景
        var s2 = new cc.Sprite(res.bg_option_png);
        s2.setPosition(yl.WIDTH / 2, yl.HEIGHT / 2);
        this.addChild(s2);

        //返回
        var b1 = new ccui.Button(res.option_bt_close_1_png, res.option_bt_close_2_png);
        b1.setPosition(yl.WIDTH * 0.8, yl.HEIGHT * 0.72);
        b1.setTag(OptionLayer.BT_EXIT);
        this.addChild(b1);
        b1.addTouchEventListener(btcallback);

        //音效开关
        var offset = cc.p(250, 90);

        this._cbtSilence = new ccui.CheckBox(res.option_bt_option_switch_0_png,res.option_bt_option_switch_1_png, res.option_bt_option_switch_1_png,  "", "");
        this._cbtSilence.setPosition(350 + offset.x, 310);
        this._cbtSilence.setSelected(GlobalUserItem.bVoiceAble);
        this.addChild(this._cbtSilence);
        this._cbtSilence.setTag(OptionLayer.CBT_SILENCE);
        this._cbtSilence.addEventListener(cbtlistener);

        //音乐开关

        this._cbtSound = new ccui.CheckBox(res.option_bt_option_switch_1_0_png, res.option_bt_option_switch_1_1_png, res.option_bt_option_switch_1_1_png,  "", "");
        this._cbtSound.setPosition(350 + offset.x, 410);
        this._cbtSound.setSelected(GlobalUserItem.bSoundAble);
        this.addChild(this._cbtSound);
        this._cbtSound.setTag(OptionLayer.CBT_SOUND);
        this._cbtSound.addEventListener(cbtlistener);


        //帐号信息
        var s3 = new cc.Sprite(res.option_frame_option_1_png);
        s3.setPosition(yl.WIDTH / 2, 166);
        this.addChild(s3);
        s3.setVisible(false);

        var s4 = new cc.Sprite(res.option_text_account_png);
        s4.setPosition(240, 166);
        this.addChild(s4);
        s4.setVisible(false);

        var testen = new cc.LabelTTF("A", "Arial", 24);
        this._enSize = testen.getContentSize().width;
        var testcn = new cc.LabelTTF("游", "Arial", 24);
        this._cnSize = testcn.getContentSize().width;
        // this._nickname = new cc.LabelTTF(stringEllipsis(GlobalUserItem.szNickName, this._enSize, this._cnSize, 200), res.round_body_ttf, 24);
        this._nickname = new cc.LabelTTF(GlobalUserItem.szNickName, res.round_body_ttf, 24);
        this._nickname.setPosition(435, 166);
        this._nickname.setVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
        this._nickname.setAnchorPoint(cc.p(0.5, 0.5));
        this._nickname.setContentSize(210, 25);
        // this._nickname.setHeight(25);
        // this._nickname.setLineBreakWithoutSpace(false);
        this._nickname.setColor(cc.color(240, 240, 240, 255));
        this.addChild(this._nickname);
        this._nickname.setVisible(false);
        //切换帐号
        var b2 = new ccui.Button(res.option_bt_option_change_0_png, res.option_bt_option_change_1_png);
        b2.setPosition(yl.WIDTH * 0.65, yl.HEIGHT * 0.42);
        b2.setTag(OptionLayer.BT_EXCHANGE);
        this.addChild(b2);
        b2.addTouchEventListener(btcallback);

        // var mgr = this._scene.getApp().getVersionMgr();
        var verstr = _version.getResVersion() || "0";

        // 版本号
        var lb = new cc.LabelTTF("版本号." + appdf.BASE_C_VERSION + "." + verstr, res.round_body_ttf, 24);
        lb.setPosition(yl.WIDTH, 0);
        lb.setAnchorPoint(cc.p(1, 0));
        this.addChild(lb);
    },

    onSelectedEvent : function (tag,sender,eventType){
        cc.log("change selected state!!!");
        if (tag == OptionLayer.CBT_SILENCE) {
            GlobalUserItem.setVoiceAble(eventType == 0);
        } else if (tag == OptionLayer.CBT_SOUND) {
            GlobalUserItem.setSoundAble(eventType == 0);
            //背景音乐
            ExternalFun.playPlazzBackgroudAudio();
        }
    },

    //按键监听
    onButtonClickedEvent : function (tag,sender) {
        cc.log("onBUttonClickedEvent called!!!");
        if (tag != OptionLayer.BT_EXCHANGE || tag != OptionLayer.BT_EXIT) {
            if (GlobalUserItem.isAngentAccount()) {
                return;
            }
        }

        if (tag == OptionLayer.BT_EXCHANGE) {
            this._scene.ExitClient();
        } else if (tag == OptionLayer.BT_EXIT) {
            this._scene.onKeyBack();
        } else if (tag == OptionLayer.BT_QUESTION) {
            this._scene.onChangeShowMode(yl.SCENE_FAQ);
        } else if (tag == OptionLayer.BT_MODIFY) {
            if (this._scene._gameFrame.isSocketServer()) {
                showToast(this, "当前页面无法使用此功能！", 1);
                return;
            }
            this._scene.onChangeShowMode(yl.SCENE_MODIFY);
        } else if (tag == OptionLayer.BT_COMMIT) {
            this._scene.onChangeShowMode(yl.SCENE_FEEDBACK);
        } else if (tag == OptionLayer.BT_LOCK) {
            cc.log("锁定机器");
            this.showLockMachineLayer(this);
        } else if (tag == OptionLayer.BT_UNLOCK) {
            cc.log("解锁机器");
            this.showLockMachineLayer(this);
        }
    },

    showLockMachineLayer : function ( parent ) {
        var self = this;
        if (null == parent)
            return;
        //网络回调
        var modifyCallBack = function (result, message) {
            this.onModifyCallBack(result, message);
        };
        //网络处理
        this._modifyFrame = new ModifyFrame(this, modifyCallBack);

        // 加载csb资源
        var csbNode = ExternalFun.loadCSB("Option/LockMachineLayer.csb", parent);

        var touchFunC = function (ref, tType) {
            if (tType == ccui.TOUCH_ENDED) {
                var tag = ref.getTag();
                if (TAG_MASK == tag || BTN_CLOSE == tag) {
                    csbNode.removeFromParent();
                } else if (OptionLayer.BT_LOCK == tag) {
                    var txt = csbNode.m_editbox.string;
                    if (txt == "") {
                        showToast(self, "密码不能为空!", 2);
                        return;
                    }
                    self._modifyFrame.onBindingMachine(1, txt);
                    csbNode.removeFromParent();
                } else if (OptionLayer.BT_UNLOCK == tag) {
                    var txt = csbNode.m_editbox.string;
                    if (txt == "") {
                        showToast(self, "密码不能为空!", 2);
                        return;
                    }
                    self._modifyFrame.onBindingMachine(0, txt);
                    csbNode.removeFromParent();
                }
            }
        };

        // 遮罩
        var mask = csbNode.getChildByName("panel_mask");
        mask.setTag(TAG_MASK);
        mask.addChilduchEventListener(touchFunC);

        var image_bg = csbNode.getChildByName("image_bg");
        image_bg.setSwallowTouches(true);

        // 输入
        var tmp = image_bg.getChildByName("sp_lockmachine_bankpw");
        var editbox = new cc.EditBox(cc.size(tmp.getContentSize().width - 10, tmp.getContentSize().height - 10), "blank.png", UI_TEX_TYPE_PLIST);
        editbox.setPosition(tmp.getPosition());
        editbox.setFontName(res.round_body_ttf);
        editbox.setPlaceholderFontName(res.round_body_ttf);
        editbox.setFontSize(30);
        editbox.setPlaceholderFontSize(30);
        editbox.setMaxLength(32);
        editbox.setInputFlag(cc.EDITBOX_INPUT_FLAG_PASSWORD);
        editbox.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        editbox.setPlaceHolder("请输入密码");
        image_bg.addChild(editbox);
        csbNode.m_editbox = editbox;

        // 锁定/解锁
        var btn = image_bg.getChildByName("btn_lock");
        btn.setTag(OptionLayer.BT_LOCK);
        btn.addChilduchEventListener(touchFunC);
        var normal = "Option/btn_lockmachine_0.png";
        var disable = "Option/btn_lockmachine_0.png";
        var press = "Option/btn_lockmachine_1.png";
        if (1 == GlobalUserItem.cbLockMachine) {
            btn.setTag(OptionLayer.BT_UNLOCK);
            normal = "Option/btn_unlockmachine_0.png";
            disable = "Option/btn_unlockmachine_0.png";
            press = "Option/btn_unlockmachine_1.png";
        }
        btn.loadTextureDisabled(disable);
        btn.loadTextureNormal(normal);
        btn.loadTexturePressed(press);

        btn = image_bg.getChildByName("btn_cancel");
        btn.setTag(BTN_CLOSE);
        btn.addChilduchEventListener(touchFunC);

        // 关闭
        btn = image_bg.getChildByName("btn_close");
        btn.setTag(BTN_CLOSE);
        btn.addChilduchEventListener(touchFunC);
    },

    onModifyCallBack : function (result, tips) {
        if (typeof(tips) == "string" && "" != tips) {
            showToast(this, tips, 2);
        }

        var normal = "Option/btn_lockmachine_0.png";
        var disable = "Option/btn_lockmachine_0.png";
        var press = "Option/btn_lockmachine_1.png";
        if (this._modifyFrame.BIND_MACHINE == result) {
            if (0 == GlobalUserItem.cbLockMachine) {
                GlobalUserItem.cbLockMachine = 1;
                this.m_btnLock.setTag(OptionLayer.BT_UNLOCK);
                normal = "Option/btn_unlockmachine_0.png";
                disable = "Option/btn_unlockmachine_0.png";
                press = "Option/btn_unlockmachine_1.png";
            } else {
                GlobalUserItem.cbLockMachine = 0;
                this.m_btnLock.setTag(OptionLayer.BT_LOCK);
            }
        }
        this.m_btnLock.loadTextureDisabled(disable);
        this.m_btnLock.loadTextureNormal(normal);
        this.m_btnLock.loadTexturePressed(press);
    }

});

OptionLayer.CBT_SILENCE 	= 1;
OptionLayer.CBT_SOUND   	= 2;
OptionLayer.BT_EXIT			= 7;

OptionLayer.BT_QUESTION		= 8;
OptionLayer.BT_COMMIT		= 9;
OptionLayer.BT_MODIFY		= 10;
OptionLayer.BT_EXCHANGE		= 11;
OptionLayer.BT_LOCK         = 12;
OptionLayer.BT_UNLOCK       = 13;

OptionLayer.PRO_WIDTH		= yl.WIDTH;
// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new OptionLayer(this);
//         this.addChild(layer);
//     }
// });
