/*
* TODO: when connect with parent, need fix!!!!
* !
* */
var RichLabel = cc.Node.extend({
    ctor : function (params) {
        this._super();
        this.init_(params);
    },

    __index   : RichLabel,
    _fontName : null,
    _fontSize : null,
    _fontColor : null,
    _containLayer : null, //装载layer
    _spriteArray : null, //精灵数组
    _textStr : null,
    _maxWidth : null,
    _maxHeight : null,

    //播放状态 1 表示未开始 2 表示播放中 3 表示已经播放完毕
    _labelStatus : 1,

    //设置text
    setLabelString : function (text) {
        if (this._textStr == text) {
            return; //相同则忽略
        }

        if (this._textStr) { //删除之前的string
            this._spriteArray = null;
            this._containLayer.removeAllChildren();
        }

        this._labelStatus = 1; //未开始
        this._textStr = text;

        //转化好的数组
        var parseArray = this.parseString_(text);

        //将字符串拆分成一个个字符
        this.formatString_(parseArray);

        //创建精灵
        var spriteArray = this.createSprite_(parseArray);
        this._spriteArray = spriteArray;

        this.adjustPosition_();
    },

    init_ : function (params) {
        //如果text的格式指定字体则使用指定字体，否则使用默认字体
        //大小和颜色同理
        var fontName = params.fontName || "Arial"; //默认字体
        var fontSize = params.fontSize || 30; //默认大小
        var fontColor = params.fontColor || cc.color(255, 255, 255);//默认白色
        var dimensions = params.dimensions || cc.size(0, 0); //默认无限扩展，即沿着x轴往右扩展
        var text = params.text;

        //装文字和图片精灵
        var containLayer = new cc.Layer();
        this.addChild(containLayer);

        this._fontName = fontName;
        this._fontSize = fontSize;
        this._fontColor = fontColor;
        this._dimensions = dimensions;
        this._containLayer = containLayer;

        this.setLabelString(text);
    },

    // //文字解析，按照顺序转换成数组，每个数组对应特定的标签
    parseString_ : function(str){
    	var clumpheadTab = []; // 标签头
    	//作用，取出所有格式为[xxxx]的标签头
    	for (w in str.match("%b[]")) {
            if (w.substring(2, 2) != "/") {// 去尾
                clumpheadTab.push(w);
                // table.insert(clumpheadTab, w)
            }
        }

    	// 解析标签
    	var totalTab = [];
    	for (k in (clumpheadTab)) {
    		var tab = [];
    		var tStr;
    		ns = clumpheadTab[k];
    		// 第一个等号前为块标签名
    		ns.substring(ns.replace(2, ns.length-1), function (w) {
                var n = w.indexOf("=");
                if (n > -1) {
                    var temTab = w.split(" "); // 支持标签内嵌
                    for (k in pairs(temTab)) {
                        var pstr = temTab[k];
                        var temtab1 = pstr.split("=");

                        var pname = temtab1[0];

                        if (k == 0) {
                            tStr = pname;
                        } // 标签头

                        var js = temtab1[2];

                        var p = js.indexOf("[^%d.]");

                        if (p > -1) {
                            js = tonumber(js);
                        }
                        /*TODO: need change
    					var switchState = {
    						["fontColor"] = function() {
                                tab["fontColor"] = this.convertColor_(js)
                            },
    					} //switch end

    					var fSwitch = switchState[pname]; //switch 方法

    					//存在switch
    					if (fSwitch) {
    						//目前只是颜色需要转换
    						var result = fSwitch(); //执行function
    					else { //没有枚举
    						tab[pname] = js;
    						return;
    					}*/
                    }
                }
            });
    		if (tStr) {
    			// 取出文本
    			var beginFind = str.indexOf("%[%/" + tStr + "%]");
    			var endFind = str.lastIndexOf("%[%/" + tStr + "%]");
    			var endNumber = beginFind-1;
    			var gs = str.substring(ns.length + 1, endNumber);
    			if (gs.indexOf("%[")) {
                    tab["text"] = gs;
                } else {
                    str.replace(gs, function (w) {
                        tab.text = w;
                    })
                }
    			// 截掉已经解析的字符
    			str = str.substring(endFind+1, str.length);
                totalTab.push(tab);
            }
        }
    	// 普通格式label显示
    	if (clumpheadTab.length == 0) {
            var ptab = {};
            ptab.text = str;
            totalTab.push(ptab);
        }
    	return totalTab;
    },

    // //将字符串转换成一个个字符
    formatString_ : function(parseArray) {
        for (i in parseArray) {
            var dic = parseArray[i];
            var text = dic.text;
            if (text) {
                var textArr = text;
                dic.textArray = textArr;
            }
        }
    },


    //创建精灵
    createSprite_ : function (parseArray) {
        cc.log("createSprite called!!!");
        var spriteArray = [];
        for (i in parseArray) {
            var dic = parseArray[i];
            var textArr = dic.textArray;
            if (textArr.length > 0) { //创建文字
                var fontName = dic.fontName || this._fontName;
                var fontSize = dic.fontSize || this._fontSize;
                var fontColor = dic.fontColor || this._fontColor;
                cc.log("********************************." + fontName);
                for (j in textArr) {
                    var word = textArr[j];
                    var label = new cc.LabelTTF(word, fontName, fontSize);
                    label.setColor(fontColor);
                    spriteArray[spriteArray.length + 1] = label;
                    this._containLayer.addChild(label);
                }
            } else if (dic.image) {
                var sprite = new cc.Sprite(dic.image);
                var scale = dic.scale || 1;
                sprite.setScale(scale);
                spriteArray[spriteArray.length + 1] = sprite;
                this._containLayer.addChild(sprite);
            } else {
                cc.log("error : not define");
            }
        }

        return spriteArray;
    },

    //调整位置（设置文字和尺寸都会触发此方法）
    adjustPosition_ : function () {

        var spriteArray = this._spriteArray;

        if (!spriteArray)//还没创建
            return;

        //获得每个精灵的宽度和高度
        var wh = this.getSizeOfSprites_(spriteArray);

        var widthArr = wh[0];
        var heightArr = wh[1];
        //获得每个精灵的坐标
        var  pxy = this.getPointOfSprite_(widthArr, heightArr, this._dimensions);
        var pointArrX = pxy[0]; var  pointArrY = pxy[1];
        for (i in spriteArray) {
            var sprite = spriteArray[i];
            sprite.setPosition(pointArrX[i], pointArrY[i]);
        }
    },

    //获得每个精灵的尺寸
    getSizeOfSprites_ : function (spriteArray) {
        var widthArr = []; //宽度数组
        var heightArr = []; //高度数组

        //精灵的尺寸
        for (i = 0; i < spriteArray.length; i++) {
            var sprite = spriteArray[i];
            var contentSize = sprite.getContentSize();
            var rect = sprite.getBoundingBox();
            widthArr[i] = rect.width;
            heightArr[i] = rect.height;
        }
        return [widthArr, heightArr];
    },


    //获得每个精灵的位置
    getPointOfSprite_ : function (widthArr, heightArr, dimensions) {
        var totalWidth = dimensions.width;
        var totalHight = dimensions.height;

        var maxWidth = 0;
        var maxHeight = 0;

        var spriteNum = widthArr.length;

        //从左往右，从上往下拓展
        var curX = 0; //当前x坐标偏移

        var curIndexX = 1; //当前横轴index
        var curIndexY = 1; //当前纵轴index

        var pointArrX = []; //每个精灵的x坐标

        var rowIndexArr = []; //行数组，以行为index储存精灵组
        var indexArrY = []; //每个精灵的行index

        //计算宽度，并自动换行
        for (i in widthArr) {
            var spriteWidth = widthArr[i];
            var nexX = curX + spriteWidth;
            var pointX;
            var rowIndex = curIndexY;

            var halfWidth = spriteWidth * 0.5;
            if (nexX > totalWidth && totalWidth != 0) { //超出界限了
                pointX = halfWidth;
                if (curIndexX == 1) { //当前是第一个，
                    curX = 0;// 重置x
                } else { //不是第一个，当前行已经不足容纳
                    rowIndex = curIndexY + 1; //换行
                    curX = spriteWidth;
                }
                curIndexX = 1; //x坐标重置
                curIndexY = curIndexY + 1; //y坐标自增
            } else {
                pointX = curX + halfWidth; //精灵坐标x
                curX = pointX + halfWidth; //精灵最右侧坐标
                curIndexX = curIndexX + 1;
            }
            pointArrX[i] = pointX; //保存每个精灵的x坐标

            indexArrY[i] = rowIndex; //保存每个精灵的行

            var tmpIndexArr = rowIndexArr[rowIndex];

            if (!tmpIndexArr) { //没有就创建
                tmpIndexArr = [];
                rowIndexArr[rowIndex] = tmpIndexArr;
            }
            tmpIndexArr[tmpIndexArr.length] = i; //保存相同行对应的精灵

            if (curX > maxWidth) {
                maxWidth = curX;
            }
        }

        var curY = 0;
        var rowHeightArr = []; //每一行的y坐标

        //计算每一行的高度
        for (i in rowIndexArr) {
            var rowInfo = rowIndexArr[i];
            var rowHeight = 0;
            for (j in rowInfo) { //计算最高的精灵
                var index = rowInfo[j];
                var height = heightArr[index];
                if (height > rowHeight) {
                    rowHeight = height;
                }
            }
            var pointY = curY + rowHeight * 0.5; //当前行所有精灵的y坐标（正数，未取反）
            rowHeightArr[rowHeightArr.length] = -pointY; //从左往右，从上到下扩展，所以是负数
            curY = curY + rowHeight; //当前行的边缘坐标（正数）

            if (curY > maxHeight) {
                maxHeight = curY;
            }
        }

        this._maxWidth = maxWidth;
        this._maxHeight = maxHeight;

        var pointArrY = [];

        for (i = 0; i < spriteNum; i++) {
            var indexY = indexArrY[i]; //y坐标是先读取精灵的行，然后再找出该行对应的坐标
            var pointY = rowHeightArr[indexY];
            pointArrY[i] = pointY;
        }

        return [pointArrX, pointArrY];
    },

    //设置尺寸
    setDimensions : function (dimensions) {
        this._containLayer.setContentSize(dimensions);
        this._dimensions = dimensions;

        this.adjustPosition_();
    },

    //获得label尺寸
    getLabelSize : function () {
        var width = this._maxWidth || 0;
        var height = this._maxHeight || 0;
        var ccsize = new cc.size(width, height);
        return ccsize;
    },

    //是否在播放动画
    isRunningAmim : function () {
        var isRunning = false;
        if (this._labelStatus == 2) {
            isRunning = true;
        }
        return isRunning;
    },




});



//
// //文字解析，按照顺序转换成数组，每个数组对应特定的标签
// function RichLabel.parseString_(str)
// 	var clumpheadTab = {} // 标签头
// 	//作用，取出所有格式为[xxxx]的标签头
// 	for w in string.gfind(str, "%b[]") do
// 		if  string.sub(w,2,2) ~= "/" then// 去尾
// 			table.insert(clumpheadTab, w)
// 		end
// 	end
//
// 	// 解析标签
// 	var totalTab = {}
// 	for k,ns in pairs(clumpheadTab) do
// 		var tab = {}
// 		var tStr
// 		// 第一个等号前为块标签名
// 		string.gsub(ns, string.sub(ns, 2, #ns-1), function (w)
// 			var n = string.find(w, "=")
// 			if n then
// 				var temTab = this.stringSplit_(w, " ") // 支持标签内嵌
// 				for k,pstr in pairs(temTab) do
// 					var temtab1 = this.stringSplit_(pstr, "=")
//
// 					var pname = temtab1[1]
//
// 					if k == 1 then
// 						tStr = pname
// 					end // 标签头
//
// 					var js = temtab1[2]
//
// 					var p = string.find(js, "[^%d.]")
//
//         			if not p then
//         				js = tonumber(js)
//         			end
//
// 					var switchState = {
// 						["fontColor"]	 = function()
// 							tab["fontColor"] = this.convertColor_(js)
// 						end,
// 					} //switch end
//
// 					var fSwitch = switchState[pname] //switch 方法
//
// 					//存在switch
// 					if fSwitch then
// 						//目前只是颜色需要转换
// 						var result = fSwitch() //执行function
// 					else //没有枚举
// 						tab[pname] = js
// 						return
// 					end
// 				end
// 			end
// 		end)
// 		if tStr then
// 			// 取出文本
// 			var beginFind,endFind = string.find(str, "%[%/"..tStr.."%]")
// 			var endNumber = beginFind-1
// 			var gs = string.sub(str, #ns+1, endNumber)
// 			if string.find(gs, "%[") then
// 				tab["text"] = gs
// 			else
// 				string.gsub(str, gs, function (w)
// 					tab["text"] = w
// 				end)
// 			end
// 			// 截掉已经解析的字符
// 			str = string.sub(str, endFind+1, #str)
// 			table.insert(totalTab, tab)
// 		end
// 	end
// 	// 普通格式label显示
// 	if table.nums(clumpheadTab) == 0 then
// 		var ptab = {}
// 		ptab.text = str
// 		table.insert(totalTab, ptab)
// 	end
// 	return totalTab
// end
//
//
// //[[解析16进制颜色rgb值]]
// function  RichLabel.convertColor_(xStr)
//     var function toTen(v)
//         return tonumber("0x" .. v)
//     end
//
//     var b = string.sub(xStr, -2, -1)
//     var g = string.sub(xStr, -4, -3)
//     var r = string.sub(xStr, -6, -5)
//
//     var red = toTen(r) or this._fontColor.r
//     var green = toTen(g) or this._fontColor.g
//     var blue = toTen(b) or this._fontColor.b
//     return cc.c3b(red, green, blue)
// end
//
// // string.split()
// function RichLabel.stringSplit_(str, flag)
// 	var tab = {}
// 	while true do
// 		var n = string.find(str, flag)
// 		if n then
// 			var first = string.sub(str, 1, n-1)
// 			str = string.sub(str, n+1, #str)
// 			table.insert(tab, first)
// 		else
// 			table.insert(tab, str)
// 			break
// 		end
// 	end
// 	return tab
// end
//
// // 拆分出单个字符
// function RichLabel.stringToChar_(str)
//     var list = {}
//     var len = string.len(str)
//     var i = 1
//     while i <= len do
//         var c = string.byte(str, i)
//         var shift = 1
//         if c > 0 and c <= 127 then
//             shift = 1
//         elseif (c >= 192 and c <= 223) then
//             shift = 2
//         elseif (c >= 224 and c <= 239) then
//             shift = 3
//         elseif (c >= 240 and c <= 247) then
//             shift = 4
//         end
//         var char = string.sub(str, i, i+shift-1)
//         i = i + shift
//         table.insert(list, char)
//     end
// 	return list, len
// end
//
// return RichLabel


// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new RichLabel({text:"Test Text!!!"});
//         this.addChild(layer);
//     }
// });