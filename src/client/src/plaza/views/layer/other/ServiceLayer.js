/*
    author : Kil
    date   : 2017-11-20
 */

var ServiceLayer = cc.Layer.extend({

    ctor : function () {
        this._super();
        this.setContentSize(yl.WIDTH, yl.HEIGHT);
        var self = this;
        cc.spriteFrameCache.addSpriteFrames(res.public_plist);

        var  btcallback = function(sender, target) {
            if (target == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(sender.getTag(), sender);
            }
        };

        var areaWidth = yl.WIDTH;
        var areaHeight = yl.HEIGHT;

        //背景
        var background = new cc.Sprite(res.background_png);
        background.setPosition(yl.WIDTH/2, yl.HEIGHT/2);
        this.addChild(background);

        //标题
        var title = new cc.Sprite(res.title_service_png);
        title.setPosition(yl.WIDTH/2, yl.HEIGHT-51);
        this.addChild(title);

        //返回
        var btnBack = new ccui.Button(res.btn_0_return_png, res.btn_1_return_png);
        btnBack.setTag(ServiceLayer.BT_EXIT);
        btnBack.setPosition(1250, yl.HEIGHT - 60);
        this.addChild(btnBack);
        btnBack.addTouchEventListener(btcallback);

        // 背景
        var frame = cc.spriteFrameCache.getSpriteFrame("sp_background_service.png");
        if (frame != null) {
            sp = new cc.Sprite(frame);
            sp.setPosition(yl.WIDTH/2, 346);
            this.addChild(sp,2);
        }

        // 读取文本
        this._scrollView = new ccui.ScrollView();
        this._scrollView.setContentSize(cc.size(1130, 560));
        this._scrollView.setAnchorPoint(cc.p(0.5, 0.5));
        this._scrollView.setPosition(cc.p(areaWidth/2 , 345));
        this._scrollView.setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL);
        this._scrollView.setBounceEnabled(true);
        this._scrollView.setScrollBarEnabled(false);
        this.addChild(this._scrollView,2);

        var str = cc.loader.getRes(res.Service_txt);
        this._strLabel = new cc.LabelTTF(str, res.round_body_ttf, 22);
        this._strLabel.setAnchorPoint(cc.p(0.5, 0));
        // this._strLabel.setLineBreakWithoutSpace(true);
        this._strLabel.boundingWidth = 1100;
        this._strLabel.textAlign = cc.TEXT_ALIGNMENT_LEFT;
        this._strLabel.setColor(cc.color(7,44,89,255));
        this._scrollView.addChild(this._strLabel);
        this._strLabel.setPosition(cc.p(1130/2, 0));
        this._scrollView.setInnerContainerSize(cc.size(1130, this._strLabel.getContentSize().height));

        return true;
    },

    onButtonClickedEvent : function(tag,sender) {
        if (tag == ServiceLayer.BT_EXIT) {
            cc.log("bt exit");
            if (this.getParent().getParent()._registerView != null)
                this.getParent().getParent().onShowRegister();
            else
                this.getParent().getParent().onShowLogon();
        } else if (tag == ServiceLayer.BT_CONFIRM) {
            if (this.getParent().getParent()._registerView != null) {
                this.getParent().getParent()._registerView.setAgreement(true);
                this.getParent().getParent().onShowRegister();
            } else
                this.getParent().getParent().onShowLogon();
        } else if (tag == ServiceLayer.BT_CANCEL) {
            if (this.getParent().getParent()._registerView != null) {
                this.getParent().getParent()._registerView.setAgreement(false);
                this.getParent().getParent().onShowRegister();
            } else {
                this.getParent().getParent().onShowLogon();

            }
        }
    }
    
});
ServiceLayer.BT_EXIT		= 5;

ServiceLayer.BT_CONFIRM		= 8;
ServiceLayer.BT_CANCEL		= 9;
//
// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new ServiceLayer();
//         this.addChild(layer);
//     }
// });