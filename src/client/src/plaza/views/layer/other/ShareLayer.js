// var ExternalFun = appdf.req(appdf.EXTERNAL_SRC + "ExternalFun");

// var WebViewLayer = appdf.CLIENT_SRC + "plaza.views.layer.plaza.WebViewLayer";
// appdf.req(appdf.CLIENT_SRC+"plaza.models.FriendMgr");
// var NotifyMgr = appdf.req(appdf.EXTERNAL_SRC + "NotifyMgr");
// var ModifyFrame = appdf.req(appdf.CLIENT_SRC+"plaza.models.ModifyFrame");
// var MultiPlatform = appdf.req(appdf.EXTERNAL_SRC + "MultiPlatform");

var g_var = ExternalFun.req_var;
var ShareLayer = cc.Layer.extend({
    BT_SHARE   	    : 1,
    BT_EXIT			: 2,
    BT_SHARE_FRIEND : 3,
    PRO_WIDTH		: yl.WIDTH,
    ctor : function (scene) {
        this._super();
        this._scene = scene;
        this.setContentSize(yl.WIDTH, yl.HEIGHT);

        var areaWidth = yl.WIDTH;
        var areaHeight = yl.HEIGHT;

        //加载csb资源
        var rc = ExternalFun.loadRootCSB(res.shartLayer_json, this);
        var rootLayer = rc.rootlayer;
        var csbNode = rc.csbnode;
        this.m_csbNode = csbNode;

        //按钮事件
        var self = this;
        var btncallback = function (ref, type) {
            if (type == ccui.Widget.TOUCH_ENDED)
                self.onButtonClickedEvent(ref.getTag(), ref)
        };

        //容器层
        var pan_1 = csbNode.getChildByName("pan_1");

        //退出按钮
        this.m_btClose = csbNode.getChildByName("bt_close");
        this.m_btClose.setTag(ShareLayer.BT_EXIT);
        this.m_btClose.addTouchEventListener(btncallback);

        //分享按钮
        this.m_btShare = pan_1.getChildByName("bt_share");
        this.m_btShare.setTag(ShareLayer.BT_SHARE);
        this.m_btShare.addTouchEventListener(btncallback);

        //分享按钮
        this.m_btShare = pan_1.getChildByName("bt_share_friend");
        this.m_btShare.setTag(ShareLayer.BT_SHARE_FRIEND);
        this.m_btShare.addTouchEventListener(btncallback);
    },

    onButtonClickedEvent : function ( tag, sender ) {
        if (ShareLayer.BT_EXIT == tag)
            this._scene.onKeyBack();
        else if (ShareLayer.BT_SHARE == tag) {
            var sharecall = function (isok) {
                if (typeof(isok) == "string" && isok == "true")
                    showToast(this, "分享完成", 1);
            }
            var url = GlobalUserItem.szWXSpreaderURL || yl.HTTP_URL;
            MultiPlatform.getInstance().shareToTarget(yl.ThirdParty.WECHAT_CIRCLE, sharecall, yl.SocialShare.title, yl.SocialShare.content, url);
        } else if (ShareLayer.BT_SHARE_FRIEND == tag) {
            var sharecall = function (isok) {
                if (typeof(isok) == "string" && isok == "true")
                    showToast(this, "分享完成", 1);

            }
            var url = GlobalUserItem.szWXSpreaderURL || yl.HTTP_URL;
            MultiPlatform.getInstance().shareToTarget(yl.ThirdParty.WECHAT, sharecall, yl.SocialShare.title, yl.SocialShare.content, url);
        } else {
            cc.log("shareLayer按钮点击事件报错");
        }
    }
});
//
// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new ShareLayer();
//         this.addChild(layer);
//     }
// });
