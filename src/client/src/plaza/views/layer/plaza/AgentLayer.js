/*
    author : Kil
    data   : 2017-11-23
 */
// var ExternalFun = require(appdf.EXTERNAL_SRC + "ExternalFun");
//
// var AgentLayer = class("AgentLayer", function(scene)
// 		var agentLayer = display.newLayer(cc.c4b(0, 0, 0, 125))
//     return agentLayer
// end)

var AgentLayer = cc.Layer.extend({
    WEB_AGENT_INFO			: 1,
    WEB_AGENT_SCALEINFO		: 2,
    WEB_AGENT_CHILDINFO		: 3,
    WEB_AGENT_PAYINFO		: 4,
    WEB_AGENT_REVENUEINFO	: 5,
    WEB_AGENT_PAYBACKINFO	: 6,

    ctor : function(scene) {
        this._super(cc.color(0, 0, 0, 125));

        this._scene = scene;

        var self = this;

        this.registerScriptHandler = function (eventType) {
            if (eventType === "enterTransitionFinish") {	//  进入场景而且过渡动画结束时候触发。
                self.onEnterTransitionFinish();
            } else if (eventType === "exitTransitionStart") {	//  退出场景而且开始过渡动画时候触发。
                self.onExitTransitionStart();
            }
        };

        // 按钮回调
        this._btcallback = function (ref, type) {
            if (type === ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };

        var cbtlistener = function (sender, eventType) {
            self.onSelectedEvent(sender.getTag(), sender, eventType);
        };

        this._webSelect = this.WEB_AGENT_INFO;

        this._webList = [
            "/Mobile/Agent/AgentInfo.aspx",
            "/Mobile/Agent/AgentScaleInfo.aspx",
            "/Mobile/Agent/AgentChildInfo.aspx",
            "/Mobile/Agent/AgentPayInfo.aspx",
            "/Mobile/Agent/AgentRevenueInfo.aspx",
            "/Mobile/Agent/AgentPayBackInfo.aspx"
        ];

        var frame = cc.spriteFrameCache.getSpriteFrame("sp_top_bg.png");
        if (null != frame) {
            var sp = new cc.Sprite(frame);
            sp.setPosition(yl.WIDTH / 2, yl.HEIGHT - 51);
            this.addChild(sp);
        }
        var s1 = new cc.Sprite(res.title_agent_png);
        s1.setPosition(yl.WIDTH / 2, yl.HEIGHT - 51);
        this.addChild(s1);
        frame = cc.spriteFrameCache.getSpriteFrame("sp_public_frame_0.png");
        if (null != frame) {
            var sp = new cc.Sprite(frame);
            sp.setPosition(yl.WIDTH / 2, 320);
            this.addChild(sp);
        }
        var s2 = new cc.Sprite(res.frame_agent_2_png);
        s2.setPosition(160, 320);
        this.addChild(s2);

        var b1 = new ccui.Button(res.bt_return_0_png, res.bt_return_1_png);
        b1.setPosition(75, yl.HEIGHT - 51);
        this.addChild(b1);
        b1.addTouchEventListener(function(ref, type) {
            if (type === ccui.Widget.TOUCH_ENDED) {
                self._scene.onKeyBack();
            }
        });

        frame = cc.spriteFrameCache.getSpriteFrame("sp_public_frame_1.png");
        if (null != frame) {
            var sp = new cc.Sprite(frame);
            sp.setPosition(788, 320);
            this.addChild(sp);
        }

        // 代理信息
        var p1 = new ccui.CheckBox(res.bt_agent_0_0_png,res.bt_agent_0_0_png, res.bt_agent_0_1_png, "", "");
        p1.setPosition(172, 540);
        p1.setSelected(true);
        p1.setTag(this.WEB_AGENT_INFO);
        this.addChild(p1);
        p1.addTouchEventListener(cbtlistener);

        // 分成信息
        var p2 = new ccui.CheckBox(res.bt_agent_1_0_png, res.bt_agent_1_0_png, res.bt_agent_1_1_png, "", "");
        p2.setPosition(172, 540 - 90);
        p2.setSelected(false);
        p2.setTag(this.WEB_AGENT_SCALEINFO);
        this.addChild(p2);
        p2.addTouchEventListener(cbtlistener);

        // 注册信息
        var p3 = new ccui.CheckBox(res.bt_agent_2_0_png, res.bt_agent_2_0_png, res.bt_agent_2_1_png, "", "");
        p3.setPosition(172, 540 - 90 * 2);
        p3.setSelected(false);
        p3.setTag(this.WEB_AGENT_CHILDINFO);
        this.addChild(p3);
        p3.addTouchEventListener(cbtlistener);

        // 充值信息
        var p4 = new ccui.CheckBox(res.bt_agent_3_0_png, res.bt_agent_3_0_png, res.bt_agent_3_1_png, "", "");
        p4.setPosition(172, 540 - 90 * 3);
        p4.setSelected(false);
        p4.setTag(this.WEB_AGENT_PAYINFO);
        this.addChild(p4);
        p4.addTouchEventListener(cbtlistener);

        // 税收信息
        var p5 = new ccui.CheckBox(res.bt_agent_4_0_png, res.bt_agent_4_0_png, res.bt_agent_4_1_png, "", "");
        p5.setPosition(172, 540 - 90 * 4);
        p5.setSelected(false);
        p5.setTag(this.WEB_AGENT_REVENUEINFO);
        this.addChild(p5);
        p5.addTouchEventListener(cbtlistener);

        // 返现信息
        var p6 = new ccui.CheckBox(res.bt_agent_5_0_png, res.bt_agent_5_0_png, res.bt_agent_5_1_png, "", "");
        p6.setPosition(170, 540 - 90 * 5);
        p6.setSelected(false);
        p6.setTag(this.WEB_AGENT_PAYBACKINFO);
        this.addChild(p6);
        p6.addTouchEventListener(cbtlistener);

        // 平台判定
        var targetPlatform = cc.sys.platform;
        if ((cc.sys.IPHONE == targetPlatform) || (cc.sys.IPAD == targetPlatform) || (cc.sys.ANDROID == targetPlatform)) {
            // 反馈页面
            this.m_webView = ccui.WebView.create();
            this.m_webView.setPosition(788, 320);
            this.m_webView.setContentSize(965, 520);

            this.m_webView.setScalesPageToFit(true);

            if (ExternalFun.visibleWebView(this.m_webView, false)) {
                this._scene.showPopWait();
            }
            this.m_webView.loadURL(this.getURL(this._webSelect));

            this.m_webView.setOnDidFailLoading(function (sender, url) {
                self._scene.dismissPopWait();
                cc.log("open " + url + " fail");
            });
            this.m_webView.setOnShouldStartLoading(function (sender, url) {
                cc.log("onWebViewShouldStartLoading, url is ", url);
                return true;
            });
            this.m_webView.setOnDidFinishLoading(function (sender, url) {
                self._scene.dismissPopWait();
                ExternalFun.visibleWebView(self.m_webView, true);
                cc.log("onWebViewDidFinishLoading, url is ", url);
            });

            this.m_webView.setOnJSCallback(function (sender, url) {
                }
            );
            this.addChild(this.m_webView);
        }
    },
    //  进入场景而且过渡动画结束时候触发。
    onEnterTransitionFinish : function () {
        return this;
    },
    //  退出场景而且开始过渡动画时候触发。
    onExitTransitionStart : function() {
        return this;
    },
    // 按键监听
    onButtonClickedEvent : function(tag,sender){

    },
    onSelectedEvent : function (tag,sender,eventType) {
        if (this._webSelect === tag) {
            return;
        }

        this._webSelect = tag;
        for (var l = 1; l <=this._webList.length; l++) {
            if (l != tag ) {
                this.getChildByTag(l).setSelected(false);
            }
        }

        var targetPlatform = cc.sys.platform;
        if ((cc.sys.IPHONE == targetPlatform) || (cc.sys.IPAD == targetPlatform) || (cc.sys.ANDROID == targetPlatform)) {

            if (ExternalFun.visibleWebView(this.m_webView, false)) {
                this._scene.showPopWait();
            }

            this.m_webView.loadURL(this.getURL(tag));
        }
    },
    // 获取网址
    getURL : function(tag) {
        var url = yl.HTTP_URL + "/SyncLogin.aspx?userid="+GlobalUserItem.dwUserID+"&time="+(new Date()).getTime()+"&signature="+GlobalUserItem.getSignature((new Date()).getTime())+"&url="+this._webList[tag];
        cc.log(url);
        return url;
    }
});

// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new AgentLayer();
//         this.addChild(layer);
//     }
// });