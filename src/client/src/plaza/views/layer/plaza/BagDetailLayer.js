/*
    author  : Kil
    date    : 2017-11-23
 */
// var BagDetailFrame = appdf.req(appdf.CLIENT_SRC + "plaza.models-cc-cc.ShopDetailFrame");

var BagDetailLayer = cc.Layer.extend({
    BT_USE				: 20,
    BT_TRANS		    : 21,
    BT_ADD				: 22,
    BT_MIN				: 23,

    ctor : function (scene, gameFrame) {
        this._super(cc.color(0,0,0,125));
        
        this._scene = scene;

        var self = this;
        
        // this.registerScriptHandler = (function(eventType) {
        //     if (eventType == "enterTransitionFinish") {	// 进入场景而且过渡动画结束时候触发。
        //         self.onEnterTransitionFinish();
        //     } else if (eventType == "exitTransitionStart") {	// 退出场景而且开始过渡动画时候触发。
        //         self.onExitTransitionStart();
        //     } else if (eventType == "exit") {
        //         if (null != self.m_listener) {
        //             self.getEventDispatcher().removeEventListener(this.m_listener);
        //             self.m_listener = null;
        //         }
        //
        //         if (self._BagDetailFrame.isSocketServer()) {
        //             self._BagDetailFrame.onCloseSocket();
        //         }
        //         if (null != self._BagDetailFrame._gameFrame) {
        //             self._BagDetailFrame._gameFrame._shotFrame = null;
        //             self._BagDetailFrame._gameFrame = null;
        //         }
        //     }
        // });
    
        //按钮回调
        this._btcallback = function(ref, type) {
            if (type == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };
    
        //网络回调
        var BagDetailCallBack = function(result,message) {
            self.onBagDetailCallBack(result, message);
        };
    
        //网络处理
        this._BagDetailFrame = new ShopDetailFrame(this,BagDetailCallBack);
        this._BagDetailFrame._gameFrame = gameFrame;

        if (null != gameFrame) {
            gameFrame._shotFrame = this._BagDetailFrame;
        }
    
        this._item = GlobalUserItem.useItem;
        this._useNum = 1;
    
        var s1= new cc.Sprite(res.frame_shop_0_png);
        s1.setPosition(yl.WIDTH/2,yl.HEIGHT - 51);
        this.addChild(s1);
        var b1= new ccui.Button(res.bt_return_0_png,res.bt_return_1_png);
        b1.setPosition(75,yl.HEIGHT-51);
        this.addChild(b1);
        b1.addTouchEventListener(function(ref, type) {
            if (type == ccui.Widget.TOUCH_ENDED) {
                self._scene.onKeyBack();
            }
        });
    
        var frame = cc.spriteFrameCache.getSpriteFrame("sp_public_frame_0.png");
        if (null != frame) {
            var sp = new cc.Sprite(frame);
            sp.setPosition(yl.WIDTH / 2, 325);
            this.addChild(sp);
        }

        var s2 = new cc.Sprite(res.frame_detail_0_png);
        s2.setPosition(840,350);
        this.addChild(s2);

        var s3 = new cc.Sprite(res.frame_detail_1_png);
        s3.setPosition(210,458);
        this.addChild(s3);

        frame = cc.spriteFrameCache.getSpriteFrame("icon_public_" + this._item._index + ".png");
        if (null != frame) {
            var sp = new cc.Sprite(frame);
            this.addChild(sp);
            sp.setPosition(210, 458);
        }

        frame = cc.spriteFrameCache.getSpriteFrame("text_public_" + this._item._index + ".png");
        if (null != sp) {
            var sp = new cc.Sprite(frame);
            this.addChild(sp);
            sp.setPosition(210, 330);
        }
        var str = "0";
        // var num = str.charCodeAt(0);
        // cc.log(num);
        this._txtNum1 = new cc.LabelAtlas("" + this._item._count, res.num_0_png, 20, 25, str);
        this._txtNum1.setAnchorPoint(cc.p(1.0,0.0));
        this._txtNum1.setPosition(266,400);
        this.addChild(this._txtNum1);

        //数量
        var s4 = new cc.Sprite(res.text_detail_3_png);
        s4.setPosition(180,277);
        this.addChild(s4);

        this._txtNum2 = new cc.LabelAtlas("" + this._item._count, res.num_3_png, 21, 26, str);
        this._txtNum2.setAnchorPoint(cc.p(0.0,0.5));
        this._txtNum2.setPosition(246,277);
        this.addChild(this._txtNum2);

        var b2 = new ccui.Button(res.bt_present_0_png,res.bt_present_1_png);
        b2.setPosition(208,158);
        b2.setTag(this.BT_TRANS);
        this.addChild(b2);
        b2.addTouchEventListener(this._btcallback);

        var b3 = new ccui.Button(res.bt_use_0_png,res.bt_use_1_png);
        b3.setPosition(844,241);
        b3.setTag(this.BT_USE);
        this.addChild(b3);
        b3.addTouchEventListener(this._btcallback);

        var s5 = new cc.Sprite(res.text_detail_0_png);
        s5.setPosition(533,460);
        this.addChild(s5);

        var s6 = new cc.Sprite(res.text_detail_1_png);
        s6.setPosition(533,360);
        this.addChild(s6);

        var s7 = new cc.Sprite(res.frame_detail_2_png);
        s7.setPosition(853,460);
        this.addChild(s7);

        var b4 = new ccui.Button(res.bt_detail_min_png,res.bt_detail_min_png);
        b4.setPosition(633,460);
        b4.setTag(this.BT_MIN);
        this.addChild(b4);
        b4.addTouchEventListener(this._btcallback);

        var b5 = new ccui.Button(res.bt_detail_add_png,res.bt_detail_add_png);
        b5.setPosition(1065,460);
        b5.setTag(this.BT_ADD);
        this.addChild(b5);
        b5.addTouchEventListener(this._btcallback);
        str = ".";
        // .charCodeAt();
        this._txtBuy = new cc.LabelAtlas("1", res.num_detail_0_png, 19, 25, str);
        this._txtBuy.setAnchorPoint(cc.p(0.5,0.5));
        this._txtBuy.setPosition(853,460);
        this.addChild(this._txtBuy);

        var s9 = new cc.Sprite(res.frame_detail_2_png);
        s9.setPosition(853,360);
        this.addChild(s9);
    
        var area = "";
        if (bit._and(this._item._area,yl.PT_ISSUE_AREA_PLATFORM)) {
            area = "大厅适用 ";
        }
        if (bit._and(this._item._area,yl.PT_ISSUE_AREA_SERVER)) {
            area = area + "房间适用 ";
        }
        if (bit._and(this._item._area,yl.PT_ISSUE_AREA_GAME)) {
            area = area + "游戏适用 ";
        }
        var lt = new cc.LabelTTF(area, res.round_body_ttf, 22);
        lt.setAnchorPoint(cc.p(0.0,0.5));
        lt.setPosition(620,360);
        lt.setFontFillColor(cc.color(195,199,239,255));
        this.addChild(lt);
    
        //右侧剩余
        var s8 = new cc.Sprite(res.text_detail_2_png);
        s8.setPosition(1100,460);
        s8.setAnchorPoint(cc.p(0.5, 0.5));
        this.addChild(s8);

        str = "0";
        // .charCodeAt();
        this._txtNum3 = new cc.LabelAtlas("" + this._item._count, res.num_1_png, 18, 23, str);
        this._txtNum3.setAnchorPoint(cc.p(0.0,0.5));
        this._txtNum3.setPosition(1170,460);
        this.addChild(this._txtNum3);
    
        //功能描述
        var lt1 = new cc.LabelTTF("功能：" + this._item._info, res.round_body_ttf, 22);
        lt1.setAnchorPoint(cc.p(0.5,0.5));
        lt1.setPosition(yl.WIDTH/2,70);
        lt1.setColor(cc.color(136,164,224,255));   //setFontFillColor
        this.addChild(lt1);
    
        this.onUpdateNum();
    
        //通知监听
        var eventListener = function (event) {
            if (self._item._index == yl.LARGE_TRUMPET) {
                self._item._count = GlobalUserItem.nLargeTrumpetCount;
            }
            self.onUpdateNum();
        };
        this.m_listener = new cc.EventListener(yl.TRUMPET_COUNT_UPDATE_NOTIFY, eventListener);
        // this.m_listener = cc.EventListenerCustom.create(yl.TRUMPET_COUNT_UPDATE_NOTIFY, eventListener);
        //todo can't find following method
        // this.getEventDispatcher().addEventListenerWithFixedPriority(this.m_listener, 1);
    
    },
    // 进入场景而且过渡动画结束时候触发。
    onEnterTransitionFinish : function() {
        return this;
    },
    // 退出场景而且开始过渡动画时候触发。
    onExitTransitionStart : function() {
        return this
    },
    //按键监听
    onButtonClickedEvent : function(tag,sender) {
        if (tag == this.BT_ADD) {
            if (this._useNum < this._item._count) {
                this._useNum = this._useNum + 1;
                this.onUpdateNum();
            }
        } else if (tag == this.BT_MIN) {
            if (this._useNum != 1) {
                this._useNum = this._useNum - 1;
                this.onUpdateNum();
            }
        } else if (tag == this.BT_USE) {
            //判断是否是消耗大小喇叭
            if (this._item._index == yl.LARGE_TRUMPET || this._item._index == yl.SMALL_TRUMPET) {
                if (this._item._index == yl.LARGE_TRUMPET && null != this._scene.getTrumpetSendLayer) {
                    this._scene.getTrumpetSendLayer();
                }
            } else {
                this._scene.showPopWait();
                this._BagDetailFrame.onPropertyUse(this._item._index, this._useNum);
            }
        } else if (tag == this.BT_TRANS) {
            this.getParent().getParent().onChangeShowMode(yl.SCENE_BAGTRANS);
        }
    },
    onUpdateNum : function() {
        this._txtBuy.setString(string_formatNumberThousands(this._useNum,true,"/"));
        this._txtNum1.setString("" + this._item._count);
        this._txtNum2.setString("" + this._item._count);
        this._txtNum3.setString("" + this._item._count-this._useNum);
    },
    onBagDetailCallBack : function (result, message) {
        cc.log("======== BagDetailLayer:onBagDetailCallBack ========");

        this._scene.dismissPopWait();
        if (message != null && message != "")
            showToast(this,message,2);

        if (result == 2) {
            this._item._count = this._item._count - this._useNum;
            this._useNum = 1;
            this.onUpdateNum();

            if (this._item._count < 1)
                this._scene.onKeyBack();
        }
    }
});
// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new BagDetailLayer();
//         this.addChild(layer);
//     }
// });