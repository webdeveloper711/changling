/*
    author  : Kil
    date    : 2017-11-23
 */
// var ShopDetailFrame = appdf.req(appdf.CLIENT_SRC + "plaza.models-cc-cc.ShopDetailFrame");
var BagLayer = cc.Layer.extend({
    CBT_GEM	    : 1,
    CBT_CARD	: 2,
    CBT_ITEM	: 3,
    CBT_GIFT	: 4,
    CBT_ALL	    : 5,

    BT_GEM		: 100,
    BT_CARD	    : this.BT_GEM+200,
    BT_ITEM	    : this.BT_CARD+200,
    BT_GIFT	    : this.BT_ITEM+200,

    ROOM_MAX	: 9999,
    ROOM_MIN	: 0,

    ctor : function (scene, gameFrame) {
        this._super(cc.color(0, 0, 0, 125));
        this._scene = scene;
        var self = this;
        //按钮回调
        this._btcallback = function (sender, type) {
            if (type === ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(sender);
            }
        };

        var cbtlistener = function (sender, eventType) {
            self.onSelectedEvent(sender.getTag(), sender, eventType);
        };

        //网络回调
        var bagCallBack = function (result, message) {
            self.onBagCallBack(result, message);
        };

        //网络处理
        this._shopDetailFrame = new ShopDetailFrame(this,bagCallBack);
        this._shopDetailFrame._gameFrame = gameFrame;
        if (null != gameFrame)
            gameFrame._shotFrame = this._shopDetailFrame;

        //显示单个复选框/全部
        //this._select = BagLayer.CBT_GEM
        this._select = this.CBT_ALL;

        //显示队列
        this._showList = [];
        //数据队列
        this._allList  = [];
        this._gemList  = [];
        this._cardList = [];
        this._itemList = [];
        this._giftList = [];

        var b1 = new ccui.Button(res.bag_bt_return_png,res.bag_bt_return_png);
        b1.setTouchEnabled(true);
        b1.setPosition(75,yl.HEIGHT-51);
        b1.addTouchEventListener(function(ref, type) {
            if (type == ccui.Widget.TOUCH_ENDED) {
                self._scene.onKeyBack();
            }
        });
        this.addChild(b1);

        var s1 = new cc.Sprite(res.bg_frame_1_png);
        s1.setPosition(yl.WIDTH/2,400);
        this.addChild(s1);

        var s2 = new cc.Sprite(res.bag_sp_roomCard_png);
        s2.setPosition(yl.WIDTH*0.25,yl.HEIGHT*0.5);
        this.addChild(s2);

        var s3 = new cc.Sprite(res.bag_sp_ingot_png);
        s3.setPosition(yl.WIDTH*0.5,yl.HEIGHT*0.5);
        this.addChild(s3);

        var s4 = new cc.Sprite(res.bag_sp_score_png);
        s4.setPosition(yl.WIDTH*0.75,yl.HEIGHT*0.5);
        this.addChild(s4);
        
        this.m_txtRoomCard = new cc.LabelTTF("123", res.round_body_ttf, 24);
        this.m_txtRoomCard.setAnchorPoint(cc.p(0.5,0.5));
        this.m_txtRoomCard.setPosition(yl.WIDTH*0.25,yl.HEIGHT*0.38);
        this.m_txtRoomCard.setColor(cc.color(100,25,2,255));
        this.addChild(this.m_txtRoomCard);

        this.m_txtIngot = new cc.LabelTTF("123", res.round_body_ttf, 24);
        this.m_txtIngot.setAnchorPoint(cc.p(0.5,0.5));
        this.m_txtIngot.setPosition(yl.WIDTH*0.5,yl.HEIGHT*0.38);
        this.m_txtIngot.setColor(cc.color(100,25,2,255));
        this.addChild(this.m_txtIngot);

        this.m_txtScore = new cc.LabelTTF("123", res.round_body_ttf, 24);
        this.m_txtScore.setAnchorPoint(cc.p(0.5,0.5));
        this.m_txtScore.setPosition(yl.WIDTH*0.75,yl.HEIGHT*0.38);
        this.m_txtScore.setColor(cc.color(100,25,2,255));
        this.addChild(this.m_txtScore);

        // 房卡输入
        this.m_editRoomCard = new cc.EditBox(cc.size(60,35), new cc.Scale9Sprite(res.bag_sp_numRoomCardBg_png));
        this.m_editRoomCard.setPosition(yl.WIDTH*0.28,yl.HEIGHT*0.25);
        this.m_editRoomCard.setAnchorPoint(cc.p(0.5,0.5));
        this.m_editRoomCard.setFontName(res.round_body_ttf);
        this.m_editRoomCard.setPlaceholderFontName(res.round_body_ttf);
        this.m_editRoomCard.setFontSize(24);
        this.m_editRoomCard.setPlaceholderFontSize(24);
        this.m_editRoomCard.setMaxLength(31);
        this.m_editRoomCard.string="";
        this.m_editRoomCard.setFontColor(cc.color(255,255,255));
        this.m_editRoomCard.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        this.addChild(this.m_editRoomCard);
        // this.m_editRoomCard.addTouchEventListener(this.onEditEvent2, this);
        this.m_editRoomCard.setVisible(false);

        //账号输入
        this.m_editUserID = new cc.EditBox(cc.size(320,35), new cc.Scale9Sprite(res.bag_sp_idBg_png));
        this.m_editUserID.setPosition(yl.WIDTH*0.45,yl.HEIGHT*0.25);
        this.m_editUserID.setAnchorPoint(cc.p(0.5,0.5));
        this.m_editUserID.setFontName(res.round_body_ttf);
        this.m_editUserID.setPlaceholderFontName(res.round_body_ttf);
        this.m_editUserID.setFontSize(24);
        this.m_editUserID.setPlaceholderFontSize(24);
        this.m_editUserID.setMaxLength(31);
        this.m_editUserID.string = "";
        this.m_editUserID.setFontColor(cc.color(255,255,255));
        this.m_editUserID.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        this.addChild(this.m_editUserID);
        // this.m_editUserID.addTouchEventListener(this.onEditEvent2, this);
        this.m_editUserID.setVisible(false);

    },
    // 进入场景而且过渡动画结束时候触发。
    onEnterTransitionDidFinish : function () {
        this._scene.showPopWait();
        this._shopDetailFrame.onQuerySend();
        return this;
    },
    // 退出场景而且开始过渡动画时候触发。
    onExitTransitionDidStart : function () {
        return this;
    },
    onExit : function () {
        if (this._shopDetailFrame.isSocketServer()) {
            this._shopDetailFrame.onCloseSocket();
        }
        if (null != this._shopDetailFrame._gameFrame) {
            this._shopDetailFrame._gameFrame._shotFrame = null;
            this._shopDetailFrame._gameFrame = null;
        }
    },
    onEditEvent2 : function(event,editbox) {

    },
    //按键监听
    onButtonClickedEvent : function(tag,sender) {
        var beginPos = sender.getTouchBeganPosition();
        var endPos = sender.getTouchEndPosition();
        if (Math.abs(endPos.x - beginPos.x) > 30
        || Math.abs(endPos.y - beginPos.y) > 30) {
            cc.log("BagLayer:onButtonClickedEvent ==> MoveTouch Filter");
            return;
        }
        cc.log("***** button clicked-" + tag + " ******");
        if ((tag>BagLayer.BT_GEM) && (tag<BagLayer.BT_CARD)) {
            GlobalUserItem.useItem = this._gemList[tag - BagLayer.BT_GEM];
            this.getParent().getParent().onChangeShowMode(yl.SCENE_BAGDETAIL);
        } else if ((tag>BagLayer.BT_CARD) && (tag<BagLayer.BT_ITEM)) {
            GlobalUserItem.useItem = this._cardList[tag - BagLayer.BT_CARD];
            this.getParent().getParent().onChangeShowMode(yl.SCENE_BAGDETAIL);
        } else if ((tag>BagLayer.BT_ITEM) && (tag<BagLayer.BT_GIFT)) {
            GlobalUserItem.useItem = this._itemList[tag - BagLayer.BT_ITEM];
            this.getParent().getParent().onChangeShowMode(yl.SCENE_BAGDETAIL);
        } else if ((tag>BagLayer.BT_GIFT) && (tag<BagLayer.BT_GIFT+200)) {
            showToast(this, "手机端暂不支持礼物道具，请前往PC客户端使用！", 2);
        }
    },
    onSelectedEvent : function (tag, sender, eventType) {
        if (this._select === tag) {
            this.getChildByTag(tag).setSelected(true);
            return;
        }
        this._select = tag;
        for (var i = 1; i < 4; i++) {
            if (i != tag) {
                this.getChildByTag(i).setSelected(false);
            }
        }
        this.onClearShowList();
        this.onUpdateShowList2();
    },
    //清除当前显示
    onClearShowList : function () {
        for (var i = 0; i < this._showList.length; i++) {
            this._showList[i].removeFromParent();
        }
        this._showList = [] ||null;
    },
    //新界面
    onUpdateShowList2 : function () {
        this.m_txtRoomCard.setString(GlobalUserItem.lRoomCard  +  "张");
        this.m_txtIngot.setString(GlobalUserItem.lUserIngot  +  "个");
        this.m_txtScore.setString("$" +  GlobalUserItem.lUserScore  +  "");
    },

    //操作结果
    onBagCallBack : function(result,message) {
        //刷新界面
        this._scene.dismissPopWait();
        this.onUpdateShowList2();

        if (true) return true;  //原界面全都不执行

        this._scene.dismissPopWait();
        if (message != null && message != "" && result != 5) {
            showToast(this, message, 2);
        }

        if (result === yl.SUB_GP_QUERY_BACKPACKET_RESULT) {
            if (message.length === 0) {
                //showToast(this, "背包为空", 2)
                showToast(this, "背包啥也没有 ~@~ ", 2);
                return;
            }

            this._gemList = [];
            this._cardList = [];
            this._itemList = [];
            this._giftList = [];
            this._allList = [];
            this._allList.strType = [];
            this._allList.value = [];
            this._allList.tag = [];
            for (i = 0; i < message.length; i++) {
                var item = message[i];
                if (Math.floor(item._index / 100) === 0) {
                    this._giftList.push(item);
                    this._allList.strType.push("gift");
                    this._allList.value.push(item);
                    this._allList.tag.push(this._giftList.length);
                } else if (Math.floor(item._index / 100) === 1) {
                    this._gemList.push(item);
                    this._allList.strType.push("gem");
                    this._allList.value.push(item);
                    this._allList.tag.push(this._gemList.length);
                } else if (Math.floor(item._index / 100) === 2) {
                    this._cardList.push(item);
                    this._allList.strType.push("card");
                    this._allList.value.push(item);
                    this._allList.tag.push(this._cardList.length);
                } else if (Math.floor(item._index / 100) === 3) {
                    this._itemList.push(item);
                    this._allList.strType.push("item");
                    this._allList.value.push(item);
                    this._allList.tag.push(this._itemList);
                }

            }

            //刷新界面
            this.onClearShowList();
            this.onUpdateShowList();

        }

    },
    ////-ysy——end
    //更新当前显示
    onUpdateShowList: function () {

        var theList = [];
        var tag = 0;
        var tagList = [];
        var typeList = [];
        if (this._select === BagLayer.CBT_GEM) {
            theList = this._gemList;
            tag = BagLayer.BT_GEM;
        } else if (this._select === BagLayer.CBT_CARD) {
            theList = this._cardList;
            tag = BagLayer.BT_CARD;
        } else if (this._select === BagLayer.CBT_ITEM) {
            theList = this._itemList;
            tag = BagLayer.BT_ITEM;
        } else if (this._select === BagLayer.CBT_GIFT) {
            theList = this._giftList;
            tag = BagLayer.BT_GIFT;
        } else if (this._select === BagLayer.CBT_ALL) {
            theList = this._allList.value;
            tagList = this._allList.tag;
            typeList = this._allList.strType;
        }

        //计算scroll滑动高度
        var scrollHeight = 0;
        if (theList.length < 19) {
            scrollHeight = 458;
            this._scrollView.setInnerContainerSize(cc.size(938, 458 + 20));
        } else {
            scrollHeight = 155 * Math.floor((theList.length + Math.floor(theList.length % 6)) / 6);
            this._scrollView.setInnerContainerSize(cc.size(938, scrollHeight + 20));
        }

        for (i = 0; i<theList.length; i++) {

            var btnTag = 0;
            if (tag != 0) {
                btnTag = tag + i;
            } else if (tagList != 0) {
                btnTag = this.onAnalysisShowList(i);
            }
            if (btnTag === 0)
                break;


            var item = theList[i];
            this._showList[i] = cc.LayerColor(cc.color(100, 100, 100, 0), 143, 143);
            this._showList[i].setPosition(80 + Math.floor((i - 1) % 6) * 154 - 143 / 2, scrollHeight - (80 + Math.floor((i - 1) / 6) * 154) - 143 / 2 + 20);
            this._scrollView.addChild(this._showList[i]);


            var b1 = new ccui.Button(res.bg_frame_3_png, res.bg_frame_3_png);
            b1.setContentSize(cc.size(143, 143));
            b1.setPosition(143 / 2, 143 / 2);
            b1.setTag(btnTag);
            this._showList[i].addChild(b1);
            b1.setSwallowTouches(false);
            b1.addTouchEventListener(this._btcallback);

            var frame = cc.spriteFrameCache.getSpriteFrame("icon_public_" + item._index + ".png");
            if (null != frame) {
                var sp = new cc.Sprite(frame);
                sp.setPosition(71.5, 71.5);
                this._showList[i];
                addChild(sp);
            }

            var tmp = "0";
            var la = new cc.LabelAtlas("" + item._count, "Bag/num_0.png", 20, 25, tmp.charCodeAt());
            la.setAnchorPoint(cc.p(1.0, 0.5));
            la.setPosition(128, 25);
            this._showList[i].addChild(la);

        }
    },
    onAnalysisShowList : function(cbNum){
        var cbTag = 0;
        if (this._allList.strType === "gift") {
            cbTag = this._allList.tag + BagLayer.BT_GEM;
        } else if (this._allList.strType === "gift") {
            cbTag = this._allList.tag + BagLayer.BT_GEM;
        } else if (this._allList.strType === "gift") {
            cbTag = this._allList.tag + BagLayer.BT_GEM;
        } else if (this._allList.strType === "gift") {
            cbTag = this._allList.tag + BagLayer.BT_GEM;
        }
        return cbTag;
    }
});
// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new BagLayer();
//         this.addChild(layer);
//     }
// });