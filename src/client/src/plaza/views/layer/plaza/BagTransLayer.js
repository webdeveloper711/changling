/*
    author  : Kil
    date    : 2017-11-24
 */
// var BagTransFrame = appdf.req(appdf.CLIENT_SRC + "plaza.models-cc-cc.ShopDetailFrame");
var BagTransLayer = cc.Layer.extend({
    CBT_USERID: 10,
    CBT_NICKNAME: 11,

    BT_TRANS: 21,
    BT_ADD: 22,
    BT_MIN: 23,

    ctor: function (scene, gameFrame) {
        this._super(cc.color(0, 0, 0, 125));
        var self = this;


        //按钮回调
        this._btcallback = function(ref, type) {
            if (type == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };


        var cbtlistener = function (sender,eventType) {
            self.onSelectedEvent(sender.getTag(), sender, eventType)
        };

        //网络回调
        var BagTransCallBack = function(result,message) {
            self.onBagTransCallBack(result, message);
        };

        this._BagTransFrame = new ShopDetailFrame(this,BagTransCallBack);
        this._BagTransFrame._gameFrame = gameFrame;
        if (null !== gameFrame) {
            //todo
            // gameFrame._shotFrame = this._BagTransFrame;
        }
        this._item = GlobalUserItem.useItem;
        this._transNum = 1;
        this._type = yl.PRESEND_GAMEID;

        var s1 = new cc.Sprite(res.frame_shop_0_png);
        s1.setPosition(yl.WIDTH/2,yl.HEIGHT - 51);
        this.addChild(s1);

        var b1 = new ccui.Button(res.bt_return_0_png,res.bt_return_0_png);
        b1.setPosition(75,yl.HEIGHT-51);
        this.addChild(b1);
        b1.addTouchEventListener(function(ref, type) {
            self._scene.onKeyBack();
        });

        var frame = cc.spriteFrameCache.getSpriteFrame("sp_public_frame_0.png");
        if (null !== frame) {
            var sp = new cc.Sprite(frame);
            sp.setPosition(yl.WIDTH / 2, 325);
            this.addChild(sp);
        }

        var s2 = new cc.Sprite(res.frame_detail_0_png);
        s2.setPosition(840,350);
        this.addChild(s2);

        var s3 = new cc.Sprite(res.frame_detail_1_png);
        s3.setPosition(210,458);
        this.addChild(s3);

        frame = cc.spriteFrameCache.getSpriteFrame("icon_public_" + this._item._index + ".png");
        if (null !== frame) {
            var sp = new cc.Sprite(frame);
            sp.setPosition(210, 458);
            this.addChild(sp);
        }

        frame = cc.spriteFrameCache.getSpriteFrame("text_public_" + this._item._index + ".png");
        if (null !== frame) {
            var sp = new cc.Sprite(frame);
            sp.setPosition(210, 330);
            this.addChild(sp);
        }
        var tmp = "0";
        this._txtNum1 = new cc.LabelAtlas("" + this._item._count, res.num_0_png, 20, 25, tmp);
        this._txtNum1.setAnchorPoint(cc.p(1.0,0.0));
        this._txtNum1.setPosition(266,400);
        this.addChild(this._txtNum1);

        //数量
        var s4 = new cc.Sprite(res.text_detail_3_png);
        s4.setPosition(180,277);
        this.addChild(s4);

        this._txtNum2 =  new cc.LabelAtlas("" + this._item._count, res.num_3_png, 20, 25, tmp);
        this._txtNum2.setAnchorPoint(cc.p(0.0,0.5));
        this._txtNum2.setPosition(246,277);
        this.addChild(this._txtNum2);

        //文字标签
        var s5 = new cc.Sprite(res.text_detail_4_png);
        s5.setAnchorPoint(cc.p(0.0,0.5));
        s5.setPosition(610,485);
        this.addChild(s5);

        var s6 = new cc.Sprite(res.text_detail_5_png);
        s6.setAnchorPoint(cc.p(0.0,0.5));
        s6.setPosition(845,485);
        this.addChild(s6);

        //依据ID
        var c1 = new ccui.CheckBox(res.cbt_choose_0_png,res.cbt_choose_0_png,res.cbt_choose_1_png,"","");
        c1.setPosition(587,485);
        this.addChild(c1);
        c1.setSelected(true);
        c1.setTag(this.CBT_USERID);
        c1.addEventListener(cbtlistener);
        //依据昵称
        var c2 = new ccui.CheckBox(res.cbt_choose_0_png,res.cbt_choose_0_png,res.cbt_choose_1_png,"","");
        c2.setPosition(824,485);
        this.addChild(c2);
        c2.setSelected(false);
        c2.setTag(this.CBT_NICKNAME);
        c2.addEventListener(cbtlistener);

        var b2 = new ccui.Button(res.bt_detail_present_0_png,res.bt_detail_present_1_png);
        b2.setPosition(837,206);
        this.addChild(b2);
        b2.setTag(this.BT_TRANS);
        b2.addTouchEventListener(this._btcallback);

        //文字标签
        var s7 = new cc.Sprite(res.text_detail_6_png);
        s7.setPosition(533,408);
        this.addChild(s7);

        var s8 = new cc.Sprite(res.text_detail_7_png);
        s8.setPosition(533,318);
        this.addChild(s8);

        //接收玩家
        this.edit_trans = new cc.EditBox(cc.size(481,49), new ccui.Scale9Sprite(res.frame_detail_2_png));
        this.edit_trans.setPosition(853,408);
        this.edit_trans.setAnchorPoint(cc.p(0.5,0.5));
        this.edit_trans.setFontName(res.round_body_ttf);
        this.edit_trans.setPlaceholderFontName(res.round_body_ttf);
        this.edit_trans.setFontSize(24);
        this.edit_trans.setMaxLength(32);
        this.edit_trans.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        this.addChild(this.edit_trans);

        //赠送数量
        var s9 = new cc.Sprite(res.frame_detail_2_png);
        s9.setPosition(853,318);
        this.addChild(s9);

        var b3 = new ccui.Button(res.bt_detail_min_png,res.bt_detail_min_png);
        b3.setPosition(633,318);
        this.addChild(b3);
        b3.setTag(this.BT_MIN);
        b3.addTouchEventListener(this._btcallback);

        var b4 = new ccui.Button(res.bt_detail_add_png,res.bt_detail_add_png);
        b4.setPosition(1065,318);
        this.addChild(b4);
        b4.setTag(this.BT_ADD);
        b4.addTouchEventListener(this._btcallback);

        this._txtBuy = new cc.LabelAtlas("1", res.num_detail_0_png, 19, 25, tmp);
        this._txtBuy.setPosition(853,318);
        this._txtBuy.setAnchorPoint(cc.p(0.5,0.5));
        this.addChild(this._txtBuy);

        //右侧剩余
        var s10 = new cc.Sprite(res.text_detail_2_png);
        s10.setAnchorPoint(cc.p(0.0,0.5));
        s10.setPosition(1100,318);
        this.addChild(s10);

        this._txtNum3 = new cc.LabelAtlas("" + this._item._count, res.num_1_png, 18, 23, tmp);
        this._txtNum3.setPosition(1170,318);
        this._txtNum3.setAnchorPoint(cc.p(0.0,0.5));
        this.addChild(this._txtNum3);

        //功能描述
        var lt = new cc.LabelTTF("功能：" + this._item._info, res.round_body_ttf, 22);
        lt.setAnchorPoint(cc.p(0.5,0.5));
        lt.setPosition(yl.WIDTH/2,70);
        lt.setColor(cc.color(136,164,224,255));
        this.addChild(lt);

        this.onUpdateNum();
    },
    onSelectedEvent : function(tag,sender,eventType) {
        var wType = 0;
        if (tag == this.CBT_USERID) {
            wType = yl.PRESEND_GAMEID;
        }else if (tag == this.CBT_NICKNAME) {
            wType = yl.PRESEND_NICKNAME;
        }

        if (this._type == wType) {
            cc.log("return");
            this.getChildByTag(tag).setSelected(true);
            return;
        }
        this._type = wType;

        for (i = this.CBT_USERID; i <= this.CBT_NICKNAME; i++) {
            if (i != tag) {
                this.getChildByTag(i).setSelected(false);
            }
        }

        this.edit_trans.string = "";
    },
    onExit: function () {
        if (this._BagTransFrame.isSocketServer()) {
            this._BagTransFrame.onCloseSocket();
        }
        if (null !== this._BagTransFrame._gameFrame) {
            this._BagTransFrame._gameFrame._shotFrame = null;
            this._BagTransFrame._gameFrame = null;
        }
    },
    onButtonClickedEvent: function (tag, sender) {
        if (tag === this.BT_ADD) {
            if (this._transNum < this._item._count) {
                this._transNum = this._transNum + 1;
                this.onUpdateNum();
            }
        } else if (tag === this.BT_MIN) {
            if (this._transNum !== 1) {
                this._transNum = this._transNum - 1;
                this.onUpdateNum();
            }
        } else if (tag === this.BT_TRANS) {
            var szTarget = (this.edit_trans.getString()).replace(" ", "");
            if (szTarget.length < 1) {
                showToast(this, "请输入赠送用户昵称或ID！", 2);
                return;
            }
            var gameid = 0;
            if (this._type === yl.PRESEND_GAMEID) {
                gameid = Number(szTarget);
                szTarget = "";
                if (gameid === 0 || gameid === null) {
                    showToast(this, "请输入正确的ID！", 2);
                    return;
                }
            }

            this._scene.showPopWait();
            this._BagTransFrame.onPropertyTrans(this._item._index, this._type, gameid, szTarget, this._transNum);
        }
    },
    onUpdateNum : function () {
        this._txtBuy.setString(string_formatNumberThousands(this._transNum,true,"/"));
        this._txtNum1.setString("" + this._item._count);
        this._txtNum2.setString("" + this._item._count);
        this._txtNum3.setString("" + this._item._count-this._transNum);
    }
});
// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new BagTransLayer();
//         this.addChild(layer);
//     }
// });