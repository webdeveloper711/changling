/*
Author. Li
 */
var TAG_MASK = 101;
var BTN_SHARE = 102;
var BTN_SAVEPIC = 103;

var BankLayer = cc.Layer.extend({
    BT_TAKE: 2,
    BT_SAVE: 3,
    BT_TRANSFER: 4,
    CBT_SAVEORTAKE: 5,
    CBT_TRANSFER: 6,
    EDIT_TRANSFER_DST: 9,
    CBT_BY_ID: 10,
    CBT_BY_NAME: 11,
    BT_EXIT: 12,
    BT_FORGET: 15,
    BT_CHECK: 16,
    EDIT_SAVEORTAKE: 17,
    BT_CLOSE: 18,
    BT_ENABLE: 19,
    BT_BACK: 20,
    //开通银行
    BT_ENABLE_RETURN: 30,
    BT_ENABLE_BACK: 31,
    BT_ENABLE_CONFIRM: 32,

    ctor: function (scene, gameFrame) {
        this._super(cc.color(0, 0, 0, 125));
        //todo
        // ExternalFun.registerNodeEvent(this);

        this._scene = scene;

        this.setContentSize(yl.WIDTH, yl.HEIGHT);

        var self = this;
        //银行配置信息
        this.m_tabBankConfigInfo = [];

        var editHanlder = function (event, editbox) {
            self.onEditEvent(event, editbox);
        };

        var btcallback = function (ref, type) {
            if (type == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };

        var cbtlistener = function (sender, eventType) {
            self.onSelectedEvent(sender, eventType);
        };

        //网络回调
        var bankCallBack = function (result, message) {
            self.onBankCallBack(result, message);
        };

        //网络处理
		//todo all
        // this._bankFrame = BankFrame.create(this, bankCallBack);
        // this._bankFrame._gameFrame = gameFrame;
        // if (null != gameFrame) {
         //    gameFrame._shotFrame = this._bankFrame;
        // }

        var areaWidth = yl.WIDTH;
        var areaHeight = yl.HEIGHT;

        //显示标题
        var frame = cc.spriteFrameCache.getSpriteFrame("sp_top_bg.png");
        if (null != frame) {
            var sp = new cc.Sprite(frame);
            sp.setPosition(areaWidth / 2, 700);
            this.addChild(sp);
        }
        var sp1 = new cc.Sprite("Bank/title_bank.png");
        sp1.setPosition(areaWidth / 2, 700);
        this.addChild(sp1);

        //返回按钮
        var btn1 = new ccui.Button(res.bt_return_0_png, res.bt_return_1_png);
        btn1.setPosition(75, yl.HEIGHT - 51);
        btn1.setTag(BankLayer.BT_EXIT);
        this.addChild(btn1);
        btn1.addTouchEventListener(btcallback);

        //背景框
        frame = cc.spriteFrameCache.getSpriteFrame("sp_public_frame_0.png");
        if (null != frame) {
            var sp = new cc.Sprite(frame);
            sp.setPosition(areaWidth / 2, 325);
            this.addChild(sp);
        }

        this._cbtSaveTake = new ccui.CheckBox("Bank/tag_bank_take_0.png", "Bank/tag_bank_take_0.png", "Bank/tag_bank_take_1.png", "", "");
        this._cbtSaveTake.setPosition(yl.WIDTH / 2, 585);
        this._cbtSaveTake.setAnchorPoint(cc.p(1.0, 0.5));
        this._cbtSaveTake.setSelected(true);
        this.addChild(this._cbtSaveTake);
        this._cbtSaveTake.setTag(this.CBT_SAVEORTAKE);
        this._cbtSaveTake.addEventListener(cbtlistener);
        this._cbtSaveTake.setVisible(false);
        this._cbtSaveTake.setEnabled(false);

        //银行赠送
        this._cbtTransfer = new ccui.CheckBox("Bank/tag_bank_present_0.png", "Bank/tag_bank_present_0.png", "Bank/tag_bank_present_1.png", "", "");
        this._cbtTransfer.setAnchorPoint(cc.p(0, 0.5));
        this._cbtTransfer.setPosition(yl.WIDTH / 2, 585);
        this._cbtTransfer.setSelected(false);
        this.addChild(this._cbtTransfer);
        this._cbtTransfer.setTag(this.CBT_TRANSFER);
        this._cbtTransfer.addEventListener(cbtlistener);
        this._cbtTransfer.setVisible(false);
        this._cbtTransfer.setEnabled(false);

        //转换区域
        this._takesaveArea = new cc.Layer();
        this._takesaveArea.setContentSize(1250, 520);
        this._takesaveArea.setPosition(42, 26);
        this.addChild(this._takesaveArea);

        var sp2 = new cc.Sprite("Bank/bank_frame_0.png");
        sp2.setPosition(1250 / 2, 430);
        this._takesaveArea.addChild(sp2);
        //携带金币
        var sp3 = new cc.Sprite("Bank/icon_bank_take.png");
        sp3.setPosition(300, 425);
        this._takesaveArea.addChild(sp3);
        var sp4 = new cc.Sprite("Bank/text_bank_gold.png");
        sp4.setPosition(455, 443);
        this._takesaveArea.addChild(sp4);
        this._txtScore = new cc.LabelAtlas(string_formatNumberThousands(GlobalUserItem.lUserScore, true, "/"), "Bank/bank_num_1.png", 19, 24, "/");
        this._txtScore.setPosition(380, 396);
        this._txtScore.setAnchorPoint(cc.p(0, 0.5));
        this._takesaveArea.addChild(this._txtScore);

        //银行金币
        var sp5 = new cc.Sprite("Bank/icon_bank_save.png");
        sp5.setPosition(785, 425);
        this._takesaveArea.addChild(sp5);
        var sp6 = new cc.Sprite("Bank/text_bank_save.png");
        sp6.setPosition(915, 443);
        this._takesaveArea.addChild(sp6);

        this._txtInsure = new cc.LabelAtlas(string_formatNumberThousands(GlobalUserItem.lUserInsure, true, "/"), "Bank/bank_num_1.png", 19, 24, "/");
        this._txtInsure.setPosition(853, 396);
        this._txtInsure.setAnchorPoint(cc.p(0, 0.5));
        this._takesaveArea.addChild(this._txtInsure);

        //其他功能
        this._txtSaveTakeChs = new cc.LabelAtlas("", "Bank/bank_num_0.png", 30, 32, "0");
        this._txtSaveTakeChs.setPosition(898, 305);
        this._txtSaveTakeChs.setAnchorPoint(cc.p(0, 0.5));
        this._takesaveArea.addChild(this._txtSaveTakeChs);

        var btn2 = new ccui.Button("Bank/bt_bank_check.png", "");
        btn2.setPosition(990, 117);
        btn2.setTag(BankLayer.BT_CHECK);
        this._takesaveArea.addChild(btn2);
        btn2.addTouchEventListener(btcallback);
        this._notifyText = new cc.LabelTTF("提示：存入游戏币免手续费，取出将扣除 的手续费。存款无需输入银行密码。", res.round_body_ttf, 24);
        this._takesaveArea.addChild(this._notifyText);
        this._notifyText.setColor(cc.color(136, 164, 224, 255));
        this._notifyText.setPosition(1250 / 2, 38);

        //存款按钮
        var btn3 = new ccui.Button("Bank/bt_bank_save_0.png", "Bank/bt_bank_save_1.png");
        btn3.setPosition(490, 117);
        btn3.setTag(BankLayer.BT_SAVE);
        this._takesaveArea.addChild(btn3);
        btn3.addTouchEventListener(btcallback);
        //取款按钮
        var btn4 = new ccui.Button("Bank/bt_bank_take_0.png", "Bank/bt_bank_take_1.png");
        btn4.setPosition(763, 117);
        btn4.setTag(BankLayer.BT_TAKE);
        this._takesaveArea.addChild(btn4);
        btn4.addTouchEventListener(btcallback);

        var btn5 = new cc.Sprite("Bank/text_bank_take.png");
        btn5.setPosition(305, 305);
        this._takesaveArea.addChild(btn5);
        var sp7 = new cc.Sprite("Bank/text_bank_password.png");
        sp7.setPosition(305, 210);
        this._takesaveArea.addChild(sp7);

        //金额大写提示
        this.m_textNumber = new ClipText().createClipText(cc.size(350, 24), "", res.round_body_ttf, 24);
        this.addChild(this.m_textNumber);
        this.m_textNumber.setPosition(930, 335);
        this.m_textNumber.setAnchorPoint(cc.p(0, 0.5));

        //金额输入
        this.edit_Score = new cc.EditBox(cc.size(492, 69), new ccui.Scale9Sprite("Bank/bank_frame_1.png"));
        this.edit_Score.setPosition(635, 305);
        this.edit_Score.setFontName(res.round_body_ttf);
        this.edit_Score.setPlaceholderFontName(res.round_body_ttf);
        this.edit_Score.setFontSize(24);
        this.edit_Score.setPlaceholderFontSize(24);
        this.edit_Score.setMaxLength(13);
        this.edit_Score.setFontColor(cc.color(255, 255, 255, 255));
        this.edit_Score.setInputMode(cc.EDITBOX_INPUT_MODE_NUMERIC);
        this.edit_Score.setTag(BankLayer.EDIT_SAVEORTAKE);
        this.edit_Score.setPlaceHolder("请输入操作金额");
        this._takesaveArea.addChild(this.edit_Score);
        this.edit_Score.registerScriptEditBoxHandler(editHanlder);

        //密码输入
        this.edit_Password = new cc.EditBox(cc.size(492, 69), new ccui.Scale9Sprite("Bank/bank_frame_1.png"));
        this.edit_Password.setPosition(635, 210);
        this.edit_Password.setFontName(res.round_body_ttf);
        this.edit_Password.setPlaceholderFontName(res.round_body_ttf);
        this.edit_Password.setFontSize(24);
        this.edit_Password.setPlaceholderFontSize(24);
        this.edit_Password.setMaxLength(32);
        this.edit_Password.setFontColor(cc.color(195, 199, 239, 255));
        this.edit_Password.setInputFlag(cc.EDITBOX_INPUT_FLAG_PASSWORD);
        this.edit_Password.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        this.edit_Password.setPlaceHolder("存款不需要密码");
        this._takesaveArea.addChild(this.edit_Password);

        //赠送界面
        this._transferArea = new cc.Layer();
        this._transferArea.setContentSize(1250, 520);
        this.addChild(this._transferArea);
        this._transferArea.setPosition(43, 25);
        this._transferArea.setVisible(false);

        var sp8 = new cc.Sprite("Bank/text_bank_preAccount.png");
        sp8.setPosition(300, 425);
        this._transferArea.addChild(sp8);
        var sp9 = new cc.Sprite("Bank/text_bank_preGold.png");
        sp9.setPosition(300, 335);
        this._transferArea.addChild(sp9);
        var sp10 = new cc.Sprite("Bank/text_bank_password.png");
        sp10.setPosition(300, 240);
        this._transferArea.addChild(sp10);

        var btn6 = new ccui.Button("Bank/bt_bank_present_0.png", "Bank/bt_bank_present_1.png");
        btn6.setPosition(625, 117);
        btn6.setTag(BankLayer.BT_TRANSFER);
        this._transferArea.addChild(btn6);
        btn6.addTouchEventListener(btcallback);

        this.edit_TransferDst = new cc.EditBox(cc.size(492, 69), new ccui.Scale9Sprite("Bank/bank_frame_1.png"));
        this.edit_TransferDst.setPosition(625, 425);
        this.edit_TransferDst.setFontName(res.round_body_ttf);
        this.edit_TransferDst.setPlaceholderFontName(res.round_body_ttf);
        this.edit_TransferDst.setFontSize(24);
        this.edit_TransferDst.setPlaceholderFontSize(24);
        this.edit_TransferDst.setMaxLength(32);
        this.edit_TransferDst.setFontColor(cc.color(195, 199, 239, 255));
        this.edit_TransferDst.setTag(BankLayer.EDIT_TRANSFER_DST);
        this.edit_TransferDst.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        this.edit_TransferDst.setPlaceHolder("请输入赠送的ID");
        this._transferArea.addChild(this.edit_TransferDst);

        this.edit_TransferScore = new cc.EditBox(cc.size(492, 69), new ccui.Scale9Sprite("Bank/bank_frame_1.png"));
        this.edit_TransferScore.setPosition(625, 335);
        this.edit_TransferScore.setFontName(res.round_body_ttf);
        this.edit_TransferScore.setPlaceholderFontName(res.round_body_ttf);
        this.edit_TransferScore.setFontSize(24);
        this.edit_TransferScore.setPlaceholderFontSize(24);
        this.edit_TransferScore.setMaxLength(13);
        this.edit_TransferScore.setFontColor(cc.color(255, 255, 255, 255));
        this.edit_TransferScore.setInputMode(cc.EDITBOX_INPUT_MODE_NUMERIC);
        this.edit_TransferScore.setPlaceHolder("请输入操作金额");
        this._transferArea.addChild(this.edit_TransferScore);
        this.edit_TransferScore.registerScriptEditBoxHandler(editHanlder);

        //密码输入
        this.edit_TransferPassword = new cc.EditBox(cc.size(492, 69), new ccui.Scale9Sprite("Bank/bank_frame_1.png"));
        this.edit_TransferPassword.setPosition(625, 240);
        this.edit_TransferPassword.setFontName(res.round_body_ttf);
        this.edit_TransferPassword.setPlaceholderFontName(res.round_body_ttf);
        this.edit_TransferPassword.setFontSize(24);
        this.edit_TransferPassword.setPlaceholderFontSize(24);
        this.edit_TransferPassword.setMaxLength(32);
        this.edit_TransferPassword.setInputFlag(cc.EDITBOX_INPUT_FLAG_PASSWORD);
        this.edit_TransferPassword.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        this.edit_TransferPassword.setFontColor(cc.color(195, 199, 239, 255));
        this.edit_TransferPassword.setPlaceHolder("请输入银行密码");
        this._transferArea.addChild(this.edit_TransferPassword);

        //其他功能
        this._txtPresentChs = new cc.LabelAtlas("", "Bank/bank_num_0.png", 30, 32, "0");
        this._txtPresentChs.setPosition(898, 305);
        this._txtPresentChs.setAnchorPoint(cc.p(0, 0.5));
        this._transferArea.addChild(this._txtPresentChs);
        var btn7 = new ccui.Button("Bank/bt_bank_check.png", "");
        btn7.setPosition(990, 117);
        btn7.setTag(BankLayer.BT_CHECK);
        this._transferArea.addChild(btn7);
        btn7.addTouchEventListener(btcallback);
        this._notifyTextPresent = new cc.LabelTTF("提示：存入游戏币免手续费，取出将扣除 的手续费。存款无需输入银行密码。", res.round_body_ttf, 24);
        this._transferArea.addChild(this._notifyTextPresent);
        this._notifyTextPresent.setColor(cc.color(136, 164, 224, 255));
        this._notifyTextPresent.setPosition(1250 / 2, 38);

        //提示区域
        this._notifyLayer = new ccui.Layout();
        this._notifyLayer.setContentSize(yl.WIDTH, yl.HEIGHT);
        this._notifyLayer.setPosition(0, 0);
        this.addChild(this._notifyLayer);
        this._notifyLayer.setVisible(false);
        this._notifyLayer.setTouchEnabled(true);
        this._notifyLayer.setSwallowTouches(true);

        var sp11 = new cc.Sprite("General/frame_0.png");
        sp11.setPosition(yl.WIDTH / 2, yl.HEIGHT / 2);
        this._notifyLayer.addChild(sp11);

        var btn8 = new ccui.Button("General/bt_close_0.png", "General/bt_close_1.png");
        btn8.setPosition(1070, 560);
        btn8.setTag(BankLayer.BT_CLOSE);
        this._notifyLayer.addChild(btn8);
        btn8.addTouchEventListener(btcallback);

        var sp12 = new cc.Sprite("General/title_general.png");
        sp12.setPosition(yl.WIDTH / 2, 530);
        this._notifyLayer.addChild(sp12);

        var la1 = new cc.LabelTTF("初次使用，请先开通银行！", res.round_body_ttf, 24);
        this._notifyLayer.addChild(la1);
        la1.setColor(cc.color(255, 255, 255, 255));
        la1.setPosition(yl.WIDTH / 2, 430);

        var btn9 = new ccui.Button("General/bt_cancel_0.png", "General/bt_cancel_1.png");
        btn9.setPosition(529, 239);
        btn9.setTag(BankLayer.BT_CLOSE);
        this._notifyLayer.addChild(btn9);
        btn9.addTouchEventListener(btcallback);

        var btn10 = new ccui.Button("General/bt_confirm_0.png", "General/bt_confirm_1.png");
        btn10.setPosition(809, 239);
        btn10.setTag(BankLayer.BT_ENABLE);
        this._notifyLayer.addChild(btn10);
        btn10.addTouchEventListener(btcallback);

        if (0 == GlobalUserItem.cbInsureEnabled) {
            this.initEnableBankLayer();
        }
    },

    initEnableBankLayer : function() {
        var btcallback = function (ref, type) {
            if (type == ccui.Widget.TOUCH_ENDED) {
                this.onButtonClickedEvent(ref.getTag(), ref);
            }
        }
        var areaWidth = yl.WIDTH;

        //开通区域
        this._enableLayer = new ccui.Layout();
        this._enableLayer.setContentSize(yl.WIDTH, yl.HEIGHT);
        this._enableLayer.setPosition(0, 0);
        this.addChild(this._enableLayer);
        this._enableLayer.setTouchEnabled(true);
        this._enableLayer.setSwallowTouches(true);

        //显示标题
        var frame = cc.spriteFrameCache.getSpriteFrame("sp_top_bg.png");
        if (null != frame) {
            var sp = new cc.Sprite(frame);
            sp.setPosition(areaWidth / 2, 700);
            this._enableLayer.addChild(sp);
        }
        var sp1 = new cc.Sprite("Bank/title_bank_enable.png");
        sp1.setPosition(areaWidth / 2, 700);
        this._enableLayer.addChild(sp1);
        //返回按钮
        var btn1 = new ccui.Button("bt_return_0.png", "bt_return_1.png");
        btn1.setPosition(75, yl.HEIGHT - 51);
        btn1.setTag(BankLayer.BT_ENABLE_RETURN);
        this._enableLayer.addChild(btn1);
        btn1.addTouchEventListener(btcallback);
        //背景框
        frame = cc.spriteFrameCache.getSpriteFrame("sp_public_frame_0.png");
        if (null != frame) {
            var sp = new cc.Sprite(frame);
            sp.setPosition(areaWidth / 2, 325);
            this._enableLayer.addChild(sp);
        }
        //银行密码提示
        var sp2 = new cc.Sprite("Bank/text_setpass_enable.png");
        sp2.setPosition(340, 425);
        this._enableLayer.addChild(sp2);
        var sp3 = new cc.Sprite("Bank/text_confirm_enable.png");
        sp3.setPosition(340, 305);
        this._enableLayer.addChild(sp3);

        //密码输入
        this.edit_EnablePassword = new cc.EditBox(cc.size(492, 69), "Bank/bank_frame_1.png");
        this.edit_EnablePassword.setPosition(710, 425);
        this.edit_EnablePassword.setFontName(res.round_body_ttf);
        this.edit_EnablePassword.setPlaceholderFontName(res.round_body_ttf);
        this.edit_EnablePassword.setFontSize(24);
        this.edit_EnablePassword.setPlaceholderFontSize(24);
        this.edit_EnablePassword.setMaxLength(32);
        this.edit_EnablePassword.setInputFlag(cc.EDITBOX_INPUT_FLAG_PASSWORD);
        this.edit_EnablePassword.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        this.edit_EnablePassword.setFontColor(cc.color(195, 199, 239, 255));
        this.edit_EnablePassword.setPlaceHolder("请输入您的银行密码");
        this._enableLayer.addChild(this.edit_EnablePassword);
        //密码确认
        this.edit_EnablePassConfirm = new cc.EditBox(cc.size(492, 69), "Bank/bank_frame_1.png");
        this.edit_EnablePassConfirm.setPosition(710, 305);
        this.edit_EnablePassConfirm.setFontName(res.round_body_ttf);
        this.edit_EnablePassConfirm.setPlaceholderFontName(res.round_body_ttf);
        this.edit_EnablePassConfirm.setFontSize(24);
        this.edit_EnablePassConfirm.setPlaceholderFontSize(24);
        this.edit_EnablePassConfirm.setMaxLength(32);
        this.edit_EnablePassConfirm.setInputFlag(cc.EDITBOX_INPUT_FLAG_PASSWORD);
        this.edit_EnablePassConfirm.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        this.edit_EnablePassConfirm.setFontColor(cc.color(195, 199, 239, 255));
        this.edit_EnablePassConfirm.setPlaceHolder("请输入您的银行密码");
        this._enableLayer.addChild(this.edit_EnablePassConfirm);

        var btn3 = new ccui.Button("Bank/bt_bank_enable_0.png", "Bank/bt_bank_enable_1.png");
        btn3.setPosition(yl.WIDTH / 2, 150);
        btn3.setTag(BankLayer.BT_ENABLE_CONFIRM);
        this._enableLayer.addChild(btn3);
        btn3.addTouchEventListener(btcallback);
    },

    onFlushBank : function() {
        this.showPopWait();
        this._bankFrame.onFlushBank();
    },
	//按键监听
    onButtonClickedEvent : function(tag,sender) {
        if (tag == BankLayer.BT_TAKE) {
            this.onTakeScore();
        }
        else if (tag == BankLayer.BT_SAVE) {
            this.onSaveScore();
        }
        else if (tag == BankLayer.BT_TRANSFER) {
            this.onTransferScore();
        }
        else if (tag == BankLayer.BT_EXIT) {
            this._scene.onKeyBack();
        }
        else if (tag == BankLayer.BT_CLOSE) {
            this._notifyLayer.setVisible(false);
        }
        else if (tag == BankLayer.BT_ENABLE) {
            this._notifyLayer.setVisible(false);
            if (null != this._enableLayer) {
                this._enableLayer.runAction(cc.MoveTo.create(0.3, cc.p(0, 0)));
            }
        }
        else if (tag == BankLayer.BT_ENABLE_RETURN) {
            this._scene.onKeyBack();
        }
        else if (tag == BankLayer.BT_ENABLE_CONFIRM) {
            this.onEnableBank();
        }
        else if (tag == BankLayer.BT_CHECK) {
            this.getParent().getParent().onChangeShowMode(yl.SCENE_BANKRECORD);
        }
    },
	//输入框监听
    onEditEvent : function(event,editbox) {
        if (event == "changed") {
            var src = editbox.getText();

            var dst = src;				 //string.gsub(src,"([^0-9])","")
            //editbox.setText(dst)

            var ndst = Number(dst);
            if (typeof(ndst) == "number" && ndst < 9999999999999) {
                this.m_textNumber.setString(ExternalFun.numberTransiform(dst));
            }
            else
                this.m_textNumber.setString("");
        }
        else if (event == "return") {
            var src = editbox.getText();
            var numstr = this.m_textNumber.getString();
            if (src != numstr) {
                var dst = string.gsub(src, "([^0-9])", "");
                var ndst = Number(dst);
                if (typeof(ndst) == "number" && ndst < 9999999999999) {
                    this.m_textNumber.setString(ExternalFun.numberTransiform(dst));
                }
                else
                    this.m_textNumber.setString("");
                editbox.setText(dst);
            }
        }
    },

    onSelectedEvent : function(sender,eventType) {
        var tag = sender.getTag();

        if (tag == BankLayer.CBT_SAVEORTAKE || tag == BankLayer.CBT_TRANSFER) {
            var transfermode = (tag == BankLayer.CBT_TRANSFER);
            this._cbtTransfer.setSelected(transfermode);
            this._cbtSaveTake.setSelected(!transfermode);
            this._transferArea.setVisible(transfermode);
            this._takesaveArea.setVisible(!transfermode);

            this.edit_Score.setText("");
            this.edit_TransferScore.string = "";
            this.m_textNumber.setString("");

            //手续费
            var str = sprintf("提示.存入游戏币免手续费,取出将扣除%d‰的手续费。存款无需输入银行密码。", this.m_tabBankConfigInfo.wRevenueTake);
            //调整位置
            if (transfermode) {
                this.m_textNumber.setPosition(930, 365);
                str = sprintf("提示.普通玩家游戏币赠送需扣除%d‰的手续费。", this.m_tabBankConfigInfo.wRevenueTransfer);
                if (0 != GlobalUserItem.cbMemberOrder) {
                    var vipConfig = GlobalUserItem.MemberList[GlobalUserItem.cbMemberOrder];
                    str = str + vipConfig._name + "扣除" + vipConfig._insure + "‰手续费。";
                }
                this._notifyTextPresent.setString(str);
            }
            else {
                this.m_textNumber.setPosition(930, 330);
                this._notifyText.setString(str);
            }
        }
        else if (tag == BankLayer.CBT_BY_ID || tag == BankLayer.CBT_BY_NAME) {
            var byID = (tag == BankLayer.CBT_BY_ID);
            this.edit_TransferDst.setText("");
            this.cbt_TransferByID.setSelected(byID);
            this.cbt_TransferByName.setSelected(!byID);

            var szRes = (byID && "bnak_word_targetid.png" || "bnak_word_targetname.png");
            this._labelTarget.setTexture(szRes);
        }
    },
	//开通银行
    onEnableBank : function() {

        //参数判断
        var szPass = this.edit_EnablePassword.getText();
        var szPassConfirm = this.edit_EnablePassConfirm.getText();

        if (szPass.length < 1) {
            showToast(this, "请输入银行密码！", 2);
            return;
        }
        if (szPass.length < 6) {
            showToast(this, "密码必须大于6个字符，请重新输入！", 2);
            return;
        }

        if (szPassConfirm.length < 1) {
            showToast(this, "请在确认栏输入银行密码！", 2);
            return;
        }
        if (szPassConfirm.length < 6) {
            showToast(this, "确认栏密码必须大于6个字符，请重新输入！", 2);
            return;
        }

        if (szPass != szPassConfirm) {
            showToast(this, "设置栏和确认栏的密码不相同，请重新输入！", 2);
            return;
        }

        // 与帐号不同
        if (string.lower(szPass) == string.lower(GlobalUserItem.szAccount)) {
            showToast(this, "密码不能与帐号相同，请重新输入！", 2);
            return;
        }

        // 银行不同登陆
        if (string.lower(szPass) == string.lower(GlobalUserItem.szPassword)) {
            showToast(this, "银行密码不能与登录密码一致!", 2);
            return;
        }

        this.showPopWait();
        this._bankFrame.onEnableBank(szPass);
    },
	//取款操作
    onTakeScore : function() {

        if (GlobalUserItem.cbInsureEnabled == 0) {
            this._notifyLayer.setVisible(true);
            return;
        }

        //参数判断
        var szScore = this.edit_Score.getText().replace("([^0-9])", "");
        szScore = szScore.replace("[.]", "");
        var szPass = this.edit_Password.getText();
        if (szScore.length < 1) {
            showToast(this, "请输入操作金额！", 2);
            return;
        }

        var lOperateScore = Number(szScore);
        if (lOperateScore < 1) {
            showToast(this, "请输入正确金额！", 2);
            return;
        }

        if (lOperateScore > GlobalUserItem.lUserInsure) {
            showToast(this, "您银行游戏币的数目余额不足,请重新输入游戏币数量！", 2);
            return;
        }

        if (szPass.length < 1) {
            showToast(this, "请输入银行密码！", 2);
            return;
        }
        if (szPass.length < 6) {
            showToast(this, "密码必须大于6个字符，请重新输入！", 2);
            return;
        }

        this.showPopWait();
        this._bankFrame.onTakeScore(lOperateScore, szPass);
    },
	//存款
    onSaveScore : function() {
        if (GlobalUserItem.cbInsureEnabled == 0) {
            this._notifyLayer.setVisible(true);
            return;
        }

        //参数判断
        var szScore = this.edit_Score.getText().replace("([^0-9])", "");
        szScore = szScore.replace("[.]", "");
        if (szScore.length < 1) {
            showToast(this, "请输入操作金额！", 2);
            return;
        }

        var lOperateScore = Number(szScore);

        if (lOperateScore < 1) {
            showToast(this, "请输入正确金额！", 2);
            return;
        }

        if (lOperateScore > GlobalUserItem.lUserScore) {
            showToast(this, "您所携带游戏币的数目余额不足,请重新输入游戏币数量!", 2);
            return;
        }

        this.showPopWait();

        this._bankFrame.onSaveScore(lOperateScore);
    },

    onTransferScore : function() {
        if (GlobalUserItem.cbInsureEnabled == 0) {
            this._notifyLayer.setVisible(true);
            return;
        }

        //参数判断
        var szScore = this.edit_TransferScore.getText().replace("([^0-9])", "");
        var szPass = this.edit_TransferPassword.getText();
        var szTarget = this.edit_TransferDst.getText();
        var byID = 1;								//this.cbt_TransferByID.isSelected() && 1 || 0;

        if (szScore.length < 1) {
            showToast(this, "请输入操作金额！", 2);
            return;
        }

        var lOperateScore = Number(szScore);
        if (lOperateScore < 1) {
            showToast(this, "请输入正确金额！", 2);
            return;
        }

        if (szPass.length < 1) {
            showToast(this, "请输入钱包密码！", 2);
            return;
        }
        if (szPass.length < 6) {
            showToast(this, "密码必须大于6个字符，请重新输入！", 2);
            return;
        }

        if (szTarget.length < 1) {
            showToast(this, "请输入赠送用户ID！", 2);
            return;
        }

        this.showPopWait();
        this._bankFrame.onTransferScore(lOperateScore, szTarget, szPass, byID);
    },
	//操作结果
    onBankCallBack : function(result,message) {

        this.dismissPopWait();
        if (message != null && message != "") {
            showToast(this._scene, message, 2);
        }

        if (result == 2) {
            if (GlobalUserItem.cbInsureEnabled != 0) {
                showToast(this, "银行开通成功！", 2);
                if (null != this._enableLayer) {
                    this._enableLayer.runAction(cc.MoveTo.create(0.3, cc.p(yl.WIDTH, 0)));
                }
                this.showPopWait();
                this._bankFrame.sendGetBankInfo();
            }
        }

        if (result == BankFrame.OP_ENABLE_BANK_GAME) {
            showToast(this, "银行开通成功！", 2);
            if (null != this._enableLayer) {
                this._enableLayer.runAction(cc.MoveTo.create(0.3, cc.p(yl.WIDTH, 0)));
            }
            this.showPopWait();
            this._bankFrame.onGetBankInfo();
        }

        if (result == 1) {
            this._txtScore.setString(string_formatNumberThousands(GlobalUserItem.lUserScore, true, "/"));
            this._txtInsure.setString(string_formatNumberThousands(GlobalUserItem.lUserInsure, true, "/"));
            this.edit_TransferDst.setText("");
            this.edit_TransferScore.setText("");
            this.edit_TransferPassword.setText("");
            this.edit_Score.setText("");
            this.edit_Password.setText("");
            this.m_textNumber.setString("");
            //更新大厅
            this.getParent().getParent()._gold.setString(string_formatNumberThousands(GlobalUserItem.lUserScore, true, "/"));

            if (this._bankFrame._oprateCode == BankFrame.OP_SEND_SCORE) {
                // 转账凭证
                this.showCerLayer(this._bankFrame._tabTarget);
            }
        }

        if (result == this._bankFrame.OP_GET_BANKINFO) {
            var enableTransfer = (0 == message.cbEnjoinTransfer);
            this._cbtTransfer.setEnabled(enableTransfer);
            this._cbtTransfer.setVisible(enableTransfer);
            this._cbtSaveTake.setEnabled(enableTransfer);
            this._cbtSaveTake.setVisible(enableTransfer);

            this.m_tabBankConfigInfo = message;
            //取款收费比例
            var str = sprintf("提示.存入游戏币免手续费,取出将扣除%d‰的手续费。存款无需输入银行密码。", message.wRevenueTake);
            this._notifyText.setString(str);

            this._txtInsure.setString(string_formatNumberThousands(GlobalUserItem.lUserInsure, true, "/"));
            this._txtScore.setString(string_formatNumberThousands(GlobalUserItem.lUserScore, true, "/"));
        }
    },
	//显示等待
    showPopWait : function() {
        this._scene.showPopWait();
    },
	//关闭等待
    dismissPopWait : function() {
        this._scene.dismissPopWait();
    },

    onEnterTransitionFinish : function() {
        if (1 == GlobalUserItem.cbInsureEnabled) {
            this.showPopWait();
            this._bankFrame.onGetBankInfo();
        }
    },

    onExit : function() {
        if (this._bankFrame.isSocketServer()) {
            this._bankFrame.onCloseSocket();
        }
        if (null != this._bankFrame._gameFrame) {
            this._bankFrame._gameFrame._shotFrame = null;
            this._bankFrame._gameFrame = null;
        }
    },
	// 显示凭证
    showCerLayer : function( tabData ) {
        if (tabData != null) {
            return;
        }
        var self = this;
        // 加载csb资源
        var result = ExternalFun.loadRootCSB("Bank/BankCerLayer.csb", this);
        var rootLayer = result.rootlayer;
        var csbNode = result.csbnode;
        var stamp = tabData.opTime || new Date().getTime();

        var hide = function () {
            var scale1 = new cc.ScaleTo(0.2, 0.0001);
            var call1 = new cc.CallFunc(function () {
                rootLayer.removeFromParent();
            });
            csbNode.m_imageBg.runAction(new cc.Sequence(scale1, call1));
        };
        var url = GlobalUserItem.szWXSpreaderURL || yl.HTTP_URL;
        // 截图分享
        var framesize = cc.director.getOpenGLView().getFrameSize();
        var area = cc.rect(0, 0, framesize.width, framesize.height);

        var touchFunC = function (ref, tType) {
            if (tType == ccui.Widget.TOUCH_ENDED) {
                var tag = ref.getTag();
                if (TAG_MASK == tag) {
                    hide();
                }
                else if (BTN_SHARE == tag) {
                    ExternalFun.popupTouchFilter(0, false);
                    captureScreenWithArea(area, "ce_code.png", function (ok, savepath) {
                        ExternalFun.dismissTouchFilter();
                        if (ok) {
                            MultiPlatform.getInstance().customShare(function (isok) {
                            }, "转账凭证", "分享我的转账凭证", url, savepath, "true");
                        }
                    });
                }
                else if (BTN_SAVEPIC == tag) {
                    ExternalFun.popupTouchFilter(0, false);
                    captureScreenWithArea(area, "ce_code.png", function (ok, savepath) {
                        if (ok) {
                            if (true == MultiPlatform.getInstance().saveImgToSystemGallery(savepath, stamp + "ce_code.png")) {
                                showToast(self, "您的转账凭证图片已保存至系统相册", 1);
                            }
                        }
                        self.runAction(new cc.Sequence(new cc.DelayTime(3), new cc.CallFunc(function () {
                            ExternalFun.dismissTouchFilter();
                        })));
                    });
                }
            }
        };

        // 遮罩
        var mask = csbNode.getChildByName("panel_mask");
        mask.setTag(TAG_MASK);
        mask.addTouchEventListener(touchFunC);

        // 底板
        var image_bg = csbNode.getChildByName("image_bg");
        image_bg.setTouchEnabled(true);
        image_bg.setSwallowTouches(true);
        image_bg.setScale(0.00001);
        csbNode.m_imageBg = image_bg;

        // 赠送人昵称
        var sendnick = new ClipText().createClipText(cc.size(210, 30), GlobalUserItem.szAccount, null, 30);
        sendnick.setColor(cc.c3b(79, 212, 253));
        sendnick.setAnchorPoint(cc.p(0, 0.5));
        sendnick.setPosition(cc.p(260, 450));
        image_bg.addChild(sendnick);

        // 赠送人ID
        var sendid = image_bg.getChildByName("txt_senduid");
        sendid.setString(GlobalUserItem.dwGameID + "");

        // 接收人昵称
        var recnick = new ClipText().createClipText(cc.size(210, 30), tabData.opTargetAcconts || "", null, 30);
        recnick.setColor(cc.c3b(79, 212, 253));
        recnick.setAnchorPoint(cc.p(0, 0.5));
        recnick.setPosition(cc.p(810, 450));
        image_bg.addChild(recnick);

        // 接收人ID
        var recid = image_bg.getChildByName("txt_recuid");
        var reuid = tabData.opTargetID || 0;
        recid.setString(reuid + "");

        // 赠送游戏币
        var sendcount = image_bg.getChildByName("atlas_sendnum");
        var count = tabData.opScore || 0;
        sendcount.setString("" + count);

        // 大写
        var szcount = image_bg.getChildByName("txt_sendnum");
        var szstr = "";
        if (count < 9999999999999) {
            szstr = ExternalFun.numberTransiform(count);
        }
        szcount.setString(szstr);

        // 日期
        var txtdate = image_bg.getChildByName("txt_date");
        var tt = os.date("*t", stamp);
        txtdate.setString(sprintf("%d.%02d.%02d-%02d.%02d.%02d", tt.year, tt.month, tt.day, tt.hour, tt.min, tt.sec));

        // 凭证
        var cer = image_bg.getChildByName("txt_cerno");
        cer.setString(md5(stamp));

        // 分享
        var btn = image_bg.getChildByName("btn_share");
        btn.setTag(BTN_SHARE);
        btn.addTouchEventListener(touchFunC);

        // 保存
        btn = image_bg.getChildByName("btn_save");
        btn.setTag(BTN_SAVEPIC);
        btn.addTouchEventListener(touchFunC);

        // 加载动画
        image_bg.runAction(new cc.ScaleTo(0.2, 1.0));
    }
});
// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new BankLayer();
//         this.addChild(layer);
//     }
// });