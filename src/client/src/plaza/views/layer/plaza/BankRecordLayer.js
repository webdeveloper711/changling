
var BankRecordLayer = cc.Layer.extend({
    ctor : function(scene) {
        this._super();
        this._scene = scene;
        var self = this;

        // this.registerScriptHandler(function (eventType) {
        //     if (eventType === "enterTransitionFinish") {	// 进入场景而且过渡动画结束时候触发。
        //         self.onEnterTransitionFinish();
        //     } else if (eventType === "exitTransitionStart") {// 退出场景而且开始过渡动画时候触发。
        //         self.onExitTransitionStart();
        //     }
        // });

        this._bankRecordList = [];

        var frame = cc.spriteFrameCache.getSpriteFrame("sp_top_bg.png");
        if (null != frame) {
            var sp = new cc.Sprite(frame);
            sp.setPosition(yl.WIDTH / 2, yl.HEIGHT - 51);
            sp.setVisible(false);
            this.addChild(sp);
        }

        frame = cc.spriteFrameCache.getSpriteFrame("sp_public_frame_0.png");
        if (null != frame) {
            var sp = new cc.Sprite(frame);
            sp.setPosition(yl.WIDTH / 2, 375);
            this.addChild(sp);
        }
        s1 = new cc.Sprite(res.title_bankrecord_png);
        s1.setPosition(yl.WIDTH / 2, yl.HEIGHT - 70);
        this.addChild(s1);
        s2 = new cc.Sprite(res.frame_back_2_png);
        s2.setPosition(yl.WIDTH / 2, 326);
        this.addChild(s2);

        b1 = new ccui.Button(res.bankrecord_bt_return_0_png, res.bankrecord_bt_return_1_png);
        b1.setPosition(yl.WIDTH - 100, yl.HEIGHT - 60);
        this.addChild(b1);
        b1.addTouchEventListener(function (ref, type) {
            if (type === ccui.Widget.TOUCH_ENDED) {
                self._scene.onKeyBack();
            }
        });

        //标题列
        s3 = new cc.Sprite(res.table_bankrecord_line_png);
        s3.setPosition(yl.WIDTH / 2, 520);
        this.addChild(s3);
        s3.setVisible(false);

        s4 = new cc.Sprite(res.title_bankrecord_0_png);
        s4.setPosition(230, 550);
        this.addChild(s4);
        s4.setVisible(false);

        s5 = new cc.Sprite(res.title_bankrecord_1_png);
        s5.setPosition(500, 550);
        this.addChild(s5);
        s5.setVisible(false);

        s6 = new cc.Sprite(res.title_bankrecord_2_png);
        s6.setPosition(700, 550);
        this.addChild(s6);
        s6.setVisible(false);

        s7 = new cc.Sprite(res.title_bankrecord_3_png);
        s7.setPosition(900, 550);
        this.addChild(s7);
        s7.setVisible(false);
        s8 = new cc.Sprite(res.title_bankrecord_4_png);
        s8.setPosition(1110, 550);
        this.addChild(s8);
        s8.setVisible(false);

        //无记录提示
        this._nullTipLabel = new cc.LabelTTF("没有银行记录", res.round_body_ttf, 32);
        this._nullTipLabel.setPosition(yl.WIDTH / 2, 326);
        this._nullTipLabel.setVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
        this._nullTipLabel.setFontFillColor(cc.color(206, 175, 255, 255));
        // this._nullTipLabel.setTextColor(cc.color(206, 175, 255, 255));
        this._nullTipLabel.setAnchorPoint(cc.p(0.5, 0.5));
        this.addChild(this._nullTipLabel);

        //记录列表
        //todo
        // this._listView = new cc.TableView(this, cc.size(1161, 420));
        // this._listView.setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL);
        // this._listView.setPosition(cc.p(90, 62));
        // this._listView.setDelegate();
        // this.addChild(this._listView);
        // this._listView.setVerticalFillOrder(cc.TABLEVIEW_FILL_TOPDOWN);
        // this._listView.registerScriptHandler(this.cellSizeForTable, cc.TABLECELL_SIZE_FOR_INDEX);
        // this._listView.registerScriptHandler(this.tableCellAtIndex, cc.TABLECELL_SIZE_AT_INDEX);
        // this._listView.registerScriptHandler(this.numberOfCellsInTableView, cc.NUMBER_OF_CELLS_IN_TABLEVIEW);

        s9 = new cc.Sprite(res.frame_back_1_png);
        s9.setPosition(yl.WIDTH / 2, 326);
        this.addChild(s9);
        s9.setVisible(false);

    },

    // 进入场景而且过渡动画结束时候触发。
    onEnterTransitionFinish : function() {
        this._scene.showPopWait();
        var self = this;

        appdf.onHttpJsionTable(yl.HTTP_URL + "/WS/MobileInterface.ashx", "GET", "action=getbankrecord&userid=" + GlobalUserItem.dwUserID + "&signature=" + GlobalUserItem.getSignature((new Date()).getTime()) + "&time=" + (new Date()).getTime() + "&number=20&page=1", function (jstable, jsdata) {
            self._scene.dismissPopWait();
            if (jstable) {
                var code = jstable["code"];
                if (Number(code) === 0) {
                    var datax = jstable["data"];
                    if (datax) {
                        var valid = datax["valid"];
                        if (valid === true) {
                            var listcount = datax["total"];
                            var list = datax["list"];
                            if (typeof(list) === "array") {
                                for (i = 0; i < list.length; i++) {
                                    var item = [];
                                    item.tradeType = list[i]["TradeTypeDescription"];
                                    item.swapScore = Number(list[i]["SwapScore"]);
                                    item.revenue = Number(list[i]["Revenue"]);
                                    item.date = GlobalUserItem.getDateNumber(list[i]["CollectDate"]);
                                    item.id = list[i]["TransferAccounts"];
                                    self._bankRecordList.push(item);
                                }
                            }
                        }
                    }
                }

                self.onUpdateShow();
            } else {
                showToast(self, "抱歉，获取银行记录信息失败！", 2, cc.color(250, 0, 0));
            }
        });
        return this;
    },

    // 退出场景而且开始过渡动画时候触发。
    onExitTransitionStart : function () {
        return this;
    },

    onUpdateShow : function () {
        cc.log("BankRecordLayer.onUpdateShow");

        if (!this._bankRecordList) {
            cc.log("this._nullTipLabel.setVisible(true)");
            this._nullTipLabel.setVisible(true);
        } else {
            this._nullTipLabel.setVisible(false);
        }

        this._listView.reloadData();

    },

    ////////////////////////////////////////////////////////////////////-

    //子视图大小
    cellSizeForTable : function (view, idx) {
        return [1161 , 60];
    },

    //子视图数目
    numberOfCellsInTableView : function(view) {
        //todo
        // return view.getParent()._bankRecordList.length;
    },

    //获取子视图
    tableCellAtIndex : function (view, idx) {
        var cell = view.dequeueCell();

        //todo
        var item = [];
        // var item = view.getParent()._bankRecordList[idx + 1];

        var width = 1161;
        //var height= 76
        var height = 60;

        if (!cell) {
            cell = new cc.TableViewCell();
        } else {
            cell.removeAllChildren();
        }

        s1 = new cc.Sprite("BankRecord/table_bankrecord_cell_" + (idx % 2) + ".png");
        s1.setPosition(width / 2, height / 2);
        cell.addChild(s1);
        s1.setVisible(false);

        //日期
        var date = new Date();
        s2 = new cc.LabelTTF(date, res.round_body_ttf, 18);
        s2.setPosition(160, height / 2);
        s2.setVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
        s2.setFontFillColor(cc.color(73, 53, 0, 255));
        s2.setAnchorPoint(cc.p(0.5, 0.5));
        cell.addChild(s2);

        s3 = new cc.LabelTTF(item.tradeType, res.round_body_ttf, 24);
        s3.setPosition(370, height / 2);
        s3.setVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
        s3.setFontFillColor(cc.color(73, 53, 0, 255));
        s3.setAnchorPoint(cc.p(0.5, 0.5));
        cell.addChild(s3);

        s4 = new cc.LabelTTF(string_formatNumberThousands(item.swapScore, true, ","), res.round_body_ttf, 24);
        s4.setPosition(580, height / 2);
        s4.setVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
        s4.setFontFillColor(cc.color(73, 53, 0, 255));
        s4.setAnchorPoint(cc.p(0.5, 0.5));
        cell.addChild(s4);

        s5 = new cc.LabelTTF(item.id, res.round_body_ttf, 24);
        s5.setPosition(760, height / 2);
        s5.setVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
        s5.setFontFillColor(cc.color(73, 53, 0, 255));
        s5.setAnchorPoint(cc.p(0.5, 0.5));
        cell.addChild(s5);

        s6 = new cc.LabelTTF(string_formatNumberThousands(item.revenue, true, ","), res.round_body_ttf, 24);
        s6.setPosition(1010, height / 2);
        s6.setVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
        s6.setFontFillColor(cc.color(73, 53, 0, 255));
        s6.setAnchorPoint(cc.p(0.5, 0.5));
        cell.addChild(s6);

        return cell;
    }
});
// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new BankRecordLayer();
//         this.addChild(layer);
//     }
// });