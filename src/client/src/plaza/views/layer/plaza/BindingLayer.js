
var BindingLayer = cc.Layer.extend({
    BT_BINDING			: 15,
    BT_BINDINGREGISTER  : 16,


    ctor : function (scene) {
        this._super();
        this._scene = scene;
        var self = this;

        // this.registerScriptHandler = function (eventType) {
        //     if (eventType === "enterTransitionFinish")	// 进入场景而且过渡动画结束时候触发。
        //         self.onEnterTransitionFinish();
        //     else if (eventType === "exitTransitionStart")	// 退出场景而且开始过渡动画时候触发。
        //         self.onExitTransitionStart();
        //     else if (eventType === "exit") {
        //         if (self._modifyFrame.isSocketServer())
        //             self._modifyFrame.onCloseSocket();
        //     }
        // };

        //按钮回调
        this._btcallback = function (ref, type) {
            if (type === ccui.Widget.TOUCH_ENDED)
                self.onButtonClickedEvent(ref.getTag(), ref);

        };

        //网络回调
        var modifyCallBack = function (result, message) {
            this.onModifyCallBack(result, message);
        };
        //todo
        //网络处理
        // this._modifyFrame = ModifyFrame.create(this, modifyCallBack);

        var frame = cc.spriteFrameCache.getSpriteFrame("sp_top_bg.png");
        if (null != frame) {
            var sp = new cc.Sprite(frame);
            sp.setPosition(yl.WIDTH / 2, yl.HEIGHT - 51);
            this.addChild(sp);
        }
        var s1 = new cc.Sprite(res.title_binding_png);
        s1.setPosition(yl.WIDTH / 2, yl.HEIGHT - 51);
        this.addChild(s1);
        frame = cc.spriteFrameCache.getSpriteFrame("sp_public_frame_0.png");
        if (null != frame) {
            var sp = new cc.Sprite(frame);
            sp.setPosition(yl.WIDTH / 2, 320);
            this.addChild(sp);
        }
        var b1 = new ccui.Button(res.bt_return_0_png, res.bt_return_1_png);
        b1.setPosition(75, yl.HEIGHT - 51);
        this.addChild(b1);
        b1.addTouchEventListener(function (ref, type) {
            if (type === ccui.Widget.TOUCH_ENDED)
                self._scene.onKeyBack();

        });

        //提示
        var s2 = new cc.Sprite(res.text_binding_0_png);
        s2.setPosition(yl.WIDTH / 2, 503);
        this.addChild(s2);

        var s3 = new cc.Sprite(res.text_binding_1_png);
        s3.setPosition(505, 390);
        s3.setAnchorPoint(cc.p(1, 0.5));
        this.addChild(s3);

        var s4 = new cc.Sprite(res.text_binding_2_png);
        s4.setPosition(505, 284);
        s4.setAnchorPoint(cc.p(1, 0.5));
        this.addChild(s4);

        //账号输入
        this.edit_Account = new cc.EditBox(cc.size(492, 69), ccui.Scale9Sprite("Binding/frame_binding_0.png"));
        this.edit_Account.setPosition(515, 390);
        this.edit_Account.setAnchorPoint(cc.p(0.0, 0.5));
        this.edit_Account.setFontName(res.round_body_ttf);
        this.edit_Account.setPlaceholderFontName(res.round_body_ttf);
        this.edit_Account.setFontSize(24);
        this.edit_Account.setMaxLength(32);
        this.edit_Account.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        this.addChild(this.edit_Account);

        //密码输入
        this.edit_Password = new cc.EditBox(cc.size(492, 69), ccui.Scale9Sprite("Binding/frame_binding_0.png"));
        this.edit_Password.setPosition(515, 284);
        this.edit_Password.setAnchorPoint(cc.p(0.0, 0.5));
        this.edit_Password.setFontName(res.round_body_ttf);
        this.edit_Password.setPlaceholderFontName(res.round_body_ttf);
        this.edit_Password.setFontSize(24);
        this.edit_Password.setMaxLength(32);
        this.edit_Password.setInputFlag(cc.EDITBOX_INPUT_FLAG_PASSWORD);
        this.edit_Password.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        this.addChild(this.edit_Password);

        //绑定按钮
        var b2 = new ccui.Button(res.bt_binding_0_png, res.bt_binding_0_png);
        b2.setTag(this.BT_BINDING);
        b2.setPosition(yl.WIDTH / 2, 140);
        this.addChild(b2);
        b2.addTouchEventListener(this._btcallback);

        //注册绑定按钮
        var b3 = new ccui.Button(res.bt_registerbind_png, res.bt_registerbind_png);
        b3.setTag(this.BT_BINDINGREGISTER);
        b3.setPosition(yl.WIDTH - 200, 70);
        this.addChild(b3);
        b3.addTouchEventListener(this._btcallback);

    },

    // 进入场景而且过渡动画结束时候触发。
    onEnterTransitionFinish : function () {
        return this;
    },

    // 退出场景而且开始过渡动画时候触发。
    onExitTransitionStart : function () {
        return this;
    },

    //按键监听
    onButtonClickedEvent : function (tag,sender) {
        if (tag === this.BT_BINDING) {
            var szAccount = this.edit_Account.string;
            var szPassword = this.edit_Password.string;
            //输入检测
            if (szAccount === null) {
                showToast(this, "游戏帐号必须为6~31个字符，请重新输入！", 2, cc.color(250, 0, 0, 255));
                return;
            }
            if (null === szPassword) {
                showToast(this, "密码必须大于6个字符，请重新输入！", 2, cc.color(250, 0, 0, 255));
                return;
            }
            var len = szAccount.length;
            if (len < 6 || len > 31) {
                showToast(this, "游戏帐号必须为6~31个字符，请重新输入！", 2, cc.color(250, 0, 0, 255));
                return;
            }

            len = szPassword.length;
            if (len < 6) {
                showToast(this, "密码必须大于6个字符，请重新输入！", 2, cc.color(250, 0, 0, 255));
                return;
            }
            this.szAccount = szAccount;
            this.szPassword = szPassword;

            var tips = "绑定帐号后该游客信息将与新帐号合并，游客帐号将会被注销，绑定成功之后需要重新登录,是否绑定帐号?";
            this._queryDialog = QueryDialog.create(tips, function (ok) {
                if (ok === true)
                    self.bindingAccount();

                self._queryDialog = null;
            });
            this._queryDialog.setCanTouchOutside(false);
            this.addChild(this._queryDialog);
        } else if (tag === this.BT_BINDINGREGISTER) {
            this._scene.onChangeShowMode(yl.SCENE_BINDINGREG);
        }
    },

    //操作结果
    onModifyCallBack : function(result,message) {
        cc.log("======== BindingLayer..onModifyCallBack ========");

        this._scene.dismissPopWait();
        if (message != null && message != "")
            showToast(this, message, 2);

        if (result === 2) {
            this._scene.showPopWait();
            GlobalUserItem.setBindingAccount();
            GlobalUserItem.szPassword = this.szPassword;
            GlobalUserItem.szAccount = this.szAccount;
            //保存数据
            GlobalUserItem.onSaveAccountConfig();

            this.runAction(new cc.Sequence(cc.DelayTime(1.0), cc.CallFunc(function () {
                self._scene.dismissPopWait();

                //重新登录
                GlobalUserItem.nCurRoomIndex = -1;
                self.getParent().getParent().getApp().enterSceneEx(appdf.CLIENT_SRC + "plaza.views.LogonScene", "FADE", 1);
                GlobalUserItem.reSetData();
                //读取配置
                GlobalUserItem.LoadData();
                //断开好友服务器
                FriendMgr.getInstance().reSetAndDisconnect();
                //通知管理
                NotifyMgr.getInstance().clear();
            })));
        }
    },

    bindingAccount : function () {
        this._scene.showPopWait();
        cc.log(this.szPassword);
        this._modifyFrame.onAccountBinding(this.szAccount, md5(this.szPassword));
    }
});

// var ModifyFrame = appdf.req(appdf.CLIENT_SRC + "plaza.models.ModifyFrame");
// var QueryDialog = require("app.views.layer.other.QueryDialog");
// var NotifyMgr = appdf.req(appdf.EXTERNAL_SRC  +  "NotifyMgr");

// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new BindingLayer();
//         this.addChild(layer);
//     }
// });