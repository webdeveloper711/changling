
// var ModifyFrame = appdf.req(appdf.CLIENT_SRC + "plaza.models.ModifyFrame");
// var NotifyMgr = appdf.req(appdf.EXTERNAL_SRC  +  "NotifyMgr");
// var ExternalFun = appdf.req(appdf.EXTERNAL_SRC  +  "ExternalFun");
var BindingRegisterLayer = cc.Layer.extend({
    BT_REGISTER : 1,
    BT_RETURN   : 2,
    BT_AGREEMENT : 3,
    CBT_AGREEMENT : 4,
    bAgreement : false,
    ctor : function (scene) {
        this._super();
        this._scene = scene;
        var self = this;

        var btcallback = function (ref, type) {
            if (type === ccui.Widget.TOUCH_ENDED)
                self.onButtonClickedEvent(ref.getTag(), ref);

        };

        //网络回调
        var modifyCallBack = function (result, message) {
            self.onModifyCallBack(result, message);
        };
        //todo
        //网络处理
        // this._modifyFrame = ModifyFrame.create(this, modifyCallBack);

        //Top背景
        var frame = cc.spriteFrameCache.getSpriteFrame("sp_top_bg.png");
        if (null != frame) {
            var sp = new cc.Sprite(frame);
            sp.setPosition(yl.WIDTH / 2, yl.HEIGHT - 51);
            this.addChild(sp);
        }

        //Top标题
        var s1 = new cc.Sprite(res.title_regist_png);
        s1.setPosition(yl.WIDTH / 2, yl.HEIGHT - 51);
        this.addChild(s1);

        //Top返回
        var b1 = new ccui.Button(res.bt_return_0_png, res.bt_return_1_png);
        b1.setTag(this.BT_RETURN);
        b1.setPosition(75, yl.HEIGHT - 51);
        this.addChild(b1);
        b1.addTouchEventListener(btcallback);

        //注册背景框
        frame = cc.spriteFrameCache.getSpriteFrame("sp_public_frame_0.png");
        if (null != frame) {
            var sp = new cc.Sprite(frame);
            sp.setPosition(yl.WIDTH / 2, 320);
            this.addChild(sp);
        }

        //帐号提示
        var s2 = new cc.Sprite(res.icon_regist_tip_png);
        s2.setPosition(293, 540);
        this.addChild(s2);

        var s3 = new cc.Sprite(res.text_regist_account_png);
        s3.setPosition(392, 540);
        this.addChild(s3);

        //账号输入
        this.edit_Account = new cc.EditBox(cc.size(490, 67), ccui.Scale9Sprite("Regist/text_field_regist.png"));
        this.edit_Account.setPosition(730, 540);
        this.edit_Account.setAnchorPoint(cc.p(0.5, 0.5));
        this.edit_Account.setFontName(res.round_body_ttf);
        this.edit_Account.setPlaceholderFontName(res.round_body_ttf);
        this.edit_Account.setFontSize(24);
        this.edit_Account.setPlaceholderFontSize(24);
        this.edit_Account.setMaxLength(31);
        this.edit_Account.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        this.edit_Account.setPlaceHolder("6-31位字符");
        this.addChild(this.edit_Account);

        //密码提示
        var s4 = new cc.Sprite(res.icon_regist_tip_png);
        s4.setPosition(293, 450);
        this.addChild(s4);

        var s5 = new cc.Sprite(res.text_regist_password_png);
        s5.setPosition(392, 450);
        this.addChild(s5);

        //密码输入
        this.edit_Password = new cc.EditBox(cc.size(490, 67), ccui.Scale9Sprite("Regist/text_field_regist.png"));
        this.edit_Password.setPosition(730, 450);
        this.edit_Password.setAnchorPoint(cc.p(0.5, 0.5));
        this.edit_Password.setFontName(res.round_body_ttf);
        this.edit_Password.setPlaceholderFontName(res.round_body_ttf);
        this.edit_Password.setFontSize(24);
        this.edit_Password.setPlaceholderFontSize(24);
        this.edit_Password.setMaxLength(26);
        this.edit_Password.setInputFlag(cc.EDITBOX_INPUT_FLAG_PASSWORD);
        this.edit_Password.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        this.edit_Password.setPlaceHolder("6-26位英文字母，数字，下划线组合");
        this.addChild(this.edit_Password);

        //确认密码提示
        var s6 = new cc.Sprite(res.icon_regist_tip_png);
        s6.setPosition(293, 358);
        this.addChild(s6);
        var s7 = new cc.Sprite(res.text_regist_confirm_png);
        s7.setPosition(392, 358);
        this.addChild(s7);

        //确认密码输入
        this.edit_RePassword = new cc.EditBox(cc.size(490, 67), ccui.Scale9Sprite("Regist/text_field_regist.png"));
        this.edit_RePassword.setPosition(730, 358);
        this.edit_RePassword.setAnchorPoint(cc.p(0.5, 0.5));
        this.edit_RePassword.setFontName(res.round_body_ttf);
        this.edit_RePassword.setPlaceholderFontName(res.round_body_ttf);
        this.edit_RePassword.setFontSize(24);
        this.edit_RePassword.setPlaceholderFontSize(24);
        this.edit_RePassword.setMaxLength(26);
        this.edit_RePassword.setInputFlag(cc.EDITBOX_INPUT_FLAG_PASSWORD);
        this.edit_RePassword.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        this.edit_RePassword.setPlaceHolder("6-26位英文字母，数字，下划线组合");
        this.addChild(this.edit_RePassword);

        //推广员
        var s8 = new cc.Sprite(res.text_regist_tuiguang_png);
        s8.setPosition(392, 268);
        this.addChild(s8);

        //推广员
        this.edit_Spreader = new cc.EditBox(cc.size(490, 67), ccui.Scale9Sprite("Regist/text_field_regist.png"));
        this.edit_Spreader.setPosition(730, 268);
        this.edit_Spreader.setAnchorPoint(cc.p(0.5, 0.5));
        this.edit_Spreader.setFontName(res.round_body_ttf);
        this.edit_Spreader.setPlaceholderFontName(res.round_body_ttf);
        this.edit_Spreader.setFontSize(24);
        this.edit_Spreader.setPlaceholderFontSize(24);
        this.edit_Spreader.setMaxLength(32);
        this.edit_Spreader.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);//setInputMode(cc.EDITBOX_INPUT_MODE_NUMERIC)
        this.edit_Spreader.setPlaceHolder("请输入推广员帐号");
        this.addChild(this.edit_Spreader);

        //条款协议
        this.cbt_Agreement = new ccui.CheckBox(res.choose_regist_0_png, res.choose_regist_0_png, res.choose_regist_1_png, "", "");
        this.cbt_Agreement.setPosition(510, 183);
        this.cbt_Agreement.setSelected(this.bAgreement);
        this.cbt_Agreement.setTag(this.CBT_AGREEMENT);
        this.addChild(this.cbt_Agreement);

        //显示协议
        var b2 = new ccui.Button(res.re_bt_regist_agreement_png, "");
        b2.setTag(this.BT_AGREEMENT);
        b2.setPosition(780, 181);
        this.addChild(b2);
        b2.addTouchEventListener(btcallback);

        //注册按钮
        var b3 = new ccui.Button(res.bt_regist_0_png, "");
        b3.setTag(this.BT_REGISTER);
        b3.setPosition(yl.WIDTH / 2, 93);
        this.addChild(b3);
        b3.addTouchEventListener(btcallback);

        //条款界面
        this._serviceView = null;
    },

    onButtonClickedEvent : function (tag,ref) {
        if (tag === this.BT_RETURN) {
            this._scene.onKeyBack();
        } else if (tag === this.BT_AGREEMENT) {
            if (this._serviceView === null) {
                this._serviceView = new ServiceLayer();
                this._serviceView.setPosition(yl.WIDTH, 0);
                this._backLayer.addChild(this._serviceView);
            } else {
                this._serviceView.stopAllActions();
            }
            this._serviceView.runAction(cc.setPositionTo(0.3, cc.p(0, 0)));
        } else if (tag === this.BT_REGISTER) {
            var szAccount = this.edit_Account.string.replace(/\s/g, '');
            var szPassword = this.edit_Password.string.replace(/\s/g, '');
            var szRePassword = this.edit_RePassword.string.replace(/\s/g, '');
            cc.log(szAccount);
            var len = szAccount.length;//#szAccount
            if (len < 6 || len > 31) {
                showToast(this, "游戏帐号必须为6~31个字符，请重新输入！", 2, cc.color(250, 0, 0, 255));
                return;
            }

            //判断emoji
            if (ExternalFun.isContainEmoji(szAccount)) {
                showToast(this, "帐号包含非法字符,请重试", 2);
                return;
            }

            //判断是否有非法字符
            if (true === ExternalFun.isContainBadWords(szAccount)) {
                showToast(this, "帐号中包含敏感字符,不能注册", 2);
                return;
            }

            len = szPassword.length;
            if (len < 6 || len > 26) {
                showToast(this, "密码必须为6~26个字符，请重新输入！", 2, cc.color(250, 0, 0, 255));
                return;
            }

            if (szPassword != szRePassword) {
                showToast(this, "二次输入密码不一致，请重新输入！", 2, cc.color(250, 0, 0, 255));
                return;
            }

            // 与帐号不同
            if (szPassword.toLowerCase() === szAccount.toLowerCase()) {
                showToast(this, "密码不能与帐号相同，请重新输入！", 2, cc.color(250, 0, 0, 255));
                return;
            }

            var bAgreement = this.getChildByTag(this.CBT_AGREEMENT).isSelected();
            if (bAgreement === false) {
                showToast(this, "请先阅读并同意《游戏中心服务条款》！", 2, cc.color(250, 0, 0, 255));
                return;
            }

            var szSpreader = this.edit_Spreader.getText().replace(" ", "");
            this._scene.showPopWait();
            this._modifyFrame.onAccountRegisterBinding(szAccount, md5(szPassword), szSpreader);
            this.szAccount = szAccount;
            this.szPassword = szPassword;
        }
    },

    setAgreement : function (bAgree) {
        this.cbt_Agreement.setSelected(bAgree);
    },

    //操作结果
    onModifyCallBack : function (result,message) {
        cc.log("======== BindingRegisterLayer..onModifyCallBack ========");

        this._scene.dismissPopWait();
        if (message != null && message != "")
            showToast(this, message, 2);


        if (result === 2) {
            this._scene.showPopWait();
            GlobalUserItem.setBindingAccount();
            GlobalUserItem.szPassword = this.szPassword;
            GlobalUserItem.szAccount = this.szAccount;
            //保存数据
            GlobalUserItem.onSaveAccountConfig();

            this.runAction(cc.Sequence(cc.DelayTime(1.0), cc.CallFunc(function () {
                self._scene.dismissPopWait();

                //重新登录
                GlobalUserItem.nCurRoomIndex = -1;
                self.getParent().getParent().getApp().enterSceneEx(appdf.CLIENT_SRC + "plaza.views.LogonScene", "FADE", 1);
                GlobalUserItem.reSetData();
                //读取配置
                GlobalUserItem.LoadData();
                //断开好友服务器
                FriendMgr.getInstance().reSetAndDisconnect();
                //通知管理
                NotifyMgr.getInstance().clear();
            })));
        }
    }
});

// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new BindingRegisterLayer();
//         this.addChild(layer);
//     }
// });