/*
author: Li
签到界面
 */
// var CheckinFrame = appdf.req(appdf.CLIENT_SRC+"plaza.models.CheckinFrame");
// var ExternalFun = appdf.req(appdf.EXTERNAL_SRC + "ExternalFun");
var qiandaoPos = [cc.p(500,480),cc.p(700,480),cc.p(900,480),cc.p(1100,480),cc.p(600,280),cc.p(800,280),cc.p(1000,280)];
var  CheckinLayer = cc.Layer.extend({

    CHECKIN_NUMBER: 7, 		//连续签到天数
    BT_EXIT: 3,
    BT_VIPGIFT: 4,
    BT_CHECKIN: 5,
    BG_CHECKIN: 6,
    ICON_CHECKIN_Y: this.BG_CHECKIN + 7,
    ICON_CHECKIN_N: this.ICON_CHECKIN_Y + 7,
    ICON_CHECKIN_DONE: this.ICON_CHECKIN_N + 7,
    ICON_GOLD: this.ICON_CHECKIN_DONE + 7,
    TEXT_GOLD: this.ICON_GOLD + 7,
    TEXT_DAY: this.TEXT_GOLD + 7,
    NUM_DAY: this.TEXT_DAY + 7,
    ICON_CHECKIN_DBG: this.NUM_DAY + 7,

    ctor: function (scene) {
        this._super(cc.color(0, 0, 0, 125));
        //注册触摸事件
        //todo
        // ExternalFun.registerTouchEvent(this, true);

        this._scene = scene;
        var self = this;

        //按钮回调
        this._btcallback = function (ref, type) {
            if (type === ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };

        //网络回调
        var checkinCallBack = function (result, message, subMessage) {
            return self.onCheckinCallBack(result, message, subMessage);
        };

        //网络处理
        //todo
        // this._checkinFrame = CheckinFrame.create(this, checkinCallBack);

        var frame = cc.spriteFrameCache.getSpriteFrame("sp_top_bg.png");
        if (null != frame) {
            var sp = new cc.Sprite(frame);
            sp.setPosition(yl.WIDTH / 2, yl.HEIGHT - 51);
            this.addChild(sp);
        }

        var sp1 = new cc.Sprite(res.title_checkin_png);
        sp1.setPosition(yl.WIDTH / 2, yl.HEIGHT - 80);
        sp1.setLocalZOrder(2);
        this.addChild(sp1);

        frame = cc.spriteFrameCache.getSpriteFrame("sp_public_frame_0.png");
        if (null != frame) {
            var sp = new cc.Sprite(frame);
            sp.setPosition(yl.WIDTH / 2, 375);
            this.addChild(sp);
        }
        frame = cc.spriteFrameCache.getSpriteFrame("sp_public_frame_2.png");
        if (null != frame) {
            var sp = new cc.Sprite(frame);
            sp.setPosition(yl.WIDTH / 2, 326);
            this.addChild(sp);
        }

        var sp2 = new cc.Sprite(res.sp_girl_png);
        sp2.setPosition(200, 300);
        this.addChild(sp2);

        var sp3 = new ccui.Button(res.ch_bt_return_0_png, res.ch_bt_return_1_png);
        sp3.setPosition(1334 - 75, yl.HEIGHT - 51);
        sp3.addTouchEventListener(function (ref, type) {
            if (type === ccui.Widget.TOUCH_ENDED)
                self._scene.onKeyBack();
        });
        this.addChild(sp3);

        //连续签到提示
        var sp4 = new cc.Sprite(res.frame_checkin_2_png);
        sp4.setPosition(yl.WIDTH / 2, 555);
        sp4.setVisible(false);
        this.addChild(sp4);

        var sp5 = new cc.Sprite(res.text_checkin_0_png);
        sp5.setPosition(yl.WIDTH / 2, 555);
        sp5.setVisible(false);
        this.addChild(sp5);

        var str = "0";
        this._txtDay = new cc.LabelAtlas("0", res.num_checkin_0_png, 22, 29, str);
        this._txtDay.setPosition(760, 555);
        this._txtDay.setAnchorPoint(cc.p(0.5, 0.5));
        this._txtDay.setVisible(false);
        this.addChild(this._txtDay);

        //签到按钮
        this._btConfig = new ccui.Button(res.bt_checkin_0_png, res.bt_checkin_1_png, res.bt_checkin_2_png);
        this._btConfig.setPosition(810, 112);
        this._btConfig.setTag(this.BT_CHECKIN);
        this.addChild(this._btConfig);
        this._btConfig.addTouchEventListener(this._btcallback);
        this._btConfig.setEnabled(GlobalUserItem.wSeriesDate === 0);

        //签到展示区域
        for (i = -1; i < this.CHECKIN_NUMBER - 1; i++) {
            //背景
            var sp6 = new cc.Sprite(res.back_checkin_0_png);
            sp6.setPosition(qiandaoPos[i + 1].x, qiandaoPos[i + 1].y);
            sp6.setTag(this.BG_CHECKIN + i);
            //.setPosition(178+163*i,326)
            this.addChild(sp6);

            //天数背景
            var sp7 = new cc.Sprite(res.text_checkin_1_png);
            sp7.setPosition(qiandaoPos[i + 1].x, qiandaoPos[i + 1].y - 100);
            sp7.setTag(this.TEXT_DAY + i);
            //.setPosition(178+163*i,470)
            this.addChild(sp7);

            //天数数字
            var la1 = new cc.LabelAtlas("" + i + 1, res.num_checkin_1_png, 19, 25, str);
            la1.setPosition(qiandaoPos[i + 1].x, qiandaoPos[i + 1].y - 100);
            la1.setAnchorPoint(cc.p(0.5, 0.5));
            la1.setTag(this.NUM_DAY + i);
            //.setPosition(178+163*i,470)
            this.addChild(la1);

            //已签到标志
            var sp8 = new cc.Sprite(res.icon_checkin_1_png);
            sp8.setPosition(qiandaoPos[i + 1].x - 50, qiandaoPos[i + 1].y + 50);
            //.setPosition(182+163*i,258)
            sp8.setTag(this.ICON_CHECKIN_DONE + i);
            sp8.setVisible(false);
            this.addChild(sp8);

            //可签到标志
            var sp9 = new cc.Sprite(res.text_checkin_2_png);
            //.setPosition(178+163*i,245)
            sp9.setPosition(qiandaoPos[i + 1].x - 50, qiandaoPos[i + 1].y + 50);
            sp9.setTag(this.ICON_CHECKIN_Y + i);
            this.addChild(sp9);

            //不可签标志
            var sp10 = new cc.Sprite(res.text_checkin_3_png);
            //.setPosition(178+163*i,245)
            sp10.setPosition(qiandaoPos[i + 1].x - 50, qiandaoPos[i + 1].y + 50);
            sp10.setTag(this.ICON_CHECKIN_N + i);
            sp10.setVisible(false);
            this.addChild(sp10);

            //金币标志
            var sp11 = new cc.Sprite(res.icon_checkin_0_png);
            sp11.setPosition(140 + 163 * i, 192);
            sp11.setTag(this.ICON_GOLD + i);
            sp11.setVisible(false);
            this.addChild(sp11);

            //金币数字
            var la2 = new cc.LabelAtlas("1000", res.num_checkin_3_png, 22, 29, str);
            //.setPosition(183+163*i,192)
            la2.setPosition(qiandaoPos[i + 1].x, qiandaoPos[i + 1].y - 60);
            la2.setAnchorPoint(cc.p(0.5, 0.5));
            la2.setTag(this.TEXT_GOLD + i);
            this.addChild(la2);
        }
        //VIP礼包按钮
        this._btVIPGift = new ccui.Button(res.bt_checkin_vip_1_png, res.bt_checkin_vip_1_png);
        this._btVIPGift.setPosition(1013, 115);
        this._btVIPGift.setTag(this.BT_VIPGIFT);
        this._btVIPGift.setVisible(false);
        this._btVIPGift.addTouchEventListener(this._btcallback);
        this.addChild(this._btVIPGift);

        cc.log("*cbMemberOrder-" + GlobalUserItem.cbMemberOrder);
        if (GlobalUserItem.cbMemberOrder === 0) {
            this.getChildByTag(this.BT_VIPGIFT).loadTextureNormal(res.bt_checkin_vip_0_png);
            this.getChildByTag(this.BT_VIPGIFT).loadTexturePressed(res.bt_checkin_vip_0_png);
        }

        //礼物列表
        this.m_giftList = null;
        this.m_spListFrame = null;
        this.m_memberGiftLayer = null;
        this.m_tabGiftList = [];
        this.m_fSix = 0;
    },
    // 进入场景而且过渡动画结束时候触发。
    onEnterTransitionFinish : function() {
        if (false === GlobalUserItem.bQueryCheckInData) {
            this._scene.showPopWait();
            this._checkinFrame.onCheckinQuery();
        }
        else
            this.onCheckinCallBack(1, null, null);
        return this;
    },

    // 退出场景而且开始过渡动画时候触发。
    onExitTransitionStart : function() {
        return this;
    },

    onExit : function(){
        if (this._checkinFrame.isSocketServer()) {
            this._checkinFrame.onCloseSocket();
        }
    },

    //按键监听
    onButtonClickedEvent : function(tag,sender) {
        if (tag === this.BT_CHECKIN) {
            this._scene.showPopWait();
            ExternalFun.enableBtn(this._btConfig, false);
            this._checkinFrame.onCheckinDone();
        }
        else if (tag === this.BT_VIPGIFT) {
            if (0 === GlobalUserItem.cbMemberOrder) {
                showToast(this, "非vip会员无法获取礼包", 2);
                return;
            }
            this._scene.showPopWait();
            this._checkinFrame.onCheckMemberGift();
        }
    },

    //操作结果
    onCheckinCallBack : function(result, message, subMessage) {
        var bRes = false;
        var self = this;
        this._scene.dismissPopWait();
        if (message != null && message != "") {
            showToast(this, message, 2);
        }

        if (result === 1) {
            this.reloadCheckin();
        }

        if (result === 10 && GlobalUserItem.bTodayChecked) {
            this._scene.coinDropDownAni(function () {
                ExternalFun.enableBtn(self._btConfig, !GlobalUserItem.bTodayChecked);
                self.reloadCheckin();
                var reword = GlobalUserItem.lRewardGold[GlobalUserItem.wSeriesDate] || 0;
                showToast(self, "恭喜您获得" + reword + "游戏币", 2);
            });
        }

        //vip礼包查询结果
        if (this._checkinFrame.QUERYMEMBERGIFT === result) {
            ExternalFun.enableBtn(this._btConfig, false, true);
            this.reloadGiftList(message, subMessage);
        }

        //vip礼包领取结果
        if (this._checkinFrame.GETMEMBERGIFT === result) {
            //会员标记当日已签到
            if (GlobalUserItem.cbMemberOrder != 0) {
                GlobalUserItem.setTodayCheckIn();
            }

            if (null != this.m_memberGiftLayer) {
                this.m_memberGiftLayer.setVisible(false);
            }
            if (GlobalUserItem.bTodayChecked === true) {
                ExternalFun.enableBtn(this._btConfig, false);
                this._btConfig.setVisible(true);
            }
            else
                ExternalFun.enableBtn(this._btConfig, true);

            if (0 != GlobalUserItem.MemberList[GlobalUserItem.cbMemberOrder]._present) {
                // 领取送金
                this._checkinFrame.sendGetVipPresend();
                bRes = true;

            }
        }
        return bRes;
    },

    reloadCheckin : function() {
        cc.log("====== CheckinLayer.reloadCheckin =======");

        var todayCheck = this.getChildByTag(this.ICON_CHECKIN_Y + GlobalUserItem.wSeriesDate);
        if (null != todayCheck) {
            todayCheck.setVisible(!GlobalUserItem.bTodayChecked);
        }
        todayCheck = this.getChildByTag(this.BG_CHECKIN + GlobalUserItem.wSeriesDate);
        if (null != todayCheck && GlobalUserItem.bTodayChecked) {
            todayCheck.setTexture("Checkin/back_checkin_1.png");
        }
        var canCheckBg = this.getChildByTag(this.ICON_CHECKIN_DBG + GlobalUserItem.wSeriesDate);
        if (null != canCheckBg) {
            canCheckBg.setVisible(!GlobalUserItem.bTodayChecked);
        }

        var otherCheck = this.getChildByTag(this.ICON_CHECKIN_N + GlobalUserItem.wSeriesDate);
        if (null != otherCheck) {
            otherCheck.setVisible(GlobalUserItem.bTodayChecked);
        }

        var haveCheck = null;
        if (GlobalUserItem.wSeriesDate != 0) {
            for (i = -1; i < GlobalUserItem.wSeriesDate - 1; i++) {
                this.getChildByTag(this.BG_CHECKIN + i).setTexture("Checkin/back_checkin_1.png");
                this.getChildByTag(this.TEXT_DAY + i).setTexture("Checkin/text_checkin_4.png");
                todayCheck = this.getChildByTag(this.ICON_CHECKIN_Y + i);
                if (null != todayCheck) {
                    todayCheck.setVisible(false);
                }
                haveCheck = this.getChildByTag(this.ICON_CHECKIN_DONE + i);
                if (null != haveCheck) {
                    haveCheck.setVisible(true);
                }
                canCheckBg = this.getChildByTag(this.ICON_CHECKIN_DBG + i);
                if (null != canCheckBg) {
                    canCheckBg.setVisible(false);
                }
                this.getChildByTag(this.ICON_GOLD + i).setTexture("Checkin/icon_checkin_2.png");
                var gold = GlobalUserItem.lRewardGold[i + 1] || 0;
                this.getChildByTag(this.TEXT_GOLD + i).setString("" + gold);
            }
        }
        for (i = GlobalUserItem.wSeriesDate; i < 6; i++) {
            this.getChildByTag(this.BG_CHECKIN + i).setTexture("Checkin/back_checkin_1.png");

            todayCheck = this.getChildByTag(this.ICON_CHECKIN_Y + i);
            if (null != todayCheck) {
                todayCheck.setVisible(false);
            }
            canCheckBg = this.getChildByTag(this.ICON_CHECKIN_DBG + i);
            if (null != canCheckBg) {
                canCheckBg.setVisible(false);
            }

            otherCheck = this.getChildByTag(this.ICON_CHECKIN_N + i);
            if (null != otherCheck) {
                otherCheck.setVisible(true);
            }
        }

        this._txtDay.setString("" + GlobalUserItem.wSeriesDate);

        if (GlobalUserItem.bTodayChecked === true) {
            ExternalFun.enableBtn(this._btConfig, false);
            this._btConfig.setVisible(true);
        }
        else
            ExternalFun.enableBtn(this._btConfig, true);

        for (i = -1; i < this.CHECKIN_NUMBER - 1; i++) {
            var reword = GlobalUserItem.lRewardGold[i + 1] || 0;
            this.getChildByTag(this.TEXT_GOLD + i).setString("" + reword + "");
        }

        cc.log("====== CheckinLayer.reloadCheckin =======");

    },

    reloadGiftList : function( tabGift ,subMessage) {
        this.m_tabGiftList = tabGift;

        if (null === this.m_giftList) {
            //加载csb资源
            //todo following statement is missing
            var tmp = ExternalFun.loadRootCSB("Checkin/MemberGiftLayer.csb", this);
            var rootLayer = tmp[0];
            var csbNode = tmp[1];
            this.m_memberGiftLayer = rootLayer;

            //背景框
            this.m_spListFrame = csbNode.getChildByName("frame_vip");

            //列表
            var content = this.m_spListFrame.getChildByName("content");
            this.m_fSix = content.getContentSize().width / 6;
            var m_tableView = new cc.TableView(content.getContentSize());
            m_tableView.setDirection(cc.SCROLLVIEW_DIRECTION_HORIZONTAL);
            m_tableView.setPosition(content.getPosition());
            m_tableView.setDelegate();
            m_tableView.registerScriptHandler(handler(this, this.cellSizeForTable), cc.TABLECELL_SIZE_FOR_INDEX);
            m_tableView.registerScriptHandler(handler(this, this.tableCellAtIndex), cc.TABLECELL_SIZE_AT_INDEX);
            m_tableView.registerScriptHandler(handler(this, this.numberOfCellsInTableView), cc.NUMBER_OF_CELLS_IN_TABLEVIEW);
            this.m_spListFrame.addChild(m_tableView);
            this.m_giftList = m_tableView;
            content.removeFromParent();

            //领取按钮
            this.m_btnGetGift = csbNode.getChildByName("sure_btn");
            this.m_btnGetGift.addTouchEventListener(function (sender, tType) {
                if (tType === ccui.Widget.TOUCH_ENDED) {
                    self._scene.showPopWait();
                    self._checkinFrame.onGetMemberGift();
                }
            });
        }
        this.m_memberGiftLayer.setVisible(true);
        this.m_giftList.reloadData();

        subMessage = subMessage || false;
        this.m_btnGetGift.setEnabled(subMessage);
    },

    getGiftCount : function() {
        if (null === this.m_tabGiftList) {
            return 0;
        }
        return this.m_tabGiftList.length;
    },

    onTouchBegan : function(touch, event) {
        if (null != this.m_memberGiftLayer && this.m_memberGiftLayer.isVisible())
            return true;
        else
            return false;
    },

    onTouchEnded : function( touch, event ) {
        var pos = touch.getLocation();
        var m_spBg = this.m_spListFrame;
        pos = m_spBg.convertToNodeSpace(pos);
        var rec = cc.rect(0, 0, m_spBg.getContentSize().width, m_spBg.getContentSize().height);
        if (false === cc.rectContainsPoint(rec, pos)) {
            this.m_memberGiftLayer.setVisible(false);

            if (GlobalUserItem.bTodayChecked === true) {
                ExternalFun.enableBtn(this._btConfig, false);
                this._btConfig.setVisible(true);
            }
            else
                ExternalFun.enableBtn(this._btConfig, true);
        }
    },

    //tablevie
    cellSizeForTable : function( view, idx ) {
        return [this.m_fSix, 112];
    },

    numberOfCellsInTableView : function( view ) {
        return this.getGiftCount();
    },

    tableCellAtIndex : function( view, idx ) {
        var cell = view.dequeueCell();
        if (null === this.m_tabGiftList) {
            return cell;
        }

        var giftitem = this.m_tabGiftList[idx + 1];
        var item = null;
        if (null === cell) {
            cell = new cc.TableViewCell();
            item = new cc.Node();
            item.setPosition(this.m_fSix * 0.5, view.getViewSize().height * 0.5);
            item.setName("gift_item_view");
            cell.addChild(item);
        }
        else
            item = cell.getChildByName("gift_item_view");
        if (null != item && null != giftitem) {
            //图片
            var frame = cc.spriteFrameCache.getSpriteFrame("icon_public_" + giftitem.id + ".png");
            if (null != frame) {
                var sp = new cc.Sprite(frame);
                var icon = item.getChildByTag(1);
                if (null === icon) {
                    icon = new cc.Sprite(sp.getSpriteFrame());
                    item.addChild(icon);
                    icon.setTag(1);
                }
                else
                    icon.setSpriteFrame(sp.getSpriteFrame());
            }

            //数量
            //todo string method
            str = sprintf("%01d", giftitem.count);
            var atlascount = item.getChildByTag(2);
            if (null === atlascount) {
                atlascount = new ccui.TextAtlas(str, "Checkin/num_vip_0.png", 17, 22, "0");
                item.addChild(atlascount);
                atlascount.setTag(2);
                atlascount.setPosition(40, -40);
            }
            else
                atlascount.setString(str);
        }

        return cell;
    }
});

// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new CheckinLayer();
//         this.addChild(layer);
//     }
// });