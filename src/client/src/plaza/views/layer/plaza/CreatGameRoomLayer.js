/*
    author : Kil
    date : 2017-12-5
 */

var  CreatGameRoomLayer = cc.Layer.extend({
    //宏定义区域
    BT_EXIT                     : 1,         //返回按钮
    CBT_MAJIANG                 : 2,         //麻将选中框
    CBT_PUKE                    : 3,         //扑克选中框

    STAT_MAJIANG                : 10,        //选择麻将的状态
    STAT_PUKE                   : 11,        //选择扑克的状态

    //根据KindID设置宏
    KIND_ZHAJINHUA              : 6,        //扎金花
    KIND_NIUNIU                 : 27,       //牛牛
    KIND_DOUDIZHU               : 200,      //斗地主
    KIND_HONGZHONGMAJIANG       : 389,      //红中麻将
    KIND_YINGKOUMAJIANG         : 390,      //营口麻将
    KIND_CHANGLINGMAJIANG       : 391,      //长岭麻将
    KIND_SHENYANGMAJIANG        : 392,      //沈阳麻将
    KIND_XIUYANMAJIANG          : 395,      //岫岩麻将
    KIND_FUSHUNMAJIANG          : 396,      //抚顺麻将
    KIND_XIAOQUMAJIANG          : 400,      //小区麻将
    KIND_HAICHENGMAJIANG        : 401,      //海城麻将
    KIND_SHIDIANBAN             : 700,      //十点半
    KIND_GONGNIU                : 701,      //拱牛
    KIND_912DOUDIZHU            : 912,      //912斗地主
    KIND_HAICHENGDOUDIZHU       : 913,      //海城斗地主

    ctor: function (scene) {
        this._super();

        this.ru = cc.director.getRunningScene();

        this._scene = scene;
        var self = this;

        var gamelist = _gameList;

        //加载csb资源
        this.Layer = ccs.load(res.CreateGameRoomLayer_json).node;
        this.addChild(this.Layer);

        //按钮监听事件
        var btcallback = function (sender, target) {
            if (target == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(sender.getTag(), sender);
            }
        };

        //选取框监听事件
        var cbtlistener = function (sender, eventType) {
            self.onSelectedEvent(sender.getTag(), sender, eventType);
        };

        //扑克麻将背景图片选取,默认选择麻将
        this.m_gametype = this.STAT_MAJIANG;
        this.m_pregamekind1 = "" + this.KIND_YINGKOUMAJIANG; // 默认营口麻将
        this.m_pregamekind2 = "" + this.KIND_912DOUDIZHU; //默认912斗地主
        this.m_pregamekind1 = sprintf("%s", this.KIND_YINGKOUMAJIANG); // 默认营口麻将
        this.m_pregamekind2 = sprintf("%s", this.KIND_912DOUDIZHU); //默认912斗地主

        //分拣数据
        this._gamelistMajiang = [];
        this._gamelistPuke = [];
        var tmp = this.getListUp(gamelist);
        this._gamelistMajiang = tmp[0];
        this._gamelistPuke = tmp[1];

        //返回按钮
        var btn_bcak = this.Layer.getChildByName("btn_back");
        btn_bcak.setTag(this.BT_EXIT);
        btn_bcak.addTouchEventListener(btcallback);

        //设置按钮背景
        this._bgmajiang = this.Layer.getChildByName("bg").getChildByName("bg_chk_majiang");
        this._bgmajiang.setVisible(this.m_gametype == this.STAT_MAJIANG);
        this._bgpuke = this.Layer.getChildByName("bg").getChildByName("bg_chk_puke");
        this._bgpuke.setVisible(this.m_gametype == this.STAT_PUKE);

        //麻将选取框
        this._cbtmajiang = this.Layer.getChildByName("CheckBox_majiang");
        this._cbtmajiang.setTag(this.CBT_MAJIANG);
        this._cbtmajiang.setSelected(this.m_gametype == this.STAT_MAJIANG);         //如果选择麻将游戏则为选中状态
        this._cbtmajiang.setTouchEnabled(this.m_gametype != this.STAT_MAJIANG);     //如果已经选择（选择麻将游戏）则不可用
        this._cbtmajiang.addEventListener(cbtlistener);

        //扑克选取框
        this._cbtpuke = this.Layer.getChildByName("CheckBox_puke");
        this._cbtpuke.setTag(this.CBT_PUKE);
        this._cbtpuke.setSelected(this.m_gametype == this.STAT_PUKE);           //如果选择扑克游戏则为选中状态
        this._cbtpuke.setTouchEnabled(this.m_gametype != this.STAT_PUKE);       //如果已经选择（选择扑克游戏）则不可用
        this._cbtpuke.addEventListener(cbtlistener);

        this._panel = this.Layer.getChildByName("Panel");
        //下载提示
        this.m_bUpdating = false;

        //游戏列表
        this._listview = this.Layer.getChildByName("ListView_checkarea");
     //   this._listview.setScrollBarEnabled(false);  //TODO
        //利用游戏列表数据和选择的游戏类型更新列表
        this.upDateListView();

    },
    //分拣数据

    // 1.根据gamelist判断加载的游戏
    // 2.当某类型的游戏约战房间数为0时，不加载该游戏
    // 3.根据常用数据排序

    getListUp : function(gamelist) {

        var gamelist = gamelist;
        var gamelist1 = [];
        var gamelist2 = [];

        for (var i = 0; i < gamelist.length; i++) {
            //排序
            if (gamelist[i]._KindID == this.m_pregamekind1)
                gamelist[i]._SortId = 1;
            else if (gamelist[i]._KindID == this.m_pregamekind2)
                gamelist[i]._SortId = 2;
            else
                gamelist[i]._SortId = gamelist[i]._SortId + 3;

            //根据游戏类型不同分列表
            if (gamelist[i]._Type == "yule") { //当游戏类型为麻将时
                //当有房间时才显示
                var count = this.checkgamecount(gamelist[i]._KindID);
                if (count > 0)
                    gamelist1.push(gamelist[i]);
            }
            else if (gamelist[i]._Type === "qipai") {
                //当有房间时才显示
                var count = this.checkgamecount(gamelist[i]._KindID);
                if (count > 0)
                    gamelist2.push(gamelist[i]);
            }
        }

        for (var i = 0; i < gamelist1.length; i++) {
            for (var j = i; j < gamelist1.length; j++) {
                if (gamelist1[i]._SortId > gamelist1[j]._SortId) {
                    temp = gamelist1[i];
                    gamelist1[i] = gamelist1[j];
                    gamelist1[j] = temp;
                }
            }
        }

        for (i = 0; i < gamelist2.length; i++) {
            for (var j = i; j < gamelist2.length; j++) {
                if (gamelist2[i]._SortId > gamelist2[j]._SortId) {
                    temp = gamelist2[i];
                    gamelist2[i] = gamelist2[j];
                    gamelist2[j] = temp;
                }
            }
        }

        return [gamelist1, gamelist2];
    },

    // 更新列表
    upDateListView : function() {
        var self = this;

        //按钮监听事件
        var btcallback = function (sender, target) {
            if (target == ccui.Widget.TOUCH_ENDED) {
                self.onGameButtonClickedEvent(sender.getTag(), sender);
            }
        };

        //清除全部按钮
        this._listview.removeAllChildren(true);

        var checklist = [];
        if (this.m_gametype === this.STAT_MAJIANG)
            checklist = this._gamelistMajiang;
        else if (this.m_gametype === this.STAT_PUKE)
            checklist = this._gamelistPuke;

        //整理列表
        if (checklist.length == 0) {
            this._panel.removeAllChildren(true);
            var lay = new cc.LabelTTF("没有相应游戏", res.round_body_ttf, 24);
            lay.setPosition(425, 274);
            lay.setColor(cc.color(100, 50, 0, 255));
            lay.setVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
            this._panel.addChild(lay);
        }

        for (var i = 0; i < checklist.length; i++) {
            var kind = checklist[i]._KindID;
            var btn = new ccui.Button("src/client/res/CreatGameRoom/game_" + kind + "_0.png", "", "src/client/res/CreatGameRoom/game_" + kind + "_1.png");
            btn.setTag(kind);
            btn._game = checklist[i];
            btn.addTouchEventListener(btcallback);
            btn.setName("btn_" + i);
            if (i == 0) {

                var gameinfo = checklist[i];

                var version = Number(_version.getResVersion(gameinfo._KindID));

                if((version == undefined) || (gameinfo._ServerResVersion > version)) {
                    cc.log("没有相应游戏或版本不对");
                } else {
                    btn.setEnabled(false);
                    this.onGameButtonClickedEvent(kind, btn);
                }
            }
            this._listview.pushBackCustomItem(btn);
        }
    },
    //游戏选择按钮事件
    onGameButtonClickedEvent : function(tag,sender) {

        //获取游戏信息
        var gameinfo = sender._game;

        //清空路径信息
        //PriRoom.getInstance().exitRoom(); //TODO
        //清空显示区
        this._panel.removeAllChildren(true);
        //更新大厅游戏信息
        this._scene.updateEnterGameInfo(gameinfo);
        GlobalUserItem.nCurGameKind = Number(gameinfo._KindID);
        GlobalUserItem.szCurGameName = gameinfo._KindName;
        //加载路径信息
        PriRoom.getInstance().enterRoom(this._scene);
        PriRoom.getInstance().getOption(gameinfo._KindID);

        //下载/更新资源
        var version = Number(_version.getResVersion(gameinfo._KindID));
        if ((version == undefined) || (gameinfo._ServerResVersion > version)) {

            this.m_downloadTip = sender;
            this.m_downloadTipOldColor = null;
            this.m_downloadTipOldColor = this.m_downloadTip.getColor();
            this.m_downloadTip.setColor(cc.color(125, 125, 125, 255));
            this.m_downloadTip.setTitleColor(cc.color(255, 255, 255, 255));
            this.m_downloadTip.setTitleFontSize(35);
            this.m_downloadTip.setTitleText("0%");
            this.m_downloadTip.setEnabled(false);

            this.updateGame(gameinfo);
        } else {
            //加载创建房间界面
            lay = PriRoom.getInstance().getTagLayer(PriRoom.LAYTAG.LAYER_CREATEPRIROOME, null, this._scene);
            lay.setName("lay");
            this._panel.addChild(lay);
            lay.setPosition(-370,-97);

            //清空选择状态
            var count = this._listview.getChildrenCount();
            for (i = 0; i < count; i++) {
                this._listview.getChildByName("btn_" + i).setEnabled(true);
            }

            //设置自身状态为选中
            sender.setEnabled(false);
        }
    },
    //选取框响应事件
    onSelectedEvent : function(tag,sender,eventType) {
        if (tag === this.CBT_MAJIANG) {
            cc.log("selected MaJiang Tag:"+ tag);
            this.m_gametype = this.STAT_MAJIANG;
        }else if (tag === this.CBT_PUKE) {
            cc.log("selected PuKe Tag:"+ tag);
            this.m_gametype = this.STAT_PUKE;
        }
        this.upDateListView();

        this._bgmajiang.setVisible(this.m_gametype == this.STAT_MAJIANG);
        this._bgpuke.setVisible(this.m_gametype == this.STAT_PUKE);
        this._cbtmajiang.setSelected(this.m_gametype == this.STAT_MAJIANG);                    //如果选择麻将游戏则为选中状态
        this._cbtmajiang.setTouchEnabled(this.m_gametype != this.STAT_MAJIANG);           //如果已经选择（选择麻将游戏）则不可用
        this._cbtpuke.setSelected(this.m_gametype == this.STAT_PUKE);                          //如果选择扑克游戏则为选中状态
        this._cbtpuke.setTouchEnabled(this.m_gametype != this.STAT_PUKE);              //如果已经选择（选择扑克游戏）则不可用
    },
    //按钮响应事件
    onButtonClickedEvent : function(tag, sender) {
        if (tag === this.BT_EXIT) { //返回按钮
            this._scene.onKeyBack();
        }
    },
    //检查游戏有没有房间
    checkgamecount : function(KindID) {
        checkKind = Number(KindID);

        for (var i = 0; i < GlobalUserItem.roomlist.length; i++) {
            var list = GlobalUserItem.roomlist[i];
            if (Number(list[0]) === checkKind) {
                if (!list[1]) {
                    break;
                }
                var nCount = 0;
                var serverList = list[1];
                serverList.forEach(function (v, k) {
                    if (v.wServerType == yl.GAME_GENRE_PERSONAL) {
                        nCount ++;
                    }
                });
                return nCount;
            }
        }
        return 0;
    },
    updateGame : function(gameinfo) {
        if (this.m_bUpdating) {
            cc.log("CreatGameRoomLayer 正在更新游戏");

            if (null != this.ru) {
                showToast(this.ru, "正在更新游戏！", 5);
            }

            return;
        }
        this.m_bUpdating = true;
        var targetPlatform = cc.sys.platform;
        if ((cc.sys.WIN32 === targetPlatform) || (cc.sys.MOBILE_BROWSER === targetPlatform) || (cc.sys.DESKTOP_BROWSER === targetPlatform)) {
            cc.log("CreatGameRoomLayer win32 跳过更新");

            if (null != this.ru) {
                showToast(this.ru, "CreatGameRoomLayer win32 跳过更新！", 5);
            }

            //更新版本号
            _version = _version.setResVersion(gameinfo._ServerResVersion, gameinfo._KindID);
            this.m_downloadTip.setColor(cc.color(255, 255, 255, 255));
            this.m_downloadTip.setTitleText("");
            this.m_downloadTip.setEnabled(true);
        }
        else {
            var cell = null;
            if (null != index) {
                cell = this._showList[index];
            }

            this.onGameUpdate(gameinfo);
        }
    },
    //更新游戏
    onGameUpdate : function(gameinfo) {
        //失败重试
        if (!gameinfo && this._update != null) {
            this.showGameUpdateWait();

            this._update.UpdateFile();
            return;
        }

        if (!gameinfo) {

            if (null != this.ru) {
                showToast(this.ru, "无效游戏信息！", 5);
            }

            showToast(this, "无效游戏信息！", 1);
            return;
        }

        this.showGameUpdateWait();

        //记录
        if (gameinfo != null) {
            this._downgameinfo = gameinfo;
        }

        //更新参数
        var newfileurl = _updateUrl + "/game/" + this._downgameinfo._Module + "/res/filemd5List.json";
        var dst = cc.sys.getWritablePath() + "game/" + this._downgameinfo._Type + "/";
        var targetPlatform = cc.sys.platform;
        if (cc.sys.WIN32 === targetPlatform) {
            dst = cc.sys.getWritablePath() + "download/game/" + this._downgameinfo._Type + "/";
        }

        var src = cc.sys.getWritablePath() + "game/" + this._downgameinfo._Module + "/res/filemd5List.json";
        var downurl = _updateUrl + "/game/" + this._downgameinfo._Type + "/";

        //创建更新
        this._update = new UpdateScene(newfileurl, dst, src, downurl);

        this._update.upDateClient(this);

    },

    //更新进度
    updateProgress : function(sub, msg, mainpersent) {
        var permsg = sprintf("%d%%", mainpersent);
        if (null != this.m_downloadTip) {
            this.m_downloadTip.setTitleText(permsg);
        }
    },

    //更新结果
    updateResult : function(result,msg) {
        var self = this;
        this.dismissGameUpdateWait();

        if (result === true) {
            this.m_bUpdating = false;

            //更新版本号
            for (k in _gameList) {
                if (_gameList[k]._KindID === this._downgameinfo._KindID) {
                    _version = _version.setResVersion(_gameList[k]._ServerResVersion, _gameList[k]._KindID);
                    _gameList[k]._Active = true;
                    break;
                }
            }

            if (null != this.m_downloadTip) {
                this.m_downloadTip.setTitleText("");
                this.m_downloadTip.setColor(cc.color(255, 255, 255, 255));
                this.m_downloadTip.setEnabled(true);
            }

        }
        else {
            this.m_bUpdating = false;
            var runScene = cc.director.getRunningScene();
            if (null != runScene) {
                var query = new QueryDialog(msg + "\n是否重试？", function (bReTry) {
                    if (bReTry === true) {
                        self.onGameUpdate(self._downgameinfo);
                    }
                });
                runScene.addChild(query);
            }
        }
    },

    //显示等待
    showPopWait : function(isTransparent) {
        this.getParent().getParent().showPopWait(isTransparent);
    },

    showGameUpdateWait : function() {
        this.m_bGameUpdate = true;
        ExternalFun.popupTouchFilter(1, false, "游戏更新中,请稍候！");
    },

    dismissGameUpdateWait : function() {
        this.m_bGameUpdate = false;
        ExternalFun.dismissTouchFilter();
    },

    //关闭等待
    dismissPopWait : function() {
        this.getParent().getParent().dismissPopWait();
    }
});