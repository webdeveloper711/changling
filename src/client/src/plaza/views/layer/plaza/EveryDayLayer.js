/*
Author. Li
每日必做界面
 */

var  EveryDayLayer = cc.Layer.extend({

    onEnterTransitionDidFinish: function(){
        this.onEnterTransitionFinish();
    },

    onExitTransitionDidStart: function(){
        this.onExitTransitionStart();
    },

    onExit : function(){
        //todo
        if (this._checkinFrame.isSocketServer()) {
            this._checkinFrame.onCloseSocket();
        }
    },

    ctor: function (scene) {
        this._super(cc.color(0, 0, 0, 125));
        var transLayer = new cc.LayerColor(cc.color(0,0,0,125));
        transLayer.setContentSize(appdf.WIDTH, appdf.HEIGHT);
        this.addChild(transLayer);
        this._scene = scene;
        var self = this;

        // this.registerScriptHandler(function (eventType) {
        //     if (eventType == "enterTransitionFinish") {	// 进入场景而且过渡动画结束时候触发。
        //         self.onEnterTransitionFinish();
        //     }
        //     else if (eventType == "exitTransitionStart") {	// 退出场景而且开始过渡动画时候触发。
        //         self.onExitTransitionStart();
        //     }
        //     else if (eventType == "exit") {
        //         if (self._checkinFrame.isSocketServer()) {
        //             self._checkinFrame.onCloseSocket();
        //         }
        //     }
        // });

        //按钮回调
        this._btcallback = function (ref, type) {
            if (type == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };

        //网络回调
        var checkinCallBack = function (result, message) {
            self.onCheckinCallBack(result, message);
        };

        //网络处理
        //todo
        this._checkinFrame = new CheckinFrame(this, checkinCallBack);
        this._EveryDayList = [];

        //标头
        var layer1 = new cc.Sprite(res.sp_header_activity_png);
        layer1.setPosition(yl.WIDTH / 2, yl.HEIGHT - 40);
        layer1.setLocalZOrder(2);
        this.addChild(layer1);
        //背景
        var layer2 = new cc.Sprite(res.actity_background_png);
        layer2.setPosition(yl.WIDTH / 2, yl.HEIGHT - 400);
        layer2.setLocalZOrder(2);
        this.addChild(layer2);

        //标题
        var layer3 = new cc.Sprite(res.title_everyday_png);
        layer3.setPosition(240, yl.HEIGHT - 38);
        layer3.setLocalZOrder(2);
        this.addChild(layer3);

        var str1 = GlobalUserItem.lUserScore + "";
        var str2 = "/";
        this._txtGold = new cc.LabelAtlas(str1.replace(".", "/"), res.num_shop_0_png, 18, 27, str2);
        this._txtGold.setPosition(475, 715);
        //this._txtGold.setPosition(400+this._txtwan.getContentSize().width,705)
        this._txtGold.setAnchorPoint(cc.p(0.5, 0.5));
        this.addChild(this._txtGold);

        str1 = GlobalUserItem.dUserBeans + "";
        this._txtBean = new cc.LabelAtlas(str1.replace(".", "/"), res.num_shop_0_png, 18, 27, str2);
        this._txtBean.setPosition(780, 715);
        this._txtBean.setAnchorPoint(cc.p(0.5, 0.5));
        this.addChild(this._txtBean);

        str1 = GlobalUserItem.lUserIngot + "";
        this._txtIngot = new cc.LabelAtlas(str1.replace(".", "/"), res.num_shop_0_png, 18, 27, str2);
        this._txtIngot.setPosition(1100, 715);
        this._txtIngot.setAnchorPoint(cc.p(0.5, 0.5));
        this.addChild(this._txtIngot);

        var btn1 = new ccui.Button(res.ed_bt_return_0_png, res.ed_bt_return_1_png);
        btn1.setPosition(1280, yl.HEIGHT - 39);
        btn1.addTouchEventListener(function (ref, type) {
            if (type == ccui.Widget.TOUCH_ENDED) {
                self._scene.onKeyBack();
            }
        });
        this.addChild(btn1);

        //记录列表
        //todo all
        this._listView = new cc.TableView(this, cc.size(1163, 538));
        this._listView.setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL);
        this._listView.setPosition(cc.p(88, 85));
        this._listView.setDelegate(this);
        this.addChild(this._listView);
        this._listView.setVerticalFillOrder(cc.TABLEVIEW_FILL_TOPDOWN);


        // this._listView.registerScriptHandler(this.cellSizeForTable, cc.TABLECELL_SIZE_FOR_INDEX);
        // this._listView.registerScriptHandler(this.tableCellAtIndex, cc.TABLECELL_SIZE_AT_INDEX);
        // this._listView.registerScriptHandler(this.numberOfCellsInTableView, cc.NUMBER_OF_CELLS_IN_TABLEVIEW);
    },

    // 进入场景而且过渡动画结束时候触发。
    onEnterTransitionFinish : function() {
        this._scene.showPopWait();
        this._checkinFrame.onBaseEnsureLoad();
        return this;
    },

    // 退出场景而且开始过渡动画时候触发。
    onExitTransitionStart : function() {
        return this;
    },

    onUpdateShow : function() {
        this._listView.reloadData();
    },

    //按键监听
    onButtonClickedEvent : function(tag,sender) {
        var self = this;

        if (tag == EveryDayLayer.BT_CELL) {
            var idx = sender.nCusTag;
            idx = idx + EveryDayLayer.BT_CELL + 1;
            if (idx == EveryDayLayer.BT_CHECKIN) {
                this.getParent().getParent().onChangeShowMode(yl.SCENE_CHECKIN);
            }
            else if (idx == EveryDayLayer.BT_ROLLTABLE) {
                this.getParent().getParent().onChangeShowMode(yl.SCENE_TABLE);
            }
            else if (idx == EveryDayLayer.BT_DIBAO) {
                this._scene.showPopWait();
                this._checkinFrame.onBaseEnsureTake();
            }
            else if (idx == EveryDayLayer.BT_CHARGE) {
                //cc.log("点击首充")
                this._scene.onChangeShowMode(yl.SCENE_SHOP, Shop.CBT_BEAN);
            }
            else if (idx == EveryDayLayer.BT_SHARE) {
                //cc.log("点击分享")
                function sharecall(isok) {
                    if (typeof(isok) == "string" && isok == "true") {
                        showToast(this, "分享成功", 2);
                        //获取奖励
                        var ostime = (new Date()).getTime();
                        var url = yl.HTTP_URL + "/WS/MobileInterface.ashx";
                        var param = "action=GetMobileShare&userid=" + GlobalUserItem.dwUserID + "&time=" + ostime + "&signature=" + GlobalUserItem.getSignature(ostime) + "&machineid=" + GlobalUserItem.szMachine;
                        appdf.onHttpJsonTable(url, "GET", param, function (jstable, jsdata) {
                            cc.log(jstable + "desciption" + 6);
                            // LogAsset.getInstance().logData(jsonStr,true)
                            GlobalUserItem.setTodayShare();

                            if (jstable != null) {
                                var data = jstable["data"];
                                var msg = jstable["msg"];
                                if (data != null && typeof(msg) == "string" && "" != msg) {
                                    GlobalUserItem.lUserScore = data["Score"] || GlobalUserItem.lUserScore;
                                    //通知更新
                                    var eventListener = cc.EventCustom.new(yl.RY_USERINFO_NOTIFY);
                                    eventListener.obj = yl.RY_MSG_USERWEALTH;
                                    cc.director.getEventDispatcher().dispatchEvent(eventListener);

                                    if (typeof(msg) == "string" && "" != msg) {
                                        showToast(self, msg, 3);
                                    }
                                }
                            }
                        });
                    }
                }

                var url = GlobalUserItem.szWXSpreaderURL || yl.HTTP_URL;
                MultiPlatform.getInstance().customShare(sharecall, null, null, url, "");
            }
            else if (idx == EveryDayLayer.BT_TASK) {
                this.getParent().getParent().onChangeShowMode(yl.SCENE_TASK);
            }
        }
    },

    updateScoreInfo : function() {
        this._txtGold.setString(string_formatNumberThousands(GlobalUserItem.lUserScore, true, "/"));
        this._txtBean.setString(string_formatNumberThousands(GlobalUserItem.dUserBeans, true, "/"));
        this._txtIngot.setString(string_formatNumberThousands(GlobalUserItem.lUserIngot, true, "/"));
    },

    //操作结果
    onCheckinCallBack : function(result,message) {
        cc.log("======== EveryDayLayer.onCheckinCallBack ========");
        this._scene.dismissPopWait();
        if (typeof(message) == "string" && message != "") {
            showToast(this, message, 2);
        }

        if (result == CheckinFrame.BASEENSUREQUERY) {
            var condition = message.condition || 0;
            var amount = message.amount || 0;
            var times = message.times || 0;
            var tmpField = [];
            tmpField["Field1"] =
                {
                    idx: 1,
                    str: "连续签到天数越多，奖励越多！",
                    order: GlobalUserItem.isFirstCheckIn() && 6 || 0
                };
            tmpField["Field2"] =
                {
                    idx: 2,
                    str: "每天送您" + GlobalUserItem.nTableFreeCount + "次大爆炸的机会！",
                    order: GlobalUserItem.isFirstTable() && 5 || -1
                };
            tmpField["Field3"] =
                {
                    idx: 3,
                    str: "金币数量低于" + condition + ",可领取" + amount + "金币！(限" + times + "次/天)。",
                    order: 4
                };
            tmpField["Field4"] =
                {
                    idx: 4,
                    str: "每日一冲就那么任性！",
                    order: GlobalUserItem.isFirstPay() && 3 || -2
                };
            tmpField["Field5"] =
                {
                    idx: 5,
                    str: "每日首次分享系统赠送" + GlobalUserItem.nShareSend + "金币！",
                    order: GlobalUserItem.isFirstShare() && 2 || -3
                };
            for (k in GlobalUserItem.tabDayTaskCache) {
                var field = tmpField[k];
                if (null != tmpField[k]) {
                    this._EveryDayList.push(field);
                }
            }
            this._EveryDayList.sort(function(a,b) {
                return a.order > b.order;
            });
            this.onUpdateShow();
        }
    },

    //子视图大小
    cellSizeForTable : function(view, idx) {
        return [1161, 508 / 4];
    },

    //子视图数目
    numberOfCellsInTableView : function(view) {
        return this._EveryDayList.length;
        // return view.getParent()._EveryDayList.length;
    },

    tableCellSizeForIndex:function (table, idx) {
        return cc.size(100, 508 / 4);
    },

    //获取子视图
    tableCellAtIndex : function(view, idx) {
        var cell = view.dequeueCell();

        var item = this._EveryDayList[idx];
        // var item = view.getParent()._EveryDayList[idx + 1];
        var src_idx = item.idx - 1;

        var width = 1161;
        var height = 508 / 4;
        if (!cell && src_idx < 5) {
            cell = new cc.TableViewCell();
            var sp1 = new cc.Sprite(res.cell_everyday_png);
            cell.addChild(sp1);
            sp1.setPosition(width / 2, height / 2);

            //图标
            var sp2 = new cc.Sprite("src/client/res/EveryDay/icon_everyday_" + src_idx + ".png");
            cell.addChild(sp2);
            sp2.setTag(1);
            sp2.setPosition(110, height / 2 - 4);

            //提示
            var clip = new ClipText(cc.size(720, 30), item.str, res.round_body_ttf, 27);
            clip.setTag(3);
            clip.setPosition(182, 65);
            //.setVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER)
            clip.setTextColor(cc.color(108, 97, 91, 255));
            clip.setAnchorPoint(cc.p(0, 0.5));
            cell.addChild(clip);

            //按钮
            var btn = new ccui.Button("src/client/res/EveryDay/bt_everyday_" + src_idx + "_0.png", "src/client/res/EveryDay/bt_everyday_" + src_idx + "_0.png", "src/client/res/EveryDay/bt_everyday_" + src_idx + "_1.png");
            cell.addChild(btn);
            btn.setPosition(1000, height / 2);
            btn.setTag(EveryDayLayer.BT_CELL);
            btn.addTouchEventListener(this._btcallback);
            // cell.addTouchEventListener(view.getParent()._btcallback);

        }
        if (src_idx < 5) {
            var cellChild1 = cell.getChildByTag(1);
            cellChild1.setTexture("src/client/res/EveryDay/icon_everyday_" + src_idx + ".png");
            var cellChild3 = cell.getChildByTag(3);
            cellChild3.setString(item.str);
            var cellChild4 = cell.getChildByTag(EveryDayLayer.BT_CELL);
            cellChild4.nCusTag = src_idx;
            var cellChild5 = cell.getChildByTag(EveryDayLayer.BT_CELL);
            cellChild5.loadTextureNormal("src/client/res/EveryDay/bt_everyday_" + src_idx + "_0.png");
            var cellChild6 = cell.getChildByTag(EveryDayLayer.BT_CELL);
            cellChild6.loadTexturePressed("src/client/res/EveryDay/bt_everyday_" + src_idx + "_1.png");
        }
        return cell;
    }
});


EveryDayLayer.BT_CELL = 15;
EveryDayLayer.BT_CHECKIN = 16;
EveryDayLayer.BT_ROLLTABLE = 17;
EveryDayLayer.BT_DIBAO = 18;
EveryDayLayer.BT_CHARGE = 19;
EveryDayLayer.BT_SHARE = 20;
EveryDayLayer.BT_TASK = 21;

// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new EveryDayLayer();
//         this.addChild(layer);
//     }
// });