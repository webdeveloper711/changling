//region *.lua
//Author. Li
//endregion

//二维码界面
var SHARE_QRCODE = 1;
var SAVE_QECODE = 2;
var  QrCodeForExtensionLayer =  cc.Layer.extend({
    ctor : function() {
        this._super(cc.color(0, 0, 0, 125));

        var transLayer = new cc.LayerColor(cc.color(0, 0, 0, 125));
        transLayer.setContentSize(appdf.WIDTH, appdf.HEIGHT);
        this.addChild(transLayer);

        //注册触摸事件
        //todo
        // ExternalFun.registerTouchEvent(this, true);
        var self = this;
        //加载csb资源
        var result = ExternalFun.loadRootCSB(res.QrCodeLayer_json, this);
        var rootLayer = result.rootlayer;
        var csbNode = result.csbnode;

        this.m_csbNode = csbNode;
        this.m_spBg = csbNode.getChildByName("qr_sp_bg");
        var bgsize = this.m_spBg.getContentSize();

        function btnEvent(sender, eventType) {
            if (eventType == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(sender.getTag(), sender);
            }
        }

        this.m_qrContent = GlobalUserItem.szWXSpreaderURL || yl.HTTP_URL;
        //todo all
        // var qr = QrNode.createQrNode(this.m_qrContent, 280, 5, 1);
        // this.m_spBg.addChild(qr);
        // qr.setPosition(bgsize.width * 0.5, bgsize.height * 0.5);
        // this.m_qrNode = qr;
        //ID
        var label = this.m_spBg.getChildByName("txt_id");
        label.setString("ID." + GlobalUserItem.dwGameID);

        //分享
        var btn = this.m_spBg.getChildByName("btn_share");
        btn.setTag(SHARE_QRCODE);
        btn.addTouchEventListener(btnEvent);
        //存储
        btn = this.m_spBg.getChildByName("btn_save");
        btn.setTag(SAVE_QECODE);
        btn.addTouchEventListener(btnEvent);

        var framesize = cc.director.getOpenGLView().getFrameSize();
        var xRate = framesize.width / yl.WIDTH;
        var yRate = framesize.height / yl.HEIGHT;
        var screenPos = this.m_spBg.convertToWorldSpace(cc.p(bgsize.width * 0.5, bgsize.height * 0.5));
        var framePosX = (screenPos.x - 190) * xRate;
        var framePosY = (screenPos.y - 190) * yRate;
        var frameWidth = 380 * xRate;
        var frameHeight = 380 * yRate;
        this.m_qrFrameRect = cc.rect(framePosX, framePosY, frameWidth, frameHeight);
        this.m_screenPos = screenPos;
        this.m_xRate = xRate;
        this.m_yRate = yRate;

        var customListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                return self.isVisible();
            },
            onTouchMoved: function (touch, event) {
                //Move the position of current button sprite
                // var target = event.getCurrentTarget();
                // var delta = touch.getDelta();
                // target.x += delta.x;
                // target.y += delta.y;
            },
            onTouchEnded : function (touch, event) {
                var pos = touch.getLocation();
                var m_spBg = self.m_spBg;
                pos = m_spBg.convertToNodeSpace(pos);
                var rec = cc.rect(0, 0, m_spBg.getContentSize().width, m_spBg.getContentSize().height);
                if (false == cc.rectContainsPoint(rec, pos)) {
                    self.removeFromParent();
                }
            }
        });
        cc.eventManager.addListener(customListener, this.m_csbNode);
    },

    onButtonClickedEvent : function( tag, sender ) {
        ExternalFun.popupTouchFilter(0, false);
        var self = this;
        if (tag == SHARE_QRCODE) {
            captureScreenWithArea(this.m_qrFrameRect, "qr_code.png", function (ok, savepath) {
                ExternalFun.dismissTouchFilter();
                if (ok) {
                    MultiPlatform.getInstance().customShare(function (isok) {
                    }, "我的推广码", "分享我的推广码", self.m_qrContent, savepath, "true");
                }
            });
        }
        else if (tag == SAVE_QECODE) {
            captureScreenWithArea(this.m_qrFrameRect, "qr_code.png", function (ok, savepath) {
                if (ok) {
                    if (true == MultiPlatform.getInstance().saveImgToSystemGallery(savepath, "qr_code.png")) {
                        showToast(self, "您的推广码二维码图片已保存至系统相册", 1);
                    }
                }
                var delayTime = new cc.DelayTime(3);
                var callFunc = new cc.CallFunc(function () {
                    ExternalFun.dismissTouchFilter();
                });
                var seq = new cc.Sequence(delayTime, callFunc);
                self.runAction(seq);
            });
        }
    },

    onExit : function() {
        this.stopAllActions();
    }
});


// 代理填写界面
var TAG_MASK = 101;
var BT_SURE = 102;
var BT_S_WECHAT = 103;	//分享到微信
var BT_S_CYCLE = 104; 		//分享到朋友圈
var BT_S_CONTACT = 105; 	//分享到短信
var BT_COPY = 106; 		//复制粘贴板
var BT_QR_ENTER = 107; 	//点击显示二维码

var  ExtensionLayer = cc.Layer.extend({
    ctor : function (viewparent) {
        this._super(cc.color(0, 0, 0, 125));
        var transLayer = new cc.LayerColor(cc.color(0, 0, 0, 125));
        transLayer.setContentSize(appdf.WIDTH, appdf.HEIGHT);
        this.addChild(transLayer);
        this.m_parent = viewparent;
        var self = this;
        var noSpreader = GlobalUserItem.szSpreaderAccount == "";

        var result = ExternalFun.loadRootCSB(res.ExtensionLayer_json, this);
        var rootLayer = result.rootlayer;
        var csbNode = result.csbnode;

        var touchFunC = function (ref, tType) {
            if (tType == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };

        // 遮罩
        var mask = csbNode.getChildByName("panel_mask");
        mask.setTag(TAG_MASK);
        //底板
        var bg = mask.getChildByName("sp_back_extension");
        this._bg = bg;

        // 编辑框
        var tmp = bg.getChildByName("sp_dikuang");
        var editbox = new cc.EditBox(cc.size(300, 40), ccui.Scale9Sprite("sp_dikuang_tuiguang.png"));
        editbox.setPosition(tmp.getPosition().x+10,tmp.getPosition().y);
        editbox.setFontName(res.round_body_ttf);
        editbox.setPlaceholderFontName(res.round_body_ttf);
        editbox.setPlaceHolder("请输入推广员ID");
        editbox.setMaxLength(32);
        editbox.setPlaceholderFontColor(cc.color(198, 164, 142));
        editbox.setFontColor(cc.color(136, 51, 39));
        editbox.setInputMode(cc.EDITBOX_INPUT_MODE_NUMERIC);
        this._bg.addChild(editbox);
        editbox.setContentSize(cc.size(tmp.getContentSize().width, tmp.getContentSize().height));
        editbox.setPlaceholderFont(res.round_body_ttf, 26);
        editbox.setFont(res.round_body_ttf, 26);
        this.m_bEditChat = false;
        this.m_editID = editbox;
        editbox.setEnabled = noSpreader;
        editbox.setVisible(true);

        // 邀请奖励
        var atlas = bg.getChildByName("atlas_sendcount");
        var count = GlobalUserItem.nInviteSend || 0;
        atlas.string = count + "";

        // 确定按钮
        var btn1 = bg.getChildByName("btn_sure");
        btn1.setTag(BT_SURE);
        btn1.addTouchEventListener(touchFunC);
        btn1.setEnabled(noSpreader);
        if (!noSpreader) {
            editbox.setPlaceHolder(GlobalUserItem.szSpreaderAccount);
            btn1.setOpacity(125);
        }

        // 关闭按钮
        var btn2 = mask.getChildByName("btn_close_912");
        btn2.addTouchEventListener(function (ref, type) {
            if (type == ccui.Widget.TOUCH_ENDED) {
                self.m_parent.onKeyBack();
            }
        });

        // 微信分享按钮
        var btn3 = bg.getChildByName("btn_wchat");
        btn3.setTag(BT_S_WECHAT);
        btn3.addTouchEventListener(touchFunC);

        // 朋友圈
        var btn4 = bg.getChildByName("btn_circle_friend");
        btn4.setTag(BT_S_CYCLE);
        btn4.addTouchEventListener(touchFunC);

        // 通讯录
        var btn5 = bg.getChildByName("btn_mail_list");
        btn5.setTag(BT_S_CONTACT);
        btn5.addTouchEventListener(touchFunC);

        // 复制
        var btn6 = bg.getChildByName("btn_copy");
        btn6.setTag(BT_COPY);
        btn6.addTouchEventListener(touchFunC);

        // 二维码
        var btn7 = bg.getChildByName("btn_qrcode");
        btn7.setTag(BT_QR_ENTER);
        btn7.addTouchEventListener(touchFunC);
        btn7.setVisible(true);

        //todo QrNode class
        this.m_qrContent = GlobalUserItem.szWXSpreaderURL || yl.HTTP_URL;
        // var qr = QrNode.createQrNode(this.m_qrContent, 184, 5, 1);
        // bg.addChild(qr);
        // qr.setPosition(btn7.getPosition());
        // this.m_qrNode = qr;
        // this.m_qrNode.setVisible(true);

        //网络回调
        var modifyCallBack = function (result, message) {
            self.onModifyCallBack(result, message);
        };
        //网络处理
        //todo
        this._modifyFrame = new ModifyFrame(this, modifyCallBack);

    },

    onButtonClickedEvent : function( tag, sender ) {
        var self = this;
        if (BT_SURE == tag) {
            var txt = this.m_editID.string.replace(".", "");
            // var txt = string.gsub(this.m_editID.getText(), ".", "");
            if (txt == "") {
                showToast(this, "推广员ID不能为空!", 2);
                return;
            }
            txt = Number(txt);
            if (null == txt) {
                showToast(this, "请输入合法的推广员ID!", 2);
                return;
            }
            this._modifyFrame.onBindSpreader(txt);
        }
        else if (BT_S_WECHAT == tag) {
            //todo following method doesn't fixed
            function sharecall(isok) {
                if (typeof(isok) == "string" && isok == "true") {
                    showToast(self, "分享完成", 1);
                }
            }

            var url = GlobalUserItem.szWXSpreaderURL || yl.HTTP_URL;
            MultiPlatform.getInstance().shareToTarget(yl.ThirdParty.WECHAT, sharecall, yl.SocialShare.title, yl.SocialShare.content, url);
        }
        else if (BT_S_CYCLE == tag) {
            function sharecall(isok) {
                if (typeof(isok) == "string" && isok == "true") {
                    showToast(self, "分享完成", 1);
                }
            }

            var url = GlobalUserItem.szWXSpreaderURL || yl.HTTP_URL;
            MultiPlatform.getInstance().shareToTarget(yl.ThirdParty.WECHAT_CIRCLE, sharecall, yl.SocialShare.title, yl.SocialShare.content, url);
        }
        else if (BT_S_CONTACT == tag) {
            function sharecall(isok) {
                if (typeof(isok) == "string" && isok == "true") {
                    showToast(self, "分享完成", 1);
                }
            }

            var url = GlobalUserItem.szSpreaderURL || yl.HTTP_URL;
            MultiPlatform.getInstance().shareToTarget(yl.ThirdParty.SMS, sharecall, yl.SocialShare.title, url);
        }
        else if (BT_COPY == tag) {
            var url = GlobalUserItem.szSpreaderURL || yl.HTTP_URL;
            //todo following statement is error two parameter
            var tmp = MultiPlatform.getInstance().copyToClipboard(url);
            var res = tmp[0];
            var msg = tmp[1];
            if (true == res) {
                showToast(this, "复制到剪贴板成功!", 1);
            }
            else {
                if (typeof(msg) == "string") {
                    showToast(this, msg, 1, cc.color(250, 0, 0));
                }
            }
        }
        else if (BT_QR_ENTER == tag) {
            var qrlayer = new QrCodeForExtensionLayer();
            this.m_parent.addChild(qrlayer);
        }
        else if (TAG_MASK == tag) {
            if (false == this.m_bEditChat) {
                this.removeFromParent();
            }
            if (true == this.m_bEditChat) {
                this.m_bEditChat = false;
            }
        }
    },

    //操作结果
    onModifyCallBack : function(result,message) {
        if (message != null && message != "") {
            showToast(this, message, 2);
        }
        if (-1 == result) {
            return;
        }

        bGender = (GlobalUserItem.cbGender == yl.GENDER_MANKIND && true || false);
        this._cbtMan.setSelected(bGender);
        this._cbtWoman.setSelected(!bGender);

        if (yl.SUB_GP_USER_FACE_INFO == result) {
            this.onFaceResultSuccess();
            this.removeChildByTag(UserInfoLayer.LAY_SELHEAD, true);
        }
        else if (loginCMD.SUB_GP_USER_INDIVIDUAL == result) {
            // 推广员按钮
            //var noSpreader = GlobalUserItem.szSpreaderAccount == ""
            //this.m_btnPromoter.setVisible(noSpreader)
            //this.m_btnPromoter.setEnabled(noSpreader)
        }
        else if (this._modifyFrame.INPUT_SPREADER == result) {
            //var noSpreader = GlobalUserItem.szSpreaderAccount == ""
            //this.m_btnPromoter.setVisible(noSpreader)
            //this.m_btnPromoter.setEnabled(noSpreader)
        }
    }
});

// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new ExtensionLayer();
//         this.addChild(layer);
//     }
// });