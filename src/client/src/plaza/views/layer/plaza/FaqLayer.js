//
// Author: Li
// Date: 2016-08-30 18:41:26
//
//常见问题页面
// var FaqLayer = class("FaqLayer", cc.Layer)
// var ExternalFun = require(appdf.EXTERNAL_SRC + "ExternalFun")
// function FaqLayer() {
//     return new cc.Layer();
// }
//返回按钮
var BT_EXIT = 101;
var FaqLayer = cc.Layer.extend({
    ctor : function( scene ) {
        this._super();
        this._scene = scene;
        var self = this;

        //todo following statement is error two parameter
        //加载csb资源
        var result = ExternalFun.loadRootCSB(res.FaqLayer_json, this);
        var rootLayer = result.rootlayer;
        var csbNode = result.csbnode;
        this.m_csbNode = csbNode;

        function btncallback(ref, type) {
            if (type === ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        }

        //返回按钮
        var btn = csbNode.getChildByName("btn_back");
        btn.setTag(BT_EXIT);
        btn.addTouchEventListener(btncallback);

        var tmp = csbNode.getChildByName("sp_public_frame");
        //平台判定
        var targetPlatform = cc.sys.os;
        if ((cc.sys.IPHONE == targetPlatform) || (cc.sys.IPAD == targetPlatform) || (cc.sys.ANDROID == targetPlatform)) {
            //页面
            this.m_webView = new ccui.WebView();
            this.m_webView.setPosition(tmp.getPosition());
            this.m_webView.setContentSize(cc.size(1155, 520));

            this.m_webView.setScalesPageToFit(true);
            var url = yl.HTTP_URL + "/Mobile/Faq.aspx";
            this.m_webView.loadURL(url);
            ExternalFun.visibleWebView(this.m_webView, false);
            this._scene.showPopWait();

            this.m_webView.setOnJSCallback(function (sender, url) {
            });

            this.m_webView.setOnDidFailLoading(function (sender, url) {
                self._scene.dismissPopWait();
                cc.log("open " + url + " fail");
            });
            this.m_webView.setOnShouldStartLoading(function (sender, url) {
                cc.log("onWebViewShouldStartLoading, url is ", url);
                return true;
            });
            this.m_webView.setOnDidFinishLoading(function (sender, url) {
                self._scene.dismissPopWait();
                ExternalFun.visibleWebView(self.m_webView, true);
                cc.log("onWebViewDidFinishLoading, url is ", url);
            });
            this.addChild(this.m_webView);
        }
        //tmp.removeFromParent()
    },
    onButtonClickedEvent : function( tag, sender ) {
        if (BT_EXIT === tag) {
            this._scene.onKeyBack();
        }
    }
});
// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new FaqLayer();
//         this.addChild(layer);
//     }
// });