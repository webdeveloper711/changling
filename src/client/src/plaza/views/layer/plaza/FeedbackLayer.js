//
// Author. Li
// Date. 2016-08-10 17.49.13
//
// var ExternalFun = require(appdf.EXTERNAL_SRC + "ExternalFun");
// var MultiPlatform = appdf.req(appdf.EXTERNAL_SRC + "MultiPlatform");

//返回按钮
var BT_EXIT = 101;
//选择图片
var BT_PICIMG = 102;
//发送按钮
var BT_SEND = 103;
//我的反馈
var BT_MYFEEDBACk = 104;

//我的反馈列表
var FeedbackListLayer =  cc.Layer.extend({
    ctor: function (scene) {
        this._super(cc.color(0, 0, 0, 125));
        this._scene = scene;
        var self = this;

        //加载csb资源
        //todo following csb file doesn't exist in res directory
        // var result = ExternalFun.loadRootCSB("feedback/FeedbackListLayer.csb", this);
        var result = ExternalFun.loadRootCSB(res.FeedbackListLayer_json, this);
        var rootLayer = result.rootlayer;
        var csbNode = result.csbnode;
        this.m_csbNode = csbNode;

        function btncallback(ref, type) {
            if (type === ccui.TouchEventType.ended) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        }

        //返回按钮
        var btn = csbNode.getChildByName("btn_back");
        btn.setTag(BT_EXIT);
        btn.addTouchEventListener(btncallback);
    },
    onButtonClickedEvent : function( tag, sender ) {
        if (BT_EXIT === tag) {
            this._scene.onKeyBack();
        }
    }
});
//反馈编辑界面
var FeedbackLayer = cc.Layer.extend({
    ctor : function( scene ) {
        this._super(cc.color(0, 0, 0, 125));
        this._scene = scene;
        var self = this;
        var bglayer = new cc.LayerColor(cc.color(0,0,0,125),appdf.WIDTH,appdf.HEIGHT);
        this.addChild(bglayer);
        //加载csb资源
        var result = ExternalFun.loadRootCSB(res.FeedbackSendLayer_json, this);
        var rootLayer = result.rootlayer;
        var csbNode = result.csbnode;

        this.m_csbNode = csbNode;

        function btncallback(ref, type) {
            if (type === ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        }

        var panel = csbNode.getChildByName("Panel_1");
        //返回按钮
        var btn = panel.getChildByName("btn_back");
        btn.setTag(BT_EXIT);
        btn.addTouchEventListener(btncallback);

        var tmp = panel.getChildByName("sp_public_frame");
        //平台判定
        var targetPlatform = cc.sys;
    },
    createFeedbackList : function( scene ) {
        var list = FeedbackListLayer.new(scene);
        return list;
    },
    onButtonClickedEvent : function( tag, sender ) {
        if (BT_EXIT === tag) {
            this._scene.onKeyBack();
        }
        else if (BT_PICIMG === tag) {
            MultiPlatform.getInstance().triggerPickImg(function (param) {
                if (typeof param === "string") {
                    cc.log("lua path ==> " + param);
                }
            }, false);
        }
        else if (BT_SEND === tag) {
        }
        else if (BT_MYFEEDBACk === tag) {
            this._scene.onChangeShowMode(yl.SCENE_FEEDBACKLIST);
        }
    }
});
// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new FeedbackLayer();
//         this.addChild(layer);
//     }
// });