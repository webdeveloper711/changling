/*
Author. Li
 */
// var ExternalFun = appdf.req(appdf.EXTERNAL_SRC + "ExternalFun");

var GameListLayer =  cc.Layer.extend({

    onEnterTransitionDidFinish: function(){
        this.onEnterTransitionFinish();
    },

    onExitTransitionDidStart: function(){
        this.onExitTransitionStart();
    },

    onExit : function(){
        if (this._logonFrame.isSocketServer()) {
            this._logonFrame.onCloseSocket();
        }
    },
    ctor: function (gamelist) {
        this._super();
        cc.log("============= 游戏列表界面创建 =============");
        this.m_bQuickStart = false;
        var self = this;

        this.setContentSize(yl.WIDTH, yl.HEIGHT);

        this._gameList = gamelist;

        var logonCallBack = function (result, message) {
            self.onLogonCallBack(result, message);
        };

        this._logonFrame = new LogonFrame(this, logonCallBack);


        // this.registerScriptHandler(function (eventType) {
        //     if (eventType === "enterTransitionFinish") {	// 进入场景而且过渡动画结束时候触发。
        //         self.onEnterTransitionFinish();
        //     }
        //     else if (eventType === "exitTransitionStart") {	// 退出场景而且开始过渡动画时候触发。
        //         self.onExitTransitionStart();
        //     }
        //     else if (eventType === "exit") {
        //         if (self._logonFrame.isSocketServer()) {
        //             self._logonFrame.onCloseSocket();
        //         }
        //     }
        // });

        this.m_fThird = yl.WIDTH / 3;

        this._gameShowList = [];
        for (i = 0; i < this._gameList.length; i++) {
            var count = this.onExistenceGame(gamelist[i]._KindID);
            if (count > 0) {
                this._gameShowList.push(gamelist[i]);
            }
        }

        this._scrollview = null;

        //列表数据
        var game_count = this._gameShowList.length;  //游戏数量
        var line = 2;   //行
        var column = Math.ceil(game_count / 2);   //列
        //   var spacing = 115      //间距
        var spacing = 50;     //间距
        var margin = 10;       //边距

        var cellsize = this.cellSizeForTable(); // 控件大小
        var Inner_width = cellsize.width * column + spacing * (column - 1) + margin * 2; //内容宽
        var Inner_height = cellsize.height * line + spacing * (line - 1) + margin * 2; //内容高
        //var scroll_width = cellsize.width*column + spacing*(column-1) + margin*2 //视图宽
        var scroll_width = 1100; //视图宽
        var scroll_height = 505;
        // cellsize.height*line + spacing*(line-1) + margin*2 /
        // 视图高

        //背景
        var sp1 = new cc.Sprite(res.sp_GameList_BG_png);
        sp1.setPosition(cc.p(yl.WIDTH / 2, yl.HEIGHT * 0.4));
        sp1.setAnchorPoint(cc.p(0.5, 0.5));
        this.addChild(sp1);

        //游戏列表（多行）
        this._scrollview = new ccui.ScrollView();
        this._scrollview.setTouchEnabled(true);
        this._scrollview.setBounceEnabled(true); //这句必须要不然就不会滚动噢
        this._scrollview.setScrollBarEnabled(false);
        //this._scrollview.setDirection(ccui.ScrollViewDir.vertical) //设置滚动的方向
        this._scrollview.setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL); //设置滚动的方向
        this._scrollview.setContentSize(cc.size(scroll_width, scroll_height)); //设置尺寸
        this._scrollview.setInnerContainerSize(cc.size(Inner_width, Inner_height));
        this._scrollview.setAnchorPoint(cc.p(0.5, 0.5));
        this._scrollview.setPosition(cc.p(yl.WIDTH / 2, yl.HEIGHT / 2 - 55));

        this.addChild(this._scrollview);
        this.onUpdateShowList(Inner_width, Inner_height, spacing, margin, column, line);

        this._txtTips = new ccui.Text("", "fonts/round_body.ttf", 32);
        this._txtTips.setAnchorPoint(cc.p(1, 0));
        this._txtTips.setPosition(yl.WIDTH, 110);
        this._txtTips.setTextColor(cc.color(0, 250, 0, 255));
        this.addChild(this._txtTips);

        //下载提示
        this.m_spDownloadMask = null;
        this.m_szMaskSize = cc.size(0, 0);
        this.m_labDownloadTip = null;
        this.m_spDownloadCycle = null;
        this.m_bGameUpdate = false;
        this.m_bUpdating = false;
    },
    // 进入场景而且过渡动画结束时候触发。
    onEnterTransitionFinish: function () {
        //this._scrollview.reloadData()
        //this._listView.reloadData()
        return this;
    },

    // 退出场景而且开始过渡动画时候触发。
    onExitTransitionStart: function () {
        return this;
    },


    //更新当前显示(宽，高，间距,每行数量)
    onUpdateShowList: function (width, height, spacing, margin, column, line) {
        if (this._gameShowList != null) {
            var count = this._gameShowList.length;

            if (line === 0) {
                line = 1;
            }
            if (column === 0) {
                column = 1;
            }

            var cellsize = this.cellSizeForTable();
            var cell_width = null;
            var cell_height = null;

            this._showList = null;
            this._showList = [];

            for (i = 1; i < count; i++) {
                this._showList[i] = this.tableCellAtIndex(i);
                this._showList[i].setAnchorPoint(0.5, 0.2);
                if (ccui.ScrollViewDir.vertical === this._scrollview.getDirection()) {
                    cell_width = Math.floor((i - 1) % column) * (cellsize.width + spacing) + cellsize.width / 2 + margin;
                    cell_height = Math.floor((i - 1) / column) * (cellsize.height + spacing) + cellsize.height / 2 + margin;
                }
                else if (ccui.ScrollViewDir.horizontal === this._scrollview.getDirection()) {
                    cell_width = Math.floor((i - 1) % column) * (cellsize.width + spacing) + cellsize.width / 2 + margin;
                    cell_height = Math.floor((i - 1) / column) * (cellsize.height + spacing) + cellsize.height / 2 + margin + 30;
                }
                else {
                    cell_width = Math.floor((i - 1) / column) * (cellsize.width + spacing) + cellsize.width / 2 + margin;
                    cell_height = Math.floor((i - 1) % column) * (cellsize.height + spacing) + cellsize.height / 2 + margin;
                }
                this._showList[i].setPosition(cell_width, height - cell_height);
                this._showList[i].addChild(this._scrollview);
            }
        }
    },

    //获取父场景节点(ClientScene)
    getRootNode: function () {
        return this.getParent().getParent();
    },

    //更新游戏进入记录
    updateEnterGameInfo: function (info) {
        this.getRootNode().updateEnterGameInfo(info);
    },

    onEnterGame: function (gameinfo, isQuickStart) {
        this.updateEnterGameInfo(gameinfo);
        this.m_bQuickStart = isQuickStart;

        //判断房间获取
        var roomCount = GlobalUserItem.GetGameRoomCount(gameinfo._KindID)
        if (!roomCount || 0 === roomCount) {
            //this.onLoadGameList(gameinfo._KindID)
            cc.log("GameListLayer 房间列表为空");
        }
        GlobalUserItem.nCurGameKind = Number(gameinfo._KindID);
        GlobalUserItem.szCurGameName = gameinfo._KindName;
        if (MatchRoom && true === MatchRoom.getInstance().onLoginEnterRoomList()) {
            cc.log(" GameListLayer enter MatchGame ");
        }
        else if (roomCount) {
            //this._scene.onChangeShowMode(yl.SCENE_ROOMLIST)
            //clientscene.onChangeShowMode(yl.SCENE_ROOMLIST, this.m_bQuickStart)
            this.getParent().getParent().onChangeShowMode(yl.SCENE_ROOMLIST, this.m_bQuickStart);
        }
        else if (PriRoom && true === PriRoom.getInstance().onLoginEnterRoomList()) {
            cc.log(" GameListLayer enter priGame ");
        }
        else {
            // 处理锁表
            var lockRoom = GlobalUserItem.GetGameRoomInfo(GlobalUserItem.dwLockServerID);
            if (GlobalUserItem.dwLockKindID === GlobalUserItem.nCurGameKind && null != lockRoom) {
                GlobalUserItem.nCurRoomIndex = lockRoom._nRoomIndex;
                this.getParent().getParent().onStartGame();
            }
            else {
                this.getParent().getParent().onChangeShowMode(yl.SCENE_ROOMLIST, this.m_bQuickStart);
            }
        }
    },

    ////////////////////////////////////////////////////////////////////-
    // listview 相关
    cellHightLight: function (view, cell) {
    },

    cellUnHightLight: function (view, cell) {
    },

    //子视图大小
    cellSizeForTable: function () {
        return cc.size(330, 181);
    },

    //子视图数目
    numberOfCellsInTableView: function (view) {
        return this._gameShowList.length;
    },

    //子视图点击
    tableCellTouched: function (view, cell, type) {
        if (type === ccui.Widget.TOUCH_ENDED) {

            if (GlobalUserItem.isAngentAccount()) {
                return;
            }

            var index = cell.getTag();
            var gamelistLayer = view.getParent();

            //获取游戏信息
            var gameinfo = gamelistLayer._gameShowList[index];
            if (!gameinfo) {
                showToast(gamelistLayer.getParent().getParent(), "未找到游戏信息！", 2);
                return;
            }
            gameinfo.gameIndex = index;

            //下载/更新资源 clientscene.getApp
            var app = gamelistLayer.getParent().getParent().getApp();
            var version = Number(app.getVersionMgr().getResVersion(gameinfo._KindID));
            if (!version || gameinfo._ServerResVersion > version) {
                gamelistLayer.updateGame(gameinfo, index);
            }
            else
                gamelistLayer.onEnterGame(gameinfo, false);
        }
    },

    //获取子视图
    tableCellAtIndex: function (idx) {
        var cell = null;
        if (this._showList != null) {
            cell = this._showList[idx];
        }

        var gameinfo = this._gameShowList[idx];
        gameinfo.gameIndex = idx;

        var filestr = "GameList/game_" + gameinfo._KindID + ".png";
        if (false === jsb.fileUtils.isFileExist(filestr)) {
            filestr = "GameList/ImageFile.png";
        }

        var mask = null;
        var spTip = null;

        var cellpos = cc.p(323 * 0.5, 272 * 0.5);
        if (!cell) {
            cell = ccui.Button.create(filestr, filestr);
            cell.setContentSize(this.cellSizeForTable());
            cell.setSwallowTouches(false);
            cell.setName(gameinfo._KindName);

            var maskSp = new cc.Sprite(filestr);
            if (null != maskSp) {
                maskSp.setColor(cc.BLACK);
                maskSp.setOpacity(100);
                var size = maskSp.getContentSize();
                maskSp.setPosition(cc.p(size.width * 0.5, size.height * 0.5));
                maskSp.setName("download_mask_sp");

                mask = new ccui.Layout();
                mask.setClippingEnabled(true);
                mask.setAnchorPoint(cc.p(0.5, 0));
                mask.setPosition(cc.p(size.width * 0.5, 0));
                mask.setContentSize(size);
                mask.addChild(maskSp);

                cell.addChild(mask);
                mask.setName("download_mask");

                spTip = cc.LabelTTF.create("", "fonts/round_body.ttf", 32);
                spTip.enableOutline(cc.color(0, 0, 0, 255), 1);
                spTip.setPosition(cellpos);
                spTip.setName("download_mask_tip");
                cell.addChild(spTip);

                var cycle = new cc.Sprite(res.spinner_circle_png);
                if (null != cycle) {
                    cycle.setPosition(cellpos);
                    cycle.setVisible(false);
                    cycle.setScale(1.3);
                    cycle.setName("download_cycle");
                    cell.addChild(cycle);
                }
            }
        }
        else {
            cell.setTexture(filestr);
            mask = cell.getChildByName("download_mask");

            if (null != mask) {
                var sp = mask.getChildByName("download_mask_sp");
                if (null != sp) {
                    var size = sp.getContentSize();
                    sp.setTexture(filestr);
                    sp.setPosition(cc.p(size.width * 0.5, size.height * 0.5));
                    mask.setContentSize(size);
                }

                spTip = mask.getChildByName("download_mask_tip");
                if (null != spTip) {
                    var size = mask.getContentSize();
                    spTip.setPosition(cellpos);
                }
            }
        }

        if (null != mask) {
            mask.setVisible(!gameinfo._Active);
        }

        if (null != spTip) {
            spTip.setString("");
        }

        var btCallBack = function (ref, type) {
            this.tableCellTouched(this._scrollview, ref, type);
        };

        cell.setVisible(true);
        cell.setTag(idx);
        cell.addTouchEventListener(btCallBack);
        return cell;
    },

    ////////////////////////////////////////////////////////////////////-

    //链接游戏
    onLoadGameList: function (nKindID) {
        if (!nKindID) {
            this.dismissPopWait();
            var ru = cc.director.getRunningScene();
            if (null != ru) {
                showToast(ru, "游戏ID有误！", 1);
            }
            return;
        }
        GlobalUserItem.nCurGameKind = Number(nKindID);
        //如果是有游客
        if (GlobalUserItem.bVisitor) {
            if (this._logonFrame.onLogonByVisitor()) {
                this.showPopWait();
            }
        }
        //如果是第三方
        else if (GlobalUserItem.bThirdPartyLogin) {
            var td = GlobalUserItem.thirdPartyData;
            //szAccount, szNick, cbgender, platform
            if (this._logonFrame.onLoginByThirdParty(td.szAccount, td.szNick, td.cbGender, td.platform)) {
                this.showPopWait();
            }
        }
        else {
            if (this._logonFrame.onLogonByAccount(GlobalUserItem.szAccount, GlobalUserItem.szPassword)) {
                this.showPopWait();
            }
        }
    },

    //链接游戏回掉
    onLogonCallBack: function (result, message) {
        this.dismissPopWait();
        if (message != null && typeof message === "string") {
            var ru = cc.director.getRunningScene();
            if (null != ru) {
                showToast(ru, message, 2);
            }
        }
        if (result === 0) {
            this.onUpdataNotify();
        }
        else if (result === 1) {
            cc.log("GameListLayer onLogonCallBack");
            var clientscene = this.getParent().getParent();
            //判断是否是快速开始
            if (null != clientscene.m_bQuickStart && true === clientscene.m_bQuickStart) {
                var roominfo = GlobalUserItem.GetRoomInfo(GlobalUserItem.nCurRoomIndex);
                if (null === roominfo) {
                    return;
                }
                if (bit._and(roominfo.wServerKind, yl.GAME_GENRE_PERSONAL) != 0) {
                    //showToast(this, "房卡房间不支持快速开始！", 2)
                    return;
                }
                clientscene.onStartGame();
            }
            else {
                if (PriRoom && true === PriRoom.getInstance().onLoginEnterRoomList()) {
                    cc.log("GameListLayer.onLogonCallBack.GameListLayer enter priGame ");
                }
                else {
                    // 处理锁表
                    var lockRoom = GlobalUserItem.GetGameRoomInfo(GlobalUserItem.dwLockServerID);
                    if (GlobalUserItem.dwLockKindID === GlobalUserItem.nCurGameKind && null != lockRoom) {
                        GlobalUserItem.nCurRoomIndex = lockRoom._nRoomIndex;
                        clientscene.onStartGame();
                    }
                    else
                        clientscene.onChangeShowMode(yl.SCENE_ROOMLIST, this.m_bQuickStart);
                }
            }
        }
    },

    //显示等待
    showPopWait: function (isTransparent) {
        this.getParent().getParent().showPopWait(isTransparent);
    },

    showGameUpdateWait: function () {
        this.m_bGameUpdate = true;
        ExternalFun.popupTouchFilter(1, false, "游戏更新中,请稍候！");
    },

    dismissGameUpdateWait: function () {
        this.m_bGameUpdate = false;
        ExternalFun.dismissTouchFilter();
    },

    //关闭等待
    dismissPopWait: function () {
        this.getParent().getParent().dismissPopWait();
    },

    updateGame: function (gameinfo, index) {
        if (this.m_bUpdating) {
            cc.log("GameListLayer 正在更新游戏");
            return;
        }
        this.m_bUpdating = true;
        var targetPlatform = cc.sys.platform;
        if ((cc.sys.WIN32 === targetPlatform) || (cc.sys.MOBILE_BROWSER === targetPlatform) || (cc.sys.DESKTOP_BROWSER === targetPlatform)) {
            cc.log("GameListLayer win32 跳过更新");
            var app = this.getParent().getParent().getApp();
            //更新版本号
            app.getVersionMgr().setResVersion(gameinfo._ServerResVersion, gameinfo._KindID);
            this.onEnterGame(gameinfo);
        }
        else {
            var cell = null;
            if (null != index) {
                cell = this._showList[index];
            }

            this.onGameUpdate(gameinfo);
            if (null != cell) {
                this.m_spDownloadMask = cell.getChildByName("download_mask");
                if (null != this.m_spDownloadMask) {
                    this.m_szMaskSize = this.m_spDownloadMask.getContentSize();
                }
                this.m_labDownloadTip = cell.getChildByName("download_mask_tip");
                if (null != this.m_labDownloadTip) {
                    this.m_labDownloadTip.setString("0%");
                }
                this.m_spDownloadCycle = cell.getChildByName("download_cycle");
                if (null != this.m_spDownloadCycle) {
                    this.m_spDownloadCycle.stopAllActions();
                    this.m_spDownloadCycle.setVisible(true);
                    this.m_spDownloadCycle.runAction(cc.RepeatForever.create(cc.RotateBy.create(1.0, 360)));
                }
            }
        }
    },

    //更新游戏
    onGameUpdate: function (gameinfo) {
        //失败重试
        if (!gameinfo && this._update != null) {
            this.showGameUpdateWait();
            //this._txtTips.setString("同步服务器信息中+.")
            this._update.UpdateFile();
            return;
        }

        if (!gameinfo) {
            showToast(this, "无效游戏信息！", 1);
            return;
        }

        this.showGameUpdateWait();
        //this._txtTips.setString("同步服务器信息中+.")

        //记录
        if (gameinfo != null) {
            this._downgameinfo = gameinfo;
        }

        //更新参数
        var newfileurl = this.getParent().getParent().getApp()._updateUrl + "/game/" + this._downgameinfo._Module + "/res/filemd5List.json";
        var dst = jsb.fileUtils.getWritablePath() + "game/" + this._downgameinfo._Type + "/";

        var targetPlatform = cc.sys.platform;
        if (cc.sys.WIN32 === targetPlatform) {
            dst = jsb.fileUtils.getWritablePath() + "download/game/" + this._downgameinfo._Type + "/";
        }

        var src = jsb.fileUtils.getWritablePath() + "game/" + this._downgameinfo._Module + "/res/filemd5List.json";
        var downurl = this.getParent().getParent().getApp()._updateUrl + "/game/" + this._downgameinfo._Type + "/";

        //创建更新
        this._update = Update.create(newfileurl, dst, src, downurl);
        this._update.upDateClient(this);
    },

    onUpdataNotify: function () {
        showToast(this, "游戏版本信息错误！", 1);
    },

    //更新进度
    updateProgress: function (sub, msg, mainpersent) {
        var permsg = sprintf("%d%%", mainpersent);
        if (null != this.m_spDownloadMask) {
            var scale = (95 - mainpersent) / 100;
            this.m_spDownloadMask.setContentSize(this.m_szMaskSize.width, this.m_szMaskSize.height * scale);
        }

        if (null != this.m_labDownloadTip) {
            this.m_labDownloadTip.setString(permsg);
        }
    },

    //更新结果
    updateResult: function (result, msg) {
        if (null != this.m_spDownloadCycle) {
            this.m_spDownloadCycle.stopAllActions();
            this.m_spDownloadCycle.setVisible(false);
        }
        this.dismissGameUpdateWait();

        if (result === true) {
            this.m_bUpdating = false;
            var app = this.getParent().getParent().getApp();

            //更新版本号
            for (k in this._gameShowList) {
                if (this._gameShowList[k]._KindID === this._downgameinfo._KindID) {
                    app.getVersionMgr().setResVersion(this._gameShowList[k]._ServerResVersion, this._gameShowList[k]._KindID);
                    this._gameShowList[k]._Active = true;
                    break;
                }
            }

            if (null != this.m_labDownloadTip) {
                this.m_labDownloadTip.setString("");
            }

            this._txtTips.setString("OK");
            this.onEnterGame(this._downgameinfo);
        }
        else {
            this.m_bUpdating = false;
            var runScene = cc.director.getRunningScene();
            if (null != runScene) {
                if (null != this.m_spDownloadMask) {
                    this.m_spDownloadMask.setContentSize(this.m_szMaskSize.width, this.m_szMaskSize.height);
                }

                if (null != this.m_labDownloadTip) {
                    this.m_labDownloadTip.setString("");
                }

                this._txtTips.setString("");
                runScene.addChild(QueryDialog.create(msg + "\n是否重试？", function (bReTry) {
                    if (bReTry === true) {
                        this.onGameUpdate(this._downgameinfo);
                    }
                }));
            }
        }
    },

    onKeyBack: function () {
        return this.m_bGameUpdate != false;
    },
    //检查游戏有没有房间
    onExistenceGame: function (KindID) {
        var checkKind = Number(KindID);
        for (i = 0; i < GlobalUserItem.roomlist.length; i++) {
            var list = GlobalUserItem.roomlist[i];
            if (Number(list[1]) === checkKind) {
                if (!list[2]) {
                    break;
                }
                var nCount = 0;
                var serverList = list[2];
                for (k in serverList) {
                    if (serverList[k].wServerType === yl.GAME_GENRE_GOLD) {
                        nCount = nCount + 1;
                    }
                }
                return nCount;
            }
        }
        return 0;
    }
});

// var Update = appdf.req(appdf.BASE_SRC+"app.controllers.ClientUpdate")
// var QueryDialog = appdf.req(appdf.BASE_SRC+"app.views.layer.other.QueryDialog")
// var LogonFrame = appdf.req(appdf.CLIENT_SRC+"plaza.models.LogonFrame")
// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new GameListLayer();
//         this.addChild(layer);
//     }
// });