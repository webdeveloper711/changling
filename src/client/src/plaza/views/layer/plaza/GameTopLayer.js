/*
    author : kil
    date : 2017-12-5
 */
var GameTopLayer = cc.Layer.extend({
    //按钮标签
    BT_COIN: 1,     //金币场
    BT_CREAT: 2,    //创建房间按钮
    BT_QUICKSTART: 3,     //快速开始按钮
    BT_JOIN: 4,     //加入游戏
    ctor : function () {
        this._super();
        var self = this;

        var logonCallBack = function (result, message) {
            self.onLogonCallBack(result, message);
        };

        var btcallback = function (sender, target) {
            if (target == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(sender.getTag(), sender);
            }
        };

        this.Layer = ccs.load(res.GameTopLayer_json).node;
        this.addChild(this.Layer);

        this._logonFrame = new LogonFrame(this, logonCallBack);

        //金币场按钮
        this._btnCoinGame = this.Layer.getChildByName("btn_Coin");
        this._btnCoinGame.setTag(this.BT_COIN);
        this._btnCoinGame.addTouchEventListener(btcallback);

        //房卡场按钮
        this._btnCreatGame = this.Layer.getChildByName("btn_CreatRoom");
        this._btnCreatGame.setTag(this.BT_CREAT);
        this._btnCreatGame.addTouchEventListener(btcallback);

        //快速开始按钮
        this._btnQuickStart = this.Layer.getChildByName("btn_QuickStart");
        this._btnQuickStart.setTag(this.BT_QUICKSTART);
        this._btnQuickStart.addTouchEventListener(btcallback);

        //加入房间按钮
        this._btnJoinGame = this.Layer.getChildByName("btn_JoinRoom");
        this._btnJoinGame.setTag(this.BT_JOIN);
        this._btnJoinGame.addTouchEventListener(btcallback);

        if (PriRoom.getInstance().m_tabAllFeeConfigList.length == 0) {
            if (PriRoom.getInstance().getNetFrame().isSocketServer()) {
                PriRoom.getInstance().getNetFrame().sendQueryRoomParam();
            } else {
                PriRoom.getInstance().getNetFrame().onGetRoomParameter();
            }
        }
    },
    onButtonClickedEvent : function(tag, sender) {
        if (tag == this.BT_COIN) { //金币场按钮
            this.getParent().getParent().onChangeShowMode(yl.SCENE_BOINLIST);
        }
        else if (tag == this.BT_CREAT) { //房卡场按钮
            this.getParent().getParent().onChangeShowMode(yl.SCENE_CREATROOM);
        }
        else if (tag == this.BT_QUICKSTART) {//快速开始按钮效果增加
            this.getParent().getParent().onChangeShowMode(yl.SCENE_CREATROOM);
        }
        else if (tag == this.BT_JOIN) { //加入房间按钮
            PriRoom.getInstance().setViewFrame(this.getRootNode());
            PriRoom.getInstance().getTagLayer(PriRoom.LAYTAG.LAYER_ROOMID);
        }
    },
    //获取父场景节点(ClientScene)
    getRootNode : function() {
        var obj = this.getParent().getParent();
        return obj;
    },

    updateEnterGameInfo : function(info) {
        this.getRootNode().updateEnterGameInfo(info);
    },

    //链接游戏回掉
    onLogonCallBack : function(result,message) {
        this.dismissPopWait();
        if (message != null && typeof(message) == "string") {
            var ru = cc.director.getRunningScene();
            if (null != ru) {
                showToast(ru, message, 2);
            }
        }
        if (result == 0) {
            this.onUpdataNotify();
        } else if (result == 1) {
            cc.log("GameTopLayer onLogonCallBack");
            var clientscene = this.getRootNode();
            //判断是否是快速开始
            if (null != clientscene.m_bQuickStart && true == clientscene.m_bQuickStart) {
                var roominfo = GlobalUserItem.GetRoomInfo(GlobalUserItem.nCurRoomIndex);
                if (null == roominfo) {
                    return;
                }
                if (bit._and(roominfo.wServerKind, yl.GAME_GENRE_PERSONAL) != 0) {
                    //showToast(this, "房卡房间不支持快速开始！", 2)
                    return;
                }
                clientscene.onStartGame();
            }
            else {
                if (PriRoom && true == PriRoom.getInstance().onLoginEnterRoomList()) {
                    cc.log("GameTopLayer.onLogonCallBack.GameTopLayer enter priGame ");
                }
                else {
                    // 处理锁表
                    var lockRoom = GlobalUserItem.GetGameRoomInfo(GlobalUserItem.dwLockServerID);
                    if (GlobalUserItem.dwLockKindID == GlobalUserItem.nCurGameKind && null != lockRoom) {
                        GlobalUserItem.nCurRoomIndex = lockRoom._nRoomIndex;
                        clientscene.onStartGame();
                    }
                    else {
                        clientscene.onChangeShowMode(yl.SCENE_ROOMLIST, this.m_bQuickStart);
                    }
                }
            }
        }
    },

    //设置过度动画函数
    onEnterTransitionDidFinish : function () {
        this.onEnterTransitionFinish();
    },

    onExitTransitionDidStart : function () {
        this.onExitTransitionStart();
    },

    onExit : function () {
        if (this._logonFrame.isSocketServer()) {
            this._logonFrame.onCloseSocket();
        }
    },

    // 进入场景而且过渡动画结束时候触发。
    onEnterTransitionFinish : function() {
        return this;
    },

    // 退出场景而且开始过渡动画时候触发。
    onExitTransitionStart : function() {
        return this;
    },
});