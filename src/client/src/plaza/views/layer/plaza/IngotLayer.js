/*
Author. Li
商城界面
包含 JunFuTongPay IngotPay ShopLayer 三个类
 */

var CBT_WECHAT = 101;
var CBT_ALIPAY = 102;
var CBT_JFT = 103;

var PAYTYPE = {};
PAYTYPE[CBT_WECHAT] =
{
    str : "wx",
    plat : yl.ThirdParty.WECHAT
};
PAYTYPE[CBT_ALIPAY] =
{
    str : "zfb",
    plat : yl.ThirdParty.ALIPAY
};
PAYTYPE[CBT_JFT] =
{
    str : "jft",
    plat : yl.ThirdParty.JFT
};

var JFT_RETURN = 1;
//竣付通页面
var  JunFuTongPay = cc.Layer.extend({
    ctor : function(parent, itemname, price, paylist, token) {
        this._super();
        this.m_parent = parent;
        //todo
        // this.m_parent.m_bJunfuTongPay = true;
        GlobalUserItem.bJftPay = true;
        this.m_token = token;

        var self = this;
        //todo
        // ExternalFun.registerTouchEvent(this, true);
        var btnpos =
            [
                [cc.p(0.5, 0.3)],
                [cc.p(0.35, 0.3), cc.p(0.65, 0.3)]
            ];

        //加载csb资源
        var result = ExternalFun.loadRootCSB(res.JunFuTongPay_json, this);
        var rootLayer = result.rootlayer;
        var csbNode = result.csbnode;

        //背景
        var bg = csbNode.getChildByName("pay_bg");
        var bgsize = bg.getContentSize();

        itemname = itemname || "";
        price = price || 0;

        //商品名称
        var txt1 = bg.getChildByName("text_name");
        txt1.setString(itemname);

        //支付金额
        var txt2 = bg.getChildByName("text_price");
        txt2.setString(price);

        //按钮回调
        var btcallback = function (ref, tType) {
            if (tType === ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };

        //返回按钮
        var btn = bg.getChildByName("btn_return");
        btn.setTag(JFT_RETURN);
        btn.addTouchEventListener(btcallback);

        //微信支付
        btn = bg.getChildByName("jft_pay3");
        btn.addTouchEventListener(btcallback);
        btn.setVisible(false);
        btn.setEnabled(false);

        //支付宝支付
        btn = bg.getChildByName("jft_pay4");
        btn.addTouchEventListener(btcallback);
        btn.setVisible(false);
        btn.setEnabled(false);

        var str = "";
        //todo
        // var tpos = btnpos[paylist.length] || {};
        var tpos = 3;
        for (k in paylist) {
            str = "jft_pay" + paylist[k];
            var paybtn = bg.getChildByName(str);
            if (null != paybtn) {
                paybtn.setEnabled(true);
                paybtn.setVisible(true);
                paybtn.setTag(paylist[k]);
                var pos = tpos[k];
                if (null != pos) {
                    paybtn.setPosition(cc.p(pos.x * bgsize.width, pos.y * bgsize.height));
                }
            }
        }

        //调起支付
        this.m_bCallPay = false;

        //监听
        function eventCall(event) {
            if (true === self.m_bCallPay) {
                self.queryUserScoreInfo();
            }
        }

        //todo two
        // this.m_listener = cc.EventListener.create(yl.RY_JFTPAY_NOTIFY, handler(this, eventCall));
        // cc.director.getEventDispatcher().addEventListenerWithSceneGraphPriority(this.m_listener, this);
    },

    queryUserScoreInfo : function() {
        var self = this;
        if (null != this.m_parent.queryUserScoreInfo) {
            this.m_parent.queryUserScoreInfo(function (needUpdate) {
                if (true === needUpdate) {
                    self.m_parent.updateScoreInfo();
                    //重新请求支付列表
                    self.m_parent.reloadBeanList();
                }
                GlobalUserItem.bJftPay = false;
                self.removeFromParent();
            });
        }
    },

    onTouchBegan : function(touch, event) {
        return this.isVisible();
    },

    onExit : function() {
        if (null != this.m_listener) {
            cc.director.getEventDispatcher().removeEventListener(this.m_listener);
            this.m_listener = null;
        }
    },

    onButtonClickedEvent : function(tag, sender) {
        var self = this;
        if (tag === JFT_RETURN) {
            this.m_parent.m_bJunfuTongPay = false;
            GlobalUserItem.bJftPay = false;
            //重新请求支付列表
            this.m_parent.reloadBeanList();
            this.removeFromParent();
        }
        else {
            var plat = 0;
            var str = "";
            if (3 === tag) {
                plat = yl.ThirdParty.WECHAT;
                str = "微信未安装,无法进行微信支付";
            }
            else if (4 === tag) {
                plat = yl.ThirdParty.ALIPAY;
                str = "支付宝未安装,无法进行支付宝支付";
            }
            //判断应用是否安装
            if (false === MultiPlatform.getInstance().isPlatformInstalled(plat)) {
                showToast(this, str, 2, cc.color(250, 0, 0, 255));
                return;
            }
            this.m_parent.showPopWait();
            this.runAction(cc.Sequence.create(cc.DelayTime.create(5), cc.CallFunc.create(function () {
                self.m_parent.dismissPopWait();
            })));

            function payCallBack(param) {
                // this.m_parent.dismissPopWait()
                // if (typeof(param) === "string" && "true" === param) {
                //     GlobalUserItem.setTodayPay();
                //
                //     showToast(this, "支付成功", 2);
                //     //更新用户游戏豆
                //     GlobalUserItem.dUserBeans = GlobalUserItem.dUserBeans + this.m_nCount;
                //     //通知更新
                //     var eventListener = cc.EventCustom.new(yl.RY_USERINFO_NOTIFY);
                //     eventListener.obj = yl.RY_MSG_USERWEALTH
                //     cc.director.getEventDispatcher().dispatchEvent(eventListener);
                //
                //     this.hide();
                //     //重新请求支付列表
                //     this.m_parent.reloadBeanList();
                // }
                // else {
                //     showToast(this, "支付异常", 2);
                // }
            }

            this.m_bCallPay = true;
            MultiPlatform.getInstance().thirdPartyPay(PAYTYPE[CBT_JFT].plat, {
                paytype: tag,
                token: this.m_token
            }, payCallBack);
        }
    }
});


var BT_CLOSE = 201;
var BT_SURE = 202;
var BT_CANCEL = 203;

//支付选择页面
var IngotPay = cc.Layer.extend({
    ctor : function(parent) {
        this._super();
        this.m_parent = parent;
        //价格
        this.m_fPrice = 0.00;
        //数量
        this.m_nCount = 0;
        // appid
        this.m_nAppId = 0;

        var self = this;

        //注册触摸事件
        //todo
        // ExternalFun.registerTouchEvent(this, true);

        //加载csb资源
        var result = ExternalFun.loadRootCSB(res.ShopPayLayer_json, this);
        var rootLayer = result.rootlayer;
        var csbNode = result.csbnode;
        cc.log(csbNode);
        this.m_spBgKuang = csbNode.getChildByName("shop_pay_bg");
        this.m_spBgKuang.setScale(0.0001);
        var bg = this.m_spBgKuang;

        //商品
        this.m_textProName = bg.getChildByName("text_name");
        this.m_textProName.setString("");

        //价格
        this.m_textPrice = bg.getChildByName("text_price");
        this.m_textPrice.setString("");

        //按钮回调
        var btcallback = function (ref, type) {
            if (type === ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };
        //关闭按钮
        var btn = bg.getChildByName("btn_close");
        btn.setTag(BT_CLOSE);
        btn.addTouchEventListener(btcallback);

        //确定按钮
        btn = bg.getChildByName("btn_sure");
        btn.setTag(BT_SURE);
        btn.addTouchEventListener(btcallback);

        //取消按钮
        btn = bg.getChildByName("btn_cancel");
        btn.setTag(BT_CANCEL);
        btn.addTouchEventListener(btcallback);

        var cbtlistener = function (sender, eventType) {
            self.onSelectedEvent(sender.getTag(), sender);
        };

        var enableList = [];
        //微信支付
        var cbt = bg.getChildByName("check_wechat");
        cbt.setTag(CBT_WECHAT);
        cbt.setSelected(true);
        cbt.addEventListener(cbtlistener);
        cbt.setVisible(false);
        cbt.setEnabled(false);
        this.m_cbtWeChat = cbt;
        if (yl.WeChat.PartnerID != " " && yl.WeChat.PartnerID != "") {
            enableList.push("check_wechat");
        }
        var wpos = cc.p(this.m_cbtWeChat.getPositionX(), this.m_cbtWeChat.getPositionY());
        var wtext = cbt.getChildByName("txt");
        wtext.setVisible(false);
        var wtpos = cc.p(wtext.getPositionX(), wtext.getPositionY());

        //支付宝支付
        cbt = bg.getChildByName("check_alipay");
        cbt.setTag(CBT_ALIPAY);
        cbt.setSelected(false);
        cbt.addEventListener(cbtlistener);
        cbt.setVisible(false);
        cbt.setEnabled(false);
        this.m_cbtAlipay = cbt;
        if (yl.AliPay.PartnerID != " " && yl.AliPay.PartnerID != "") {
            enableList.push("check_alipay");
        }
        var apos = cc.p(this.m_cbtAlipay.getPositionX(), this.m_cbtAlipay.getPositionY());
        var atext = cbt.getChildByName("txt");
        atext.setVisible(false);
        var atpos = cc.p(atext.getPositionX(), atext.getPositionY());

        //竣付通支付
        cbt = bg.getChildByName("check_jft");
        cbt.setTag(CBT_JFT);
        cbt.setSelected(false);
        cbt.addEventListener(cbtlistener);
        cbt.setVisible(false);
        cbt.setEnabled(false);
        this.m_cbtJft = cbt;
        if (yl.JFT.PartnerID != " " && yl.JFT.PartnerID != "") {
            enableList.push("check_jft");
        }
        var jpos = cc.p(this.m_cbtJft.getPositionX(), this.m_cbtJft.getPositionY());

        var jtext = cbt.getChildByName("txt");
        jtext.setVisible(false);
        var jtpos = cc.p(jtext.getPositionX(), jtext.getPositionY());

        var cbtPosition =
            [
                [wpos],
                [wpos, apos],
                [wpos, apos, jpos]
            ];
        var textPosition =
            [
                [wtpos],
                [wtpos, atpos],
                [wtpos, atpos, jtpos]
            ];
        var poslist = cbtPosition[enableList.length];
        var tposlist = textPosition[enableList.length];
        for (k in enableList) {
            var tmp = bg.getChildByName(enableList[k]);
            if (null != tmp) {
                tmp.setEnabled(true);
                tmp.setVisible(true);

                pos = poslist[k];
                if (null != pos) {
                    tmp.setPosition(pos);
                }
            }
            tmp = bg.getChildByName(enableList[k] + "_t");
            if (null != tmp) {
                tmp.setVisible(true);
                var pos = tposlist[k];
                if (null != pos) {
                    tmp.setPosition(pos);
                }
            }
        }

        this.m_select = null;
        if (enableList.length > 0) {
            var tmp = bg.getChildByName(enableList[0]);
            // var tmp = bg.getChildByName(enableList[1]);
            if (null != tmp) {
                tmp.setSelected(true);
                this.m_select = tmp.getTag();
            }
        }

        //加载动画
        this.m_actShowAct = cc.ScaleTo.create(0.2, 1.0);
        ExternalFun.SAFE_RETAIN(this.m_actShowAct);

        var scale = cc.ScaleTo.create(0.2, 0.0001);
        var call = cc.CallFunc.create(function () {
            this.showLayer(false);
        });
        this.m_actHideAct = cc.Sequence.create(scale, call);
        ExternalFun.SAFE_RETAIN(this.m_actHideAct);

        this.showLayer(false);
    },

    isPayMethodValid : function() {
        if (yl.WeChat.PartnerID != " " && yl.WeChat.PartnerID != ""
            || (yl.AliPay.PartnerID != " " && yl.AliPay.PartnerID != "")
            || (yl.JFT.PartnerID != " " && yl.JFT.PartnerID != "")) {
            return true;
        }
        else {
            return false;
        }
    },

    showLayer : function(par) {
        this.setVisible(par);

        if (true === par) {
            this.m_spBgKuang.stopAllActions();
            this.m_spBgKuang.runAction(this.m_actShowAct);
        }
    },

    refresh : function(count, name, sprice, fprice, appid) {
        this.m_textProName.setString(name);
        this.m_textPrice.setString(sprice);

        this.m_fPrice = fprice;
        this.m_nCount = count;
        this.m_nAppId = appid;
    },

    onButtonClickedEvent : function(tag, sender) {
        var self = this;
        if (tag === BT_CLOSE || tag === BT_CANCEL) {
            this.hide();
        }
        else if (tag === BT_SURE) {
            if (null === this.m_select) {
                return;
            }

            var str = "无法支付";
            var plat = PAYTYPE[this.m_select].plat;
            if (yl.ThirdParty.WECHAT === PAYTYPE[this.m_select].plat) {
                plat = yl.ThirdParty.WECHAT;
                str = "微信未安装,无法进行微信支付";
            }
            else if (yl.ThirdParty.ALIPAY === PAYTYPE[this.m_select].plat) {
                plat = yl.ThirdParty.ALIPAY;
                str = "支付宝未安装,无法进行支付宝支付";
            }
            //判断应用是否安装
            if (false === MultiPlatform.getInstance().isPlatformInstalled(plat) && plat != yl.ThirdParty.JFT) {
                showToast(this, str, 2, cc.color(250, 0, 0, 255));
                return;
            }

            this.m_parent.showPopWait();
            this.runAction(cc.Sequence.create(cc.DelayTime.create(5), cc.CallFunc.create(function () {
                self.m_parent.dismissPopWait();
            })));
            //生成订单
            var url = yl.HTTP_URL + "/WS/MobileInterface.ashx";
            var account = GlobalUserItem.dwGameID;
            // if (GlobalUserItem.bThirdPartyLogin){
            //     account = GlobalUserItem.thirdPartyData.szAccount;
            // }
            // account = string.urlencode(account);
            var action = "action=CreatPayOrderID&gameid=" + account + "&amount=" + this.m_fPrice + "&paytype=" + PAYTYPE[this.m_select].str + "&appid=" + this.m_nAppId;
            cc.log(action);
            // todo following will be needed
            appdf.onHttpJsionTable(url,"GET",action,function(jstable,jsdata) {
                cc.log(jstable + "jstable" + 6);
                if (typeof(jstable) === "array") {
                    var data = jstable["data"];
                    if (typeof(data) === "array") {
                        if (null != data["valid"] && true === data["valid"]) {
                            var payparam = {};
                            if (self.m_select === CBT_WECHAT) { //微信支付
                                //获取微信支付订单id
                                var paypackage = data["PayPackage"];
                                if (typeof(paypackage) === "string") {
                                    // var ok, paypackagetable = pcall(function()
                                    // 	return JSON.parse(paypackage)
                                    // end)
                                    try {
                                        paypackagetable = JSON.parse(paypackage);
                                        var payid = paypackagetable["prepayid"];
                                        if (null === payid) {
                                            showToast(self, "微信支付订单获取异常", 2);
                                            return;
                                        }
                                        payparam["info"] = paypackagetable;
                                    } catch (e) {
                                        showToast(self, "微信支付订单获取异常", 2);
                                        return;
                                    }
                                }
                            }
                            else if (self.m_select === CBT_JFT) { //竣付通支付
                                self.onJunFuTongPay(data);
                                return;
                            }
                            //订单id
                            payparam["orderid"] = data["OrderID"];
                            //价格
                            payparam["price"] = self.m_fPrice;
                            //商品名
                            payparam["name"] = self.m_textProName.getString();

                            function payCallBack(param) {
                                self.m_parent.dismissPopWait();
                                if (typeof(param) === "string" && "true" === param) {
                                    GlobalUserItem.setTodayPay();

                                    showToast(self, "支付成功", 2);
                                    //更新用户游戏豆
                                    GlobalUserItem.dUserBeans = GlobalUserItem.dUserBeans + self.m_nCount;
                                    //通知更新
                                    var eventListener = cc.EventCustom.new(yl.RY_USERINFO_NOTIFY);
                                    eventListener.obj = yl.RY_MSG_USERWEALTH;
                                    cc.director.getEventDispatcher().dispatchEvent(eventListener);

                                    self.hide();
                                    //重新请求支付列表
                                    self.m_parent.reloadBeanList();

                                    self.m_parent.updateScoreInfo();
                                }
                                else {
                                    showToast(self, "支付异常", 2);
                                }
                            }

                            MultiPlatform.getInstance().thirdPartyPay(PAYTYPE[self.m_select].plat, payparam, payCallBack);
                        }
                        else {
                            if (typeof(jstable["msg"]) === "string" && jstable["msg"] != "") {
                                showToast(self, jstable["msg"], 2);
                            }
                        }
                    }
                }
            });
        }
    },

    onSelectedEvent : function(tag, sender) {
        if (this.m_select === tag) {
            this.m_spBgKuang.getChildByTag(tag).setSelected(true);
            return;
        }

        this.m_select = tag;
        // for i=101,103 do
        for (i = 101; i < 103; i++) {
            if (i != tag) {
                this.m_spBgKuang.getChildByTag(i).setSelected(false);
            }
        }

        //微信支付
        if (tag === CBT_WECHAT) {
            cc.log("wechat");
        }

        //支付宝
        if (tag === CBT_ALIPAY) {
            cc.log("alipay");
        }

        //俊付通
        if (tag === CBT_JFT) {
            cc.log("jft");
        }
    },

    onTouchBegan : function(touch, event) {
        return this.isVisible();
    },

    onTouchEnded : function(touch, event) {
        var pos = touch.getLocation();
        var m_spBg = this.m_spBgKuang;
        pos = m_spBg.convertToNodeSpace(pos);
        var rec = cc.rect(0, 0, m_spBg.getContentSize().width, m_spBg.getContentSize().height);
        if (false === cc.rectContainsPoint(rec, pos)) {
            this.hide();
        }
    },

    onExit : function() {
        ExternalFun.SAFE_RELEASE(this.m_actShowAct);
        this.m_actShowAct = null;
        ExternalFun.SAFE_RELEASE(this.m_actHideAct);
        this.m_actHideAct = null;
    },

    onJunFuTongPay : function(data) {
        // token参数
        var tokenparam = {};
        var self = this;
        tokenparam.uid = yl.JFT["PartnerID"];
        tokenparam.oid = data["OrderID"] || "";
        tokenparam.mon = "" + this.m_fPrice;
        tokenparam.rurl = yl.JFT["NotifyURL"];
        tokenparam.nurl = yl.JFT["NotifyURL"];
        tokenparam.serviceType = "JFT";
        var szparam = JSON.stringify(tokenparam);

        //获取支付列表
        this.m_parent.showPopWait();
        MultiPlatform.getInstance().getPayList(szparam, function (listjson) {
            self.m_parent.dismissPopWait();
            if (typeof(listjson) === "string" && "" != listjson) {
                // var ok, listtable = pcall(function () {
                //     return JSON.parse(listjson);
                // });
                try {
                    listtable = JSON.parse(listjson);
                    cc.log(listtable, "listtable", 6);
                    // typeid=3 为微信， typeid=4为支付宝
                    var itemname = self.m_textProName.getString();
                    var itemprice = self.m_textPrice.getString();
                    var jft = JunFuTongPay.create(self.m_parent, itemname, itemprice, listtable, token);
                    self.m_parent.addChild(jft);
                    self.hide()
                } catch (e) {
                }
            }
        });
    },

    hide : function() {
        this.m_spBgKuang.stopAllActions();
        this.m_spBgKuang.runAction(this.m_actHideAct);
    }
});

// 支付模式
var APPSTOREPAY = 10; // iap支付
var THIRDPAY = 20; // 第三方支付

//商城页面
var IngotLayer = cc.Layer.extend({
    CBT_SCORE: 1,
    CBT_BEAN: 2,
    CBT_VIP: 3,
    CBT_PROPERTY: 4,
    CBT_ENTITY: 5,
    CBT_BEAN_SCORE: 6,
    CBT_BEAN_ROOMCARD: 7,

    BT_SCORE: 30,
    BT_VIP: 50,
    BT_PROPERTY: 60,
    BT_GOODS: 120,
    BT_BEAN: 520,

    BT_ORDERRECORD: 1001,
    BT_BAG: 1002,

    BEAN_BEAN: 2, //豆
    BEAN_SCORE: 1, //金币
    BEAN_ROOMCARD: 3, //房卡
    //scene
    //stmod 进入商店后的选择类型
    ctor: function (scene, stmod) {
        this._super(cc.color(0, 0, 0, 125));
        //todo
        stmod = stmod || IngotLayer.CBT_PROPERTY;
        this.m_nPayMethod = GlobalUserItem.tabShopCache["nPayMethod"] || THIRDPAY;
        this._scene = scene;
        var self = this;

        // this.registerScriptHandler(function (eventType) {
        //     if (eventType === "enterTransitionFinish") {	// 进入场景而且过渡动画结束时候触发。
        //         self.onEnterTransitionFinish();
        //     }
        //     else if (eventType === "exitTransitionStart") {	// 退出场景而且开始过渡动画时候触发。
        //         self.onExitTransitionStart();
        //     }
        // });

        //按钮回调
        this._btcallback = function (ref, type) {
            if (type === ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };

        var cbtlistener = function (sender, eventType) {
            self.onSelectedEvent(sender.getTag(), sender, eventType);
        };

        this._select = stmod;

        this._showList = [];

        this._scoreList = GlobalUserItem.tabShopCache["shopScoreList"] || [];
        this._propertyList = GlobalUserItem.tabShopCache["shopPropertyList"] || [];
        this._vipList = GlobalUserItem.tabShopCache["shopVipList"] || [];
        //游戏豆购买列表
        this._beanList = GlobalUserItem.tabShopCache["shopBeanList"] || [];
        //实物兑换页面
        this._goodsList = null;
        //商店物品typeid
        this._shopTypeIdList = GlobalUserItem.tabShopCache["shopTypeIdList"] || [];
        //购买界面
        this.m_payLayer = null;
        //购买汇率
        this.m_nRate = GlobalUserItem.tabShopCache["shopRate"] || 0;
        //竣付通支付界面
        this.m_bJunfuTongPay = false;
        //道具关联信息
        this.m_tabPropertyRelate = GlobalUserItem.tabShopCache["propertyRelate"] || [];

        var frame = cc.spriteFrameCache.getSpriteFrame("sp_public_frame_0.png");

        //返回按钮
        var btn1 = new ccui.Button(res.bt_return_0_png, res.bt_return_1_png);
        btn1.setPosition(75, yl.HEIGHT - 51);
        btn1.addTouchEventListener(function (ref, type) {
            if (type === ccui.Widget.TOUCH_ENDED) {
                self._scene.onKeyBack();
            }
        });
        this.addChild(btn1);


        //兑换记录
        var topBtn = new ccui.Button(res.btn_ubag_0_png, res.btn_ubag_1_png);
        topBtn.setPosition(yl.WIDTH - 90, yl.HEIGHT - 51);
        topBtn.setTag(IngotLayer.BT_BAG);
        this.addChild(topBtn);
        topBtn.addTouchEventListener(function (ref, tType) {
            if (tType === ccui.Widget.TOUCH_ENDED) {
                var tag = ref.getTag();
                if (tag === IngotLayer.BT_ORDERRECORD) {
                    self._scene.onChangeShowMode(yl.SCENE_ORDERRECORD);
                }
                else if (tag === IngotLayer.BT_BAG) {
                    self._scene.onChangeShowMode(yl.SCENE_BAG);
                }
            }
        });

        this.m_btnTopBtn = topBtn;
        this.m_btnTopBtn.setVisible(false);

        var sp1 = new cc.Sprite(res.sp_Ingot_bg_png);
        sp1.setPosition(yl.WIDTH * 0.5, yl.HEIGHT * 0.5);
        this.addChild(sp1);

        var str = ".";
        this._txtGold = new cc.LabelAtlas(string_formatNumberThousands(GlobalUserItem.lUserScore, true, "/"), res.ig_num_shop_0_png, 16, 22, str);
        this._txtGold.setPosition(293, 80);
        this._txtGold.setAnchorPoint(cc.p(0, 0.5));
        this._txtGold.setVisible(false);
        this.addChild(this._txtGold);
        this._txtIngot = new cc.LabelAtlas(string_formatNumberThousands(GlobalUserItem.lUserIngot, true, "/"), res.ig_num_shop_0_png, 16, 22, str);
        this._txtIngot.setPosition(924, 80);
        this._txtIngot.setAnchorPoint(cc.p(0, 0.5));
        this._txtIngot.setVisible(false);
        this.addChild(this._txtIngot);

        this._txtRoomCard = new cc.LabelAtlas(string_formatNumberThousands(GlobalUserItem.lUserIngot, true, "/"), res.ig_num_shop_0_png, 16, 22, str);
        this._txtRoomCard.setPosition(603, 80);
        this._txtRoomCard.setAnchorPoint(cc.p(0, 0.5));
        this._txtRoomCard.setVisible(false);
        this.addChild(this._txtRoomCard);

        //游戏币
        var chk1 = new ccui.CheckBox(res.bt_shop_0_0_png, res.bt_shop_0_0_png, res.bt_shop_0_1_png, "", "");
        chk1.setPosition(190, 530);
        chk1.setSelected(false);
        chk1.setVisible(false);
        chk1.setEnabled(false);
        chk1.setName("check" + 5);
        chk1.setTag(IngotLayer.CBT_SCORE);
        chk1.addEventListener(cbtlistener);
        this.addChild(chk1);


        //游戏豆
        var chk2 = new ccui.CheckBox(res.bt_shop_1_0_png, res.bt_shop_1_0_png, res.bt_shop_1_1_png, "", "");
        chk2.setPosition(190, 530 - 104);
        chk2.setSelected(false);
        chk2.setVisible(false);
        chk2.setEnabled(false);
        chk2.setName("check" + 6);
        chk2.setTag(IngotLayer.CBT_BEAN);
        chk2.addEventListener(cbtlistener);
        this.addChild(chk2);


        //VIP
        var chk3 = new ccui.CheckBox(res.bt_shop_2_0_png, res.bt_shop_2_0_png, res.bt_shop_2_1_png, "", "");
        chk3.setPosition(190, 530 - 104 * 2);
        chk3.setSelected(false);
        chk3.setVisible(false);
        chk3.setEnabled(false);
        chk3.setName("check" + 7);
        chk3.setTag(IngotLayer.CBT_VIP);
        chk3.addEventListener(cbtlistener);
        this.addChild(chk3);


        //道具
        var chk4 = new ccui.CheckBox(res.bt_shop_3_0_png, res.bt_shop_3_0_png, res.bt_shop_3_1_png, "", "");
        chk4.setPosition(190, 530 - 104 * 3);
        chk4.setSelected(false);
        chk4.setVisible(false);
        chk4.setEnabled(false);
        chk4.setName("check" + 8);
        chk4.setTag(IngotLayer.CBT_PROPERTY);
        chk4.addEventListener(cbtlistener);
        this.addChild(chk4);


        //实物
        var chk5 = new ccui.CheckBox(res.bt_shop_4_0_png, res.bt_shop_4_0_png, res.bt_shop_4_1_png, "", "");
        chk5.setPosition(190, 530 - 104 * 4);
        chk5.setSelected(false);
        chk5.setVisible(false);
        chk5.setEnabled(false);
        chk5.setName("check" + 9);
        chk5.setTag(IngotLayer.CBT_ENTITY);
        chk5.addEventListener(cbtlistener);
        this.addChild(chk5);

        ////////////////////////
        //游戏豆分支中的现金充值金币和现金充值房卡
        this.m_cbScoreOrRoomCard = 0;
        //现金充值金币
        var chk6 = new ccui.CheckBox(res.bt_shop_bean_0_0_png, res.bt_shop_bean_0_0_png, res.bt_shop_bean_0_1_png, "", "");
        chk6.setPosition(yl.WIDTH * 0.5, 633);
        chk6.setAnchorPoint(cc.p(1, 0.5));
        chk6.setSelected(false);
        chk6.setVisible(false);
        chk6.setEnabled(false);
        chk6.setName("check_bean_" + 1);
        chk6.setTag(IngotLayer.CBT_BEAN_SCORE);
        chk6.addEventListener(cbtlistener);
        this.addChild(chk6);

        //现金充值房卡
        var chk7 = new ccui.CheckBox(res.bt_shop_bean_1_0_png, res.bt_shop_bean_1_0_png, res.bt_shop_bean_1_1_png, "", "");
        chk7.setPosition(yl.WIDTH * 0.5, 633);
        chk7.setAnchorPoint(cc.p(0, 0.5));
        chk7.setSelected(false);
        chk7.setVisible(false);
        chk7.setEnabled(false);
        chk7.setName("check_bean_" + 2);
        chk7.setTag(IngotLayer.CBT_BEAN_ROOMCARD);
        chk7.addEventListener(cbtlistener);
        this.addChild(chk7);

        //////////////////////
        this.m_tabCheckBoxPosition =
            [
                [cc.p(190, 530)],
                [cc.p(190, 530), cc.p(190, 426)],
                [cc.p(190, 530), cc.p(190, 426), cc.p(190, 322)],
                [cc.p(190, 530), cc.p(190, 426), cc.p(190, 322), cc.p(190, 218)],
                [cc.p(190, 530), cc.p(190, 426), cc.p(190, 322), cc.p(190, 218), cc.p(190, 114)],
                [cc.p(190, 530), cc.p(190, 426), cc.p(190, 322), cc.p(190, 218), cc.p(190, 114), cc.p(190, 10)]
            ];
        this.m_tabActiveCheckBox = GlobalUserItem.tabShopCache["shopActiveCheckBox"] || [];

        this._scrollView = new ccui.ScrollView();
        //this._scrollView.setContentSize(cc.size(938,520))
        this._scrollView.setContentSize(cc.size(1100, 410));
        this._scrollView.setAnchorPoint(cc.p(0.5, 0.5));
        this._scrollView.setPosition(cc.p(yl.WIDTH * 0.49, 314));
        this._scrollView.setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL);
        this._scrollView.setBounceEnabled(true);
        this._scrollView.setScrollBarEnabled(false);
        this.addChild(this._scrollView);
    },

    // 进入场景而且过渡动画结束时候触发。
    onEnterTransitionFinish : function() {
        //this.loadPropertyAndVip(this._select)
        //todo following method doesn't fixed
        if (0 === table.nums(this._shopTypeIdList)) {
            this.getShopPropertyType();
        }
        else {
            //刷新界面显示
            this.updateCheckBoxList();
        }
        return this;
    },

    // 退出场景而且开始过渡动画时候触发。
    onExitTransitionStart : function() {
        return this;
    },

    updateCheckBoxList : function() {
        var poslist = this.m_tabCheckBoxPosition[this.m_tabActiveCheckBox.length];
        if (null === poslist) {
            return;
        }
        for (k in this.m_tabActiveCheckBox) {
            var tmp = this.getChildByName(this.m_tabActiveCheckBox[k]);
            if (null != tmp) {
                tmp.setEnabled(true);
                tmp.setVisible(true);

                var pos = poslist[k];
                if (null != pos) {
                    tmp.setPosition(pos);
                }
            }
        }

        //选择的类型
        var tmp = this.getChildByTag(this._select);
        if (null != tmp && tmp.isVisible()) {
            tmp.setSelected(true);
            //请求物品列表
            this.loadPropertyAndVip(this._select);
        }

        for (k in this.m_tabActiveCheckBox) {
            var tmp = this.getChildByName(this.m_tabActiveCheckBox[k]);
            if (null != tmp) {
                tmp.setEnabled(false);
                tmp.setVisible(false);

                var pos = poslist[k];
                if (null != pos) {
                    tmp.setPosition(pos);
                }
            }
        }

    },

    getShopPropertyType : function() {
        var self = this;
        this._scene.showPopWait();
        //todo following will be needed
        appdf.onHttpJsionTable(yl.HTTP_URL + "/WS/MobileInterface.ashx","GET","action=GetMobilePropertyType",function(jstable,jsdata) {
            self._scene.dismissPopWait();
            cc.log(jstable+"jstable"+6);
            if (typeof(jstable) === "array") {
                var data = jstable["data"];
                if (typeof(data) === "array") {
                    if (null != data["valid"] && true === data["valid"]) {
                        var list = data["list"];
                        if (typeof(list) === "array") {
                            for (k in list) {
                                self._shopTypeIdList["check" + list[k].TypeID] = list[k];
                                self.m_tabActiveCheckBox.puch("check" + list[k].TypeID);
                            }
                            GlobalUserItem.tabShopCache["shopTypeIdList"] = self._shopTypeIdList;
                            GlobalUserItem.tabShopCache["shopActiveCheckBox"] = self.m_tabActiveCheckBox;
                            //刷新界面显示
                            self.updateCheckBoxList();
                            return;
                        }
                    }
                }

                var msg = jstable["msg"];
                if (typeof(msg) === "string") {
                    showToast(self, msg, 2);
                }
            }
        });

    },

    //按键监听
    onButtonClickedEvent : function(tag,sender) {
        var self = this;
        var beginPos = sender.getTouchBeganPosition();
        var endPos = sender.getTouchEndPosition();
        if (Math.abs(endPos.x - beginPos.x) > 30
            || Math.abs(endPos.y - beginPos.y) > 30) {
            cc.log("IngotLayer.onButtonClickedEvent ==> MoveTouch Filter");
            return;
        }

        var name = sender.getName();

        if (name === SHOP_BUY[IngotLayer.BT_SCORE]) {
            //游戏币获取
            GlobalUserItem.buyItem = this._scoreList[tag - IngotLayer.BT_SCORE];
            if (GlobalUserItem.buyItem.id === "game_score" && PriRoom) {
                this.getParent().getParent().onChangeShowMode(PriRoom.LAYTAG.LAYER_EXCHANGESCORE, GlobalUserItem.buyItem.resultGold);
            }
            else {
                this.getParent().getParent().onChangeShowMode(yl.SCENE_SHOPDETAIL);
            }
        }
        else if (name === SHOP_BUY[IngotLayer.BT_BEAN]) {
            //游戏豆获取
            var item = this._beanList[tag - IngotLayer.BT_BEAN];
            if (null === item) {
                return;
            }
            var bThirdPay = true;

            if (ClientConfig.APPSTORE_VERSION
                && (targetPlatform === cc.PLATFORM_OS_IPHONE || targetPlatform === cc.PLATFORM_OS_IPAD)) {
                if (this.m_nPayMethod === APPSTOREPAY) {
                    bThirdPay = false;
                    var payparam = [];
                    payparam.http_url = yl.HTTP_URL;
                    payparam.uid = GlobalUserItem.dwUserID;
                    payparam.productid = item.nProductID;
                    payparam.price = item.price;

                    this.showPopWait();
                    this.runAction(cc.Sequence.create(cc.DelayTime.create(5), cc.CallFunc.create(function () {
                        self.dismissPopWait();
                    })));
                    showToast(this, "正在连接iTunes Store+.", 4);

                    function payCallBack(param) {
                        if (typeof(param) === "string" && "true" === param) {
                            GlobalUserItem.setTodayPay();

                            showToast(self, "支付成功", 2);
                            //更新用户游戏豆
                            GlobalUserItem.dUserBeans = GlobalUserItem.dUserBeans + item.count;
                            //通知更新
                            var eventListener = cc.EventCustom.new(yl.RY_USERINFO_NOTIFY);
                            eventListener.obj = yl.RY_MSG_USERWEALTH;
                            cc.director.getEventDispatcher().dispatchEvent(eventListener);

                            //重新请求支付列表
                            self.reloadBeanList();

                            self.updateScoreInfo();
                        }
                        else {
                            showToast(self, "支付异常", 2);
                        }
                    }

                    MultiPlatform.getInstance().thirdPartyPay(yl.ThirdParty.IAP, payparam, payCallBack);
                }
            }

            if (bThirdPay) {
                if (false === IngotPay.isPayMethodValid()) {
                    showToast(this, "支付服务未开通!", 2, cc.color(250, 0, 0, 255));
                    return;
                }
                if (null === this.m_payLayer) {
                    this.m_payLayer = IngotPay.create(this);
                    this.addChild(this.m_payLayer);
                }
                var sprice = sprintf("%.2f元", item.price);
                this.m_payLayer.refresh(item.count, item.name, sprice, item.price, item.appid);
                this.m_payLayer.showLayer(true);
            }
        }
        else if (name === SHOP_BUY[IngotLayer.BT_VIP]) {
            //vip购买
            GlobalUserItem.buyItem = this._vipList[tag - IngotLayer.BT_VIP];
            this.getParent().getParent().onChangeShowMode(yl.SCENE_SHOPDETAIL);
        }
        else if (name === SHOP_BUY[IngotLayer.BT_PROPERTY]) {
            //道具购买
            GlobalUserItem.buyItem = this._propertyList[tag - IngotLayer.BT_PROPERTY];
            if (GlobalUserItem.buyItem.id === "room_card" && PriRoom) {
                this.getParent().getParent().onChangeShowMode(PriRoom.LAYTAG.LAYER_BUYCARD, GlobalUserItem.buyItem);
            }
            else {
                this.getParent().getParent().onChangeShowMode(yl.SCENE_SHOPDETAIL);
            }
        }
    },

    onSelectedEvent : function(tag,sender,eventType) {

        if (this._select === tag) {
            this.getChildByTag(tag).setSelected(true);
            return;
        }

        this._select = tag;
        // for (i=i;i<5;i++){
        for (i = 0; i < 5; i++) {
            if (i != tag) {
                this.getChildByTag(i).setSelected(false);
            }
            this.getChildByTag(i).setVisible(false);
        }
        for (i = 6; i <= 7; i++) {
            if (i != tag) {
                this.getChildByTag(i).setSelected(false);
            }
        }

        //游戏币
        if (tag === IngotLayer.CBT_SCORE) {
            if (0 === this._scoreList.length) {
                this.loadPropertyAndVip(tag);
            }
            else {
                this.onUpdateScore();
            }
        }

        //游戏豆 // 现金充值游戏豆
        if (tag === IngotLayer.CBT_BEAN) {
            this.onClearShowList();
            this.m_cbScoreOrRoomCard = IngotLayer.BEAN_BEAN;
            if (0 === this._beanList.length) {
                this.loadPropertyAndVip(tag);
            }
            else {
                this.onUpdateBeanList();
            }
        }

        //游戏豆//分支现金充值金币
        if (tag === IngotLayer.CBT_BEAN_SCORE) {
            this._select = IngotLayer.CBT_BEAN;
            this.m_cbScoreOrRoomCard = IngotLayer.BEAN_SCORE;
            this.onClearShowList();
            if (0 === this._beanList.length) {
                this.loadPropertyAndVip(tag);
            }
            else {
                this.onUpdateBeanList();
            }
        }

        //游戏豆//分支现金充值房卡
        if (tag === IngotLayer.CBT_BEAN_ROOMCARD) {
            this._select = IngotLayer.CBT_BEAN;
            this.m_cbScoreOrRoomCard = IngotLayer.BEAN_ROOMCARD;
            this.onClearShowList();
            if (0 === this._beanList.length) {
                this.loadPropertyAndVip(tag);
            }
            else {
                this.onUpdateBeanList();
            }
        }

        //vip
        if (tag === IngotLayer.CBT_VIP) {
            if (this._vipList.length === 0) {
                this.loadPropertyAndVip(tag);
            }
            else {
                this.onUpdateVIP();
            }
        }

        //道具
        if (tag === IngotLayer.CBT_PROPERTY) {
            if (this._propertyList.length === 0) {
                this.loadPropertyAndVip(tag);
            }
            else {
                this.onUpdateProperty();
            }
        }

        var topBtnTag = IngotLayer.BT_BAG;
        var normalFile = "Information/btn_ubag_0.png";
        var pressFile = "Information/btn_ubag_1.png";
        //实物
        if (tag === IngotLayer.CBT_ENTITY) {
            this.onClearShowList();
            this.onUpdateGoodsList();

            topBtnTag = IngotLayer.BT_ORDERRECORD;
            normalFile = "Ingot/bt_shop_exchange_0.png";
            pressFile = "Ingot/bt_shop_exchange_1.png";
        }
        this.m_btnTopBtn.setTag(topBtnTag);
        this.m_btnTopBtn.loadTextureNormal(normalFile);
        this.m_btnTopBtn.loadTexturePressed(pressFile);
    },

    //网络请求
    loadPropertyAndVip : function(tag) {
        var self = this;
        var typid = 0;

        var cbt = this.getChildByTag(tag);
        if (null != cbt) {
            if (null != this._shopTypeIdList[cbt.getName()]) {
                typid = this._shopTypeIdList[cbt.getName()].TypeID;
            }
        }

        //实物特殊处理
        if (tag === IngotLayer.CBT_ENTITY) {
            this.onUpdateGoodsList();
        }
        //游戏豆额外处理
        else if (tag === IngotLayer.CBT_BEAN) {
            if (0 != this._beanList.length) {
                this.onUpdateBeanList();
                return;
            }
            this._scene.showPopWait();
            if (ClientConfig.APPSTORE_VERSION
                && (targetPlatform === cc.PLATFORM_OS_IPHONE || targetPlatform === cc.PLATFORM_OS_IPAD)) {
                //todo following will be needed
                // 内购开关
                appdf.onHttpJsionTable(yl.HTTP_URL + "/WS/MobileInterface.ashx","GET","action=iosnotappstorepayswitch",function(jstable,jsdata) {
                    var errmsg = "获取支付配置异常!";
                    if (typeof(jstable) === "array") {
                        var jdata = jstable["data"];
                        if (typeof(jdata) === "array") {
                            var valid = jdata["valid"] || false;
                            if (true === valid) {
                                errmsg = null;
                                var value = jdata["State"] || "0";
                                value = Number(value);
                                if (1 === value) {
                                    GlobalUserItem.tabShopCache["nPayMethod"] = APPSTOREPAY;
                                    self.m_nPayMethod = APPSTOREPAY;
                                    self.requestPayList(1);
                                }
                                else {
                                    GlobalUserItem.tabShopCache["nPayMethod"] = THIRDPAY;
                                    // 请求列表
                                    self.requestPayList();
                                }
                            }
                        }
                    }

                    self._scene.dismissPopWait();
                    if (typeof(errmsg) === "string" && "" != errmsg) {
                        showToast(self, errmsg, 2, cc.color(250, 0, 0));
                    }
                });
            }
            else {
                // 请求列表
                this.requestPayList();
            }
            if (tag === IngotLayer.CBT_VIP && 0 != this._vipList.length) {
                this.onUpdateVIP();
                return;
            }
            if (tag === IngotLayer.CBT_PROPERTY && 0 != this._propertyList.length) {
                this.onUpdateProperty();
                return;
            }
            if (tag === IngotLayer.CBT_SCORE && 0 != this._scoreList.length) {
                this.onUpdateScore();
                return;
            }
            this.requestPropertyList(typid, tag);
        }
    },

    requestPayList : function(isIap) {
        var self = this;
        isIap = isIap || 0;
        var beanurl = yl.HTTP_URL + "/WS/MobileInterface.ashx";
        var ostime = (new Date()).getTime();

        this._scene.showPopWait();
        //todo following will be needed
        appdf.onHttpJsionTable(beanurl, "GET", "action=GetPayProduct&userid=" + GlobalUserItem.dwUserID + "&time=" + ostime + "&signature=" + GlobalUserItem.getSignature(ostime) + "&typeID=" + isIap, function (sjstable, sjsdata) {
            cc.log(sjstable + "支付列表" + 6);
            var errmsg = "获取支付列表异常!";
            self._scene.dismissPopWait();
            if (typeof(sjstable) === "array") {
                var sjdata = sjstable["data"];
                var msg = sjstable["msg"];
                errmsg = null;
                if (typeof(msg) === "string") {
                    errmsg = msg;
                }

                if (typeof(sjdata) === "array") {
                    var isFirstPay = sjdata["IsPay"] || "0";
                    isFirstPay = Number(isFirstPay);
                    var sjlist = sjdata["list"];
                    if (typeof(sjlist) === "array") {
                        for (i = 0; i < sjlist.length; i++) {
                            var sitem = sjlist[i];
                            var item = [];
                            item.price = sitem["Price"];
                            item.isfirstpay = isFirstPay;
                            item.paysend = sitem["AttachCurrency"] || "0";
                            item.paysend = Number(item.paysend);
                            item.paycount = sitem["PresentCurrency"] || "0";
                            item.paycount = Number(item.paycount);
                            item.price = Number(item.price);
                            item.count = item.paysend + item.paycount;
                            item.description = sitem["Description"];
                            item.name = sitem["ProductName"];
                            item.sortid = Number(sitem["SortID"]) || 0;
                            item.nOrder = 0;
                            item.appid = Number(sitem["AppID"]);
                            item.nProductID = sitem["ProductID"] || "";
                            item.PayObjectType = sitem["PayObjectType"] || 0;

                            //首充赠送
                            if (0 != item.paysend) {
                                //当日未首充
                                if (0 === isFirstPay) {
                                    item.nOrder = 1;
                                    self._beanList.push(item);
                                }
                                self._beanList.push(item);
                            }
                        }
                        for (i = 0; i < self._beanList.length; i++) {
                            for (j = i; j < self._beanList.length; j++) {
                                if (self._beanList[i].nOrder != self._beanList[j].nOrder) {
                                    if (self._beanList[i].nOrder < self._beanList[j].nOrder) {
                                        var tmp = self._beanList[i];
                                        self._beanList[i] = self._beanList[j];
                                        self._beanList[j] = tmp;
                                    }
                                } else {
                                    if (self._beanList[i].sortid > self._beanList[j].sortid) {
                                        var tmp = self._beanList[i];
                                        self._beanList[i] = self._beanList[j];
                                        self._beanList[j] = tmp;
                                    }
                                }
                            }
                        }
                        // table.sort(self._beanList, function(a,b)
                        //         if (a.nOrder != b.nOrder){
                        //             return a.nOrder > b.nOrder
                        //             {
                        //             return a.sortid < b.sortid
                        //         end
                        //     end)
                        GlobalUserItem.tabShopCache["shopBeanList"] = self._beanList;
                        self.onUpdateBeanList();
                    }
                }
            }

            if (typeof(errmsg) === "string" && "" != errmsg) {
                showToast(self, errmsg, 2, cc.color(250, 0, 0));
            }
        });
    },

    requestPropertyList : function(nTypeID, tag) {
        var self = this;
        nTypeID = nTypeID || 0;
        this._scene.showPopWait();
        //todo following will be needed
        appdf.onHttpJsionTable(yl.HTTP_URL + "/WS/MobileInterface.ashx","GET","action=GetMobileProperty&TypeID=" + nTypeID,function(jstable,jsdata) {
            self._scene.dismissPopWait();
            cc.log(jstable + "jstable" + 7);
            if (typeof(jstable) === "array") {
                var code = jstable["code"];
                var tmpList = [];

                if (Number(code) === 0) {
                    var datax = jstable["data"];
                    if (datax) {
                        var valid = datax["valid"];
                        if (valid === true) {
                            var listcount = datax["total"];
                            var list = datax["list"];
                            if (typeof(list) === "array") {
                                for (i = 0; i < list.length; i++) {
                                    var item = [];
                                    item.id = Number(list[i]["ID"]);
                                    item.name = list[i]["Name"];
                                    item.bean = Number(list[i]["Cash"]);
                                    item.gold = Number(list[i]["Gold"]);
                                    item.ingot = Number(list[i]["UserMedal"]);
                                    item.loveliness = Number(list[i]["LoveLiness"]);
                                    item.resultGold = Number(list[i]["UseResultsGold"]);
                                    item.description = list[i]["RegulationsInfo"];
                                    item.sortid = list[i]["SortID"] || "0";
                                    item.sortid = Number(item.sortid);
                                    item.minPrice = Number(list[i]["minPrice"]) || 0;
                                    if (0 != item.id && null != item.id) {
                                        if (item.loveliness != 0 && (item.bean === 0 && item.gold === 0 && item.ingot === 0)) {
                                            if (tag === IngotLayer.CBT_PROPERTY && item.id === 501) {
                                                if (GlobalUserItem.bEnableRoomCard) {
                                                    item.id = "room_card";
                                                    tmpList.push(item);
                                                }
                                            }
                                            else if (tag === IngotLayer.CBT_SCORE && item.id === 501 && 0 != item.resultGold) {
                                                if (GlobalUserItem.bEnableRoomCard) {
                                                    item.id = "game_score";
                                                    item.minPrice = item.resultGold;
                                                    item.bean = 0;
                                                    item.gold = 0;
                                                    item.ingot = 0;
                                                    item.loveliness = 0;
                                                    tmpList.push(item);
                                                }
                                                tmpList.push(item);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //产品排序
                for (i = 0; i < tmpList.length; i++) {
                    for (j = i; j < tmpList.length; j++) {
                        if (tmpList[i].sortid > tmpList[j].sortid) {
                            var tmp = tmpList[i];
                            tmpList[i] = tmpList[j];
                            tmpList[j] = tmp;
                        }
                    }
                }
                // table.sort(tmpList, function(a,b)
                //     return a.sortid < b.sortid
                // end)

                if (tag === IngotLayer.CBT_VIP) {
                    GlobalUserItem.tabShopCache["shopVipList"] = tmpList;
                    self._vipList = tmpList;
                    self.onUpdateVIP();
                }
                else if (tag === IngotLayer.CBT_PROPERTY) {
                    GlobalUserItem.tabShopCache["shopPropertyList"] = tmpList;
                    self._propertyList = tmpList;
                    self.onUpdateProperty();
                }
                else if (tag === IngotLayer.CBT_SCORE) {
                    GlobalUserItem.tabShopCache["shopScoreList"] = tmpList;
                    self._scoreList = tmpList;
                    self.onUpdateScore();
                }
            }
            else {
                showToast(self, "抱歉，获取道具信息失败！", 2, cc.color(250, 0, 0));
            }
        });
    },

    reloadBeanList : function() {
        this.onClearShowList();
        GlobalUserItem.tabShopCache["shopBeanList"] = [];
        this._beanList = [];
        this.loadPropertyAndVip(IngotLayer.CBT_BEAN);
    },

    updateScoreInfo : function() {
        this._txtGold.setString(string_formatNumberThousands(GlobalUserItem.lUserScore, true, "/"));
        this._txtBean.setString(string_formatNumberThousands(GlobalUserItem.dUserBeans, true, "/"));
        this._txtIngot.setString(string_formatNumberThousands(GlobalUserItem.lUserIngot, true, "/"));
        this._txtRoomCard.setString(string_formatNumberThousands(GlobalUserItem.lRoomCard, true, "/"));

    },

    //更新游戏币
    onUpdateScore : function() {
        this.onClearShowList();
        this.onUpdateShowList(this._scoreList, IngotLayer.BT_SCORE);
    },

    //更新游戏豆
    onUpdateBeanList : function() {
        this.onClearShowList();
        this.onUpdateShowList(this._beanList, IngotLayer.BT_BEAN);
    },

    //更新VIP
    onUpdateVIP : function() {
        this.onClearShowList();
        this.onUpdateShowList(this._vipList, IngotLayer.BT_VIP);
    },

    //更新道具
    onUpdateProperty : function() {
        this.onClearShowList();
        this.onUpdateShowList(this._propertyList, IngotLayer.BT_PROPERTY);
    },

    //更新实物兑换列表
    onUpdateGoodsList : function() {
        var self = this;
        var url = yl.HTTP_URL + "/SyncLogin.aspx?userid=" + GlobalUserItem.dwUserID + "&time=" + (new Date()).getTime() + "&signature=" + GlobalUserItem.getSignature((new Date()).getTime()) + "&url=/Mobile/Shop/Goods.aspx";
        if (null === this._goodsList) {
            //平台判定
            var targetPlatform = cc.sys.platform;
            if ((cc.sys.IPHONE == targetPlatform) || (cc.sys.IPAD == targetPlatform) || (cc.sys.ANDROID == targetPlatform)) {
                this._goodsList = ccui.WebView.create();
                this._goodsList.setPosition(cc.p(805, 324));
                this._goodsList.setContentSize(cc.size(938, 520));

                this._goodsList.setJavascriptInterfaceScheme("ryweb");
                this._goodsList.setScalesPageToFit(true);
                this._goodsList.setOnJSCallback(function (sender, url) {
                    this.queryUserScoreInfo(function (ok) {
                        if (ok) {
                            self.updateScoreInfo();
                            self._goodsList.reload();
                        }
                    });
                });

                this._goodsList.setOnDidFailLoading(function (sender, url) {
                    self._scene.dismissPopWait();
                    cc.log("open " + url + " fail");
                });
                this._goodsList.setOnShouldStartLoading(function (sender, url) {
                    cc.log("onWebViewShouldStartLoading, url is ", url);
                    return true;
                });
                this._goodsList.setOnDidFinishLoading(function (sender, url) {
                    self._scene.dismissPopWait()
                    ExternalFun.visibleWebView(self._goodsList, true);
                    cc.log("onWebViewDidFinishLoading, url is ", url);
                });
                this.addChild(this._goodsList);
            }
        }

        if (null != this._goodsList) {
            this._scene.showPopWait();
            ExternalFun.visibleWebView(this._goodsList, false);
            this._goodsList.loadURL(url);
        }
    },

    //清除当前显示
    onClearShowList : function() {
        for (i = 0; i < this._showList.length; i++) {
            this._showList[i].removeFromParent();
        }
        this._showList = null;
        this._showList = [];

        if (null != this._goodsList) {
            this._goodsList.removeFromParent();
            this._goodsList = null;
        }
    },

    //更新当前显示
    onUpdateShowList : function(theList,tag){
        var bGold = (this._select===IngotLayer.CBT_SCORE);
        var bOther= (this._select!==IngotLayer.CBT_SCORE);
        var bBean = (this._select===IngotLayer.CBT_BEAN);

        // for (i =  IngotLayer.CBT_SCORE;i<IngotLayer.CBT_ENTITY;i++){
        for (i =  IngotLayer.CBT_SCORE;i<=IngotLayer.CBT_ENTITY;i++) {
            // if (this._select != i){
            this.getChildByTag(i).setVisible(false);
            // end
        }
        // for (i =  IngotLayer.CBT_BEAN_SCORE;i<=IngotLayer.CBT_BEAN_ROOMCARD;i++){
        for (i =  IngotLayer.CBT_BEAN_SCORE;i<=IngotLayer.CBT_BEAN_ROOMCARD;i++) {
            this.getChildByTag(i).setSelected(false);
            if (IngotLayer.BEAN_SCORE === this.m_cbScoreOrRoomCard) {
                this.getChildByTag(IngotLayer.CBT_BEAN_SCORE).setSelected(true);
            }
            else if (IngotLayer.BEAN_ROOMCARD === this.m_cbScoreOrRoomCard) {
                this.getChildByTag(IngotLayer.CBT_BEAN_ROOMCARD).setSelected(true);
            }
//         this.getChildByTag(i).setVisible(true)
//         this.getChildByTag(i).setEnabled(true)
        }

        //计算scroll滑动高度
        var scrollHeight = 0;
        if (theList.length<5) {
            scrollHeight = 400;
            this._scrollView.setInnerContainerSize(cc.size(1130, 430));
        }
        else {
            scrollHeight = 400 * Math.ceil(theList.length / 4);//Math.floor((#theList+Math.floor(#theList%3))/3)
            this._scrollView.setInnerContainerSize(cc.size(1130, scrollHeight + 30));
        }

        for (i=0;i<theList.length;i++){
            var item = theList[i];
            var nCount = 0;
            var bShow  = false;
            if (item.PayObjectType === this.m_cbScoreOrRoomCard || bBean === false) {
                bShow = true;
                nCount = this._showList.length + 1;
                this._showList[nCount] = cc.LayerColor.create(cc.color(100, 100, 100, 0), 261, 240);
                this._showList[nCount].setPosition(160 + Math.floor((nCount - 1) % 4) * 270 - 130, scrollHeight - (8 + 120 + Math.floor((nCount - 1) / 4) * 400) - 150);
                this._scrollView.addChild(this._showList[nCount]);

                var btn = ccui.Button.create("Ingot/frame_shop_7.png", "Ingot/frame_shop_7.png");
                btn.setContentSize(cc.size(261, 240));
                btn.setPosition(130, 120);
                btn.setTag(tag + i);
                btn.setSwallowTouches(false);
                btn.setName(SHOP_BUY[tag]);
                btn.addTouchEventListener(this._btcallback);
                this._showList[nCount].addChild(btn);

                var sp1 = new cc.Sprite("Ingot/frame_Ingot_sp_bg_1.png");
                sp1.setPosition(130, 160);
                this._showList[nCount].addChild(sp1);

                var sp2 = new cc.Sprite("Ingot/Ingot_bt_duiHuan.png");
                sp2.setPosition(130, -20);
                this._showList[nCount].addChild(sp2);

            }
            var price = 0;
            var sign = null;
            var pricestr = "";
            var signTxt = "";

            //物品信息
            var showSp = null;
            //标题
            var titleSp = null;
            if (bBean){
                if (bShow){
                    var cbTypeImg = 0;
                    var cbTypeText = 0;
                    if (item.PayObjectType === IngotLayer.BEAN_SCORE) { //游戏币
                        cbTypeImg = 4;
                        cbTypeText = 0;
                    }
                    else if (item.PayObjectType === IngotLayer.BEAN_BEAN) { //游戏豆
                        cbTypeImg = 5;
                        cbTypeText = 1;
                    }
                    else if (item.PayObjectType === IngotLayer.BEAN_ROOMCARD) { //房卡
                        cbTypeImg = 3;
                        cbTypeText = 3;
                    }
                    showSp = new cc.Sprite("Ingot/icon_shop_" + cbTypeImg +".png");
                    var str1 = item.count + "";
                    var str2 = "/";
                    var atlas = cc.LabelAtlas._create(str1.replace("[.]", "/"), "Ingot/num_shop_5.png", 20, 25, str2);
                    atlas.setAnchorPoint(cc.p(1.0,0.5));
                    this._showList[nCount].addChild(atlas);
                    var name = new cc.Sprite("Ingot/text_shop_" + cbTypeText + ".png");
                    name.setAnchorPoint(cc.p(0,0.5));
                    this._showList[nCount].addChild(name);
                    var wid = (atlas.getContentSize().width + name.getContentSize().width) / 2;
                    atlas.setPosition(130 + (atlas.getContentSize().width - wid), 250);
                    name.setPosition(atlas.getPositionX(), 250);

                    //首充
                    if (null != item.paysend && 0 != item.paysend) {
                        var fsp = new cc.Sprite("Ingot/shop_firstpay_sp.png");
                        fsp.setAnchorPoint(cc.p(0, 1.0));
                        fsp.setPosition(-8, 248);
                        this._showList[nCount].addChild(fsp);
                        var isFirstPay = item.isfirstpay === 0;
                        btn.setEnabled(isFirstPay);
                    }
                }
                price = item.price;
                pricestr = "￥" + string_formatNumberThousands(price,true,",");
            }
            else {
                var frame = cc.spriteFrameCache.getSpriteFrame("icon_public_" + item.id + ".png");
                if (null != frame) {
                    showSp = new cc.SpriteWithSpriteFrame(frame);
                }
                if (jsb.fileUtils.isFileExist("Ingot/title_property_" + item.id + ".png")) {
                    titleSp = new cc.Sprite("Ingot/title_property_" + item.id + ".png");
                }

                // var goldLabel = cc.LabelAtlas._create(string_formatNumberThousands(item.resultGold,true,"/"), "Ingot/num_shop_1.png", 20, 25, string.byte("/"))
                // 	.setPosition(130,220)
                // 	.setAnchorPoint(cc.p(0.5,0.5))
                // 	.setVisible(bGold)
                // 	.addChild(this._showList[i])
                // new cc.Sprite("Ingot/sign_shop_2.png")
                // .setPosition(130-goldLabel.getContentSize().width/2-13,220)
                // .setVisible(bGold)
                // .addChild(this._showList[i])
                sign = 0;
                signTxt = "豆";
                if (item.bean === 0) {
                    if (item.ingot === 0) {
                        if (item.gold === 0) {
                            if (item.loveliness === 0) {
                                price = item.minPrice;
                                sign = 4;
                                signTxt = "房卡";
                            }
                            else {
                                price = item.loveliness;
                                sign = 3;
                                signTxt = "";

                            }
                        }
                        else {
                            price = item.gold;
                            sign = 2;
                            signTxt = "金币";
                        }
                    }
                    else {
                        price = item.ingot;
                        sign = 1;
                        signTxt = "元宝";
                    }
                }
                else {
                    price = item.bean;
                }
                pricestr = string_formatNumberThousands(price, true, ",");

                //
                var icon_star = new cc.Sprite("Ingot/icon_star_sp.png");
                if (null != icon_star) {
                    icon_star.setPosition(130, 140);
                    this._showList[nCount].addChild(icon_star, 1);
                }
            }
            if (bShow || bBean === false) {
                if (price === 0) {
                    cc.log("======= ***** 价格信息有误 ***** =======");
                    return;
                }

                if (null != showSp) {
                    showSp.setPosition(130, 160);
                    this._showList[nCount].addChild(showSp);
                }
                if (null != titleSp) {
                    titleSp.setPosition(130, 280);
                    //titleSp.setVisible(bOther)
                    this._showList[nCount].addChild(titleSp);
                }

                var priceLabel = new cc.LabelTTF("需要" + pricestr + signTxt, res.round_body_ttf, 24);
                priceLabel.setAnchorPoint(cc.p(0.5, 0.5));
                priceLabel.setPosition(100, -64);
                priceLabel.setTextColor(cc.color(255, 255, 255, 255));
                this._showList[nCount].addChild(priceLabel);

                if (null != sign) {
                    var width = 0;
                    if (jsb.fileUtils.isFileExist("Ingot/sign_shop_" + sign + ".png")) {
                        var spsign = new cc.Sprite("Ingot/sign_shop_" + sign + ".png");
                        spsign.setVisible(false);
                        width = spsign.getContentSize().width + priceLabel.getContentSize().width * 1.2;
                        spsign.setAnchorPoint(cc.p(0.0, 0.5));
                        spsign.setPosition((260 - width) / 2, -64);
                        this._showList[nCount].addChild(spsign);
                        width = (260 - width) * 0.5 + spsign.getContentSize().width;
                    }

                    priceLabel.setAnchorPoint(cc.p(0, 0.5));
                    priceLabel.setPosition(width, -64);
                }
            }
        }
    },

    onKeyBack : function() {
        if (null != this.m_payLayer) {
            if (true === this.m_payLayer.isVisible()) {
                return true;
            }

            if (true === this.m_bJunfuTongPay) {
                return true;
            }
        }
        return false;
    },

    showPopWait : function() {
        this._scene.showPopWait();
    },

    dismissPopWait : function() {
        this._scene.dismissPopWait();
    },

    queryUserScoreInfo : function(queryCallback) {
        if (null != this._scene.queryUserScoreInfo) {
            this._scene.queryUserScoreInfo(queryCallback);
        }
    }
});

var SHOP_BUY = {};
SHOP_BUY[IngotLayer.BT_SCORE] = "shop_score_buy";
SHOP_BUY[IngotLayer.BT_BEAN] = "shop_bean_buy";
SHOP_BUY[IngotLayer.BT_VIP] = "shop_vip_buy";
SHOP_BUY[IngotLayer.BT_PROPERTY] = "shop_prop_buy";
SHOP_BUY[IngotLayer.BT_GOODS] = "shop_goods_buy";

// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new IngotLayer();
//         this.addChild(layer);
//     }
// });