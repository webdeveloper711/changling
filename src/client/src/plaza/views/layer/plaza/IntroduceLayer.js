/*
Author. Li
 */
// 玩法介绍
var TAG_MASK = 101;
var BTN_CLOSE = 102;

var IntroduceLayer = cc.Layer.extend({
    ctor : function( scene, url ) {
        this._super();
        this._scene = scene;
        var self = this;
        url = url || yl.HTTP_URL;

        //todo following statement is error two parameter
        // 加载csb资源
        var result = ExternalFun.loadRootCSB(res.IntroduceLayer_json, this);
        var rootLayer = result.rootlayer;
        var csbNode = result.csbnode;

        var touchFunC = function (ref, tType) {
            if (tType === ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };

        // 遮罩
        var mask = csbNode.getChildByName("panel_mask");
        mask.setTag(TAG_MASK);
        mask.addTouchEventListener(touchFunC);

        var image_bg = csbNode.getChildByName("image_bg");
        image_bg.setTouchEnabled(true);
        image_bg.setSwallowTouches(true);

        // 退出按钮
        var btn = image_bg.getChildByName("btn_close");
        btn.setTag(BTN_CLOSE);
        btn.addTouchEventListener(touchFunC);

        // 界面
        var tmp = image_bg.getChildByName("content");
        //平台判定
        var targetPlatform = cc.sys.platform;
        if ((cc.sys.IPHONE == targetPlatform) || (cc.sys.IPAD == targetPlatform) || (cc.sys.ANDROID == targetPlatform)) {
            //介绍页面
            this.m_webView = ccui.WebView.create();
            this.m_webView.setPosition(tmp.getPosition());
            this.m_webView.setContentSize(tmp.getContentSize());

            this.m_webView.setScalesPageToFit(true);
            this.m_webView.loadURL(url);
            ExternalFun.visibleWebView(this.m_webView, false);
            this._scene.showPopWait();

            this.m_webView.setOnJSCallback(function (sender, url) {
            });

            this.m_webView.setOnDidFailLoading(function (sender, url) {
                self._scene.dismissPopWait();
                cc.log("open " + url + " fail");
            });
            this.m_webView.setOnShouldStartLoading(function (sender, url) {
                cc.log("onWebViewShouldStartLoading, url is ", url);
                return true;
            });
            this.m_webView.setOnDidFinishLoading(function (sender, url) {
                self._scene.dismissPopWait();
                ExternalFun.visibleWebView(self.m_webView, true);
            });
            image_bg.addChild(this.m_webView);
        }
        tmp.removeFromParent();
    },

    onButtonClickedEvent : function(tag, ref) {
        if (TAG_MASK === tag || BTN_CLOSE === tag) {
            this._scene.dismissPopWait();
            this.removeFromParent();
        }
    },
    // scrollview 创建
    createLayer : function(scene, nKindId, nType) {
        var self = this;
        if (null === nKindId || null === nType) {
            return null;
        }
        this._scene = scene;
        var parent = ccui.Layout.create();
        parent.setTouchEnabled(false);
        //todo following statement is error two parameter
        // 加载csb资源
        var res = ExternalFun.loadRootCSB("plaza/IntroduceLayer.csb", parent);
        var rootLayer = res[0];
        var csbNode = res[1];

        var touchFunC = function (ref, tType) {
            if (tType === ccui.Widget.TOUCH_ENDED) {
                self._scene.dismissPopWait();
                parent.removeFromParent();
            }
        };

        // 遮罩
        var mask = csbNode.getChildByName("panel_mask");
        mask.setTag(TAG_MASK);
        mask.addTouchEventListener(touchFunC);

        var image_bg = csbNode.getChildByName("image_bg");
        image_bg.setTouchEnabled(true);
        image_bg.setSwallowTouches(true);

        // 退出按钮
        var btn = image_bg.getChildByName("btn_close");
        btn.setTag(BTN_CLOSE);
        btn.addTouchEventListener(touchFunC);

        // 界面
        var tmp = image_bg.getChildByName("content");
        // 读取文本
        this._scrollView = ccui.ScrollView.create();
        this._scrollView.setContentSize(tmp.getContentSize());
        this._scrollView.setPosition(tmp.getPosition());
        this._scrollView.setAnchorPoint(tmp.getAnchorPoint());
        this._scrollView.setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL);
        this._scrollView.setBounceEnabled(true);
        this._scrollView.setScrollBarEnabled(false);
        image_bg.addChild(this._scrollView);
        tmp.removeFromParent();

        var tabKindList = GlobalUserItem.tabIntroduceCache[nKindId];
        var szIntroduce = null;
        if (typeof(tabKindList) === "array") {
            var tips = tabKindList[nType];
            if (typeof(tips) === "string") {
                szIntroduce = tips;
            }
        }
        else {
            GlobalUserItem.tabIntroduceCache[nKindId] = [];
        }
        if (null === szIntroduce) {
            if (typeof(scene.showPopWait) === "function") {
                scene.showPopWait();
            }
            var url = yl.HTTP_URL + "/WS/MobileInterface.ashx?action=getgameintroduce&kindid=" + nKindId + "&typeid=" + nType;
            //todo following statement will be needed
            appdf.onHttpJsionTable(url ,"GET","",function(jstable,jsdata) {
                if (typeof(jstable) === "array") {
                    var data = jstable["data"];
                    var msg = jstable["msg"];
                    if (typeof(data) === "array") {
                        var content = data["Content"];
                        if (typeof(content) === "string") {
                            msg = null;
                            self.refreshIntroduce(content);
                            GlobalUserItem.tabIntroduceCache[nKindId][nType] = content;
                        }
                    }
                }
                if (typeof(scene.dismissPopWait) === "function") {
                    scene.dismissPopWait();
                }
                if (typeof(msg) === "string" && "" != msg) {
                    showToast(image_bg, msg, 2);
                }
            });
        }
        else
            this.refreshIntroduce(szIntroduce);
        return parent;
    },

    refreshIntroduce : function( szTips ) {
        var viewSize = this._scrollView.getContentSize();
        this._strLabel = cc.Label.createWithTTF(szTips, res.round_body_ttf, 25);
        this._strLabel.setLineBreakWithoutSpace(true);
        this._strLabel.setMaxLineWidth(viewSize.width);
        this._strLabel.setTextColor(cc.color(0, 0, 0, 255));
        this._strLabel.setAnchorPoint(cc.p(0.5, 1.0));
        this._scrollView.addChild(his._strLabel);
        var labelSize = this._strLabel.getContentSize();
        var fHeight = labelSize.height > viewSize.height && labelSize.height || viewSize.height;
        this._strLabel.setPosition(cc.p(viewSize.width * 0.5, fHeight));
        this._scrollView.setInnerContainerSize(cc.size(viewSize.width, labelSize.height));
    }
});
// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new IntroduceLayer();
//         this.addChild(layer);
//     }
// });