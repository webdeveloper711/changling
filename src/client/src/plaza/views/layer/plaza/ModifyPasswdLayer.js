//
// Author. zhong
// Date. 2016-08-02 15.57.41
//密码修改界面
//按钮
var BT_EXIT = 101;
var BT_SURE = 102;
var BT_CANCEL = 103;
//复选框
var CBT_LOGIN = 201;
var CBT_BANK = 202;

var ModifyPasswdLayer = cc.Layer.extend({
    ctor : function( scene ) {
        this._super();
        //todo
        // ExternalFun.registerNodeEvent(this);
        this._scene = scene;
        var self = this;

        //加载csb资源
        var result = ExternalFun.loadRootCSB(res.ModifyPassLayer_json, this);
        var rootLayer = result.rootlayer;
        var csbNode = result.csbnode;
        this.m_csbNode = csbNode;

        //旧密码
        this.m_spOldPass = csbNode.getChildByName("sp_pass_old");
        //新密码
        this.m_spNewPass = csbNode.getChildByName("sp_pass_new");

        function btncallback(ref, type) {
            if (type === ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        }

        //返回按钮
        var btn = csbNode.getChildByName("back_btn");
        btn.setTag(BT_EXIT);
        btn.addTouchEventListener(btncallback);
        //确定按钮
        btn = csbNode.getChildByName("sure_btn");
        btn.setTag(BT_SURE);
        btn.addTouchEventListener(btncallback);
        //取消按钮
        btn = csbNode.getChildByName("cancel_btn");
        btn.setTag(BT_CANCEL);
        btn.addTouchEventListener(btncallback);

        function checkEvent(sender, eventType) {
            self.onCheckBoxClickEvent(sender, eventType);
        }

        //修改登录密码
        this.m_checkLogin = csbNode.getChildByName("login_check");
        this.m_checkLogin.setTag(CBT_LOGIN);
        this.m_checkLogin.addEventListener(checkEvent);
        this.m_checkLogin.setSelected(true);
        //修改银行密码
        this.m_checkBank = csbNode.getChildByName("bank_check");
        this.m_checkBank.setTag(CBT_BANK);
        this.m_checkBank.addEventListener(checkEvent);
        this.m_checkBank.setSelected(false);
        //当前选择的box
        this.m_nSelectBox = CBT_LOGIN;

        //旧密码编辑
        var tmp = csbNode.getChildByName("sp_edit_old");
        var editbox = new cc.EditBox(cc.size(tmp.getContentSize().width - 10, tmp.getContentSize().height - 10), new cc.Scale9Sprite("blank.png"));
        // var editbox = new cc.EditBox(cc.size(tmp.getContentSize().width - 10, tmp.getContentSize().height - 10), "blank.png", UI_TEX_TYPE_PLIST);
        editbox.setPosition(tmp.getPosition());
        editbox.setFontName(res.round_body_ttf);
        editbox.setPlaceholderFontName(res.round_body_ttf);
        editbox.setFontSize(30);
        editbox.setPlaceholderFontSize(30);
        editbox.setMaxLength(26);
        editbox.setInputFlag(cc.EDITBOX_INPUT_FLAG_PASSWORD);
        editbox.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        editbox.setPlaceHolder("请输入您的旧登录密码");
        csbNode.addChild(editbox);
        this.m_editOldPass = editbox;
        //新密码编辑
        tmp = csbNode.getChildByName("sp_edit_new");
        editbox = new cc.EditBox(cc.size(tmp.getContentSize().width - 10, tmp.getContentSize().height - 10), new cc.Scale9Sprite("blank.png"));
        // editbox = new cc.EditBox(cc.size(tmp.getContentSize().width - 10, tmp.getContentSize().height - 10), "blank.png", UI_TEX_TYPE_PLIST);
        editbox.setPosition(tmp.getPosition());
        editbox.setFontName(res.round_body_ttf);
        editbox.setPlaceholderFontName(res.round_body_ttf);
        editbox.setFontSize(30);
        editbox.setPlaceholderFontSize(30);
        editbox.setMaxLength(26);
        editbox.setInputFlag(cc.EDITBOX_INPUT_FLAG_PASSWORD);
        editbox.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        editbox.setPlaceHolder("请输入您的新登录密码");
        csbNode.addChild(editbox);
        this.m_editNewPass = editbox;
        //确认密码编辑
        tmp = csbNode.getChildByName("sp_edit_confirm");
        editbox = new cc.EditBox(cc.size(tmp.getContentSize().width - 10, tmp.getContentSize().height - 10), new cc.Scale9Sprite("blank.png"));
        // editbox = new cc.EditBox(cc.size(tmp.getContentSize().width - 10, tmp.getContentSize().height - 10), "blank.png", UI_TEX_TYPE_PLIST);
        editbox.setPosition(tmp.getPosition());
        editbox.setFontName(res.round_body_ttf);
        editbox.setPlaceholderFontName(res.round_body_ttf);
        editbox.setFontSize(30);
        editbox.setPlaceholderFontSize(30);
        editbox.setMaxLength(26);
        editbox.setInputFlag(cc.EDITBOX_INPUT_FLAG_PASSWORD);
        editbox.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        editbox.setPlaceHolder("确认密码");
        csbNode.addChild(editbox);
        this.m_editConfirmPass = editbox;

        //网络回调
        var modifyCallBack = function (result, message) {
            self.onModifyCallBack(result, message);
        };
        //网络处理
        //todo
        // this._modifyFrame = ModifyFrame.create(this, modifyCallBack);
    },

    onButtonClickedEvent : function( tag, ref ) {
        if (BT_EXIT === tag) {
            this._scene.onKeyBack();
        }
        else if (BT_SURE === tag) {
            //todo following statement is error two parameter
            var res = this.confirmPasswd();
            var par = res[0];
            var tips = res[1];
            if (false === par) {
                showToast(this, tips, 2);
                return;
            }
            var oldpass = this.m_editOldPass.getText();
            var newpass = this.m_editNewPass.getText();
            if (CBT_LOGIN === this.m_nSelectBox) {
                if (GlobalUserItem.bVistor) {
                    showToast(this, "游客不能修改登录密码!", 2);
                    return;
                }
                if (GlobalUserItem.bWeChat) {
                    showToast(this, "游客不能修改登录密码!", 2);
                    return;
                }
                this._modifyFrame.onModifyLogonPass(oldpass, newpass);
            }
            else if (CBT_BANK === this.m_nSelectBox) {
                // 银行不同登陆
                if (newpass.toLowerCase() === GlobalUserItem.szPassword.toLowerCase()) {
                    showToast(this, "银行密码不能与登录密码一致!", 2);
                    return;
                }
                this._modifyFrame.onModifyBankPass(oldpass, newpass);
            }
        }
        else if (BT_CANCEL === tag) {
            this.clearEdit();
        }
    },

    onCheckBoxClickEvent : function(sender, eventType) {
        if (null === sender) {
            return;
        }

        var tag = sender.getTag();
        if (this.m_nSelectBox === tag) {
            sender.setSelected(true);
            return;
        }
        this.m_nSelectBox = tag;

        var oldstr = "";
        var newstr = "";
        var oldTips = "";
        var newTips = "";
        var confirmTips = "";
        if (tag === CBT_LOGIN) {
            this.m_checkBank.setSelected(false);
            oldstr = "sp_login_pass.png";
            newstr = "sp_new_lgpass.png";
            oldTips = "输入您的旧登录密码";
            newTips = "输入您的新登录密码";
            confirmTips = "确认密码";
        }
        else if (tag === CBT_BANK) {
            this.m_checkLogin.setSelected(false);
            oldstr = "sp_old_bkpass.png";
            newstr = "sp_new_bkpass.png";
            oldTips = "输入您的旧银行密码";
            newTips = "输入您的新银行密码";
            confirmTips = "确认密码";
        }

        //刷新界面
        var frame = cc.spriteFrameCache.getSpriteFrame(oldstr);
        if (null != frame) {
            this.m_spOldPass.setSpriteFrame(frame);
        }
        frame = cc.spriteFrameCache.getSpriteFrame(newstr);
        if (null != frame) {
            this.m_spNewPass.setSpriteFrame(frame);
        }
        this.m_editOldPass.setPlaceHolder(oldTips);
        this.m_editNewPass.setPlaceHolder(newTips);
        this.m_editConfirmPass.setPlaceHolder(confirmTips);
        this.clearEdit();
    },

    onModifyCallBack : function( result, tips ) {
        if (typeof(tips) === "string" && "" != tips) {
            showToast(this, tips, 2);
        }

        if (-1 != result) {
            this.clearEdit();
        }
    },

    clearEdit : function(  ) {
        this.m_editOldPass.string = "";
        this.m_editNewPass.string = "";
        this.m_editConfirmPass.string = "";
    },

    confirmPasswd : function(  ) {
        var oldpass = this.m_editOldPass.string;
        var newpass = this.m_editNewPass.string;
        var confirm = this.m_editConfirmPass.string;

        var oldlen = oldpass.length;
        if (0 === oldlen) {
            return [false, "原始密码不能为空"];
        }

        if (oldlen > 26 || oldlen < 6) {
            return [false, "原始密码请输入6-26位字符"];
        }

        var md5old = md5(oldpass);
        if (this.m_nSelectBox === CBT_LOGIN) {
            if (oldpass != GlobalUserItem.szPassword) {
                return [false, "您输入的原登录密码有误"];
            }
        }

        var newlen = ExternalFun.stringLen(newpass);
        if (0 === newlen) {
            return [false, "新密码不能为空!"];
        }

        if (newlen > 26 || newlen < 6) {
            return [false, "新密码请输入6-26位字符"];
        }

        //空格
        if (newpass.search(" ") === -1) {
            return [false, "新密码不能输入空格字符,请重新输入"];
        }
        // var b, e = string.find(newpass, " ");
        // if (b != e) {
        //     return [false, "新密码不能输入空格字符,请重新输入"];
        // }

        //新旧密码
        if (oldpass === newpass) {
            return [false, "新密码与原始密码一致,请重新输入"];
        }

        //密码确认
        if (newpass != confirm) {
            return [false, "两次输入的密码不一致,请重新输入"];
        }

        // 首位为字母
        // if (1 != string.find(newpass, "%a")) {
        //    return [false, "密码首位必须为字母，请重新输入！"]
        // }

        // 与帐号不同
        if (newpass.toLowerCase() === GlobalUserItem.szAccount.toLowerCase()) {
            return [false, "密码不能与帐号相同，请重新输入！"];
        }

        return [true, ""];
    },

    onExit : function() {
        if (this._modifyFrame.isSocketServer()) {
            this._modifyFrame.onCloseSocket();
        }
    }
});
// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new ModifyPasswdLayer();
//         this.addChild(layer);
//     }
// });