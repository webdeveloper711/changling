/*
Author: Li
实物兑换界面
 */

var OrderRecordLayer = cc.Layer.extend({
    ctor : function(scene) {
        this._super(cc.color(0, 0, 0, 125));
        this._scene = scene;
        var self = this;

        // this.registerScriptHandler(function (eventType) {
        //     if (eventType === "enterTransitionFinish") {	// 进入场景而且过渡动画结束时候触发。
        //         self.onEnterTransitionFinish();
        //     }
        //     else if (eventType === "exitTransitionStart") {	// 退出场景而且开始过渡动画时候触发。
        //         self.onExitTransitionStart();
        //     }
        // });

        this._OrderRecordList = [];

        var frame = cc.spriteFrameCache.getSpriteFrame("sp_top_bg.png");
        if (null != frame) {
            var sp = cc.Sprite.createWithSpriteFrame(frame);
            sp.setPosition(yl.WIDTH / 2, yl.HEIGHT - 51);
            this.addChild(sp);
        }

        var sp1 = new cc.Sprite(res.title_orderrecord_png);
        sp1.setPosition(yl.WIDTH / 2, yl.HEIGHT - 51);
        this.addChild(sp1);
        frame = cc.spriteFrameCache.getSpriteFrame("sp_public_frame_0.png");
        if (null != frame) {
            var sp = new cc.Sprite(frame);
            sp.setPosition(yl.WIDTH / 2, 326);
            this.addChild(sp);
        }

        var sp2 = new cc.Sprite(res.frame_back_2_png);
        sp2.setPosition(yl.WIDTH / 2, 326);
        this.addChild(sp2);

        var btn1 = new ccui.Button(res.bt_return_0_png, res.bt_return_1_png);
        btn1.setPosition(75, yl.HEIGHT - 51);
        btn1.addTouchEventListener(function (ref, type) {
            if (type === ccui.Widget.TOUCH_ENDED)
                self._scene.onKeyBack();
        });
        this.addChild(btn1);


        //标题列
        var sp3 = new cc.Sprite(res.text_orderrecord_0_png);
        sp3.setPosition(118 + 80 + 10, 550);
        this.addChild(sp3);

        var sp4 = new cc.Sprite(res.text_orderrecord_1_png);
        sp4.setPosition(350 + 80 + 30, 550);
        this.addChild(sp4);

        var sp5 = new cc.Sprite(res.text_orderrecord_2_png);
        sp5.setPosition(585 + 80, 550);
        this.addChild(sp5);

        var sp6 = new cc.Sprite(res.text_orderrecord_3_png);
        sp6.setPosition(818 + 80 - 30, 550);
        this.addChild(sp6);

        var sp7 = new cc.Sprite(res.text_orderrecord_4_png);
        sp7.setPosition(1050 + 80 - 20, 550);
        this.addChild(sp7);


        //无记录提示
        this._nullTipLabel = new cc.LabelTTF("没有兑换记录", res.round_body_ttf, 32);
        this._nullTipLabel.setPosition(yl.WIDTH / 2, 326);
        this._nullTipLabel.setVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
        this._nullTipLabel.setFontFillColor(cc.color(206, 175, 255, 255));
        this._nullTipLabel.setAnchorPoint(cc.p(0.5, 0.5));
        // .setVisible(false)
        this.addChild(this._nullTipLabel);

        //记录列表
        //todo all
        // this._listView = new cc.TableView(this, cc.size(1161, 454));
        // this._listView.setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL);
        // this._listView.setPosition(cc.p(90, 62));
        // this._listView.setDelegate();
        // this.addChild(this._listView);
        // this._listView.setVerticalFillOrder(cc.TABLEVIEW_FILL_TOPDOWN);
        // this._listView.registerScriptHandler(this.cellSizeForTable, cc.TABLECELL_SIZE_FOR_INDEX);
        // this._listView.registerScriptHandler(this.tableCellAtIndex, cc.TABLECELL_SIZE_AT_INDEX);
        // this._listView.registerScriptHandler(this.numberOfCellsInTableView, cc.NUMBER_OF_CELLS_IN_TABLEVIEW);

        var sp8 = new cc.Sprite(res.frame_back_1_png);
        sp8.setPosition(yl.WIDTH / 2, 326);
        this.addChild(sp8);

    },
    // 进入场景而且过渡动画结束时候触发。
    onEnterTransitionFinish : function() {
        var self = this;
        this._scene.showPopWait();
        //todo following will be needed
        appdf.onHttpJsionTable(yl.HTTP_URL + "/WS/MobileInterface.ashx","GET","action=getorderrecord&userid="+GlobalUserItem.dwUserID+"&signature="+GlobalUserItem.getSignature((new Date()).getTime())+"&time="+(new Date()).getTime()+"&number=20&page=1",function(jstable,jsdata) {
            this._scene.dismissPopWait();
            cc.log(jstable + "jstable");
            if (typeof(jstable) === "array") {
                var code = jstable["code"];
                if (Number(code) === 0) {
                    var datax = jstable["data"];
                    if (typeof(datax) === "array") {
                        var valid = datax["valid"];
                        if (valid === true) {
                            var listcount = datax["total"];
                            var list = datax["list"];
                            if (typeof(list) === "array") {
                                for (i = 0; i < list.length; i++) {
                                    var item = {};
                                    item.tradeType = list[i]["OrderStatusDescription"];
                                    item.name = list[i]["AwardName"];
                                    item.price = Number(list[i]["TotalAmount"]);
                                    item.count = Number(list[i]["AwardCount"]);
                                    item.date = GlobalUserItem.getDateNumber(list[i]["BuyDate"]);
                                    self._OrderRecordList.push(item);
                                }
                            }
                        }
                    }
                }

                self.onUpdateShow();
            }
            else
                showToast(self, "抱歉，获取订单记录信息失败！", 2, cc.color(250, 0, 0));
        });
        return this;
    },
    // 退出场景而且开始过渡动画时候触发。
    onExitTransitionStart : function() {
        return this;
    },

    onUpdateShow : function(){
        if (0 === this._OrderRecordList.length)
            this._nullTipLabel.setVisible(true);
        else
            this._nullTipLabel.setVisible(false);
        this._listView.reloadData();
    },
    ////////////////////////////////////////////////////////////////////-
    //子视图大小
    cellSizeForTable : function(view, idx) {
        return [1161, 76];
    },
    //子视图数目
    numberOfCellsInTableView : function(view) {
        return view.getParent()._OrderRecordList.length;
    },
    //获取子视图
    tableCellAtIndex : function(view, idx) {

        var cell = view.dequeueCell();

        var item = view.getParent()._OrderRecordList[idx + 1];

        var width = 1161;
        var height = 76;

        if (!cell) {
            cell = cc.TableViewCell.new();

            cell.addChild(new cc.Sprite("BankRecord/table_bankrecord_cell_" + (idx % 2) + ".png"));
            cell.setPosition(width / 2, height / 2);

            //日期
            var date = new Date();
            // var date = os.date("%Y-%m-%d %H.%M", Number(item.date) / 1000);
            cell.addChild(cc.LabelTTF.create(date, res.round_body_ttf, 28));
            cell.setPosition(118, height / 2);
            cell.setVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
            cell.setTextColor(cc.color(206, 175, 255, 255));
            cell.setAnchorPoint(cc.p(0.5, 0.5));

            cell.addChild(cc.LabelTTF.create(item.name, res.round_body_ttf, 28));
            cell.setPosition(350 + 20, height / 2);
            cell.setVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
            cell.setTextColor(cc.color(206, 175, 255, 255));
            cell.setAnchorPoint(cc.p(0.5, 0.5));


            cell.addChild(cc.LabelTTF.create(string_formatNumberThousands(item.count, true, ","), res.round_body_ttf, 28));
            cell.setPosition(585 - 10, height / 2);
            cell.setVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
            cell.setTextColor(cc.color(206, 175, 255, 255));
            cell.setAnchorPoint(cc.p(0.5, 0.5));


            cell.addChild(cc.LabelTTF.create(string_formatNumberThousands(item.price, true, ",") + "元宝", res.round_body_ttf, 28));
            cell.setPosition(818 - 40, height / 2);
            cell.setVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
            cell.setTextColor(cc.color(206, 175, 255, 255));
            cell.setAnchorPoint(cc.p(0.5, 0.5));


            cell.addChild(cc.LabelTTF.create(item.tradeType, res.round_body_ttf, 28));
            cell.setPosition(1050 - 30, height / 2);
            cell.setVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
            cell.setTextColor(cc.color(206, 175, 255, 255));
            cell.setAnchorPoint(cc.p(0.5, 0.5));
        }

        return cell;
    }
});

// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new OrderRecordLayer();
//         this.addChild(layer);
//     }
// });