var RankingListLayer = cc.Layer.extend({
    //继续游戏
    BT_CONTINUE: 101,
    DIKUANG: 1,
    TUBIAO: 2,
    MINGCI: 3,
    NICKNAME: 5,
    GOLD: 6,
    SHUZI: 7,
    WANZI: 8,
    ctor: function (scene, preTag) {
        this._super(cc.color(0, 0, 0, 125));
        var backLayer = new cc.LayerColor(cc.color(0,0,0,125));
        backLayer.setContentSize(appdf.WIDTH, appdf.HEIGHT);
        this.addChild(backLayer);
        cc.log("RankingListLayer created!!!");
        cc.log("preTag: " + preTag);
        var self = this;
        this.m_bRequestData = false;
        this._scene = scene;
        //上一个页面
        this.m_preTag = preTag;
        this.m_userInfo = [];

        // this.registerScriptHandler(function (eventType) {
        //     if (eventType === "enterTransitionFinish") {	// 进入场景而且过渡动画结束时候触发。
        //         self.onEnterTransitionFinish();
        //     }
        //     else if (eventType === "exitTransitionStart") {	// 退出场景而且开始过渡动画时候触发。
        //         self.onExitTransitionStart();
        //     }
        // });

        this._myRank = GlobalUserItem.tabRankCache.rankMyInfo || {
            name: GlobalUserItem.szNickName,
            lScore: "0",
            rank: "0"
        };
        cc.log("myRank : "+ this._myRank + "nickname : " + this._myRank.name );
        this._rankList = GlobalUserItem.tabRankCache.rankList || [];

        //背景
        var sp1 = new cc.Sprite(res.background_912_png);
        sp1.setPosition(667, yl.HEIGHT - 380);
        this.addChild(sp1);
        var sp2 = new cc.Sprite(res.paihang_ground_png);
        sp2.setPosition(667, yl.HEIGHT - 456);
        this.addChild(sp2);
        //自己底框
        var sp3 = new cc.Sprite(res.mythis_dikuang_png);
        sp3.setPosition(667, 560);
        this.addChild(sp3);

        var btn1 = new ccui.Button(res.bt_return_0_rank_png, res.bt_return_1_rank_png);
        btn1.setPosition(1244, yl.HEIGHT - 83);
        btn1.addTouchEventListener(function (ref, type) {
            if (type === ccui.Widget.TOUCH_ENDED) {
                self._scene.onKeyBack();
            }
        });
        this.addChild(btn1);

        var y = 542;
        var head = ((new PopupInfoHead()).createNormal(GlobalUserItem, 71))[0];
        cc.log("Head:"+head);
        head.setPosition(220, 562);
        this.addChild(head);
        head.setIsGamePop(false);
        //根据会员等级确定头像框
        head.enableHeadFrame(false);
        // head:enableInfoPop(true, cc.p(420, 120), cc.p(0, 1))
        this._myFace = head;

        // var testen = cc.Label.createWithSystemFont("A","Arial", 24);
        // this._enSize = testen.getContentSize().width;
        // var testcn = cc.Label.createWithSystemFont("游","Arial", 24);
        // this._cnSize = testcn.getContentSize().width;

        var sp4 = new cc.Sprite(res.dikuang7_png);
        sp4.setPosition(220, 560);
        this.addChild(sp4);
        //自己Id
        /*this._myID = cc.LabelTTF(appdf.stringEllipsis(GlobalUserItem.dwGameID, this._enSize, this._cnSize, 165), res.round_body_ttf, 21);*/

        this._myID = new cc.LabelTTF(GlobalUserItem.dwGameID, "Arial", 24, 165);
        this._myID.setPosition(315, y + 33);
        this._myID.setVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
        this._myID.setAnchorPoint(cc.p(0, 0.5));
        this._myID.setColor(cc.color(108, 53, 21));
        this.addChild(this._myID);
        var sp5 = new cc.Sprite(res.biaoti3_png);
        sp5.setPosition(835, y + 16);
        this.addChild(sp5);

        this._myRankFlag = new cc.Sprite(res.tubiao8_png);
        this._myRankFlag.setPosition(310, y);
        this.addChild(this._myRankFlag);


        var str2 = "/";
        this._myScore = new cc.LabelAtlas("0".replace("[.]", "/"), res.shuzi3_png, 18, 25, str2);
        this._myScore.setPosition(840, y + 15);
        this._myScore.setAnchorPoint(cc.p(0.5, 0.5));
        this.addChild(this._myScore);

       //游戏列表
        this._listView = new cc.TableView(this, cc.size(1147, 410) );
        this._listView.setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL);
        this._listView.setPosition(cc.p(95, 90));
        this._listView.setDelegate(this);
        this.addChild(this._listView);
        this._listView.setVerticalFillOrder(cc.TABLEVIEW_FILL_TOPDOWN);
        /*
        this._listView.registerScriptHandler(this.cellSizeForTable, cc.TABLECELL_SIZE_FOR_INDEX);
        this._listView.registerScriptHandler(this.tableCellAtIndex, cc.TABLECELL_SIZE_AT_INDEX);
        this._listView.registerScriptHandler(this.tableCellTouched, cc.TABLECELL_TOUCHED);
        this._listView.registerScriptHandler(this.numberOfCellsInTableView, cc.NUMBER_OF_CELLS_IN_TABLEVIEW);*/
        //TODO: to use onEntertransitionFinish() i added new line31 like follow
        this.onEnterTransitionFinish();

    },

    // 进入场景而且过渡动画结束时候触发。
    onEnterTransitionFinish : function() {
        cc.log("onEnterTransitionFinish called!!!");
        var self = this;
        if (0 == this._rankList.length) {
            this.m_bRequestData = true;
            this._scene.showPopWait();
            cc.log("rankList:"+this._rankList);
            appdf.onHttpJsonTable(yl.HTTP_URL + "/WS/PhoneRank.ashx", "GET", "action=getscorerank&pageindex=1&pagesize=10&userid=" + GlobalUserItem.dwUserID, function (jstable, jsdata) {
                self.m_bRequestData = false;
                self._scene.dismissPopWait();
                if (typeof(jstable) == "object") {
                    for (i = 0; i < jstable.length; i++) {
                        if (i == 0) { cc.log("this is array");
                            self._myRank.szNickName = jstable[i].NickName;
                            self._myRank.lScore = jstable[i].Score;
                            self._myRank.rank = jstable[i].Rank + "";
                            self._myRank.lv = jstable[i].Experience;
                        }
                        else {
                            var item = {};
                            item.szNickName = jstable[i].NickName;
                            item.lScore = jstable[i].Score + "";
                            item.wFaceID = Number(jstable[i].FaceID);
                            item.lv = jstable[i].Experience;
                            item.cbMemberOrder = Number(jstable[i].MemberOrder);
                            item.dBeans = Number(jstable[i].Currency);
                            item.lIngot = Number(jstable[i].UserMedal);
                            item.dwGameID = Number(jstable[i].GameID);
                            item.dwUserID = Number(jstable[i].UserID);
                            item.szSign = jstable[i].szSign || "此人很懒，没有签名";
                            item.szIpAddress = jstable[i].ip;
                            self._rankList.push(item);
                        }
                    }
                    GlobalUserItem.tabRankCache.rankMyInfo = self._myRank;
                    GlobalUserItem.tabRankCache.rankList = self._rankList;
                    self.onUpdateShow();
                }
                else {
                    cc.log("this is not array");
                    showToast(self, "抱歉，获取排行榜信息失败！", 2, cc.color(250, 0, 0));
                }

            });
        }
        else
            this.onUpdateShow();

        return this;
    },

    // 退出场景而且开始过渡动画时候触发。
    onExitTransitionStart : function() {
        return this;
    },

    onUpdateShow : function() {

        var rank = Number(this._myRank.rank);
        if (rank > 0 && rank < 21){
            this._myRankFlag.setTexture(res.tubiao9_png);
        } else{
            this._myRankFlag.setTexture(res.tubiao8_png);
        }

        var str = (this._myRank.lScore + "").replace("[.]", "/");
        this._myScore.setString(str);

        this._listView.reloadData();
    },

    onKeyBack : function() {
        return this.m_bRequestData;
    },

    ////////////////////////////////////////////////////////////////////-

    //子视图大小
    cellSizeForTable : function(view, idx) {
        return [1147, 70];
    },

    tableCellSizeForIndex : function(view, idx) {
        return {width : 1147, height: 70};
    },

    scrollViewDidScroll : function () {
      return false;
    },

    //子视图数目
    numberOfCellsInTableView : function(view) {
        return this._rankList.length;
    },

    //获取子视图
    tableCellAtIndex : function(view, idx) {
        //TODO: Can't use view.getParent and used this instead view.getParent. check later
        var cell = view.dequeueCell();
        // var item = view.getParent()._rankList[idx + 1];
        var item = this._rankList[idx];

        if (cell == null || cell == undefined) {
            var cy = 35;
            cell = new cc.TableViewCell();

            var sp = new cc.Sprite(res.dikuang5_rank_png);
            cell.addChild(sp);
            sp.setPosition(1147 / 2, cy);
            sp.setTag(this.DIKUANG);

            sp = new cc.Sprite(res.tubiao1_png);
            cell.addChild(sp);
            sp.setPosition(100, cy);
            sp.setTag(this.TUBIAO);

            //名次数字
            var str2 = "0";
            // var la = new cc.LabelAtlas("10", res.shuzi2_png, 17, 20, str2.charCodeAt());
            var la = new cc.LabelAtlas("10", res.shuzi2_png, 17, 20, str2);
            cell.addChild(la);
            la.setPosition(100, cy);
            la.setTag(this.MINGCI);
            la.setAnchorPoint(cc.p(0.5, 0.5));

            sp = new cc.Sprite(res.tubiao7_png);
            cell.addChild(sp);
            sp.setPosition(260, cy);

            sp = new cc.Sprite(res.biaoti2_rank_png);
            cell.addChild(sp);
            sp.setPosition(310, cy);

            // la = new cc.LabelAtlas(GlobalUserItem.wCurrLevelID + "", res.shuzi1_png, 18, 22, str2.charCodeAt());
            la = new cc.LabelAtlas(GlobalUserItem.wCurrLevelID + "", res.shuzi1_png, 18, 22, str2);
            cell.addChild(la);
            la.setPosition(335, cy);
            la.setTag(this.SHUZI);
            la.setAnchorPoint(cc.p(0, 0.5));

            //昵称
            var lt = new cc.LabelTTF("游戏玩家", res.round_body_ttf, 21);

            lt.setPosition(390, cy);
            lt.setTag(this.NICKNAME);
            //			.setVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER)
            lt.setAnchorPoint(cc.p(0, 0.5));
            lt.setColor(cc.color(108, 53, 21));
            cell.addChild(lt);
            //添加好友
            var btn = new ccui.Button(res.btn_join_912_1_png, res.btn_join_912_2_png);
            btn.setPosition(1010, cy);
            cc.log("idx : " + idx);
            btn.setName("btn_add_friend");
            cell.addChild(btn);

            var self = this;
            function btnEvent(sender, eventType) {
                if (eventType == ccui.Widget.TOUCH_ENDED) {
                    var tag = sender.getTag();
                    var addFriendTab = {};
                    addFriendTab.dwUserID = GlobalUserItem.dwUserID;
                    var friendTab = self._rankList[sender.getTag()].dwUserID;
                    addFriendTab.dwFriendID = friendTab || -1;
                    addFriendTab.cbGroupID = 0;

                    function sendResult(isAction) {
                        sender.setEnabled(!isAction);
                        var tmp = self;
                        if (!isAction) {
                            sender.setOpacity(255);//设置不透明度
                            showToast(tmp, "添加好友失败", 5);
                        }
                        else {
                            sender.setOpacity(125);
                            showToast(tmp, "添加好友成功", 5);
                        }
                    }

                    //添加好友
                    FriendMgr.getInstance().sendAddFriend(addFriendTab, sendResult);
                }
            }

            btn.addTouchEventListener(btnEvent);

            //金币标识
            sp = new cc.Sprite(res.biaoti3_png);
            cell.addChild(sp);
            sp.setPosition(740, cy);

            //金币
            str2 = "/";
            // la = new cc.LabelAtlas("0".replace("[.]", "/"), res.shuzi3_png, 18, 25, str2.charCodeAt());
            la = new cc.LabelAtlas("0".replace("[.]", "/"), res.shuzi3_png, 18, 25, str2);
            cell.addChild(la);
            la.setPosition(760, cy - 2);
            la.setTag(this.GOLD);
            la.setAnchorPoint(cc.p(0.5, 0.5));

        }
        if (cell.getChildByName("cell_face")) {
            cell.getChildByName("cell_face").updateHead(item);
        } else {
            //头像
            var head = (new PopupInfoHead()).createNormal(item, 41)[0];
            head.setPosition(180, 35);
            head.setIsGamePop(false);
            head.enableHeadFrame(false);
            cell.addChild(head);
            head.setName("cell_face");

            var sp1 = new cc.Sprite(res.dikuang8_png);
            cell.addChild(sp1);
            sp1.setPosition(180, 32);
        }

        var rankidx = (idx + 1) + "";
        // if (rankidx !== view.getParent()._myRank.rank){
        if (rankidx !== this._myRank.rank){
            cell.getChildByTag(this.DIKUANG).setTexture(res.dikuang5_rank_png);
        } else {
            cell.getChildByTag(this.DIKUANG).setTexture(res.dikuang4_png);
        }

        if (idx == 0) {
            cell.getChildByTag(this.TUBIAO).setTexture(res.tubiao1_png);
            cell.getChildByTag(this.TUBIAO).setVisible(true);
            cell.getChildByTag(this.MINGCI).setVisible(false);
        } else if (idx == 1) {
            cell.getChildByTag(this.TUBIAO).setTexture(res.tubiao2_png);
            cell.getChildByTag(this.TUBIAO).setVisible(true);
            cell.getChildByTag(this.MINGCI).setVisible(false);
        } else if (idx == 2) {
            cell.getChildByTag(this.TUBIAO).setTexture(res.tubiao3_png);
            cell.getChildByTag(this.TUBIAO).setVisible(true);
            cell.getChildByTag(this.MINGCI).setVisible(false);
        } else {
            cell.getChildByTag(this.TUBIAO).setVisible(false);
            cell.getChildByTag(this.MINGCI).setString((idx + 1) + "");
            cell.getChildByTag(this.MINGCI).setVisible(true);
        }
        cell.getChildByTag(this.SHUZI).setString(item.lv);
        cell.getChildByTag(this.NICKNAME).setString(item.szNickName);
        var str = (item.lScore + "").replace("[.]", "/");
        cell.getChildByTag(this.GOLD).string = str;
        cell.getChildByName("btn_add_friend").setTag(idx);
        return cell;
    },

    refresh : function( useritem, popPos ,anr) {
        this.reSet();
        if (null == useritem)
            return;
        this.m_userInfo = useritem;

        var ipAdress = useritem.szIpAddress || "";
        if (null !== useritem.dwIpAddress) {
            if (0 == useritem.dwIpAddress) {
                useritem.dwIpAddress = ExternalFun.random_longip();
            }
            var ipTable = ExternalFun.long2ip(useritem.dwIpAddress);
            var r1 = ipTable.b;
            var r2 = ipTable.s;
            var r3 = ipTable.m;
            var r4 = ipTable.p;
            if (null == r1 || null == r2 || null == r3 || null == r4)
                useritem.szIpAddress = "";
            else
                useritem.szIpAddress = r1 + "." + r2 + "." + r3 + "." + r4;
            ipAdress = useritem.szIpAddress;
        }

        sign = (sign == "") && "此人很懒，没有签名" || sign;

        var bEnable = useritem.dwUserID !==GlobalUserItem.dwUserID;
        var visible = useritem.dwUserID !== GlobalUserItem.dwUserID;

        //自己签名
        if (useritem.dwGameID == GlobalUserItem.dwGameID) {
            sign = GlobalUserItem.szSign;
            ipAdress = GlobalUserItem.szIpAdress;
        }
        else
        // 请求玩家信息
            FriendMgr.getInstance().sendQueryUserLocation(useritem.dwUserID);
        this.m_spBgKuang.setPosition(650, 350);

        this.m_spBgKuang.stopAllActions();
        this.m_spBgKuang.runAction(this.m_actShowAct);


        //好友状态
        if (true == bEnable) {
            var friendinfo = FriendMgr.getInstance().getFriendInfoByID(useritem.dwUserID);
            var strfile = "";
            if (null !== friendinfo)
                bEnable = false;

            //已是好友
            if (this.m_bIsGame)
                strfile = "sp_friend_912.png";
            if ("" !== strfile)
                this.m_btnAddFriend.loadTextureNormal(strfile, UI_TEX_TYPE_PLIST);
        }

        this.m_btnAddFriend.setEnabled(bEnable);
        this.m_btnAddFriend.setVisible(visible);
        if (false == bEnable)
            this.m_btnAddFriend.setOpacity(125);
        else
            this.m_btnAddFriend.setOpacity(255);
    },

    tableCellTouched : function(view, cell) {

    },
// //todo: added by vs23
//     tableCellSizeForIndex : function(datasource, i){
//         var cellsize  =  {
//             width : 1147,
//             height : 100
//         };
//         return cellsize;
//     }
});

// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new RankingListLayer();
//         this.addChild(layer);
//     }
// });


