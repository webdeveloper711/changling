/*  Author  :    JIN */

var RoomListLayer =  cc.Layer.extend({
    BT_EXIT	: 1,
    BT_PRIROOM : 2,
    tabWordColor : [[142,56,4],[4,93,65],[90,9,134]],
    ctor: function (scene, isQuickStart) {
        this._super();
        this._scene = scene;
        var self = this;
        this.m_bIsQuickStart = isQuickStart || false;
/*
        var enterGame = this._scene.getEnterGameInfo();
        //缓存资源
        var modulestr = enterGame._KindName.replace(".", "/");
        var path = "game/" + modulestr + "res/roomlist/roomlist.plist";
        if (false === cc.SpriteFrame.isSpriteFramesWithFileLoaded(path)) {
            if (jsb.fileUtils.isFileExist(path)) {
                cc.spriteFrameCache.addSpriteFrames(path);
            }
        }
*/

        var btcallback = function (ref, type) {
            if (type === ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };

        this.m_fThree = yl.WIDTH / 4;

        this.m_tabRoomListInfo = [];
        for (k in GlobalUserItem.roomlist) {
            if (Number(GlobalUserItem.roomlist[k][1]) === GlobalUserItem.nCurGameKind) {
                var listinfo = GlobalUserItem.roomlist[k][2];
                if (typeof(listinfo) !== "array") {
                    break;
                }
                var normalList = [];
                for (k in listinfo) {
                    if (listinfo[k].wServerType !== yl.GAME_GENRE_PERSONAL) {
                        normalList.push(listinfo[k]);
                    }
                }
                this.m_tabRoomListInfo = normalList;
                break;
            }
        }

        this._scrollview = null;

        //列表数据
        var room_count = this.m_tabRoomListInfo;  //游戏数量
        var column = 3;     //列
        var line = Math.ceil(room_count / column);  //行

        var spacing = 40;      //间距
        var margin = 20;       //边距

        var cellsize = this.cellSizeForTable(); // 控件大小
        var Inner_width = cellsize.width * column + spacing * (column - 1) + margin * 2; //内容宽
        var Inner_height = cellsize.height * line + spacing * (line - 1) + margin * 2; //内容高
        var scroll_width = cellsize.width * column + spacing * (column - 1) + margin * 2; //视图宽
        var scroll_height = 600; //cellsize.height*line + spacing*(line-1) + margin*2 //视图高

        if (Inner_height < scroll_height) {
            Inner_height = scroll_height;
        }

        //背景
        var sp = new cc.Sprite(res.sp_RoomList_Bg_png);
        sp.setPosition(cc.p(yl.WIDTH / 2, yl.HEIGHT * 0.4));
        sp.setAnchorPoint(cc.p(0.5, 0.5));
        this.addChild(sp);
/*
        //游戏列表（多行）
        this._scrollview = new ccui.ScrollView();
        this._scrollview.setTouchEnabled(true);
        this._scrollview.setBounceEnabled(false); //这句必须要不然就不会滚动噢
        this._scrollview.setScrollBarEnabled(false);
        this._scrollview.setDirection(ccui.ScrollViewDir.vertical); //设置滚动的方向
        this._scrollview.setContentSize(cc.size(scroll_width, scroll_height)); //设置尺寸
        this._scrollview.setInnerContainerSize(cc.size(Inner_width, Inner_height));
        this._scrollview.setAnchorPoint(cc.p(0.5, 0.5));
        this._scrollview.setPosition(cc.p(yl.WIDTH / 2, yl.HEIGHT * 0.3));
        this.addChild(this._scrollview);
 */
        this.onUpdateShowList(Inner_width, Inner_height, spacing, margin, column, line);

        //区域设置
        this.setContentSize(yl.WIDTH, yl.HEIGHT);

        // this.registerScriptHandler(function (eventType) {
        //     if (eventType === "enterTransitionFinish") { 	// 进入场景而且过渡动画结束时候触发。
        //         self.onEnterTransitionFinish();
        //     } else if (eventType === "exitTransitionStart") { 	// 退出场景而且开始过渡动画时候触发。
        //         self.onExitTransitionStart()
        //     }
        // });

        if (true === this.m_bIsQuickStart) {
            this.stopAllActions();
            GlobalUserItem.nCurRoomIndex = 1;
            this.onStartGame();
        }


    },

    // 进入场景而且过渡动画结束时候触发。
    onEnterTransitionFinish : function() {
    //	this._listView:reloadData()
        return this;
    },
    // 退出场景而且开始过渡动画时候触发。
    onExitTransitionStart : function() {
        return this;
    },

    onSceneAniFinish : function() {
    },

    //更新当前显示
    onUpdateShowList : function(width, height,spacing,margin,column,line) {
        if (this.m_tabRoomListInfo !== null) {
            var count = this.m_tabRoomListInfo.length;

            if (line === 0) {
                line = 0;
            }
            if (column === 0) {
                column = 0
            }

            var cellsize = this.cellSizeForTable();
            var cell_width = null;
            var cell_height = null;

            this._showList = null;
            this._showList = {};

            for (var i = 0; i < count; i++) {
                if (ccui.ScrollViewBar.verticalAlign === this._scrollview.getDirection()) {
                    cell_width = Math.floor((i - 1) % column) * (cellsize.width + spacing) + cellsize.width / 2 + margin;
                    cell_height = Math.floor((i - 1) / column) * (cellsize.height + spacing) + cellsize.height / 2 + margin;
                } else {
                    cell_width = Math.floor((i - 1) / column) * (cellsize.width + spacing) + cellsize.width / 2 + margin;
                    cell_height = Math.floor((i - 1) % column) * (cellsize.height + spacing) + cellsize.height / 2 + margin;
                }

                var iRoomIndex = i;
                this._showList[iRoomIndex] = this.tableCellAtIndex(iRoomIndex);
                this._showList[iRoomIndex].setPosition(cell_width, height - cell_height);
                this._scrollview.addChild(this.showList[iRoomIndex]);
            }

        }
    },

    cellHightLight : function(view,cell)
    {

    },

    cellUnHightLight : function(view,cell) {

    },

    //子视图大小
    cellSizeForTable : function() {
        return cc.size(286, 177);
    },

    //子视图数目
    numberOfCellsInTableView : function(view) {
        return this.m_tabRoomListInfo.length;
    },

    tableCellTouched : function(view, cell) {
        var index = cell.getTag();
        var roomlistLayer = view.getParent();

        var roominfo = roomlistLayer.m_tabRoomListInfo[index];
        if (!roominfo) {
            return;
        }

        GlobalUserItem.nCurRoomIndex = roominfo._nRoomIndex;
        GlobalUserItem.bPrivateRoom = (roominfo.wServerType === yl.GAME_GENRE_PERSONAL);
        if (view.getParent()._scene.roomEnterCheck()) {
            view.getParent().onStartGame();
        }
    },


    //获取子视图
    tableCellAtIndex : function(idx) {
        var cell = null;
        if (this._showList !== null) {
            cell = this._showList[idx];
        }

        var iteminfo = this.m_tabRoomListInfo[idx];
        var wLv = (iteminfo === null && 0 || iteminfo.wServerLevel);
        var rule = (iteminfo === null && 0 || iteminfo.dwServerRule);
        wLv = (bit._and(yl.SR_ALLOW_AVERT_CHEAT_MODE, rule) !== 0) && 10 || iteminfo.wServerLevel;
        wLv = (wLv !== 0) && wLv || 1;
        var nImgType = 0;
        if (wLv === 0 || wLv > 3) {
            nImgType = 1;
        } else {
            nImgType = wLv;
        }

        //背景
        var filestr = "src/client/res/RoomList/icon_roomlist_frame_" + nImgType + ".png";
        var filestr_frame = new cc.Sprite(filestr);


        if (!cell) {
            cell = new ccui.Button(filestr, filestr);
            cell.setContentSize(this.cellSizeForTable());
            cell.setSwallowTouches(false);
            cell.setName(iteminfo.szServerName);
        } else {
            cell.setTexture(filestr);
        }
        cell.removeAllChildren();

        var sizeBgX = filestr_frame.getContentSize().width;
        var sizeBgY = filestr_frame.getContentSize().height;

        if (8 === wLv) {
            //比赛场单独处理
        } else {
            var wRoom = math_mod(wLv, 3);//bit:_and(wLv, 3)
            var szName = (iteminfo === null && "房间名称" || iteminfo.szServerName);
            var szCount = (iteminfo === null && "0" || (iteminfo.dwOnLineCount + ""));
            var szServerScore = (iteminfo === null && "0" || iteminfo.lCellScore);
            var enterGame = this._scene.getEnterGameInfo();

            //检查房间背景资源
            var modulestr = enterGame._KindName.replace(".", "/");
            var path = "src/game/" + modulestr + "res/roomlist/icon_roomlist_" + wRoom + ".png";
            var framename = enterGame._KindID + "_icon_roomlist_" + wRoom + ".png";
            var frame = cc.spriteFrameCache.getInstance().getSpriteFrame(framename);

            var sp = new Sprite("src/client/RoomList/sp_lowScore_" + nImgType + ".png");
            sp.setPosition(cc.p(sizeBgX * 0.33, sizeBgY * 0.5));
            sp.setAnchorPoint(cc.p(0.5, 0.5));
            this.addChild(cell);

            var lat = new cc.LabelAtlas._create(szServerScore, "src/client/RoomList/num_lowScore_" + nImgType + ".png", 24, 36, string.byte("0"));
            lat.setPosition(sizeBgX * 0.55, sizeBgY * 0.5).setAnchorPoint(cc.p(0, 0.5));
            cell.addChild(lat);

            //图在线人数
            var newsp = new cc.Sprite(res.sp_onLineCount_png);
            newsp.setPosition(cc.p(sizeBgX * 0.3, sizeBgY * 0.2));
            newsp.setAnchorPoint(cc.p(0.5, 0.5));
            cell.addChild(newsp);
            //数字在线人数
            var lab1 = new cc.LabelTTF("1000人", res.round_body_ttf, 24);
            lab1.setPosition(cc.p(sizeBgX * 0.4, sizeBgY * 0.2)).setTextColor(cc.color(255, 255, 255, 255));
            lab1.setAnchorPoint(cc.p(0, 0.5));
            cell.addChild(lab1);
            //准入
            var lab2 = new cc.LabelTTF("准入1000", res.round_body_ttf, 24);
            lab2.setPosition(cc.p(sizeBgX * 0.5, sizeBgY * 0.85));
            lab2.setTextColor(cc.color(RoomListLayer.tabWordColor[nImgType][1], RoomListLayer.tabWordColor[nImgType][2], RoomListLayer.tabWordColor[nImgType][3], 255));
            lab2.setAnchorPoint(cc.p(0.5, 0.5));
            cell.addChild(lab2);
        }


        var btCallBack = function (ref, type) {
            this.tableCellTouched(this._scrollview, ref);
        };

        cell.setVisible(true);
        cell.setTag(idx);
        cell.addTouchEventListener(btCallBack);

        return cell;
    },

    //显示等待
    showPopWait : function() {
        if (this._scene) {
            this._scene.showPopWait();
        }
    },

    //关闭等待
    dismissPopWait : function() {
        if (this._scene) {
            this._scene.dismissPopWait();
        }
    },


    onStartGame : function(index) {
        var iteminfo = GlobalUserItem.GetRoomInfo(index);
        if (iteminfo !== null) {
            this._scene.onStartGame(index);
        }
    },

    //按键监听
    onButtonClickedEvent : function(tag,sender) {
        if (tag === BT_EXIT) {
            this._scene.onKeyBack();
        } else if (tag === BT_PRIROOM) {
            this._scene.onChangeShowMode(PriRoom.LAYTAG.LAYER_ROOMLIST);
        }
    }

});

// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new RoomListLayer();
//         this.addChild(layer);
//     }
// });




