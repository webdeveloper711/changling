/*  Author  :   JIN */
//var ShopDetailFrame = appdf.req(appdf.CLIENT_SRC+"plaza.models.ShopDetailFrame");

var ShopDetailLayer = cc.Layer.extend({

    onEnterTransitionDidFinish: function(){
        this.onEnterTransitionFinish();
    },

    onExitTransitionDidStart: function(){
        this.onExitTransitionStart();
    },

    onExit : function(){
        if (this._shopDetailFrame.isSocketServer()) {
            this._shopDetailFrame.onCloseSocket();
        }
        if (null != this._shopDetailFrame._gameFrame) {
            this._shopDetailFrame._gameFrame._shotFrame = null;
            this._shopDetailFrame._gameFrame = null;
        }

        if (null != this.m_listener) {
            //todo
            // cc.director.getEventDispatcher().removeEventListener(this.m_listener);
            // this.m_listener = null;
        }
    },

    ctor : function(scene, gameFrame) {

        this._super(cc.color(0,0,0,125));
        var layerColor = new cc.LayerColor(cc.color(0,0,0,125));
        layerColor.setContentSize(appdf.WIDTH, appdf.HEIGHT);
        this.addChild(layerColor);

        var self = this;

        this._scene = scene;

        // this.registerScriptHandler(function (eventType) {
        //     if (eventType == "enterTransitionFinish") {	// 进入场景而且过渡动画结束时候触发。
        //         self.onEnterTransitionFinish();
        //     } else if (eventType == "exitTransitionStart") {	// 退出场景而且开始过渡动画时候触发。
        //         self.onExitTransitionStart();
        //     } else if (eventType == "exit"){
        //         if (self._shopDetailFrame.isSocketServer()) {
        //             self._shopDetailFrame.onCloseSocket();
        //         }
        //         if (null != self._shopDetailFrame._gameFrame) {
        //             self._shopDetailFrame._gameFrame._shotFrame = null;
        //             self._shopDetailFrame._gameFrame = null;
        //         }
        //
        //         if (null != self.m_listener) {
        //             cc.director.getEventDispatcher().removeEventListener(self.m_listener);
        //             self.m_listener = null;
        //         }
        //     }
        // });

        //按钮回调
        this._btcallback = function (ref, type) {
            if (type == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref)
            }
        };

        var cbtlistener = function (sender, eventType) {
            self.onSelectedEvent(sender.getTag(), sender, eventType);
        };

        //网络回调
        var shopDetailCallBack = function (result, message) {
            return self.onShopDetailCallBack(result, message);
        };

        //网络处理
        this._shopDetailFrame = new ShopDetailFrame(this, shopDetailCallBack);
        this._shopDetailFrame._gameFrame = gameFrame;
        if (null != gameFrame) {
            gameFrame._shotFrame = this._shopDetailFrame;
        }

        this._select = ShopDetailLayer.CBT_BEAN;
        this._item = GlobalUserItem.buyItem;
        this._buyNum = 0;
        this._toUse = 0;
        this._type = yl.CONSUME_TYPE_CASH;

        var sp = new cc.Sprite(res.frame_shop_0_png);
        sp.setPosition(yl.WIDTH / 2, yl.HEIGHT - 51);
        this.addChild(sp);

        var btn = new ccui.Button(res.bt_return_0_png, res.bt_return_1_png);
        btn.setPosition(75, yl.HEIGHT - 51);
        this.addChild(btn);
        btn.addTouchEventListener(function (ref, type) {
            if (type == ccui.Widget.TOUCH_ENDED) {
                self._scene.onKeyBack()
            }
        });

        sp = new cc.Sprite(res.frame_detail_0_png);
        sp.setPosition(840, 350);
        this.addChild(sp);

        sp = new cc.Sprite(res.frame_detail_1_png);
        sp.setPosition(210, 477);
        this.addChild(sp);

        frame = cc.spriteFrameCache.getSpriteFrame("icon_public_" + this._item.id + ".png");
        if (null != frame) {
            sp = new cc.Sprite(frame);
            this.addChild(sp);
            sp.setPosition(210, 477);
        }

        frame = cc.spriteFrameCache.getSpriteFrame("text_public_" + this._item.id + ".png");
        if (null != frame) {
            sp = new cc.Sprite(frame);
            this.addChild(sp);
            sp.setPosition(210, 367);
        }

        var y = 300;

        //游戏豆
        if (this._item.bean != 0) {
            var lbl = new cc.LabelTTF(string_formatNumberThousands(this._item.bean, true, ",") + "游戏豆", res.round_body_ttf, 24);
            lbl.setAnchorPoint(cc.p(0.0, 0.5));
            lbl.setPosition(144, y);
            lbl.setColor(cc.color(255, 255, 0, 255));
            this.addChild(lbl);
            checkbox = new ccui.CheckBox(res.cbt_detail_0_png, res.cbt_detail_0_png, res.cbt_detail_1_png, "", "");
            checkbox.setPosition(110, y);
            this.addChild(checkbox);
            checkbox.setSelected(true);
            checkbox.setTag(ShopDetailLayer.CBT_BEAN);
            checkbox.addEventListener(cbtlistener);

            y = y - 61;
        }

        //元宝
        if (this._item.ingot != 0) {
            lbl = new cc.LabelTTF(string_formatNumberThousands(this._item.ingot, true, ",") + "元宝", res.round_body_ttf, 24);
            lbl.setAnchorPoint(cc.p(0.0, 0.5));
            lbl.setPosition(144, y);
            lbl.setColor(cc.color(255, 153, 0, 255));
            this.addChild(lbl);
            var checkbox = new ccui.CheckBox(res.cbt_detail_0_png, res.cbt_detail_0_png, res.cbt_detail_1_png, "", "");
            checkbox.setPosition(110, y);
            this.addChild(checkbox);
            checkbox.setSelected(this._item.bean == 0).setTag(ShopDetailLayer.CBT_INGOT).addEventListener(cbtlistener);

            if (this._item.bean == 0) {
                this._select = ShopDetailLayer.CBT_INGOT;
            }

            y = y - 61;
        }

        //游戏币
        if (this._item.gold != 0) {
            lbl = new cc.LabelTTF(string_formatNumberThousands(this._item.gold, true, ",") + "游戏币", res.round_body_ttf, 24);
            lbl.setAnchorPoint(cc.p(0.0, 0.5));
            lbl.setPosition(144, y);
            lbl.setColor(cc.color(255, 204, 0, 255));
            this.addChild(lbl);
            var chkbox = new ccui.CheckBox(res.cbt_detail_0_png, res.cbt_detail_0_png, res.cbt_detail_1_png, "", "");
            chkbox.setPosition(110, y);
            this.addChild(chkbox);
            chkbox.setSelected(this._item.ingot == 0 && this._item.bean == 0);
            chkbox.setTag(ShopDetailLayer.CBT_GOLD);
            chkbox.addEventListener(cbtlistener);

            if (this._item.ingot == 0 && this._item.bean == 0) {
                this._select = ShopDetailLayer.CBT_GOLD;
            }

            y = y - 61;
        }


        sp = new cc.Sprite(res.text_detail_0_shop_png);
        sp.setAnchorPoint(cc.p(1.0, 0.5));
        sp.setPosition(660, 509);
        this.addChild(sp);
        for (var i = 1; i <= 4; i++) {
            sp = new cc.Sprite("src/client/res/Shop/Detail/text_detail_" + i + ".png");
            sp.setAnchorPoint(cc.p(1.0, 0.5));
            sp.setPosition(660, 439 - (46 * (i - 1)));
            this.addChild(sp);
        }

        this._priceTag1 = new cc.Sprite(res.text_detail_5_0_png);
        this._priceTag1.setAnchorPoint(cc.p(1.0, 0.5));
        this._priceTag1.setPosition(660, 439 - (46 * (5 - 1)));
        this.addChild(this._priceTag1);
        this._priceTag2 = new cc.Sprite(res.text_detail_6_0_png);
        this._priceTag2.setAnchorPoint(cc.p(1.0, 0.5));
        this._priceTag2.setPosition(1142, 439 - (46 * (1 - 1)));
        this.addChild(this._priceTag2);
        this._priceTag3 = new cc.Sprite(res.text_detail_6_0_png);
        this._priceTag3.setAnchorPoint(cc.p(1.0, 0.5));
        this._priceTag3.setPosition(1142, 439 - (46 * (3 - 1)));
        this.addChild(this._priceTag3);
        this._priceTag4 = new cc.Sprite(res.text_detail_7_0_png);
        this._priceTag4.setAnchorPoint(cc.p(1.0, 0.5));
        this._priceTag4.setPosition(1142, 439 - (46 * (4 - 1)));
        this.addChild(this._priceTag4);
        // 数量
        this._txtPrice = new cc.LabelAtlas(string_formatNumberThousands(GlobalUserItem.lUserScore, true, "/"), res.num_shop_0_png, 18, 27, "/");
        this._txtPrice.setPosition(1142, 439 - (46 * (5 - 1)));
        this._txtPrice.setAnchorPoint(cc.p(1.0, 0.5));
        this.addChild(this._txtPrice);

        this.onUpdatePrice();

        var btn1 = new ccui.Button(res.bt_detail_0_0_png, res.bt_detail_0_1_png);
        btn1.setPosition(674, 173);
        btn1.setTag(ShopDetailLayer.BT_BUY1);
        this.addChild(btn1);
        btn1.addTouchEventListener(this._btcallback);
        var btn2 = new ccui.Button(res.bt_detail_1_0_png, res.bt_detail_1_1_png);
        btn2.setPosition(999, 173);
        btn2.setTag(ShopDetailLayer.BT_BUY2);
        this.addChild(btn2);
        btn2.addTouchEventListener(this._btcallback);

        sp = new cc.Sprite(res.frame_detail_2_png);
        sp.setPosition(910, 509);
        this.addChild(sp);
        var btn3 = new ccui.Button(res.bt_detail_min_png, res.bt_detail_min_png);
        btn3.setPosition(689, 509);
        btn3.setTag(ShopDetailLayer.BT_MIN);
        this.addChild(btn3);
        btn3.addTouchEventListener(this._btcallback);
        var btn4 = new ccui.Button(res.bt_detail_add_png, res.bt_detail_add_png);
        btn4.setPosition(1120, 509);
        btn4.setTag(ShopDetailLayer.BT_ADD);
        this.addChild(btn4);
        btn4.addTouchEventListener(this._btcallback);

        var editHanlder = function (event, editbox) {
            this.onEditEvent(event, editbox);
        };

        // 编辑框
        var editbox = new cc.EditBox(cc.size(400, 48), new cc.Scale9Sprite(res.blank_png));
        // var editbox = new cc.EditBox(cc.size(400, 48), "blank.png", UI_TEX_TYPE_PLIST);
        editbox.setPosition(cc.p(910, 509));
        editbox.setFontName(res.round_body_ttf);
        editbox.setPlaceholderFontName(res.round_body_ttf);
        editbox.setFontSize(30);
        editbox.setColor(cc.color(250, 204, 38));
        editbox.setPlaceholderFontSize(30);
        editbox.setInputMode(cc.EDITBOX_INPUT_MODE_NUMERIC);
        editbox.setName("EDITBOX");
        editbox.setDelegate(this);
        this.addChild(editbox);
        editbox.setVisible(false);
        editbox.string = "1";
        editbox.placeHolder = "1";
        this.m_editNumber = editbox;
        btn = new ccui.Button(res.blank_png, res.blank_png, res.blank_png);
        // btn = new ccui.Button("blank.png", "blank.png", "blank.png", UI_TEX_TYPE_PLIST);
        btn.setScale9Enabled(true);
        btn.setContentSize(cc.size(400, 48));
        btn.setPosition(cc.p(910, 509));
        btn.setTag(ShopDetailLayer.BT_BLANK);
        btn.addTouchEventListener(this._btcallback);
        this.addChild(btn);
        this._editBoxBackButton = btn;


        this._txtNum = new cc.LabelAtlas("0", res.num_detail_0_png, 19, 25, ".");
        this._txtNum.setPosition(910, 509);
        this._txtNum.setAnchorPoint(cc.p(0.5, 0.5));
        this.addChild(this._txtNum);
        // 道具单价
        this._txtPrice1 = new cc.LabelAtlas("0", res.num_detail_0_png, 19, 25, ".");
        this._txtPrice1.setPosition(1000, 439); // 439-(46*(1-1))
        this._txtPrice1.setAnchorPoint(cc.p(1.0, 0.5));
        this.addChild(this._txtPrice1);

        var vip = GlobalUserItem.cbMemberOrder || 0;
        var bShowDiscount = vip != 0;
        this.m_discount = 100;
        if (vip != 0) {
            this.m_discount = GlobalUserItem.MemberList[vip]._shop;
        }
        // 折扣
        this._txtDiscount = new cc.LabelTTF(this.m_discount + "%折扣", res.round_body_ttf, 25);
        this._txtDiscount.setAnchorPoint(cc.p(1.0, 0.5));
        this._txtDiscount.setPosition(1142, 393); // 439-(46*(2-1))
        this._txtDiscount.setColor(cc.color(255, 0, 0, 255));
        this._txtDiscount.setVisible(bShowDiscount);
        this.addChild(this._txtDiscount);

        // 会员标识
        var sp_vip = new cc.Sprite(res.atlas_vipnumber_png);
        if (null != sp_vip) {
            sp_vip.setPosition(1142 - this._txtDiscount.getContentSize().width - 20, 393);
            this.addChild(sp_vip);
            sp_vip.setTextureRect(cc.rect(28 * vip, 0, 28, 26));
            sp_vip.setVisible(bShowDiscount);
        }

        // 折后价格
        this._txtPrice2 = new cc.LabelAtlas("0", res.num_detail_0_png, 19, 25, ".");
        this._txtPrice2.setPosition(1000, 347); // 439-(46*(3-1))
        this._txtPrice2.setAnchorPoint(cc.p(1.0, 0.5));
        this.addChild(this._txtPrice2);
        // 购买价格
        this._txtPrice3 = new cc.LabelAtlas("0", res.num_detail_0_png, 19, 25,".");
        this._txtPrice3.setPosition(1047, 301); // 439-(46*(4-1))
        this._txtPrice3.setAnchorPoint(cc.p(1.0, 0.5));
        this.addChild(this._txtPrice3);

        this._buyNum = 1;
        this.onUpdateNum();

        //功能描述
        lbl = new cc.LabelTTF("功能：" + this._item.description, res.round_body_ttf, 22);
        lbl.setAnchorPoint(cc.p(0.0, 0.5));
        lbl.setPosition(417, 70);
        lbl.setColor(cc.color(136, 164, 224, 255));
        this.addChild(lbl);

        // 通知监听
        this.m_listener = new cc.EventListener(yl.RY_USERINFO_NOTIFY, this.onUserInfoChange);
        // this.m_listener = new cc.EventListenerCustom(yl.RY_USERINFO_NOTIFY, handler(this, this.onUserInfoChange));
        //todo
        // cc.director.getEventDispatcher().addEventListenerWithSceneGraphPriority(this.m_listener, this);

    },


    // 进入场景而且过渡动画结束时候触发。
    onEnterTransitionFinish : function(){
        return this;
    },

    // 退出场景而且开始过渡动画时候触发。
    onExitTransitionStart : function() {
        return this;
    },

    onUserInfoChange : function( event  ) {
        cc.log("//////////userinfo change notify////////////");

        var msgWhat = event.obj;

        if (null != msgWhat && msgWhat == yl.RY_MSG_USERWEALTH) {
            //更新财富
            this.onUpdatePrice();
        }
    },

    //按键监听
    onButtonClickedEvent : function(tag,sender) {

        if (tag == ShopDetailLayer.BT_ADD) {
            this._txtNum.setVisible(true);
            this.m_editNumber.setVisible(false);
            this._editBoxBackButton.setVisible(true);
            this._buyNum = this._buyNum + 1;
            this.m_editNumber.setString(this._buyNum + "");
            this.onUpdateNum();
        } else if (tag == ShopDetailLayer.BT_MIN) {
            this._txtNum.setVisible(true);
            this.m_editNumber.setVisible(false);
            this._editBoxBackButton.setVisible(true);
            if (this._buyNum != 1) {
                this.m_editNumber.setString(this._buyNum + "");
                this._buyNum = this._buyNum - 1;
                this.onUpdateNum();
            }
        } else if (tag == ShopDetailLayer.BT_BUY1) {
            if (GlobalUserItem.cbInsureEnabled == 0 && this._type == yl.CONSUME_TYPE_GOLD) {
                showToast(this, "未设置银行密码，无法使用", 3);
                return;
            }
            this._toUse = 0;
            this._scene.showPopWait();
            this._shopDetailFrame.onPropertyBuy(this._type, this._buyNum, this._item.id, 0);
        } else if (tag == ShopDetailLayer.BT_BUY2) {
            if (GlobalUserItem.cbInsureEnabled == 0 && this._type == yl.CONSUME_TYPE_GOLD) {
                showToast(this, "未设置银行密码，无法使用", 3);
                return;
            }

            this._scene.showPopWait();
            //判断是否是消耗小喇叭
            if (this._item.id == yl.SMALL_TRUMPET) {
            } else {
                this._toUse = 1;
                this._shopDetailFrame.onPropertyBuy(this._type, this._buyNum, this._item.id, 1);
            }
        } else if (tag == ShopDetailLayer.BT_BLANK) {
            this._editBoxBackButton.setVisible(false);
            this._txtNum.setVisible(false);
            this.m_editNumber.setVisible(true);
            // this.m_editNumber.touchDownAction(this.m_editNumber, ccui.Widget.TOUCH_ENDED);
        }
    },

    onSelectedEvent : function(tag,sender,eventType) {

        if (this._select == tag) {
            this.getChildByTag(tag).setSelected(true);
            return;
        }

        this._select = tag;
        this.onUpdatePrice();
        this.onUpdateNum();

        for (var i = 1; i <= ShopDetailLayer.CBT_LOVELINESS; i++) {
            if (i != tag) {
                if (this.getChildByTag(i)) {
                    this.getChildByTag(i).setSelected(false);
                }
            }
        }
    },

    onUpdatePrice : function() {
        this._priceTag1.setTexture("src/client/res/Shop/Detail/text_detail_5_" + (this._select - 1) + ".png");
        this._priceTag2.setTexture("src/client/res/Shop/Detail/text_detail_6_" + (this._select - 1) + ".png");
        this._priceTag3.setTexture("src/client/res/Shop/Detail/text_detail_6_" + (this._select - 1) + ".png");
        this._priceTag4.setTexture("src/client/res/Shop/Detail/text_detail_7_" + (this._select - 1) + ".png");

        var priceStr = "";
        if (this._select == ShopDetailLayer.CBT_BEAN) {
            this._type = yl.CONSUME_TYPE_CASH;
            priceStr = string_formatNumberThousands(GlobalUserItem.dUserBeans, true, "/");
        } else if (this._select == ShopDetailLayer.CBT_INGOT) {
            this._type = yl.CONSUME_TYPE_USEER_MADEL;
            priceStr = string_formatNumberThousands(GlobalUserItem.lUserIngot, true, "/");
        } else if (this._select == ShopDetailLayer.CBT_GOLD) {
            this._type = yl.CONSUME_TYPE_GOLD;
            priceStr = string_formatNumberThousands(GlobalUserItem.lUserScore, true, "/");
        } else if (this._select == ShopDetailLayer.CBT_LOVELINESS) {
            this._type = yl.CONSUME_TYPE_LOVELINESS;
            priceStr = string_formatNumberThousands(GlobalUserItem.dwLoveLiness, true, "/");
        }

        this._txtPrice.setString(priceStr);
    },

    onUpdateNum : function() {
        this._txtNum.setString(string_formatNumberThousands(this._buyNum, true, "."));

        var itemPrice = 0;
        if (this._select == ShopDetailLayer.CBT_BEAN) {
            itemPrice = this._item.bean;
        } else if (this._select == ShopDetailLayer.CBT_INGOT) {
            itemPrice = this._item.ingot;
        } else if (this._select == ShopDetailLayer.CBT_GOLD) {
            itemPrice = this._item.gold;
        } else if (this._select == ShopDetailLayer.CBT_LOVELINESS) {
            itemPrice = this._item.loveliness;
        }

        this._txtPrice1.setString(string_formatNumberThousands(itemPrice, true, "."));
        this._txtPrice2.setString(string_formatNumberThousands(itemPrice * (this.m_discount * 0.01), true, "."));
        this._txtPrice3.setString(string_formatNumberThousands(itemPrice * this._buyNum * (this.m_discount * 0.01), true, "."));
    },

    //操作结果
    onShopDetailCallBack : function(result,message) {
        cc.log("======== ShopDetailLayer.onShopDetailCallBack ========");
        var bRes = false;
        this._scene.dismissPopWait();
        if (typeof(message) == "string" && message != "") {
            showToast(this, message, 2);
        }

        //大喇叭购买且消费
        if (result == yl.SUB_GP_PROPERTY_BUY_RESULT && 1 == this._toUse && message == yl.LARGE_TRUMPET) {
            this._toUse = 0;
            if (null != this._scene.getTrumpetSendLayer) {
                bRes = true;
                this._scene.getTrumpetSendLayer();
            }
        }
        return bRes;
    },

    editBoxEditingDidBegin: function (editbox) {
        // cc.log("editBox  DidBegin !");
    },

    editBoxEditingDidEnd: function (editbox) {
        var ndst = Number(editbox.string);
        if ("number" == typeof(ndst)) {
            this._buyNum = ndst;
        }
        editbox.setVisible(false);
        this._editBoxBackButton.setVisible(true);
        this._txtNum.setVisible(true);
        this.onUpdateNum();
        // this._txtNum.setVisible(false);
        // cc.log("editBox  DidEnd !");
    },

    editBoxTextChanged: function (editbox, text) {
        // cc.log("editBox  TextChanged");
    },

    editBoxReturn: function (editbox) {
        // cc.log("editBox  was returned !");
    },

    onEditEvent : function(event,editbox) {
        if (event == "began") {
            this._txtNum.setVisible(false);
        } else if (event == "return") {
            var ndst = Number(editbox.string);
            if ("number" == typeof(ndst)) {
                this._buyNum = ndst;
            }
            editbox.setVisible(false);
            this.onUpdateNum();
        }
    }

});


ShopDetailLayer.CBT_BEAN			= 1;
ShopDetailLayer.CBT_INGOT			= 2;
ShopDetailLayer.CBT_GOLD			= 3;
ShopDetailLayer.CBT_LOVELINESS		= 4;

ShopDetailLayer.BT_BUY1				= 20;
ShopDetailLayer.BT_BUY2				= 21;
ShopDetailLayer.BT_ADD				= 22;
ShopDetailLayer.BT_MIN				= 23;
ShopDetailLayer.BT_BLANK            = 24;
// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new ShopDetailLayer();
//         this.addChild(layer);
//     }
// });




