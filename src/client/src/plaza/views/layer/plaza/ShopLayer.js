/*  Author  :   JIN */

// var ExternalFun = appdf.req(appdf.EXTERNAL_SRC + "ExternalFun");
// var MultiPlatform = appdf.req(appdf.EXTERNAL_SRC + "MultiPlatform");
// var targetPlatform = cc.sys;
// var ClientConfig = appdf.req(appdf.BASE_SRC +"app.models.ClientConfig");

var CBT_WECHAT = 101;
var CBT_ALIPAY = 102;
var CBT_JFT = 103;

var PAYTYPE = [];
PAYTYPE[CBT_WECHAT] =
{
    str : "wx",
    plat : yl.ThirdParty.WECHAT
};
PAYTYPE[CBT_ALIPAY] =
{
    str : "zfb",
    plat : yl.ThirdParty.ALIPAY
};
PAYTYPE[CBT_JFT] =
{
    str : "jft",
    plat : yl.ThirdParty.JFT
};

//竣付通页面
var JunFuTongPay = cc.Layer.extend({
    JFT_RETURN : 1,
    ctor : function(parent, itemname, price, paylist, token) {
        this._super();
        var self = this;
        this.m_parent = parent;
        //this.m_parent.m_bJunfuTongPay = true;
        GlobalUserItem.bJftPay = true;
        this.m_token = token;

       // ExternalFun.registerTouchEvent(this, true);
        var btnpos =
            [
                [cc.p(0.5, 0.3)],
                [cc.p(0.35, 0.3), cc.p(0.65, 0.3)]
            ];

        //加载csb资源
        var rootLayer = (ExternalFun.loadRootCSB(res.JunFuTongPay_json, this)).rootlayer;
        var csbNode = (ExternalFun.loadRootCSB(res.JunFuTongPay_json, this)).csbnode;
        //背景
        var bg = csbNode.getChildByName("pay_bg");
        var bgsize = bg.getContentSize();

        itemname = itemname || "";
        price = price || 0;

        //商品名称
        bg.getChildByName("text_name").setString(itemname);

        //支付金额
        bg.getChildByName("text_price").setString(price);

        //按钮回调
        var btcallback = function (ref, tType) {
            if (tType == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };

        //返回按钮
        var btn = bg.getChildByName("btn_return");
        btn.setTag(this.JFT_RETURN);
        btn.addTouchEventListener(btcallback);

        //微信支付
        btn = bg.getChildByName("jft_pay3");
        btn.addTouchEventListener(btcallback);
        btn.setVisible(false);
        btn.setEnabled(false);

        //支付宝支付
        btn = bg.getChildByName("jft_pay4");
        btn.addTouchEventListener(btcallback);
        btn.setVisible(false);
        btn.setEnabled(false);

        var str = "";
        var tpos = btnpos[paylist.length] || [];
        for (k in paylist) {
            str = "jft_pay" + paylist[k];
            var paybtn = bg.getChildByName(str);
            if (null != paybtn) {
                paybtn.setEnabled(true);
                paybtn.setVisible(true);
                paybtn.setTag(paylist[k]);
                var pos = tpos[k];
                if (null != pos) {
                    paybtn.setPosition(cc.p(pos.x * bgsize.width, pos.y * bgsize.height));
                }
            }
        }

        //调起支付
        this.m_bCallPay = false;
        //监听
        var eventCall = function (event) {
            if (true == self.m_bCallPay) {
                self.queryUserScoreInfo();
            }
        };
        //this.m_listener = new cc.EventListenerCustom(yl.RY_JFTPAY_NOTIFY, handler(this, eventCall));
        //cc.director().getEventDispatcher().addEventListenerWithSceneGraphPriority(this.m_listener, this);
    },

    queryUserScoreInfo : function() {
        if (null != this.m_parent.queryUserScoreInfo) {
            var self = this;
            this.m_parent.queryUserScoreInfo(function (needUpdate) {
                if (true == needUpdate) {
                    self.m_parent.updateScoreInfo();
                    //重新请求支付列表
                    self.m_parent.reloadBeanList();
                }
                GlobalUserItem.bJftPay = false;
                this.removeFromParent();
            });
        }
    },

    onTouchBegan : function(touch, event) {
        return this.isVisible();
    },

    onExit : function() {
        if (null != this.m_listener) {
            cc.director().getEventDispatcher().removeEventListener(this.m_listener);
            this.m_listener = null;
        }
    },

    onButtonClickedEvent : function(tag, sender) {
        cc.log(tag);
        var self = this;
        if (tag == this.JFT_RETURN) {
            this.m_parent.m_bJunfuTongPay = false;
            GlobalUserItem.bJftPay = false;
            //重新请求支付列表
            this.m_parent.reloadBeanList();
            this.removeFromParent();
        } else {
            var plat = 0;
            var str = "";
            if (3 == tag) {
                plat = yl.ThirdParty.WECHAT;
                str = "微信未安装,无法进行微信支付";
            } else if (4 == tag) {
                plat = yl.ThirdParty.ALIPAY;
                str = "支付宝未安装,无法进行支付宝支付";
            }
            //判断应用是否安装
            if (false == MultiPlatform.getInstance().isPlatformInstalled(plat)) {
                showToast(this, str, 2, cc.color(250, 0, 0, 255));
                return;
            }
            this.m_parent.showPopWait();
            this.runAction(new cc.Sequence(new cc.DelayTime(5), cc.CallFunc(function () {
                self.m_parent.dismissPopWait();
            })));
            var payCallBack = function (param) {

            };
            this.m_bCallPay = true;
            MultiPlatform.getInstance().thirdPartyPay(PAYTYPE[CBT_JFT].plat, [paytype = tag, token = self.m_token], payCallBack);
        }
    }

});


//支付选择页面
var ShopPay = cc.Layer.extend({
    BT_CLOSE : 201,
    BT_SURE : 202,
    BT_CANCEL : 203,

    ctor : function(parent) {
        this._super();
        this.m_parent = parent;
        //价格
        this.m_fPrice = 0.00;
        //数量
        this.m_nCount = 0;
        // appid
        this.m_nAppId = 0;

        //注册触摸事件
        //ExternalFun.registerTouchEvent(this, true);

        //加载csb资源
        var rootLayer = (ExternalFun.loadRootCSB(res.ShopPayLayer_json, this)).rootlayer;
        var csbNode = (ExternalFun.loadRootCSB(res.ShopPayLayer_json, this)).csbnode;

        this.m_spBgKuang = csbNode.getChildByName("shop_pay_bg");
        this.m_spBgKuang.setScale(0.0001);
        var bg = this.m_spBgKuang;

        //商品
        this.m_textProName = bg.getChildByName("text_name");
        this.m_textProName.setString("");

        //价格
        this.m_textPrice = bg.getChildByName("text_price");
        this.m_textPrice.setString("");

        //按钮回调
        var btcallback = function (ref, type) {
            if(type == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };
        //关闭按钮
        var btn = bg.getChildByName("btn_close");
        btn.setTag(this.BT_CLOSE);
        btn.addTouchEventListener(btcallback);

        //确定按钮
        btn = bg.getChildByName("btn_sure");
        btn.setTag(this.BT_SURE);
        btn.addTouchEventListener(btcallback);

        //取消按钮
        btn = bg.getChildByName("btn_cancel");
        btn.setTag(this.BT_CANCEL);
        btn.addTouchEventListener(btcallback);

        var cbtlistener = function (sender, eventType) {
            self.onSelectedEvent(sender.getTag(), sender);
        };

        var enableList = [];
        //微信支付
        var cbt = bg.getChildByName("check_wechat");
        cbt.setTag(this.CBT_WECHAT);
        cbt.setSelected(true);
        cbt.addEventListener(cbtlistener);
        cbt.setVisible(false);
        cbt.setEnabled(false);
        this.m_cbtWeChat = cbt;
        if(yl.WeChat.PartnerID != " " && yl.WeChat.PartnerID !=="") {
            enableList.push("check_wechat");
        }

        var wpos = cc.p(this.m_cbtWeChat.getPositionX(), this.m_cbtWeChat.getPositionY());
        var wtext = cbt.getChildByName("txt");
        wtext.setVisible(false);
        var wtpos = cc.p(wtext.getPositionX(), wtext.getPositionY());

        //支付宝支付
        cbt = bg.getChildByName("check_alipay");
        cbt.setTag(CBT_ALIPAY);
        cbt.setSelected(false);
        cbt.addEventListener(cbtlistener);
        cbt.setVisible(false);
        cbt.setEnabled(false);
        this.m_cbtAlipay = cbt;
        if (yl.AliPay.PartnerID != " " && yl.AliPay.PartnerID != "") {
            enableList.push("check_alipay");
        }
        var apos = cc.p(this.m_cbtAlipay.getPositionX(), this.m_cbtAlipay.getPositionY());
        var atext = cbt.getChildByName("txt");
        atext.setVisible(false);
        var atpos = cc.p(atext.getPositionX(), atext.getPositionY());

        //竣付通支付
        cbt = bg.getChildByName("check_jft");
        cbt.setTag(CBT_JFT);
        cbt.setSelected(false);
        cbt.addEventListener(cbtlistener);
        cbt.setVisible(false);
        cbt.setEnabled(false);
        this.m_cbtJft = cbt;
        if(yl.JFT.PartnerID != " " && yl.JFT.PartnerID != "") {
            table.insert(enableList, "check_jft");
        }
        var jpos = cc.p(this.m_cbtJft.getPositionX(), this.m_cbtJft.getPositionY());
        var jtext = cbt.getChildByName("txt");
        jtext.setVisible(false);
        var jtpos = cc.p(jtext.getPositionX(), jtext.getPositionY());

        var cbtPosition = [
            [wpos],
            [wpos, apos],
            [wpos, apos, jpos]
        ];
        var textPosition =
            [
                [wtpos],
                [wtpos, atpos],
                [wtpos, atpos, jtpos]
            ];
        var poslist = cbtPosition[enableList.length];
        var tposlist = textPosition[enableList.length];

        for (k in enableList) {
            var tmp = bg.getChildByName(enableList[k]);
            if (null != tmp) {
                tmp.setEnabled(true);
                tmp.setVisible(true);

                var pos1 = poslist[k];
                if (null != pos1) {
                    tmp.setPosition(pos1);
                }
            }
            tmp = bg.getChildByName(enableList[k] + "_t");
            if (null != tmp) {
                tmp.setVisible(true);
                var pos2 = tposlist[k];
                if (null != pos2) {
                    tmp.setPosition(pos2);
                }
            }
        }

        this.m_select = null;
        if (enableList.length > 0) {
            var tmp1 = bg.getChildByName(enableList[1]);
            if (null != tmp1) {
                tmp1.setSelected(true);
                this.m_select = tmp1.getTag();
            }
        }

        //加载动画
        this.m_actShowAct = cc.ScaleTo(0.2, 1.0);
        ExternalFun.SAFE_RETAIN(this.m_actShowAct);

        var scale = new cc.ScaleTo(0.2, 0.0001);
        var call = new cc.CallFunc(function () {
            self.showLayer(false);
        });
        this.m_actHideAct = cc.Sequence(scale, call);
        ExternalFun.SAFE_RETAIN(this.m_actHideAct);

        this.showLayer(false);

    },

    isPayMethodValid : function() {
        if ((yl.WeChat.PartnerID != " " && yl.WeChat.PartnerID != "")
            || (yl.AliPay.PartnerID != " " && yl.AliPay.PartnerID != "")
            || (yl.JFT.PartnerID != " " && yl.JFT.PartnerID != "")
        ) {
            return true;
        } else {
            return false;
        }
    },

    showLayer : function(va) {
        this.setVisible(va);

        if (true == va) {
            this.m_spBgKuang.stopAllActions();
            this.m_spBgKuang.runAction(this.m_actShowAct);
        }
    },

    refresh : function(count, name, sprice, fprice, appid) {
        this.m_textProName.setString(name);
        this.m_textPrice.setString(sprice);

        this.m_fPrice = fprice;
        this.m_nCount = count;
        this.m_nAppId = appid;
    },

    onButtonClickedEvent : function(tag, sender) {
        if (tag == BT_CLOSE || tag == BT_CANCEL) {
            this.hide();
        } else if (tag == BT_SURE) {
            if (null == this.m_select) {
                return;
            }

            var str = "无法支付";
            var plat = PAYTYPE[this.m_select].plat;
            if (yl.ThirdParty.WECHAT == PAYTYPE[this.m_select].plat) {
                plat = yl.ThirdParty.WECHAT;
                str = "微信未安装,无法进行微信支付";
            } else if (yl.ThirdParty.ALIPAY == PAYTYPE[this.m_select].plat) {
                plat = yl.ThirdParty.ALIPAY;
                str = "支付宝未安装,无法进行支付宝支付";
            }
            //判断应用是否安装
            if (false == MultiPlatform.getInstance().isPlatformInstalled(plat) && plat != yl.ThirdParty.JFT) {
                showToast(this, str, 2, cc.color(250, 0, 0, 255));
                return;
            }

            this.m_parent.showPopWait();
            this.runAction(cc.Sequence(cc.DelayTime.create(5), cc.CallFunc.create(function () {
                this.m_parent.dismissPopWait();
            })));
            var self = this;
            //生成订单
            var url = yl.HTTP_URL + "/WS/MobileInterface.ashx";
            var account = GlobalUserItem.dwGameID;

            var action = "action=CreatPayOrderID&gameid=" + account + "&amount=" + this.m_fPrice + "&paytype=" + PAYTYPE[this.m_select].str + "&appid=" + this.m_nAppId;
            //cc.log(action)
            appdf.onHttpJsonTable(url, "GET", action, function (jstable, jsdata) {
                if (jstable != null) {
                    var data = jstable["data"];
                    if (data!= null) {
                        if (null != data["valid"] && true == data["valid"]) {
                            var payparam = [];
                            if (self.m_select == self.CBT_WECHAT) { //微信支付
                                //获取微信支付订单id
                                var paypackage = data["PayPackage"];
                                if (typeof(paypackage) == "string") {
                                    try{
                                        paypackagetable = cjson.decode(paypackage);
                                        var payid = paypackagetable["prepayid"];
                                        if (null == payid) {
                                            showToast(self, "微信支付订单获取异常", 2);
                                            return;
                                        }
                                        payparam["info"] = paypackagetable;

                                    }catch (e){
                                        showToast(self, "微信支付订单获取异常", 2);
                                        return;
                                    }
                                }
                            } else if (self.m_select == self.CBT_JFT) { //竣付通支付
                                self.onJunFuTongPay(data);
                                return;
                            }
                            //订单id
                            payparam["orderid"] = data["OrderID"];
                            //价格
                            payparam["price"] = self.m_fPrice;
                            //商品名
                            payparam["name"] = self.m_textProName.getString();

                            var payCallBack = function (param) {
                                self.m_parent.dismissPopWait();
                                if (typeof(param) == "string" && "true" == param) {
                                    GlobalUserItem.setTodayPay();

                                    showToast(self, "支付成功", 2);
                                    //更新用户游戏豆
                                    GlobalUserItem.dUserBeans = GlobalUserItem.dUserBeans + self.m_nCount;
                                    //通知更新
                                    var eventListener = cc.EventCustom.new(yl.RY_USERINFO_NOTIFY);
                                    eventListener.obj = yl.RY_MSG_USERWEALTH;
                                    cc.director().getEventDispatcher().dispatchEvent(eventListener);

                                    self.hide();
                                    //重新请求支付列表
                                    self.m_parent.reloadBeanList();

                                    self.m_parent.updateScoreInfo();
                                } else {
                                    showToast(self, "支付异常", 2);
                                }
                            };
                            MultiPlatform.getInstance().thirdPartyPay(PAYTYPE[self.m_select].plat, payparam, payCallBack);
                        } else {
                            if (typeof(jstable["msg"]) == "string" && jstable["msg"] != "") {
                                showToast(self, jstable["msg"], 2);
                            }
                        }
                    }
                }
            });
        }
    },

    onSelectedEvent : function(tag, sender) {
        if (this.m_select == tag) {
            this.m_spBgKuang.getChildByTag(tag).setSelected(true);
            return;
        }

        this.m_select = tag;

        for (var i = 101; i <= 103; i++) {
            if (i != tag) {
                this.m_spBgKuang.getChildByTag(i).setSelected(false);
            }
        }

        //微信支付
        if ((tag == this.CBT_WECHAT)) {
            cc.log("wechat");
        }

        //支付宝
        if ((tag == this.CBT_ALIPAY)) {
            cc.log("alipay");
        }

        //俊付通
        if ((tag == this.CBT_JFT)) {
            cc.log("jft");
        }
    },

    onTouchBegan : function(touch, event) {
        return this.isVisible();
    },

    onTouchEnded : function(touch, event) {
        var pos = touch.getLocation();
        var m_spBg = this.m_spBgKuang;
        pos = m_spBg.convertToNodeSpace(pos);
        var rec = cc.rect(0, 0, m_spBg.getContentSize().width, m_spBg.getContentSize().height);
        if (false == cc.rectContainsPoint(rec, pos)) {
            this.hide();
        }
    },

    onExit : function() {
        ExternalFun.SAFE_RELEASE(this.m_actShowAct);
        this.m_actShowAct = null;
        ExternalFun.SAFE_RELEASE(this.m_actHideAct);
        this.m_actHideAct = null;
    },

    onJunFuTongPay : function(data) {
        // token参数
        var tokenparam = [];
        tokenparam.uid = yl.JFT["PartnerID"];
        tokenparam.oid = data["OrderID"] || "";
        tokenparam.mon = "" + this.m_fPrice;
        tokenparam.rurl = yl.JFT["NotifyURL"];
        tokenparam.nurl = yl.JFT["NotifyURL"];
        tokenparam.serviceType = "JFT";
        var szparam = cjson.encode(tokenparam);

        var self = this;
        //获取支付列表
        this.m_parent.showPopWait();
        MultiPlatform.getInstance().getPayList(szparam, function (listjson) {
            self.m_parent.dismissPopWait();
            if (typeof(listjson) == "string" && "" != listjson) {
                try{
                   var listtable =  cjson.decode(listjson);
                    //dump(listtable, "listtable", 6)
                    // typeid=3 为微信， typeid=4为支付宝
                    var itemname = this.m_textProName.getString();
                    var itemprice = this.m_textPrice.getString();
                    var jft = JunFuTongPay.create(self.m_parent, itemname, itemprice, listtable, token);
                    self.m_parent.addChild(jft);
                    this.hide();
                }catch(e){}
            }
        });
    },

    hide : function() {
        this.m_spBgKuang.stopAllActions();
        this.m_spBgKuang.runAction(this.m_actHideAct);
    }
});


var tabCheckBoxPositionX = 260;
// 支付模式
var APPSTOREPAY = 10;                   // iap支付;
var THIRDPAY = 20;                      // 第三方支付;

//商城页面
var ShopLayer = cc.Layer.extend({
    onEnterTransitionDidFinish : function () {
        this.onEnterTransitionFinish();
    },
    // 进入场景而且过渡动画结束时候触发。
    onEnterTransitionFinish : function() {
        //this.loadPropertyAndVip(this._select)
        if (0 == Object.keys(this._shopTypeIdList).length) {
            this.getShopPropertyType();
        } else {
            //刷新界面显示
            this.updateCheckBoxList();
        }
        return this;
    },

    onExitTransitionDidStart : function () {
        this.onExitTransitionStart();
    },
    // 退出场景而且开始过渡动画时候触发。
    onExitTransitionStart : function() {
        return this;
    },

    //scene
    //stmod 进入商店后的选择类型
    ctor : function(scene, stmod) {
        this._super(cc.color(0, 0, 0, 125));

        stmod = stmod || ShopLayer.CBT_SCORE;
        this.m_nPayMethod = GlobalUserItem.tabShopCache["nPayMethod"] || THIRDPAY;

        var self = this;
        this._scene = scene;

        // this.registerScriptHandler(function (eventType) {
        //     if (eventType == "enterTransitionFinish") {	// 进入场景而且过渡动画结束时候触发。
        //         self.onEnterTransitionFinish();
        //     } else if (eventType == "exitTransitionStart") {	// 退出场景而且开始过渡动画时候触发。
        //         self.onExitTransitionStart();
        //     }
        // });

        var tempLayer = ExternalFun.loadRootCSB(res.ShopLayer_json, this);
        var rootLayer = tempLayer.rootlayer;
        var csbNode = tempLayer.csbnode;

        //按钮回调
        this._btcallback = function (ref, type) {
            if (type == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };

        var cbtlistener = function (sender, eventType) {
            self.onSelectedEvent(sender.getTag(), sender, eventType);
        };

        this._select = stmod;

        this._showList = [];

        this._scoreList = GlobalUserItem.tabShopCache["shopScoreList"] || [];
        this._propertyList = GlobalUserItem.tabShopCache["shopPropertyList"] || [];
        this._vipList = GlobalUserItem.tabShopCache["shopVipList"] || [];
        //游戏豆购买列表
        this._beanList = GlobalUserItem.tabShopCache["shopBeanList"] || [];
        //实物兑换页面
        this._goodsList = null;
        //商店物品typeid
        this._shopTypeIdList = GlobalUserItem.tabShopCache["shopTypeIdList"] || [];
        //购买界面
        this.m_payLayer = null;
        //购买汇率
        this.m_nRate = GlobalUserItem.tabShopCache["shopRate"] || 0;
        //竣付通支付界面
        this.m_bJunfuTongPay = false;
        //道具关联信息
        this.m_tabPropertyRelate = GlobalUserItem.tabShopCache["propertyRelate"] || [];

        var sprite = new cc.Sprite(res.frame_shop_0_png);
        sprite.setPosition(yl.WIDTH / 2, yl.HEIGHT - 51);
        this.addChild(sprite);
        sprite.setVisible(false);

        var frame = cc.spriteFrameCache.getSpriteFrame("sp_public_frame_0.png");
        if (null != frame) {
            var sp = new cc.Sprite(frame);
            sp.setPosition(yl.WIDTH / 2, 320);
            sp.setVisible(false);
            this.addChild(sp);
        }

        //返回按钮
        var btn = csbNode.getChildByName("btn_back");
        btn.addTouchEventListener(function (ref, type) {
            if (type == ccui.Widget.TOUCH_ENDED) {
                self._scene.onKeyBack();
            }
        });


        //兑换记录
        var topBtn = new ccui.Button(res.btn_ubag_0_png, res.btn_ubag_1_png);
        topBtn.setPosition(yl.WIDTH - 90, yl.HEIGHT - 51);
        this.addChild(topBtn);
        topBtn.setTag(ShopLayer.BT_BAG);
        topBtn.setVisible(false);
        topBtn.addTouchEventListener(function (ref, tType) {
            if (tType == ccui.Widget.TOUCH_ENDED) {
                var tag = ref.getTag();
                if (tag == ShopLayer.BT_ORDERRECORD) {
                    self._scene.onChangeShowMode(yl.SCENE_ORDERRECORD);
                } else if (tag == ShopLayer.BT_BAG) {
                    self._scene.onChangeShowMode(yl.SCENE_BAG);
                }
            }
        });
        this.m_btnTopBtn = topBtn;


        //金币、游戏豆、元宝
        var sp1 = new cc.Sprite(res.frame_shop_2_png);
        sp1.setPosition(365, 699);
        this.addChild(sp1);
        sp1.setVisible(false);
        var sp2 = new cc.Sprite(res.icon_shop_0_png);
        sp2.setPosition(247, 699);
        this.addChild(sp2);
        sp2.setVisible(false);
        var sp3 = new cc.Sprite(res.frame_shop_3_png);
        sp3.setPosition(679, 699);
        this.addChild(sp3);
        sp3.setVisible(false);
        var sp4 = new cc.Sprite(res.icon_shop_1_png);
        sp4.setPosition(555, 699);
        this.addChild(sp4);
        sp4.setVisible(false);
        var sp5 = new cc.Sprite(res.frame_shop_4_png);
        sp5.setPosition(1000, 699);
        this.addChild(sp5);
        sp5.setVisible(false);
        var sp6 = new cc.Sprite(res.icon_shop_2_png);
        sp6.setPosition(877, 699);
        this.addChild(sp6);
        sp6.setVisible(false);

        this._txtGold = new cc.LabelAtlas((GlobalUserItem.lUserScore + "").replace(".", "/"), res.num_shop_0_png, 18, 27, "/");
        this._txtGold.setPosition(475, 710);
        this._txtGold.setAnchorPoint(cc.p(0.5, 0.5));
        this.addChild(this._txtGold);

        this._txtBean = new cc.LabelAtlas((GlobalUserItem.dUserBeans + "").replace(".", "/"), res.num_shop_0_png, 18, 27, "/");
        this._txtBean.setPosition(780, 710);
        this._txtBean.setAnchorPoint(cc.p(0.5, 0.5));
        this.addChild(this._txtBean);

        this._txtIngot = new cc.LabelAtlas((GlobalUserItem.lUserIngot + "").replace(".", "/"), res.num_shop_0_png, 18, 27, "/");
        this._txtIngot.setPosition(1100, 710);
        this._txtIngot.setAnchorPoint(cc.p(0.5, 0.5));
        this.addChild(this._txtIngot);

        sp1 = new cc.Sprite(res.frame_shop_5_png);
        sp1.setPosition(806, 320);
        this.addChild(sp1);
        sp1.setVisible(false);
        sp2 = new cc.Sprite(res.frame_shop_6_png);
        sp2.setPosition(178, 320);
        this.addChild(sp2);
        sp2.setVisible(false);
        //游戏币
        var chk1 = new ccui.CheckBox(res.bt_shop_0_0_shop_png, res.bt_shop_0_0_shop_png, res.bt_shop_0_1_shop_png, "", "");
        chk1.setPosition(190, 530);
        this.addChild(chk1);
        chk1.setSelected(false);
        chk1.setVisible(false);
        chk1.setEnabled(false);
        chk1.setName("check" + 5);
        chk1.setTag(ShopLayer.CBT_SCORE);
        chk1.addEventListener(cbtlistener);

        //游戏豆
        var chk2 = new ccui.CheckBox(res.bt_shop_1_0_shop_png, res.bt_shop_1_0_shop_png, res.bt_shop_1_1_shop_png, "", "");
        chk2.setPosition(190, 426);
        this.addChild(chk2);
        chk2.setSelected(false);
        chk2.setVisible(false);
        chk2.setEnabled(false);
        chk2.setName("check" + 6);
        chk2.setTag(ShopLayer.CBT_BEAN);
        chk2.addEventListener(cbtlistener);

        //VIP
        var chk3 = new ccui.CheckBox(res.bt_shop_2_0_shop_png, res.bt_shop_2_0_shop_png, res.bt_shop_2_1_shop_png, "", "");
        chk3.setPosition(190, 322);
        this.addChild(chk3);
        chk3.setSelected(false);
        chk3.setVisible(false);
        chk3.setEnabled(false);
        chk3.setName("check" + 7);
        chk3.setTag(ShopLayer.CBT_VIP);
        chk3.addEventListener(cbtlistener);

        //道具
        var chk4 = new ccui.CheckBox(res.bt_shop_3_0_shop_png, res.bt_shop_3_0_shop_png, res.bt_shop_3_1_shop_png, "", "");
        chk4.setPosition(190, 218);
        this.addChild(chk4);
        chk4.setSelected(false);
        chk4.setVisible(false);
        chk4.setEnabled(false);
        chk4.setName("check" + 8);
        chk4.setTag(ShopLayer.CBT_PROPERTY);
        chk4.addEventListener(cbtlistener);

        //实物
        var chk5 = new ccui.CheckBox(res.bt_shop_4_0_shop_png, res.bt_shop_4_0_shop_png, res.bt_shop_4_1_shop_png, "", "");
        chk5.setPosition(190, 114);
        this.addChild(chk5);
        chk5.setSelected(false);
        chk5.setVisible(false);
        chk5.setEnabled(false);
        chk5.setName("check" + 9);
        chk5.setTag(ShopLayer.CBT_ENTITY);
        chk5.addEventListener(cbtlistener);

        this.m_tabCheckBoxPosition =
            [
                [cc.p(tabCheckBoxPositionX, 540)],
                [cc.p(tabCheckBoxPositionX, 540), cc.p(tabCheckBoxPositionX, 426)],
                [cc.p(tabCheckBoxPositionX, 540), cc.p(tabCheckBoxPositionX, 426), cc.p(tabCheckBoxPositionX, 322)],
                [cc.p(tabCheckBoxPositionX, 540), cc.p(tabCheckBoxPositionX, 426), cc.p(tabCheckBoxPositionX, 322), cc.p(tabCheckBoxPositionX, 218)],
                [cc.p(tabCheckBoxPositionX, 540), cc.p(tabCheckBoxPositionX, 441), cc.p(tabCheckBoxPositionX, 344), cc.p(tabCheckBoxPositionX, 247), cc.p(tabCheckBoxPositionX, 150)],
                [cc.p(tabCheckBoxPositionX, 540), cc.p(tabCheckBoxPositionX, 426), cc.p(tabCheckBoxPositionX, 322), cc.p(tabCheckBoxPositionX, 218), cc.p(tabCheckBoxPositionX, 114), cc.p(tabCheckBoxPositionX, 10)]
            ];
        this.m_tabActiveCheckBox = GlobalUserItem.tabShopCache["shopActiveCheckBox"] || [];

        this._scrollView = new ccui.ScrollView();
        this._scrollView.setContentSize(cc.size(900, 500));
        this._scrollView.setAnchorPoint(cc.p(0.5, 0.5));
        this._scrollView.setPosition(cc.p(820, 334));
        this._scrollView.setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL);
        this._scrollView.setBounceEnabled(true);
        this._scrollView.setScrollBarEnabled(false);
        this.addChild(this._scrollView);

    },

    updateCheckBoxList : function() {
        var poslist = this.m_tabCheckBoxPosition[this.m_tabActiveCheckBox.length];
        if (null == poslist) {
            return;
        }
        for (k in this.m_tabActiveCheckBox) {
            var tmp = this.getChildByName(this.m_tabActiveCheckBox[k]);
            if (null != tmp) {
                tmp.setEnabled(true);
                tmp.setVisible(true);

                var pos = poslist[k];
                if (null != pos) {
                    tmp.setPosition(pos);
                }
            }
        }

        //选择的类型
        var tmp1 = this.getChildByTag(this._select);
        if (null != tmp1 && tmp1.isVisible()) {
            tmp1.setSelected(true);
            //请求物品列表
            this.loadPropertyAndVip(this._select);
        }
    },

    getShopPropertyType : function() {
        this._scene.showPopWait();
        var self = this;
        appdf.onHttpJsonTable(yl.HTTP_URL + "/WS/MobileInterface.ashx", "GET", "action=GetMobilePropertyType", function (jstable, jsdata) {
            self._scene.dismissPopWait();
            if (jstable != null) {
                var data = jstable["data"];
                if (data != null) {
                    if (null != data["valid"] && true == data["valid"]) {
                        var list = data["list"];
                        if (list != null) {
                            for (k in list) {
                                self._shopTypeIdList["check" + list[k].TypeID] = list[k];
                                self.m_tabActiveCheckBox.push("check" + list[k].TypeID);
                            }
                            GlobalUserItem.tabShopCache["shopTypeIdList"] = self._shopTypeIdList;
                            GlobalUserItem.tabShopCache["shopActiveCheckBox"] = self.m_tabActiveCheckBox;
                            //刷新界面显示
                            self.updateCheckBoxList();
                            return;
                        }
                    }
                }

                var msg = jstable["msg"];
                if (typeof(msg) == "string") {
                    showToast(self, msg, 2);
                }
            }
        });

    },

    //按键监听
    onButtonClickedEvent : function(tag,sender) {
        var beginPos = sender.getTouchBeganPosition();
        var endPos = sender.getTouchEndPosition();
        if (Math.abs(endPos.x - beginPos.x) > 30
            || Math.abs(endPos.y - beginPos.y) > 30) {
            cc.log("ShopLayer.onButtonClickedEvent ==> MoveTouch Filter");
            return;
        }

        var name = sender.getName();
        if (name == SHOP_BUY[ShopLayer.BT_SCORE]) {
            //游戏币获取
            GlobalUserItem.buyItem = this._scoreList[tag - ShopLayer.BT_SCORE];
            if (GlobalUserItem.buyItem.id == "game_score" && PriRoom) {
    //            this.getParent().getParent().onChangeShowMode(PriRoom.LAYTAG.LAYER_EXCHANGESCORE, GlobalUserItem.buyItem.resultGold)
                showToast(this, "支付服务未开通!", 2, cc.color(250, 0, 0, 255));
            } else {
                //           this.getParent().getParent().onChangeShowMode(yl.SCENE_SHOPDETAIL)
                showToast(this, "支付服务未开通!", 2, cc.color(250, 0, 0, 255));
            }
        } else if (name == SHOP_BUY[ShopLayer.BT_BEAN]) {
            //游戏豆获取
            var item = this._beanList[tag - ShopLayer.BT_BEAN];
            if (null == item) {
                return;
            }
            var bThirdPay = true;
            var self = this;
            if (ClientConfig.APPSTORE_VERSION
                && (targetPlatform == cc.PLATFORM_OS_IPHONE || targetPlatform == cc.PLATFORM_OS_IPAD)) {
                if (this.m_nPayMethod == APPSTOREPAY) {
                    bThirdPay = false;
                    var payparam = [];
                    payparam.http_url = yl.HTTP_URL;
                    payparam.uid = GlobalUserItem.dwUserID;
                    payparam.productid = item.nProductID;
                    payparam.price = item.price;

                    this.showPopWait();
                    this.runAction(new cc.Sequence(new cc.DelayTime(5), cc.CallFunc(function () {
                        self.dismissPopWait();
                    })));
                    showToast(this, "正在连接iTunes Store...", 4);
                    var payCallBack = function (param) {
                        if (typeof(param) == "string" && "true" == param) {
                            GlobalUserItem.setTodayPay();

                            showToast(self, "支付成功", 2);
                            //更新用户游戏豆
                            GlobalUserItem.dUserBeans = GlobalUserItem.dUserBeans + item.count;
                            //通知更新
                            var eventListener = cc.EventCustom.new(yl.RY_USERINFO_NOTIFY);
                            eventListener.obj = yl.RY_MSG_USERWEALTH;
                            cc.director().getEventDispatcher().dispatchEvent(eventListener);

                            //重新请求支付列表
                            self.reloadBeanList();

                            self.updateScoreInfo();
                        } else {
                            showToast(self, "支付异常", 2);
                        }
                    };
                    MultiPlatform.getInstance().thirdPartyPay(yl.ThirdParty.IAP, payparam, payCallBack);
                }
            }

            if (bThirdPay) {
                if (false == ShopPay.isPayMethodValid()) {
                    showToast(this, "支付服务未开通!", 2, cc.color(250, 0, 0, 255));
                    return;
                }
                if (null == this.m_payLayer) {
                    this.m_payLayer = new ShopPay(this);
                    this.addChild(this.m_payLayer);
                }
                var sprice = sprintf("%.2f元", item.price);
                this.m_payLayer.refresh(item.count, item.name, sprice, item.price, item.appid);
                this.m_payLayer.showLayer(true);
            }
        } else if (name ===SHOP_BUY[ShopLayer.BT_VIP]) {
            //vip购买
            cc.log("VIPLAYER");
            GlobalUserItem.buyItem = this._vipList[tag - ShopLayer.BT_VIP];
            this.getParent().getParent().onChangeShowMode(yl.SCENE_SHOPDETAIL);
        } else if (name == SHOP_BUY[ShopLayer.BT_PROPERTY]) {
            //道具购买
            GlobalUserItem.buyItem = this._propertyList[tag - ShopLayer.BT_PROPERTY];
            if (GlobalUserItem.buyItem.id == "room_card" && PriRoom) {
                this.getParent().getParent().onChangeShowMode(PriRoom.LAYTAG.LAYER_BUYCARD, GlobalUserItem.buyItem);
            } else {
                this.getParent().getParent().onChangeShowMode(yl.SCENE_SHOPDETAIL);
            }
        }
    },

    onSelectedEvent : function(tag,sender,eventType) {

        if (this._select == tag) {
            this.getChildByTag(tag).setSelected(true);
            return;
        }

        this._select = tag;

        for (var i = 1; i <= 5; i++) {
            if (i != tag) {
                this.getChildByTag(i).setSelected(false);
            }
        }

        //游戏币
        if (tag == ShopLayer.CBT_SCORE) {
            if (0 == this._scoreList.length) {
                this.loadPropertyAndVip(tag);
            } else {
                this.onUpdateScore();
            }
        }

        //游戏豆
        if (tag == ShopLayer.CBT_BEAN) {
            this.onClearShowList();
            if (0 == this._beanList.length) {
                this.loadPropertyAndVip(tag);
            } else {
                this.onUpdateBeanList();
            }
        }

        //vip
        if (tag == ShopLayer.CBT_VIP){
            if (( this._vipList.length == 0)) {
                this.loadPropertyAndVip(tag);
            } else {
                this.onUpdateVIP();
            }
        }

        //道具
        if (tag == ShopLayer.CBT_PROPERTY) {
            if ((this._propertyList.length == 0)) {
                this.loadPropertyAndVip(tag);
            } else {
                this.onUpdateProperty();
            }
        }

        var topBtnTag = ShopLayer.BT_BAG;
        var normalFile = "src/client/res/Information/btn_ubag_0.png";
        var pressFile = "src/client/res/Information/btn_ubag_1.png";
        //实物
        if (tag == ShopLayer.CBT_ENTITY) {
            this.onClearShowList();
            this.onUpdateGoodsList();

            topBtnTag = ShopLayer.BT_ORDERRECORD;
            normalFile = "src/client/res/Shop/bt_shop_exchange_0.png";
            pressFile = "src/client/res/Shop/bt_shop_exchange_1.png";
        }
        this.m_btnTopBtn.setTag(topBtnTag);
        this.m_btnTopBtn.loadTextureNormal(normalFile);
        this.m_btnTopBtn.loadTexturePressed(pressFile);
    },

    //网络请求
    loadPropertyAndVip : function(tag) {
        var self = this;
        var typid = 0;

        var cbt = this.getChildByTag(tag);
        if (null != cbt) {
            if (null != this._shopTypeIdList[cbt.getName()]) {
                typid = this._shopTypeIdList[cbt.getName()].TypeID;
            }
        }

        //实物特殊处理
        if (tag == ShopLayer.CBT_ENTITY) {
            this.onUpdateGoodsList();
            //游戏豆额外处理
        } else if (tag == ShopLayer.CBT_BEAN) {
            if (0 != this._beanList) {
                this.onUpdateBeanList();
                return;
            }
            this._scene.showPopWait();
            if (ClientConfig.APPSTORE_VERSION
                && (targetPlatform == cc.PLATFORM_OS_IPHONE || targetPlatform == cc.PLATFORM_OS_IPAD)) {
                // 内购开关
                appdf.onHttpJsonTable(yl.HTTP_URL + "/WS/MobileInterface.ashx", "GET", "action=iosnotappstorepayswitch", function (jstable, jsdata) {
                    var errmsg = "获取支付配置异常!";
                    if (jstable != null) {
                        var jdata = jstable["data"];
                        if (jdata != null) {
                            var valid = jdata["valid"] || false;
                            if (true == valid) {
                                errmsg = null;
                                var value = jdata["State"] || "0";
                                value = Number(value);
                                if (1 == value) {
                                    GlobalUserItem.tabShopCache["nPayMethod"] = APPSTOREPAY;
                                    self.m_nPayMethod = APPSTOREPAY;
                                    self.requestPayList(1);
                                } else {
                                    GlobalUserItem.tabShopCache["nPayMethod"] = THIRDPAY;
                                    // 请求列表
                                    self.requestPayList();
                                }
                            }
                        }
                    }

                    self._scene.dismissPopWait();
                    if (typeof errmsg == "string" && "" != errmsg) {
                        showToast(this, errmsg, 2, cc.color(250, 0, 0))
                    }
                });
            } else {
                // 请求列表
                this.requestPayList();
            }
        } else {
            if (tag == ShopLayer.CBT_VIP && 0 != this._vipList.length) {
                this.onUpdateVIP();
                return;
            } else if (tag == ShopLayer.CBT_PROPERTY && 0 != this._propertyList.length) {
                this.onUpdateProperty();
                return;
                //  } else if (  tag == ShopLayer.CBT_SCORE && 0 != #this._scoreList ) {
                //      this.onUpdateScore()
                //      return
            } else if (tag == ShopLayer.CBT_SCORE) {
                if (0 != this._beanList.length) {
                    this.onUpdateScore();
                    return;
                }
                this._scene.showPopWait();
                if (ClientConfig.APPSTORE_VERSION
                    && (targetPlatform == cc.PLATFORM_OS_IPHONE || targetPlatform == cc.PLATFORM_OS_IPAD)) {
                    // 内购开关
                    appdf.onHttpJsonTable(yl.HTTP_URL + "/WS/MobileInterface.ashx", "GET", "action=iosnotappstorepayswitch", function (jstable, jsdata) {
                        var errmsg = "获取支付配置异常!";
                        if (jstable != null) {
                            var jdata = jstable["data"];
                            if (jdata != null) {
                                var valid = jdata["valid"] || false;
                                if (true == valid) {
                                    errmsg = null;
                                    var value = jdata["State"] || "0";
                                    value = Number(value);
                                    if (1 == value) {
                                        GlobalUserItem.tabShopCache["nPayMethod"] = APPSTOREPAY;
                                        this.m_nPayMethod = APPSTOREPAY;
                                        self.requestScorePayList(1);
                                    } else {
                                        GlobalUserItem.tabShopCache["nPayMethod"] = THIRDPAY;
                                        // 请求列表
                                        self.requestScorePayList();
                                    }
                                }
                            }
                        }

                        self._scene.dismissPopWait();
                        if (typeof(errmsg) == "string" && "" != errmsg) {
                            showToast(self, errmsg, 2, cc.color(250, 0, 0))
                        }
                    });
                } else {
                    // 请求列表
                    this.requestScorePayList();
                }
                return
            }
            this.requestPropertyList(typid, tag)
        }
    },

    //wmc
    requestScorePayList : function(isIap) {
        isIap = isIap || 0;
        var beanurl = yl.HTTP_URL + "/WS/MobileInterface.ashx";
        var ostime = (new Date()).getTime();
        var self = this;
        this._scene.showPopWait();
        appdf.onHttpJsonTable(beanurl, "GET", "action=GetPayProduct&userid=" + GlobalUserItem.dwUserID + "&time=" + ostime + "&signature=" + GlobalUserItem.getSignature(ostime) + "&typeID=" + isIap, function (sjstable, sjsdata) {
            //dump(sjstable, "支付列表", 6);
            var errmsg = "获取支付列表异常!";
            self._scene.dismissPopWait();
            if (sjstable != null) {
                var sjdata = sjstable["data"];
                var msg = sjstable["msg"];
                errmsg = null;
                if (typeof(msg) == "string") {
                    errmsg = msg;
                }

                if (sjdata!== null) {
                    var isFirstPay = sjdata["IsPay"] || "0";
                    isFirstPay = Number(isFirstPay);
                    var sjlist = sjdata["list"];
                    if (sjlist != null) {
                        //Ryu 从备案那边copy过来 清空列表显示
                        self._scoreList = null;
                        self._scoreList = [];
                        //Ryu 不清空是等死呢？
                        for (i = 0; i<sjlist.length;i++) {
                            var sitem = sjlist[i];
                            var item = [];
                            item.price = sitem["Price"];
                            item.isfirstpay = isFirstPay;
                            item.paysend = sitem["AttachCurrency"] || "0";
                            item.paysend = Number(item.paysend);
                            item.paycount = sitem["PresentCurrency"] || "0";
                            item.paycount = Number(item.paycount);
                            item.price = Number(item.price);
                            item.count = item.paysend + item.paycount;
                            item.description = sitem["Description"];
                            item.name = sitem["ProductName"];
                            item.sortid = Number(sitem["SortID"]) || 0;
                            item.nOrder = 0;
                            item.appid = Number(sitem["AppID"]);
                            item.nProductID = sitem["ProductID"] || "";
                            item.payObjectType = Number(sitem["PayObjectType"]) || 0;

                            //首充赠送
                            if (0 != item.paysend) {
                                //当日未首充
                                if (0 == isFirstPay) {
                                    item.nOrder = 1;
                                    if (item.payObjectType == 1) {
                                        self._scoreList.push(item);
                                    }
                                }
                            } else {
                                if (item.payObjectType == 1) {
                                    self._scoreList.push(item);
                                }
                            }
                        }
                        self._scoreList.sort(function (a, b) {
                            if (a.nOrder != b.nOrder) {
                                return a.nOrder > b.nOrder;
                            } else {
                                return a.sortid < b.sortid;
                            }
                        });
                        GlobalUserItem.tabShopCache["shopScoreList"] = this._scoreList;
                        self.onUpdateScore();
                    }
                }
            }

            if (typeof(errmsg) == "string" && "" != errmsg) {
                showToast(self, errmsg, 2, cc.color(250, 0, 0));
            }
        });
    },
    //end wmc

    requestPayList : function(isIap) {
        isIap = isIap || 0;
        var self = this;
        var beanurl = yl.HTTP_URL + "/WS/MobileInterface.ashx";
        var ostime = (new Date()).getTime();

        this._scene.showPopWait();
        appdf.onHttpJsonTable(beanurl, "GET", "action=GetPayProduct&userid=" + GlobalUserItem.dwUserID + "&time=" + ostime + "&signature=" + GlobalUserItem.getSignature(ostime) + "&typeID=" + isIap, function (sjstable, sjsdata) {
            // dump(sjstable, "支付列表", 6)
            var errmsg = "获取支付列表异常!";
            self._scene.dismissPopWait();
            if (sjstable != null) {
                var sjdata = sjstable["data"];
                var msg = sjstable["msg"];
                errmsg = null;
                if (typeof(msg) == "string") {
                    errmsg = msg;
                }

                if (sjdata != null) {
                    var isFirstPay = sjdata["IsPay"] || "0";
                    isFirstPay = Number(isFirstPay);
                    var sjlist = sjdata["list"];
                    if (sjlist != null) {
                        for (var i = 0; i < sjlist.length; i++) {
                            var sitem = sjlist[i];
                            var item = [];
                            item.price = sitem["Price"];
                            item.isfirstpay = isFirstPay;
                            item.paysend = sitem["AttachCurrency"] || "0";
                            item.paysend = Number(item.paysend);
                            item.paycount = sitem["PresentCurrency"] || "0";
                            item.paycount = Number(item.paycount);
                            item.price = Number(item.price);
                            item.count = item.paysend + item.paycount;
                            item.description = sitem["Description"];
                            item.name = sitem["ProductName"];
                            item.sortid = Number(sitem["SortID"]) || 0;
                            item.nOrder = 0;
                            item.appid = Number(sitem["AppID"]);
                            item.nProductID = sitem["ProductID"] || "";
                            item.payObjectType = Number(sitem["PayObjectType"]) || 0;

                            //首充赠送
                            if (0 != item.paysend) {
                                //当日未首充
                                if (0 == isFirstPay) {
                                    item.nOrder = 1;
                                    if (item.payObjectType == 2) {
                                        self._beanList.push(item);
                                    }
                                }
                            } else {
                                if (item.payObjectType == 2) {
                                    table.insert(self._beanList, item);
                                }
                            }
                        }
                        self._beanList.sort(function (a, b) {
                            if (a.nOrder != b.nOrder) {
                                return a.nOrder > b.nOrder;
                            } else {
                                return a.sortid < b.sortid;
                            }
                        });
                        GlobalUserItem.tabShopCache["shopBeanList"] = self._beanList;
                        self.onUpdateBeanList();
                    }
                }
            }

            if (typeof(errmsg) == "string" && "" != errmsg) {
                showToast(self, errmsg, 2, cc.color(250, 0, 0));
            }
        });
    },

    requestPropertyList : function(nTypeID, tag) {
        nTypeID = nTypeID || 0;
        var self = this;
        this._scene.showPopWait();
        appdf.onHttpJsonTable(yl.HTTP_URL + "/WS/MobileInterface.ashx", "GET", "action=GetMobileProperty&TypeID=" + nTypeID, function (jstable, jsdata) {
            self._scene.dismissPopWait();
            // dump(jstable, "jstable", 7)
            if (jstable != null) {
                var code = jstable["code"];
                var tmpList = [];

                if (Number(code) == 0) {
                    var datax = jstable["data"];
                    if (datax) {
                        var valid = datax["valid"];
                        if (valid == true) {
                            var listcount = datax["total"];
                            var list = datax["list"];
                            if (list != null) {
                                for (var i = 0; i < list.length; i++) {
                                    var item = [];
                                    item.id = Number(list[i]["ID"]);
                                    item.name = list[i]["Name"];
                                    item.bean = Number(list[i]["Cash"]);
                                    item.gold = Number(list[i]["Gold"]);
                                    item.ingot = Number(list[i]["UserMedal"]);
                                    item.loveliness = Number(list[i]["LoveLiness"]);
                                    item.resultGold = Number(list[i]["UseResultsGold"]);
                                    item.description = list[i]["RegulationsInfo"];
                                    item.sortid = list[i]["SortID"] || "0";
                                    item.sortid = Number(item.sortid);
                                    item.minPrice = Number(list[i]["minPrice"]) || 0;
                                    if (0 != item.id && null != item.id) {
                                        if ((item.loveliness != 0) && (item.bean == 0 && item.gold == 0 && item.ingot == 0)) {

                                        } else {
                                            if (tag == ShopLayer.CBT_PROPERTY && item.id == 501) {
                                                if (GlobalUserItem.bEnableRoomCard) {
                                                    item.id = "room_card";
                                                    tmpList.push(item);
                                                }
                                            } else if (tag == ShopLayer.CBT_SCORE && item.id == 501 && 0 != item.resultGold) {
                                                if (GlobalUserItem.bEnableRoomCard) {
                                                    item.id = "game_score";
                                                    item.minPrice = item.resultGold;
                                                    item.bean = 0;
                                                    item.gold = 0;
                                                    item.ingot = 0;
                                                    item.loveliness = 0;
                                                    tmpList.push(item);
                                                }
                                            } else {
                                                tmpList.push(item);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //产品排序
                tmpList.sort(function (a, b) {
                    return a.sortid > b.sortid;
                });

                if (tag == ShopLayer.CBT_VIP) {
                    GlobalUserItem.tabShopCache["shopVipList"] = tmpList;
                    self._vipList = tmpList;
                    self.onUpdateVIP();
                } else if (tag == ShopLayer.CBT_PROPERTY) {
                    GlobalUserItem.tabShopCache["shopPropertyList"] = tmpList;
                    self._propertyList = tmpList;
                    self.onUpdateProperty();
                } else if (tag == ShopLayer.CBT_SCORE) {
                    GlobalUserItem.tabShopCache["shopScoreList"] = tmpList;
                    self._scoreList = tmpList;
                    self.onUpdateScore();
                }
            } else {
                showToast(self, "抱歉，获取道具信息失败！", 2, cc.color(250, 0, 0));
            }
        });
    },

    reloadBeanList : function() {
        this.onClearShowList();
        GlobalUserItem.tabShopCache["shopBeanList"] = [];
        this._beanList = [];
        this.loadPropertyAndVip(ShopLayer.CBT_BEAN);
    },

    updateScoreInfo : function() {
        this._txtGold.setString(string_formatNumberThousands(GlobalUserItem.lUserScore, true, "/"));
        this._txtBean.setString(string_formatNumberThousands(GlobalUserItem.dUserBeans, true, "/"));
        this._txtIngot.setString(string_formatNumberThousands(GlobalUserItem.lUserIngot, true, "/"));
    },

    //更新游戏币
    onUpdateScore : function() {
        this.onClearShowList();
        this.onUpdateShowList(this._scoreList, ShopLayer.BT_SCORE);
    },

    //更新游戏豆
    onUpdateBeanList : function() {
        this.onClearShowList();
        this.onUpdateShowList(this._beanList, ShopLayer.BT_BEAN);
    },

    //更新VIP
    onUpdateVIP : function() {
        this.onClearShowList();
        this.onUpdateShowList(this._vipList, ShopLayer.BT_VIP);
    },

    //更新道具
    onUpdateProperty : function() {
        this.onClearShowList();
        this.onUpdateShowList(this._propertyList, ShopLayer.BT_PROPERTY);
    },

    //更新实物兑换列表
    onUpdateGoodsList : function() {
        var url = yl.HTTP_URL + "/SyncLogin.aspx?userid=" + GlobalUserItem.dwUserID + "&time=" + (new Date()).getTime() + "&signature=" + GlobalUserItem.getSignature((new Date()).getTime()) + "&url=/Mobile/Shop/Goods.aspx";
        if (null == this._goodsList) {
            //平台判定
            var targetPlatform = cc.sys;
            if ((cc.sys.IPHONE == targetPlatform) || (cc.sys.IPAD == targetPlatform) || (cc.sys.ANDROID == targetPlatform)) {
                this._goodsList = ccexp.WebView.create();
                this._goodsList.setPosition(cc.p(805, 324));
                this._goodsList.setContentSize(cc.size(938, 520));

                this._goodsList.setJavascriptInterfaceScheme("ryweb");
                this._goodsList.setScalesPageToFit(true);
                this._goodsList.setOnJSCallback(function (sender, url) {
                    this.queryUserScoreInfo(function (ok) {
                        if (ok) {
                            this.updateScoreInfo();
                            this._goodsList.reload();
                        }
                    });
                });

                this._goodsList.setOnDidFailLoading(function (sender, url) {
                    this._scene.dismissPopWait();
                    cc.log("open " + url + " fail");
                });
                this._goodsList.setOnShouldStartLoading(function (sender, url) {
                    cc.log("onWebViewShouldStartLoading, url is ", url);
                    return true;
                });
                this._goodsList.setOnDidFinishLoading(function (sender, url) {
                    this._scene.dismissPopWait();
                    ExternalFun.visibleWebView(this._goodsList, true);
                    cc.log("onWebViewDidFinishLoading, url is ", url);
                });
                this.addChild(this._goodsList);
            }
        }

        if (null != this._goodsList) {
            this._scene.showPopWait();
            ExternalFun.visibleWebView(this._goodsList, false);
            this._goodsList.loadURL(url);
        }
    },

    //清除当前显示
    onClearShowList : function() {
        for (var i = 0; i<this._showList.length; i++) {
            this._showList[i].removeFromParent();
        }
        this._showList = null;
        this._showList = [];

        if (null != this._goodsList) {
            this._goodsList.removeFromParent();
            this._goodsList = null;
        }
    },

    //更新当前显示
    onUpdateShowList : function(theList,tag) {
        var bGold = (this._select == ShopLayer.CBT_SCORE);
        var bOther = (this._select != ShopLayer.CBT_SCORE);
        var bBean = (this._select == ShopLayer.CBT_BEAN);

        //计算scroll滑动高度
        var scrollHeight = 0;
        if (theList.length < 7) {
            scrollHeight = 500;
            this._scrollView.setInnerContainerSize(cc.size(1130, 500));
        } else {
            scrollHeight = 260 * Math.ceil(theList.length / 3) + 30;//Math.floor((#theList+Math.floor(#theList%3))/3)
            this._scrollView.setInnerContainerSize(cc.size(1130, scrollHeight));
        }
        this._scrollView.jumpToTop();

        for (var i = 0; i < theList.length; i++) {
            var item = theList[i];

            this._showList[i] = new cc.LayerColor(cc.color(100, 100, 100, 0), 0, 0);
            // this._showList[i] = new cc.LayerColor(cc.color(100, 100, 100, 0), 261, 240);
            this._showList[i].setPosition(160 + Math.floor(i % 3) * 260 - 50 + 10, scrollHeight - (8 + 120 + Math.floor(i / 3) * 200) - 90 + 10);
            // this._showList[i].setPosition(160 + Math.floor((i - 1) % 3) * 260 - 50, scrollHeight - (8 + 120 + Math.floor((i - 1) / 3) * 200) - 90);
            this._scrollView.addChild(this._showList[i]);

            var btn = new ccui.Button(res.frame_shop_7_png, res.frame_shop_7_png);
            btn.setContentSize(cc.size(300, 240));
            btn.setPosition(70+20, 120+15);
            btn.setTag(tag + i);
            btn.setSwallowTouches(false);
            btn.setName(SHOP_BUY[tag]);
            this._showList[i].addChild(btn);
            btn.addTouchEventListener(this._btcallback);

            var price = 0;
            var sign = null;
            var pricestr = "";


            //物品信息
            var showSp = null;
            //标题
            var titleSp = null;
            if (bGold) {
                if (item.count < 300) {
                    showSp = new cc.Sprite(res.icon_public_game_score_1_png);
                } else if (item.count < 800) {
                    showSp = new cc.Sprite(res.icon_public_game_score_2_png);
                } else if (item.count < 1000) {
                    showSp = new cc.Sprite(res.icon_public_game_score_3_png);
                } else if (item.count < 5000) {
                    showSp = new cc.Sprite(res.icon_public_game_score_4_png);
                } else if (item.count >= 5000) {
                    showSp = new cc.Sprite(res.icon_public_game_score_5_png);
                } else {
                    showSp = new cc.Sprite(res.icon_public_game_score_1_png);
                }


                showSp.setPosition(60, 90);
                //gold数量
                var atlas = new cc.LabelAtlas((item.count + "").replace(".", "/"), res.num_shop_5_png, 24, 30, "/");
                atlas.setAnchorPoint(cc.p(0.5, 0.5));
                atlas.setLocalZOrder(20);
                atlas.setPosition(70, 95);
                this._showList[i].addChild(atlas);

                var name = new cc.Sprite(res.text_shop_0_png);
                name.setAnchorPoint(cc.p(0, 0.5));
                name.setLocalZOrder(20);
                name.setVisible(false);
                name.setPosition(10, 60);
                this._showList[i].addChild(name);
                var wid = (atlas.getContentSize().width + name.getContentSize().width) / 2;

                price = item.price;
                pricestr = price + "";
                //   			pricestr = "￥" + string.formatNumberThousands(price,true,",")

                //首充
                if (null != item.paysend && 0 != item.paysend) {
                    var fsp1 = new cc.Sprite(res.shop_firstpay_sp_png);
                    fsp1.setAnchorPoint(cc.p(0, 1.0));
                    fsp1.setPosition(90, 215);
                    this._showList[i].addChild(fsp1);
                    var isFirstPay1 = item.isfirstpay == 0;
                    btn.setEnabled(isFirstPay1);
                }
                //end wmc
            } else if (bBean) {
                if (item.count < 300) {
                    showSp = new cc.Sprite(res.icon_shop_5_1_png);
                } else if (item.count < 800) {
                    showSp = new cc.Sprite(res.icon_shop_5_2_png);
                } else if (item.count < 1000) {
                    showSp = new cc.Sprite(res.icon_shop_5_3_png);
                } else if (item.count < 5000) {
                    showSp = new cc.Sprite(res.icon_shop_5_4_png);
                } else if (item.count >= 5000) {
                    showSp = new cc.Sprite(res.icon_shop_5_5_png);
                } else {
                    showSp = new cc.Sprite(res.icon_shop_5_png);
                }


                showSp.setPosition(60, 90);
                //bean数量
                atlas = new cc.LabelAtlas((item.count + "").replace(".", "/"), res.num_shop_5_png, 24, 30, "/");
                atlas.setAnchorPoint(cc.p(0.5, 0.5));
                atlas.setLocalZOrder(20);
                atlas.setPosition(70, 95);
                this._showList[i].addChild(atlas);

                name = new cc.Sprite(res.text_shop_1_png);
                name.setAnchorPoint(cc.p(0, 0.5));
                name.setLocalZOrder(20);
                name.setVisible(false);
                name.setPosition(10, 60);
                this._showList[i].addChild(name);
                wid = (atlas.getContentSize().width + name.getContentSize().width) / 2;

                price = item.price;
                pricestr = price + "";

                //首充
                if (null != item.paysend && 0 != item.paysend) {
                    var fsp = new cc.Sprite(res.shop_firstpay_sp_png);
                    fsp.setAnchorPoint(cc.p(0, 1.0));
                    fsp.setPosition(90, 215);
                    this._showList[i].addChild(fsp);
                    var isFirstPay = item.isfirstpay == 0;
                    btn.setEnabled(isFirstPay);
                }
            } else {
                var frame = null;
                var str = "";
                if (typeof(item.id) == "string" && item.id == "game_score") {
                    var nTag = 3;
                    //if ( end
                    str = "src/client/res/Shop/Icon_public/icon_public_" + item.id + "_" + nTag + ".png";
                    // frame = cc.spriteFrameCache.getSpriteFrame(str);
                } else {
                    str = "src/client/res/Shop/Icon_public/icon_public_" + item.id + ".png";
                    // frame = cc.spriteFrameCache.getSpriteFrame("icon_public_" + item.id + ".png");
                }
                // frame = cc.spriteFrameCache.getSpriteFrame("icon_public_" + item.id + ".png");
                if (null != str) {
                // if (null != frame) {
                    showSp = new ccui.ImageView(str);
                    // showSp = new cc.Sprite(frame);
                }
                //todo
                // if (cc.FileUtils.getInstance().isFileExist("Shop/title_property_" + item.id + ".png")) {
                    titleSp = new ccui.ImageView("src/client/res/Shop/title_property_" + item.id + ".png");
                    titleSp.setVisible(false);
                // }
                sign = 0;
                if (item.bean == 0) {
                    if (item.ingot == 0) {
                        if (item.gold == 0) {
                            if (item.loveliness == 0) {
                                price = item.minPrice;
                                sign = 4;
                            } else {
                                price = item.loveliness;
                                sign = 3;
                            }
                        } else {
                            price = item.gold;
                            sign = 2;
                        }
                    } else {
                        price = item.ingot;
                        sign = 1;
                    }
                } else {
                    price = item.bean;
                }
                //pricestr = string.formatNumberThousands(price,true,",")
                pricestr = price + "";

                //小闪光
                var icon_star = new ccui.ImageView(res.icon_star_sp_png);
                if (null != icon_star) {
                    icon_star.setPosition(130, 140);
                    this._showList[i].addChild(icon_star, 1);
                }
            }
            if (price == 0) {
                cc.log("======= ***** 价格信息有误 ***** =======");
                return;
            }

            if (null != showSp) {
                showSp.setPosition(70, 140);
                this._showList[i].addChild(showSp);
            }
            if (null != titleSp) {
                titleSp.setPosition(80, 200);
                //titleSp.setVisible(bOther)
                this._showList[i].addChild(titleSp);
            }
            //支付方式底图
            var spPayType = new ccui.ImageView(res.sign_btn_0_png);
            spPayType.setAnchorPoint(cc.p(0.0, 0.5));
            spPayType.setPosition(10, 60);
            this._showList[i].addChild(spPayType, 1);

            var priceLabel = new cc.LabelAtlas((pricestr + "").replace(".", "/"), res.num_shop_0_png, 18, 22, "/");
            priceLabel.setAnchorPoint(cc.p(0.5, 0.5));
            priceLabel.setPosition(70, 60);
            this._showList[i].addChild(priceLabel);
            priceLabel.setLocalZOrder(20);

            if (null != sign) {
                var width = 0;
                //todo
                // if (cc.FileUtils.getInstance().isFileExist("src/client/res/Shop/sign_shop_" + sign + ".png")) {
                    var spsign = new ccui.ImageView("src/client/res/Shop/sign_shop_" + sign + ".png");
                    spsign.setAnchorPoint(cc.p(0.0, 0.5));
                    spsign.setPosition(10, 60);
                    this._showList[i].addChild(spsign);
                    spsign.setLocalZOrder(20);
                // }

                priceLabel.setAnchorPoint(cc.p(0, 0.5));
                priceLabel.setPosition(40, 60);
                priceLabel.setLocalZOrder(20);
            }
            var yuan = new cc.Sprite(res.num_yuan_png);
            yuan.setPosition(40 + priceLabel.getContentSize().width, 60);
            yuan.setAnchorPoint(0, 0.5);
            yuan.setVisible(sign == 4);
            this._showList[i].addChild(yuan, 1);
        }
    },

    onKeyBack : function() {
        if (null != this.m_payLayer) {
            if (true == this.m_payLayer.isVisible()) {
                return true;
            }

            if (true == this.m_bJunfuTongPay) {
                return true;
            }
        }
        return false;
    },

    showPopWait : function() {
        this._scene.showPopWait();
    },

    dismissPopWait : function() {
        this._scene.dismissPopWait();
    },

    queryUserScoreInfo : function(queryCallback) {
        if (null != this._scene.queryUserScoreInfo) {
            this._scene.queryUserScoreInfo(queryCallback);
        }
    }

});
ShopLayer.CBT_SCORE			= 1;
ShopLayer.CBT_BEAN			= 2;
ShopLayer.CBT_VIP			= 3;
ShopLayer.CBT_PROPERTY		= 4;
ShopLayer.CBT_ENTITY		= 5;

ShopLayer.BT_SCORE			= 30;
ShopLayer.BT_VIP			= 50;
ShopLayer.BT_PROPERTY		= 60;
ShopLayer.BT_GOODS			= 120;
ShopLayer.BT_BEAN			= 520;

ShopLayer.BT_ORDERRECORD    = 1001;
ShopLayer.BT_BAG            = 1002;

SHOP_BUY = [];
SHOP_BUY[ShopLayer.BT_SCORE] = "shop_score_buy";
SHOP_BUY[ShopLayer.BT_BEAN] = "shop_bean_buy";
SHOP_BUY[ShopLayer.BT_VIP] = "shop_vip_buy";
SHOP_BUY[ShopLayer.BT_PROPERTY] = "shop_prop_buy";
SHOP_BUY[ShopLayer.BT_GOODS] = "shop_goods_buy";
// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new ShopLayer(null);
//         this.addChild(layer);
//     }
// });