/*  Author  :   JIN */

// var ExternalFun = appdf.req(appdf.EXTERNAL_SRC + "ExternalFun")
// var QueryDialog = appdf.req("app.views.layer.other.QueryDialog")

//游戏转盘界面
var TableLayer = cc.Layer.extend({

    rotateParam :
        [
            [key = 100	, 	angle = 360	],
            [key = 200	, 	angle = 330	],
            [key = 300	, 	angle = 300	],
            [key = 400	, 	angle = 270	],
            [key = 500	, 	angle = 240	],
            [key = 600	, 	angle = 210	],
            [key = 700	, 	angle = 180	],
            [key = 800	, 	angle = 150	],
            [key = 900	, 	angle = 120	],
            [key = 1000	, 	angle = 90	],
            [key = 1100	, 	angle = 60	],
            [key = 1200	, 	angle = 30	]
        ],

    ctor : function(scene) {
        this._super();
        this._scene = scene;

        var self = this;
        //注册触摸事件
        //ExternalFun.registerTouchEvent(this, true);

        //加载csb资源
        var rootLayer = (ExternalFun.loadRootCSB(res.TableLayer_json, this)).rootlayer;
        var csbNode = (ExternalFun.loadRootCSB(res.TableLayer_json, this)).csbnode;
        //转盘
        this.m_spTable = csbNode.getChildByName("sp_table");
        //转盘金币
        var node_point = this.m_spTable.getChildByName("node_point");
        this.m_tabAtlasPos = {};

        for (var i = 0; i < 12; i++) {
            this.m_tabAtlasPos[i] = node_point.getChildByName(sprintf("altas_pos_%d", i));
        }

        //开始按钮
        this.m_btnStart = csbNode.getChildByName("start_btn");
        this.m_btnStart.addTouchEventListener(function (sender, tType) {
            if (tType == ccui.Widget.TOUCH_ENDED) {
                if (self.m_nFreeCount <= 0) {
                    if (1 == self.m_bConfigPay) {
                        this._queryDialog = new QueryDialog("您的免费次数已用完，是否付费 " + self.m_lChargeFee + " 进行抽奖？", function (ok) {
                            if (ok == true) {
                                self.onRequestTable();
                            }
                            this._queryDialog = null;
                        });
                        this._queryDialog.setCanTouchOutside(false);
                        this.addChild(this._queryDialog);
                    } else {
                        showToast(self, "您的免费次数已用完！", 3);
                    }
                } else {
                    this.onRequestTable();
                }
            }
        });

        //转盘机会
        this.m_atlasCount = csbNode.getChildByName("atlas_count");
        this.m_atlasCount.setString("");
        this.m_nFreeCount = 0;

        //游戏结果
        this.m_atlasRes = csbNode.getChildByName("atlas_res");
        this.m_atlasRes.setString("");

        //金币背景
        this.m_spRes = csbNode.getChildByName("sp_res");
        this.m_spRes.setVisible(false);

        //是否转盘结束
        this.m_bRotateOver = true;

        //转盘结果
        this.m_LotteryRes = {};

        // 配置允许付费
        this.m_bConfigPay = 0;
        // 付费金额
        this.m_lChargeFee = 0;

    },

    onKeyBack : function() {
        if (false === self.m_bRotateOver) {
            showToast(this, "暂时不可返回", 2);
            return true;
        }
        return false;
    },

    onEnterTransitionFinish : function() {
        this.showPopWait();
        //获取抽奖奖品配置
        var url = yl.HTTP_URL + "/WS/Lottery.ashx";
        appdf.onHttpJsionTable(url, "GET", "action=LotteryConfig", function (jstable, jsdata) {
            // dump(jstable, "jstable", 6)
            if (typeof jstable == "table") {
                var data = jstable["data"];
                if (typeof data == "table") {
                    var valid = data["valid"];
                    if (null != valid && true == valid) {
                        var list = data["list"];
                        if (typeof list == "table") {
                            for (var i = 0; list.length; i++) {
                                //配置转盘
                                var lottery = list[i];
                                rotateParam[lottery.ItemIndex].key = lottery.ItemQuota;
                                this.m_tabAtlasPos[i].setString(lottery.ItemQuota + "");
                            }
                        }
                    }
                }
                var msg = jstable["msg"];
                //获取用户信息
                //TODO:cc.time();
                var ostime = new Date();
                appdf.onHttpJsionTable(url, "GET", "action=LotteryUserInfo&userid=" + GlobalUserItem.dwUserID + "&time=" + ostime + "&signature=" + GlobalUserItem.getSignature(ostime), function (sjstable, sjsdata) {
                    this.dismissPopWait();
                    // dump(sjstable, "sjstable", 6)
                    if (typeof sjstable == "table") {
                        var sdata = sjstable["data"];
                        if (typeof sdata == "table") {
                            var valid = sdata["valid"];
                            msg = sdata["msg"];
                            if (null != valid && true == valid) {
                                var slist = sdata["list"];
                                if (typeof slist == "table") {
                                    msg = null;
                                    //免费转盘机会
                                    var freecount = slist["FreeCount"] || 0;
                                    //剩余免费次数
                                    var aready = slist["AlreadyCount"] || 0;
                                    // 抽奖付费
                                    this.m_lChargeFee = slist["ChargeFee"] || 0;
                                    // 允许收费
                                    this.m_bConfigPay = slist["IsCharge"] || 0;

                                    var leftcount = freecount - aready;
                                    freecount = (leftcount >= 0) && leftcount || 0;
                                    this.m_nFreeCount = freecount;
                                    this.m_atlasCount.setString(freecount + "");
                                }
                            }
                        }
                    }
                })
            }
        });
    },

    onTouchBegan : function(touch, event) {
        return (self.m_bRotateOver && this.isVisible());
    },

    onTouchEnded : function(touch, event) {
        var pos = touch.getLocation();
        var m_spBg = self.m_spTable;
        pos = m_spBg.convertToNodeSpace(pos);
        var rec = cc.rect(0, 0, m_spBg.getContentSize().width, m_spBg.getContentSize().height);
        if (false == cc.rectContainsPoint(rec, pos)) {
            this._scene.onKeyBack();
        }
    },

    // 请求转盘
    onRequestTable : function() {
        self.m_spRes.setVisible(false);
        self.m_atlasRes.setString("");
        self.m_bRotateOver = false;
        self.m_btnStart.setEnabled(false);

        this.showPopWait();
        var ostime = new Date();
        var url = yl.HTTP_URL + "/WS/Lottery.ashx";
        appdf.onHttpJsionTable(url, "GET", "action=LotteryStart&userid=" + GlobalUserItem.dwUserID + "&time=" + ostime + "&signature=" + GlobalUserItem.getSignature(ostime), function (jstable, jsdata) {
            // dump(jstable, "jstable", 6);
            var msg = "转盘数据获取失败";
            if (typeof jstable == "table") {
                msg = jstable["msg"];
                var data = jstable["data"];
                if (typeof data == "table") {
                    var valid = data["valid"];
                    if (null != valid && true == valid) {
                        var list = data["list"];
                        if (typeof list == "table") {
                            self.m_LotteryRes = list;
                            var idx = list["ItemIndex"];
                            msg = null;
                            if (null != idx) {
                                self.rotateTo(Number(idx));

                                self.m_nFreeCount = self.m_nFreeCount - 1;
                                self.m_nFreeCount = (self.m_nFreeCount >= 0) && self.m_nFreeCount || 0;
                                self.m_atlasCount.setString(self.m_nFreeCount + "");
                            } else {
                                msg = "抽奖异常";
                            }
                        }
                    }
                }
            }

            self.dismissPopWait();
            if (null != msg) {
                showToast(self, msg, 3);
                self.m_btnStart.setEnabled(true);
                self.m_bRotateOver = true;
            }
        });
    },

    rotateTable : function( toKey ) {
        var idx = 1;
        for (k in rotateParam) {
            if (rotateParam[key] === toKey) {
                idx = k;
                break;
            }
        }
        self.rotateTo(idx);
    },

    rotateTo : function( idx ) {
        if (null === rotateParam[idx]) {
            self.m_bRotateOver = true;
            self.m_btnStart.setEnabled(true);
            return;
        }
        cc.log("rotate to " + rotateParam[idx].angle);

        var angle = rotateParam[idx].angle + 360 * 5;
        var act = new cc.RotateTo(10, angle);
        var easeRotate = cc.EaseCircleActionOut(act);
        var call = new cc.CallFunc(function () {
            cc.log("rotate over");
            GlobalUserItem.setTodayTable();

            self.m_btnStart.setEnabled(true);
            var str = string.format("%d", rotateParam[idx].key);
            self.m_atlasRes.setString(str);

            var toastMsg = "";
            var frame = null;
            //判断类型
            if (0 == self.m_LotteryRes.ItemType) {  		//金币
                frame = new cc.SpriteFrameCache().getSpriteFrame("sp_res.png");
                toastMsg = "恭喜您获得" + str + "金币!";
            } else if (1 == self.m_LotteryRes.ItemType) {  	//游戏豆
                frame = cc.SpriteFrameCache.getInstance().getSpriteFrame("sp_res_bean.png");
                toastMsg = "恭喜您获得" + str + "游戏豆!";
            } else if (2 == self.m_LotteryRes.ItemType) {      //元宝
                frame = cc.SpriteFrameCache.getInstance().getSpriteFrame("sp_res_ingot.png");
                toastMsg = "恭喜您获得" + str + "元宝!";
            }
            if (null != frame) {
                self.m_spRes.setSpriteFrame(frame);
                self.m_spRes.setVisible(true);
            }
            self._scene.coinDropDownAni(function () {
                self.m_bRotateOver = true;
                showToast(self, toastMsg, 2);
            });

            GlobalUserItem.lUserScore = self.m_LotteryRes.Score || GlobalUserItem.lUserScore;
            GlobalUserItem.dUserBeans = self.m_LotteryRes.Currency || GlobalUserItem.dUserBeans;
            GlobalUserItem.lUserIngot = self.m_LotteryRes.UserMedal || GlobalUserItem.lUserIngot;

            //通知更新
            var eventListener = cc.EventCustom.new(yl.RY_USERINFO_NOTIFY);
            eventListener.obj = yl.RY_MSG_USERWEALTH;
            cc.Director.getInstance().getEventDispatcher().dispatchEvent(eventListener);
        });
        var seq = cc.Sequence(easeRotate, call);
        self.m_spTable.stopAllActions();
        self.m_spTable.runAction(seq);
    },

    showPopWait : function() {
        self._scene.showPopWait();
    },

    dismissPopWait : function() {
        self._scene.dismissPopWait();
    }

});

// var testScene = cc.Scene.extend({
//     onEnter : function() {
//         this._super();
//         var layer = new TableLayer();
//         this.addChild(layer);
//     }
// });









