/*  Author  :   JIN */

// 分享到指定平台
var TargetShareLayer = cc.Layer.extend({
     TAG_MASK  : 101,
     TAG_MASK2 : 102,
    // 微信好友分享
     BT_WECHAT : 103,
    // 朋友圈分享
     BT_CYCLE : 104,
    // 我的好友分享
     BT_FRIEND : 105,

    ctor : function( callback ) {
        this._super();
        var self = this;
        this.m_shareCallBack = callback;
        // 加载csb资源
        var jsonNode = ExternalFun.loadRootCSB(res.TargetShareLayer_json, this);
        var rootLayer = jsonNode.rootlayer;
        var csbNode = jsonNode.csbnode;


        var touchFunC = function (ref, tType) {
            if (tType === ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };

        // 遮罩
        var mask = csbNode.getChildByName("panel_mask");
        mask.setTag(this.TAG_MASK);
        mask.addTouchEventListener(touchFunC);

        // 底板
        var panel = csbNode.getChildByName("panel_holder");
        this.m_panelHolder = panel;

        // 微信好友分享
        var btn = panel.getChildByName("btn_wechat");
        btn.setTag(this.BT_WECHAT);
        btn.addTouchEventListener(touchFunC);

        // 朋友圈分享
        btn = panel.getChildByName("btn_cycle");
        btn.setTag(this.BT_CYCLE);
        btn.addTouchEventListener(touchFunC);

        // 好友分享
        btn = panel.getChildByName("btn_friend");
        btn.setTag(this.BT_FRIEND);
        btn.addTouchEventListener(touchFunC);

        panel.stopAllActions();

        var call = new cc.CallFunc(
            ExternalFun.popupTouchFilter(1, false)
        );
        var move = new cc.MoveTo(0.2, cc.p(0, 0));
        var call2 = new cc.CallFunc(
            ExternalFun.dismissTouchFilter()
        );

        var seq = new cc.Sequence(call, move, call2);
        panel.runAction(seq);

    },

    onButtonClickedEvent : function(tag, ref) {
        var self = this;
        if (this.TAG_MASK == tag || this.TAG_MASK2 == tag) {
            this.hide();
        } else if (this.BT_WECHAT === tag) {
            this.hide(new cc.CallFunc(function () {
                if (typeof(self.m_shareCallBack) === "function") {
                    self.m_shareCallBack(yl.ThirdParty.WECHAT);
                }
            }));
        } else if (this.BT_CYCLE == tag) {
            this.hide(new cc.CallFunc(function () {
                if (typeof(self.m_shareCallBack) === "function") {
                    self.m_shareCallBack(yl.ThirdParty.WECHAT_CIRCLE);
                }
            }));
        } else if (this.BT_FRIEND == tag) {
            this.hide(new cc.CallFunc(function () {
                if (typeof(self.m_shareCallBack) === "function") {
                    self.m_shareCallBack(null, true);
                }
            }));
        }
    },

    hide : function( callfun ) {
        var self = this;
        this.m_panelHolder.stopAllActions();
        var call = new cc.CallFunc(function () {
            ExternalFun.popupTouchFilter(1, false);
        });
        var move = new cc.MoveTo(0.2, cc.p(0, -150));
        var call2 = new cc.CallFunc(function () {
            ExternalFun.dismissTouchFilter();
            self.removeFromParent(true);
        });
        if (null != callfun) {
            seq = new cc.Sequence(call, move, callfun, call2);
            this.m_panelHolder.runAction(seq);
        } else {
            seq = new cc.Sequence(call, move, call2);
            this.m_panelHolder.runAction(seq);
        }
    }

});

// var testScene = cc.Scene.extend({
//     onEnter : function() {
//         this._super();
//         var layer = new TargetShareLayer();
//         this.addChild(layer);
//     }
// });



