/*  Author  :   JIN */
var TaskLayer = cc.Layer.extend({

    onEnterTransitionDidFinish: function(){
        this.onEnterTransitionFinish();
    },

    onExitTransitionDidStart: function(){
        this.onExitTransitionStart();
    },

    onExit : function(){
        //todo
        if (this._taskFrame.isSocketServer()) {
            this._taskFrame.onCloseSocket();
        }
        if (null != this._taskFrame._gameFrame) {
            this._taskFrame._gameFrame._shotFrame = null;
            this._taskFrame._gameFrame = null;
        }
    },
    ctor : function(scene, gameFrame) {
        this._super(cc.Color(0,0,0,125));
        var self = this;
        this._scene = scene;
        var transLayer = new cc.LayerColor(cc.color(0, 0, 0, 125));
        transLayer.setContentSize(appdf.WIDTH, appdf.HEIGHT);
        this.addChild(transLayer);
        // this.registerScriptHandler(function (eventType) {
        //     if (eventType == "enterTransitionFinish") { 	// 进入场景而且过渡动画结束时候触发。
        //         this.onEnterTransitionFinish();
        //     } else if (eventType == "exitTransitionStart") { 	// 退出场景而且开始过渡动画时候触发。
        //         this.onExitTransitionStart();
        //     } else if (eventType == "exit") {
        //         if (this._taskFrame.isSocketServer()) {
        //             this._taskFrame.onCloseSocket();
        //         }
        //
        //         if (null != this._taskFrame._gameFrame) {
        //             this._taskFrame._gameFrame._shotFrame = null;
        //             this._taskFrame._gameFrame = null;
        //         }
        //     }
        // });

        //按钮回调
        this._btcallback = function (ref, type) {
            if (type == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref)
            }
        };

        //网络回调
        var taskCallBack = function(result, message) {
            return self.onTaskCallBack(result, message);
        };

        //网络处理
        this._taskFrame = new TaskFrame(this, taskCallBack);
        this._taskFrame._gameFrame = gameFrame;
        if (null != gameFrame) {
            gameFrame._shotFrame = this._taskFrame;
        }

        this._wTaskID = 0;
        this._wCommand = 0;


        var btn = new ccui.Button(res.btn_return_912_0_png, res.btn_return_912_1_png);
        btn.setPosition(1280, 710);
        this.addChild(btn);
      //  btn.setLocalZOrder(2);
        btn.addTouchEventListener(function (ref, type) {
            if (type == ccui.Widget.TOUCH_ENDED) {
                self._scene.onKeyBack();
            }
        });

        // 标头
        var sp = new cc.Sprite(res.header_912_png);
        sp.setPosition(665, 705);
        this.addChild(sp);

        //背景
        frame = cc.spriteFrameCache.getSpriteFrame("sp_public_frame_22.png");
        if (null != frame) {
            sp = new cc.Sprite(frame);
            sp.setPosition(yl.WIDTH / 2, 340);
            this.addChild(sp);
        }

        this._txtGold = new cc.LabelAtlas((GlobalUserItem.lUserScore + "").replace(".", "/"), res.num_task_0_png, 18, 27, "/");
        this._txtGold.setPosition(475, 710);
        this._txtGold.setAnchorPoint(cc.p(0.5, 0.5));
        this._txtGold.setVisible(true);
        this.addChild(this._txtGold);

        this._txtBean = new cc.LabelAtlas((GlobalUserItem.dUserBeans + "").replace(".", "/"), res.num_task_0_png, 18, 27, "/");
        this._txtBean.setPosition(775, 710);
        this._txtBean.setAnchorPoint(cc.p(0.5, 0.5));
        this._txtBean.setVisible(true);
        this.addChild(this._txtBean);

        this._txtIngot = new cc.LabelAtlas((GlobalUserItem.lUserIngot + "").replace(".", "/"), res.num_task_0_png, 18, 27, "/");
        this._txtIngot.setPosition(1100, 710);
        this._txtIngot.setAnchorPoint(cc.p(0.5, 0.5));
        this._txtIngot.setVisible(true);
        this.addChild(this._txtIngot);

        //游戏列表
        this._listView = new cc.TableView(this, cc.size(1145, 560));
        this._listView.setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL);
        this._listView.setPosition(cc.p(95, 70));
        this._listView.setDelegate(this);
        this.addChild(this._listView);
        this._listView.setVerticalFillOrder(cc.TABLEVIEW_FILL_TOPDOWN);
        // this._listView.registerScriptHandler(this.cellSizeForTable, cc.TABLECELL_SIZE_FOR_INDEX);
        // this._listView.registerScriptHandler(handler(this, this.tableCellAtIndex), cc.TABLECELL_SIZE_AT_INDEX);
        // this._listView.registerScriptHandler(handler(this, this.numberOfCellsInTableView), cc.NUMBER_OF_CELLS_IN_TABLEVIEW);

    },

    // 进入场景而且过渡动画结束时候触发。
    onEnterTransitionFinish : function() {
        this._scene.showPopWait();
        this._taskFrame.onTaskLoad();
        return this;
    },

    // 退出场景而且开始过渡动画时候触发。
    onExitTransitionStart : function() {
        return this;
    },

    //操作任务
    onHandleTask : function(wTaskID, cbTaskStatus, item) {
        this._scene.showPopWait();
        if (cbTaskStatus == yl.TASK_STATUS_WAIT) {
            this._wCommand = yl.SUB_GP_TASK_TAKE;
            this._taskFrame.onTaskTake(wTaskID);
        } else if (cbTaskStatus == yl.TASK_STATUS_UNFINISH || cbTaskStatus == yl.TASK_STATUS_FAILED) {
            this._wCommand = yl.SUB_GP_TASK_GIVEUP;
            this._taskFrame.onTaskGiveup(wTaskID);
        } else if (cbTaskStatus == yl.TASK_STATUS_SUCCESS) {
            this._wCommand = yl.TASK_STATUS_SUCCESS;
            this._taskFrame.onTaskReward(wTaskID);
        }
    },

    //按键监听
    onButtonClickedEvent : function(tag,sender) {
        if (tag == TaskLayer.BT_CELL) {
            var idx = sender.getParent().getIdx();
            var item = this._taskFrame.getTastList()[idx + 1];
            if (null != item) {
                this._wTaskID = item.wTaskID;
                this.onHandleTask(item.wTaskID, item.cbTaskStatus, item);
            }
        }
    },

    onTaskCallBack : function(result,message)
    {
        cc.log("======== TaskLayer.onTaskCallBack ========");
        var bRes = false;
        this._scene.dismissPopWait();
        if (message != null && message != "") {
            showToast(this, message, 2);
        }

        if (result == 1) {
            this._listView.reloadData();
        } else if (result == 2) {
            cc.log(" taskid " + this._wTaskID + " command " + this._wCommand);
            this._taskFrame.updateTask(this._wTaskID, this._wCommand);
            this._listView.reloadData();

            this._txtGold.setString(string_formatNumberThousands(GlobalUserItem.lUserScore, true, "/"));
            this._txtBean.setString(string_formatNumberThousands(GlobalUserItem.dUserBeans, true, "/"));
            this._txtBean.setString(string_formatNumberThousands(GlobalUserItem.dUserBeans, true, "/"));
            //bRes = true
        }
        return bRes;
    },

    ////////////////////////////////////////////////////////////////////-

    //子视图大小
    cellSizeForTable : function(view, idx) {
        return cc.size(1145, 110);
    },

    //子视图数目
    numberOfCellsInTableView : function(view) {
        return (this._taskFrame.getTastList()).length;
    },

    tableCellSizeForIndex:function (table, idx) {
        return cc.size(100, 110);
    },

    //获取子视图
    tableCellAtIndex : function(view, idx) {
        var cell = view.dequeueCell();
        var item = this._taskFrame.getTastList()[idx];

        if (!cell) {
            cell = new cc.TableViewCell();
        } else {
            cell.removeAllChildren();
        }
        if (null == item) {
            return cell;
        }

        this.createTaskItem(cell, item, view);
        cell.getChildByTag(3).setTexture("src/client/res/Task/icon_task_" + item.wTaskType + ".png");

        if (item.dwTimeLimit != 0) {
            var hour = Math.floor(item.dwTimeLimit / 3600);
            var minute = Math.floor((item.dwTimeLimit - hour * 3600) / 60);
            var second = item.dwTimeLimit - hour * 3600 - minute * 60;
            var str = "";

            if (0 != minute) {
                str = (str != "") && (str + ".") || str;
                str = str + minute + " 分钟";
            }

            cell.getChildByTag(8).setText("" + minute + "");
            cell.getChildByTag(8).setText(str);

        }
        // dump(item, "item", 7)

        //按钮处理
        var btn = cell.getChildByTag(TaskLayer.BT_CELL);
        if (null != btn) {
            btn.setEnabled(true);
            btn.setVisible(true);
            if (item.cbTaskStatus == yl.TASK_STATUS_UNFINISH) {
                btn.loadTextureNormal(res.bt_task_giveup_0_png);
                btn.loadTexturePressed(res.bt_task_giveup_1_png);
                btn.loadTextureDisabled(res.bt_task_giveup_0_png);
            } else if (item.cbTaskStatus == yl.TASK_STATUS_WAIT) {
                if (item.cbPlayerType == 2) {
                    btn.loadTextureNormal(res.bt_task_vip_take_0_png);
                    btn.loadTexturePressed(res.bt_task_vip_take_1_png);
                    btn.loadTextureDisabled(res.bt_task_vip_take_0_png);
                    btn.setVisible(false);
                } else {
                    btn.loadTextureNormal(res.bt_task_take_0_png);
                    btn.loadTexturePressed(res.bt_task_take_1_png);
                    btn.loadTextureDisabled(res.bt_task_take_0_png);
                }
            } else if (item.cbTaskStatus == yl.TASK_STATUS_FAILED) {
                btn.loadTextureNormal(res.bt_task_fail_0_png);
                btn.loadTexturePressed(res.bt_task_fail_1_png);
                btn.loadTextureDisabled(res.bt_task_fail_0_png);
//            btn.setEnabled(false)
            } else if (item.cbTaskStatus == yl.TASK_STATUS_SUCCESS) {
                btn.loadTextureNormal(res.bt_task_reward_0_png);
                btn.loadTexturePressed(res.bt_task_reward_1_png);
                btn.loadTextureDisabled(res.bt_task_reward_0_png);
            } else {
                btn.setVisible(false);
                btn.setEnabled(false);
            }
        }
        //进度条处理
        var cellChild6 = cell.getChildByTag(6);
        if (item.wTaskObject > 0) {
            var scalex = (item.wTaskProgress * 1.0) / (item.wTaskObject * 1.0);
            if (scalex > 1) {
                scalex = 1;
            }
            cellChild6.setTextureRect(cc.rect(0, 0, 218 * scalex, 20));
        } else {
            cellChild6.setTextureRect(cc.rect(0, 0, 1, 20));
        }

        return cell;
    },

    createTaskItem : function(cell, item, view) {
        var cy = 38;
        var cellwidth = 1000;
        //线
        var frame = new cc.Sprite(res.background_line_912_png);
        cell.addChild(frame);
        frame.setPosition(570, 40);

        // 任务图标
        var sp = new cc.Sprite(res.icon_task_0_png);
        sp.setPosition(130, 65);
        sp.setTag(3);
        sp.setVisible(true);
        cell.addChild(sp);

        //任务名称
        var cpName = new ClipText.createClipText(cc.size(700, 60), item.szTaskName, res.round_body_ttf, 27);
        cpName.setAnchorPoint(cc.p(0, 0.5));
        cpName.setPosition(210, 65);
        cpName.setTextColor(cc.Color(108, 97, 91, 255));
        cpName.setTag(5);
        cell.addChild(cpName);

        //任务进度
        var sp1 = new cc.Sprite(res.text_task_progress_png);
        cell.addChild(sp1);
        sp1.setPosition(160, 15);
        var sp2 = new cc.Sprite(res.frame_task_progress_0_png);
        cell.addChild(sp2);
        sp2.addChild(310, 15);
    //    进度条
        var sp3 = new cc.Sprite(res.frame_task_progress_1_png);
        sp3.setTextureRect(cc.rect(0, 0, 1, 20));
        sp3.setAnchorPoint(cc.p(0, 0.5));
        sp3.setPosition(220, 15);
        sp3.setTag(6);
        cell.addChild(sp3);
        //进度文字
        var lbl = new cc.LabelTTF("" + item.wTaskProgress + "/" + item.wTaskObject, res.round_body_ttf, 20);
        lbl.setAnchorPoint(cc.p(0.5, 0.5));
        lbl.setPosition(310, 15);
        lbl.setVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
        lbl.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        lbl.setWidth(100);
        lbl.setHeight(15);
        lbl.setTag(7);
        lbl.setLineBreakWithoutSpace(false);
        lbl.setTextColor(cc.Color(105, 53, 15, 255));
        cell.addChild(lbl);

        //时间限制
        var sp4 = new cc.Sprite(res.text_task_time_png);
        sp4.setPosition(570, 65);
        cell.addChild(sp4);
        //  new cc.LabelTTF("无时限", res.round_body_ttf, 24)
        lbl = new cc.LabelTTF("", res.round_body_ttf, 27);
        lbl.setAnchorPoint(cc.p(0, 0.5));
        lbl.setPosition(630, 65);
        lbl.setVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
        lbl.setHorizontalAlignment(cc.TEXT_ALIGNMENT_LEFT);
        lbl.setWidth(300);
        lbl.setHeight(30);
        lbl.setTag(8);
        lbl.setLineBreakWithoutSpace(false);
        lbl.setTextColor(cc.Color(108, 97, 91, 255));
        cell.addChild(lbl);

        //任务奖励
        var sp5 = new cc.Sprite(res.text_task_reward_png);
        sp5.setPosition(570, 15);
        cell.addChild(sp5);

        var sp6 = new cc.Sprite(res.coin_background_912_png);
        sp6.setPosition(760, 20);
        cell.addChild(sp6);

        var sp7 = new cc.Sprite(res.icon_task_gold_1_png);
        sp7.setPosition(630, 20);
        cell.addChild(sp7);
        //  cc.LabelAtlas._create(string_formatNumberThousands(item.lStandardAwardGold,true,"/"), "Task/num_task_1.png", 18, 25, string.byte("/"))
        var atlas = new cc.LabelAtlas(string_formatNumberThousands("" + item.lStandardAwardGold, true, "/"), res.num_task_1_png, 18, 25, "/");
        atlas.setPosition(670, 20).setAnchorPoint(cc.p(0, 0.5));
        cell.addChild(atlas);

        var sp8 = new cc.Sprite(res.icon_task_ingot_0_png);
        sp8.setPosition(780, 20);
        cell.addChild(sp8);
        //  cc.LabelAtlas._create(string_formatNumberThousands(item.lStandardAwardMedal,true,"/"), "Task/num_task_1.png", 18, 25, string.byte("/"))
        var atlas2 = new cc.LabelAtlas(string_formatNumberThousands("" + item.lStandardAwardGold, true, "/"), res.num_task_1_png, 18, 25, "/");
        atlas.setPosition(810, 20);
        atlas.setAnchorPoint(cc.p(0, 0.5));
        cell.addChild(atlas2);

        //操作按钮
        var btn = new ccui.Button(res.bt_task_take_0_png, res.bt_task_take_1_png, res.bt_task_take_0_png);
        btn.setPosition(1000, cy);
        btn.setTag(TaskLayer.BT_CELL);
        cell.addChild(btn);
        btn.addTouchEventListener(this._btcallback);
    }

});
TaskLayer.BT_EXIT = 3;
TaskLayer.BT_CELL = 15;


// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new TaskLayer();
//         this.addChild(layer);
//     }
// });
