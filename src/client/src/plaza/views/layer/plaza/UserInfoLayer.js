/*  Author  :   JIN */
//二维码界面
var QrCodeLayer = cc.Layer.extend({

    ctor : function() {
        this._super();
        var self = this;
        //注册触摸事件
        //ExternalFun.registerTouchEvent(this, true);

        //加载csb资源
        var result = ExternalFun.loadRootCSB(res.QrCodeLayer_json, this);
        var rootLayer = result.rootlayer;
        var csbNode = result.csbnode;
        this.m_spBg = csbNode.getChildByName("qr_sp_bg");
        var bgsize = this.m_spBg.getContentSize();

        var btnEvent = function (sender, eventType) {
            if (eventType == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(sender.getTag(), sender);
            }
        };

        this.m_qrContent = GlobalUserItem.szWXSpreaderURL || yl.HTTP_URL;
        // var qr = QrNode.createQrNode(this.m_qrContent, 500, 5, 1);
        // this.m_spBg.addChild(qr);
        // qr.setPosition(bgsize.width * 0.5, bgsize.height * 0.5);
        // this.m_qrNode = qr;
        // var head = HeadSprite.createClipHead(GlobalUserItem, 40);
        // head.enableHeadFrame(true);
        // this.m_qrNode.addChild(head);

        //分享
        var btn = this.m_spBg.getChildByName("btn_share");
        btn.setTag(QrCodeLayer.SHARE_QRCODE);
        btn.addTouchEventListener(btnEvent);
        //存储
        btn = this.m_spBg.getChildByName("btn_save");
        btn.setTag(QrCodeLayer.SAVE_QECODE);
        btn.addTouchEventListener(btnEvent);

        var framesize = cc.director.getOpenGLView().getFrameSize();
        var xRate = framesize.width / yl.WIDTH;
        var yRate = framesize.height / yl.HEIGHT;
        var screenPos = this.m_spBg.convertToWorldSpace(cc.p(bgsize.width * 0.5, bgsize.height * 0.5));
        var framePosX = (screenPos.x - 250) * xRate;
        var framePosY = (screenPos.y - 250) * yRate;
        var frameWidth = 500 * xRate;
        var frameHeight = 500 * yRate;
        this.m_qrFrameRect = new cc.rect(framePosX, framePosY, frameWidth, frameHeight);
        this.m_screenPos = screenPos;
        this.m_xRate = xRate;
        this.m_yRate = yRate;

        var customListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                cc.log("onTouchBegan");
                return self.isVisible();
            },
            onTouchMoved: function (touch, event) {
                cc.log("onTouchMoved");
                return false;
            },
            onTouchEnded : function (touch, event) {
                cc.log("onTouchEnded");
                var pos = touch.getLocation();
                var m_spBg = self.m_spBg;
                pos = m_spBg.convertToNodeSpace(pos);
                var rec = new cc.rect(0, 0, m_spBg.getContentSize().width, m_spBg.getContentSize().height);
                if (false == cc.rectContainsPoint(rec, pos)) {
                    self.removeFromParent();
                }
            }
        });
        cc.eventManager.addListener(customListener, this.m_spBg);
    },

    onButtonClickedEvent : function( tag, sender ) {
        ExternalFun.popupTouchFilter(0, false);
        if (tag == QrCodeLayer.SHARE_QRCODE) {
            captureScreenWithArea(this.m_qrFrameRect, "qr_code.png", function (ok, savepath) {
                ExternalFun.dismissTouchFilter();
                if (ok) {
                    MultiPlatform.getInstance().customShare(function (isok) {
                    }, "我的推广码", "分享我的推广码", this.m_qrContent, savepath, "true");
                }
            });
        } else if (tag == QrCodeLayer.SAVE_QECODE) {
            captureScreenWithArea(this.m_qrFrameRect, "qr_code.png", function (ok, savepath) {
                if (ok) {
                    if (true == MultiPlatform.getInstance().saveImgToSystemGallery(savepath, "qr_code.png")) {
                        showToast(this, "您的推广码二维码图片已保存至系统相册", 1);
                    }
                }
                this.runAction(new cc.Sequence(new cc.DelayTime(3), new cc.CallFunc(function () {
                    ExternalFun.dismissTouchFilter();
                })));
            });
        }
    },

    onExit : function() {
        this.stopAllActions();
    }
});

QrCodeLayer.SHARE_QRCODE = 1;
QrCodeLayer.SAVE_QECODE = 2;

var PromoterInputLayer = cc.Layer.extend({

    // 代理填写界面
    TAG_MASK : 101,
    BT_SURE : 102,
    BT_S_WECHAT : 103, 	//分享到微信
    BT_S_CYCLE : 104, 		//分享到朋友圈
    BT_S_CONTACT : 105, 	//分享到短信
    BT_COPY : 106, 		//复制粘贴板
    BT_QR_ENTER : 107, 	//点击显示二维码

    ctor : function( viewparent ) {
        this._super();
        var self = this;
        this.m_parent = viewparent;
        var noSpreader = GlobalUserItem.szSpreaderAccount == "";

        //加载csb资源
        var result = ExternalFun.loadRootCSB(res.PromoterInputLayer_json, this);
        var rootLayer = result.rootlayer;
        var csbNode = result.csbnode;
        this.m_spBg = csbNode.getChildByName("qr_sp_bg");
        this.m_spBg.setTouchEnabled(true);
        this.m_spBg.setSwallowTouches(true);

        var touchFunC = function (ref, tType) {
            if (tType == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };

        // 遮罩
        var mask = csbNode.getChildByName("panel_mask");
        mask.setTag(this.TAG_MASK);
        mask.addTouchEventListener(touchFunC);

        var editHanlder = function (event, editbox) {
            self.onEditEvent(event, editbox);
        };

        // 编辑框
        var tmp = this.m_spBg.getChildByName("sp_uinfoedit_bg");
 /*       //var editbox = new ccui.EditBox(cc.size(tmp.getContentSize().width - 10, 60), "blank.png", UI_TEX_TYPE_PLIST);
        var editbox = new ccui.EditBox(cc.size(tmp.getContentSize().width - 10, 60), ccui.Scale9Sprite("blank.png"));
        editbox.setPosition(tmp.getPosition());
        editbox.setFontName(res.round_body_ttf);
        editbox.setPlaceholderFontName(res.round_body_ttf);
        editbox.setPlaceHolder("请输入推广员ID");
        editbox.setMaxLength(32);
        editbox.setPlaceholderFontColor(cc.color(51, 45, 106));
        editbox.setFontColor(cc.color(51, 45, 106));
        editbox.setInputMode(cc.EDITBOX_INPUT_MODE_NUMERIC);
        this.m_spBg.addChild(editbox);
        editbox.setContentSize(cc.size(tmp.getContentSize().width - 10, tmp.getContentSize().height - 10));
        editbox.setPlaceholderFont(res.round_body_ttf, 30);
        editbox.setFont(res.round_body_ttf, 30);
        editbox.registerScriptEditBoxHandler(editHanlder);
        this.m_bEditChat = false;
        this.m_editID = editbox;
        editbox.setEnabled(noSpreader);
*/
        // 邀请奖励
        var atlas = this.m_spBg.getChildByName("atlas_sendcount");
        var count = GlobalUserItem.nInviteSend || 0;
        atlas.setString(count + "");

        // 确定按钮
        var btn = this.m_spBg.getChildByName("btn_sure");
        btn.setTag(this.BT_SURE);
        btn.addTouchEventListener(touchFunC);
        btn.setEnabled(noSpreader);
        if (!noSpreader) {
            editbox.setPlaceHolder(GlobalUserItem.szSpreaderAccount);
            btn.setOpacity(125);
        }

        // 关闭按钮
        btn = this.m_spBg.getChildByName("btn_close");
        btn.setTag(this.TAG_MASK);
        btn.addTouchEventListener(touchFunC);
        btn.setVisible(false);

        // 微信分享按钮
        btn = this.m_spBg.getChildByName("btn_wechat");
        btn.setTag(this.BT_S_WECHAT);
        btn.addTouchEventListener(touchFunC);

        // 朋友圈
        btn = this.m_spBg.getChildByName("btn_cycle");
        btn.setTag(this.BT_S_CYCLE);
        btn.addTouchEventListener(touchFunC);

        // 通讯录
        btn = this.m_spBg.getChildByName("btn_contact");
        btn.setTag(this.BT_S_CONTACT);
        btn.addTouchEventListener(touchFunC);

        // 复制
        btn = this.m_spBg.getChildByName("btn_copy");
        btn.setTag(this.BT_COPY);
        btn.addTouchEventListener(touchFunC);

        // 二维码
        btn = this.m_spBg.getChildByName("btn_qrcode");
        btn.setTag(this.BT_QR_ENTER);
        btn.addTouchEventListener(touchFunC);
        btn.setVisible(false);

        // this.m_qrContent = GlobalUserItem.szWXSpreaderURL || yl.HTTP_URL;
        // var qr = QrNode.createQrNode(this.m_qrContent, 184, 5, 1);
        // this.m_spBg.addChild(qr);
        // qr.setPosition(btn.getPosition());
        // this.m_qrNode = qr;
        // this.m_qrNode.setVisible(false);
    },

    onButtonClickedEvent : function( tag, sender ) {
        var self = this;
        if (this.BT_SURE == tag) {
            var txt = this.m_editID.string.replace(".", "");
            if (txt == "") {
                showToast(this, "推广员ID不能为空!", 1);
                return;
            }
            txt = Number(txt);
            if (null == txt) {
                showToast(this, "请输入合法的推广员ID!", 1);
                return;
            }
            this.m_parent.showPopWait();
            this.m_parent._modifyFrame.onBindSpreader(txt);
            this.runAction(new cc.Sequence(new cc.DelayTime(0.5), new cc.CallFunc(function () {
                self.removeFromParent();
            })));
        } else if (this.BT_S_WECHAT == tag) {
            var sharecall1 = function (isok) {
                if (typeof(isok) == "string" && isok == "true") {
                    showToast(self, "分享完成", 1);
                }
            };
            var url = GlobalUserItem.szWXSpreaderURL || yl.HTTP_URL;
            MultiPlatform.getInstance().shareToTarget(yl.ThirdParty.WECHAT, sharecall1, yl.SocialShare.title, yl.SocialShare.content, url);
        } else if (this.BT_S_CYCLE == tag) {
            var sharecall2 = function (isok) {
                if (typeof(isok) == "string" && isok == "true") {
                    showToast(this, "分享完成", 1);
                }
            };
            var url3 = GlobalUserItem.szWXSpreaderURL || yl.HTTP_URL;
            MultiPlatform.getInstance().shareToTarget(yl.ThirdParty.WECHAT_CIRCLE, sharecall2, yl.SocialShare.title, yl.SocialShare.content, url3);
        } else if (this.BT_S_CONTACT == tag) {
            var sharecall3 = function (isok) {
                if (typeof(isok) == "string" && isok == "true") {
                    showToast(this, "分享完成", 1);
                }
            };
            var url2 = GlobalUserItem.szSpreaderURL || yl.HTTP_URL;
            MultiPlatform.getInstance().shareToTarget(yl.ThirdParty.SMS, sharecall3, yl.SocialShare.title, url2);
        } else if (this.BT_COPY == tag) {
            var url1 = GlobalUserItem.szSpreaderURL || yl.HTTP_URL;
            var res, msg = MultiPlatform.getInstance().copyToClipboard(url1);
            if (true == res) {
                showToast(this, "复制到剪贴板成功!", 1);
            } else {
                if (typeof(msg) == "string") {
                    showToast(this, msg, 1, cc.color(250, 0, 0));
                }
            }
        } else if (this.BT_QR_ENTER == tag) {
            var qrlayer = new QrCodeLayer();
            this.m_parent.addChild(qrlayer);
            this.removeFromParent();
        } else if (this.TAG_MASK == tag) {
            if (false == this.m_bEditChat) {
                this.removeFromParent();
            }
            if (true == this.m_bEditChat) {
                this.m_bEditChat = false;
            }
        }
    },

    onEditEvent : function(event,editbox) {
        var src = editbox.string;
        if (event == "began") {
            this.m_bEditChat = src.length != 0;
        } else if (event == "changed") {
            this.m_bEditChat = src.length != 0;
        }
    }
});

//头像选择界面
var SelectHeadlayer = cc.Layer.extend({
    BTN_LOCAL : 101,
    BTN_SYS : 102,
    ctor : function(viewparent) {
        this._super();
        this.m_parent = viewparent;
        //注册触摸事件
        //ExternalFun.registerTouchEvent(this, true);
        var self = this;
        //加载csb资源
        var result = ExternalFun.loadRootCSB(res.SelectHeadLayer_json, this);
        var rootLayer = result.rootlayer;
        var csbNode = result.csbnode;

        var btnEvent = function (sender, eventType) {
            if (eventType == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(sender.getTag(), sender);
            }
        };
        //////
        //选择方式
        this.m_spSelectType = csbNode.getChildByName("select_type");
        //本地图片
        var btn = this.m_spSelectType.getChildByName("local_btn");
        btn.setTag(this.BTN_LOCAL);
        btn.addTouchEventListener(btnEvent);
        //系统头像
        btn = this.m_spSelectType.getChildByName("system_btn");
        btn.setTag(this.BTN_SYS);
        btn.addTouchEventListener(btnEvent);
        //////

        //////
        //系统头像列表
        this.m_spSysSelect = csbNode.getChildByName("head_list");
        this.m_spSysSelect.setVisible(false);
        this.m_tableView = null;
        this.m_tabSystemHead = {};
        this.m_vecTouchBegin = { x : 0, y : 0};
        ////////////////////////////////
        var customListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                self.m_vecTouchBegin = {x : touch.getLocation().x, y : touch.getLocation().y};
                return self.isVisible();
            },
            onTouchEnded : function (touch, event) {
                var pos = touch.getLocation();
                if (Math.abs(pos.x - self.m_vecTouchBegin.x) > 30|| Math.abs(pos.y - self.m_vecTouchBegin.y) > 30) {
                    self.m_vecTouchBegin = {x : 0, y : 0};
                    return;
                }
                var m_spBg = null;
                if (self.m_spSelectType.isVisible()) {
                    m_spBg = self.m_spSelectType;
                } else if (self.m_spSysSelect.isVisible()) {
                    m_spBg = self.m_spSysSelect;

                    var touchHead = false;
                    var touchSp = null;
                    for (i in self.m_tabSystemHead) {
                        var tmppos = self.m_tabSystemHead[i].convertToNodeSpace(pos);
                        var headRect = new cc.rect(0, 0, self.m_tabSystemHead[i].getContentSize().width, self.m_tabSystemHead[i].getContentSize().height);
                        if (true == cc.rectContainsPoint(headRect, tmppos)) {
                            touchHead = true;
                            touchSp = self.m_tabSystemHead[i];
                            break;
                        }
                    }

                    if (true == touchHead && null != touchSp) {
                        var tag = touchSp.getTag();
                        cc.log("touch head " + tag);
                        if (null != self.m_parent && null != self.m_parent.sendModifySystemFace) {
                            self.m_parent.sendModifySystemFace(tag);
                        }
                        return;
                    }
                }
                if (null == m_spBg) {
                    self.removeFromParent();
                    return;
                }

                pos = m_spBg.convertToNodeSpace(pos);
                var rec = cc.rect(0, 0, m_spBg.getContentSize().width, m_spBg.getContentSize().height);
                if (false == cc.rectContainsPoint(rec, pos)) {
                    self.removeFromParent();
                }
            }
        });
        cc.eventManager.addListener(customListener, this.m_spSelectType);
    },

    refreshSystemHeadList : function() {
        if (null == this.m_tableView) {
            var content = this.m_spSysSelect.getChildByName("content");
            var m_tableView = new cc.TableView(this, content.getContentSize());
            m_tableView.setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL);
            m_tableView.setPosition(content.getPosition());
            m_tableView.setDelegate(this);
            // m_tableView.registerScriptHandler(this.cellSizeForTable, cc.TABLECELL_SIZE_FOR_INDEX);
            // m_tableView.registerScriptHandler(handler(this, this.tableCellAtIndex), cc.TABLECELL_SIZE_AT_INDEX);
            // m_tableView.registerScriptHandler(this.numberOfCellsInTableView, cc.NUMBER_OF_CELLS_IN_TABLEVIEW);
            this.m_spSysSelect.addChild(m_tableView);
            this.m_tableView = m_tableView;
            content.removeFromParent();
            this.m_tableView.reloadData();
        }
    },

    onButtonClickedEvent : function( tag, sender ) {
        this.m_spSelectType.setVisible(false);
        var self = this;
        if (this.BTN_LOCAL == tag) {
            var callback = function (param) {
                if (typeof(param) == "string") {
                    cc.log("lua call back " + param);
                    if (cc.FileUtils.getInstance().isFileExist(param)) {
                        //发送上传头像
                        var url = yl.HTTP_URL + "/WS/Account.ashx?action=uploadface";
                        var uploader = CurlAsset.createUploader(url, param);
                        if (null == uploader) {
                            showToast(self, "自定义头像上传异常", 2);
                            return;
                        }
                        var nres = uploader.addToFileForm("file", param, "image/png");
                        //用户标示
                        nres = uploader.addToForm("userID", GlobalUserItem.dwUserID);
                        //登陆时间差
                        var delta = Number(currentTime()) - Number(GlobalUserItem.LogonTime);
                        cc.log("time delta " + delta);
                        nres = uploader.addToForm("time", delta + "");
                        //客户端ip
                        var ip = MultiPlatform.getInstance().getClientIpAdress() || "192.168.1.1";
                        nres = uploader.addToForm("clientIP", ip);
                        //机器码
                        var machine = GlobalUserItem.szMachine || "A501164B366ECFC9E249163873094D50";
                        nres = uploader.addToForm("machineID", machine);
                        //会话签名
                        nres = uploader.addToForm("signature", GlobalUserItem.getSignature(delta));
                        if (0 != nres) {
                            showToast(self, "上传表单提交异常,error code ==> " + nres, 2);
                            return;
                        }
                        self.m_parent.showPopWait();
                        uploader.uploadFile(function (sender, ncode, msg) {
                            self.onUploadFaceResult(sender, ncode, msg, param);
                        });
                    }
                }
            };
            MultiPlatform.getInstance().triggerPickImg(callback, true);
        } else if (this.BTN_SYS == tag) {
            this.m_spSysSelect.setVisible(true);
            this.refreshSystemHeadList();
        }
    },

    onUploadFaceResult : function(sender, ncode, msg, param) {
        this.m_parent.dismissPopWait();
        if (0 == ncode) {
            if (typeof(msg) == "string") {
                cc.log("msg ==> " + msg);
            }
            try{
                datatable = JSON.parse(msg);
                //dump(datatable, "datatable")
                if (null != datatable.code && 0 == datatable.code) {
                    var msgdata = datatable.data;
                    // dump(msgdata, "msgdata")
                    if (null != msgdata && msgdata != null) {
                        var valid = msgdata.valid;
                        if (valid) {
                            cc.director().getTextureCache().removeTextureForKey(param);
                            var sp = cc.Sprite.create(param);
                            if (null != sp) {
                                GlobalUserItem.dwCustomID = Number(msgdata.CustomID);
                                var frame = sp.getSpriteFrame();
                                var framename = GlobalUserItem.dwUserID + "_custom_" + GlobalUserItem.dwCustomID + ".ry";
                                var oldframe = cc.spriteFrameCache.getSpriteFrame(framename);
                                if (null != oldframe) {
                                    oldframe.release();
                                }
                                cc.spriteFrameCache.removeSpriteFrameByName(framename);

                                cc.spriteFrameCache.addSpriteFrame(frame, framename);
                                frame.retain();

                                if (null != this.m_parent && null != this.m_parent.onFaceResultSuccess) {
                                    this.m_parent.onFaceResultSuccess();
                                }
                                showToast(this, "自定义头像上传成功", 2);
                            }
                        } else {
                            if (typeof(msg.msg) == "string") {
                                showToast(this, msg.msg, 2);
                            }
                        }
                    }
                }
            } catch(e){
                showToast(this, "自定义头像上传异常", 2);
            }
            return;
        }
        showToast(this, "自定义头像上传异常, error code ==> " + ncode, 2);
    },

    //tableview
    cellSizeForTable : function( view, idx ) {
        return [940, 95];
    },

    tableCellSizeForIndex:function (table, idx) {
        return cc.size(100, 95);
    },

    numberOfCellsInTableView : function( view ) {
        //一行10个，200个
        return 20;
    },

    tableCellAtIndex : function( view, idx ) {
        var cell = view.dequeueCell();
        idx = 19 - idx;


        if (null == cell) {
            cell = new cc.TableViewCell();
            var item1 = this.groupSysHead(idx, view);
            item1.setPosition(view.getViewSize().width * 0.5, 0);
            item1.setName("head_item_view");
            item1.setTag(idx);
            cell.addChild(item1);
        } else {
            var item = cell.getChildByName("head_item_view");
            item.setTag(idx);
            this.updateCellItem(item, idx, view);
        }

        return cell;
    },

    //var xTable : [-450];
    groupSysHead : function( idx, view ) {
        var item = new cc.Node();
        var xStart = -450;
        var str = "";
        var head = null;
        var frame = null;
        var tag = 0;
        for (i = 0; i <= 9; i++) {
            head = null;
            tag = idx * 10 + i;
            str = sprintf("Avatar%d.png", tag);
            frame = cc.spriteFrameCache.getSpriteFrame(str);
            if (null != frame) {
                head = new cc.Sprite(frame);
            }

            if (null != head) {
                item.addChild(head);
                head.setTag(tag);
                var xPos = xStart + i * 100;
                head.setPosition(xPos, 45);
                head.setScale(80 / 96);
                head.setName("head_" + i);

                this.m_tabSystemHead[tag] = head;

                frame = null;
                if (tag == GlobalUserItem.wFaceID) {
                    frame = cc.spriteFrameCache.getSpriteFrame("sp_select_bg.png");
                } else {
                    frame = cc.spriteFrameCache.getSpriteFrame("sp_normal_frame.png");
                }
                if (null != frame) {
                    var spFrame = new cc.Sprite(frame);
                    spFrame.setPosition(xPos, 45);
                    item.addChild(spFrame, -1);
                    spFrame.setName("frame_" + i);
                }
            }
        }
        return item;
    },

    updateCellItem : function( item, idx, view ){
        var tag = 0;
        var frame = null;
        var str = "";
        for (var i = 0; i <= 9; i++) {
            head = null;
            tag = idx * 10 + i;
            frame = null;
            str = "";

            var head = item.getChildByName("head_" + i);
            if (null != head) {
                str = sprintf("Avatar%d.png", tag);
                frame = cc.spriteFrameCache.getSpriteFrame(str);
                if (null != frame) {
                    head.setSpriteFrame(frame);
                    head.setTag(tag);

                    this.m_tabSystemHead[tag] = head;
                }
            }

            var spFrame = item.getChildByName("frame_" + i);
            if (null != spFrame) {
                frame = null;
                if (tag == GlobalUserItem.wFaceID) {
                    frame = cc.spriteFrameCache.getSpriteFrame("sp_select_bg.png")
                } else {
                    frame = cc.spriteFrameCache.getSpriteFrame("sp_normal_frame.png")
                }

                if (null != frame) {
                    spFrame.setSpriteFrame(frame);
                }
            }
        }
    }

});

//个人信息界面
var bGender = false;
var UserInfoLayer = cc.Layer.extend({
    
    ctor : function(scene) {
        this._super();
        var transLayer = new cc.LayerColor(cc.color(0, 0, 0, 125));
        transLayer.setContentSize(appdf.WIDTH, appdf.HEIGHT);
        this.addChild(transLayer);
        //ExternalFun.registerNodeEvent(this);
        this._scene = scene;
        var self = this;
        this.setContentSize(yl.WIDTH, yl.HEIGHT);
    
        var btcallback = function (ref, type) {
            if (type == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };
    
        var cbtlistener = function (sender, eventType) {
            self.onSelectedEvent(sender.getTag(), sender, eventType);
        };
    
        this._bankNotify = function () {
            self._txtScore.setString(string_formatNumberThousands(GlobalUserItem.lUserScore));
        };
    
        //网络回调
        var modifyCallBack = function (result, message) {
            self.onModifyCallBack(result, message);
        };
        //网络处理
       this._modifyFrame = new ModifyFrame(this, modifyCallBack);
    
        bGender = (GlobalUserItem.cbGender == yl.GENDER_MANKIND && true || false);
    
        //上方背景
        var frame = cc.spriteFrameCache.getSpriteFrame("sp_top_bg.png");

    
        //下方背景
        frame = cc.spriteFrameCache.getSpriteFrame("sp_public_frame_3.png");

    
        //信息背景
        var _sp = new cc.Sprite(res.info_frame_1_png);
        _sp.setPosition(670, 380);
        this.addChild(_sp);

        var btn = new ccui.Button(res.bt_return_0_info_png, res.bt_return_1_info_png);
        btn.setPosition(yl.WIDTH * 0.93, yl.HEIGHT * 0.86);
        btn.setTag(UserInfoLayer.BT_EXIT);
        this.addChild(btn,3);
        btn.addTouchEventListener(btcallback);

        var head = new HeadSprite().createNormal(GlobalUserItem, 92);
        head.setAnchorPoint(cc.p(0, 1));
        head.setPosition(211, 578);
        head.enableHeadFrame(false);
        head.registerInfoPop(true, function () {
            self.onClickUserHead();
        });
        this.addChild(head);
        this._head = head;
    
        sp = new cc.Sprite(res.head_info_frame_png);
        sp.setPosition(260, 525);
        this.addChild(sp);
    
        //ID
        var sp = new cc.Sprite(res.text_info_id_png);
        sp.setPosition(228, 450);
        this.addChild(sp);
        var textId = sprintf("%d", GlobalUserItem.dwGameID);
        this._txtID = new cc.LabelTTF(textId, res.round_body_ttf, 30);
        this._txtID.setColor(cc.color(108, 97, 91, 255));
        this._txtID.setAnchorPoint(cc.p(0, 0.5));
        this._txtID.setDimensions(345, 60);
        this._txtID.setVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
        this._txtID.setHorizontalAlignment(cc.TEXT_ALIGNMENT_LEFT);
        this._txtID.setPosition(250, 450);
        this.addChild(this._txtID);
    
        //等级
        sp = new cc.Sprite(res.icon_info_level_png);
        sp.setPosition(230, 410);
        this.addChild(sp);
        sp = new cc.Sprite(res.text_info_level_png);
        sp.setPosition(280, 410);
        this.addChild(sp);

        this._level = new cc.LabelAtlas(GlobalUserItem.wCurrLevelID + "", res.num_info_level_png, 17, 23, "0");
        this._level.setPosition(300, 410);
        this._level.setAnchorPoint(cc.p(0, 0.5));
        this.addChild(this._level);
        this._level.setVisible(true);
    
        //进度条
        sp = new cc.Sprite(res.frame_info_level_png);
        sp.setPosition(270, 370);
        this.addChild(sp);
        sp.setVisible(true);
        //进度条
        this._levelpro = new cc.Sprite(res.progress_info_level_png);
        this._levelpro.setAnchorPoint(cc.p(0, 0.5));
        this._levelpro.setPosition(165, 373);
        this.addChild(this._levelpro);
        this._levelpro.setVisible(true);
    
        //等级信息
        sp = new cc.Sprite(res.info_frame_4_png);
        sp.setPosition(270, 280);
        this.addChild(sp);
        sp.setVisible(true);
    
        if (GlobalUserItem.dwUpgradeExperience > 0) {
            var scalex = GlobalUserItem.dwExperience / GlobalUserItem.dwUpgradeExperience;
            if (scalex > 1) {
                scalex = 1;
            }
            this._levelpro.setTextureRect(cc.rect(0, 0, 218 * scalex, 27));
        } else {
            this._levelpro.setTextureRect(cc.rect(0, 0, 1, 27));
        }
    
        var nextexp = GlobalUserItem.dwUpgradeExperience - GlobalUserItem.dwExperience;
        nextexp = (nextexp < 0) && 0 || nextexp;
        var lbl = new cc.LabelTTF("下次升级还需要" + (nextexp) + "经验", res.round_body_ttf, 18);
        lbl.setPosition(265, 305);
        lbl.setColor(cc.color(104, 2, 2));
        this.addChild(lbl);
        lbl.setVisible(true);
    
        this._txticon = new cc.LabelAtlas(string_formatNumberThousands(GlobalUserItem.lUpgradeRewardGold, true, "/"), res.reward_number_png, 10, 13, ".");
        this._txticon.setAnchorPoint(cc.p(0, 0.5));
        this._txticon.setPosition(280, 275);
        this.addChild(this._txticon);
        sp = new cc.Sprite(res.icon_info_up_png);
        sp.setPosition(200, 255);
        this.addChild(sp);
        sp.setVisible(true);
    
        sp = new cc.Sprite(res.sp_coin_lv_png);
        sp.setPosition(20 + this._txticon.getContentSize().width + 280, 275);
        this.addChild(sp);
        sp.setVisible(true);
    
        this._txtgold = new cc.LabelAtlas(string_formatNumberThousands(GlobalUserItem.lUpgradeRewardIngot, true, "/"), res.reward_number_png, 10, 13, ".");
        this._txtgold.setAnchorPoint(cc.p(0, 0.5));
        this._txtgold.setPosition(280, 245);
        this.addChild(this._txtgold);
        sp = new cc.Sprite(res.sp_gold_lv_png);
        sp.setPosition(20 + this._txtgold.getContentSize().width + 280, 245);
        //	.move(340,245)
        this.addChild(sp);
        sp.setVisible(true);
    
        sp = new cc.Sprite(res.sp_jiahao_png);
        sp.setPosition(270, 245);
        this.addChild(sp);
        sp.setVisible(true);
    
        sp = new cc.Sprite(res.sp_jiahao_png);
        sp.setPosition(270, 275);
        this.addChild(sp);
        sp.setVisible(true);
    
        sp = new cc.Sprite(res.sp_reward_png);
        sp.setPosition(240, 275);
        this.addChild(sp);
        sp.setVisible(true);
    
        var gold = GlobalUserItem.lUpgradeRewardGold;
        var szgold;
        if (gold > 9999) {
            szgold = sprintf("%0.2f万", gold / 10000);
        } else {
            szgold = gold + "";
        }
        lbl = new cc.LabelTTF("奖励：", res.round_body_ttf, 18);
        lbl.setPosition(210 - 50, 115 + 40);
        lbl.setColor(cc.color(250, 250, 0));
        this.addChild(lbl);
        lbl.setVisible(false);
        lbl = new cc.LabelTTF("+ " + szgold + " 游戏币", res.round_body_ttf, 18);
        lbl.setPosition(165 + 20, 115 + 40);
        lbl.setAnchorPoint(cc.p(0, 0.5));
        lbl.setColor(cc.color(250, 250, 0));
        this.addChild(lbl);
        lbl.setVisible(false);
        lbl = new cc.LabelTTF("+ " + GlobalUserItem.lUpgradeRewardIngot + " 元宝", res.round_body_ttf, 18);
        lbl.setPosition(165 + 20, 120 + 10);
        lbl.setAnchorPoint(cc.p(0, 0.5));
        lbl.setColor(cc.color(250, 250, 0));
        this.addChild(lbl);
        lbl.setVisible(false);
    
        this._txtScore = new cc.LabelAtlas((GlobalUserItem.lUserScore + "").replace(".", "/"), res.number_money_png, 17, 23, "/");
        this._txtScore.setAnchorPoint(cc.p(0.5, 0.5));
        this._txtScore.setPosition(700, 286);
        this.addChild(this._txtScore);
    
        this._txtBean = new cc.LabelAtlas((GlobalUserItem.dUserBeans + "").replace(".", "/"), res.number_money_png, 17, 23, "/");
        this._txtBean.setAnchorPoint(cc.p(0.5, 0.5));
        this._txtBean.setPosition(700, 220);
        this.addChild(this._txtBean);
        this._txtIngot = new cc.LabelAtlas((GlobalUserItem.lUserIngot + "").replace(".", "/"), res.number_money_png, 17, 23, "/");
        this._txtIngot.setAnchorPoint(cc.p(0.5, 0.5));
        this._txtIngot.setPosition(700, 160);
        this.addChild(this._txtIngot);
    
        var account = GlobalUserItem.szAccount;
        //微信登陆显示昵称
        if(GlobalUserItem.bWeChat) {
            account = GlobalUserItem.szNickName;
        }
        var accountClip = new ClipText(cc.size(200, 30), account, res.round_body_ttf, 22);
        accountClip.setTextColor(cc.color(108, 97, 91, 255));
        accountClip.setAnchorPoint(cc.p(0, 0.5));
        accountClip.setPosition(605, 548);
        this.addChild(accountClip);
    
        if (GlobalUserItem.bVisitor == true && false == GlobalUserItem.getBindingAccount()) {

        }
    
        var vip = GlobalUserItem.cbMemberOrder || 0;
        if (0 == vip) {
            //开通VIP
            btn = new ccui.Button(res.bt_info_vip_0_png, res.bt_info_vip_1_png);
            btn.setPosition(1064, 545);
            btn.setTag(UserInfoLayer.BT_VIP);
            this.addChild(btn);
            btn.addTouchEventListener(btcallback);
        } else {

        }

        if (GlobalUserItem.bIsAngentAccount) {

        }

        //性别选择
        this._cbtMan = new ccui.CheckBox(res.checkbox_info_0_png, res.checkbox_info_1_png);
        this._cbtMan.setPosition(650, 485);
        this._cbtMan.setSelected(bGender);
        this.addChild(this._cbtMan);
        this._cbtMan.setTag(UserInfoLayer.CBT_MAN);
        this._cbtMan.addEventListener(cbtlistener);
    
        this._cbtWoman = new ccui.CheckBox(res.checkbox_info_2_png, res.checkbox_info_3_png);
        this._cbtWoman.setPosition(750, 485);
        this._cbtWoman.setSelected(!bGender);
        this.addChild(this._cbtWoman);
        this._cbtWoman.setTag(UserInfoLayer.CBT_WOMAN);
        this._cbtWoman.addEventListener(cbtlistener);
    

        sp = new cc.Sprite(res.info_frame_3_png);
        sp.setPosition(910, 150);
        this.addChild(sp);
        sp.setVisible(false);
    
        var editHanlder = function (name, sender) {
            self.onEditEvent(name, sender);
        };

        sp = new cc.Sprite(res.nickname_kuang_png);
        sp.setPosition(720, 417);
        this.addChild(sp);
        sp.setVisible(true);
    
        sp = new cc.Sprite(res.person_kuang_png);
        sp.setPosition(835, 350);
        this.addChild(sp);
        sp.setVisible(true);

         //输入框
        this.edit_Nickname = new cc.EditBox(cc.size(300, 40), ccui.Scale9Sprite(res.nickname_kuang_png));
        this.edit_Nickname.setPosition(610, 417);
        this.edit_Nickname.setAnchorPoint(cc.p(0, 0.5));
        this.edit_Nickname.setFontName(res.round_body_ttf);
        this.edit_Nickname.setPlaceholderFontName(res.round_body_ttf);
        this.edit_Nickname.setFontSize(22);
        this.edit_Nickname.setPlaceholderFontSize(24);
        this.edit_Nickname.setMaxLength(32);
        this.edit_Nickname.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        this.edit_Nickname.setPlaceHolder("请输入您的游戏昵称");
        this.edit_Nickname.setFontColor(cc.color(254, 248, 229, 255));
        this.edit_Nickname.setString(GlobalUserItem.szNickName);
        this.addChild(this.edit_Nickname);
        this.edit_Nickname.setDelegate(this);
        this.edit_Nickname.setName("edit_nickname");
        this.m_szNick = GlobalUserItem.szNickName;

        this.edit_Sign = new cc.EditBox(cc.size(600, 40), ccui.Scale9Sprite(res.person_kuang_png));
        this.edit_Sign.setPosition(910, 350);
        this.edit_Sign.setAnchorPoint(cc.p(0.5, 0.5));
        this.edit_Sign.setFontName(res.round_body_ttf);
        this.edit_Sign.setPlaceholderFontName(res.round_body_ttf);
        this.edit_Sign.setFontSize(22);
        this.edit_Sign.setPlaceholderFontSize(24);
        this.edit_Sign.setMaxLength(32);
        this.edit_Sign.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        this.edit_Sign.setPlaceHolder("请输入您的个人签名");
        this.edit_Sign.setFontColor(cc.color(254, 248, 229, 255));
        this.edit_Sign.setString(GlobalUserItem.szSign);
        this.addChild(this.edit_Sign);
        this.edit_Sign.setDelegate(this);
        this.edit_Sign.setName("edit_sign");
        this.m_szSign = GlobalUserItem.szSign;

        //金币充值
        btn = new ccui.Button(res.bt_info_recharge_0_png, res.bt_info_recharge_1_png);
        btn.setPosition(1064, 286);
        btn.setTag(UserInfoLayer.BT_RECHARGE);
        this.addChild(btn);
        btn.addTouchEventListener(btcallback);
    
        //钻石充值
        btn = new ccui.Button(res.bt_info_recharge_0_png, res.bt_info_recharge_1_png);
        btn.setPosition(1064, 220);
        btn.setTag(UserInfoLayer.BT_RECHARGE);
        this.addChild(btn);
        btn.addTouchEventListener(btcallback);
    
        //元宝兑换
        btn = new ccui.Button(res.bt_info_exchange_0_png, res.bt_info_exchange_1_png);
        btn.setPosition(1064, 160);
        btn.setTag(UserInfoLayer.BT_EXCHANGE);
        this.addChild(btn);
        btn.addTouchEventListener(btcallback);

    },

    onButtonClickedEvent : function(tag,ref){
        if ( tag == UserInfoLayer.BT_EXIT) {
            this._scene.onKeyBack();
        } else if ( tag == UserInfoLayer.BT_TAKE) {
            if ( GlobalUserItem.isAngentAccount()) {
                return;
            }
            this._scene.onChangeShowMode(yl.SCENE_BANK);
        } else if ( tag == UserInfoLayer.BT_AGENT) {
            this._scene.onChangeShowMode(yl.SCENE_AGENT);
        } else if ( tag == UserInfoLayer.BT_BINDING) {
            if ( GlobalUserItem.isAngentAccount()) {
                return;
            }
            this.getParent().getParent().onChangeShowMode(yl.SCENE_BINDING);
        } else if ( tag == UserInfoLayer.BT_CONFIRM) {
            if (GlobalUserItem.isAngentAccount()) {
                return;
            }
    
            var szNickname = this.edit_Nickname.string.replace(" ", "");
    
            //判断昵称长度
            if (szNickname.length < 6) {
                showToast(this, "游戏昵称必须大于6位以上,请重新输入!", 2);
            }
    
            //判断是否有非法字符
            if ( true == ExternalFun.isContainBadWords(szNickname)) {
                showToast(this, "昵称中包含敏感字符,请重试", 2);
                return;
            }
    
            var szSign = this.edit_Sign.replace(" ","");
    
            //判断是否有非法字符
            if ( true == ExternalFun.isContainBadWords(szSign)) {
                showToast(this, "个性签名中包含敏感字符,请重试", 2);
                return;
            }
    
            if ( szNickname != GlobalUserItem.szNickName || szSign != GlobalUserItem.szSign) {
                this.showPopWait();
                this._modifyFrame.onModifyUserInfo(GlobalUserItem.cbGender, szNickname, szSign);
            }
        } else if ( tag == UserInfoLayer.BT_VIP) {
            if ( GlobalUserItem.isAngentAccount()) {
                return;
            }
    
            this.getParent().getParent().onChangeShowMode(yl.SCENE_SHOP, ShopLayer.CBT_VIP);
        } else if ( tag == UserInfoLayer.BT_RECHARGE) {
            if ( GlobalUserItem.isAngentAccount()) {
                return;
            }
    
            this._scene.onChangeShowMode(yl.SCENE_SHOP, ShopLayer.CBT_BEAN);
        } else if ( tag == UserInfoLayer.BT_EXCHANGE) {
            if ( GlobalUserItem.isAngentAccount()) {
                return;
            }
    
            this._scene.onChangeShowMode(yl.SCENE_SHOP, ShopLayer.CBT_ENTITY);
        } else if ( tag == UserInfoLayer.BT_BAG) {
            if ( GlobalUserItem.isAngentAccount()) {
                return;
            }
    
            this._scene.onChangeShowMode(yl.SCENE_BAG);
        } else if ( tag == UserInfoLayer.BT_QRCODE) {
            //var qrlayer = QrCodeLayer.create(this)
            //this.addChild(qrlayer)
            var prolayer = PromoterInputLayer.create(this);
            this.addChild(prolayer);
        } else if ( tag == UserInfoLayer.BT_PROMOTER) {
            //var prolayer = PromoterInputLayer.create(this)
            //this.addChild(prolayer)
        } else if ( tag == UserInfoLayer.BT_SAFE) {
            //安全按钮
        }
    },
    
    onClickUserHead : function() {
        cc.log("onClickUserHead");
        if (GlobalUserItem.isAngentAccount()) {
            return;
        }
        if (GlobalUserItem.notEditAble()) {
            return;
        }
    
        var tmp = new SelectHeadlayer(this);
        tmp.setTag(UserInfoLayer.LAY_SELHEAD);
        this.addChild(tmp);
    },
    
    onSelectedEvent : function(tag,sender,eventType) {
        if (GlobalUserItem.isAngentAccount()) {
            sender.setSelected(!sender.isSelected());
            return;
        }
        if (GlobalUserItem.notEditAble()) {
            sender.setSelected(!sender.isSelected());
            return;
        }
    
        var szNickname = this.edit_Nickname.string.replace(" ", "");
        var szSign = this.edit_Sign.string.replace(" ", "");
        var cbGenderMale = yl.GENDER_MANKIND;
        var cbGenderFeMale = yl.GENDER_FEMALE;
        if (tag == UserInfoLayer.CBT_MAN) {                             //if male checkbox is selected
            if (GlobalUserItem.cbGender == yl.GENDER_MANKIND) {         //if the user was already male
                // sender.setSelected(!sender.isSelected());
                this.showPopWait();
                this._modifyFrame.onModifyUserInfo(cbGenderFeMale, szNickname, szSign);
                this._cbtMan.selected = false;
                this._cbtWoman.selected = true;
            }else{
            // if (bGender != true) {                                      //if the user was female
                this.showPopWait();
                this._modifyFrame.onModifyUserInfo(cbGenderMale, szNickname, szSign);
                this._cbtWoman.selected = false;
                this._cbtMan.selected = true;
            }
        }
    
        if (tag == UserInfoLayer.CBT_WOMAN) {                           //if male checkbox is selected
            if (GlobalUserItem.cbGender == yl.GENDER_FEMALE) {
                // sender.setSelected(!sender.isSelected());
                this.showPopWait();
                this._modifyFrame.onModifyUserInfo(cbGenderMale, szNickname, szSign);
                this._cbtWoman.selected = false;
                this._cbtMan.selected = true;
            }else{
            // if (bGender != false) {                             //if the user was male
                this.showPopWait();
                this._modifyFrame.onModifyUserInfo(cbGenderFeMale, szNickname, szSign);
                this._cbtMan.selected = false;
                this._cbtWoman.selected = true;
            }
        }
    },

    editBoxEditingDidBegin: function (editbox) {
        cc.log("editBox  DidBegin !");
    },

    editBoxEditingDidEnd: function (editbox) {
        cc.log("editBox  DidEnd !"+editbox.getName());

        if ("edit_sign" == editbox.getName()) {
            if (GlobalUserItem.isAngentAccount()) {
                this.edit_Sign.setText(GlobalUserItem.szSign);
                return;
            }
            if (GlobalUserItem.notEditAble()) {
                this.edit_Sign.setText(GlobalUserItem.szSign);
                return;
            }

            var szSign = this.edit_Sign.string.replace(" ", "");
            //判断长度
            if (szSign.length < 1) {
                showToast(this, "个性签名不能为空", 2);
                this.edit_Sign.setText(GlobalUserItem.szSign);
                return;
            }

            //判断emoji
            if (ExternalFun.isContainEmoji(szSign)) {
                showToast(this, "个性签名中包含非法字符,请重试", 2);
                this.edit_Sign.setText(GlobalUserItem.szSign);
                return;
            }

            //判断是否有非法字符
            if (true == ExternalFun.isContainBadWords(szSign)) {
                showToast(this, "个性签名中包含敏感字符,请重试", 2);
                this.edit_Sign.setText(GlobalUserItem.szSign);
                return;
            }
            this.m_szSign = szSign;
            if (szSign == GlobalUserItem.szSign) {
                return;
            }
        }
        else if ("edit_nickname" == editbox.getName()) {
            if (GlobalUserItem.isAngentAccount()) {
                this.edit_Nickname.setText(GlobalUserItem.szNickName);
                return;
            }

            if (GlobalUserItem.notEditAble()) {
                this.edit_Nickname.setText(GlobalUserItem.szNickName);
                return;
            }

            var szNickname = this.edit_Nickname.string.replace(" ", "");
            //判断长度
            if (szNickname.length < 6) {
                showToast(this, "游戏昵称必须大于6位以上,请重新输入!", 2);
                this.edit_Nickname.setText(GlobalUserItem.szNickName);
                return;
            }

            //判断emoji
            if (ExternalFun.isContainEmoji(szNickname)) {
                showToast(this, "昵称中包含非法字符,请重试", 2);
                this.edit_Sign.setText(GlobalUserItem.szSign);
                return;
            }

            //判断是否有非法字符
            if (true == ExternalFun.isContainBadWords(szNickname)) {
                showToast(this, "昵称中包含敏感字符,请重试", 2);
                this.edit_Nickname.setText(GlobalUserItem.szNickName);
                return;
            }
            this.m_szNick = szNickname;
            if (szNickname == GlobalUserItem.szNickName) {
                return;
            }
        }

        if (this.m_szNick == "" || this.m_szSign == "") {
            return;
        }
        this.showPopWait();
        this._modifyFrame.onModifyUserInfo(GlobalUserItem.cbGender, this.m_szNick, this.m_szSign);
    },

    editBoxTextChanged: function (editbox, text) {
        cc.log("editBox  TextChanged, text: " + text);
    },

    editBoxReturn: function (editbox) {
        cc.log("editBox  was returned !");
    },
    
    onEditEvent : function(name, editbox) {
        if ("return" == name) {
            if ("edit_sign" == editbox.getName()) {
                if (GlobalUserItem.isAngentAccount()) {
                    this.edit_Sign.setText(GlobalUserItem.szSign);
                    return;
                }
                if (GlobalUserItem.notEditAble()) {
                    this.edit_Sign.setText(GlobalUserItem.szSign);
                    return;
                }
    
                var szSign = this.edit_Sign.string.replace(" ", "");
                //判断长度
                if (szSign.length < 1) {
                    showToast(this, "个性签名不能为空", 2);
                    this.edit_Sign.setText(GlobalUserItem.szSign);
                    return;
                }
    
                //判断emoji
                if (ExternalFun.isContainEmoji(szSign)) {
                    showToast(this, "个性签名中包含非法字符,请重试", 2);
                    this.edit_Sign.setText(GlobalUserItem.szSign);
                    return;
                }
    
                //判断是否有非法字符
                if (true == ExternalFun.isContainBadWords(szSign)) {
                    showToast(this, "个性签名中包含敏感字符,请重试", 2);
                    this.edit_Sign.setText(GlobalUserItem.szSign);
                    return;
                }
                this.m_szSign = szSign;
                if (szSign == GlobalUserItem.szSign) {
                    return;
                }
            } else if ("edit_nickname" == editbox.getName()) {
                if (GlobalUserItem.isAngentAccount()) {
                    this.edit_Nickname.setText(GlobalUserItem.szNickName);
                    return;
                }
    
                if (GlobalUserItem.notEditAble()) {
                    this.edit_Nickname.setText(GlobalUserItem.szNickName);
                    return;
                }
    
                var szNickname = this.edit_Nickname.string.replace(" ", "");
                //判断长度
                if (szNickname.length < 6) {
                    showToast(this, "游戏昵称必须大于6位以上,请重新输入!", 2);
                    this.edit_Nickname.setText(GlobalUserItem.szNickName);
                    return;
                }
    
                //判断emoji
                if (ExternalFun.isContainEmoji(szNickname)) {
                    showToast(this, "昵称中包含非法字符,请重试", 2);
                    this.edit_Sign.setText(GlobalUserItem.szSign);
                    return;
                }
    
                //判断是否有非法字符
                if (true == ExternalFun.isContainBadWords(szNickname)) {
                    showToast(this, "昵称中包含敏感字符,请重试", 2);
                    this.edit_Nickname.setText(GlobalUserItem.szNickName);
                    return;
                }
                this.m_szNick = szNickname;
                if (szNickname == GlobalUserItem.szNickName) {
                    return;
                }
            }
    
            if (this.m_szNick == "" || this.m_szSign == "") {
                return;
            }
            this.showPopWait();
            this._modifyFrame.onModifyUserInfo(GlobalUserItem.cbGender, this.m_szNick, this.m_szSign);
        }
    },
    
    onExit : function() {
        if (null != this.edit_Sign) {
            //todo event
            // this.edit_Sign.unregisterScriptEditBoxHandler();
        }
    
        if (null != this.edit_Nickname) {
            //todo event
            // this.edit_Nickname.unregisterScriptEditBoxHandler();
        }
    
        if (this._modifyFrame.isSocketServer()) {
            this._modifyFrame.onCloseSocket();
        }
    },
    
    onEnterTransitionFinish : function() {
        this.showPopWait();
        this._modifyFrame.onQueryUserInfo();
    },
    
    sendModifySystemFace : function( wFaceId ) {
        this.showPopWait();
        this._modifyFrame.onModifySystemHead(wFaceId);
    },
    
    //操作结果
    onModifyCallBack : function(result,message) {
        this.dismissPopWait();
        if (message != null && message != "") {
            showToast(this, message, 2);
        }
        if (-1 == result) {
            return;
        }
    
        bGender = (GlobalUserItem.cbGender == yl.GENDER_MANKIND && true || false);
        this._cbtMan.setSelected(bGender);
        this._cbtWoman.setSelected(!bGender);
    
        if (yl.SUB_GP_USER_FACE_INFO == result) {
            this.onFaceResultSuccess();
            this.removeChildByTag(UserInfoLayer.LAY_SELHEAD, true);
        } else if (CMD_LogonServer.SUB_GP_USER_INDIVIDUAL == result) {
            // 推广员按钮
            //var noSpreader = GlobalUserItem.szSpreaderAccount == ""
            //this.m_btnPromoter.setVisible(noSpreader)
            //this.m_btnPromoter.setEnabled(noSpreader)
        } else if (this._modifyFrame.INPUT_SPREADER == result) {
            //var noSpreader = GlobalUserItem.szSpreaderAccount == ""
            //this.m_btnPromoter.setVisible(noSpreader)
            //this.m_btnPromoter.setEnabled(noSpreader)
        }
    },
    
    onKeyBack : function() {
        if (null != this._scene._popWait) {
            showToast(this, "当前操作不可返回", 2);
        }
        return true;
    },
    
    //显示等待
    showPopWait : function() {
        this._scene.showPopWait();
    },
    
    //关闭等待
    dismissPopWait : function() {
        this._scene.dismissPopWait();
    },
    
    //头像更改结果
    onFaceResultSuccess : function() {
        //更新头像
        this._head.updateHead(GlobalUserItem);
        //通知
        var eventListener = new cc.EventCustom(yl.RY_USERINFO_NOTIFY);
        eventListener.obj = yl.RY_MSG_USERHEAD;
        cc.eventManager.dispatchEvent(eventListener);

        // var eventListener = new cc.EventCustom(yl.RY_USERINFO_NOTIFY);
        // eventListener.obj = yl.RY_MSG_USERHEAD;
        // cc.director.getEventDispatcher().dispatchEvent(eventListener);
    }

});

UserInfoLayer.BT_MODIFY_INFO 	= 1;
UserInfoLayer.BT_BANK			= 2;
UserInfoLayer.BT_MODIFY_PASS	= 3;
UserInfoLayer.BT_ADD			= 4;
UserInfoLayer.BT_EXIT			= 5;

UserInfoLayer.BT_BINDING 		= 6;
UserInfoLayer.BT_VIP 			= 7;
UserInfoLayer.BT_NICKEDIT 		= 8;
UserInfoLayer.BT_SIGNEDIT 		= 9;
UserInfoLayer.BT_TAKE 			= 10;
UserInfoLayer.BT_RECHARGE 		= 11;
UserInfoLayer.BT_EXCHANGE 		= 12;
UserInfoLayer.BT_AGENT 			= 16;
UserInfoLayer.BT_CONFIRM		= 15;

UserInfoLayer.CBT_MAN			= 13;
UserInfoLayer.CBT_WOMAN			= 14;
UserInfoLayer.LAY_SELHEAD		= 17;

UserInfoLayer.BT_BAG			= 18;
UserInfoLayer.BT_QRCODE 		= 19;
UserInfoLayer.BT_PROMOTER 		= 20;
UserInfoLayer.BT_SAFE 		    = 21;