var VerifiedLayer = cc.Layer.extend({

    ctor:function(scene){
        this._super(cc.color(0, 0, 0, 125));
        var transLayer = new cc.LayerColor(cc.color(0, 0, 0, 125));
        transLayer.setContentSize(appdf.WIDTH, appdf.HEIGHT);
        this.addChild(transLayer);
        this._scene = scene;

        var self = this;
        this.Layer = ccs.load(res.VerifiedLayer_json).node;
        this.addChild(this.Layer);

        var  btcallback = function(ref, type){
            if(type == ccui.Widget.TOUCH_ENDED){
                self.onButtonClickedEvent(ref.getTag(),ref);
            }
        };

        var btn_back = this.Layer.getChildByName("btn_return");
        btn_back.setTag(VerifiedLayer.BT_EXIT);
        btn_back.addTouchEventListener(btcallback);

        var btn_confirm = this.Layer.getChildByName("btn_confirm");
        btn_confirm.setTag(VerifiedLayer.BT_CONFRIM);
        btn_confirm.addTouchEventListener(btcallback);

        var bg = this.Layer.getChildByName("bg");
        var imgFrame1 = bg.getChildByName("input_frame_1");
        var imgFrame2 = bg.getChildByName("input_frame_2");

        var edit_width_1 = imgFrame1.getContentSize().width;
        var edit_height_1 = imgFrame1.getContentSize().height;
        var edit_posX_1 = imgFrame1.getPositionX();
        var edit_posY_1 = imgFrame1.getPositionY();

        var edit_width_2 = imgFrame2.getContentSize().width;
        var edit_height_2 = imgFrame2.getContentSize().height;
        var edit_posX_2 = imgFrame2.getPositionX();
        var edit_posY_2 = imgFrame2.getPositionY();

        var name = this.Layer.getChildByName("name").setVisible(false);
        var cardid = this.Layer.getChildByName("cardid").setVisible(false);

        //真实姓名
        this.edit_trueName = new cc.EditBox(cc.size(edit_width_1,edit_height_1), ccui.Scale9Sprite("invite/sp_input_bg_invite.png"));
        this.edit_trueName.setPosition(edit_posX_1+5,edit_posY_1-15);
        this.edit_trueName.setAnchorPoint(cc.p(0.5,0.5));
        this.edit_trueName.setFontName(res.round_body_ttf);
        this.edit_trueName.setFontColor(cc.color(246,223,209));
        this.edit_trueName.setFontSize(16);
        this.edit_trueName.setPlaceholderFontName(res.round_body_ttf);
        this.edit_trueName.setPlaceholderFontColor(cc.color(246,223,209));
        this.edit_trueName.setPlaceholderFontSize(16);
        this.edit_trueName.setMaxLength(32);
        this.edit_trueName.setInputMode(cc.EDITBOX_INPUT_MODE_ANY);  //setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        this.edit_trueName.setPlaceHolder("请输入真实姓名");
        bg.addChild(this.edit_trueName);

        //身份证号
        this.edit_IdentityCard = new cc.EditBox(cc.size(edit_width_2,edit_height_2), ccui.Scale9Sprite("invite/sp_input_bg_invite.png"));
        this.edit_IdentityCard.setPosition(edit_posX_2+5,edit_posY_2);
        this.edit_IdentityCard.setAnchorPoint(cc.p(0.5,0.5));
        this.edit_IdentityCard.setFontName(res.round_body_ttf);
        this.edit_IdentityCard.setFontColor(cc.color(246,223,209));
        this.edit_IdentityCard.setFontSize(16);
        this.edit_IdentityCard.setPlaceholderFontName(res.round_body_ttf);
        this.edit_IdentityCard.setPlaceholderFontColor(cc.color(246,223,209));
        this.edit_IdentityCard.setPlaceholderFontSize(16);
        this.edit_IdentityCard.setMaxLength(32);
        this.edit_IdentityCard.setInputMode(cc.EDITBOX_INPUT_MODE_NUMERIC);//.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE)
        this.edit_IdentityCard.setPlaceHolder("请输入身份证号");
        bg.addChild(this.edit_IdentityCard);

        //网络回调
        var modifyCallBack = function(result,message){
            self.onModifyCallBack(result,message);
        };
        //网络处理
        this._modifyFrame = new ModifyFrame(this,modifyCallBack);

    },

    onExit : function() {
        if(this._modifyFrame.isSocketServer()){
            this._modifyFrame.onCloseSocket();
        }
        this._scene.onKeyBack();
    },

    onEnterTransitionFinish : function(){
        this.showPopWait();
        this._modifyFrame.onQueryUserInfo();
    },

     AnalysisInput : function(){
        var bSure = false;
        var szTureName = "";
        var szIdentityCard = "";
        //    var tempName =  "人人网"
        //    var tempID = "210181190012121234"
        var tempName = this.edit_trueName.string;
        var tempID = this.edit_IdentityCard.string;

        var countNumber1 = tempName.length;
        var countNumber2 = tempID.length;
        var countNumber3 = tempName.length;
        var oneSize = (countNumber3/countNumber1);

        if(countNumber1 <= 4 && countNumber1 >= 2 && countNumber2 == 18 && oneSize == 3){
            if(this.AnalysisIdentityCard(tempID)){
                bSure = true;
                szTureName = tempName;
                szIdentityCard = tempID;
            }
        }
        return [bSure,szTureName,szIdentityCard];
     },
    //身份证校验
    AnalysisIdentityCard : function(szIdentityCard){
        var bSure = false;
        var szLast = szIdentityCard.substring(17);
        // var szLast = szIdentityCard.substring(18);
        var tabIdentityCard = {};
        for(var i = 0 ; i < 17 ; i++ ){
            var tempIdentityCard_1 = szIdentityCard.substring(i-1,i-1);
            // var tempIdentityCard_1 = szIdentityCard.substring(i,i);
            var intNumber = Number(tempIdentityCard_1);
            tabIdentityCard[i] = intNumber;
        }
        var sum = 0;
        for(i = 0 ; i < 17 ; i++){
            sum = sum + tabIdentityCard[i]*coefficient[i];
        }
        var mod = math_mod(sum,11);
        var szMod = mod + 1;
        if(Remainder[szMod] == szLast){
            bSure = true;
        }
        return bSure;
    },

    onButtonClickedEvent : function(tag,sender){
        if(tag == VerifiedLayer.BT_EXIT) {
            this.onExit();
        }else if(tag == VerifiedLayer.BT_CONFRIM){
            var bSure,trueName,cardId = this.AnalysisInput();
            if(bSure) {
                this._modifyFrame.onModifyTrueUserInfo(trueName, cardId);
            } else{
                showToast(this, "对不起，您输入有误哦", 2);
            }
        }
    },
    //操作结果
    onModifyCallBack : function(result,message){
        this.dismissPopWait();
        if( message != null && message != ""){
            showToast(this,message,2);
        }
        if(-1 == result){
            return;
        }
        if(yl.SUB_GP_USER_FACE_INFO == result) {
            this.onFaceResultSuccess();
            this.removeChildByTag(this.LAY_SELHEAD, true);
        }else{
            showToast(this,"成功",2);
        }
    },
    //关闭等待
    dismissPopWait : function(){
        this._scene.dismissPopWait();
    }
});

VerifiedLayer.BT_EXIT       = 1;
VerifiedLayer.BT_CONFRIM    = 2;
//余数
var Remainder = ["1","0","X","9","8","7","6","5","4","3","2"];
//系数
var coefficient = [7,9,10,5,8,4,2,1,6,3,7,9,10,5,8,4,2];
// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new VerifiedLayer();
//         this.addChild(layer);
//     }
// });
