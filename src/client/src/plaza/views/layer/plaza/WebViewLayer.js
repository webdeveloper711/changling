/*  Author  .   Jin */
//
var WebViewLayer = cc.Layer.extend({
    //访问本地html文件
    LOAD_FILE : 1,
    //访问url
    LOAD_URL : 2,

    ctor : function(scene) {
        this._super();
        this._scene = scene;

        var URL_SCHEME = "ryweb";
        var JS_SCHEME = "ryweb.//";
        var TAG_MASK = 101;
        var BTN_CLOSE = 102;
        //加载csb资源
        var rootLayer = (ExternalFun.loadRootCSB(res.WebViewLayer_json, this)).rootlayer;
        var csbNode = (ExternalFun.loadRootCSB(res.WebViewLayer_json, this)).csbnode;
        var touchFunC = function (ref, tType) {
            if(tType === ccui.Widget.TOUCH_ENDED) {
                this.onButtonClickedEvent(ref.getTag(), ref);
            }
        };

        // 遮罩
        var mask = csbNode.getChildByName("panel_mask");
        mask.setTag(TAG_MASK);
        mask.addTouchEventListener(touchFunC);

        // 背景
        this.m_spBg = csbNode.getChildByName("image_bg");

        // 退出按钮
        var btn = this.m_spBg.getChildByName("btn_close");
        btn.setTag(BTN_CLOSE);
        btn.addTouchEventListener(touchFunC);

        // 今日不再显示
        var swith = this.m_spBg.getChildByName("check_switch");
        swith.addEventListener(function(sender, eventType) {
            GlobalUserItem.setTodayNoAdNotice(swith.isSelected());
        });

        // 界面
        var tmp = this.m_spBg.getChildByName("content");
        //dump(tmp.getContentSize(), "desciption", 6)
        //平台判定
        var targetPlatform = cc.sys.os;
        if ((cc.sys.OS_IOS == targetPlatform) || (cc.sys.OS_ANDROID == targetPlatform)) {
            //介绍页面
            this.m_webView = ccexp.WebView.create();
            this.m_webView.setPosition(tmp.getPosition());
            this.m_webView.setContentSize(cc.size(710, 320));

            this.m_webView.setScalesPageToFit(true);
            var times = (new Date).getTime();
            var url = yl.HTTP_URL + "/Mobile/AdsNotice.aspx";

            this.m_webView.loadURL(url);
            this.m_webView.setJavascriptInterfaceScheme(URL_SCHEME);
            ExternalFun.visibleWebView(this.m_webView, false);
            this._scene.showPopWait();

            this.m_webView.setOnDidFailLoading(function (sender, url) {
                this._scene.dismissPopWait();
                cc.log("open " + url + " fail");
            });
            this.m_webView.setOnShouldStartLoading(function (sender, url) {
                cc.log("onWebViewShouldStartLoading, url is ", url);
                return true;
            });
            this.m_webView.setOnDidFinishLoading(function (sender, url) {
                this._scene.dismissPopWait();
                ExternalFun.visibleWebView(this.m_webView, true);
            });
            this.m_webView.setOnJSCallback(function (sender, url) {
                if (url === "ryweb.//param.close") {
                    this._scene.dismissPopWait();
                    this.removeFromParent();
                } else {
                    this._scene.queryUserScoreInfo(function (ok) {
                        if (ok) {
                            this.m_webView.reload();
                        }
                    })
                }
            });

            this.m_spBg.addChild(this.m_webView);
        }
        tmp.removeFromParent();
    },

    onButtonClickedEvent : function(tag, ref) {
        if (BTN_CLOSE === tag) {
            this._scene.dismissPopWait();
            this.removeFromParent();
        }
    },

    onKeyBack : function() {
        if (null !== this.m_webView && true === this.m_webView.canGoBack()) {
            this.m_webView.goBack();
            return true;
        }
        return false;
    },

    getSignature : function(times) {
        cc.log("******  WebViewLayer.getSignature ******");
        cc.log("timevalue-" + times);
        var pstr = "3a5ke4a11fzb5c5e1" + GlobalUserItem.dwUserID + times;
        pstr = string.lower(md5(pstr));
        cc.log("signature-" + pstr);
        cc.log("******  WebViewLayer.getSignature ******");
        return pstr;
    },

    reloadWebView : function() {
        if (null !== this.m_webView) {
            this.m_webView.reload();
        }
    }

});

// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new WebViewLayer();
//         this.addChild(layer);
//     }
// });





