/*	Author	:	JIN	*/

var RoomLayer = cc.Layer.extend({
    BT_FIND : 1,
    BT_LOCK : 2,
    ctor: function (frameEngine, scene, bQuickStart) {
        this._super();

        var self = this;

        //椅子位置定义 (以桌子背景为父节点得到的坐标)
        this.USER_POS =
            [
                [cc.p(168, 219)],	//1
                [cc.p(168, 219), cc.p(168, -29)], //2
                [cc.p(18, 18), cc.p(170, 222), cc.p(320, 18)], // 3
                [cc.p(-18, 109), cc.p(170, 220), cc.p(360, 109), cc.p(170, -27)], // 4
                [cc.p(-3, 160), cc.p(170, 220), cc.p(342, 160), cc.p(310, 6), cc.p(30, 6)], // 5
                [cc.p(14, 180), cc.p(170, 220), cc.p(324, 180), cc.p(324, 19), cc.p(170, -26), cc.p(14, 19)], //6
                [cc.p(-20, 100), cc.p(33, 200), cc.p(170, 220), cc.p(304, 200), cc.p(360, 100), cc.p(304, 0), cc.p(33, 0)], //7
                [cc.p(-20, 100), cc.p(33, 200), cc.p(170, 220), cc.p(304, 200), cc.p(360, 100), cc.p(304, 0), cc.p(170, -26), cc.p(33, 0)], // 8
                [cc.p(-18, 72), cc.p(9, 178), cc.p(107, 220), cc.p(224, 220), cc.p(326, 178), cc.p(352, 72), cc.p(282, -8), cc.p(167, -30), cc.p(51, -8)] //9
            ];

        this._frameEngine = frameEngine;
        this._scene = scene;
        this.m_bQuickStart = bQuickStart || false;

        this.setContentSize(yl.WIDTH, yl.HEIGHT);

        this._btcallBack = function (ref, type) {
            if (type == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };

        //todo following statements will be needed
        // this._frameEngine.setViewFrame(this);
        //
        // this._tableCount = this._frameEngine._wTableCount;
        // this._chairCount = this._frameEngine._wChairCount;
        // this._cellCount = this._tableCount; //math.floor(this._tableCount/2)
        //
        // this._chairMode = 1;
        // if (this._chairCount > 9 || this._chairCount == 0) {
        //     this._chairMode = 1;
        // } else {
        //     this._chairMode = this._chairCount;
        // }

        this._bSitdown = true;

        //cell宽度
        this.getCellWidth();

        //区域设置
        this.setContentSize(yl.WIDTH, yl.HEIGHT);

        //房间列表
        //todo following statements will be needed
        // this._listView = new cc.TableView(cc.size(yl.WIDTH, 420));
        // this._listView.setDirection(cc.SCROLLVIEW_DIRECTION_HORIZONTAL);
        // this._listView.setPosition(cc.p(0, yl.HEIGHT / 2 - 220));
        // this._listView.setDelegate();
        // this.addChild(this._listView);
        // this._listView.registerScriptHandler(this.tableCellTouched, cc.TABLECELL_TOUCHED);
        // this._listView.registerScriptHandler(handler(this, this.cellSizeForTable), cc.TABLECELL_SIZE_FOR_INDEX);
        // this._listView.registerScriptHandler(handler(this, this.tableCellAtIndex), cc.TABLECELL_SIZE_AT_INDEX);
        // this._listView.registerScriptHandler(this.numberOfCellsInTableView, cc.NUMBER_OF_CELLS_IN_TABLEVIEW);
        // this._listView.reloadData();
        var btn = new ccui.Button(res.bt_lock_0_png, res.bt_lock_1_png);
        this.addChild(btn);
        btn.setTag(RoomLayer.BT_LOCK);
        btn.setPosition(70, 580);
        //.setScale(0.75)

        btn.addTouchEventListener(this._btcallBack);

        if (true == this.m_bQuickStart) {
            this.stopAllActions();
            this.onSitDown(1, 1);
        }

        this.m_bEnableKeyBack = true;
    },
    // var HeadSprite = appdf.req(appdf.EXTERNAL_SRC + "HeadSprite")
    onExitRoom : function() {
        this.stopAllActions();
        this.dismissPopWait();
        this._scene.onChangeShowMode(yl.SCENE_ROOMLIST);
    },
    //显示等待
    showPopWait : function() {
        if (this._scene && this._scene.showPopWait) {
            this._scene.showPopWait();
        }
    },
    //关闭等待
    dismissPopWait : function() {
        if (this._scene && this._scene.dismissPopWait) {
            this._scene.dismissPopWait();
        }
    },

    onButtonClickedEvent : function(tag,ref) {
        var self = this;
        cc.log("RoomLayer.onButtonClickedEvent." + tag);
        if (tag == RoomLayer.BT_LOCK) {
            this.m_bEnableKeyBack = false;
            this._scene.createPasswordEdit("请输入要设置的桌子密码", function (pass) {
                self.m_bEnableKeyBack = true;
                self._frameEngine.SendEncrypt(pass);
            });
        } else if (tag > 410 && tag < 420) {
            var tableid = ref.getParent().getParent().getIdx();
            var chairid = tag - 411;
            this.onSitDown(tableid, chairid);
        }
    },

//子视图大小
    cellSizeForTable : function(view, idx) {
        return [this.m_nCellWidth , 420];
    },

//子视图数目
    numberOfCellsInTableView : function(view) {
        return view.getParent()._cellCount;
    },

    tableCellTouched : function(view, cell) {

    },

//获取子视图
    tableCellAtIndex : function(view, idx) {
        var cell = view.dequeueCell();
        //椅子模式
        var chairMode = this._chairMode;
        //用户列表
        var tableUser = this._tableUser;
        //游戏引擎
        var engine = this._frameEngine;

        var chair_pos = this.USER_POS[chairMode];
        var bg1 = null;

        if (!cell) {
            cell = new cc.TableViewCell();

            var btcallback = function (ref, type) {
                if (type == ccui.Widget.TOUCH_ENDED) {
                    view.getParent().onButtonClickedEvent(ref.getTag(), ref);
                }
            };
            //桌子背景
            var bg1 = this.createTableBg();
            bg1.setPosition(yl.WIDTH / 6, 420 / 2);
            bg1.setTag(100);
            cell.addChild(bg1);

            //椅子用户
            for (var i = 1; i <= 9; i++) {
                var btn = new ccui.Button(res.bg_chair_png, res.bg_chair_png);
                btn.setTag(410 + i);
                btn.addTo(bg1);
                btn.addTouchEventListener(btcallback);
            }
        } else {
            bg1 = cell.getChildByTag(100);
        }
        if (null == bg1) {
            return cell;
        }

        chair_pos = bg1.chair_pos;
        if (null == bg1 || typeof chair_pos != "table") {
            return cell;
        }

        //桌子号
        var tableid = idx; //idx*2;

        //是否显示桌子
        var bShow = tableid < view.getParent()._tableCount;
        cell.getChildByTag(100).setVisible(bShow);
        if (bShow) {
            cell.getChildByTag(100).getChildByTag(1).setString(sprintf("%03d", tableid + 1) + "号桌");
            var tablestatus = engine.getTableInfo(tableid + 1);

            // 桌子状态(进行中/桌子加密)
            if (tablestatus) {
                if (tablestatus.cbPlayStatus && tablestatus.cbPlayStatus == 1) {
                    cell.getChildByTag(100).getChildByTag(2).setTexture(res.flag_playstatus_png);
                } else {
                    cell.getChildByTag(100).getChildByTag(2).setTexture(res.flag_waitstatus_png);
                }

                if (tablestatus.cbTableLock && tablestatus.cbTableLock == 1) {
                    cell.getChildByTag(100).getChildByTag(3).setVisible(true);
                } else {
                    cell.getChildByTag(100).getChildByTag(3).setVisible(false);
                }

                this.updateTable(bg1, tablestatus.cbPlayStatus);
            } else {
                cell.getChildByTag(100).getChildByTag(2).setTexture(res.flag_waitstatus_png);
                cell.getChildByTag(100).getChildByTag(3).setVisible(false);
            }
        }
        for (var i = 1; i <= 9; i++) {
            var headclip = bg1.getChildByTag(310 + i);
            if (i > chairMode || !bShow) {

                // 隐藏多余椅子
                if (null != headclip) {
                    headclip.setVisible(false);
                }
                bg1.getChildByTag(410 + i).setVisible(false);
            } else {

                // 更新椅子头像
                var useritem = engine.getTableUserItem(tableid, i - 1);
                var pos = chair_pos[i] || cc.p(0, 0);
                var c = bg1.getChildByTag(410 + i);
                c.setPosition(pos.x, pos.y);
                c.setVisible(true);
                if (!useritem) {
                    var tmpclip = bg1.getChildByTag(310 + i);
                    if (null != tmpclip) {
                        tmpclip.setVisible(false);
                    }
                    bg1.getChildByTag(410 + i).loadTextures(res.chair_empty_png, res.chair_empty_png);
                } else {
                    if (null == headclip) {
                        var head = HeadSprite.createClipHead(useritem, 40);
                        head.setTag(310 + i);
                        bg1.addChild(head);
                        headclip = head;
                    } else {
                        headclip.updateHead(useritem)
                    }
                    headclip.setPosition(pos.x, pos.y);
                    headclip.setVisible(true);
                    bg1.getChildByTag(410 + i).loadTextures(res.bg_chair_png, res.bg_chair_png);
                }
            }
        }
        return cell;
    },

    //根据游戏类型、游戏人数区分桌子底图
    createTableBg : function() {
        var entergame = this._scene.getEnterGameInfo();
        var sp_table = null;
        var chair_pos = null;

        //自定义资源
        var modulestr = entergame._KindName.replace("%.", "/");
        var targetPlatform = cc.sys.platform;
        var customRoomFile = "";
        if ((cc.sys.WIN32 === targetPlatform) || (cc.sys.MOBILE_BROWSER === targetPlatform) || (cc.sys.DESKTOP_BROWSER === targetPlatform)) {
            customRoomFile = "game/" + modulestr + "src/views/GameRoomLayer.js";
        } else {
            customRoomFile = "game/" + modulestr + "src/views/GameRoomLayer.js";  //GameRoomLayer.luac
        }
        if (jsb.fileUtils.isFileExist(customRoomFile)) {
            // var res = appdf.req(customRoomFile).getTableParam(this._frameEngine);
            sp_table = res[0];
            chair_pos = res[1];
        }

        var bgSize = cc.size(0, 0);
        //默认资源
        if (null == sp_table || null == chair_pos) {
            cc.log("RoomLayer.createTableBg default param");
            sp_table = new cc.Sprite(res.bg_table_png);
            chair_pos = this.USER_POS[this._chairMode];

            bgSize = sp_table.getContentSize();
            //桌号背景
    //		display.newSprite("Room/bg_tablenum.png")
    //			.addTo(sp_table)
    //			.move(bgSize.width * 0.5,10)
            var txt = new ccui.Text("", res.round_body_ttf, 16);
            sp_table.addChild(txt);
            txt.setColor(cc.c4b(255, 193, 200, 255));
            txt.setTag(1);
            txt.setPosition(bgSize.width * 0.5, 12);
            //状态
            var sp = new cc.Sprite(res.flag_waitstatus_png);
            sp_table.addChild(sp);
            sp.setTag(2);
            sp.setPosition(bgSize.width * 0.5, bgSize.height * 0.5);
        }
        bgSize = sp_table.getContentSize();
        //锁桌
        var sp = new cc.Sprite(res.plazz_sp_locker_png);
        sp_table.addChild(sp);
        sp.setTag(3);
        sp.setPosition(bgSize.width * 0.5, bgSize.height * 0.5);
        sp_table.chair_pos = chair_pos;

        return sp_table;
    },

    // 更新桌子图(游戏状态/非游戏状态)
    updateTable : function( spTable ,cbStatus ) {
        var tableFile = "";

        var entergame = this._scene.getEnterGameInfo();
        var modulestr = entergame._KindName.replace("%.", "/");
        if (1 == cbStatus) {
            // 游戏中
            tableFile = "game/" + modulestr + "res/roomlist/roomtable_play.png";
        } else {
            // 等待中
            tableFile = "game/" + modulestr + "res/roomlist/roomtable.png";
        }

        if (cc.FileUtils.isFileExist(tableFile)) {
            spTable.setTexture(tableFile);
        }
    },

    getCellWidth : function() {
        //todo following statements will be needed
        // var entergame = this._scene.getEnterGameInfo();
        //
        // //自定义数量
        // var modulestr = entergame._KindName.replace("%.", "/");
        // var targetPlatform = cc.sys.os;
        // var customRoomFile = "";
        // if (cc.sys.OS_WINDOWS == targetPlatform) {
        //     customRoomFile = "game/" + modulestr + "src/views/GameRoomLayer.js";
        // } else {
        //     customRoomFile = "game/" + modulestr + "src/views/GameRoomLayer.luac";
        // }
        //
        // var count = null;
        // if (jsb.fileUtils.isFileExist(customRoomFile)) {
        //     // count = appdf.req(customRoomFile).getShowCount();
        // }
        // count = count || 3;
        //
        // this.m_nCellWidth = yl.WIDTH / count;
    },

    //查找桌子
    onFindTable : function(){
        // body
    },

    //加密桌子
    onCreateLockTable : function() {
        // body
    },

    //加密桌子
    onEnterLockTable : function(tableid,chairid) {
        var self = this;
        this.m_bEnableKeyBack = false;
        this._scene.createPasswordEdit("请输入桌子密码", function (pass) {
            self.m_bEnableKeyBack = true;
            if (self._frameEngine.SitDown(tableid, chairid, pass)) {
                self.showPopWait();
            }
        });
    },

    //坐下桌子
    onSitDown : function(tableid,chairid) {
        cc.log("onSitDown table" + tableid + " chair" + chairid);
        var tablestatus = this._frameEngine.getTableInfo(tableid + 1);
        if (tablestatus) {
            //已经开始 判断是否动态加入
            if (tablestatus.cbPlayStatus != 0) {
                cc.log("已经开始");
                //return
            }
            //加锁处理 显示密码界面
            if (tablestatus.cbTableLock != 0) {
                this.onEnterLockTable(tableid, chairid);
                return;
            }
        }

        if (this._frameEngine.SitDown(tableid, chairid)) {
            this.showPopWait();
        }
    },

    onKeyBack : function() {
        return !this.m_bEnableKeyBack;
    },

    upDataTableStatus : function(tableid,tablestatus) {
        this._listView.updateCellAtIndex(tableid - 1);//math.floor(tableid/2))
    },

    onEventUserEnter : function(tableid,chairid,useritem) {
        this._listView.updateCellAtIndex(tableid);//math.floor(tableid/2));
    },

    onEventUserStatus : function(useritem,newstatus,oldstatus) {
        if (oldstatus && oldstatus.wTableID != yl.INVALID_TABLE) {
            this._listView.updateCellAtIndex(oldstatus.wTableID);//math.floor(oldstatus.wTableID/2))
        }
        if (newstatus && newstatus.wTableID != yl.INVALID_TABLE) {
            this._listView.updateCellAtIndex(newstatus.wTableID);//math.floor(newstatus.wTableID/2))
        }
    },

    onGetTableInfo : function() {
        if (null != this._listView) {
            this._listView.reloadData();
        }
    },

    onQuickStart : function() {
        if (this._frameEngine.QueryChangeDesk() == true) {
            this.showPopWait();
        }
    },

    onEnterTable : function() {
        this._scene.onEnterTable();
    },

    onReQueryFailure : function(code,msg) {
        this.dismissPopWait();

        if (msg && msg.length > 0) {
            showToast(this._scene, msg, 1);
        }
    }
});
// var testScene = cc.Scene.extend({
//     onEnter : function () {
//         this._super();
//         var layer = new RoomLayer();
//         this.addChild(layer);
//     }
// });