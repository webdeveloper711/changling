/*
 author : kil
 date : 2017-12-6
 */

var CreateLayerModel = cc.Layer.extend({
    ctor : function (scene) {
        this._super();
        // ExternalFun.registerNodeEvent(this); TODO; will delete because this is js built in method
        this._scene = scene;
        this._cmd_pri_login = cmd_private.login;
        this._cmd_pri_game = cmd_private.game;
        this.m_quickin = false;
    },

    getToastlayer : function () {
        return this._scene;
    },

    // 刷新界面
    onRefreshInfo : function () {
        cc.log("base refresh");
    },

    // 获取邀请分享内容
    getInviteShareMsg : function (roomDetailInfo) {
        cc.log("base get invite");
        return {title : "", content : ""};
    },

    //私人房创建成功
    onRoomCreateSuccess : function() {
        // 创建成功
        if (0 == PriRoom.getInstance().m_tabRoomOption.cbIsJoinGame) {
            // 非必须加入
            PriRoom.getInstance().getTagLayer(PriRoom.LAYTAG.LAYER_CREATERESULT, this.m_quickin, this._scene)
        }
        this.onRefreshInfo();
    },

    // 私人房登陆完成
    onLoginPriRoomFinish :function() {
        cc.log("base login finish");
        return false;
    }
});