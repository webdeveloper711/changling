
//Author: Li
//Date: 2016-12-28 16:16:34

//私人房网络处理
// var BaseFrame = appdf.req(appdf.CLIENT_SRC+"plaza.models-cc-cc.BaseFrame")
// var PriFrame = class("PriFrame",BaseFrame)
// var ExternalFun = appdf.req(appdf.EXTERNAL_SRC + "ExternalFun")
//
// var cmd_private = appdf.req(PriRoom.MODULE.PRIHEADER + "CMD_Private")
// var define_private = appdf.req(PriRoom.MODULE.PRIHEADER + "Define_Private")
// var struct_private = appdf.req(PriRoom.MODULE.PRIHEADER + "Struct_Private")
//
// var QueryDialog = appdf.req("base.src.app.views.layer.other.QueryDialog")

//登陆服务器CMD
var cmd_pri_login = cmd_private.login;
//游戏服务器CMD
var cmd_pri_game = cmd_private.game;

PriFrame.OP_CREATEROOM = cmd_pri_login.SUB_MB_QUERY_GAME_SERVER;                 //创建房间
PriFrame.OP_SEARCHROOM = cmd_pri_login.SUB_MB_SEARCH_SERVER_TABLE;               //查询房间
PriFrame.OP_ROOMPARAM = cmd_pri_login.SUB_MB_GET_PERSONAL_PARAMETER;             //私人房配置
PriFrame.OP_QUERYLIST = cmd_pri_login.SUB_MB_QUERY_PERSONAL_ROOM_LIST;          //私人房列表
PriFrame.OP_DISSUMEROOM = cmd_pri_login.SUB_MB_DISSUME_SEARCH_SERVER_TABLE;      //解散桌子
PriFrame.OP_QUERYJOINLIST = cmd_pri_login.SUB_GR_USER_QUERY_ROOM_SCORE;          //查询参与列表
PriFrame.OP_EXCHANGEROOMCARD = cmd_pri_login.SUB_MB_ROOM_CARD_EXCHANGE_TO_SCORE;//房卡兑换游戏币

LOGINSERVER = function(code) {
    return {m : cmd_pri_login.MDM_MB_PERSONAL_SERVICE, s : code};
};
GAMESERVER = function(code) {
    return {m: cmd_pri_game.MDM_GR_PERSONAL_TABLE, s: code};
};

function PriFrame(view, callback) {
    BaseFrame.call(this, view, callback);
}

PriFrame.prototype = Object.create(BaseFrame.prototype);
PriFrame.prototype.constructor = PriFrame;

//创建房间
PriFrame.prototype.onCreateRoom = function() {
    //操作记录
    this._oprateCode = PriFrame.OP_CREATEROOM;
    if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null != this._callBack)
        this._callBack(LOGINSERVER(-1), "建立连接失败！");
    //动作定义
    PriRoom.getInstance().m_nLoginAction = PriRoom.L_ACTION.ACT_CREATEROOM;
};

//查询房间
PriFrame.prototype.onSearchRoom = function( roomId ) {
    //操作记录
    this._oprateCode = PriFrame.OP_SEARCHROOM;
    this._roomId = roomId || "";
    if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null != this._callBack)
        this._callBack(LOGINSERVER(-1), "建立连接失败！");
};

//私人房间配置
PriFrame.prototype.onGetRoomParameter = function() {
    //操作记录
    this._oprateCode = PriFrame.OP_ROOMPARAM;
    if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null != this._callBack)
        this._callBack(LOGINSERVER(-1), "建立连接失败！");
};

//查询私人房列表
PriFrame.prototype.onQueryRoomList = function() {
    //操作记录
    this._oprateCode = PriFrame.OP_QUERYLIST;
    if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null != this._callBack)
        this._callBack(LOGINSERVER(-1), "建立连接失败！");
};

//解散房间
PriFrame.prototype.onDissumeRoom = function( roomId ) {
    //操作记录
    this._oprateCode = PriFrame.OP_DISSUMEROOM;
    this._roomId = roomId || "";
    if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null != this._callBack)
        this._callBack(LOGINSERVER(-1), "建立连接失败！");
};

//查询参与列表
PriFrame.prototype.onQueryJoinList = function() {
    //操作记录
    this._oprateCode = PriFrame.OP_QUERYJOINLIST;
    if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null != this._callBack)
        this._callBack(LOGINSERVER(-1), "建立连接失败！");
};

//房卡兑换游戏币
PriFrame.prototype.onExchangeScore = function( lCount ) {
    //操作记录
    this._oprateCode = PriFrame.OP_EXCHANGEROOMCARD;
    this._lExchangeRoomCard = lCount;
    if (!this.onCreateSocket(yl.LOGONSERVER, yl.LOGONPORT) && null != this._callBack)
        this._callBack(LOGINSERVER(-1), "建立连接失败！");
};

//连接结果
PriFrame.prototype.onConnectCompeleted = function() {
    cc.log("============ PriFrame.onConnectCompeleted ============ ==> " + this._oprateCode);
    if (this._oprateCode == PriFrame.OP_CREATEROOM)              //创建房间
        this.sendCreateRoom();
    else if (this._oprateCode == PriFrame.OP_SEARCHROOM)          //查询房间
        this.sendSearchRoom();
    else if (this._oprateCode == PriFrame.OP_ROOMPARAM)           //私人房配置
        this.sendQueryRoomParam();
    else if (this._oprateCode == PriFrame.OP_QUERYLIST)           //请求私人房列表
        this.sendQueryRoomList();
    else if (this._oprateCode == PriFrame.OP_DISSUMEROOM)         //解散桌子
        this.sendDissumeRoom();
    else if (this._oprateCode == PriFrame.OP_QUERYJOINLIST)       //参与列表
        this.sendQueryJoinRoomList();
    else if (this._oprateCode == PriFrame.OP_EXCHANGEROOMCARD)    //房卡兑换游戏币
        this.sendExchangeScore();
    else {
        this.onCloseSocket();
        if (null != this._callBack)
            this._callBack(LOGINSERVER(-1), "未知操作模式！");
        PriRoom.getInstance().dismissPopWait();
    }
};

//发送创建房间
PriFrame.prototype.sendCreateRoom = function() {
    var buffer = ExternalFun.create_netdata(cmd_pri_login.CMD_MB_QueryGameServer);
    buffer.setcmdinfo(cmd_pri_login.MDM_MB_PERSONAL_SERVICE, cmd_pri_login.SUB_MB_QUERY_GAME_SERVER);
    buffer.pushdword(GlobalUserItem.dwUserID);
    buffer.pushdword(GlobalUserItem.nCurGameKind);
    buffer.pushbyte(PriRoom.getInstance().m_tabRoomOption.cbIsJoinGame);
    if (!this.sendSocketData(buffer) && null != this._callBack)
        this._callBack(LOGINSERVER(-1), "发送创建房间失败！");
};

//发送查询私人房
PriFrame.prototype.sendSearchRoom = function() {
    var buffer = ExternalFun.create_netdata(cmd_pri_login.CMD_MB_SearchServerTable);
    buffer.setcmdinfo(cmd_pri_login.MDM_MB_PERSONAL_SERVICE, cmd_pri_login.SUB_MB_SEARCH_SERVER_TABLE);
////-[//////////////[QTC_MODIFY_AA]//////////////////
    buffer.pushscore(GlobalUserItem.dUserBeans);
    buffer.pushscore(GlobalUserItem.lRoomCard);
    buffer.pushdword(GlobalUserItem.dwUserID);
////-]//////////////[QTC_MODIFY_AA]//////////////////
    buffer.pushstring(this._roomId, private_define.ROOM_ID_LEN);
//   buffer.pushdword(GlobalUserItem.nCurGameKind)
    buffer.pushdword(0);
    if (!this.sendSocketData(buffer) && null != this._callBack)
        this._callBack(LOGINSERVER(-1), "发送查询房间失败！");
};

//发送请求配置
PriFrame.prototype.sendQueryRoomParam = function() {
    var buffer = ExternalFun.create_netdata(cmd_pri_login.CMD_MB_GetPersonalParameter);
    buffer.setcmdinfo(cmd_pri_login.MDM_MB_PERSONAL_SERVICE, cmd_pri_login.SUB_MB_GET_PERSONAL_PARAMETER);
//   buffer.pushdword(GlobalUserItem.nCurGameKind)
    buffer.pushdword(0);
    if (!this.sendSocketData(buffer) && null != this._callBack)
        this._callBack(LOGINSERVER(-1), "发送请求配置失败！");
};

//发送查询私人房列表
PriFrame.prototype.sendQueryRoomList = function() {
    var buffer = ExternalFun.create_netdata(cmd_pri_login.CMD_MB_QeuryPersonalRoomList);
    buffer.setcmdinfo(cmd_pri_login.MDM_MB_PERSONAL_SERVICE, cmd_pri_login.SUB_MB_QUERY_PERSONAL_ROOM_LIST);
    buffer.pushdword(GlobalUserItem.dwUserID);
    buffer.pushdword(GlobalUserItem.nCurGameKind);
    if (!this.sendSocketData(buffer) && null != this._callBack)
        this._callBack(LOGINSERVER(-1), "发送查询房间列表失败！");
};

//发送解散房间
PriFrame.prototype.sendDissumeRoom = function() {
    var buffer = ExternalFun.create_netdata(cmd_pri_login.CMD_MB_DissumeSearchServerTable);
    buffer.setcmdinfo(cmd_pri_login.MDM_MB_PERSONAL_SERVICE, cmd_pri_login.SUB_MB_DISSUME_SEARCH_SERVER_TABLE);

    buffer.pushstring(this._roomId, private_define.ROOM_ID_LEN);

    if (!this.sendSocketData(buffer) && null != this._callBack)
        this._callBack(LOGINSERVER(-1), "发送解散房间失败！");
};

//发送查询参与列表
PriFrame.prototype.sendQueryJoinRoomList = function() {
    var buffer = ExternalFun.create_netdata(cmd_pri_login.CMD_MB_QeuryPersonalRoomList);
    buffer.setcmdinfo(cmd_pri_login.MDM_MB_PERSONAL_SERVICE, cmd_pri_login.SUB_GR_USER_QUERY_ROOM_SCORE);
    buffer.pushdword(GlobalUserItem.dwUserID);
    buffer.pushdword(GlobalUserItem.nCurGameKind);
    if (!this.sendSocketData(buffer) && null != this._callBack)
        this._callBack(LOGINSERVER(-1), "发送查询参与列表失败！");
};

//发送房卡兑换游戏币
PriFrame.prototype.sendExchangeScore = function() {
    var buffer = ExternalFun.create_netdata(cmd_pri_login.CMD_GP_ExchangeScoreByRoomCard);
    buffer.setcmdinfo(cmd_pri_login.MDM_MB_PERSONAL_SERVICE, cmd_pri_login.SUB_MB_ROOM_CARD_EXCHANGE_TO_SCORE);
    buffer.pushdword(GlobalUserItem.dwUserID);
    buffer.pushscore(this._lExchangeRoomCard);
    buffer.pushstring(GlobalUserItem.szMachine, yl.LEN_MACHINE_ID);
    if (!this.sendSocketData(buffer) && null != this._callBack)
        this._callBack(LOGINSERVER(-1), "发送兑换游戏币失败！");
};

//发送游戏服务器消息
PriFrame.prototype.sendGameServerMsg = function( buffer ) {
    if (null != this._gameFrame && this._gameFrame.isSocketServer()) {
        if (!this._gameFrame.sendSocketData(buffer))
            this._callBack(GAMESERVER(-1), "发送失败！");
    }
};

//发送进入私人房
PriFrame.prototype.sendEnterPrivateGame = function() {
    if (null != this._gameFrame && this._gameFrame.isSocketServer()){
        var c = yl.INVALID_TABLE;
        //找座椅
        var chaircount = this._gameFrame._wChairCount;
        for (var i = 0; i < chaircount; i++) {
            var sc = i;
            if (null == this._gameFrame.getTableUserItem(PriRoom.getInstance().m_dwTableID, sc)) {
                c = sc;
                break;
            }
        }
        cc.log("PriFrame.prototype.sendEnterPrivateGame ==> private enter " + PriRoom.getInstance().m_dwTableID + " ## " + c);

        this._gameFrame.SitDown(PriRoom.getInstance().m_dwTableID, c);
        this._gameFrame.SendGameOption();
    }
};


//强制解散游戏
PriFrame.prototype.sendDissumeGame = function( tableId ) {
    tableId = tableId || 0;
    if (null != this._gameFrame && this._gameFrame.isSocketServer()) {
        var buffer = ExternalFun.create_netdata(cmd_pri_game.CMD_GR_HostDissumeGame);
        buffer.setcmdinfo(cmd_pri_game.MDM_GR_PERSONAL_TABLE, cmd_pri_game.SUB_GR_HOSTL_DISSUME_TABLE);
        buffer.pushdword(GlobalUserItem.dwUserID);
        buffer.pushdword(tableId);
        if (!this._gameFrame.sendSocketData(buffer))
            this._callBack(GAMESERVER(-1), "发送解散游戏失败！");
    }
};

// 请求解散游戏
PriFrame.prototype.sendRequestDissumeGame = function() {
    if (null != this._gameFrame && this._gameFrame.isSocketServer()) {
        cc.log("game-cc socket sendRequestDissumeGame");
        var buffer = ExternalFun.create_netdata(cmd_pri_game.CMD_GR_CancelRequest);
        buffer.setcmdinfo(cmd_pri_game.MDM_GR_PERSONAL_TABLE, cmd_pri_game.SUB_GR_CANCEL_REQUEST);
        buffer.pushdword(GlobalUserItem.dwUserID);
        buffer.pushdword(this._gameFrame.GetTableID());
        buffer.pushdword(this._gameFrame.GetChairID());
        if (!this._gameFrame.sendSocketData(buffer))
            this._callBack(GAMESERVER(-1), "请求解散游戏失败！");
    }
};

//回复请求
PriFrame.prototype.sendRequestReply = function( cbAgree ) {
    if (null != this._gameFrame && this._gameFrame.isSocketServer()) {
        var buffer = ExternalFun.create_netdata(cmd_pri_game.CMD_GR_RequestReply);
        buffer.setcmdinfo(cmd_pri_game.MDM_GR_PERSONAL_TABLE, cmd_pri_game.SUB_GR_REQUEST_REPLY);
        buffer.pushdword(GlobalUserItem.dwUserID);
        buffer.pushdword(this._gameFrame.GetTableID());
        buffer.pushbyte(cbAgree);
        if (!this._gameFrame.sendSocketData(buffer))
            this._callBack(GAMESERVER(-1), "回复请求失败！");
    }
};

//网络信息(短连接)
PriFrame.prototype.onSocketEvent = function( main,sub,pData ) {
    cc.log("PriFrame:onSocketEvent ==> " + main + "##" + sub);
    var needClose = true;
    var dissmissPop = true;
    if (cmd_pri_login.MDM_MB_PERSONAL_SERVICE == main) {
        if (cmd_pri_login.SUB_MB_SEARCH_RESULT == sub)                           //房间搜索结果
            this.onSubSearchRoomResult(pData);
        else if (cmd_pri_login.SUB_MB_DISSUME_SEARCH_RESULT == sub)               //解散搜索结果
            this.onSubDissumeSearchReasult(pData);
        else if (cmd_pri_login.SUB_MB_QUERY_PERSONAL_ROOM_LIST_RESULT == sub)     //私人房列表
            this.onSubPrivateRoomList(pData);
        else if (cmd_pri_login.SUB_GR_USER_QUERY_ROOM_SCORE_RESULT == sub)        //参与列表
            this.onSubJoinRoomList(pData);
        else if (cmd_pri_login.SUB_MB_PERSONAL_PARAMETER == sub) {                  //私人房间属性
            needClose = false;
            this.onSubRoomOption(pData);
//           PriRoom.getInstance().m_tabRoomOption = ExternalFun.read_netdata( struct_private.tagPersonalRoomOption, pData )
//           PriRoom.getInstance().cbIsJoinGame = PriRoom.getInstance().m_tabRoomOption.cbIsJoinGame
        }
        else if (cmd_pri_login.SUB_MB_PERSONAL_FEE_PARAMETER == sub)              //私人房费用配置
            this.onSubFeeParameter(pData);
        else if (cmd_pri_login.SUB_MB_QUERY_GAME_SERVER_RESULT == sub)            //创建结果
            dissmissPop = this.onSubGameServerResult(pData);
        else if (cmd_pri_login.SUB_GP_EXCHANGE_ROOM_CARD_RESULT == sub)           //房卡兑换游戏币结果
            this.onSubExchangeRoomCardResult(pData);
    }

    if (needClose) {
        this.onCloseSocket();
        if (dissmissPop)
            PriRoom.getInstance().dismissPopWait();
    }
};

//房间搜索结果
PriFrame.prototype.onSubSearchRoomResult = function( pData ){
    var cmd_table = ExternalFun.read_netdata(cmd_pri_login.CMD_MB_SearchResult, pData);
    ExternalFun.dump(cmd_table, "CMD_MB_SearchResult", 6);

    if (0 == cmd_table.dwServerID){
////-[//////////////[QTC_MODIFY_AA]//////////////////
		if (cmd_table.cbLate != 0) {
            if (null != this._callBack)
                this._callBack(LOGINSERVER(cmd_pri_login.SUB_MB_SEARCH_RESULT), "这是个AA制房间, 游戏已经开始, 不能中途加入, 请重新输入!");
            PriRoom.getInstance().m_tabJoinGameRecord[GlobalUserItem.nCurGameKind] = [];
        }
		else if(cmd_table.cbPoor != 0 && 1 == cmd_table.cbBeanOrRoomCard) {
            if (null != this._callBack) {
                this._callBack(LOGINSERVER(cmd_pri_login.SUB_MB_SEARCH_RESULT), "这是个AA制房间,您的房卡少于" + cmd_table.dwFee + ", 请重新输入!");
                PriRoom.getInstance().m_tabJoinGameRecord[GlobalUserItem.nCurGameKind] = [];
            }
        }
		else if(cmd_table.cbPoor != 0 && 0 == cmd_table.cbBeanOrRoomCard) {
            if (null != this._callBack)
                this._callBack(LOGINSERVER(cmd_pri_login.SUB_MB_SEARCH_RESULT), "这是个AA制房间,您的游戏豆少于" + cmd_table.dwFee + ", 请重新输入!");
            PriRoom.getInstance().m_tabJoinGameRecord[GlobalUserItem.nCurGameKind] = [];
        }
        else if( null != this._callBack){
            this._callBack(LOGINSERVER(cmd_pri_login.SUB_MB_SEARCH_RESULT), "该房间ID不存在, 请重新输入!");
            PriRoom.getInstance().m_tabJoinGameRecord[GlobalUserItem.nCurGameKind] = [];
        }
////-]//////////////[QTC_MODIFY_AA]//////////////////
        return;
    }

    //更新判断
    //appdf
    
   // var app = PriRoom.getInstance()._scene.getApp();
    var gamelist = _gameList;
    for (var i = 0 ; i < gamelist.length ; i++) {
        var kind = gamelist[i]._KindID;
        if (Number(kind) == cmd_table.dwKindID) {
            var gameinfo = gamelist[i];
            //var version = Number(app.getVersionMgr().getResVersion(cmd_table.dwKindID));  TODO
            var version = 0; // inserted by Han
            if (version == undefined || gameinfo._ServerResVersion > version) {

                cc.log("没有相应游戏或版本不对");

                //暂时出个版本不对提示，等更新做完了再把提示去掉
                if (null != this._callBack) {
                    this._callBack(LOGINSERVER(cmd_pri_login.SUB_MB_SEARCH_RESULT), "游戏版本过期，请到“金币场”或者“创建房间界面”更新对应游戏!");
                    PriRoom.getInstance().m_tabJoinGameRecord[GlobalUserItem.nCurGameKind] = [];
                    return;
                }


                return;
            }
            else {
                //信息记录
                PriRoom.getInstance().m_dwTableID = cmd_table.dwTableID;
                GlobalUserItem.nCurGameKind = cmd_table.dwKindID;
                //动作定义
                PriRoom.getInstance().m_nLoginAction = PriRoom.L_ACTION.ACT_SEARCHROOM;
                //发送登陆
                PriRoom.getInstance().onLoginRoom(cmd_table.dwServerID);
                return;
            }
        }
    }
};

//解散搜索结果
PriFrame.prototype.onSubDissumeSearchReasult = function( pData ) {
    var cmd_table = ExternalFun.read_netdata(cmd_pri_login.CMD_MB_DissumeSearchResult, pData);
    ExternalFun.dump(cmd_table, "CMD_MB_DissumeSearchResult", 6);

    //信息记录
    PriRoom.getInstance().m_dwTableID = cmd_table.dwTableID;
    //动作定义
    PriRoom.getInstance().m_nLoginAction = PriRoom.L_ACTION.ACT_DISSUMEROOM;
    //发送登陆
    PriRoom.getInstance().onLoginRoom(cmd_table.dwServerID);
};

//私人房列表
PriFrame.prototype.onSubPrivateRoomList = function( pData ) {
    PriRoom.getInstance().m_tabCreateRecord = [];

    var cmd_table = ExternalFun.read_netdata(cmd_pri_login.CMD_MB_PersonalRoomInfoList, pData);
    var listinfo = cmd_table.PersonalRoomInfo[0];
    for (var i = 0; i < private_define.MAX_CREATE_PERSONAL_ROOM; i++) {
        var info = listinfo[i];
        if (info.szRoomID != "") {
            info.lScore = info.lTaxCount;//this.getMyReword(info.PersonalUserScoreInfo[1])
            //时间戳
            var tt = info.sysDissumeTime;
            // info.sortTimeStmp = os.time({day=tt.wDay, month=tt.wMonth, year=tt.wYear, hour=tt.wHour, min=tt.wMinute, sec=tt.wSecond})
            info.sortTimeStmp = new Date(tt.wYear, tt.wMonth, tt.wDay, tt.wHour, tt.wMinute, tt.wSecond).getTime();
            tt = info.sysCreateTime;
            // info.createTimeStmp = os.time({day=tt.wDay, month=tt.wMonth, year=tt.wYear, hour=tt.wHour, min=tt.wMinute, sec=tt.wSecond})
            info.createTimeStmp = new Date(tt.wYear, tt.wMonth, tt.wDay, tt.wHour, tt.wMinute, tt.wSecond).getTime();
            PriRoom.getInstance().m_tabCreateRecord.push(info);
        }
        else
            break;
    }

    PriRoom.getInstance().m_tabCreateRecord.sort(function (a, b) {
        if (a.cbIsDisssumRoom != b.cbIsDisssumRoom)
            return a.cbIsDisssumRoom > b.cbIsDisssumRoom;
        else if (a.cbIsDisssumRoom == 0 && a.cbIsDisssumRoom == b.cbIsDisssumRoom)
            return a.createTimeStmp < b.createTimeStmp;
        else
            return a.sortTimeStmp < b.sortTimeStmp;
    });
    if (null != this._callBack)
        this._callBack(LOGINSERVER(cmd_pri_login.SUB_MB_QUERY_PERSONAL_ROOM_LIST_RESULT));
};

//参与列表
PriFrame.prototype.onSubJoinRoomList = function( pData ) {
    PriRoom.getInstance().m_tabJoinRecord = [];
    //计算数目
    var len = pData.getlen();
    var itemcount = Math.floor(len / private_define.LEN_PERSONAL_ROOM_SCORE);
    cc.log("PriFrame onSubJoinRoomList " + itemcount);
    for (var i = 0 ; i < itemcount; i++) {
        var pServer = ExternalFun.read_netdata(private_struct.tagQueryPersonalRoomUserScore, pData);
        pServer.lScore = this.getMyReword(pServer["PersonalUserScoreInfo"][1]);
        //时间戳
        var tt = pServer.sysDissumeTime;
        pServer.sortTimeStmp = new Date(tt.wYear, tt.wMonth, tt.wDay, tt.wHour, tt.wMinute, tt.wSecond).getTime() || 0;
        pServer.bFlagOnGame = (pServer.sortTimeStmp == 0);
        pServer.nOnGameOrder = pServer.bFlagOnGame && 1 || 0;
        tt = pServer.sysCreateTime;
        pServer.sortCreateTimeStmp = new Date(tt.wYear, tt.wMonth, tt.wDay, tt.wHour, tt.wMinute, tt.wSecond).getTime() || 0;
        PriRoom.getInstance().m_tabJoinRecord.push(pServer);
    }
    for (i = 0; i < PriRoom.getInstance().m_tabJoinRecord.length; i++) {
        for (var j = i; j < PriRoom.getInstance().m_tabJoinRecord.length; j++) {
            if (PriRoom.getInstance().m_tabJoinRecord[i].bFlagOnGame != PriRoom.getInstance().m_tabJoinRecord[j].bFlagOnGame) {
                if (PriRoom.getInstance().m_tabJoinRecord[i].nOnGameOrder == PriRoom.getInstance().m_tabJoinRecord[j].nOnGameOrder) {
                    if(PriRoom.getInstance().m_tabJoinRecord[i].sortCreateTimeStmp > PriRoom.getInstance().m_tabJoinRecord[j].sortCreateTimeStmp) {
                        temp = PriRoom.getInstance().m_tabJoinRecord[i];
                        PriRoom.getInstance().m_tabJoinRecord[i] = PriRoom.getInstance().m_tabJoinRecord[j];
                        PriRoom.getInstance().m_tabJoinRecord[j] = temp;
                    }
                } else{
                    if (PriRoom.getInstance().m_tabJoinRecord[i].nOnGameOrder > PriRoom.getInstance().m_tabJoinRecord[j].nOnGameOrder) {
                        temp = PriRoom.getInstance().m_tabJoinRecord[i];
                        PriRoom.getInstance().m_tabJoinRecord[i] = PriRoom.getInstance().m_tabJoinRecord[j];
                        PriRoom.getInstance().m_tabJoinRecord[j] = temp;
                    }
                }
            } else {
                if (PriRoom.getInstance().m_tabJoinRecord[i].sortTimeStmp > PriRoom.getInstance().m_tabJoinRecord[j].sortTimeStmp) {
                    temp = PriRoom.getInstance().m_tabJoinRecord[i];
                    PriRoom.getInstance().m_tabJoinRecord[i] = PriRoom.getInstance().m_tabJoinRecord[j];
                    PriRoom.getInstance().m_tabCreateRecord[j] = temp;
                }
            }
        }
    }
    // table.sort( PriRoom.getInstance().m_tabJoinRecord, function(a, b)
    //     if a.bFlagOnGame != b.bFlagOnGame)
    //         if a.nOnGameOrder == b.nOnGameOrder)
    //             return a.sortCreateTimeStmp < b.sortCreateTimeStmp
    //         else
    //             return a.nOnGameOrder < b.nOnGameOrder
    //         end
    //     else
    //         return a.sortTimeStmp < b.sortTimeStmp
    //     end
    // end )
    if (null != this._callBack)
        this._callBack(LOGINSERVER(cmd_pri_login.SUB_GR_USER_QUERY_ROOM_SCORE_RESULT));
};

PriFrame.prototype.getMyReword = function( list ) {
    if (typeof list != "array")   //if (type(list) != "table")
        return 0;
    for (k in list) {
        if (list[k]["dwUserID"] == GlobalUserItem.dwUserID)
            return (Number(list[k].lScore) || 0);
    }
    return 0;
};

//私人房配置信息
PriFrame.prototype.onSubRoomOption = function( pData ) {
    PriRoom.getInstance().m_tabAllRoomOption = [];
    var len = pData.getlen();
    var count = Math.floor(len / private_define.LEN_PERSONAL_ROOM_OPTION);
    for (var idx = 0; idx < count; idx++) {
        var param = ExternalFun.read_netdata(private_struct.tagPersonalRoomOption, pData);
        PriRoom.getInstance().m_tabAllRoomOption.push(param);
    }
//   table.sort( this.m_tabAllRoomOption, function(a, b)
//       return a.dwKindID < b.dwKindID
//   end )
};

//私人房费用配置
PriFrame.prototype.onSubFeeParameter = function( pData ) {
    cc.log("priframe onsubfeeparameter");
    PriRoom.getInstance().m_tabAllFeeConfigList = [];
    var len = pData.getlen();
    var count = Math.floor(len / private_define.LEN_PERSONAL_TABLE_PARAMETER);
    for (var idx = 0; idx < count; idx++) {
        var param = ExternalFun.read_netdata(private_struct.tagPersonalTableParameter, pData);
        PriRoom.getInstance().m_tabAllFeeConfigList.push(param);
    }

    for (i = 0; i < PriRoom.getInstance().m_tabAllFeeConfigList.length; i++) {
        for (var j = i; j < PriRoom.getInstance().m_tabAllFeeConfigList.length; j++) {
            if (PriRoom.getInstance().m_tabAllFeeConfigList[i].nOnGameOrder == PriRoom.getInstance().m_tabAllFeeConfigList[j].nOnGameOrder) {
                if (PriRoom.getInstance().m_tabAllFeeConfigList[i].dwDrawCountLimit > PriRoom.getInstance().m_tabAllFeeConfigList[j].dwDrawCountLimit) {
                    var temp = PriRoom.getInstance().m_tabAllFeeConfigList[i];
                    PriRoom.getInstance().m_tabAllFeeConfigList[i] = PriRoom.getInstance().m_tabAllFeeConfigList[j];
                    PriRoom.getInstance().m_tabAllFeeConfigList[j] = temp;
                }
            }
        }
    }
    // table.sort( PriRoom.getInstance().m_tabAllFeeConfigList, function(a, b)
    //     return a.dwDrawCountLimit < b.dwDrawCountLimit
    // end )
    ExternalFun.dump(PriRoom.getInstance().m_tabAllFeeConfigList, "SUB_MB_PERSONAL_FEE_PARAMETER", 6);
    if (null != this._callBack)
        this._callBack(LOGINSERVER(cmd_pri_login.SUB_MB_PERSONAL_FEE_PARAMETER));
};

//创建结果
PriFrame.prototype.onSubGameServerResult = function( pData ) {
    var cmd_table = ExternalFun.read_netdata(cmd_pri_login.CMD_MB_QueryGameServerResult, pData);
    ExternalFun.dump(cmd_table, "CMD_MB_QueryGameServerResult", 6);

    var tips = cmd_table.szErrDescrybe;

    if (false == cmd_table.bCanCreateRoom) {
        if (null != this._callBack && typeof tips == "string")
            this._callBack(LOGINSERVER(-1), tips);
        return true;
    }
    if (0 == cmd_table.dwServerID && true == cmd_table.bCanCreateRoom) {
        if (null != this._callBack && typeof tips == "string")
            this._callBack(LOGINSERVER(-1), tips);
        return true;
    }
    //发送登陆
    PriRoom.getInstance().onLoginRoom(cmd_table.dwServerID);
    return false;
};

//房卡兑换游戏币结果
PriFrame.prototype.onSubExchangeRoomCardResult = function( pData ) {
    var cmd_table = ExternalFun.read_netdata(cmd_pri_login.CMD_GP_ExchangeRoomCardResult, pData);
    ExternalFun.dump(cmd_table, "CMD_GP_ExchangeRoomCardResult", 6);

    if (true == cmd_table.bSuccessed) {
        //个人财富
        GlobalUserItem.lUserScore = cmd_table.lCurrScore;
        GlobalUserItem.lRoomCard = cmd_table.lRoomCard;
        //通知更新        
        var eventListener = cc.EventCustom.new(yl.RY_USERINFO_NOTIFY);
        eventListener.obj = yl.RY_MSG_USERWEALTH;
        cc.director.getEventDispatcher().dispatchEvent(eventListener);
    }

    if (null != this._callBack)
        this._callBack(LOGINSERVER(cmd_pri_login.CMD_GP_ExchangeRoomCardResult), cmd_table.szNotifyContent);
};

//网络消息(长连接)
PriFrame.prototype.onGameSocketEvent = function( main,sub,pData ) {
    if (cmd_pri_game.MDM_GR_PERSONAL_TABLE == main) {
        if (cmd_pri_game.SUB_GR_CREATE_SUCCESS == sub)               //创建成功
            this.onSubCreateSuccess(pData);
        else if (cmd_pri_game.SUB_GR_CREATE_FAILURE == sub)           //创建失败
            this.onSubCreateFailure(pData);
        else if (cmd_pri_game.SUB_GR_CANCEL_TABLE == sub)             //解散桌子
            this.onSubTableCancel(pData);
        else if (cmd_pri_game.SUB_GR_CANCEL_REQUEST == sub)           //请求解散
            this.onSubCancelRequest(pData);
        else if (cmd_pri_game.SUB_GR_REQUEST_REPLY == sub)            //请求答复
            this.onSubRequestReply(pData);
        else if (cmd_pri_game.SUB_GR_REQUEST_RESULT == sub)           //请求结果
            this.onSubReplyResult(pData);
        else if (cmd_pri_game.SUB_GR_WAIT_OVER_TIME == sub)           //超时等待
            this.onSubWaitOverTime(pData);
        else if (cmd_pri_game.SUB_GR_PERSONAL_TABLE_TIP == sub)       //提示信息/游戏信息
            this.onSubTableTip(pData);
        else if (cmd_pri_game.SUB_GR_PERSONAL_TABLE_END == sub)       //结束
            this.onSubTableEnd(pData);
        else if (cmd_pri_game.SUB_GR_CANCEL_TABLE_RESULT == sub)      //私人房解散结果
            this.onSubCancelTableResult(pData);
        else if (cmd_pri_game.SUB_GR_CURRECE_ROOMCARD_AND_BEAN == sub) //强制解散桌子后的游戏豆和房卡数量
            this.onSubCancelTableScoreInfo(pData);
        else if (cmd_pri_game.SUB_GR_CHANGE_CHAIR_COUNT == sub)       //改变椅子数量
            this.onSubChangeChairCount(pData);
        else if (cmd_pri_game.SUB_GF_PERSONAL_MESSAGE == sub) {         //私人房消息
            var cmd_table = ExternalFun.read_netdata(cmd_pri_game.Personal_Room_Message, pData);
            //cbMessageType 0表示坐下type, 1表示断线重连
            if (1 == cmd_table.cbMessageType) {
                cc.log("PriFrame 断线重连发送起立");
                if (null != this._callBack)
                    this._callBack(GAMESERVER(cmd_pri_game.SUB_GF_PERSONAL_MESSAGE), cmd_table);
                //发送起立
                if (this._gameFrame.St && Up(1))
                    this._gameFrame.onCloseSocket();
            }
            else {
                if (null != this._callBack)
                    this._callBack(GAMESERVER(-1), cmd_table.szMessage);
                if (this._gameFrame.isSocketServer())
                    this._gameFrame.onCloseSocket();
            }
            GlobalUserItem.nCurRoomIndex = -1;
        }
    }
};
//创建成功
PriFrame.prototype.onSubCreateSuccess = function( pData ) {
    var cmd_table = ExternalFun.read_netdata(cmd_pri_game.CMD_GR_CreateSuccess, pData);
    ExternalFun.dump(cmd_table, "CMD_GR_CreateSuccess", 6);

    //更新私人房数据
    PriRoom.getInstance().m_tabPriData.dwDrawTimeLimit = cmd_table.dwDrawTimeLimit;
    PriRoom.getInstance().m_tabPriData.dwDrawCountLimit = cmd_table.dwDrawCountLimit;
    PriRoom.getInstance().m_tabPriData.szServerID = cmd_table.szServerID;
    //个人财富
    GlobalUserItem.dUserBeans = cmd_table.dBeans;
    GlobalUserItem.lRoomCard = cmd_table.lRoomCard;
    //通知更新        
    var eventListener = new cc.EventCustom(yl.RY_USERINFO_NOTIFY);
    eventListener.obj = yl.RY_MSG_USERWEALTH;
    // cc.director.getEventDispatcher().dispatchEvent(eventListener); TODO;

    //
    if (null != this._callBack)
        this._callBack(GAMESERVER(cmd_pri_game.SUB_GR_CREATE_SUCCESS));
    if (0 == PriRoom.getInstance().m_tabRoomOption.cbIsJoinGame) {
        if (this._gameFrame.isSocketServer())
            this._gameFrame.onCloseSocket();
    } else
            PriRoom.getInstance().m_nLoginAction = PriRoom.L_ACTION.ACT_SEARCHROOM;
};

//创建失败
PriFrame.prototype.onSubCreateFailure = function( pData ) {
    var cmd_table = ExternalFun.read_netdata(cmd_pri_game.CMD_GR_CreateFailure, pData);
    ExternalFun.dump(cmd_table, "CMD_GR_CreateFailure", 6);

    if (null != this._callBack)
        this._callBack(GAMESERVER(-1), cmd_table.szDescribeString);
};

//解散桌子
PriFrame.prototype.onSubTableCancel = function( pData ) {
    var cmd_table = ExternalFun.read_netdata(cmd_pri_game.CMD_GR_CancelTable, pData);
    ExternalFun.dump(cmd_table, "CMD_GR_CancelTable", 6);

    if (null != this._callBack)
        this._callBack(GAMESERVER(cmd_pri_game.SUB_GR_CANCEL_TABLE), cmd_table);
};

//请求解散
PriFrame.prototype.onSubCancelRequest = function( pData ) {
    var cmd_table = ExternalFun.read_netdata(cmd_pri_game.CMD_GR_CancelRequest, pData);
    //自己不处理
    if (cmd_table.dwUserID == GlobalUserItem.dwUserID) {
        cc.log("自己请求解散");
        return;
    }
    if (null != this._callBack)
        this._callBack(GAMESERVER(cmd_pri_game.SUB_GR_CANCEL_REQUEST), cmd_table);
};

//请求答复
PriFrame.prototype.onSubRequestReply = function( pData ) {
    var cmd_table = ExternalFun.read_netdata(cmd_pri_game.CMD_GR_RequestReply, pData);
    if (null != this._callBack)
        this._callBack(GAMESERVER(cmd_pri_game.SUB_GR_REQUEST_REPLY), cmd_table);
};

//请求结果
PriFrame.prototype.onSubReplyResult = function( pData ) {
    var cmd_table = ExternalFun.read_netdata(cmd_pri_game.CMD_GR_RequestResult, pData);
    if (null != this._callBack)
        this._callBack(GAMESERVER(cmd_pri_game.SUB_GR_REQUEST_RESULT), cmd_table);
}

//超时等待
PriFrame.prototype.onSubWaitOverTime = function( pData ) {
    var cmd_table = ExternalFun.read_netdata(cmd_pri_game.CMD_GR_WaitOverTime, pData);
    if (null != this._callBack)
        this._callBack(GAMESERVER(cmd_pri_game.SUB_GR_WAIT_OVER_TIME), cmd_table);
};

//提示信息
PriFrame.prototype.onSubTableTip = function( pData ) {
    var cmd_table = ExternalFun.read_netdata(cmd_pri_game.CMD_GR_PersonalTableTip, pData);
    PriRoom.getInstance().m_tabPriData = cmd_table;
    PriRoom.getInstance().m_bIsMyRoomOwner = (cmd_table.dwTableOwnerUserID == GlobalUserItem.dwUserID);
    PriRoom.getInstance().cbIsJoinGame = PriRoom.getInstance().m_tabPriData.cbIsJoinGame;
    if (null != this._callBack)
        this._callBack(GAMESERVER(cmd_pri_game.SUB_GR_PERSONAL_TABLE_TIP), cmd_table);
    cc.log(PriRoom.getInstance().m_tabPriData + ":PriRoom.getInstance().m_tabPriData");
};

//结束消息
PriFrame.prototype.onSubTableEnd = function( pData ) {
    var cmd_table = ExternalFun.read_netdata(cmd_pri_game.CMD_GR_PersonalTableEnd, pData);
    if (null != this._callBack)
        this._callBack(GAMESERVER(cmd_pri_game.SUB_GR_PERSONAL_TABLE_END), cmd_table, pData);
    //结束主动起立
    this._gameFrame.St && Up(1);
};

//私人房解散结果
PriFrame.prototype.onSubCancelTableResult = function( pData ) {
    var cmd_table = ExternalFun.read_netdata(cmd_pri_game.CMD_GR_DissumeTable, pData);
    if (null != this._callBack)
        this._callBack(GAMESERVER(cmd_pri_game.SUB_GR_CANCEL_TABLE_RESULT), cmd_table);
    if (this._gameFrame.isSocketServer())
        this._gameFrame.onCloseSocket();
};

//解散后财富信息
PriFrame.prototype.onSubCancelTableScoreInfo = function( pData ){
    var cmd_table = ExternalFun.read_netdata(cmd_pri_game.CMD_GR_CurrenceRoomCard&&Beans, pData);
    //个人财富
    GlobalUserItem.dUserBeans = cmd_table.dbBeans;
    GlobalUserItem.lRoomCard = cmd_table.lRoomCard;
    //通知更新        
    var eventListener = cc.EventCustom.new(yl.RY_USERINFO_NOTIFY);
    eventListener.obj = yl.RY_MSG_USERWEALTH;
    cc.director.getEventDispatcher().dispatchEvent(eventListener);

    if ( null != this._callBack)
        this._callBack(GAMESERVER(SUB_GR_CURRECE_ROOMCARD_&&_BEAN), cmd_table);
};

//改变椅子数量
PriFrame.prototype.onSubChangeChairCount = function( pData ) {
    var cmd_table = ExternalFun.read_netdata(cmd_pri_game.CMD_GR_ChangeChairCount, pData);
    if (null != cmd_table.dwChairCount)
        this._gameFrame._wChairCount = cmd_table.dwChairCount;
};