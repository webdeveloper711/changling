//
// Author: Li
// Date: 2017-11-24 22:36:44
//
// 私人房数据管理 全局模式

// PriRoom = PriRoom || class("PriRoom")
// var private_define = appdf.req(appdf.CLIENT_SRC +"privatemode.header-cc.Define_Private")
// var cmd_private = appdf.req(appdf.CLIENT_SRC +"privatemode.header-cc.CMD_Private")
// var QueryDialog = appdf.req("app.views.layer.other.QueryDialog")
// var QueryLayer = appdf.req(appdf.EXTERNAL_SRC +"QueryLayer")

// 私人房模块
var MODULE = private_define.tabModule;
PriRoom.MODULE = MODULE;
// 私人房界面tag
var LAYTAG = private_define.tabLayTag;
PriRoom.LAYTAG = LAYTAG;
// 游戏服务器登陆操作定义
var L_ACTION = private_define.tabLoginAction;
PriRoom.L_ACTION = L_ACTION;
// 登陆服务器CMD
var cmd_pri_login = cmd_private.login;
// 游戏服务器CMD
var cmd_pri_game = cmd_private.game;

//var PriFrame = appdf.req(MODULE.PLAZAMODULE +"models-cc-cc.PriFrame");
//var RoomRecordLayer = appdf.req(MODULE.PLAZAMODULE +"views.RoomRecordLayer");

// roomID 输入界面
var NAME_ROOMID_INPUT = "___private_roomid_input_layername___";

//todo can't find in cc-js class
var targetPlatform = cc.sys.platform;
function PriRoom(){
    // 私人房大厅资源搜索路径
    //todo following two statement have to be fixed
    // this._searchPath = jsb.fileUtils.getWritablePath() + "client/src/privatemode/plaza/res";
    // jsb.fileUtils.addSearchPath(this._searchPath);
    // 私人房游戏资源搜索路径
    this._gameSearchPath = "";
    var self = this;
    //网络回调
    var privateCallBack = function (command, message, dataBuffer, notShow) {
        if (command != null && typeof command != "string" && typeof command != "Number") {
            if (command.m == cmd_pri_login.MDM_MB_PERSONAL_SERVICE)
                return self.onPrivateLoginServerMessage(command.s, message, dataBuffer, notShow);
            else if (command.m == cmd_pri_game.MDM_GR_PERSONAL_TABLE)
                return self.onPrivateGameServerMessage(command.s, message, dataBuffer, notShow);
        }
        else {
            self.popMessage(message, notShow);
            if (-1 == command)
                self.dismissPopWait();
        }
    };
    this._priFrame = new PriFrame(this, privateCallBack);

    this.reSet();
}

// 实现单例
PriRoom._instance = null;
PriRoom.getInstance = function( ) {
    if (null == PriRoom._instance) {
        PriRoom._instance = new PriRoom();
    }
    return PriRoom._instance;
};

PriRoom.prototype.reSet = function() {
    // 私人房模式游戏列表
    this.m_tabPriModeGame = [];
    // 私人房列表
    this.m_tabPriRoomList = [];
    // 创建记录
    this.m_tabCreateRecord = [];
    // 参与记录
    this.m_tabJoinRecord = [];
    // 私人房数据  CMD_GR_PersonalTableTip
    this.m_tabPriData = [];
    // 私人房属性 tagPersonalRoomOption
    this.m_tabRoomOption = [];
    this.m_tabAllRoomOption = [];
    // 私人房费用配置 tagPersonalTableParameter
    this.m_tabFeeConfigList = [];
    this.m_tabAllFeeConfigList = [];
    // 是否自己房主
    this.m_bIsMyRoomOwner = false;
    // 私人房桌子号( 进入/查到到的 )
    this.m_dwTableID = yl.INVALID_TABLE;
    // 选择的私人房配置信息
    this.m_tabSelectRoomConfig = [];

    // 大厅场景
    this._scene = null;
    // 网络消息处理层
    this._viewFrame = null;
    // 私人房信息层
    this._priView = null;

    // 游戏服务器登陆动作
    this.m_nLoginAction = L_ACTION.ACT_NULL;
    this.cbIsJoinGame = 0;
    // 是否已经取消桌子/退出
    this.m_bCancelTable = false;
    // 是否收到结算消息
    this.m_bRoomEnd = false;
    // 参与游戏记录(用于暂离游戏时能返回游戏)
    this.m_tabJoinGameRecord = [];
};

// 当前游戏是否开启私人房模式
PriRoom.prototype.isCurrentGameOpenPri = function( nKindID ) {
    return (this.m_tabPriModeGame[nKindID] || false);
};

// 获取私人房
PriRoom.prototype.getPriRoomByServerID = function( dwServerID ) {
    var currentGameRoomList = this.m_tabPriRoomList[GlobalUserItem.nCurGameKind];
    if (null == currentGameRoomList)
        return null;
    for (k in currentGameRoomList) {
        if (currentGameRoomList[k].wServerID == dwServerID && currentGameRoomList[k].wServerType == yl.GAME_GENRE_PERSONAL)
            return currentGameRoomList[k];
    }
    return null;
},

// 登陆私人房
PriRoom.prototype.onLoginRoom = function( dwServerID, bLockEnter ) {
    var pServer = this.getPriRoomByServerID(dwServerID);
    if (null == pServer) {
        cc.log("PriRoom server null");
        var curTag = null;
        if (null != this._scene && null != this._scene._sceneRecord)
            curTag = this._scene._sceneRecord[this._scene._sceneRecord.length-1];
        if (curTag == LAYTAG.LAYER_ROOMLIST)
            showToast(this._scene, "房间未找到, 请重试!", 2);
        return false;
    }

    // 登陆房间
    if (null != this._priFrame && null != this._priFrame._gameFrame) {
        bLockEnter = bLockEnter || false;
        // 锁表进入
        if (bLockEnter)
        // 动作定义
            PriRoom.getInstance().m_nLoginAction = PriRoom.L_ACTION.ACT_SEARCHROOM;
        this.showPopWait();
        GlobalUserItem.bPrivateRoom = pServer.wServerType == yl.GAME_GENRE_PERSONAL;
        this._priFrame._gameFrame.setEnterAntiCheatRoom(false);
        GlobalUserItem.nCurRoomIndex = pServer._nRoomIndex;

        var index = 0;
        for (var i = 0; i < _gameList.length; i++) {
            if (Number(_gameList[i]._KindID) == GlobalUserItem.nCurGameKind) { //查找营口麻将
                index = i;
                break;
            }
        }
        var entergame = _gameList[index];
        if (null != entergame) {
            this.m_bSingleGameMode = true;
            this._scene.updateEnterGameInfo(entergame);
        }

        this._scene.onStartGame();
        this.m_bCancelTable = false;
        return true;
    }
    return false;
};

//
PriRoom.prototype.onEnterPlaza = function( scene, gameFrame ) {
    this._scene = scene;
    this._priFrame._gameFrame = gameFrame;
};

PriRoom.prototype.onExitPlaza = function() {
    if (null != this._priFrame._gameFrame)
        this._priFrame._gameFrame = null;
    // cc.director.getTextureCache().removeUnusedTextures();
    // cc.spriteFrameCache().removeUnusedSpriteFrames();
    // 清理暂离记录
    this.m_tabJoinGameRecord = [];
    this.exitRoom();
};

PriRoom.prototype.onEnterPlazaFinish = function() {
    // 判断锁表
    if (GlobalUserItem.dwLockServerID != 0) {
        GlobalUserItem.nCurGameKind = GlobalUserItem.dwLockKindID;
        // 更新逻辑
        if (!this._scene.updateGame(GlobalUserItem.dwLockKindID)
            && !this._scene.loadGameList(GlobalUserItem.dwLockKindID)) {
            //
            var entergame = this._scene.getGameInfo(GlobalUserItem.dwLockKindID);
            ExternalFun.dump(entergame, "this._scene.getGameInfo", 3);
            if (null != entergame) {
                this._scene.updateEnterGameInfo(entergame);
                //启动游戏
                cc.log("PriRoom.prototype.onEnterPlazaFinish ==> lock pri game-cc");
                return [true, false, this.onLoginRoom(GlobalUserItem.dwLockServerID, true)];
            }
        }
        cc.log("PriRoom.prototype.onEnterPlazaFinish ==> lock && update game-cc");
        return [true, true, false];
    }
    cc.log("PriRoom.prototype.onEnterPlazaFinish ==> not lock game-cc");
    return [false, false, false];
},

// 登陆后进入房间列表
PriRoom.prototype.onLoginEnterRoomList = function() {
    // 判断是否开启私人房
    if (false == PriRoom.prototype.getInstance().isCurrentGameOpenPri(GlobalUserItem.nCurGameKind)) {
        cc.log("PriRoom.prototype.onLoginEnterRoomList: not open prigame");
        return false;
    }

    if (GlobalUserItem.dwLockServerID != 0 && GlobalUserItem.dwLockKindID == GlobalUserItem.nCurGameKind) {
        cc.log("PriRoom.prototype.onLoginEnterRoomList: onLoginRoom");
        //启动游戏
        return this.onLoginRoom(GlobalUserItem.dwLockServerID, true);
    }
    else {
        cc.log("PriRoom.prototype.onLoginEnterRoomList: this._scene.onChangeShowMode(PriRoom.prototype.LAYTAG.LAYER_ROOMLIST)");
        this._scene.onChangeShowMode(PriRoom.LAYTAG.LAYER_ROOMLIST);
        return true;
    }
},

PriRoom.prototype.onLoginPriRoomFinish = function() {
    cc.log("PriRoom.prototype.onLoginPriRoomFinish");
    var bHandled = false;
    if (null != this._viewFrame && null != this._viewFrame.onLoginPriRoomFinish) {
        bHandled = this._viewFrame.onLoginPriRoomFinish();
    }
    // 清理锁表
    GlobalUserItem.dwLockServerID = 0;
    GlobalUserItem.dwLockKindID = 0;

    if (!bHandled) {
        var meUser = this.getMeUserItem();
        if (null == meUser)
            return;
        ExternalFun.dump(meUser, "this.getMeUserItem()",1);
        if (meUser.cbUserStatus == yl.US_FREE || meUser.cbUserStatus == yl.US_NULL) {
            // 搜索登陆
            if (this.m_nLoginAction == L_ACTION.ACT_SEARCHROOM) {
                cc.log("PriRoom.prototype.onLoginPriRoomFinish [sendEnterPrivateGame]");
                this.showPopWait();
                // 进入游戏
                this.getNetFrame().sendEnterPrivateGame();
            }
            // 解散登陆
            else if (PriRoom.getInstance().m_nLoginAction == PriRoom.L_ACTION.ACT_DISSUMEROOM) {
                cc.log("PriRoom.onLoginPriRoomFinish [sendDissumeGame]");
                this.showPopWait();
                // 发送解散
                this.getNetFrame().sendDissumeGame(this.m_dwTableID);
            }
            else {
                this._priFrame._gameFrame.onCloseSocket();
                this._priFrame.onCloseSocket();
                GlobalUserItem.nCurRoomIndex = -1;
                // 退出游戏房间
                this._scene.onKeyBack();

            }
        }
        else if (meUser.cbUserStatus == yl.US_PLAYING || meUser.cbUserStatus == yl.US_READY || meUser.cbUserStatus == yl.US_SIT) {
            // 搜索登陆
            if (PriRoom.getInstance().m_nLoginAction == PriRoom.L_ACTION.ACT_SEARCHROOM
                || PriRoom.getInstance().m_nLoginAction == PriRoom.L_ACTION.ACT_ENTERTABLE) {
                cc.log("PriRoom.onLoginPriRoomFinish [SendGameOption]");
                this.showPopWait();
                // 切换游戏场景
                this._scene.onEnterTable();
                // 发送配置
                this._priFrame._gameFrame.SendGameOption();
            }

            // 解散登陆
            else if (PriRoom.getInstance().m_nLoginAction == PriRoom.L_ACTION.ACT_DISSUMEROOM) {
                cc.log("PriRoom.onLoginPriRoomFinish [sendDissumeGame]");
                this.showPopWait();
                // 发送解散
                this.getNetFrame().sendDissumeGame(this.m_dwTableID);
            }
        }
    }
},

// 用户状态变更( 进入、离开、准备 等)
PriRoom.prototype.onEventUserState = function(viewid, useritem, bLeave) {
    bLeave = bLeave || false;
    if (this.m_bCancelTable)
        return;
    if (null != this._priView && null != this._priView.onRefreshInviteBtn)
        this._priView.onRefreshInviteBtn();
},

PriRoom.prototype.popMessage = function(message, notShow) {
    notShow = notShow || false;
    if (typeof message == "string" && "" != message) {
        if (notShow || null == this._viewFrame)
            cc.log(message);
        else if (null != this._viewFrame) {
            //新增消息显示界面，当有设置时，则使用消息显示界面Ryu
            if (this._viewFrame.getToastlayer)
                showToast(this._viewFrame.getToastlayer(), message, 2);
            else
                showToast(this._viewFrame, message, 2);
        }
    }
},

PriRoom.prototype.onPrivateLoginServerMessage = function(result, message, dataBuffer, notShow) {
    this.popMessage(message, notShow);

    if (cmd_pri_login.SUB_MB_QUERY_PERSONAL_ROOM_LIST_RESULT == result
        || cmd_pri_login.SUB_GR_USER_QUERY_ROOM_SCORE_RESULT == result) {
        // 列表记录
        if (null != this._viewFrame && null != this._viewFrame.onReloadRecordList)
            this._viewFrame.onReloadRecordList();
    }
    else if (cmd_pri_login.SUB_MB_PERSONAL_FEE_PARAMETER == result)
        cc.log("PriRoom fee list call back");
    this.dismissPopWait();
},

PriRoom.prototype.onPrivateGameServerMessage = function(result, message, dataBuffer, notShow) {
    this.popMessage(message, notShow);
    this.dismissPopWait();
    var self = this;
    if (cmd_pri_game.SUB_GR_CREATE_SUCCESS == result) {
        // 创建成功
        if (null != this._viewFrame && null != this._viewFrame.onRoomCreateSuccess)
            this._viewFrame.onRoomCreateSuccess();
    }
    else if (cmd_pri_game.SUB_GR_CANCEL_TABLE == result) {
        cc.log("PriRoom  SUB_GR_CANCEL_TABLE");
        GlobalUserItem.bWaitQuit = true;
        this.m_bCancelTable = true;
        // 清理暂离记录
        this.m_tabJoinGameRecord[GlobalUserItem.nCurGameKind] = [];
        // 解散桌子
        var curTag = null;
        if (null != this._scene && null != this._scene._sceneRecord)
            curTag = this._scene._sceneRecord[this._scene._sceneRecord.length-1];
        if (curTag == yl.SCENE_GAME && !this.m_bRoomEnd) {
            var query = new QueryDialog(message.szDescribeString, function (ok) {
                GlobalUserItem.bWaitQuit = false;
                if (null != self._viewFrame && null != self._viewFrame.onExitRoom)
                    self._viewFrame.onExitRoom();
            });
            query.setCanTouchOutside(false);
            this._viewFrame.addChild(query);
            var zorder = 0;
            if (null != this._viewFrame.priGameLayerZorder)
                zorder = this._viewFrame.priGameLayerZorder() - 1;
            query.setLocalZOrder(zorder);
        }
        else
            showToast(this._viewFrame, message.szDescribeString, 2);
        this.m_bRoomEnd = false;
    }
    else if (cmd_pri_game.SUB_GR_CANCEL_REQUEST == result) {
        // 请求解散
        // message = game-cc.CMD_GR_CancelRequest
        var useritem = this._priFrame._gameFrame._UserList[message.dwUserID];
        if (null == useritem)
            return;
        var curTag = null;
        if (null != this._scene && null != this._scene._sceneRecord)
            curTag = this._scene._sceneRecord[this._scene._sceneRecord.length-1];
        if (null != this.m_queryQuit && null != this.m_queryQuit.getParent())
            this.m_queryQuit.removeFromParent();
        if (curTag == yl.SCENE_GAME) {
            this.m_queryQuit = new QueryDialog(useritem.szNickName + "请求解散房间, 是否同意?", function (ok) {
                if (ok)
                    self.getNetFrame().sendRequestReply(1);
                else
                    self.getNetFrame().sendRequestReply(0);
                self.m_queryQuit = null;
            });
            this.m_queryQuit.setCanTouchOutside(false);
            this._viewFrame.addChild(this.m_queryQuit);
        }
    }
    else if (cmd_pri_game.SUB_GR_REQUEST_REPLY == result) {
        // 请求答复
        // message = game-cc.CMD_GR_RequestReply
        var useritem = this._priFrame._gameFrame._UserList[message.dwUserID];
        if (null == useritem)
            return;
        var bHandled = false;
        if (null != this._viewFrame && null != this._viewFrame.onCancellApply)
            bHandled = this._viewFrame.onCancellApply(useritem, message);
        if (!bHandled) {
            var tips = "同意解散";
            if (0 == message.cbAgree)
                tips = "不同意解散";
            var curTag = null;
            if (null != this._scene && null != this._scene._sceneRecord)
                curTag = this._scene._sceneRecord[this._scene._sceneRecord.length-1];
            if (curTag == yl.SCENE_GAME)
                showToast(this._viewFrame, useritem.szNickName + tips, 2);
        }
    }
    else if (cmd_pri_game.SUB_GR_REQUEST_RESULT == result) {
        // 请求结果
        // message = game-cc.CMD_GR_RequestResult
        if (0 == message.cbResult) {
            var curTag = null;
            if (null != this._scene && null != this._scene._sceneRecord)
                curTag = this._scene._sceneRecord[this._scene._sceneRecord.length-1];
            if (curTag == yl.SCENE_GAME)
                showToast(this._viewFrame, "解散房间请求未通过", 2);
            return;
        }
        this.m_bCancelTable = true;
        var bHandled = false;
        if (null != this._viewFrame && null != this._viewFrame.onCancelResult)
            bHandled = this._viewFrame.onCancelResult(message);
        if (!bHandled) {
        }

    }
    else if (cmd_pri_game.SUB_GR_WAIT_OVER_TIME == result) {
        // 超时提示
        // message = game-cc.CMD_GR_WaitOverTime
        var useritem = this._priFrame._gameFrame._UserList[message.dwUserID];
        if (null == useritem)
            return;
        var curTag = null;
        if (null != this._scene && null != this._scene._sceneRecord)
            curTag = this._scene._sceneRecord[this._scene._sceneRecord.length-1];
        if (curTag == yl.SCENE_GAME) {
            var query = new QueryDialog(useritem.szNickName + "断线等待超时, 是否继续等待?", function (ok) {
                if (ok)
                    self.getNetFrame().sendRequestReply(0);
                else
                    self.getNetFrame().sendRequestReply(1);
                //this.showPopWait()
            }).setCanTouchOutside(false).addTo(this._viewFrame);
        }
    }
    else if (cmd_pri_game.SUB_GR_PERSONAL_TABLE_TIP == result) {
        // 游戏信息
        if (null != this._priView && null != this._priView.onRefreshInfo)
            this._priView.onRefreshInfo();
    }
    else if (cmd_pri_game.SUB_GR_PERSONAL_TABLE_END == result) {
        GlobalUserItem.bWaitQuit = true;
        // 屏蔽重连功能
        GlobalUserItem.bAutoConnect = false;
        // 清理暂离记录
        this.m_tabJoinGameRecord[GlobalUserItem.nCurGameKind] = [];
        // 结束消息
        if (null != this._priView && null != this._priView.onPriGameEnd)
            this._priView.onPriGameEnd(message, dataBuffer);
        this.m_bRoomEnd = true;
    }
    else if (cmd_pri_game.SUB_GR_CANCEL_TABLE_RESULT == result) {
        // 解散结果
        // message = game-cc.CMD_GR_DissumeTable
        if (1 == message.cbIsDissumSuccess)
            showToast(this._viewFrame, "解散成功", 2);
        // 更新创建记录
        for (k in this.m_tabCreateRecord) {
            if (message.szRoomID == this.m_tabCreateRecord[k].szRoomID) {
                this.m_tabCreateRecord[k].cbIsDisssumRoom = 1;
                this.m_tabCreateRecord[k].sysDissumeTime = message.sysDissumeTime;
                var tt = this.m_tabCreateRecord[k].sysDissumeTime;
                this.m_tabCreateRecord[k].sortTimeStmp = new Date(tt.wYear, tt.wMonth, tt.wDay, tt.wHour, tt.wMinute, tt.wSecond).getTime();
                break;
            }
        }
        // 排序


        for (i = 0; i < PriRoom.getInstance().m_tabCreateRecord.length; i++) {
            for (var j = i; j < PriRoom.getInstance().m_tabCreateRecord.length; j++) {
                if (PriRoom.getInstance().m_tabCreateRecord[i].cbIsDisssumRoom != PriRoom.getInstance().m_tabCreateRecord[j].cbIsDisssumRoom) {
                    if (PriRoom.getInstance().m_tabCreateRecord[i].cbIsDisssumRoom < PriRoom.getInstance().m_tabCreateRecord[j].cbIsDisssumRoom) {
                        temp = PriRoom.getInstance().m_tabCreateRecord[i];
                        PriRoom.getInstance().m_tabCreateRecord[i] = PriRoom.getInstance().m_tabCreateRecord[j];
                        PriRoom.getInstance().m_tabCreateRecord[j] = temp;
                    }
                } else if (PriRoom.getInstance().m_tabCreateRecord[i].cbIsDisssumRoom == 0 && PriRoom.getInstance().m_tabCreateRecord[i].cbIsDisssumRoom == PriRoom.getInstance().m_tabCreateRecord[j].cbIsDisssumRoom) {
                    if (PriRoom.getInstance().m_tabCreateRecord[i].createTimeStmp > PriRoom.getInstance().m_tabCreateRecord[j].createTimeStmp) {
                        temp = PriRoom.getInstance().m_tabCreateRecord[i];
                        PriRoom.getInstance().m_tabCreateRecord[i] = PriRoom.getInstance().m_tabCreateRecord[j];
                        PriRoom.getInstance().m_tabCreateRecord[j] = temp;
                    }
                } else {
                    if (PriRoom.getInstance().m_tabCreateRecord[i].sortTimeStmp > PriRoom.getInstance().m_tabCreateRecord[j].sortTimeStmp) {
                        temp = PriRoom.getInstance().m_tabCreateRecord[i];
                        PriRoom.getInstance().m_tabCreateRecord[i] = PriRoom.getInstance().m_tabCreateRecord[j];
                        PriRoom.getInstance().m_tabCreateRecord[j] = temp;
                    }
                }
            }
        }
        // table.sort( PriRoom.prototype.getInstance().m_tabCreateRecord, function(a, b)
        //     if a.cbIsDisssumRoom != b.cbIsDisssumRoom)
        //         return a.cbIsDisssumRoom > b.cbIsDisssumRoom
        //     else if(a.cbIsDisssumRoom == 0 && a.cbIsDisssumRoom == b.cbIsDisssumRoom)
        //         return a.createTimeStmp < b.createTimeStmp
        //     else
        //         return a.sortTimeStmp < b.sortTimeStmp
        //     end
        // end )
        //刷新列表
        if (null != this._viewFrame && null != this._viewFrame.onReloadRecordList)
            this._viewFrame.onReloadRecordList();
    }
    else if (cmd_pri_game.SUB_GF_PERSONAL_MESSAGE == result) {
        if (null == this._viewFrame) {
            this._scene.onKeyBack();
            return;
        }
        var query = new QueryDialog(message.szMessage, function (ok) {
            if (null != self._viewFrame && null != self._viewFrame.onExitRoom)
                self._viewFrame.onExitRoom();
        }, null, 1);
        query.setCanTouchOutside(false);
        query.addTo(this._viewFrame);
    }
    else if (cmd_pri_game.SUB_GR_CURRECE_ROOMCARD_AND_BEAN == result) {
        // 解散后游戏信息
        if (null != this._viewFrame && null != this._viewFrame.onRefreshInfo)
            this._viewFrame.onRefreshInfo();
    }
},

// 网络管理
PriRoom.prototype.getNetFrame = function() {
    return this._priFrame;
},

// 设置网络消息处理层
PriRoom.prototype.setViewFrame = function(viewFrame) {
    this._viewFrame = viewFrame;
},

// 获取自己数据
PriRoom.prototype.getMeUserItem = function() {
    return this._priFrame._gameFrame.GetMeUserItem();
},

// 获取游戏玩家数(椅子数)
PriRoom.prototype.getChairCount = function() {
    return this._priFrame._gameFrame.GetChairCount();
},

// 获取大厅场景
PriRoom.prototype.getPlazaScene = function() {
    return this._scene;
},

// 界面切换
PriRoom.prototype.getTagLayer = function(tag, param, scene) {
    if (LAYTAG.LAYER_ROOMLIST == tag) {
        this.exitRoom();
        // 设置搜索路径
        this.enterRoom(scene);
        // 房间列表
        var list = new PriRoomListLayer(scene);
        // 绑定回调
        this.setViewFrame(list);
        return list;
    } else if (LAYTAG.LAYER_CREATEPRIROOME == tag) {
        // 创建私人房
        var entergame = scene.getEnterGameInfo();
        if (null != entergame) {
            var modulestr = entergame._KindName.replace("%.", "/");
            var roomCreateFile = "";
            //TODO
            if ((cc.sys.WIN32 === targetPlatform) || (cc.sys.MOBILE_BROWSER === targetPlatform) || (cc.sys.DESKTOP_BROWSER === targetPlatform))
                 roomCreateFile = "game/" + modulestr + "src/privateroom/PriRoomCreateLayer.js";
            else
                 roomCreateFile = "game/" + modulestr + "src/privateroom/PriRoomCreateLayer.js";
            if ( cc.sys.isNative ) {
                if( jsb.fileUtils.isFileExist(roomCreateFile) ){
                    var lay = appdf.req(roomCreateFile).create(scene);
                    // 绑定回调
                    this.setViewFrame(lay);
                    lay.setLocalZOrder(5);
                    return lay;
                }
            }
            // 绑定回调
            var lay = new PriRoomCreateLayer(scene);
            this.setViewFrame(lay);
            lay.setLocalZOrder(5);
            return lay;
        }
    } else if (LAYTAG.LAYER_MYROOMRECORD == tag) {
        // 记录界面
        var lay = new RoomRecordLayer(scene, RoomRecordLayer.CHOICE_CREAT);
        // 绑定回调
        this.setViewFrame(lay);
        return lay;
    } else if (LAYTAG.LAYER_ROOMID == tag) {
        // ROOMID 界面
        var runScene = this.getPlazaScene();
        if (null != runScene) {
            var roomidLayer = runScene.getChildByName(NAME_ROOMID_INPUT);
            if (null == roomidLayer) {
                // TODO : check this command
                // roomidLayer = appdf.req(MODULE.PLAZAMODULE + "views.RoomIdInputLayer").create();
                roomidLayer = new RoomIdInputLayer();
                roomidLayer.setName(NAME_ROOMID_INPUT);
                runScene.addChild(roomidLayer);
            }
            roomidLayer.showLayer(true);
            roomidLayer.setLocalZOrder(5);
        }
    }
    else if (LAYTAG.LAYER_CREATERESULT == tag) {
        // 创建结果
        if (null != this._scene) {
            var title = new RoomCreateResult(); // TODO will check param
            var query = new QueryLayer(this._scene, title, null, param, false, QueryLayer.TYPE_NORMALTYPE, false);
        }
    }
    else if (LAYTAG.LAYER_EXCHANGESCORE == tag) {
        // 房卡兑换游戏币
        var tab = [];
        tab.exchangeRate = param || 0;
        tab.bExchange = true;
        var lay = new RoomCardShopLayer(tab);
        // 绑定回调
        this.setViewFrame(lay);
        return lay;
    }
    else if (LAYTAG.LAYER_BUYCARD == tag) {
        // 购买房卡
        var tab = [];
        tab.item = param || [];
        tab.bExchange = false;
        var lay = new RoomCardShopLayer(tab);
        // 绑定回调
        this.setViewFrame(lay);
        return lay;
    }
    else if (LAYTAG.LAYER_FRIENDLIST == tag) {
        // 好友分享
        var runScene = this.getPlazaScene();
        if (null != runScene) {
            var friendshare = new FriendShareListLayer(param);
            runScene.addChild(friendshare, yl.MAX_INT - 2);
        }
    }
    else if (LAYTAG.LAYER_COMBATLOG == tag) {
        // 记录界面
        var lay = new RoomRecordLayer(scene, RoomRecordLayer.CHOICE_ONLYJOIN);
        // 绑定回调
        this.setViewFrame(lay);
        return lay;
    }
},

// 主界面是否顶、底栏
PriRoom.haveBottomTop = function( tag ) {
    return ( tag == LAYTAG.LAYER_CREATEPRIROOME || tag == LAYTAG.LAYER_MYROOMRECORD || tag == LAYTAG.LAYER_ROOMLIST );
},

// 是否快速开始
PriRoom.enableQuickStart= function( tag ){
    return !( tag == LAYTAG.LAYER_CREATEPRIROOME || tag == LAYTAG.LAYER_MYROOMRECORD || tag == LAYTAG.LAYER_ROOMLIST);
},

// 是否房间界面
PriRoom.isRoomListLayer = function( tag ){
    return (tag == LAYTAG.LAYER_ROOMLIST || tag == yl.SCENE_ROOMLIST );
},

// 是否返回按钮
PriRoom.enableBackBtn = function( tag ){
    return ( tag == LAYTAG.LAYER_CREATEPRIROOME || tag == LAYTAG.LAYER_MYROOMRECORD );
},

//获取配置信息
PriRoom.prototype.getOption = function( dwKindID ){
    var kindID = Number(dwKindID);
    this.m_tabRoomOption = [];
    this.m_tabFeeConfigList = [];
    for (var i = 0 ; i<this.m_tabAllRoomOption.length ; i++) {
        if (this.m_tabAllRoomOption[i].dwKindID == kindID) {
            this.m_tabRoomOption = this.m_tabAllRoomOption[i];
            this.cbIsJoinGame = this.m_tabRoomOption.cbIsJoinGame;
        }
    }

    for (i = 0 ; i<this.m_tabAllFeeConfigList.length ; i++) {
        if (this.m_tabAllFeeConfigList[i].dwKindID == kindID)
            this.m_tabFeeConfigList.push(this.m_tabAllFeeConfigList[i]);
    }
};

// 进入游戏房间
PriRoom.prototype.enterRoom = function( scene ){
    return; //TODO insert by han
    var entergame = scene.getEnterGameInfo();
    var bPirMode = PriRoom.prototype.getInstance().isCurrentGameOpenPri(GlobalUserItem.nCurGameKind);
    if (null != entergame && true == bPirMode && "" == this._gameSearchPath) {
        // var modulestr = string.gsub(entergame._KindName, "%.", "/");
        var modulestr = entergame._KindName.replace("%.", "/");
        this._gameSearchPath = jsb.fileUtils.getWritablePath() + "game-cc/" + modulestr + "/res/privateroom/";
        jsb.fileUtils.addSearchPath(this._gameSearchPath);
    }
};

PriRoom.prototype.exitRoom = function( ){
    //重置搜索路径
    //todo all
    // var oldPaths = jsb.fileUtils.getSearchPaths();
    // var newPaths = [];
    // for (k in oldPaths) {
    //     if (String(oldPaths[k]) != String(this._gameSearchPath))
    //         newPaths.push(oldPaths[k]);
    // }
    // jsb.fileUtils.setSearchPaths(newPaths);
    // this._gameSearchPath = "";
    // this.setViewFrame(null);
};

// 进入游戏界面
PriRoom.prototype.enterGame = function(gameLayer, scene){
    this.enterRoom(scene);
    var entergame = scene.getEnterGameInfo();
    if (null != entergame) {
        // var modulestr = string.gsub(entergame._KindName, "%.", "/")
        var modulestr = entergame._KindName.replace("%.", "/");
        var gameFile = "";
        // if ((cc.sys.WIN32 === targetPlatform) || (cc.sys.MOBILE_BROWSER === targetPlatform) || (cc.sys.DESKTOP_BROWSER === targetPlatform))
        //     gameFile = "game/" + modulestr + "src/privateroom/PriGameLayer.js";
        // else
        //     gameFile = "game/" + modulestr + "src/privateroom/PriGameLayer.luac";
        // if (jsb.fileUtils.isFileExist(gameFile)) {
        //     var lay = appdf.req(gameFile).create(gameLayer);
            var lay = new PriGameLayer(gameLayer);
            if (null != lay) {
                gameLayer.addPrivateGameLayer(lay);
                gameLayer._gameView._priView = lay;
                this._priView = lay;
            }
            // 绑定回调
            this.setViewFrame(gameLayer);
     //   }
    }
    this.m_bRoomEnd = false;
};

// 退出游戏界面
PriRoom.prototype.exitGame = function(){
    if (null != this._viewFrame && null != this._viewFrame.onExitRoom)
        this._viewFrame.onExitRoom();
    this._priView = null;
};

PriRoom.prototype.showPopWait = function(){
    this._scene.showPopWait();
};

PriRoom.prototype.dismissPopWait = function(){
    this._scene.dismissPopWait();
};

// 请求退出游戏
PriRoom.prototype.queryQuitGame = function( cbGameStatus ){
    if (this.m_bCancelTable) {
        cc.log("PriRoom.prototype.queryQuitGame 已经取消!");
        return;
    }
    if (1 == PriRoom.getInstance().cbIsJoinGame && PriRoom.getInstance().m_bIsMyRoomOwner) {
        var tip = "你是房主, 是否要解散该房间?";
        var query = new QueryDialog(tip, function (ok) {
            if (ok == true)
            //this.showPopWait()
                this.getNetFrame().sendRequestDissumeGame();
        });
        query.setCanTouchOutside(false);
        this._viewFrame.addChild(query);
        return;
    }

    // 未玩且free
    if (0 == PriRoom.getInstance().m_tabPriData.dwPlayCount
        && 0 == cbGameStatus) {
        this._scene.onKeyBack();
        return;
    }

    if (this.m_queryQuitLeave != null) return;
    var tip = "约战房在游戏中退出需其他玩家同意, 是否申请解散房间?";
    this.m_queryQuitLeave = new QueryDialog(tip, function(ok) {
        if (ok == true)
        //this.showPopWait()
            this.getNetFrame().sendRequestDissumeGame();
        if (this.m_queryQuitLeave != null)
            this.m_queryQuitLeave.dismiss2();
        this.m_queryQuitLeave = null;
    });
    this.m_queryQuitLeave.setCanTouchOutside(false);
    this._viewFrame.addChild(this.m_queryQuitLeave);
};

// 请求解散房间
PriRoom.prototype.queryDismissRoom = function(){
    if (this.m_bCancelTable) {
        cc.log("PriRoom.prototype.queryQuitGame 已经取消!");
        return;
    }
    if (this.m_queryQuitLeave != null) return;
    var tip = "约战房在游戏中退出需其他玩家同意, 是否申请解散房间?";
    var self = this;
    this.m_queryQuitLeave = new QueryDialog(tip, function(ok) {
        if (ok == true) {
            //this.showPopWait()
            self.getNetFrame().sendRequestDissumeGame();
        }
        if (self.m_queryQuitLeave != null) {
            self.m_queryQuitLeave.dismiss2();
        }
        self.m_queryQuitLeave = null;
    });
    this.m_queryQuitLeave.setCanTouchOutside(false);
    this._viewFrame.addChild(this.m_queryQuitLeave);
};

// 获取邀请分享内容
PriRoom.prototype.getInviteShareMsg = function( roomDetailInfo ){
    var entergame = this._scene.getEnterGameInfo();
    if (null != entergame) {
        // var modulestr = string.gsub(entergame._KindName, "%.", "/")
        var modulestr = entergame._KindName.replace("%.", "/");
        var gameFile = "";
        if ((cc.sys.WIN32 === targetPlatform) || (cc.sys.MOBILE_BROWSER === targetPlatform) || (cc.sys.DESKTOP_BROWSER === targetPlatform))
            gameFile = "game-cc/" + modulestr + "src/privateroom/PriRoomCreateLayer.js";
        else
            gameFile = "game-cc/" + modulestr + "src/privateroom/PriRoomCreateLayer.js"; //PriRoomCreateLayer.luac
        if (jsb.fileUtils.isFileExist(gameFile))
            return appdf.req(gameFile).GetInviteShareMsg(roomDetailInfo);
    }
    return {title : "", content : ""};
},

// 私人房邀请好友
PriRoom.prototype.priInviteFriend = function(frienddata, gameKind, wServerNumber, tableId, inviteMsg){
    if (null == frienddata || null == this._scene.inviteFriend)
        return;
    if (!gameKind)
        gameKind = GlobalUserItem.nCurGameKind;
    else
        gameKind = Number(gameKind);
    var id = frienddata.dwUserID;
    if (null == id)
        return;
    var tab = [];
    tab.dwInvitedUserID = id;
    tab.wKindID = gameKind;
    tab.wServerNumber = wServerNumber || 0;
    tab.wTableID = tableId || 0;
    tab.szInviteMsg = inviteMsg || "";
    if (FriendMgr.getInstance().sendInvitePrivateGame(tab)) {
        var runScene = cc.director.getRunningScene();
        if (null != runScene)
            showToast(runScene, "邀请消息已发送!", 1);
    }
},

// 分享图片给好友
PriRoom.prototype.imageShareToFriend = function( frienddata, imagepath, sharemsg ){
    if (null == frienddata || null == this._scene.imageShareToFriend)
        return;
    var id = frienddata.dwUserID;
    if (null == id)
        return;
    this._scene.imageShareToFriend(id, imagepath, sharemsg);
},

// 暂离游戏
// @param[nKindId] 游戏kindid
// @param[szRoomId] 房间id
PriRoom.prototype.tempLeaveGame = function( nKindId, szRoomId ) {
    nKindId = nKindId || GlobalUserItem.nCurGameKind;
    szRoomId = szRoomId || this.m_tabPriData.szServerID;
    var joinGame = {roomid: szRoomId};
    this.m_tabJoinGameRecord[nKindId] = joinGame;

    this.getNetFrame()._gameFrame.setEnterAntiCheatRoom(false);
    this.getNetFrame()._gameFrame.onCloseSocket();
}