//
// Author. zhong
// Date. 2016-12-21 15.21.11
//
// 私人房模式 房间列表
var BTN_NORMAL_ROOMLIST   = 101;               // 普通房间列表
var BTN_JOIN_PRIROOM      = 102;               // 加入房间
var BTN_CREATE_PRIROOM    = 103;               // 创建房间
var BTN_BACK_GAME         = 104;
var PriRoomListLayer = cc.Layer.extend({
    // 返回游戏
    ctor : function( scene ) {
        this._super();
        ExternalFun.registerNodeEvent(this);
        GlobalUserItem.nCurRoomIndex = -1;

        this._scene = scene;
        this._isRoom = false;

        // 加载csb资源
        var rootLayer, csbNode = ExternalFun.loadRootCSB("room/PriRoomListLayer.csb", this);

        var touchFunC = function (ref, tType) {
            if (tType == ccui.Widget.TOUCH_ENDED) {
                this.onButtonClickedEvent(ref.getTag(), ref);
            }
        }
        // 无普通房间
        var bHaveNormalRoom = (0 != GlobalUserItem.GetGameRoomCount());
        // 普通房间列表
        var btn = csbNode.getChildByName("btn_roomlist");
        btn.setTag(BTN_NORMAL_ROOMLIST);
        btn.setVisible(bHaveNormalRoom);
        btn.setEnabled(bHaveNormalRoom);
        btn.addTouchEventListener(touchFunC);
        btn.setPositionY(-444);

        // 加入房间
        btn = csbNode.getChildByName("btn_joinroom");
        btn.setTag(BTN_JOIN_PRIROOM);
        btn.addTouchEventListener(touchFunC);
        var joinBtn = btn;

        // 创建房间
        btn = csbNode.getChildByName("btn_createroom");
        btn.setTag(BTN_CREATE_PRIROOM);
        btn.addTouchEventListener(touchFunC);
//    var createBtn = btn
//    if ( not bHaveNormalRoom ) { 
//        joinBtn.setPositionX(392)
//        createBtn.setPositionX(940)
//    end


        btn = csbNode.getChildByName("btn_close");
        btn.setTag(BTN_BACK_GAME);
        btn.addTouchEventListener(touchFunC);

        this._scene.showPopWait();
        // 请求私人房配置
        PriRoom.getInstance().getNetFrame().onGetRoomParameter();
    },

    onButtonClickedEvent : function( tag, sender ) {
        if (BTN_NORMAL_ROOMLIST == tag) {
            // 重置搜索路径
            PriRoom.getInstance().exitRoom();
            this._scene.onChangeShowMode(yl.SCENE_ROOMLIST);
        } else if (BTN_JOIN_PRIROOM == tag) {
            PriRoom.getInstance().getTagLayer(PriRoom.LAYTAG.LAYER_ROOMID);
        } else if (BTN_CREATE_PRIROOM == tag) {
            //PriRoom.getInstance().getTagLayer(PriRoom.LAYTAG.LAYER_CREATEPRIROOME)
            PriRoom.getInstance().getTagLayer(PriRoom.LAYTAG.LAYER_CREATEPRIROOME);
//        this.getParent().getParent().onChangeShowMode(yl.SCENE_CREATROOM)
//        this.getParent().getParent().onChangeShowMode(PriRoom.LAYTAG.LAYER_CREATEPRIROOME)
            //this._scene.onChangeShowMode(PriRoom.LAYTAG.LAYER_CREATEPRIROOME)
        } else if (BTN_BACK_GAME == tag) {
            this._scene.onKeyBack();
            //PriRoom.getInstance().showPopWait()
            //PriRoom.getInstance().getNetFrame().onSearchRoom(this.m_szBackGameRoomId)
        }
    },

    onEnterTransitionFinish : function() {
    },

    getIsRoom : function(args) {
        this._isRoom = args;
    },


});
// var ExternalFun = appdf.req(appdf.EXTERNAL_SRC + "ExternalFun")

