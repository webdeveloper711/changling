/*  Author  :   JIN */

var BTN_ADD = 101;
var BTN_SUB = 102;
var BTN_BLANK = 103;
var BTN_EXCHANGE = 104;  // 兑换
var BTN_BUY = 105; // 购买
var CBT_NULL = 200;
var CBT_BEAN = 201; // 游戏豆选择
var CBT_GOLD = 202; // 游戏币选择
var CBT_INGOT =203; // 元宝选择
// param[tabParam] 参数列表
var RoomCardShopLayer = cc.Layer.extend({

    onExit : function(){
        if (this._shopDetailFrame.isSocketServer()) {
            this._shopDetailFrame.onCloseSocket();
        }
        if (null != this._shopDetailFrame._gameFrame) {
            this._shopDetailFrame._gameFrame._shotFrame = null;
            this._shopDetailFrame._gameFrame = null;
        }

        if (null != this.m_listener) {
            cc.director().getEventDispatcher().removeEventListener(this.m_listener);
            this.m_listener = null;
        }
    },

    ctor : function( tabParam ) {
        this._super();
        this.m_tabParam = tabParam;
        // this.registerScriptHandler(function (eventType) {
        //     if (eventType == "enterTransitionFinish") {
        //
        //     } else if (eventType == "exitTransitionStart") {
        //     } else if (eventType == "exit") {
        //         if (this._shopDetailFrame.isSocketServer()) {
        //             this._shopDetailFrame.onCloseSocket();
        //         }
        //         if (null != this._shopDetailFrame._gameFrame) {
        //             this._shopDetailFrame._gameFrame._shotFrame = null;
        //             this._shopDetailFrame._gameFrame = null;
        //         }
        //
        //         if (null != this.m_listener) {
        //             cc.director().getEventDispatcher().removeEventListener(this.m_listener);
        //             this.m_listener = null;
        //         }
        //     }
        // });
        //网络回调
        var shopDetailCallBack = function (result, message) {
            return this.onShopDetailCallBack(result, message);
        };

        var gameFrame = PriRoom.getInstance().getNetFrame()._gameFrame;
        //网络处理
        this._shopDetailFrame = new ShopDetailFrame(this, shopDetailCallBack);
        this._shopDetailFrame._gameFrame = gameFrame;
        if (null != gameFrame) {
            gameFrame._shotFrame = this._shopDetailFrame;
        }

        var frame = cc.spriteFrameCache.getSpriteFrame("sp_public_frame_0.png");

        var sp = new cc.Sprite(res.frame_detail_0_png);
        sp.setPosition(yl.WIDTH * 0.5, yl.HEIGHT * 0.5);
        this.addChild(sp);

        var btn = new ccui.Button(res.Detail_btn_close_png, res.Detail_btn_close_png);
        btn.setPosition(yl.WIDTH * 0.73, yl.HEIGHT * 0.78);
        this.addChild(btn);
        btn.addTouchEventListener(function (ref, type) {
            if (type == ccui.Widget.TOUCH_ENDED) {
                PriRoom.getInstance().getPlazaScene().onKeyBack();
            }
        });

        //按钮回调
        this._btcallback = function (ref, type) {
            if (type == ccui.Widget.TOUCH_ENDED) {
                this.onButtonClickedEvent(ref.getTag(), ref);
            }
        };

        this._nCount = 1;
        this.m_buyRate = 0;
        // 加载csb资源
        var result = ExternalFun.loadRootCSB(res.RoomCardShopLayer_json, this);
        rootLayer = result.rootlayer;
        csbNode = result.csbnode;
        var bExchange = tabParam.bExchange;
        if (null == bExchange) {
            return;
        }

        if (bExchange) {
            this.initExchangeUI(csbNode);
        } else {
            this.initBuyUI(csbNode);
        }
        this.onUpdateNum();
    },

// 兑换ui
    initExchangeUI : function(csbNode) {
        var exchangeUi = csbNode.getChildByName("exchange_panel");
        exchangeUi.setVisible(true);
        var buyUi = csbNode.getChildByName("buy_panel");
        buyUi.setVisible(false);
        this.m_buyUi = buyUi;

        var editHanlder = function (event, editbox) {
            this.onEditEvent(event, editbox);
        };
        // 编辑框
        var editbox = new cc.EditBox(cc.size(300, 48), res.frame_detail_2_png);
        editbox.setPosition(cc.p(807, 526));
        editbox.setFontName(res.round_body_ttf);
        editbox.setPlaceholderFontName(res.round_body_ttf);
        editbox.setFontSize(30);
        editbox.setFontColor(cc.color(250, 204, 38));
        editbox.setPlaceholderFontSize(30);
        edotbox.setInputMode(cc.EDITBOX_INPUT_MODE_NUMERIC);
        exchangeUi.addChild(editbox);
        editbox.string = "1";
        editbox.setVisible(false);
        // editbox.registerScriptEditBoxHandler(editHanlder);
        this.m_editNumber = editbox;

        var btn = new ccui.Button(res.frame_detail_2_png, res.frame_detail_2_png, res.frame_detail_2_png);
        btn.setScale9Enabled(true);
        btn.setContentSize(cc.size(300, 48));
        btn.setPosition(cc.p(807, 526));
        btn.setTag(BTN_BLANK);
        btn.addTouchEventListener(this._btcallback);
        exchangeUi.addChild(btn);

        // 加减按钮
        btn = exchangeUi.getChildByName("btn_add");
        btn.setTag(BTN_ADD);
        btn.addTouchEventListener(this._btcallback);

        btn = exchangeUi.getChildByName("btn_sub");
        btn.setTag(BTN_SUB);
        btn.addTouchEventListener(this._btcallback);

        // 兑换按钮
        btn = exchangeUi.getChildByName("btn_exchange");
        btn.setTag(BTN_EXCHANGE);
        btn.addTouchEventListener(this._btcallback);

        // 数量
        this.m_atlasCount = exchangeUi.getChildByName("atlas_count");

        // 兑换数量
        this.m_atlasExchangeCount = exchangeUi.getChildByName("atlas_exchange");

        // 剩余
        this.m_textLeft = exchangeUi.getChildByName("txt_left");
        this.m_textLeft.setString("剩余" + GlobalUserItem.lRoomCard);

        this.m_listener = new cc.EventListenerCustom(yl.RY_USERINFO_NOTIFY, handler(this, this.onUserInfoChange));
        cc.director().getEventDispatcher().addEventListenerWithSceneGraphPriority(this.m_listener, this);
    },

// 购买ui
    initBuyUI : function(csbNode) {
        var exchangeUi = csbNode.getChildByName("exchange_panel");
        exchangeUi.setVisible(false);
        var buyUi = csbNode.getChildByName("buy_panel");
        buyUi.setVisible(true);
        this.m_buyUi = buyUi;

        var editHanlder = function (event, editbox) {
            this.onEditEvent(event, editbox);
        };
        // 编辑框
        var editbox = new cc.EditBox(cc.size(300, 48), ccui.Scale9Sprite(res.frame_detail_2_png));
        editbox.setPosition(cc.p(807, 526));
        editbox.setFontName(res.round_body_ttf);
        editbox.setPlaceholderFontName(res.round_body_ttf);
        editbox.setFontSize(30);
        editbox.setFontColor(cc.color(250, 204, 38));
        editbox.setPlaceholderFontSize(30);
        editbox.setInputMode(cc.EDITBOX_INPUT_MODE_NUMERIC);
        buyUi.addChild(editbox);
        editbox.string = "1";
        editbox.setVisible(false);
        // editbox.registerScriptEditBoxHandler(editHanlder);
        this.m_editNumber = editbox;

        var btn = new ccui.Button(res.frame_detail_2_png, res.frame_detail_2_png, res.frame_detail_2_png);
        btn.setScale9Enabled(true);
        btn.setContentSize(cc.size(200, 48));
        btn.setPosition(cc.p(807, 526));
        btn.setTag(BTN_BLANK);
        btn.addTouchEventListener(this._btcallback);
        buyUi.addChild(btn, -1);

        // 加减按钮
        btn = buyUi.getChildByName("btn_add");
        btn.setTag(BTN_ADD);
        btn.addTouchEventListener(this._btcallback);

        btn = buyUi.getChildByName("btn_sub");
        btn.setTag(BTN_SUB);
        btn.addTouchEventListener(this._btcallback);

        // 购买按钮
        btn = buyUi.getChildByName("btn_buy");
        btn.setTag(BTN_BUY);
        btn.setPosition(650, 200);
        btn.addTouchEventListener(this._btcallback);

        // 数量
        this.m_atlasCount = buyUi.getChildByName("atlas_count");

        // 价格
        this.m_atlasItemPrice = buyUi.getChildByName("atlas_price");
        // 类型
        this.m_txtItemPriceType = buyUi.getChildByName("txt_pricetype");
        // 折扣价格
        this.m_atlasDiscountPrice = buyUi.getChildByName("atlas_price_discount");
        // 折后类型
        this.m_txtDiscountPriceType = buyUi.getChildByName("txt_pricetype_discount");
        // 购买价格
        this.m_atlasBuyPrice = buyUi.getChildByName("atlas_price_buy");
        // 购买类型
        this.m_txtBuyPriceType = buyUi.getChildByName("txt_pricetype_buy");

        var cbtlistener = function (sender, eventType) {
            this.onSelectedEvent(sender.getTag(), sender, eventType);
        };
        this.m_nSelect = CBT_NULL;
        // 类型选择
        var y = 300;
        // 游戏豆
        if (null != this.m_tabParam.item.bean && 0 != this.m_tabParam.item.bean) {
//        cc.Label.createWithTTF(string_formatNumberThousands(this.m_tabParam.item.bean,true,",")+"游戏豆", "fonts/round_body.ttf", 24)
//            .setAnchorPoint(cc.p(0.0,0.5))
//            .move(144,y)
//            .setTextColor(cc.color(255,255,0,255))
//            .addTo(buyUi)
//        ccui.CheckBox.create("Shop/Detail/cbt_detail_0.png","","Shop/Detail/cbt_detail_1.png","","")
//            .move(110,y)
//            .addTo(buyUi)
//            .setSelected(true)
//            .setTag(CBT_BEAN)
//            .addEventListener(cbtlistener)
            y = y - 61;
            this.m_nSelect = CBT_BEAN;
            this.m_buyRate = this.m_tabParam.item.bean;
        }
        // 游戏币
        if (null != this.m_tabParam.item.gold && 0 != this.m_tabParam.item.gold) {
            var bSelect = false;
            if (null == this.m_tabParam.item.bean || 0 == this.m_tabParam.item.bean) {
                this.m_nSelect = CBT_GOLD;
                bSelect = true;
                this.m_txtItemPriceType.setString("游戏币");
                this.m_buyRate = this.m_tabParam.item.gold;
            }

//        cc.Label.createWithTTF(string_formatNumberThousands(this.m_tabParam.item.gold,true,",")+"游戏币", "fonts/round_body.ttf", 24)
//            .setAnchorPoint(cc.p(0.0,0.5))
//            .move(144,y)
//            .setTextColor(cc.color(255,255,0,255))
//            .addTo(buyUi)
//        ccui.CheckBox.create("Shop/Detail/cbt_detail_0.png","","Shop/Detail/cbt_detail_1.png","","")
//            .move(110,y)
//            .addTo(buyUi)
//            .setSelected(bSelect)
//            .setTag(CBT_GOLD)
//            .addEventListener(cbtlistener)
        }
        // 元宝
        if (null != this.m_tabParam.item.ingot && 0 != this.m_tabParam.item.ingot) {
            var bSelect = false;
            if (null != this.m_tabParam.item.ingot || 0 != this.m_tabParam.item.ingot) {
                this.m_nSelect = CBT_INGOT;
                bSelect = true;
                this.m_txtItemPriceType.setString("元宝");
                this.m_buyRate = this.m_tabParam.item.ingot;
            }

//        cc.Label.createWithTTF(string_formatNumberThousands(this.m_tabParam.item.ingot,true,",")+"元宝", "fonts/round_body.ttf", 24)
//            .setAnchorPoint(cc.p(0.0,0.5))
//            .move(144,y)
//            .setTextColor(cc.color(255,255,0,255))
//            .addTo(buyUi)
//        ccui.CheckBox.create("Shop/Detail/cbt_detail_0.png","","Shop/Detail/cbt_detail_1.png","","")
//            .move(110,y)
//            .addTo(buyUi)
//            .setSelected(bSelect)
//            .setTag(CBT_INGOT)
//            .addEventListener(cbtlistener)
        }

        var vip = GlobalUserItem.cbMemberOrder || 0;
        var bShowDiscount = vip != 0;
        this.m_discount = 100;
        if (vip != 0) {
            this.m_discount = GlobalUserItem.MemberList[vip]._shop;
        }
        // 折扣
        this._txtDiscount = new cc.LabelTTF(this.m_discount + "%折扣", res.round_body_ttf, 24);
        this._txtDiscount.setAnchorPoint(cc.p(1.0, 0.5));
        this._txtDiscount.setPosition(1130, 380);
        this._txtDiscount.setColor(cc.color(255, 0, 0, 255));
        this._txtDiscount.setVisible(bShowDiscount);
        this.addChild(this._txtDiscount);
        // 会员标识
        var sp_vip = new cc.Sprite(res.atlas_vipnumber_png);
        if (null != sp_vip) {
            sp_vip.setPosition(1130 - this._txtDiscount.getContentSize().width - 20, 380);
            this.addChild(sp_vip);
            sp_vip.setTextureRect(cc.rect(28 * vip, 0, 28, 26));
            sp_vip.setVisible(bShowDiscount);
        }
        this.onUpdatePrice();
    },

    onButtonClickedEvent : function(tag, ref) {
        if (tag == BTN_ADD) {
            this._nCount = this._nCount + 1;
            this.m_editNumber.string = this._nCount + "";
            this.onUpdateNum();
        } else if (tag == BTN_SUB) {
            if (this._nCount > 0) {
                this._nCount = this._nCount - 1;
                this.m_editNumber.string = this._nCount + "";
                this.onUpdateNum();
            }
        } else if (tag == BTN_BLANK) {
            this.m_editNumber.setVisible(true);
            this.m_editNumber.touchDownAction(this.m_editNumber, ccui.Widget.TOUCH_ENDED);
        } else if (tag == BTN_EXCHANGE) {
            if (this._nCount > 0) {
                PriRoom.getInstance().showPopWait();
                PriRoom.getInstance().getNetFrame().onExchangeScore(this._nCount);
            }
        } else if (tag == BTN_BUY) {
            if (this._nCount > 0) {
                PriRoom.getInstance().showPopWait();
                if (this.m_nSelect == CBT_BEAN) {
                    this._shopDetailFrame.onPropertyBuy(yl.CONSUME_TYPE_CASH, this._nCount, 501, 0);
                } else if (this.m_nSelect == CBT_GOLD) {
                    this._shopDetailFrame.onPropertyBuy(yl.CONSUME_TYPE_GOLD, this._nCount, 501, 0);
                } else if (this.m_nSelect == CBT_INGOT) {
                    this._shopDetailFrame.onPropertyBuy(yl.CONSUME_TYPE_USEER_MADEL, this._nCount, 501, 0);
                }
            }
        }
    },

    onSelectedEvent : function(tag,sender,eventType) {
        if (this.m_nSelect == tag) {
            this.m_buyUi.getChildByTag(tag).setSelected(true);
            return;
        }
        this.m_nSelect = tag;

        if (tag == CBT_BEAN) {
            this.m_buyRate = this.m_tabParam.item.bean;
            if (null != this.m_buyUi.getChildByTag(CBT_GOLD)) {
                //this.m_buyUi.getChildByTag(CBT_GOLD).setSelected(false)
            }
        } else if (tag == CBT_GOLD) {
            this.m_buyRate = this.m_tabParam.item.gold;
            if (null != this.m_buyUi.getChildByTag(CBT_BEAN)) {
                //this.m_buyUi.getChildByTag(CBT_BEAN).setSelected(false)
            }
        } else if (tag == CBT_INGOT) {
            this.m_buyRate = this.m_tabParam.item.ingot;
            if (null != this.m_buyUi.getChildByTag(CBT_INGOT)) {
                //this.m_buyUi.getChildByTag(CBT_INGOT).setSelected(false)
            }
        }
        this.onUpdatePrice();
        this.onUpdateNum();
    },

    onEditEvent : function(event,editbox) {
        if (event == "began") {
            this.m_atlasCount.setVisible(false);
        } else if (event == "return") {
            var ndst = tonumber(editbox.getText());
            if ("number" == typeof ndst) {
                this._nCount = ndst;
            }
            editbox.setVisible(false);
            this.onUpdateNum();
        }
    },

    onUpdateNum : function() {
        this.m_atlasCount.setVisible(true);
        this.m_atlasCount.setString(string_formatNumberThousands(this._nCount, true, "."));

        // 获取数量
        var bExchange = this.m_tabParam.bExchange;
        if (null == bExchange) {
            return;
        }
        if (bExchange) {
            var count = this._nCount * this.m_tabParam.exchangeRate;
            this.m_atlasExchangeCount.setString(string_formatNumberThousands(count, true, "."));
        } else {
            // 道具价格
            var price = this.m_buyRate;
            var str = string_formatNumberThousands(price, false, "*");
            str = str.replace(".", "/");
            str = str.replace("*", ".");
            this.m_atlasItemPrice.setString(str);
            // 折后价格
            price = this.m_buyRate * (this.m_discount * 0.01);
            str = string_formatNumberThousands(price, false, "*");
            str = str.replace(".", "/");
            str = str.replace("*", ".");
            this.m_atlasDiscountPrice.setString(str);
            // 购买价格
            price = this._nCount * this.m_buyRate * (this.m_discount * 0.01);
            str = string_formatNumberThousands(price, false, "*");
            str = str.replace(".", "/");
            str = str.replace("*", ".");
            this.m_atlasBuyPrice.setString(str);
        }
    },

    onUpdatePrice : function()
    {
        if (CBT_BEAN == this.m_nSelect) {
            this.m_txtItemPriceType.setString("游戏豆/个");
            this.m_txtBuyPriceType.setString("游戏豆");
            this.m_txtDiscountPriceType.setString("游戏豆/个");
        } else if (CBT_GOLD == this.m_nSelect) {
            this.m_txtItemPriceType.setString("游戏币/个");
            this.m_txtBuyPriceType.setString("游戏币");
            this.m_txtDiscountPriceType.setString("游戏币/个");
        } else if (CBT_INGOT == this.m_nSelect) {
            this.m_txtItemPriceType.setString("元宝/个");
            this.m_txtBuyPriceType.setString("元宝");
            this.m_txtDiscountPriceType.setString("元宝/个");
        }
    },

    onUserInfoChange : function( event ) {
        var msgWhat = event.obj;
        if (null != msgWhat && msgWhat == yl.RY_MSG_USERWEALTH) {
            var bExchange = this.m_tabParam.bExchange;
            if (null == bExchange) {
                return;
            }
            if (bExchange) {
                //更新财富
                this.m_textLeft.setString("剩余" + GlobalUserItem.lRoomCard);
            }
        }
    },

    onShopDetailCallBack : function(result,message) {
        var bRes = false;
        PriRoom.getInstance().dismissPopWait();
        if (typeof message == "string" && message != "") {
            showToast(this, message, 2);
        }

        if (1 == result) {
            PriRoom.getInstance().getPlazaScene().queryUserScoreInfo();
        }
        return bRes;
    }

});
// var ExternalFun = appdf.req(appdf.EXTERNAL_SRC + "ExternalFun");
// var ShopDetailFrame = appdf.req(appdf.CLIENT_SRC+"plaza.models.ShopDetailFrame");

