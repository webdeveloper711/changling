/*  Author  :   JIN */

// 房间创建结果

var BT_JOIN = 102;
var BT_INVITE = 103;

var RoomCreateResult = cc.Layer.extend({

    ctor : function( scene, param, query) {
        this._super();
        this.scene = scene;
        var self = this;
        // 加载csb资源
        var tempLayer = ExternalFun.loadRootCSB(res.CreateRoomResult_json, this);
        var rootLayer = tempLayer.rootlayer;
        this.csbNode = tempLayer.csbnode;

        var btcallback = function (ref, tType) {
            if (tType == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };

        this._query = query;
        this.m_room = true; // 关闭时是否显示房间列表
        // 创建结果
        var tips = this.csbNode.getChildByName("txt_tips");
        tips.setString("房间 " + PriRoom.getInstance().m_tabPriData.szServerID + " 创建成功, 是否进入游戏?");

        // 加入游戏
        var btn = this.csbNode.getChildByName("btn_join");
        btn.setTag(BT_JOIN);
        btn.addTouchEventListener(btcallback);

        // 邀请好友
        btn = this.csbNode.getChildByName("btn_invite");
        btn.setTag(BT_INVITE);
        btn.addTouchEventListener(btcallback);

        if (true == param) {
            rootLayer.setVisible(false);
            this.onButtonClickedEvent(BT_JOIN, null);
        }

    },

    onExit : function() {
        if (this.m_room) {
            // 切换场景
            this.scene.onChangeShowMode(PriRoom.LAYTAG.LAYER_MYROOMRECORD);
            // 移除创建房间记录
            var idx = null;
            for (k in this.scene._sceneRecord) {
                var v = this.scene._sceneRecord[k];
                if (v == PriRoom.LAYTAG.LAYER_CREATEPRIROOME) {
                    idx = k;
                    break;
                }
            }
            if (null != idx) {
                this.scene._sceneRecord.pop(idx);
            }
        }
    },

    onButtonClickedEvent : function( tag, sender ) {
        var self = this;
        if (BT_JOIN == tag) {
            PriRoom.getInstance().showPopWait();
            PriRoom.getInstance().getNetFrame().onSearchRoom(PriRoom.getInstance().m_tabPriData.szServerID);
            this.m_room = false;
            this._query.exit();
        } else if (BT_INVITE == tag) {
            PriRoom.getInstance().getPlazaScene().popTargetShare(function (target, bMyFriend) {
                bMyFriend = bMyFriend || false;
                var sharecall = function (isok) {
                    if (typeof isok == "string" && isok == "true") {
                        showToast(self, "分享成功", 2);
                    }
                };
                var m_tabDetail = {};
                m_tabDetail.szRoomID = PriRoom.getInstance().m_tabPriData.szServerID;
                m_tabDetail.dwPlayTurnCount = PriRoom.getInstance().m_tabPriData.dwDrawCountLimit;
                var msgTab = PriRoom.getInstance().getInviteShareMsg(m_tabDetail);
                var url = GlobalUserItem.szWXSpreaderURL || yl.HTTP_URL;
                if (bMyFriend) {
                    PriRoom.getInstance().getTagLayer(PriRoom.LAYTAG.LAYER_FRIENDLIST, function (frienddata) {
                        var serverid = Number(PriRoom.getInstance().m_tabPriData.szServerID) || 0;
                        PriRoom.getInstance().priInviteFriend(frienddata, GlobalUserItem.nCurGameKind, serverid, yl.INVALID_TABLE, msgTab.friendContent);
                    });
                } else if (null != target) {
                    MultiPlatform.getInstance().shareToTarget(target, sharecall, msgTab.title, msgTab.content, url, "");
                }
            });
        }
    }

});

