/*  Author  :   JIN */

// 房间详情界面

var TAG_MASK          = 101;
var TAG_SHARE_GRADE   = 102;  // 战绩分享
var TAG_JOIN_GAME     = 103;   // 加入游戏
var TAG_DISSUME_GAME  = 104;   // 解散房间
var TAG_INVITE_FRIEND = 105;   // 邀请好友

var tabPosition =
    [
        [cc.p(175+40, 270+36)],
        [cc.p(175+40, 270+36), cc.p(175+40, 230+36)],
        [cc.p(175+40, 270+36), cc.p(175+40, 230+36), cc.p(175+40, 190+36)],
        [cc.p(175+40, 270+36), cc.p(175+40, 230+36), cc.p(175+40, 190+36), cc.p(175+40, 150+36)],
        [cc.p(175+40, 270+36), cc.p(175+40, 230+36), cc.p(175+40, 190+36), cc.p(175+40, 150+63), cc.p(445+40, 270+36)],
        [cc.p(175+40, 270+36), cc.p(175+40, 230+36), cc.p(175+40, 190+36), cc.p(175+40, 150+36), cc.p(445+40, 270+36), cc.p(445+40, 230+36)],
        [cc.p(175+40, 270+36), cc.p(175+40, 230+36), cc.p(175+40, 190+36), cc.p(175+40, 150+36), cc.p(445+40, 270+36), cc.p(445+40, 230+36), cc.p(445+40, 190+36)],
        [cc.p(175+40, 270+36), cc.p(175+40, 230+36), cc.p(175+40, 190+36), cc.p(175+40, 150+36), cc.p(445+40, 270+36), cc.p(445+40, 230+36), cc.p(445+40, 190+36), cc.p(445+40, 150+36)],
    ];

var RoomDetailLayer = cc.Layer.extend({
    
    // 详情战绩 是否结束
    ctor : function( tabDetail ) {
        this._super();

        var self = this;

        this.m_tabDetail = tabDetail;
        // 是否游戏中
        var onGame = tabDetail.onGame || false;
        // 是否允许解散
        var enableDismiss = tabDetail.enableDismiss || false;
    
        // 加载csb资源
        var jsonNode = ExternalFun.loadRootCSB(res.RoomInfoLayer_json, this);
        var rootLayer = jsonNode.rootlayer;
        var csbNode = jsonNode.csbnode;
    
        var touchFunC = function (ref, tType) {
            if (tType == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };
    
        // 遮罩
        var mask = csbNode.getChildByName("Panel_1");
        mask.setTag(TAG_MASK);
        mask.addTouchEventListener(touchFunC);
    
        // 底板
        var image_bg = csbNode.getChildByName("image_bg");
        image_bg.setTouchEnabled(true);
        image_bg.setSwallowTouches(true);
        image_bg.setScale(0.00001);
        this.m_imageBg = image_bg;
    
        // 房间ID
        this.m_txtRoomId = image_bg.getChildByName("txt_roomid");
        var szRoomId = tabDetail.szRoomID || "";
        this.m_txtRoomId.setString(szRoomId);
    
        var gradList = tabDetail.PersonalUserScoreInfo[0];
        this.m_tabGradList = gradList;
        var count = gradList.length;
        count = (count > 8) && 8 || count;
        var posList = tabPosition[count - 1];
        // 房间战绩列表
        if (onGame) {
            var clipGrad = new ClipText(cc.size(373, 28), "该房间正在游戏中,暂无战绩!", res.round_body_ttf, 24);
            clipGrad.setAnchorPoint(cc.p(0, 0.5));
            clipGrad.setColor(cc.color(140, 86, 47));
            var pos = posList[1];
            clipGrad.setPosition(pos);
            image_bg.addChild(clipGrad);
        } else {
            this.m_nCount = 0;
            this.m_bNoGradInfo = false;
            for (var i = 0; i < count; i++) {
                var v = gradList[i];
                if (0 == v.dwUserID) {
                    if (0 == i) {
                        // 无战绩
                        cc.log(" no score");
                        this.m_nCount = 1;
                        this.m_bNoGradInfo = true;
                    }
                    break;
                }
                this.m_nCount = this.m_nCount + 1;
            }
    
            // 列表
            this._listView = new cc.TableView(this, cc.size(700, 170));
            this._listView.setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL);
            this._listView.setPosition(cc.p(220, 158));
            this._listView.setDelegate(this);
            image_bg.addChild(this._listView);
            // this._listView.registerScriptHandler(handler(this, this.tableCellTouched), cc.TABLECELL_TOUCHED);
            // this._listView.registerScriptHandler(handler(this, this.cellSizeForTable), cc.TABLECELL_SIZE_FOR_INDEX);
            // this._listView.registerScriptHandler(handler(this, this.tableCellAtIndex), cc.TABLECELL_SIZE_AT_INDEX);
            // this._listView.registerScriptHandler(handler(this, this.numberOfCellsInTableView), cc.NUMBER_OF_CELLS_IN_TABLEVIEW);
            this._listView.reloadData();
        }
    
        // 按钮列表
        if (onGame) {
            // 加入游戏
            var btn = image_bg.getChildByName("btn_jion");
            btn.setVisible(true);
            btn.setTag(TAG_JOIN_GAME);
            btn.addTouchEventListener(touchFunC);
    
            // 解散房间
            btn = image_bg.getChildByName("btn_dismiss");
            btn.setVisible(true);
            btn.setTag(TAG_DISSUME_GAME);
            btn.setEnabled(enableDismiss);
            btn.addTouchEventListener(touchFunC);
    
            // 邀请好友
            btn = image_bg.getChildByName("btn_invite");
            btn.setVisible(true);
            btn.setTag(TAG_INVITE_FRIEND);
            btn.addTouchEventListener(touchFunC);
        } else {
            // 分享战绩
            var btn = image_bg.getChildByName("btn_invite");
            btn.setTag(TAG_SHARE_GRADE);
            btn.addTouchEventListener(touchFunC);
        }
    
        // 加载动画
        image_bg.runAction(new cc.ScaleTo(0.2, 1.0));
    },
    
    tableCellTouched : function(view, cell) {
    
    },
    
    cellSizeForTable : function(view, idx) {
        return [0, 45];
    },
    
    numberOfCellsInTableView : function(view) {
        return this.m_nCount;
    },

    tableCellSizeForIndex:function (table, idx) {
        return cc.size(100, 45);
    },
    
    tableCellAtIndex : function(view, idx) {
        var cell = view.dequeueCell();
        if (!cell) {
            cell = new cc.TableViewCell();
        } else {
            cell.removeAllChildren();
        }
        if (true == this.m_bNoGradInfo) {
            var clipGrad = new ClipText(cc.size(373, 28), "该房间无战绩, 房间已解散", res.round_body_ttf, 24);
            clipGrad.setColor(cc.color(140, 86, 47));
            clipGrad.setAnchorPoint(cc.p(0, 0.5));
            clipGrad.setPosition(0, 24);
            cell.addChild(clipGrad);
    
            return cell;
        }
        var gradInfo = this.m_tabGradList[idx];
        if (null != gradInfo) {
            var str = gradInfo.szUserNicname;
            var clipGrad = new ClipText(cc.size(200, 27), str, res.round_body_ttf, 24);
            clipGrad.setAnchorPoint(cc.p(0, 0.5));
            clipGrad.setColor(cc.color(140, 86, 47));
            clipGrad.setPosition(0, 24);
            cell.addChild(clipGrad);
    
            str = "+" + gradInfo.lScore;
            if (gradInfo.lScore < 0) {
                str = "" + gradInfo.lScore;
            }
            clipGrad = new ClipText(cc.size(200, 27), str, res.round_body_ttf, 24);
            clipGrad.setAnchorPoint(cc.p(0, 0.5));
            clipGrad.setPosition(210, 24);
            clipGrad.setColor(cc.color(140, 86, 47));
            cell.addChild(clipGrad);
        }
    
        return cell;
    },
    
    onButtonClickedEvent : function( tag, sender) {
        if (TAG_SHARE_GRADE == tag) {
            PriRoom.getInstance().getPlazaScene().popTargetShare(function (target, bMyFriend) {
                bMyFriend = bMyFriend || false;
                var sharecall = function (isok) {
                    if (typeof(isok) == "string" && isok == "true") {
                        showToast(this, "分享成功", 2);
                    }
                }
                var url = GlobalUserItem.szWXSpreaderURL || yl.HTTP_URL;
                // 截图分享
                var framesize = cc.director.getOpenGLView().getFrameSize();
                var area = cc.rect(0, 0, framesize.width, framesize.height);
                var imagename = "grade_share.jpg";
                if (bMyFriend) {
                    imagename = "grade_share_" + (new Date).getTime() + ".jpg";
                }
                ExternalFun.popupTouchFilter(0, false);
                captureScreenWithArea(area, imagename, function (ok, savepath) {
                    ExternalFun.dismissTouchFilter();
                    if (ok) {
                        if (bMyFriend) {
                            PriRoom.getInstance().getTagLayer(PriRoom.LAYTAG.LAYER_FRIENDLIST, function (frienddata) {
                                PriRoom.getInstance().imageShareToFriend(frienddata, savepath, "分享我的约战房战绩");
                            });
                        } else if (null != target) {
                            MultiPlatform.getInstance().shareToTarget(target, sharecall, "我的约战房战绩", "分享我的约战房战绩", url, savepath, "true");
                        }
                    }
                });
            });
        } else if (TAG_JOIN_GAME == tag) {
            PriRoom.getInstance().showPopWait();
            PriRoom.getInstance().getNetFrame().onSearchRoom(this.m_txtRoomId.getString());
        } else if (TAG_DISSUME_GAME == tag) {
            PriRoom.getInstance().showPopWait();
            PriRoom.getInstance().getNetFrame().onDissumeRoom(this.m_txtRoomId.getString());
        } else if (TAG_INVITE_FRIEND == tag) {
            PriRoom.getInstance().getPlazaScene().popTargetShare(function (target, bMyFriend) {
                bMyFriend = bMyFriend || false;
                var sharecall = function (isok) {
                    if (typeof isok == "string" && isok == "true") {
                        showToast(this, "分享成功", 2);
                    }
                }
                var msgTab = PriRoom.getInstance().getInviteShareMsg(this.m_tabDetail);
                var url = GlobalUserItem.szWXSpreaderURL || yl.HTTP_URL;
                if (bMyFriend) {
                    PriRoom.getInstance().getTagLayer(PriRoom.LAYTAG.LAYER_FRIENDLIST, function (frienddata) {
                        var serverid = tonumber(this.m_tabDetail.szRoomID) || 0;
                        // dump(msgTab, "desciption", 6)
                        PriRoom.getInstance().priInviteFriend(frienddata, GlobalUserItem.nCurGameKind, serverid, yl.INVALID_TABLE, msgTab.friendContent);
                    });
                } else if (null != target) {
                    MultiPlatform.getInstance().shareToTarget(target, sharecall, msgTab.title, msgTab.content, url, "");
                }
            })
        } else if (TAG_MASK == tag) {
            this.hide();
        }
    },
    
    hide : function() {
        var self = this;
        var scale1 = new cc.ScaleTo(0.2, 0.0001);
        var call1 = new cc.CallFunc(function () {
            self.removeFromParent();
        });
        this.m_imageBg.runAction(new cc.Sequence(scale1, call1));
    },
    

});

// var ExternalFun = appdf.req(appdf.EXTERNAL_SRC + "ExternalFun");
// var ClipText = appdf.req(appdf.EXTERNAL_SRC + "ClipText");
// var MultiPlatform = appdf.req(appdf.EXTERNAL_SRC + "MultiPlatform");


