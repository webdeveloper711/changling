/*  Author  .   JIN */

var RoomIdInputLayer = cc.Layer.extend({

    ctor : function() {
        // 注册触摸事件
        this._super();
        ExternalFun.registerTouchEvent(this, true);

        var self = this;

        // 加载csb资源
        var jsonload = ExternalFun.loadRootCSB(res.RoomIdInputLayer_json, this);
        var rootLayer = jsonload.rootlayer;
        var csbNode = jsonload.csbnode;

        //getChildByName
        this.m_spBg = csbNode.getChildByName("pri_sp_ideditbg");
        this.m_spBg.setScale(0.000001);
        var bg = this.m_spBg;

        // 房间ID
        this.m_atlasRoomId = bg.getChildByName("atlas_roomid");
        this.m_atlasRoomId.setString("");

        // TODO : Added By Kil
        this.placeholder = bg.getChildByName("pri_sp_holder");

        var btncallback = function (ref, tType) {
            if (tType == ccui.Widget.TOUCH_ENDED) {
                self.onNumButtonClickedEvent(ref.getTag(), ref);
            }
        };
        // 数字按钮
        for (var i = 1; i <= 10; i++) {
            var tag = i - 1;
            var btn = bg.getChildByName("btn_num" + tag);
            btn.setTag(tag);
            btn.addTouchEventListener(btncallback);
        }

        var callback = function (ref, tType) {
            if (tType == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };
        // 清除按钮
        var btn = bg.getChildByName("btn_clear");
        btn.setTag(BTN_CLEAR);
        btn.setVisible(true);
        btn.addTouchEventListener(callback);

        // 删除按钮
        btn = bg.getChildByName("btn_del");
        btn.setTag(BTN_DEL);
        btn.addTouchEventListener(callback);

        //关闭按钮
        var btn = bg.getChildByName("btn_close");
        btn.setTag(BTN_CLOSE);
        btn.setVisible(false);
        btn.addTouchEventListener(callback);

        // 加载动画
        var call = new cc.CallFunc(function () {
            self.setVisible(true);
        });
        var scale = cc.ScaleTo.create(0.2, 1);
        this.m_actShowAct = new cc.Sequence(call, scale);
        ExternalFun.SAFE_RETAIN(this.m_actShowAct);

        var scale1 = new cc.ScaleTo(0.2, 0.0001);
        var call1 = new cc.CallFunc(function () {
            self.setVisible(false);
        });
        this.m_actHideAct = new cc.Sequence(scale1, call1);
        ExternalFun.SAFE_RETAIN(this.m_actHideAct);

        this.setVisible(false);

        var customListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                return self.isVisible();
            },
            onTouchEnded : function (touch, event) {
                var pos = touch.getLocation();
                var m_spBg = self.m_spBg;
                pos = m_spBg.convertToNodeSpace(pos);
                var rec = cc.rect(0, 0, m_spBg.getContentSize().width, m_spBg.getContentSize().height);
                if (false == cc.rectContainsPoint(rec, pos)) {
                    self.showLayer(false);
                    var roomid = self.m_atlasRoomId.getString();
                    if (roomid.length == 6) {
                        PriRoom.getInstance().showPopWait();
                        PriRoom.getInstance().getNetFrame().onSearchRoom(roomid);
                        self.m_atlasRoomId.setString("");
                    }
                }
            }
        });
        cc.eventManager.addListener(customListener, this.m_spBg);
    },

    showLayer : function( va ) {
        var self = this;
        var ani = null;
        if (va) {
            ani = this.m_actShowAct;

            GlobalUserItem.szCopyRoomId = GlobalUserItem.szCopyRoomId || "";
            var copyId = GlobalUserItem.szCopyRoomId.replace("([^0-9])", "");
            var joinGame = PriRoom.getInstance().m_tabJoinGameRecord[GlobalUserItem.nCurGameKind];
            var enterRoomId = "";
            if (6 == copyId.length) {
                // 复制id
                enterRoomId = copyId;
            } else if ("object" == typeof(joinGame) && "string" == typeof(joinGame["roomid"]) && 6 == joinGame["roomid"].length) {
                // 缓存暂离id
                enterRoomId = joinGame["roomid"];
            }

            if (6 == enterRoomId.length) {
                this.m_atlasRoomId.setString(enterRoomId);
                PriRoom.getInstance().showPopWait();
                this.runAction(new cc.Sequence(new cc.DelayTime(1.5), new cc.CallFunc(function () {
                    self.showLayer(false);
                    PriRoom.getInstance().getNetFrame().onSearchRoom(enterRoomId);
                    this.m_atlasRoomId.setString("");
                    GlobalUserItem.szCopyRoomId = "";
                })));
            } else {
                this.m_atlasRoomId.setString("");
            }
        } else {
            ani = this.m_actHideAct;
        }

        if (null != ani) {
            this.m_spBg.stopAllActions();
            this.m_spBg.runAction(ani);
        }
    },

    onExit : function() {
        ExternalFun.SAFE_RELEASE(this.m_actShowAct);
        this.m_actShowAct = null;
        ExternalFun.SAFE_RELEASE(this.m_actHideAct);
        this.m_actHideAct = null;
    },

    onTouchBegan : function(touch, event) {
        return this.isVisible();
    },

    onTouchEnded : function(touch, event) {
        var pos = touch.getLocation();
        var m_spBg = this.m_spBg;
        pos = m_spBg.convertToNodeSpace(pos);
        var rec = cc.rect(0, 0, m_spBg.getContentSize().width, m_spBg.getContentSize().height);
        if (false == cc.rectContainsPoint(rec, pos)) {
            this.showLayer(false);
            var roomid = this.m_atlasRoomId.getString();
            if (roomid.length == 6) {
                PriRoom.getInstance().showPopWait();
                PriRoom.getInstance().getNetFrame().onSearchRoom(roomid);
                this.m_atlasRoomId.setString("");
            }
        }
    },

    // TODO : Added By Kil
    isShowPlaceHolder : function () {
        var roomid = this.m_atlasRoomId.getString();
        if (roomid.length == 0) {
            this.placeholder.setVisible(true);
        } else {
            this.placeholder.setVisible(false);
        }
    },

    onNumButtonClickedEvent : function( tag, sender ) {
        var self = this;
        var roomid = this.m_atlasRoomId.getString();
        if ((roomid).length < 6) {
            roomid = roomid + tag;
            this.m_atlasRoomId.setString(roomid);
        }
        if (roomid.length == 6) {
            PriRoom.getInstance().showPopWait();
            this.runAction(new cc.Sequence(new cc.DelayTime(1.0), new cc.CallFunc(function () {
                self.showLayer(false);
                PriRoom.getInstance().getNetFrame().onSearchRoom(roomid);
                self.m_atlasRoomId.setString("");
                GlobalUserItem.szCopyRoomId = "";
            })));
        }
        this.isShowPlaceHolder();
    },

    onButtonClickedEvent : function( tag, sender ) {
        if (BTN_CLEAR == tag) {
            this.m_atlasRoomId.setString("");
        } else if (BTN_DEL == tag) {
            var roomid = this.m_atlasRoomId.getString();
            var len = roomid.length;
            if (len > 0) {
                roomid = roomid.substring(0, len - 1);
            }
            this.m_atlasRoomId.setString(roomid);
        } else if (BTN_CLOSE == tag) {
            this.showLayer(false);
        }
        this.isShowPlaceHolder();
    }

});

var BTN_CLEAR = 1;
var BTN_DEL = 2;
var BTN_CLOSE = 3;
