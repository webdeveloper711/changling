/*  Author : KIL */
var RoomRecordLayer = cc.Layer.extend({
    ctor : function (scene , choice) {
        this._super();
        cc.log("RoomRecordLayer : ctor");

        var self = this;

        this.scene = scene;
        // 加载csb资源
        var m_Data = ExternalFun.loadRootCSB(res.RecordLayer_json, this);
        var rootLayer = m_Data.rootlayer;
        var csbNode = m_Data.csbnode;

        var cbtlistener = function (sender, eventType) {
            self.onSelectedEvent(sender.getTag(), sender, eventType);
            self.m_checkSwitch.getChildByName("on").setVisible(sender.isSelected());
            self.m_checkSwitch.getChildByName("off").setVisible(!sender.isSelected());
        };

        var callback = function (ref, tType) {
            if (tType == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };

        // 切换按钮
        this.m_checkSwitch = csbNode.getChildByName("check_switchrec");
        if (choice == RoomRecordLayer.CHOICE_CREAT) {
            this.m_checkSwitch.setSelected(true);
            this.m_checkSwitch.getChildByName("on").setVisible(true);
            this.m_checkSwitch.getChildByName("off").setVisible(false);
        } else if (choice == RoomRecordLayer.CHOICE_JOIN) {
            this.m_checkSwitch.setSelected(false);
            this.m_checkSwitch.getChildByName("on").setVisible(false);
            this.m_checkSwitch.getChildByName("off").setVisible(true);
        } else {
            this.m_checkSwitch.setVisible(false);
        }
        this.m_checkSwitch.addEventListener(cbtlistener);

        // 关闭钮
        this.m_btnClose = csbNode.getChildByName("btn_close");
        this.m_btnClose.addTouchEventListener(callback);

        // 创建记录
        this.m_layCreateRec = csbNode.getChildByName("lay_create");
        this.m_layCreateRec.setVisible(choice == RoomRecordLayer.CHOICE_CREAT);
        this.m_creCreateTime = this.m_layCreateRec.getChildByName("pri_sp_createtime");
        this.m_creRoomID = this.m_layCreateRec.getChildByName("pri_sp_roomid");
        this.m_creRoomLimit = this.m_layCreateRec.getChildByName("pri_sp_roomlimit");
        this.m_creCost = this.m_layCreateRec.getChildByName("pri_sp_createcost");
        this.m_creDisTime = this.m_layCreateRec.getChildByName("pri_sp_dissolvetime");
        this.m_creAward = this.m_layCreateRec.getChildByName("pri_sp_award");
        this.m_creStatus = this.m_layCreateRec.getChildByName("pri_sp_roomstatus");

        // 参与记录
        this.m_layJoinRec = csbNode.getChildByName("lay_join");
        this.m_layJoinRec.setVisible(choice != RoomRecordLayer.CHOICE_CREAT);
        this.m_joinCreateTime = this.m_layJoinRec.getChildByName("pri_sp_createtime");
        this.m_joinRoomID = this.m_layJoinRec.getChildByName("pri_sp_roomid");
        this.m_joinCreateUser = this.m_layJoinRec.getChildByName("pri_sp_createuser");
        this.m_joinUinfo = this.m_layJoinRec.getChildByName("pri_sp_uinfo");
        this.m_joinDisTime = this.m_layJoinRec.getChildByName("pri_sp_dissolvetime");

        var content = csbNode.getChildByName("lay_content");
        // 列表
        var m_tableView = new cc.TableView(this, content.getContentSize());
        m_tableView.setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL);
        m_tableView.setPosition(content.getPosition());
        m_tableView.setDelegate(this);
        // m_tableView(this.cellSizeForTable, cc.TABLECELL_SIZE_FOR_INDEX);
        // m_tableView(handler(this, this.tableCellAtIndex), cc.TABLECELL_SIZE_AT_INDEX);
        // m_tableView(handler(this, this.numberOfCellsInTableView), cc.NUMBER_OF_CELLS_IN_TABLEVIEW);
        csbNode.addChild(m_tableView);
        this.m_tableView = m_tableView;
        content.removeFromParent();

        // 房间信息
        this.m_layRoomDetail = null;
    },

    onButtonClickedEvent : function ( tag, sender ) {
        this.scene.onChangeShowMode();
    },

    onSelectedEvent : function ( tag,sender,eventType ) {
        var sel = sender.isSelected();
        this.m_layCreateRec.setVisible(sel);
        this.m_layJoinRec.setVisible(!sel);

        if (!sel && 0 == PriRoom.getInstance().m_tabJoinRecord.length) {
            PriRoom.getInstance().showPopWait();
            PriRoom.getInstance().getNetFrame().onQueryJoinList();
            return;
        }
        this.m_tableView.reloadData();
    },

    onEnterTransitionDidFinish : function () {
        this.onEnterTransitionFinish();
    },

    onEnterTransitionFinish : function () {
        PriRoom.getInstance().showPopWait();
        if (this.m_checkSwitch.isSelected())
        // 请求创建记录列表
            PriRoom.getInstance().getNetFrame().onQueryRoomList();
        else
        // 请求加入记录列表
            PriRoom.getInstance().getNetFrame().onQueryJoinList();
    },

    //退出时清理数据
    onExit : function () {
        // 清除缓存
        PriRoom.getInstance().m_tabJoinRecord = [];
        PriRoom.getInstance().m_tabCreateRecord = [];
    },

    //重载数据，在PriRoom中以this._viewFrame.onReloadRecordList()调用。
    onReloadRecordList : function () {
        this.m_tableView.reloadData();
        var rd = this.getChildByName(ROOMDETAIL_NAME);
        if (null != rd)
            rd.hide();
    },

    //返回列表大小
    cellSizeForTable : function ( view, idx ) {
        return [1030, 80];
    },

    //返回列表数量
    numberOfCellsInTableView : function ( view ) {
        if (this.m_checkSwitch.isSelected()) {
            var v = PriRoom.getInstance().m_tabCreateRecord.length;
            return v;
        }
        else {
            v= PriRoom.getInstance().m_tabJoinRecord.length;
            return v;
        }
    },

    tableCellSizeForIndex:function (table, idx) {
        return cc.size(100, 80);
    },

    //绘画子界面
    tableCellAtIndex : function ( view, idx ) {
        var cell = view.dequeueCell();
        if (!cell)
            cell = new cc.TableViewCell();
        else
            cell.removeAllChildren();

        if (this.m_checkSwitch.isSelected()) {
            var tabData = PriRoom.getInstance().m_tabCreateRecord[idx];
            var item = this.createRecordItem(tabData);
            item.setPosition(view.getViewSize().width * 0.5, 40);
            cell.addChild(item);
        } else {
            var tabData = PriRoom.getInstance().m_tabJoinRecord[idx];
            var item = this.joinRecordItem(tabData);
            item.setPosition(view.getViewSize().width * 0.5, 40);
            cell.addChild(item);
        }

        return cell;
    },

    // 创建记录
    createRecordItem : function ( tabData ) {
        var item = new ccui.Widget();
        item.setContentSize(cc.size(1030, 72));

        // 线
        var frame = new ccui.ImageView(res.pri_sp_listline1_png);
        frame.setAnchorPoint(cc.p(0.5, 0.5));
        frame.setScale9Enabled(true);
        frame.setContentSize(1030, 72);
        frame.setPosition(1030 / 2, 72 / 2);
        item.addChild(frame);

        // 创建时间
        var tabTime = tabData.sysCreateTime;        
        var strTime = sprintf("%d-%02d-%02d", tabTime.wYear, tabTime.wMonth, tabTime.wDay);
        var createtime = new cc.LabelTTF(strTime, res.round_body_ttf, 24);
        createtime.setColor(cc.color(108, 53, 21));
        item.addChild(createtime);
        createtime.setPosition(this.m_creCreateTime.getPositionX(), 46);

        // 创建时间
        var tabTime2 = tabData.sysCreateTime;
        var strTime2 = sprintf("%02d:%02d:%02d", tabTime.wHour, tabTime.wMinute, tabTime.wSecond);
        var createtime2 = new cc.LabelTTF(strTime2, res.round_body_ttf, 24);
        createtime2.setColor(cc.color(108, 53, 21));
        item.addChild(createtime2);
        createtime2.setPosition(this.m_creCreateTime.getPositionX(), 26);

        // 房间ID
        var roomid = new cc.LabelTTF(tabData.szRoomID, res.round_body_ttf, 24);
        roomid.setColor(cc.color(108, 53, 21));
        item.addChild(roomid);
        roomid.setPosition(this.m_creRoomID.getPositionX(), 36);

        // 房间限制
        var roomlimit = new cc.LabelTTF(tabData.dwPlayTurnCount + "", res.round_body_ttf, 24);
        roomlimit.setColor(cc.color(108, 53, 21));
        item.addChild(roomlimit);
        roomlimit.setPosition(this.m_creRoomLimit.getPositionX(), 36);

        var feeType = "房卡";
        if (tabData.cbCardOrBean == 0)
            feeType = "钻石";
        // 创建消耗
        var cost = new cc.LabelTTF(tabData.lFeeCardOrBeanCount + feeType, res.round_body_ttf, 24);
        cost.setColor(cc.color(108, 53, 21));
        item.addChild(cost);
        cost.setPosition(this.m_creCost.getPositionX(), 36);

        // 奖励
        var award = new cc.LabelTTF(tabData.lScore + "金币", res.round_body_ttf, 24);
        award.setColor(cc.color(108, 53, 21));
        item.addChild(award);
        award.setPosition(this.m_creAward.getPositionX(), 36);

        // 房间状态
        var bOnGame = false;
        var status = new cc.LabelTTF("", res.round_body_ttf, 24);
        if (tabData.cbIsDisssumRoom == 1) {// 解散
            status.setColor(cc.color(217, 51, 6));
            status.setString("已解散");
            tabTime = tabData.sysDissumeTime;
            strTime = sprintf("%d-%02d-%02d", tabTime.wYear, tabTime.wMonth, tabTime.wDay);
            strTime2 = sprintf("%02d:%02d.:%02d", tabTime.wHour, tabTime.wMinute, tabTime.wSecond);
        } else { // 游戏中
            status.setColor(cc.color(64, 121, 9));
            status.setString("游戏中");
            bOnGame = true;
            strTime = "";
            strTime2 = "";
        }
        item.addChild(status);
        status.setPosition(this.m_creStatus.getPositionX(), 36);

        // 解散时间
        var distime = new cc.LabelTTF(strTime, res.round_body_ttf, 24);
        distime.setColor(cc.color(244, 237, 182));
        item.addChild(distime);
        distime.setPosition(this.m_creDisTime.getPositionX(), 46);

        var distime2 = new cc.LabelTTF(strTime2, res.round_body_ttf, 24);
        distime2.setColor(cc.color(244, 237, 182));
        item.addChild(distime2);
        distime2.setPosition(this.m_creDisTime.getPositionX(), 26);

        item.setTouchEnabled(true);
        item.setSwallowTouches(false);

        var self = this;
        var itemFunC = function (ref, tType) {
            if (tType == ccui.Widget.TOUCH_ENDED) {
                var tabDetail = tabData;
                tabDetail.onGame = bOnGame;
                tabDetail.enableDismiss = true;
                var rd = new RoomDetailLayer(tabDetail);
                rd.setName(ROOMDETAIL_NAME);
                self.addChild(rd);
            }
        }
        item.addTouchEventListener(itemFunC);
        return item;
    },

    // 参与记录
    joinRecordItem : function ( tabData ) {
        var item = new ccui.Widget();
        item.setContentSize(cc.size(1030, 72));

        // 线
        var frame = new ccui.ImageView(res.pri_sp_listline1_png);
        frame.setAnchorPoint(cc.p(0.5,0.5));
        frame.setScale9Enabled(true);
        frame.setContentSize(1030,72);
        frame.setPosition(1030/2,72/2);
        item.addChild(frame);

        // 创建时间
        var tabTime = tabData.sysCreateTime;
        var strTime = sprintf("%d-%02d-%02d", tabTime.wYear, tabTime.wMonth, tabTime.wDay);
        var createtime = new cc.LabelTTF(strTime,res.round_body_ttf,24);
        createtime.setColor(cc.color(108,53,21));
        item.addChild(createtime);
        createtime.setPosition(this.m_joinCreateTime.getPositionX(), 46);

        var strTime2 = sprintf("%02d:%02d:%02d", tabTime.wHour, tabTime.wMinute, tabTime.wSecond);
        var createtime2 = new cc.LabelTTF(strTime2,res.round_body_ttf,24);
        createtime2.setColor(cc.color(108,53,21));
        item.addChild(createtime2);
        createtime2.setPosition(this.m_joinCreateTime.getPositionX(), 26);

        // 房间ID
        var roomid = new cc.LabelTTF(tabData.szRoomID,res.round_body_ttf,24);
        roomid.setColor(cc.color(108,53,21));
        item.addChild(roomid);
        roomid.setPosition(this.m_joinRoomID.getPositionX(), 36);

        // 创建玩家
        var createusr = new ClipText(cc.size(250, 30), tabData.szUserNicname, res.round_body_ttf, 24);
        createusr.setAnchorPoint(cc.p(0.5, 0.5));
        createusr.setColor(cc.color(108,53,21,255));
        item.addChild(createusr);
        createusr.setPosition(this.m_joinCreateUser.getPositionX(), 36);

        var scorestr = "+" + tabData.lScore;
        if (tabData.lScore < 0) {
            scorestr = "" + tabData.lScore;
        }
        if (tabData.bFlagOnGame) {
            scorestr = "";
        }
        // 个人战绩
        var uinfo = new ClipText(cc.size(150, 30), scorestr, res.round_body_ttf, 24);
        uinfo.setAnchorPoint(cc.p(0.5, 0.5));
        uinfo.setColor(cc.color(217,51,6,255));
        item.addChild(uinfo);
        uinfo.setPosition(this.m_joinUinfo.getPositionX(), 36);

        // 解散时间
        tabTime = tabData.sysDissumeTime;
        strTime = sprintf("%d-%02d-%02d", tabTime.wYear, tabTime.wMonth, tabTime.wDay);
        strTime2 = sprintf("%02d:%02d:%02d", tabTime.wHour, tabTime.wMinute, tabTime.wSecond);
        var distime = new cc.LabelTTF(strTime,res.round_body_ttf,24);
        var distime2 = new cc.LabelTTF(strTime2,res.round_body_ttf,24);
        distime.setPosition(this.m_joinDisTime.getPositionX(), 46);
        distime2.setPosition(this.m_joinDisTime.getPositionX(), 26);
        distime.setColor(cc.color(108,53,21));
        distime2.setColor(cc.color(108,53,21));
        if (tabData.bFlagOnGame) {
            distime.setString("游戏中");
            distime.setColor(cc.color(108, 53, 21));
            distime.setPosition(this.m_joinDisTime.getPositionX(), 36);
            distime2.setColor(cc.color(108, 53, 21));
            distime2.setString("");
        }
        item.addChild(distime);
        item.addChild(distime2);


        item.setTouchEnabled(true);
        item.setSwallowTouches(false);
        var self = this;
        var itemFunC = function(ref, tType) {
            if (tType == ccui.Widget.TOUCH_ENDED) {
                var tabDetail = tabData;
                tabDetail.onGame = false; //tabData.bFlagOnGame or false
                tabDetail.enableDismiss = false;
                var rd = new RoomDetailLayer(tabDetail);
                rd.setName(ROOMDETAIL_NAME);
                self.addChild(rd);
            }
        }
        item.addTouchEventListener( itemFunC );
        return item;
    }
});

// var ExternalFun = appdf.req(appdf.EXTERNAL_SRC + "ExternalFun");
// var ClipText = appdf.req(appdf.EXTERNAL_SRC + "ClipText");
// var RoomDetailLayer = appdf.req(PriRoom.MODULE.PLAZAMODULE + "views.RoomDetailLayer");
// var cmd_private = appdf.req(PriRoom.MODULE.PRIHEADER + "CMD_Private");

var ROOMDETAIL_NAME = "__pri_room_detail_layer_name__";

RoomRecordLayer.CHOICE_CREAT = 1; // 创建信息
RoomRecordLayer.CHOICE_JOIN = 2; // 加入信息
RoomRecordLayer.CHOICE_ONLYJOIN = 3; //仅查看加入信息
