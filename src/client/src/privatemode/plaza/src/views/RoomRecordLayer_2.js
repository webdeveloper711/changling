/*  Author : KIL*/

var RoomRecordLayer_2 = cc.Layer.extend({
    ctor: function () {
        this._super();
    }
});
// var ExternalFun = appdf.req(appdf.EXTERNAL_SRC + "ExternalFun");
// var ClipText = appdf.req(appdf.EXTERNAL_SRC + "ClipText");
// var RoomDetailLayer = appdf.req(PriRoom.MODULE.PLAZAMODULE + "views.RoomDetailLayer");
// var cmd_private = appdf.req(PriRoom.MODULE.PRIHEADER + "CMD_Private");

var ROOMDETAIL_NAME = "__pri_room_detail_layer_name__";
RoomRecordLayer.ctor = function ( scene ) {
    ExternalFun.registerNodeEvent(this);

    this.scene = scene;
    // 加载csb资源
    var rootLayer, csbNode = ExternalFun.loadRootCSB("room/RecordLayer_2.csb", this);

    var cbtlistener = function (sender, eventType) {
        this.onSelectedEvent(sender.getTag(), sender, eventType);
    }
    var callback = function (ref, tType) {
        if (tType == ccui.TouchEventType.ended) {
            this.onButtonClickedEvent(ref.getTag(), ref);
        }
    }
    // 切换按钮
    this.m_checkSwitch = csbNode.getChildByName("check_switchrec");
    this.m_checkSwitch.setSelected(false);
    this.m_checkSwitch.addEventListener(cbtlistener);
    this.m_checkSwitch.setVisible(false);

    // 关闭钮
    this.m_btnClose = csbNode.getChildByName("btn_close");
    this.m_btnClose.addTouchEventListener(callback);

    //参与记录
    this.m_spRecordBg1 = csbNode.getChildByName("pri_sp_listbg_1");
    this.m_spRecordBg1.setVisible(false);

    //创建记录
    this.m_spRecordBg2 = csbNode.getChildByName("pri_sp_listbg_2");
    this.m_spRecordBg2.setVisible(false);

    //战绩
    this.m_spRecordBg2 = csbNode.getChildByName("pri_sp_listbg_2");
    this.m_spRecordBg2.setVisible(false);

    // 创建记录
    this.m_layCreateRec = csbNode.getChildByName("lay_create");
    //this.m_layCreateRec.setVisible(true)
    this.m_layCreateRec.setVisible(false);
    this.m_creCreateTime = this.m_layCreateRec.getChildByName("pri_sp_createtime");
    this.m_creRoomID = this.m_layCreateRec.getChildByName("pri_sp_roomid");
    this.m_creRoomLimit = this.m_layCreateRec.getChildByName("pri_sp_roomlimit");
    this.m_creCost = this.m_layCreateRec.getChildByName("pri_sp_createcost");
    this.m_creDisTime = this.m_layCreateRec.getChildByName("pri_sp_dissolvetime");
    this.m_creAward = this.m_layCreateRec.getChildByName("pri_sp_award");
    this.m_creStatus = this.m_layCreateRec.getChildByName("pri_sp_roomstatus");

    // 参与记录
    this.m_layJoinRec = csbNode.getChildByName("lay_join");
    this.m_layJoinRec.setVisible(false);
    this.m_joinCreateTime = this.m_layJoinRec.getChildByName("pri_sp_createtime");
    this.m_joinRoomID = this.m_layJoinRec.getChildByName("pri_sp_roomid");
    this.m_joinCreateUser = this.m_layJoinRec.getChildByName("pri_sp_createuser");
    this.m_joinUinfo = this.m_layJoinRec.getChildByName("pri_sp_uinfo");
    this.m_joinDisTime = this.m_layJoinRec.getChildByName("pri_sp_dissolvetime");

    var content = csbNode.getChildByName("lay_content");
    // 列表
    var m_tableView = cc.TableView(content.getContentSize());
    m_tableView.setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL);
    m_tableView.setPosition(content.getPosition());
    m_tableView.setDelegate();
    m_tableView.registerScriptHandler(this.cellSizeForTable, cc.TABLECELL_SIZE_FOR_INDEX);
    m_tableView.registerScriptHandler(handler(this, this.tableCellAtIndex), cc.TABLECELL_SIZE_AT_INDEX);
    m_tableView.registerScriptHandler(handler(this, this.numberOfCellsInTableView), cc.NUMBER_OF_CELLS_IN_TABLEVIEW);
    csbNode.addChild(m_tableView);
    this.m_tableView = m_tableView;
    content.removeFromParent();

    // 房间信息
    this.m_layRoomDetail = null;
}

RoomRecordLayer.onSelectedEvent = function ( tag,sender,eventType ) {
    var sel = sender.isSelected();
    //this.m_layCreateRec.setVisible(sel)
    this.m_layCreateRec.setVisible(false);
    this.m_spRecordBg2.setVisible(sel);
    //this.m_layJoinRec.setVisible(not sel)
    this.m_layJoinRec.setVisible(false);
    this.m_spRecordBg1.setVisible(!sel);

    if (!sel && 0 == PriRoom.getInstance().m_tabJoinRecord.length) {
        PriRoom.getInstance().showPopWait();
        PriRoom.getInstance().getNetFrame().onQueryJoinList();
        return;
    }
    this.m_tableView.reloadData();
}
RoomRecordLayer.onEnterTransitionFinish = function () {
    PriRoom.getInstance().showPopWait();
    // 请求记录列表  
    PriRoom.getInstance().getNetFrame().onQueryJoinList();
}
RoomRecordLayer.onExit = function () {
    // 清除缓存
    PriRoom.getInstance().m_tabJoinRecord = {};
    PriRoom.getInstance().m_tabCreateRecord = {};
}

RoomRecordLayer.onReloadRecordList =function () {
    this.m_tableView.reloadData();
    var rd = this.getChildByName(ROOMDETAIL_NAME);
    if (null != rd)
        rd.hide();
}

RoomRecordLayer.cellSizeForTable = function ( view, idx ) {
    return 1042, 80;
}

RoomRecordLayer.numberOfCellsInTableView = function ( view ) {
    if (this.m_checkSwitch.isSelected())
        return PriRoom.getInstance().m_tabCreateRecord.length;
    else
        return PriRoom.getInstance().m_tabJoinRecord.length
}

RoomRecordLayer.tableCellAtIndex = function ( view, idx ) {
    var cell = view.dequeueCell();
    if (!cell)
        cell = cc.TableViewCell.new();
    else
        cell.removeAllChildren();

    if (this.m_checkSwitch.isSelected()) {
        var tabData = PriRoom.getInstance().m_tabCreateRecord[idx + 1];
        var item = thisRecordItem(tabData);
        item.setPosition(view.getViewSize().width * 0.5, 25);
        cell.addChild(item);
    } else {
        var tabData = PriRoom.getInstance().m_tabJoinRecord[idx + 1];
        var item = this.joinRecordItem(tabData);
        item.setPosition(view.getViewSize().width * 0.5, 25);
        cell.addChild(item);
    }

    return cell;
}

// 创建记录
RoomRecordLayerRecordItem = function ( tabData ) {
    //tabData = tagPersonalRoomInfo
    var item = ccui.Widget();
    item.setContentSize(cc.size(1042, 80));

    // 线
    var frame = cc.spriteFrameCache.getSpriteFrame("pri_sp_listline1.png");
    if (null != frame) {
        var sp = cc.Sprite(frame);
        item.addChild(sp);
        sp.setPosition(1042 / 2, 80 / 2);
    }

    // 创建时间
    var tabTime = tabData.sysCreateTime;
    //var strTime = string.format("%d-%02d-%02d %02d.%02d.%02d", tabTime.wYear, tabTime.wMonth, tabTime.wDay, tabTime.wHour, tabTime.wMinute, tabTime.wSecond)
    var strTime = string.format("%d-%02d-%02d", tabTime.wYear, tabTime.wMonth, tabTime.wDay);
    var createtime = cc.LabelTTF(strTime, res.round_body_ttf, 20);
    createtime.setColor(cc.color(108, 53, 21));
    item.addChild(createtime);
    createtime.setPosition(this.m_creCreateTime.getPositionX(), 35);

    // 创建时间
    var tabTime2 = tabData.sysCreateTime;
    var strTime2 = string.format("%02d.%02d.%02d", tabTime.wHour, tabTime.wMinute, tabTime.wSecond);
    var createtime2 = cc.LabelTTF(strTime2, res.round_body_ttf, 20);
    createtime2.setColor(cc.color(108, 53, 21));
    item.addChild(createtime2);
    createtime2.setPosition(this.m_creCreateTime.getPositionX(), 15);

    // 房间ID
    var roomid = cc.LabelTTF(tabData.szRoomID, res.round_body_ttf, 20);
    roomid.setColor(cc.color(108, 53, 21));
    item.addChild(roomid);
    roomid.setPosition(this.m_creRoomID.getPositionX(), 25);

    // 房间限制
    var roomlimit = cc.LabelTTF(tabData.dwPlayTurnCount + "", res.round_body_ttf, 20);
    roomlimit.setColor(cc.color(108, 53, 21));
    item.addChild(roomlimit);
    roomlimit.setPosition(this.m_creRoomLimit.getPositionX(), 25);

    var feeType = "房卡";
    if (tabData.cbCardOrBean == 0)
        feeType = "游戏豆";

    // 创建消耗
    var cost = cc.LabelTTF(tabData.lFeeCardOrBeanCount + feeType, res.round_body_ttf, 20);
    cost.setColor(cc.color(108, 53, 21));
    item.addChild(cost);
    cost.setPosition(this.m_creCost.getPositionX(), 25);

    // 奖励
    var award = cc.LabelTTF(tabData.lScore + "游戏币", res.round_body_ttf, 20);
    award.setColor(cc.color(108, 53, 21));
    item.addChild(award);
    award.setPosition(this.m_creAward.getPositionX(), 25);

    // 房间状态
    var bOnGame = false;
    var status = cc.LabelTTF("", res.round_body_ttf, 20);
    if (tabData.cbIsDisssumRoom == 1) {// 解散
        status.setColor(cc.color(217, 51, 6));
        status.setString("已解散");
        tabTime = tabData.sysDissumeTime;
        strTime = string.format("%d-%02d-%02d", tabTime.wYear, tabTime.wMonth, tabTime.wDay);
        strTime2 = string.format("%02d.%02d.%02d", tabTime.wHour, tabTime.wMinute, tabTime.wSecond);
    } else {// 游戏中
        status.setColor(cc.color(64, 121, 9));
        status.setString("游戏中");
        bOnGame = true;
        strTime = "";
        strTime2 = "";
    }
    item.addChild(status);
    status.setPosition(this.m_creStatus.getPositionX(), 25);

    // 解散时间    
    var distime = cc.LabelTTF(strTime, res.round_body_ttf, 20);
    distime.setColor(cc.color(244, 237, 182));
    item.addChild(distime);
    distime.setPosition(this.m_creDisTime.getPositionX(), 35);

    var distime2 = cc.LabelTTF(strTime2, res.round_body_ttf, 20);
    distime2.setColor(cc.color(244, 237, 182));
    item.addChild(distime2);
    distime2.setPosition(this.m_creDisTime.getPositionX(), 15);

    item.setTouchEnabled(true);
    item.setSwallowTouches(false);
    var itemFunC = function (ref, tType) {
        if (tType == ccui.TouchEventType.ended) {
            var tabDetail = tabData;
            tabDetail.onGame = bOnGame;
            tabDetail.enableDismiss = true;
            var rd = RoomDetailLayer(tabDetail);
            rd.setName(ROOMDETAIL_NAME);
            this.addChild(rd);
        }
    }
    item.addTouchEventListener(itemFunC);
    return item;
}

// 参与记录
RoomRecordLayer.joinRecordItem = function( tabData ) {
    //tabData = tagQueryPersonalRoomUserScore
    var item = ccui.Widget();
    item.setContentSize(cc.size(1130, 50));

    // 线
    var frame = cc.spriteFrameCache.getSpriteFrame("pri_sp_listline1.png");
    if (null != frame) {
        var sp = cc.Sprite(frame);
        item.addChild(sp);
        sp.setPosition(565, 20);
    }

    // 创建时间
    var tabTime = tabData.sysCreateTime;
    var strTime = string.format("%d-%02d-%02d", tabTime.wYear, tabTime.wMonth, tabTime.wDay);
    var createtime = cc.LabelTTF(strTime, res.round_body_ttf, 20);
    createtime.setColor(cc.color(108, 53, 21));
    item.addChild(createtime);
    createtime.setPosition(this.m_joinCreateTime.getPositionX() + 15, 35);

    var strTime2 = string.format("%02d.%02d.%02d", tabTime.wHour, tabTime.wMinute, tabTime.wSecond);
    var createtime2 = cc.LabelTTF(strTime2, res.round_body_ttf, 20);
    createtime2.setColor(cc.color(108, 53, 21));
    item.addChild(createtime2);
    createtime2.setPosition(this.m_joinCreateTime.getPositionX() + 15, 15);

    // 房间ID
    var roomid = cc.LabelTTF(tabData.szRoomID, res.round_body_ttf, 20);
    roomid.setColor(cc.color(108, 53, 21));
    item.addChild(roomid);
    roomid.setPosition(this.m_joinRoomID.getPositionX() + 15, 25);

    // 创建玩家
    var createusr = ClipTextClipText(cc.size(120, 30), tabData.szUserNicname, res.round_body_ttf, 20);
    createusr.setAnchorPoint(cc.p(0.5, 0.5));
    createusr.setColor(cc.color(108, 53, 21, 255));
    item.addChild(createusr);
    createusr.setPosition(this.m_joinCreateUser.getPositionX() + 15, 25);

    var scorestr = "+" + tabData.lScore;
    if (tabData.lScore < 0)
        scorestr = "" + tabData.lScore;

    if (tabData.bFlagOnGame)
        scorestr = "";

    // 个人战绩
    var uinfo = ClipTextClipText(cc.size(150, 30), scorestr, res.round_body_ttf, 20);
    uinfo.setAnchorPoint(cc.p(0.5, 0.5));
    uinfo.setColor(cc.color(217, 51, 6, 255));
    item.addChild(uinfo);
    uinfo.setPosition(this.m_joinUinfo.getPositionX() + 15, 25);

    // 解散时间
    tabTime = tabData.sysDissumeTime;
    strTime = string.format("%d-%02d-%02d", tabTime.wYear, tabTime.wMonth, tabTime.wDay);
    strTime2 = string.format("%02d.%02d.%02d", tabTime.wHour, tabTime.wMinute, tabTime.wSecond);
    var distime = cc.LabelTTF(strTime, res.round_body_ttf, 20);
    var distime2 = cc.LabelTTF(strTime2, res.round_body_ttf, 20);
    distime.setPosition(this.m_joinDisTime.getPositionX() + 15, 35);
    distime2.setPosition(this.m_joinDisTime.getPositionX() + 15, 15);
    distime.setColor(cc.color(108, 53, 21));
    distime2.setColor(cc.color(108, 53, 21));
    if (tabData.bFlagOnGame) {
        distime.setString("游戏中");
        distime.setColor(cc.color(108, 53, 21));
        distime.setPosition(this.m_joinDisTime.getPositionX() + 15, 25);
        distime2.setColor(cc.color(108, 53, 21));
        distime2.setString("");
    }
    item.addChild(distime);
    item.addChild(distime2);


    item.setTouchEnabled(true);
    item.setSwallowTouches(false);
    var itemFunC = function (ref, tType) {
        if (tType == ccui.TouchEventType.ended) {
            var tabDetail = tabData;
            tabDetail.onGame = false;//tabData.bFlagOnGame or false
            tabDetail.enableDismiss = false;
            var rd = RoomDetailLayer(tabDetail);
            rd.setName(ROOMDETAIL_NAME);
            this.addChild(rd);
        }
    }
    item.addTouchEventListener(itemFunC);
    return item;
}
RoomRecordLayer.onButtonClickedEvent = function ( tag, sender ) {
    this.scene.onChangeShowMode();
}