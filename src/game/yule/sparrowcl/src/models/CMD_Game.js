var ccmd = {};

ccmd.RES_PATH 				    = "src/game/yule/sparrowcl/res/";
//游戏版本
ccmd.VERSION 					= appdf.VersionValue(6,7,0,1);
//游戏标识
ccmd.KIND_ID						= 391;
//游戏人数
ccmd.GAME_PLAYER					= 4;
//视图位置(1~3)
ccmd.MY_VIEWID					= 2;
//最大牌数
ccmd.MAX_COUNT 					= 14;
//最大库存
ccmd.MAX_REPERTORY 				= 136;
//最大组合
ccmd.MAX_WEAVE 					= 4;

// 语音动画
ccmd.VOICE_ANIMATION_KEY 		= "voice_ani_key";

//********************       定时器标识         ***************//
ccmd.IDI_START_GAME				= 201;						//开始定时器
ccmd.IDI_OUT_CARD				= 202;						//出牌定时器
ccmd.IDI_OPERATE_CARD			= 203;						//操作定时器

//*******************        时间标识         *****************//
//快速出牌时间
ccmd.TIME_OUT_CARD_FAST			= 10;
//出牌时间
ccmd.TIME_OUT_CARD				= 30;
//准备时间
ccmd.TIME_START_GAME 			= 30;


//******************         游戏状态             ************//
//等待开始
ccmd.GAME_SCENE_FREE				= 0;
//叫庄状态
ccmd.GAME_SCENE_PLAY				= 100;
//ysy——start******         是否存在听牌         ************//
ccmd.SHOW_TING                   = false;
ccmd.EXISTENCE_HUI               = false;
ccmd.EXISTENCE_JUE               = false;
//ysy——end////////////////////////////////////////////////////
//组合子项
ccmd.tagWeaveItem = [
	{k : "cbWeaveKind", t : "dword"},							//组合类型
	{k : "cbCenterCard", t : "byte"},							//中心扑克
	{k : "cbParam", t : "byte"},								//类型标志
	{k : "wProvideUser", t : "word"},							//供应用户
	{k : "cbCardData", t : "byte", l : [4]}					//麻将数据
];

//空闲状态
ccmd.CMD_S_StatusFree =
[
	//基础积分
	{k : "lCellScore", t : "int"},								//基础积分
	//时间信息
 	{k : "cbTimeOutCard", t : "byte"},							//出牌时间
 	{k : "cbTimeOperateCard", t : "byte"},						//操作时间
 	{k : "cbTimeStartGame", t : "byte"},						//开始时间
	//历史积分
	{k : "lTurnScore", t : "score", l : [ccmd.GAME_PLAYER]},		//积分信息
	{k : "lCollectScore", t : "score", l : [ccmd.GAME_PLAYER]},	//积分信息
	{k : "cbPlayerCount", t : "byte"},
	{k : "cbMaCount", t : "byte"},
     //-ysy_s
    {k : "cbQuanCount", t : "byte"},                            //已经打多少圈
    {k : "lDeltaScore", t : "score", l : [ccmd.GAME_PLAYER]},    //各玩家分数的变化值
    {k : "dwRoundCount", t : "dword"},                          //局数分子
    {k : "dwRoundLimit", t : "dword"}                           //局数分母
    //-ysy_e  
];
//游戏状态
ccmd.CMD_S_StatusPlay =
[
	//时间信息
	{k : "cbTimeOutCard", t : "byte"},							//出牌时间
	{k : "cbTimeOperateCard", t : "byte"},						//叫分时间
	{k : "cbTimeStartGame", t : "byte"},						//开始时间
	//游戏变量
	{k : "lCellScore", t : "score"},							//单元积分
	{k : "wBankerUser", t : "word"},							//庄家用户
	{k : "wCurrentUser", t : "word"},							//当前用户
    //ysy_start
	{k : "cbMagicIndex", t : "byte"},							//墙头牌
    {k : "ctRoundNo", t : "int"},						    //局号
    //ysy_end
	{k : "cbPlayerCount", t : "byte"},
	{k : "cbMaCount", t : "byte"},

	{k : "cbActionCard", t : "byte"},							//动作扑克
	{k : "cbActionMask", t : "dword"},							//动作掩码
	{k : "cbLeftCardCount", t : "byte"},						//剩余数目
	{k : "bTrustee", t : "bool", l : [ccmd.GAME_PLAYER]},		//是否托管
	{k : "bTing", t : "bool", l : [ccmd.GAME_PLAYER]},			//是否听牌
	{k : "wOutCardUser", t : "word"},							//出牌用户
	{k : "cbOutCardData", t : "byte"},							//出牌扑克
	{k : "cbDiscardCount", t : "byte", l : [ccmd.GAME_PLAYER]},	//丢弃数目
	{k : "cbDiscardCard", t : "byte", l : [60,60,60,60]},       //丢弃记录
	{k : "cbCardCount", t : "byte", l : [ccmd.GAME_PLAYER]},		//扑克数目
	{k : "cbCardData", t : "byte", l : [ccmd.MAX_COUNT]},		//扑克列表
	{k : "cbSendCardData", t : "byte"},							//发送扑克
    {k : "cbChaseArrowArray", t : "byte", l : [34, 34, 34, 34]},//追风个数
	{k : "cbWeaveItemCount", t : "byte", l : [ccmd.GAME_PLAYER]},//组合数目
	{k : "WeaveItemArray", t : "table", d : ccmd.tagWeaveItem, l : [4, 4, 4, 4]},//组合扑克
	{k : "wHeapHead", t : "word"},								//堆立头部
	{k : "wHeapTail", t : "word"},								//堆立尾部
	{k : "cbHeapCardInfo", t : "byte", l : [2, 2, 2, 2]},		//牌堆信息

	//用于提示听牌
	{k : "cbHuCardCount", t : "byte", l : [ccmd.MAX_COUNT]},
	{k : "cbHuCardData", t : "byte", l : [28,28,28,28,28,28,28,28,28,28,28,28,28,28]},
	{k : "cbOutCardCount", t : "byte"},
	{k : "cbOutCardDataEx", t : "byte", l : [ccmd.MAX_COUNT]},
	//历史积分
	{k : "lTurnScore", t : "score", l : [ccmd.GAME_PLAYER]},		//积分信息
	{k : "lCollectScore", t : "score", l : [ccmd.GAME_PLAYER]},	//积分信息
    {k : "cbQuanCount", t : "byte"},                             //游戏圈数
    //-ysy_s    
    {k : "lDeltaScore", t : "score", l : [ccmd.GAME_PLAYER]},   //各玩家分数的变化值
    {k : "dwRoundCount", t : "dword"},                          //局数分子
    {k : "dwRoundLimit", t : "dword"}                            //局数分母
    //-ysy_e 
];

//*********************      服务器命令结构       ************//
ccmd.SUB_S_GAME_START			= 100;								//游戏开始
ccmd.SUB_S_OUT_CARD				= 101;								//用户出牌
ccmd.SUB_S_SEND_CARD				= 102;								//发送扑克
ccmd.SUB_S_OPERATE_NOTIFY		= 103;								//操作提示
ccmd.SUB_S_OPERATE_RESULT		= 104;								//操作命令
ccmd.SUB_S_LISTEN_CARD			= 105;								//用户听牌
ccmd.SUB_S_TRUSTEE				= 106;								//用户托管
ccmd.SUB_S_REPLACE_CARD			= 107;								//用户补牌
ccmd.SUB_S_GAME_CONCLUDE			= 108;								//游戏结束
ccmd.SUB_S_SET_BASESCORE			= 109;								//设置基数
ccmd.SUB_S_HU_CARD				= 110;								//听牌胡牌数据
ccmd.SUB_S_RECORD				= 111;								//房卡结算记录
ccmd.SUB_S_ANSWER_RULE			= 112;						        //游戏规则
ccmd.SUB_S_DISTANCE              = 115;                               //发送距离警告

//发送扑克
ccmd.CMD_S_GameStart =
[
	{k : "wBankerUser", t : "word"},							//庄家用户
	{k : "wReplaceUser", t : "word"},							//补花用户
	{k : "wSiceCount", t : "word"},								//筛子点数
	{k : "wHeapHead", t : "word"},								//牌堆头部
	{k : "wHeapTail", t : "word"},								//牌堆尾部
    //ysy_start//
	{k : "cbMagicIndex", t : "byte"},							//墙头牌
    {k : "ctRoundNo", t : "int"},						    //局号
    //ysy_end//
	{k : "cbHeapCardInfo", t : "byte", l : [2,2,2,2]},          //堆立信息
	{k : "cbUserAction", t : "dword"},							//用户动作
	{k : "cbCardData", t : "byte", l : [ccmd.MAX_COUNT]},		//麻将列表
    {k : "cbOutCardCount", t : "byte"},                          //？？？
    {k : "cbOutCardData", t : "byte", l : [28,28,28,28,28,28,28,28,28,28,28,28,28,28]},		//???
    {k : "cbQuanCount", t : "byte"}  ,                           //游戏圈数
     //-ysy_s    
    {k : "lDeltaScore", t : "score", l : [ccmd.GAME_PLAYER]},     //各玩家分数的变化值
    {k : "dwRoundCount", t : "dword"},                           //局数分子
    {k : "dwRoundLimit", t : "dword"}                            //局数分母
     //-ysy_e 

];

//用户出牌
ccmd.CMD_S_OutCard =
[
	{k : "wOutCardUser", t : "word"},							//出牌用户
	{k : "cbOutCardData", t : "byte"},							//出牌扑克
	{k : "bSysOut", t : "bool"},								//托管系统出牌
];
//发送扑克
ccmd.CMD_S_SendCard =
[
	{k : "cbCardData", t : "byte"},								//扑克数据
	{k : "cbActionMask", t : "dword"},							//动作掩码
	{k : "wCurrentUser", t : "word"},							//当前用户
	{k : "wSendCardUser", t : "word"},							//发牌用户
	{k : "wReplaceUser", t : "word"},							//补牌用户
	{k : "bTail", t : "bool"},									//末尾发牌
];
//操作提示
ccmd.CMD_S_OperateNotify =
[
	{k : "cbActionMask", t : "dword"},							//动作掩码
	{k : "cbActionCard", t : "byte"}							//动作扑克
];
//操作命令
ccmd.CMD_S_OperateResult =
[
	{k : "wOperateUser", t : "word"},							//操作用户
	{k : "cbActionMask", t : "dword"},							//动作掩码
	{k : "wProvideUser", t : "word"},							//供应用户
	{k : "cbOperateCode", t : "dword"},							//操作代码
	{k : "cbOperateCard", t : "byte", l : [3]},					//操作扑克
];
//提示听牌
ccmd.CMD_S_Hu_Data =
[
	{k : "cbOutCardCount", t : "byte"},
	{k : "cbOutCardData", t : "byte", l : [14]},
	{k : "cbHuCardCount", t : "byte", l : [14]},
	{k : "cbHuCardData", t : "byte", l : [28, 28, 28, 28, 28, 28, 28,
											28, 28, 28, 28, 28, 28, 28]},
	{k : "cbHuCardRemainingCount", t : "byte", l : [28, 28, 28, 28, 28, 28, 28,
											28, 28, 28, 28, 28, 28, 28]},
];
//听牌操作命令
ccmd.CMD_S_ListenCard =
[
	{k : "wListenUser", t : "word"},							//听牌用户
	{k : "bListen", t : "bool"},								//是否听牌

	{k : "cbHuCardCount", t : "byte"},
	{k : "cbHuCardData", t : "byte", l : [34]},
];
//游戏结束
ccmd.CMD_S_GameConclude =
[
	{k : "lCellScore", t : "int"},								//单元积分
	{k : "lGameScore", t : "score", l : [ccmd.GAME_PLAYER]},		//游戏积分
	{k : "lRevenue", t : "score", l : [ccmd.GAME_PLAYER]},		//税收积分
	{k : "lGangScore", t : "score", l : [ccmd.GAME_PLAYER]},		//本局杠输赢分
	{k : "wProvideUser", t : "word"},							//供应用户
	{k : "cbProvideCard", t : "byte"},							//供应扑克
	{k : "cbSendCardData", t : "byte"},							//最后发牌
	{k : "cbChiHuKind", t : "dword", l : [ccmd.GAME_PLAYER]},	//胡牌类型
	{k : "dwChiHuRight", t : "dword", l : [1, 1, 1, 1]},		//胡牌类型
	{k : "wLeftUser", t : "word"},								//玩家逃跑
	{k : "wLianZhuang", t : "word"},							//连庄
	{k : "cbCardCount", t : "byte", l : [ccmd.GAME_PLAYER]},		//扑克数目
	{k : "cbHandCardData", t : "byte", l : [14, 14, 14, 14]},	//扑克列表
    {k : "cbJue", t : "byte"},							        //绝个数
	{k : "cbWallTopJue", t : "byte"},							//是否有墙头绝
    {k : "lHuScore", t : "score", l : [ccmd.GAME_PLAYER]},		//游戏胡分
	{k : "lJueScore", t : "score", l : [ccmd.GAME_PLAYER]},		//绝分
    ////-ysy_end
];
//用户托管
ccmd.CMD_S_Trustee =
[
	{k : "bTrustee", t : "bool"},								//是否托管
	{k : "wChairID", t : "word"}								//托管用户
];
//补牌命令
ccmd.CMD_S_ReplaceCard =
[
	{k : "wReplaceUser", t : "word"},							//补牌用户
	{k : "cbReplaceCard", t : "byte"}							//补牌扑克
];
//游戏记录
ccmd.CMD_S_Record =
[
	{k : "nCount", t : "int"},									//
	{k : "cbHuCount", t : "byte", l : [ccmd.GAME_PLAYER]},		//胡牌次数
	{k : "cbMaCount", t : "byte", l : [ccmd.GAME_PLAYER]},		//中码个数
	{k : "cbAnGang", t : "byte", l : [ccmd.GAME_PLAYER]},		//暗杠次数
	{k : "cbMingGang", t : "byte", l : [ccmd.GAME_PLAYER]},		//明杠次数
	{k : "lAllScore", t : "score", l : [ccmd.GAME_PLAYER]},		//总结算分
	{k : "lDetailScore", t : "score", l : [32, 32, 32, 32]},	//单局结算分
    //-ysy_start   
    {k : "cbBankerCount", t : "byte", l : [ccmd.GAME_PLAYER]},		        //坐庄次数
    {k : "cbMaxChainBankerCount", t : "byte", l : [ccmd.GAME_PLAYER]},		//最大连庄次数
    {k : "cbZimoCount", t : "byte", l : [ccmd.GAME_PLAYER]},		            //自摸次数
    {k : "cbDianPaoCount", t : "byte", l : [ccmd.GAME_PLAYER]},		        //给他人点炮次数
    {k : "cbWindCount", t : "byte", l : [ccmd.GAME_PLAYER]},		            //杠出【东南西北】的次数
    {k : "cbArrowCount", t : "byte", l : [ccmd.GAME_PLAYER]},		        //杠出【中发白】的次数
    {k : "cbWTOtherCount", t : "byte", l : [ccmd.GAME_PLAYER]},		        //杠出【墙头他杠】的次数
    {k : "cbWTSelfCount", t : "byte", l : [ccmd.GAME_PLAYER]},		        //杠出【墙头自杠】的次数
    {k : "cbJueCount", t : "byte", l : [ccmd.GAME_PLAYER]},		            //胡牌时出现绝的总个数
    //-ysy_end 
];

//距离警告
ccmd.CMD_S_Distance =
[
    {k : "wFirstChairID", t : "word"},                           //首玩家
    {k : "wSecondChairID", t : "word"},                            //尾玩家
    {k : "nDistance", t : "score"},                               //距离
];


//**********************    客户端命令结构        ************//
ccmd.SUB_C_OUT_CARD				= 1;									//出牌命令
ccmd.SUB_C_OPERATE_CARD			= 2;									//操作扑克
ccmd.SUB_C_LISTEN_CARD			= 3;									//用户听牌
ccmd.SUB_C_TRUSTEE				= 4;									//用户托管
ccmd.SUB_C_REPLACE_CARD			= 5;									//用户补牌
ccmd.SUB_C_SEND_CARD             = 6;									//发送扑克
ccmd.SUB_C_ASK_RULE              = 9;                                 //查询游戏规则
ccmd.SUB_C_DISTANCE              = 11;                                //用户距离

//出牌命令
ccmd.CMD_C_OutCard =
[
	{k : "cbCardData", t : "byte"}								//扑克数据
];
//操作命令
ccmd.CMD_C_OperateCard =
[
	{k : "cbOperateCode", t : "dword"},							//操作代码
	{k : "cbOperateCard", t : "byte", l : [3]}					//操作扑克
];
//用户听牌
ccmd.CMD_C_ListenCard =
[
	{k : "bListenCard", t : "bool"}								//是否听牌
];
//用户托管
ccmd.CMD_C_Trustee =
[
	{k : "bTrustee", t : "bool"}								//是否托管
];
//补牌命令
ccmd.CMD_C_ReplaceCard =
[
	{k : "cbCardData", t : "byte"}								//扑克数据
];
//发送扑克
ccmd.CMD_C_SendCard =
[
	{k : "cbControlGameCount", t : "byte"},						//控制次数
	{k : "cbCardCount", t : "byte"},							//扑克数目
	{k : "wBankerUser", t : "word"},							//控制庄家
	{k : "cbCardData", t : "byte", l : [ccmd.MAX_REPERTORY]}		//扑克数据
];
//游戏规则
ccmd.CMD_C_CustomRule =
[
    {k : "bCustomRule", t : "byte"},						    //定制规则（1,0）
	{k : "cbTimes", t : "byte"},							    //圈数	（4,8）
	{k : "cbDianPao", t : "byte"},							    //点炮包三家（0，n）
	{k : "cbWindOrBird", t : "byte"},	                        //风杠或鸟杠（可赞，不可攒）
    {k : "cbBigWind", t : "byte"},						        //大风刮一吃到底(0,n)
	{k : "cbPlayer", t : "byte"}							    //(2/3/4)人麻将
    
];
//距离警告
ccmd.CMD_C_Distance=
[
    {k : "dwUserID", t : "dword"},
    {k : "nJuLi", t : "score"}
];