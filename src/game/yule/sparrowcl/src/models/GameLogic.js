var GameLogic = {};

var cmd = appdf.req(appdf.GAME_SRC+"yule.sparrowcl.src.models.CMD_Game");

//牌库数目
GameLogic.FULL_COUNT				= 112;
GameLogic.MAGIC_DATA 				= 0x35; // 53
GameLogic.MAGIC_INDEX 				= 31;
GameLogic.NORMAL_DATA_MAX 			= 0x29; // 41
//GameLogic.NORMAL_INDEX_MAX 			= 27
GameLogic.NORMAL_INDEX_MAX 			= 33;

//显示类型
GameLogic.SHOW_NULL					= 0;							//无操作
GameLogic.SHOW_CHI 					= 1;						//吃
GameLogic.SHOW_PENG					= 2;						//碰
GameLogic.SHOW_MING_GANG			= 3;						//明杠（碰后再杠）
GameLogic.SHOW_FANG_GANG			= 4;						//放杠
GameLogic.SHOW_AN_GANG				= 5;						//暗杠

//-<////////////////////////////////////-<WW_ZhuiFengGang>////////////////////////////////////////////////
GameLogic.SHOW_ZHUI_GANG			= 6;							//追风杠
GameLogic.SHOW_XUAN_GANG			= 7;							//旋风杠
//->////////////////////////////////////-<WW_ZhuiFengGang>////////////////////////////////////////////////
//动作标志
GameLogic.WIK_NULL					= 0x00;						//没有类型//0
GameLogic.WIK_LEFT					= 0x01;						//左吃类型//1
GameLogic.WIK_CENTER				= 0x02;						//中吃类型//2
GameLogic.WIK_RIGHT					= 0x04;						//右吃类型//4
GameLogic.WIK_PENG					= 0x08;						//碰牌类型//8
GameLogic.WIK_GANG					= 0x10;						//杠牌类型//16
GameLogic.WIK_LISTEN				= 0x20;						//听牌类型//32
GameLogic.WIK_CHI_HU				= 0x40;						//吃胡类型//64
GameLogic.WIK_FANG_PAO				= 0x80;						//放炮//128
GameLogic.WIK_FENG_GANG				= 0x90;						//风杠//144
//动作类型
GameLogic.WIK_GANERAL				= 0x00;						//普通操作
GameLogic.WIK_MING_GANG				= 0x01;						//明杠（碰后再杠）
GameLogic.WIK_FANG_GANG				= 0x02;						//放杠
GameLogic.WIK_AN_GANG				= 0x03;						//暗杠

GameLogic.WIK_WALLTOP_S				= 0x00000400;				//【墙头自杠】三张都来自【自己】,属于暗杠	(Wall Top Self)
GameLogic.WIK_WALLTOP_O				= 0x00000800;				//【墙头他杠】一张来自【他人】,其实是碰		(Wall Top Others)
//-<//////////////////-<WW_ZhuiFengGang>////////////////////////////////
GameLogic.WIK_CHASEARROW			= 0x00000100;				//【中发白】的杠追加【中发白】牌后的杠
GameLogic.WIK_CHASEWIND				= 0x00000200;				//【东南西北】的杠追加风牌后的杠
//
GameLogic.WIK_CHASEBIRD				= 0x00001000;				//【幺鸡、中发白】杠后追加的【中发白+幺鸡】
GameLogic.WIK_ARROW					= 0X00002000;				//【中发白】三张【箭牌】在一起组成的杠
GameLogic.WIK_WIND					= 0x00004000;				//【东南西北】四张【风牌】在一起组成的杠
GameLogic.WIK_BIRD					= 0x00008000;				//【幺鸡、中发白】在一起组成的杠
//->//////////////////-<WW_ZhuiFengGang>////////////////////////////////
GameLogic.varCardData = [
	0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09,
	0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19,
	0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29,
	0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37
];

GameLogic.TotalCardData =[
	0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09,
	0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09,
	0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09,
	0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09,
	0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19,
	0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19,
	0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19,
	0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19,
	0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29,
	0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29,
	0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29,
	0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29,
	0x31, 0x31, 0x31, 0x31,
    0x32, 0x32, 0x32, 0x32,
    0x33, 0x33, 0x33, 0x33,
    0x34, 0x34, 0x34, 0x34,
    0x35, 0x35, 0x35, 0x35,
    0x36, 0x36, 0x36, 0x36,
    0x37, 0x37, 0x37, 0x37
];
GameLogic.HuiCardData = [];

GameLogic.SwitchToCardIndex = function(card) {
    var index = 0;
    for (var i = 0; i < GameLogic.varCardData.length; i++) {
        if (GameLogic.varCardData[i] == card) {
            index = i;
            break;
        }
    }
    var strError = sprintf("The card %d is error!", card);
    appdf.assert(index != 0, strError);
    return index;
};

GameLogic.SwitchToCardData = function(index){
    appdf.assert(index >= 0 && index <= 33, "The card index is error!");
    return GameLogic.varCardData[index];
};

GameLogic.DataToCardIndex = function(cbCardData) {
    appdf.assert(cbCardData != null);
    //初始化
    var cbCardIndex = {};
    for (var i = 0; i < 34; i++) {
        cbCardIndex[i] = 0;
    }
    //累加
    for (var i = 0; i < cbCardData.length; i++) {
        var bCardExist = false;
        for (var j = 0; j < 34; j++) {
            if (cbCardData[i] == GameLogic.varCardData[j]) {
                cbCardIndex[j] = cbCardIndex[j] + 1;
                bCardExist = true;
            }
        }
        appdf.assert(bCardExist, "This card is not exist!");
    }

    return cbCardIndex;
};

//删除扑克
GameLogic.RemoveCard = function(cbCardData, cbRemoveCard) {
    appdf.assert(cbCardData != null && cbRemoveCard != null);
    var cbCardCount = cbCardData.length;
    var cbRemoveCount = cbRemoveCard.length;
    appdf.assert(cbRemoveCount <= cbCardCount);

    //置零扑克
    for (var i = 0; i < cbRemoveCount; i++) {
        for (var j = 0; j < cbCardCount; j++) {
            if (cbRemoveCard[i] == cbCardData[j]) {
                cbCardData[j] = 0;
                break;
            }
        }
    }
    //清理扑克
    var resultData = [];
    var cbCardPos = 0;
    for (var i = 0; i < cbCardCount; i++) {
        if (cbCardData[i] != 0) {
            resultData[cbCardPos] = cbCardData[i];
            cbCardPos = cbCardPos + 1;
        }
    }

    return resultData;
};

//混乱扑克
GameLogic.RandCardList = function(cbCardData) {
    appdf.assert(cbCardData != null);
    //混乱准备
    var cbCardCount = cbCardData.length;
    //todo
    var cbCardTemp = cbCardData;
    // var cbCardTemp = clone(cbCardData);
    var cbCardBuffer = [];
    //开始
    var cbRandCount = 0;
    var cbPosition = 0;
    while (cbRandCount < cbCardCount) {
        cbPosition = Math.random(cbCardCount - cbRandCount);
        cbCardBuffer[cbRandCount] = cbCardTemp[cbPosition];
        cbCardTemp[cbPosition] = cbCardTemp[cbCardCount - cbRandCount];
        cbRandCount = cbRandCount + 1;
    }
    return cbCardBuffer;
};

//排序
GameLogic.SortCardList = function(cbCardData) {
    //校验
    appdf.assert(cbCardData != null && cbCardData.length > 0);
    var cbCardCount = cbCardData.length;
    //排序操作
    var bSorted = false;
    var cbLast = cbCardCount - 1;
    while (bSorted == false) {
        bSorted = true;
        for (var i = 0; i < cbLast; i++) {
            if (cbCardData[i] > cbCardData[i + 1]){
                bSorted = false;
                var temp = cbCardData[i + 1];
                cbCardData[i + 1] = cbCardData[i];
                cbCardData[i] = temp;
            }
        }
        cbLast = cbLast - 1;
    }
};

GameLogic.RemoveNull = function(cbCardData){
    while(cbCardData[cbCardData.length - 1] == null){
        cbCardData.splice(cbCardData.length - 1, 1);
    }
    return cbCardData;
};

////////////////-ysy——start//////////////////////////////////////////////////////-
//排序2//会牌
GameLogic.SortCardList2 = function(cbCardData) {
    //校验
    appdf.assert(cbCardData != null && cbCardData.length > 0);
    var cbCardCount = cbCardData.length;

    var cbHuiCard = GameLogic.getHuiCard();
    var cbHuiCount = cbHuiCard.length;

    var bSorted = false;
    var cbLast = 0;
    //排序会牌
    bSorted = false;
    cbLast = cbHuiCount - 1;
    while (bSorted == false) {
        bSorted = true;
        for (var i = 0; i < cbLast; i++) {
            if (cbHuiCard[i] > cbHuiCard[i + 1]) {
                bSorted = false;
                var temp = cbHuiCard[i + 1];
                cbHuiCard[i + 1] = cbHuiCard[i];
                cbHuiCard[i] = temp;
            }
        }
        cbLast = cbLast - 1;
    }
    //会先排在前
    cbLast = cbHuiCount;
    var num = 0;
    // var num = 1;
    for (var i = 0 ; i < cbCardCount; i++) {
        for (var j = 0 ; j < cbLast; j++) {
            if (cbHuiCard[j] == cbCardData[i]) {
                var temp = cbCardData[i];
                cbCardData[i] = cbCardData[num];
                cbCardData[num] = temp;
                num = num + 1;
            }
        }
    }
    //排序操作
    bSorted = false;
    cbLast = cbCardCount - 1;
    if (num == 0) {
        num = 1;
    }
    while (bSorted == false) {
        bSorted = true;
        for (var i = num - 1 ; i < cbLast; i++) {
            if (cbCardData[i] > cbCardData[i + 1]) {
                bSorted = false;
                var temp = cbCardData[i + 1];
                cbCardData[i + 1] = cbCardData[i];
                cbCardData[i] = temp;
            }
        }
        cbLast = cbLast - 1;
    }
};
//设置会牌
GameLogic.setHuiCard = function(cbCardData) {
    //校验
    appdf.assert(cbCardData != null && cbCardData.length > 0);
    //会牌
    GameLogic.HuiCardData = cbCardData;
};
//获取会牌
GameLogic.getHuiCard = function() {
    //会牌
    return GameLogic.HuiCardData;
};
////////////////-ysy——end//////////////////////////////////////////////////////-
//分析打哪一张牌后听哪张牌
GameLogic.AnalyseListenCard = function(cbCardData) {
    appdf.assert(cbCardData != null);
    var cbCardCount = cbCardData.length;
    appdf.assert(math_mod(cbCardCount - 2, 3) == 0);

    var cbListenList = {};
    var cbListenData = {cbOutCard: 0, cbListenCard: {}};
    var tempCard = 0;
    var bWin = false;
    for (i = 0; i < cbCardCount; i++) {
        if (tempCard != cbCardData[i]) {		//过滤重复牌
            cbListenData.cbOutCard = cbCardData[i];
            cbListenData.cbListenCard = [];
            var cbTempData = cbCardData;
            // var cbTempData = clone(cbCardData)
            for (j = 0; j < GameLogic.NORMAL_INDEX_MAX; i++) {
                var varCard = GameLogic.varCardData[j];
                var insertData = cbTempData;
                // var insertData = clone(cbTempData)
                insertData[i] = varCard;
                GameLogic.SortCardList2(insertData);
                if (GameLogic.AnalyseChiHuCard(insertData, true)) {
                    cbListenData.cbListenCard.push(varCard);
                    if (cbCardData[i] == GameLogic.varCardData[j]) {
                        bWin = true; //胡牌
                        break;
                    }
                }
            }
            if (cbListenData.cbListenCard.length > 0) {
                cbListenList.push(cbListenData);
            }

            if (bWin) {
                break;
            }
        }
        tempCard = cbCardData[i];
    }
    return [cbListenList, bWin];
};

//分析是否胡牌(带红中)
GameLogic.AnalyseChiHuCard = function(cbCardData, bNoneThePair){
	var cbCardCount = cbCardData.length;
	//红中统计
	var cbCardIndex = GameLogic.DataToCardIndex(cbCardData);
	var cbMagicCardCount = cbCardIndex[GameLogic.MAGIC_INDEX];
	//成功，全部合格
	if (cbCardCount == 0) {
        return true;
    }
	var cbRemoveData = [0, 0, 0];
	//三张相同
	if (cbCardData[0] == cbCardData[1] && cbCardData[0] == cbCardData[2]) {
        bThree = true;
        cbRemoveData[0] = cbCardData[0];
        cbRemoveData[1] = cbCardData[1];
        cbRemoveData[2] = cbCardData[2];
        var cbTempData = GameLogic.RemoveCard(cbCardData, cbRemoveData);
        // var cbTempData = GameLogic.RemoveCard(clone(cbCardData), cbRemoveData)
        if (GameLogic.AnalyseChiHuCard(cbTempData, bNoneThePair)) { 		//递归
            return true;
        }
    }
	//三张相连
	var index = GameLogic.SwitchToCardIndex(cbCardData[0]);
	if (math_mod(index, 9) + 2 <= 9 && cbCardIndex[index + 1] != 0 && cbCardIndex[index + 2] != 0) {
        cbRemoveData[0] = cbCardData[0];
        cbRemoveData[1] = GameLogic.SwitchToCardData(index + 1);
        cbRemoveData[2] = GameLogic.SwitchToCardData(index + 2);
        // var cbTempData = GameLogic.RemoveCard(clone(cbCardData), cbRemoveData);
        var cbTempData = GameLogic.RemoveCard(cbCardData, cbRemoveData);
        if (GameLogic.AnalyseChiHuCard(cbTempData, bNoneThePair)) { 		//递归
            return true;
        }
    }
	//两张相同组成一对将（不使用红中代替）
	if (cbCardData[0] == cbCardData[1] && bNoneThePair) {
        bNoneThePair = false;
        cbRemoveData[0] = cbCardData[0];
        cbRemoveData[1] = cbCardData[1];
        cbRemoveData[2] = 0;
        var cbTempData = GameLogic.RemoveCard(cbCardData, cbRemoveData);
        if (GameLogic.AnalyseChiHuCard(cbTempData, bNoneThePair)) { 		//递归
            return true;
        }
    }
	//有红中时使用红中代替
	if (cbMagicCardCount > 0) {
        //两张相同
        if (cbCardData[0] == cbCardData[1]) {
            //print("两张相同")
            cbRemoveData[0] = cbCardData[0];
            cbRemoveData[1] = cbCardData[1];
            cbRemoveData[2] = GameLogic.MAGIC_DATA;
            var cbTempData = GameLogic.RemoveCard(cbCardData, cbRemoveData);
            if (GameLogic.AnalyseChiHuCard(cbTempData, bNoneThePair)) { 		//递归
                return true;
            }
        }
        //两张相邻
        if (cbCardData[0] + 1 == cbCardData[1]) {
            //print("两张相邻")
            cbRemoveData[0] = cbCardData[0];
            cbRemoveData[1] = cbCardData[1];
            cbRemoveData[2] = GameLogic.MAGIC_DATA;
            var cbTempData = GameLogic.RemoveCard(cbCardData, cbRemoveData);
            if (GameLogic.AnalyseChiHuCard(cbTempData, bNoneThePair)) { 		//递归
                return true;
            }
        }
        //两张相隔
        if (cbCardData[0] + 2 == cbCardData[1]) {
            //print("两张相隔")
            cbRemoveData[0] = cbCardData[0];
            cbRemoveData[1] = cbCardData[1];
            cbRemoveData[2] = GameLogic.MAGIC_DATA;
            var cbTempData = GameLogic.RemoveCard(cbCardData, cbRemoveData);
            if (GameLogic.AnalyseChiHuCard(cbTempData, bNoneThePair)) { 		//递归
                return true;
            }
        }
        //一张组成一对将
        if (bNoneThePair) {
            //print("一张组成一对将")
            bNoneThePair = false;
            var cbRemoveData = [cbCardData[0],];
            cbRemoveData[0] = cbCardData[0];
            cbRemoveData[1] = GameLogic.MAGIC_DATA;
            cbRemoveData[2] = 0;
            var cbTempData = GameLogic.RemoveCard(cbCardData, cbRemoveData);
            if (GameLogic.AnalyseChiHuCard(cbTempData, bNoneThePair)) { 		//递归
                return true;
            }
        }
    }

	return false;
};

//胡牌分析(在不考虑红中的情况下)
GameLogic.AnalyseHuPai = function(cbCardData) {
    //校验
    appdf.assert(cbCardData != null);
    var cbCardCount = cbCardData;
    appdf.assert(math_mod(cbCardCount - 2, 3) == 0);
    //成功，剩一对酱
    if (cbCardCount == 2 && cbCardData[0] == cbCardData[1]) {
        //print("这个时候已经算胡了")
        return true;
    }
    //转换成牌下标
    var cbCardIndex = GameLogic.DataToCardIndex(cbCardData);
    //遍历
    for (i = 0; i < cbCardCount - 2; i++) {
        //三张相同
        if (cbCardData[i] == cbCardData[i + 1] && cbCardData[i] == cbCardData[i + 2]) {
            //print("三张相同")
            var cbRemoveData = [cbCardData[i], cbCardData[i + 1], cbCardData[i + 2]];
            var cbTempData = GameLogic.RemoveCard(cbCardData, cbRemoveData);
            if (GameLogic.AnalyseHuPai(cbTempData)) { 		//递归
                return true;
            }
        }
        //三张相连
        var index = GameLogic.SwitchToCardIndex(cbCardData[i]);
        if (math_mod(index, 9) + 2 <= 9 &&
            cbCardIndex[index + 1] != 0 &&
            cbCardIndex[index + 2] != 0) {
            //print("三张相连")
            var data1 = GameLogic.SwitchToCardData(index + 1);
            var data2 = GameLogic.SwitchToCardData(index + 2);
            var cbRemoveData = [cbCardData[i], data1, data2];
            var cbTempData = GameLogic.RemoveCard(cbCardData, cbRemoveData);
            if (GameLogic.AnalyseHuPai(cbTempData)) { 		//递归
                return true;
            }
        }
        //也不是一对将
        if (cbCardData[i] != cbCardData[i + 1] && cbCardData[i] != cbCardData[i - 1]) {
            //print("这样走不通")
            return false;
        }
    }

    return false;
};

//临近牌统计
GameLogic.NearCardGether = function(cbCardData) {
    appdf.assert(cbCardData != null);

    var nearCardData = [];
    for (i = 0; i < cbCardData.length; i++) {
        appdf.assert(cbCardData[i] != GameLogic.MAGIC_DATA);
        for (j = cbCardData[i] - 1; cbCardData[i] + 1; i++) {
            var num = math_mod(j, 16);
            if (num >= 1 && num <= 9) {
                nearCardData.push(j);
            }
        }
    }

    return GameLogic.RemoveRepetition(nearCardData);
};

//去除重复元素
GameLogic.RemoveRepetition = function(cbCardData) {
    appdf.assert(cbCardData != null);

    var bExist = [];
    for (k in cbCardData) {
        bExist[k] = true;
    }

    var result = [];
    for (k in bExist) {
        result.push(bExist[k]);
    }

    GameLogic.SortCardList2(result);

    return result;
};