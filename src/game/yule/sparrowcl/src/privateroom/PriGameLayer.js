/*
    author : kil
    date : 2017-12-6
*/
var BTN_DISMISS = 101;
var BTN_INVITE = 102;
var BTN_SHARE = 103;
var BTN_QUIT = 104;
var BTN_ZANLI = 105;

// 私人房游戏顶层
var PriGameLayer = PrivateLayerModel.extend({
    ctor : function (gameLayer) {
        this._super(gameLayer); //fixed by Kil
        var self = this;

        cc.spriteFrameCache.addSpriteFrames(res.privateRoom_plist);
        // 加载csb资源
        var tempLayer = ExternalFun.loadRootCSB(res.RoomGameLayer_json, this);
        var rootLayer = tempLayer.rootlayer;
        var csbNode = tempLayer.csbnode;
        this.m_rootLayer = rootLayer;
        this.m_csbNode = csbNode;

        var image_bg = csbNode.getChildByName("sp_roomInfoBg");
        image_bg.setVisible(false);

        // 房间ID
        this.m_atlasRoomID = image_bg.getChildByName("Text_roomNum");
        this.m_atlasRoomID.setString("房号：1008611");
        //名称(改作扎码数量了)
        //var cbMaCount = this._gameLayer.getMaCount();
        this.m_textRoomName = image_bg.getChildByName("Text_roomName");
        this.m_textRoomName.setString("0个扎码");
        // 局数
        this.m_atlasCount = image_bg.getChildByName("Text_index");
        this.m_atlasCount.setString("1 / 5 局");
        //剩余
        this.m_textRemain = image_bg.getChildByName("Text_innings");
        this.m_textRemain.setString("剩 4 局");

        var btncallback = function(ref, tType) {
            if (tType == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };
        // 解散按钮
        var btn = csbNode.getChildByName("bt_dismiss");
        btn.setTag(BTN_DISMISS);
        btn.addTouchEventListener(btncallback);
        //btn.setVisible(false);
        btn.setPosition(320, 730);

        // 暂离按钮
        btn = csbNode.getChildByName("bt_zanli");
        btn.setTag(BTN_ZANLI);
        btn.addTouchEventListener(btncallback);
        btn.setVisible(false);

        // 邀请按钮
        this.m_btnInvite = csbNode.getChildByName("bt_invite");
        this.m_btnInvite.setTag(BTN_INVITE);
        this.m_btnInvite.addTouchEventListener(btncallback);
        this.m_btnInvite.setPosition(667, 360);

    },
    
    onButtonClickedEvent : function( tag, sender ) {
        var cbMaCount = this._gameLayer.getMaCount();
        var strMa = "";
        if (cbMaCount == 1)
            strMa = "一码全中";
        else
            strMa = "扎码" + cbMaCount + "只";

        if (BTN_DISMISS == tag) {              // 请求解散游戏
            PriRoom.getInstance().queryDismissRoom();
        } else if (BTN_INVITE == tag) {
            PriRoom.getInstance().getPlazaScene().popTargetShare(function (target, bMyFriend) {
                bMyFriend = bMyFriend || false;
                var sharecall = function (isok) {
                    if (typeof(isok) == "string" && isok == "true") {
                        showToast(self, "分享成功", 2);
                    }
                    GlobalUserItem.bAutoConnect = true;
                };
                var shareTxt = "房号" + PriRoom.getInstance().m_tabPriData.szServerID +
                    "，圈数" + PriRoom.getInstance().m_tabPriData.dwDrawCountLimit +
                    "。912长岭麻将游戏精彩刺激, 一起来玩吧！";
                var friendC = "912长岭麻将房号" + PriRoom.getInstance().m_tabPriData.szServerID +
                    "，圈数" + PriRoom.getInstance().m_tabPriData.dwDrawCountLimit;
                var url = GlobalUserItem.szWXSpreaderURL || yl.HTTP_URL;
                if (bMyFriend) {
                    PriRoom.getInstance().getTagLayer(PriRoom.LAYTAG.LAYER_FRIENDLIST, function (frienddata) {
                        var serverid = Number(PriRoom.getInstance().m_tabPriData.szServerID) || 0;
                        PriRoom.getInstance().priInviteFriend(frienddata, GlobalUserItem.nCurGameKind, serverid, yl.INVALID_TABLE, friendC);
                    });
                } else if (null != target) {
                    GlobalUserItem.bAutoConnect = false;
                    MultiPlatform.getInstance().shareToTarget(target, sharecall, "912长岭麻将-" + PriRoom.getInstance().m_tabPriData.szServerID, shareTxt, url, "");
                }
            });
        } else if (BTN_SHARE == tag) {
            cc.log("分享");
            PriRoom.getInstance().getPlazaScene().popTargetShare(function (target, bMyFriend) {
                bMyFriend = bMyFriend || false;
                var sharecall = function (isok) {
                    if (typeof(isok) == "string" && isok == "true") {
                        showToast(self, "分享成功", 2);
                    }
                    GlobalUserItem.bAutoConnect = true;
                };
                var url = GlobalUserItem.szWXSpreaderURL || yl.HTTP_URL;
                // 截图分享
                var framesize = cc.director.getOpenGLView().getFrameSize();
                var area = cc.rect(0, 0, framesize.width, framesize.height);
                var imagename = "grade_share.jpg";
                if (bMyFriend) {
                    imagename = "grade_share_" + new Date().getTime() + ".jpg";
                }
                ExternalFun.popupTouchFilter(0, false);
                captureScreenWithArea(area, imagename, function (ok, savepath) {
                    ExternalFun.dismissTouchFilter();
                    if (ok) {
                        if (bMyFriend) {
                            PriRoom.getInstance().getTagLayer(PriRoom.LAYTAG.LAYER_FRIENDLIST, function (frienddata) {
                                PriRoom.getInstance().imageShareToFriend(frienddata, savepath, "分享我的约战房战绩");
                            })
                        } else if (null != target) {
                            GlobalUserItem.bAutoConnect = false;
                            MultiPlatform.getInstance().shareToTarget(target, sharecall, "我的约战房战绩", "分享我的约战房战绩", url, savepath, "true");
                        }
                    }
                });
            });
        } else if (BTN_QUIT == tag) {
            this.m_rootLayer.removeAllChildren();
            GlobalUserItem.bWaitQuit = false;
            this._gameLayer.onExitRoom();
        } else if (BTN_ZANLI == tag) {
            PriRoom.getInstance().tempLeaveGame();
            this._gameLayer.onExitRoom();
        }

    },
    // 刷新界面
    onRefreshInfo : function() {
        // 房间ID
        this.m_atlasRoomID.setString("房号: " + PriRoom.getInstance().m_tabPriData.szServerID || "000000");
        //扎码数量
        var strMa = "";
        var cbMaCount = this._gameLayer.getMaCount();
        if (cbMaCount == 1) {
            strMa = "一码全中";
        } else {
            strMa = cbMaCount + "个扎码";
        }
        this.m_textRoomName.setString(strMa);
        // 局数
        var dwPlayCount = PriRoom.getInstance().m_tabPriData.dwPlayCount;
        cc.log("局数：", dwPlayCount);
        var dwDrawCountLimit = PriRoom.getInstance().m_tabPriData.dwDrawCountLimit;
        var strcount = dwPlayCount + " / " + dwDrawCountLimit + " 局";
        this.m_atlasCount.setString(strcount);
        //剩余
        this.m_textRemain.setString("剩 " + (dwDrawCountLimit - dwPlayCount) + " 局");
        this.onRefreshInviteBtn();
        //房主
        this._gameLayer.updateRoomHost();
    },
    onRefreshInviteBtn : function() {
        cc.log("invite  +  ");
        cc.log(this._gameLayer.m_cbGameStatus);
        if (this._gameLayer.m_cbGameStatus != 0) { //不是空闲场景
            this.m_btnInvite.setVisible(false);
            return;
        }
        // 邀请按钮
        if (null != this._gameLayer.onGetSitUserNum) {
            var chairCount = PriRoom.getInstance().getChairCount();
            if (this._gameLayer.onGetSitUserNum() == chairCount) {
                this.m_btnInvite.setVisible(false);
                return;
            }
        }

        this.m_btnInvite.setVisible(true);
    },
    // 私人房游戏结束
    onPriGameEnd : function(cmd_table) {
        var child = this.m_rootLayer.getChildByName("private_end_layer");
        if(child != null){
            cc.log("The Child exist!");
            this.m_rootLayer.removeChild(child, true);
        }
        var self = this;

        //房卡结算屏蔽层
        this.layoutShield = new ccui.Layout();
        this.layoutShield.setContentSize(cc.size(1334, 750));
        this.layoutShield.setTouchEnabled(true);
        this.m_rootLayer.addChild(this.layoutShield, 1);
        //加载房卡结算
        var csbNode = ExternalFun.loadCSB(res.RoomResultLayer_json, this.m_rootLayer);
        csbNode.setVisible(false);
        csbNode.setLocalZOrder(1);
        csbNode.setName("private_end_layer");

        var btncallback = function (ref, tType) {
            if (tType == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };

        //大赢家
        var scoreList = cmd_table.lScore[0];
        var nRoomHostIndex = 1;
        var scoreListTemp = scoreList;
        scoreListTemp.sort();
        var scoreMax = scoreListTemp[scoreListTemp.length];

        //全部成绩
        // var jsonStr = cjson.encode(scoreList);
        // LogAsset.getInstance().logData(jsonStr, true);
        var tabUserRecord = this._gameLayer.getDetailScore();
        var bshowScore = this.isShowScore(scoreMax, scoreList, tabUserRecord);

        //点炮
        var dianpaoList = [];
        for (i = 0; i < 4; i++) {
            dianpaoList[i] = tabUserRecord[i].cbDianPaoCount;
        }
        var dianpaoListTemp = dianpaoList;
        dianpaoListTemp.sort();
        var dianpaoMax = dianpaoListTemp[dianpaoListTemp.length];
        //判断是否显示点炮或
        var nCountDianpao = 0;
        var bDianpao = true;
        for (i = 0; i < 4; i++) {
            if (dianpaoMax == tabUserRecord[i].cbDianPaoCount) {
                nCountDianpao = tabUserRecord[i].cbDianPaoCount + 1;
            }
            if (nCountDianpao > 1) {
                bDianpao = false;
                break;
            }
        }

        for (i = 0; i < 4; i++) {
            var userItem = this._gameLayer.getUserInfoByChairID(i);
            var nodeUser = csbNode.getChildByName("FileNode_" + (i + 1));
            appdf.assert(nodeUser, "The UI have problem!");
            if (userItem) {
                //头像         
                var tempHead = (new PopupInfoHead()).createNormal(userItem, 103);
                head = tempHead[0];
                head.setPosition(-79, 100);         //初始位置           
                head.enableHeadFrame(false);
                head.enableInfoPop(false, null, null);
                nodeUser.addChild(head);
                //头像框
                var spHead = nodeUser.getChildByName("sp_head");
                spHead.setLocalZOrder(2);
                spHead.setPosition(-79, 100);
                //昵称
                var textNickname = nodeUser.getChildByName("Text_account");
                // var strNickname = string.EllipsisByConfig(userItem.szNickName, 190,
                //     string.getConfig("fonts/round_body.ttf", 21));
                textNickname.setString(userItem.szNickName);
                //玩家ID
                var textUserId = nodeUser.getChildByName("Text_id");
                textUserId.setString("ID：" + userItem.dwGameID);
                //胡牌次数
                var textHuNum = nodeUser.getChildByName("Text_huNum");
                textHuNum.setString(tabUserRecord[i].cbHuCount);

                //点炮次数
                var textDianpaoNum = nodeUser.getChildByName("Text_diaopaoNum");
                textDianpaoNum.setString(tabUserRecord[i].cbDianPaoCount);
                //自摸次数
                var textzimoNum = nodeUser.getChildByName("Text_zimoNum");
                textzimoNum.setString(tabUserRecord[i].cbZimoCount);
                //坐庄次数
                var textzuozhuangNum = nodeUser.getChildByName("Text_zhuNum");
                textzuozhuangNum.setString(tabUserRecord[i].cbBankerCount);

                //公杠次数
                var textGongGangNum = nodeUser.getChildByName("Text_gongGangNum");
                textGongGangNum.setString(tabUserRecord[i].cbMingGang);
                //暗杠次数
                var textAnGangNum = nodeUser.getChildByName("Text_anGangNum");
                textAnGangNum.setString(tabUserRecord[i].cbAnGang);

                //GangFen
                var textGangFen = nodeUser.getChildByName("Text_43");
                textGangFen.setString("");
                //总成绩
                var textGradeTotal = nodeUser.getChildByName("Text_gradeTotal");
                var textGradeTotalNum = nodeUser.getChildByName("Text_gradeTotalNum");
                textGradeTotalNum.setString(scoreList[i]);
                //房主标志
                if (userItem.dwUserID == PriRoom.getInstance().m_tabPriData.dwTableOwnerUserID) {
                    var frame = cc.spriteFrameCache.getSpriteFrame("sp_roomHost_1.png");
                    var spRoomHost = new cc.Sprite(frame);
                    spRoomHost.setPosition(-117, 117);
                    nodeUser.addChild(spRoomHost);
                    spRoomHost.setLocalZOrder(2);
                }
                //点炮标志
                if (dianpaoList[i] == dianpaoMax && dianpaoList[i] > 0 && bDianpao) {
                    var frame = cc.spriteFrameCache.getSpriteFrame("sp_best.png");
                    var s = new cc.Sprite(frame);
                    s.setPosition(120, 135);
                    nodeUser.addChild(s);
                    s.setLocalZOrder(2);
                }
                //大赢家标志
                if (scoreList[i] == scoreMax && scoreList[i] > 0 && bshowScore) {
                    var frame = cc.spriteFrameCache.getSpriteFrame("sp_bigWinner.png");
                    s = new cc.Sprite(frame);
                    s.setPosition(4, -325);
                    nodeUser.addChild(s);
                    s.setLocalZOrder(2);
                }
                nodeUser.setVisible(true);
            } else {
                nodeUser.setVisible(false);
            }
        }

        // 分享按钮
        var btn = csbNode.getChildByName("bt_share");
        btn.setTag(BTN_SHARE);
        btn.addTouchEventListener(btncallback);

        // 退出按钮
        var btn = csbNode.getChildByName("bt_leaveRoom");
        btn.setTag(BTN_QUIT);
        btn.addTouchEventListener(btncallback);

        //历史局号
        var str = this._gameLayer.getTimesHistory();
        this.txtTimesHistory = csbNode.getChildByName("txt_TimesHistory");
        this.txtTimesHistory.setString(str);

        this.setButtonEnabled(false);
        csbNode.runAction(new cc.Sequence(new cc.DelayTime(3),
            new cc.CallFunc(function () {
                csbNode.setVisible(true)
            })));
        this.setLocalZOrder(yl.MAX_INT - 1);
    },
    isShowScore : function(scoreMax,scoreList,tabUserRecord) {
        var bshowScore = false;
        var nCountScoreMax = 0;
        var tabScore = [];
        for (i = 0; i < 4; i++) {
            if (scoreMax == scoreList[i]) {
                nCountScoreMax = nCountScoreMax + 1;
            }
        }
        if (nCountScoreMax <= 1) {
            bshowScore = true;
        }
        return bshowScore;
    },
    setButtonEnabled : function(bEnabled) {
        this.m_csbNode.getChildByTag(BTN_DISMISS).setEnabled(bEnabled);
        this.m_csbNode.getChildByTag(BTN_ZANLI).setEnabled(bEnabled);
        this.m_csbNode.getChildByTag(BTN_INVITE).setEnabled(bEnabled);
    },
    onExit : function () {
        cc.spriteFrameCache.removeSpriteFramesFromFile(res.privateRoom_plist);
    }
});