/*
    author : Kil
    date : 2017-12-6
 */
// 长岭麻将私人房创建界面
var BT_CREATE       = 1;
var BT_HELP         = 2;
var BT_PAY          = 3;

var CBX_MACOUNT_1   = 4;
var CBX_MACOUNT_2   = 5;
var CBX_MACOUNT_3   = 6;
var CBX_MACOUNT_4   = 7;
var CBX_MACOUNT_5   = 8;
var CBX_MACOUNT_6   = 9;

var CBX_INNINGS_1   = 10;
var CBX_INNINGS_2   = 11;
var CBX_INNINGS_3   = 12;
var CBX_INNINGS_4   = 13;
var CBX_INNINGS_5   = 14;
var CBX_INNINGS_6   = 15;

var CBX_USERNUM_2   = 16;
var CBX_USERNUM_3   = 17;
var CBX_USERNUM_4   = 18;

var BT_MYROOM       = 19;

//会（点炮包三家，带攒风杠鸟杠，大风刮一吃到底）
var CBX_USERHUI_1   = 20;
var CBX_USERHUI_2   = 21;
var CBX_USERHUI_3   = 22;
//番数（穷胡，七对，十三幺，天胡，地胡）
var CBX_USERFAN_1   = 23;
var CBX_USERFAN_2   = 24;
var CBX_USERFAN_3   = 25;
var CBX_USERFAN_4   = 26;
var CBX_USERFAN_5   = 27;

//游戏局数（四圈，八圈）
var CBX_GAMENUM_1   = 30;
var CBX_GAMENUM_2   = 31;
var CBX_GAMENUM_3   = 32;
//杠（杠，绝）
var CBX_USERGANG_1  = 40;
var CBX_USERGANG_2  = 41;
//杠（无旋风杠，不带攒旋风杠，带攒旋风杠）
var CBX_NONEGANG_1  = 42;
var CBX_HASGANG_2   = 43;
var CBX_NOTGANG_3   = 44;
//人数
var CBX_PLAYER_1    = 50;
var CBX_PLAYER_2    = 51;
var CBX_PLAYER_3    = 52;

//支付方式
var CBX_PAY_1       = 70;
var CBX_PAY_2       = 71;

var PriRoomCreateLayer = CreateLayerModel.extend({
    ctor : function( scene ) {
        this._super(this, scene);
        this.onInitData();

        var self = this;
        this._scene = scene;

        // 加载csb资源
        var tempLayer = ExternalFun.loadRootCSB(res.RoomCardLayer_json, this);
        var rootLayer = tempLayer.rootlayer;
        this.m_csbNode = tempLayer.csbnode;

        this.m_csbNode.setPosition(20, 0);
        var btncallback = function (ref, tType) {
            if (tType == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref);
            }
        };
        // 创建按钮
        var btn = this.m_csbNode.getChildByName("bt_createRoom");
        btn.setTag(BT_CREATE);
        btn.addTouchEventListener(btncallback);
        // 帮助按钮
        //TODO
        // btn = this.m_csbNode.getChildByName("bt_help");
        // btn.setTag(BT_HELP);
        // btn.setVisible(false);
        // btn.addTouchEventListener(btncallback);
        // 充值按钮
        // btn = this.m_csbNode.getChildByName("bt_pay");
        // btn.setTag(BT_PAY);
        // btn.addTouchEventListener(btncallback);
        //我的房间
        btn = this.m_csbNode.getChildByName("bt_myRoom");
        btn.setTag(BT_MYROOM);
        btn.addTouchEventListener(btncallback);

        //自己房卡数目
        // this.textCardNum = this.m_csbNode.getChildByName("Text_myRoomCardNum");
        // this.textCardNum.setString(GlobalUserItem.lRoomCard + "");
        //复选按钮
        var cbtlistener2 = function (sender, eventType) {
            self.onSelectedEvent2(sender.getTag(), sender);
        };
        //房间局数 单选
        this.cbGameNum = 4;

        var cbx = this.m_csbNode.getChildByName("cbx_innings_2");
        cbx.setTag(CBX_GAMENUM_2);
        cbx.setSelected(true);
        cbx.addEventListener(cbtlistener2);

        cbx = this.m_csbNode.getChildByName("cbx_innings_3");
        cbx.setTag(CBX_GAMENUM_3);
        cbx.addEventListener(cbtlistener2);
        //玩法 复选 （点炮包三家，带攒风杠鸟杠，大风刮一吃到底）
        this.cbHuiNum[0] = 1;
        this.cbHuiNum[1] = 1;
        this.cbHuiNum[2] = 1;
        cbx = this.m_csbNode.getChildByName("cbx_userHui_1");
        cbx.setTag(CBX_USERHUI_1);
        cbx.setSelected(true);
        cbx.addEventListener(cbtlistener2);

        cbx = this.m_csbNode.getChildByName("cbx_userHui_2");
        cbx.setTag(CBX_USERHUI_2);
        cbx.setSelected(true);
        cbx.addEventListener(cbtlistener2);

        cbx = this.m_csbNode.getChildByName("cbx_userHui_3");
        cbx.setTag(CBX_USERHUI_3);
        cbx.setSelected(true);
        cbx.addEventListener(cbtlistener2);
        //人数 单选
        this.cbPlayerNum = 4;
        cbx = this.m_csbNode.getChildByName("cbx_playerNum_1");
        cbx.setTag(CBX_PLAYER_1);
        cbx.addEventListener(cbtlistener2);

        // cbx = this.m_csbNode.getChildByName("cbx_playerNum_2");
        // cbx.setTag(CBX_PLAYER_2);
        // cbx.addEventListener(cbtlistener2);
        //
        // cbx = this.m_csbNode.getChildByName("cbx_playerNum_3");
        // cbx.setTag(CBX_PLAYER_3);
        // cbx.setSelected(true);
        // cbx.addEventListener(cbtlistener2);

        //支付方式
        this.m_payment = 0;
        cbx = this.m_csbNode.getChildByName("cbx_pay_host");
        cbx.setTag(CBX_PAY_2);
        cbx.setSelected(true);
        cbx.addEventListener(cbtlistener2);

        cbx = this.m_csbNode.getChildByName("cbx_pay_AA");
        cbx.setTag(CBX_PAY_1);
        cbx.addEventListener(cbtlistener2);
        //创建房卡花费花费
        this.m_tabSelectConfig = PriRoom.getInstance().m_tabFeeConfigList[0];
        this.m_bLow = false;

        // this.textCreateCost = this.m_csbNode.getChildByName("Text_costNum");
        // this.textCreateCost.setString("");
        // this.textCreateCost.setVisible(false);
        var feeType = "房卡";
        var strLockPrompt = "sp_lackRoomCard.png";
        var lMyTreasure = GlobalUserItem.lRoomCard;
        if (null != this.m_tabSelectConfig) {
            var dwCost = this.m_tabSelectConfig.lFeeScore;
            if (PriRoom.getInstance().m_tabRoomOption.cbCardOrBean == 0) {
                feeType = "游戏豆";
                strLockPrompt = "sp_lackBean.png";
                lMyTreasure = GlobalUserItem.dUserBeans;
            }
            if (lMyTreasure < dwCost || lMyTreasure == 0) {
                this.m_bLow = true;
            }
            // this.textCreateCost.setString(dwCost + feeType);
            // this.textCreateCost.setVisible(false);
        }
        /*
        //房卡或游戏豆不足提示
        this.sp_roomCardLack = this.m_csbNode.getChildByName("sp_roomCardLack");
        this.sp_roomCardLack.setVisible(this.m_bLow);
        */
    },
    onInitData : function() {
        this.cbMaCount = 0;
        this.cbInningsCount = 0;
        this.cbUserNum = 0;

        this.cbGameNum = 0;
        this.cbGangOrYue = 0;
        this.cbGangNum = 0;
        this.cbPlayerNum = 0;
        this.cbHuiNum = [0, 0, 0];
        this.cbFanNum = [0, 0, 0, 0, 0];
    },
    onResetData : function() {
        this.cbMaCount = 0;
        this.cbInningsCount = 0;
        this.cbUserNum = 0;
    },
    // 刷新界面
    onRefreshInfo : function() {
        // 房卡数更新
        //this.textCardNum.setString(GlobalUserItem.lRoomCard + "");
    },
    onLoginPriRoomFinish : function() {
        var meUser = PriRoom.getInstance().getMeUserItem();
        if (null == meUser) {
            return false;
        }
        // 发送创建桌子
        if (meUser.cbUserStatus == yl.US_FREE || meUser.cbUserStatus == yl.US_NULL || meUser.cbUserStatus == yl.US_PLAYING) {
            if (PriRoom.getInstance().m_nLoginAction == PriRoom.L_ACTION.ACT_CREATEROOM) {
                // 创建登陆
                var buffer = new Cmd_Data(192);
                buffer.setcmdinfo(this._cmd_pri_game.MDM_GR_PERSONAL_TABLE, this._cmd_pri_game.SUB_GR_CREATE_TABLE);
                buffer.pushdword(0);
                buffer.pushscore(this.m_payment); //AA制度可选
                buffer.pushdword(this.m_tabSelectConfig.dwDrawCountLimit);
                buffer.pushdword(this.m_tabSelectConfig.dwDrawTimeLimit);
                buffer.pushword(0);
                buffer.pushdword(0);
                buffer.pushstring("", yl.LEN_PASSWORD);
                //单个游戏规则(额外规则)
                buffer.pushbyte(1);
                buffer.pushbyte(this.cbGameNum);               // 圈数
                //会
                for (i = 0; i < 3; i++) {
                    buffer.pushbyte(this.cbHuiNum[i]);          // 会
                }
                buffer.pushbyte(this.cbPlayerNum);        //人数
                for (i = 0; i < 95; i++) {
                    buffer.pushbyte(0);
                }
                PriRoom.getInstance().getNetFrame().sendGameServerMsg(buffer);
                return true;
            }
        }
        return false
    },
    getInviteShareMsg : function( roomDetailInfo ) {
        var shareTxt = "912长岭麻将约战 房间ID:" + roomDetailInfo.szRoomID + " 局数:" + roomDetailInfo.dwPlayTurnCount;
        var friendC = "912长岭麻将房间ID:" + roomDetailInfo.szRoomID + " 局数:" + roomDetailInfo.dwPlayTurnCount;
        return {title : "912长岭麻将约战", content : shareTxt + " 912长岭麻将游戏精彩刺激, 一起来玩吧! ", friendContent : friendC};
    },
    // 继承/覆盖
    onButtonClickedEvent : function(tag, sender) {
        var self = this;
        if (BT_HELP == tag) {
            cc.log("帮助");
            this._scene.popHelpLayer2(389, 1);
        } else if (BT_CREATE == tag) {
            cc.log("创建房间_tom");
            cc.log("this.m_bLow = " + this.m_bLow);
            if (this.m_bLow) {
                var feeType = "房卡";
                if (PriRoom.getInstance().m_tabRoomOption.cbCardOrBean == 0) {
                    feeType = "游戏豆";
                }
                var query = new QueryDialog("您的"  +  feeType  +  "数量不足，是否前往商城充值！", function(ok) {
                    if (ok == true) {
                        if (feeType == "游戏豆")
                            self._scene.onChangeShowMode(yl.SCENE_SHOP, ShopLayer.CBT_BEAN);
                        else
                            self._scene.onChangeShowMode(yl.SCENE_SHOP, ShopLayer.CBT_PROPERTY);
                    }
                    query = null;
                });
                query.setCanTouchOutside(false);
                this._scene.addChild(query);
                return;
            }
            if (null == this.m_tabSelectConfig || Object.keys(this.m_tabSelectConfig).length == 0) {
                showToast(this, "未选择玩法配置!", 2);
                return;
            }
            PriRoom.getInstance().showPopWait();
            PriRoom.getInstance().getNetFrame().onCreateRoom();
        } else if (BT_PAY == tag) {
            cc.log("充值");
            var feeType = "房卡";
            if (PriRoom.getInstance().m_tabRoomOption.cbCardOrBean == 0)
                feeType = "游戏豆";
            if (feeType == "游戏豆")
                this._scene.onChangeShowMode(yl.SCENE_SHOP, ShopLayer.CBT_BEAN);
            else
                this._scene.onChangeShowMode(yl.SCENE_SHOP, ShopLayer.CBT_PROPERTY);
        } else if (BT_MYROOM == tag) {
            cc.log("我的房间");
            this._scene.onChangeShowMode(PriRoom.LAYTAG.LAYER_MYROOMRECORD);
        }
    },
    onSelectedEvent : function(tag, sender) {
        cc.log("进", tag);
        if (CBX_MACOUNT_1 <= tag && tag <= CBX_MACOUNT_6) {
            for (i = CBX_MACOUNT_1; i <= CBX_MACOUNT_6; i++) {
                var checkBox = this.m_csbNode.getChildByTag(i);
                if (i == tag)
                    this.cbMaCount = checkBox.isSelected() && i - CBX_MACOUNT_1 + 1 || 0;
                else
                    checkBox.setSelected(false);
            }
        } else if (CBX_INNINGS_1 <= tag && tag <= CBX_INNINGS_6) {
            for (i = CBX_INNINGS_1; i <= CBX_INNINGS_6; i++) {
                var checkBox = this.m_csbNode.getChildByTag(i);
                if (i == tag) {
                    var feeType = "房卡";
                    var lMyTreasure = GlobalUserItem.lRoomCard;
                    if (PriRoom.getInstance().m_tabRoomOption.cbCardOrBean == 0) {
                        feeType = "游戏豆";
                        lMyTreasure = GlobalUserItem.dUserBeans;
                    }
                    if (checkBox.isSelected()) {
                        var index = i - CBX_INNINGS_1 + 1;
                        this.cbInningsCount = index;
                        this.m_tabSelectConfig = PriRoom.getInstance().m_tabFeeConfigList[index];
                        this.textCreateCost.setString(this.m_tabSelectConfig.lFeeScore + feeType);
                        this.textCreateCost.setVisible(false);
                        if (lMyTreasure < this.m_tabSelectConfig.lFeeScore || lMyTreasure == 0) { //房卡或游戏豆不足
                            this.sp_roomCardLack.setVisible(true);
                            this.m_bLow = true;
                        } else {
                            this.sp_roomCardLack.setVisible(false);
                            this.m_bLow = false;
                        }
                    } else {
                        this.cbInningsCount = 0;
                        this.m_tabSelectConfig = null;
                        this.textCreateCost.setString("0" + feeType);
                        this.textCreateCost.setVisible(false);
                        this.sp_roomCardLack.setVisible(lMyTreasure == 0);
                    }
                } else {
                    checkBox.setSelected(false);
                }
            }
        } else if (CBX_USERNUM_2 <= tag && tag <= CBX_USERNUM_4) {
            for (i = CBX_USERNUM_2; i <= CBX_USERNUM_4; i++) {
                var checkBox = this.m_csbNode.getChildByTag(i);
                if (i == tag) {
                    this.cbUserNum = checkBox.isSelected() && i - CBX_USERNUM_2 + 2 || 0;
                } else {
                    checkBox.setSelected(false);
                }
            }
        } else {
            appdf.assert(false);
        }
        cc.log(this.cbMaCount, this.cbInningsCount, this.cbUserNum);
    },
    onSelectedEvent2 : function(tag, sender) {
        cc.log("进" + tag);

        if (CBX_USERHUI_1 <= tag && tag <= CBX_USERHUI_3) {           // 会20-22 复选
            for (i = CBX_USERHUI_1; i <= CBX_USERHUI_3; i++) {
                var checkBox = this.m_csbNode.getChildByTag(i);
                if (i == tag)
                    this.cbHuiNum[i - CBX_USERHUI_1 + 1] = checkBox.isSelected() && 1 || 0;
            }
        } else if (CBX_USERFAN_1 <= tag && tag <= CBX_USERFAN_5) {       // 番数23-27 复选
            for (i = CBX_USERFAN_1; i <= CBX_USERFAN_5; i++) {
                var checkBox = this.m_csbNode.getChildByTag(i);
                if (i == tag)
                    this.cbFanNum[i - CBX_USERFAN_1 + 1] = checkBox.isSelected() && 1 || 0;
            }
        } else if (CBX_GAMENUM_2 <= tag && tag <= CBX_GAMENUM_3) {       // 游戏局数 30-32  单选
            for (i = CBX_GAMENUM_2; i <= CBX_GAMENUM_3; i++) {
                var checkBox = this.m_csbNode.getChildByTag(i);
                if (i == tag) {
                    this.cbGameNum = checkBox.isSelected() && i - CBX_GAMENUM_1 + 1 || 0;
                    this.cbGameNum = i - CBX_GAMENUM_2 + 1;
                    if (this.cbGameNum == 1)
                        this.cbGameNum = 4;
                    else if (this.cbGameNum == 2)
                        this.cbGameNum = 8;
                    else if (this.cbGameNum == 3)
                        this.cbGameNum = 8;
                    checkBox.setSelected(true);
                } else {
                    checkBox.setSelected(false);
                }
            }
        } else if (CBX_USERGANG_1 <= tag && tag <= CBX_USERGANG_2) {  // 杠40-41  单选
            for (i = CBX_USERGANG_1; i <= CBX_USERGANG_2; i++) {
                var checkBox = this.m_csbNode.getChildByTag(i);
                if (i == tag) {
                    this.cbGangOrYue = checkBox.isSelected() && i - CBX_USERGANG_1 + 1 || 0;
                    this.cbGangOrYue = i - CBX_USERGANG_1 + 1;
                    if (this.cbGangOrYue == 2)
                        this.cbGangOrYue = 0;
                    checkBox.setSelected(true);
                } else {
                    checkBox.setSelected(false);
                }
            }
        } else if (CBX_NONEGANG_1 <= tag && tag <= CBX_NOTGANG_3) {      // 带杠  42-44 单选
            for (i = CBX_NONEGANG_1; i <= CBX_NOTGANG_3; i++) {
                var checkBox = this.m_csbNode.getChildByTag(i);
                if (i == tag) {
                    this.cbGangNum = checkBox.isSelected() && i - CBX_NONEGANG_1 || 0;
                    this.cbGangNum = (i - CBX_NONEGANG_1) * 10;
                    checkBox.setSelected(true);
                } else {
                    checkBox.setSelected(false);
                }
            }
        } else if (CBX_PLAYER_1 <= tag && tag <= CBX_PLAYER_3) {   //人数 50-52 单选
            for (i = CBX_PLAYER_1; i <= CBX_PLAYER_3; i++) {
                var checkBox = this.m_csbNode.getChildByTag(i);
                if (i == tag) {
                    this.cbPlayerNum = checkBox.isSelected() && i - CBX_PLAYER_1 || 0;
                    this.cbPlayerNum = tag - CBX_PLAYER_1 + 2;
                    checkBox.setSelected(true);
                } else {
                    checkBox.setSelected(false);
                }
            }
        } else if (CBX_PAY_1 <= tag && tag <= CBX_PAY_2) {     // 支付方式70-71  单选
            for (i = CBX_PAY_1; i <= CBX_PAY_2; i++) {
                var checkBox = this.m_csbNode.getChildByTag(i);
                if (i == tag) {
                    this.m_payment = checkBox.isSelected() && i - CBX_PAY_1 + 1 || 0;
                    this.m_payment = i - CBX_PAY_1 + 1;
                    if (this.m_payment == 2) {
                        this.m_payment = 0;
                    }
                    checkBox.setSelected(true);
                } else {
                    checkBox.setSelected(false);
                }
            }
        } else {
            appdf.assert(false);
        }

        cc.log(" cbHuiNum1 = " + this.cbHuiNum[1] + " , cbHuiNum2 = " + this.cbHuiNum[2] + " , cbHuiNum3 = " + this.cbHuiNum[3] + " , cbGameNum = " + this.cbGameNum + " , cbPlayerNum = " + this.cbPlayerNum);
    }
});