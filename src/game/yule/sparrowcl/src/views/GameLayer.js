/*
	author . Kil
	date . 2017-12-6
 */
var GameLayer = GameModel.extend({
	ctor : function (frameEngine, scene) {
		this._super(frameEngine, scene);
    },

    CreateView : function() {
        // var self = this;
        // var layer = new GameViewLayer(self);
        // var res = this.addChild(layer);
        // return this;
        var layer = new GameViewLayer(this);
        this.addChild(layer);
        return layer;
    },

	OnInitGameEngine : function() {
        this.lCellScore = 0;
        this.cbTimeOutCard = 0;
        this.cbTimeOperateCard = 0;
        this.cbTimeStartGame = 0;
        this.wCurrentUser = yl.INVALID_CHAIR;
        this.wBankerUser = yl.INVALID_CHAIR;
        this.cbPlayStatus = [0, 0, 0, 0];
        this.cbGender = [0, 0, 0, 0];
        this.bTrustee = false;
        this.nGameSceneLimit = 0;
        this.cbAppearCardData = []; 		//已出现的牌
        this.bMoPaiStatus = false;
        this.cbListenPromptOutCard = [];
        this.cbListenCardList = [];
        this.cbActionMask = null;
        this.cbActionCard = null;
        this.bSendCardFinsh = false;
        this.cbPlayerCount = 4;
        this.lDetailScore = [];
        this.m_userRecord = [];
        this.cbMaCount = 0;
        //房卡需要
        this.wRoomHostViewId = 0;
        cc.log("Hello Hello!");
        //局号
        this.wTimesNow = 0;
        this.m_cbTimesHistory = [];
        this.m_cbQiangTouCard = 0;
        this.m_cbGangZhuiFeng = [];
        this.m_cbUserScore = [0, 0, 0, 0];
        for (i = 0; i < 4; i++) {
            this.m_cbGangZhuiFeng[i] = [0, 0, 0, 0, 0, 0, 0, 0];
        }
        this.cbGameTimes = 1;
    },
    OnResetGameEngine : function() {
        // GameLayer.super.OnResetGameEngine(this)
        this._gameView.onResetData();
        this.nGameSceneLimit = 0;
        this.bTrustee = false;
        this.cbAppearCardData = [];		//已出现的牌
        this.bMoPaiStatus = false;
        this.cbActionMask = null;
        this.cbActionCard = null;
        for (i = 0; i < 4; i++) {
            this.m_cbGangZhuiFeng[i] = [0, 0, 0, 0, 0, 0, 0, 0];
        }
        this.cbGameTimes = 1;
    },
	//获取gamekind
	getGameKind : function() {
        return ccmd.KIND_ID;
    },

	// 房卡信息层zorder
	priGameLayerZorder : function() {
        return 3;
    },

	onExitRoom : function() {
        this._gameFrame.onCloseSocket();
        this.stopAllActions();
        this.KillGameClock();
        this.dismissPopWait();
        this._scene.onKeyBack();
    },

    // 椅子号转视图位置,注意椅子号从0~nChairCount-1,返回的视图位置从1~nChairCount
    SwitchViewChairID : function(chair) {
        var viewid = yl.INVALID_CHAIR;
        var nChairCount = this._gameFrame.GetChairCount();
        var nChairID = this.GetMeChairID();

        if (chair != yl.INVALID_CHAIR && chair < ccmd.GAME_PLAYER) {

            if (nChairCount == 2) {
                if (chair == 2) { // 无用ID
                    viewid = 2;
                } else if (chair == 3) { //无用ID
                    viewid = 4;
                } else if (chair == nChairID) {
                    viewid = 3;
                } else {
                    viewid = 1;
                }
            } else if (nChairCount == 3) {
                if (chair == 3) { // 无用ID
                    viewid = 1;
                } else {
                    viewid = math_mod(chair + (3 - 2 - nChairID), 3) + 2;
                }
            } else if (nChairCount == 4) {
                viewid = math_mod(chair + Math.floor(nChairCount * 3 / 2) - nChairID, nChairCount) + 1;
            }
        }

        return viewid - 1;
    },


    updateRoomHost : function() {
        //appdf.assert(GlobalUserItem.bPrivateRoom); TODO: Use the cc.log instead of this
        if (this.wRoomHostViewId >= 0 && this.wRoomHostViewId < 4) {
            this._gameView.setRoomHost(this.wRoomHostViewId);
        }
    },

    getUserInfoByChairID : function(chairId) {
        var viewId = this.SwitchViewChairID(chairId);
        //    return this._gameFrame:getTableUserItem(this._gameFrame:GetTableID(), chairId)
        var usetItem = this._gameFrame.getTableUserItem(this._gameFrame.GetTableID(), chairId);
        if (usetItem == null) {
            usetItem = this._gameView.m_tabUserItem[viewId];
        }
        return usetItem;
    },

    getMaCount: function() {
        return this.cbMaCount;
    },

    getTimesHistory : function() {
        var str = "历史局号：";
        for (var i = 0; i < this.m_cbTimesHistory.length; i++) {
            if (math_mod(i, 5) == 0)
                str = str + "\n" + this.m_cbTimesHistory[i];
            else
                str = str + this.m_cbTimesHistory[i] + ",";
        }
        return str;
    },

	onGetSitUserNum : function() {
        var num = 0;
        for (i = 0; i < ccmd.GAME_PLAYER; i++) {
            if (null != this._gameView.m_sparrowUserItem[i]) {
                num = num + 1;
            }
        }

        return num;
    },
	
	// 计时器响应
	OnEventGameClockInfo : function(chair,time,clockId) {
        if (GlobalUserItem.bPrivateRoom)
            return;

        var meChairId = this.GetMeChairID();
        if (clockId == ccmd.IDI_START_GAME) {
            //托管
            if (this.bTrustee && this._gameView.btStart.isVisible()) {

            }
            //超时
            if (time <= 0) {
                this._gameFrame.setEnterAntiCheatRoom(false); //退出防作弊，如果有的话
            } else if (time <= 5) {
                this.PlaySound(ccmd.RES_PATH + "sound/GAME_WARN.wav");
            }
        } else if (clockId == ccmd.IDI_OUT_CARD) {
            if (chair == meChairId) {
                //托管
                if (this.bTrustee) {

                }
                //超时
                if (time <= 0) {
                    this._gameView._cardLayer.outCardAuto();
                } else if (time <= 5) {
                    this.PlaySound(ccmd.RES_PATH + "sound/GAME_WARN.wav");
                }
            }
        } else if (clockId == ccmd.IDI_OPERATE_CARD) {
            if (chair == meChairId) {
                //托管
                if (this.bTrustee) {

                }
                //超时
                if (time <= 0) {
                    if (this._gameView._cardLayer.isUserMustWin())
                        this._gameView.onButtonClickedEvent(GameViewLayer.BT_WIN);

                    this._gameView.onButtonClickedEvent(GameViewLayer.BT_PASS);
                    this._gameView._cardLayer.outCardAuto();
                } else if (time <= 5) {
                    this.PlaySound(ccmd.RES_PATH + "sound/GAME_WARN.wav");
                }
            }
        }
    },
	//用户聊天
	onUserChat : function(chat, wChairId) {
        this._gameView.userChat(this.SwitchViewChairID(wChairId), chat.szChatString);
    },

	//用户表情
	onUserExpression : function(expression, wChairId) {
        this._gameView.userExpression(this.SwitchViewChairID(wChairId), expression.wItemIndex);
    },

	// 语音播放开始
	onUserVoiceStart : function( useritem, filepath ) {
        this._gameView.onUserVoiceStart(this.SwitchViewChairID(useritem.wChairID));
    },

	// 语音播放结束
	onUserVoiceEnded : function( useritem, filepath ) {
        this._gameView.onUserVoiceEnded(this.SwitchViewChairID(useritem.wChairID));
    },
	// 场景消息
	onEventGameScene : function(cbGameStatus, dataBuffer) {
        //获取规则
        this.findGameRule();
        this.m_cbGameStatus = cbGameStatus;
        this.nGameSceneLimit = this.nGameSceneLimit + 1;
        if (this.nGameSceneLimit > 1) {
            //限制只进入场景消息一次
            return true;
        }
        if (PriRoom.getInstance().m_tabPriData.szServerID != null) {
            var txt = this._gameView.textRoomID;
            txt.setString("房间号：" + PriRoom.getInstance().m_tabPriData.szServerID);
            this._gameView.textRoomID.setVisible(true);
        }
        var wTableId = this.GetMeTableID();
        var wMyChairId = this.GetMeChairID();
        this._gameView.setRoomInfo(wTableId, wMyChairId);
        //初始化用户信息
        for (i = 0; i < ccmd.GAME_PLAYER; i++) {
            var wViewChairId = this.SwitchViewChairID(i);
            var userItem = this._gameFrame.getTableUserItem(wTableId, i);
            this._gameView.OnUpdateUser(wViewChairId, userItem);

            //更新分数
            if (userItem) {
                this.cbGender[wViewChairId] = userItem.cbGender;
                if (PriRoom && GlobalUserItem.bPrivateRoom) {
                    if (userItem.dwUserID == PriRoom.getInstance().m_tabPriData.dwTableOwnerUserID) {
                        this.wRoomHostViewId = wViewChairId;
                    }
                }
            }
        }

        if (cbGameStatus == ccmd.GAME_SCENE_FREE) {
            cc.log("空闲状态", wMyChairId);
            var cmd_data = ExternalFun.read_netdata(ccmd.CMD_S_StatusFree, dataBuffer);
            this.lCellScore = cmd_data.lCellScore;
            this.cbTimeOutCard = cmd_data.cbTimeOutCard;
            this.cbTimeOperateCard = cmd_data.cbTimeOperateCard;
            this.cbTimeStartGame = cmd_data.cbTimeStartGame;
            this.cbPlayerCount = cmd_data.cbPlayerCount || 4;
            this.cbMaCount = cmd_data.cbMaCount;
            cc.log("设置码数", this.cbMaCount);

            this._gameView.btStart.setVisible(true);
            for (var i = 0; i < ccmd.GAME_PLAYER; i++) {
                //更新分数
                var wViewChairId = this.SwitchViewChairID(i);
                this.m_cbUserScore[wViewChairId] = cmd_data.lDeltaScore[0][i];
                this._gameView.upDataScore(wViewChairId, this.m_cbUserScore[wViewChairId]);
            }

            this.SetGameClock(wMyChairId, ccmd.IDI_START_GAME, this.cbTimeStartGame);
            //游戏圈数
            this._gameView.upDataGameTimes(cmd_data.dwRoundCount, cmd_data.dwRoundLimit);

        } else if (cbGameStatus == ccmd.GAME_SCENE_PLAY) {
            cc.log("游戏状态", wMyChairId);
            var cmd_data = ExternalFun.read_netdata(ccmd.CMD_S_StatusPlay, dataBuffer);

            this.lCellScore = cmd_data.lCellScore;
            this.cbTimeOutCard = cmd_data.cbTimeOutCard;
            this.cbTimeOperateCard = cmd_data.cbTimeOperateCard;
            this.cbTimeStartGame = cmd_data.cbTimeStartGame;
            this.wCurrentUser = cmd_data.wCurrentUser;
            this.wBankerUser = cmd_data.wBankerUser;
            this.cbPlayerCount = cmd_data.cbPlayerCount || 4;
            this.cbMaCount = cmd_data.cbMaCount;

            //游戏圈数
            this._gameView.upDataGameTimes(cmd_data.dwRoundCount, cmd_data.dwRoundLimit);

            this.AnalysisZhuiFengNum(cmd_data.cbChaseArrowArray);
            for (i = 0; i < ccmd.GAME_PLAYER; i++) {
                //更新分数
                var wViewChairId = this.SwitchViewChairID(i);
                this.m_cbUserScore[wViewChairId] = cmd_data.lDeltaScore[0][i];
                this._gameView.upDataScore(wViewChairId, this.m_cbUserScore[wViewChairId]);
            }

            //墙头牌
            this.m_cbQiangTouCard = cmd_data.cbMagicIndex;
            var cmd_data2 = new Cmd_Data(136);
            for (i = 0; i < 12; i++) {
                cmd_data2.pushbyte(0);
            }
            this.SendData(ccmd.SUB_C_ASK_RULE, cmd_data2);

            //局号
            this._gameView.showTimes(cmd_data.ctRoundNo);
            this.wTimesNow = cmd_data.ctRoundNo;
            var num = this.m_cbTimesHistory.length;
            this.m_cbTimesHistory[num] = cmd_data.ctRoundNo;

            //庄家
            this._gameView.setBanker(this.SwitchViewChairID(this.wBankerUser));
            //设置手牌
            var viewCardCount = [];
            for (i = 0; i < ccmd.GAME_PLAYER; i++) {
                var viewId = this.SwitchViewChairID(i);
                viewCardCount[viewId] = cmd_data.cbCardCount[0][i];
                if (viewCardCount[viewId] > 0) {
                    this.cbPlayStatus[viewId] = 1;
                }
            }
            var cbHandCardData = [];
            for (i = 0; i < ccmd.MAX_COUNT; i++) {
                var data = cmd_data.cbCardData[0][i];
                if (data > 0) { 				//去掉末尾的0
                    cbHandCardData[i] = data;
                } else {
                    break;
                }
            }
            GameLogic.SortCardList2(cbHandCardData); 		//排序
            var cbSendCard = cmd_data.cbSendCardData;
            if (cbSendCard > 0 && this.wCurrentUser == wMyChairId) {
                for (i = 0; cbHandCardData.length; i++) {
                    if (cbHandCardData[i] == cbSendCard) {
                        cbHandCardData.splice(i,1);				//把刚抓的牌放在最后
                        break;
                    }
                }
                cbHandCardData.push(cbSendCard);
            }
            for (i = 0; i < ccmd.GAME_PLAYER; i++) {
                this._gameView._cardLayer.setHandCard(i, viewCardCount[i], cbHandCardData);
            }
            this.bSendCardFinsh = true;
            //记录已出现牌
            this.insertAppearCard(cbHandCardData);
            //组合牌
            for (var i = 0; i < ccmd.GAME_PLAYER; i++) {
                var wViewChairId = this.SwitchViewChairID(i);
                for (j = 0; j < cmd_data.cbWeaveItemCount[0][i]; j++) {
                    var cbOperateData = [];
                    for (v = 0; v < 3; v++) {
                        var data = cmd_data.WeaveItemArray[i][j].cbCardData[0][v];
                        if (data > 0) {
                            cbOperateData.push(data);
                        }
                    }
                    var nShowStatus = GameLogic.SHOW_NULL;
                    var cbParam = cmd_data.WeaveItemArray[i][j].cbParam;
                    if (cbParam == GameLogic.WIK_GANERAL) {
                        if (cbOperateData[0] == cbOperateData[1]) 	//碰
                            nShowStatus = GameLogic.SHOW_PENG;
                        else 											//吃
                            nShowStatus = GameLogic.SHOW_CHI;
                    } else if (cbParam == GameLogic.WIK_MING_GANG) {
                        nShowStatus = GameLogic.SHOW_MING_GANG;
                    } else if (cbParam == GameLogic.WIK_FANG_GANG) {
                        nShowStatus = GameLogic.SHOW_FANG_GANG;
                    } else if (cbParam == GameLogic.WIK_AN_GANG) {
                        nShowStatus = GameLogic.SHOW_AN_GANG;
                    } else if (cbParam == GameLogic.WIK_CHASEWIND || cbParam == GameLogic.WIK_CHASEARROW || cbParam == GameLogic.WIK_CHASEBIRD) {
                        nShowStatus = GameLogic.SHOW_ZHUI_GANG;
                    } else if (cbParam == GameLogic.WIK_BIRD) {
                        nShowStatus = GameLogic.SHOW_XUAN_GANG;
                    }

                    if (cmd_data.WeaveItemArray[i][j].cbWeaveKind == GameLogic.WIK_WALLTOP_S) {
                        nShowStatus = GameLogic.SHOW_AN_GANG;
                    } else if (cmd_data.WeaveItemArray[i][j].cbWeaveKind == GameLogic.WIK_WIND) {
                        if (ccmd.MY_VIEWID == wViewChairId) {
                            nShowStatus = GameLogic.SHOW_FANG_GANG;
                        } else {
                            nShowStatus = GameLogic.SHOW_FANG_GANG;
                        }
                    }
                    var bshowGang = this.AnalysisGang(nShowStatus, ccmd.MY_VIEWID, wViewChairId);
                    this._gameView._cardLayer.bumpOrBridgeCard(wViewChairId, cbOperateData, nShowStatus, bshowGang, cmd_data.WeaveItemArray[i][j].cbWeaveKind);
                    if (wViewChairId == ccmd.MY_VIEWID && nShowStatus == GameLogic.SHOW_PENG) {
                        this._gameView._cardLayer.setJueCard(cbOperateData[0]);
                    }
                    //记录已出现牌
                    this.insertAppearCard(cbOperateData);
                }
            }
            //设置牌堆
            var wViewHeapHead = this.SwitchViewChairID(cmd_data.wHeapHead);
            var wViewHeapTail = this.SwitchViewChairID(cmd_data.wHeapTail);
            for (i = 0; i < ccmd.GAME_PLAYER; i++) {
                var viewId = this.SwitchViewChairID(i);
                for (j = 1; j < cmd_data.cbDiscardCount[0][i]; j++) {
                    //已出的牌
                    this._gameView._cardLayer.discard(viewId, cmd_data.cbDiscardCard[i][j]);
                    //记录已出现牌
                    var cbAppearCard = [cmd_data.cbDiscardCard[i][j]];
                    this.insertAppearCard(cbAppearCard);
                }
                //牌堆
                this._gameView._cardLayer.setTableCardByHeapInfo(viewId, cmd_data.cbHeapCardInfo[i], wViewHeapHead, wViewHeapTail);
                //托管
                this._gameView.setUserTrustee(viewId, cmd_data.bTrustee[0][i]);
                if (viewId == ccmd.MY_VIEWID) {
                    this.bTrustee = cmd_data.bTrustee[0][i];
                }
            }
            //刚出的牌
            if (cmd_data.cbOutCardData && cmd_data.cbOutCardData > 0) {
                var wOutUserViewId = this.SwitchViewChairID(cmd_data.wOutCardUser);
                this._gameView.showCardPlate(wOutUserViewId, cmd_data.cbOutCardData);
            }
            //计时器
            this.SetGameClock(this.wCurrentUser, ccmd.IDI_OUT_CARD, this.cbTimeOutCard);

            //提示听牌数据
            this.cbListenPromptOutCard = [];
            this.cbListenCardList = [];
            for (i = 0; i < cmd_data.cbOutCardCount; i++) {
                this.cbListenPromptOutCard[i] = cmd_data.cbOutCardDataEx[0][i];
                this.cbListenCardList[i] = [];
                for (j = 0; j < cmd_data.cbHuCardCount[0][i]; j++) {
                    this.cbListenCardList[i][j] = cmd_data.cbHuCardData[i][j];
                }
            }
            var cbPromptHuCard = this.getListenPromptHuCard(cmd_data.cbOutCardData);
            this._gameView.setListeningCard(cbPromptHuCard);
            //提示操作
            this._gameView.recognizecbActionMask(cmd_data.cbActionMask, cmd_data.cbActionCard);
            if (this.wCurrentUser == wMyChairId) {
                this._gameView._cardLayer.promptListenOutCard(this.cbListenPromptOutCard);
            }
        } else {
            cc.log("\ndefault\n");
            return false;
        }

        // 刷新房卡
        if (PriRoom && GlobalUserItem.bPrivateRoom) {
            if (null != this._gameView._priView && null != this._gameView._priView.onRefreshInfo) {
                this._gameView._priView.onRefreshInfo();
            }
        }

        this.dismissPopWait();

        return true;
    },
	// 游戏消息
	onEventGameMessage : function(sub, dataBuffer) {
        // body
        if (sub == ccmd.SUB_S_GAME_START) { 					//游戏开始
            cc.log("游戏开始！");
            return this.onSubGameStart(dataBuffer);
        } else if (sub == ccmd.SUB_S_OUT_CARD) { 					//用户出牌
            cc.log("用户出牌！");
            return this.onSubOutCard(dataBuffer);
        } else if (sub == ccmd.SUB_S_SEND_CARD) { 					//发送扑克
            cc.log("发送扑克！");
            return this.onSubSendCard(dataBuffer);
        } else if (sub == ccmd.SUB_S_OPERATE_NOTIFY) { 			//操作提示
            cc.log("操作提示！");
            return this.onSubOperateNotify(dataBuffer);
        } else if (sub == ccmd.SUB_S_HU_CARD) { 					//听牌提示
            cc.log("听牌提示！");
            return this.onSubListenNotify(dataBuffer);
        } else if (sub == ccmd.SUB_S_OPERATE_RESULT) { 			//操作命令
            cc.log("操作命令！");
            return this.onSubOperateResult(dataBuffer);
        } else if (sub == ccmd.SUB_S_LISTEN_CARD) { 				//用户听牌
            cc.log("用户听牌！");
            return this.onSubListenCard(dataBuffer);
        } else if (sub == ccmd.SUB_S_TRUSTEE) { 					//用户托管
            cc.log("用户托管！");
            return this.onSubTrustee(dataBuffer);
        } else if (sub == ccmd.SUB_S_GAME_CONCLUDE) { 				//游戏结束
            return this.onSubGameConclude(dataBuffer);
        } else if (sub == ccmd.SUB_S_RECORD) { 					//游戏记录
            cc.log("游戏记录！");
            return this.onSubGameRecord(dataBuffer);
        } else if (sub == ccmd.SUB_S_SET_BASESCORE) { 				//设置基数
            cc.log("设置基数！");
            this.lCellScore = dataBuffer.readint();
            return true;
        } else if (sub == ccmd.SUB_S_ANSWER_RULE) {                //游戏规则
            cc.log("游戏规则！");
            return this.setPlayHow(dataBuffer);
            ////////////-ysy_start_合距离警告//////////////////////////////////-
        } else if (sub == ccmd.SUB_S_DISTANCE) {               //距离警告
            cc.log("距离警告");
            //        this.onSubDistanceResult(dataBuffer)
            ////////////-ysy_end_合距离警告//////////////////////////////////-
        } else {
            appdf.assert(false, "default");
        }

        return false;
    },
	//游戏开始
	onSubGameStart : function(dataBuffer) {
        cc.log("游戏开始");
        this.m_cbGameStatus = ccmd.GAME_SCENE_PLAY;
        var cmd_data = ExternalFun.read_netdata(ccmd.CMD_S_GameStart, dataBuffer);
        //cc.log(cmd_data, "CMD_S_GameStart")
        for (var i = 0; i < ccmd.GAME_PLAYER; i++) {
            var viewId = this.SwitchViewChairID(i);
            var head = cmd_data.cbHeapCardInfo[i][0];
            var tail = cmd_data.cbHeapCardInfo[i][1];
        }
        this.cbGameTimes = cmd_data.cbQuanCount;
        this._gameView.upDataGameTimes(cmd_data.dwRoundCount, cmd_data.dwRoundLimit);
        this.wBankerUser = cmd_data.wBankerUser;
        var wViewBankerUser = this.SwitchViewChairID(this.wBankerUser);
        this._gameView.setBanker(wViewBankerUser);
        var cbCardCount = [0, 0, 0, 0];
        for (var i = 0; i < ccmd.GAME_PLAYER; i++) {
            var userItem = this._gameFrame.getTableUserItem(this.GetMeTableID(), i);
            var wViewChairId = this.SwitchViewChairID(i);
            this._gameView.OnUpdateUser(wViewChairId, userItem);
            this.m_cbUserScore[wViewChairId] = cmd_data.lDeltaScore[0][i];
            //更新分数
            this._gameView.upDataScore(wViewChairId, this.m_cbUserScore[wViewChairId]);
            if (userItem) {
                this.cbPlayStatus[wViewChairId] = 1;
                cbCardCount[wViewChairId] = 13;
                if (wViewChairId == wViewBankerUser) {
                    cbCardCount[wViewChairId] = cbCardCount[wViewChairId] + 1;
                }
            }
        }

        //墙头牌
        this.m_cbQiangTouCard = cmd_data.cbMagicIndex;

        //局号
        this._gameView.showTimes(cmd_data.ctRoundNo);
        this.wTimesNow = cmd_data.ctRoundNo;
        var num = this.m_cbTimesHistory.length;
        this.m_cbTimesHistory[num + 1] = cmd_data.ctRoundNo;

        if (this.wBankerUser != this.GetMeChairID()) {
            cmd_data.cbCardData[0][ccmd.MAX_COUNT - 1] = null;
        }

        //筛子
        var cbSiceCount1 = math_mod(cmd_data.wSiceCount, 256);
        var cbSiceCount2 = Math.floor(cmd_data.wSiceCount / 256);
        //起始位置
        var wStartChairId = math_mod(this.wBankerUser + cbSiceCount1 + cbSiceCount2 - 1, ccmd.GAME_PLAYER);
        var wStartViewId = this.SwitchViewChairID(wStartChairId);
        //起始位置数的起始牌
        var nStartCard = Math.min(cbSiceCount1, cbSiceCount2) * 2 + 1;
        //开始发牌
        var cardData = GameLogic.RemoveNull(cmd_data.cbCardData[0]);
        this._gameView.gameStart(wStartViewId, nStartCard, cardData, cbCardCount, cbSiceCount1, cbSiceCount2);

        //记录已出现的牌
        this.insertAppearCard(cmd_data.cbCardData[0]);
        this.wCurrentUser = cmd_data.wBankerUser;
        this.cbActionMask = cmd_data.cbUserAction;
        this.bMoPaiStatus = true;
        this.bSendCardFinsh = false;
        this.PlaySound(ccmd.RES_PATH + "sound/GAME_START.wav");
        // 刷新房卡
        if (PriRoom && GlobalUserItem.bPrivateRoom) {
            if (null != this._gameView._priView && null != this._gameView._priView.onRefreshInfo) {
                PriRoom.getInstance().m_tabPriData.dwPlayCount = PriRoom.getInstance().m_tabPriData.dwPlayCount + 1;
                this._gameView._priView.onRefreshInfo();
            }
        }

        //计时器
        this.SetGameClock(this.wCurrentUser, ccmd.IDI_OUT_CARD, this.cbTimeOutCard);
        return true;
    },
	//用户出牌
	onSubOutCard : function(dataBuffer) {
        var cmd_data = ExternalFun.read_netdata(ccmd.CMD_S_OutCard, dataBuffer);
        //cc.log(cmd_data, "CMD_S_OutCard")
        cc.log("用户出牌", cmd_data.cbOutCardData);

        var wViewId = this.SwitchViewChairID(cmd_data.wOutCardUser);
        this._gameView.gameOutCard(wViewId, cmd_data.cbOutCardData);

        //记录已出现的牌
        if (wViewId != ccmd.MY_VIEWID) {
            var cbAppearCard = [cmd_data.cbOutCardData];
            this.insertAppearCard(cbAppearCard);
        }

        this.bMoPaiStatus = false;
        this.KillGameClock();
        this._gameView.HideGameBtn();
        this.PlaySound(ccmd.RES_PATH + "sound/OUT_CARD.wav");
        this.playCardDataSound(wViewId, cmd_data.cbOutCardData);
        //轮到下一个
        this.wCurrentUser = cmd_data.wOutCardUser;
        var wTurnUser = this.wCurrentUser + 1;
        var wViewTurnUser = this.SwitchViewChairID(wTurnUser);
        while (this.cbPlayStatus[wViewTurnUser] != 1) {
            wTurnUser = wTurnUser + 1;
            if (wTurnUser > 3) {
                wTurnUser = 0;
            }
            wViewTurnUser = this.SwitchViewChairID(wTurnUser);
        }
        //设置听牌
        this._gameView._cardLayer.promptListenOutCard(null);
        if (wViewId == ccmd.MY_VIEWID) {
            var cbPromptHuCard = this.getListenPromptHuCard(cmd_data.cbOutCardData);
            this._gameView.setListeningCard(cbPromptHuCard);
            //听牌数据置空
            this.cbListenPromptOutCard = [];
            this.cbListenCardList = [];
        }
        //设置时间
        this.SetGameClock(wTurnUser, ccmd.IDI_OUT_CARD, this.cbTimeOutCard);
        return true;
    },

	//发送扑克(抓牌)
	onSubSendCard : function(dataBuffer) {
        var cmd_data = ExternalFun.read_netdata(ccmd.CMD_S_SendCard, dataBuffer);
        //cc.log(cmd_data, "CMD_S_SendCard")
        cc.log("发送扑克", cmd_data.cbCardData);

        this.wCurrentUser = cmd_data.wCurrentUser;
        var wCurrentViewId = this.SwitchViewChairID(this.wCurrentUser);
        this._gameView.gameSendCard(wCurrentViewId, cmd_data.cbCardData, cmd_data.bTail);

        this.SetGameClock(this.wCurrentUser, ccmd.IDI_OUT_CARD, this.cbTimeOutCard);

        this._gameView.HideGameBtn();
        if (this.wCurrentUser == this.GetMeChairID()) {
            this._gameView.recognizecbActionMask(cmd_data.cbActionMask, cmd_data.cbCardData);
            //自动胡牌
            if (cmd_data.cbActionMask >= GameLogic.WIK_CHI_HU && this.bTrustee) {
                this._gameView.onButtonClickedEvent(GameViewLayer.BT_WIN);
            }
        }

        //记录已出现的牌
        if (wCurrentViewId == ccmd.MY_VIEWID) {
            var cbAppearCard = [cmd_data.cbCardData];
            this.insertAppearCard(cbAppearCard);
        }

        this.bMoPaiStatus = true;
        this.PlaySound(ccmd.RES_PATH + "sound/SEND_CARD.wav");
        if (cmd_data.bTail) {
            this.playCardOperateSound(ccmd.MY_VIEWID, true, null);
        }
        return true;
    },
	//操作提示
	onSubOperateNotify : function(dataBuffer) {
        cc.log("操作提示");
        var cmd_data = ExternalFun.read_netdata(ccmd.CMD_S_OperateNotify, dataBuffer);
        //cc.log(cmd_data, "CMD_S_OperateNotify")

        if (this.bSendCardFinsh) { 	//发牌完成
            this._gameView.recognizecbActionMask(cmd_data.cbActionMask, cmd_data.cbActionCard);
        } else {
            this.cbActionMask = cmd_data.cbActionMask;
            this.cbActionCard = cmd_data.cbActionCard;
        }
        return true;
    },

	//听牌提示
	onSubListenNotify : function(dataBuffer) {
        cc.log("听牌提示");
        var cmd_data = ExternalFun.read_netdata(ccmd.CMD_S_Hu_Data, dataBuffer);
        //cc.log(cmd_data, "CMD_S_Hu_Data")

        this.cbListenPromptOutCard = [];
        this.cbListenCardList = [];
        for (i = 0; i < cmd_data.cbOutCardCount; i++) {
            this.cbListenPromptOutCard[i] = cmd_data.cbOutCardData[0][i];
            this.cbListenCardList[i] = [];
            for (j = 0; j <= cmd_data.cbHuCardCount[0][i]; j++) {
                this.cbListenCardList[i][j] = cmd_data.cbHuCardData[i][j];
            }
            cc.log("this.cbListenCardList" + i, this.cbListenCardList[i].join());
        }
        cc.log("this.cbListenPromptOutCard", this.cbListenPromptOutCard.join());

        return true;
    },

	//操作结果
	onSubOperateResult : function(dataBuffer) {
        cc.log("操作结果");

        var cmd_data = ExternalFun.read_netdata(ccmd.CMD_S_OperateResult, dataBuffer);
        //cc.log(cmd_data, "CMD_S_OperateResult")
        if (cmd_data.cbOperateCode == GameLogic.WIK_NULL) {
            appdf.assert(false, "没有操作也会进来？");
            return true;
        }
        this._gameView.showWaitOtherOperation(false);
        this._gameView.HideGameBtn();
        var wOperateViewId = this.SwitchViewChairID(cmd_data.wOperateUser);
        if (cmd_data.cbOperateCode < GameLogic.WIK_LISTEN || cmd_data.cbOperateCode == GameLogic.WIK_WIND
            || cmd_data.cbOperateCode == GameLogic.WIK_WALLTOP_O || cmd_data.cbOperateCode == GameLogic.WIK_WALLTOP_S
            || cmd_data.cbOperateCode == GameLogic.WIK_ARROW
            //-<//////////////////////////////<WW_ZhuiFengGang>////////////////////////////////////////-
            || cmd_data.cbOperateCode == GameLogic.WIK_CHASEWIND || cmd_data.cbOperateCode == GameLogic.WIK_CHASEARROW
            || cmd_data.cbOperateCode == GameLogic.WIK_CHASEBIRD || cmd_data.cbOperateCode == GameLogic.WIK_BIRD
        //->//////////////////////////////<WW_ZhuiFengGang>////////////////////////////////////////-
        ) { 		//并非听牌
            var nShowStatus = GameLogic.SHOW_NULL;
            var data1 = cmd_data.cbOperateCard[0][0];
            var data2 = cmd_data.cbOperateCard[0][1];
            var data3 = cmd_data.cbOperateCard[0][2];
            var cbOperateData = [];
            var cbRemoveData = [];
            if (cmd_data.cbOperateCode == GameLogic.WIK_GANG) {
                cbOperateData = [data1, data1, data1, data1];
                cbRemoveData = [data1, data1, data1];
                //检查杠的类型
                var cbCardCount = this._gameView._cardLayer.cbCardCount[wOperateViewId];
                if (math_mod(cbCardCount - 2, 3) == 0) {
                    if (this._gameView._cardLayer.checkBumpOrBridgeCard(wOperateViewId, data1)) {
                        nShowStatus = GameLogic.SHOW_MING_GANG;
                    } else {
                        nShowStatus = GameLogic.SHOW_AN_GANG;
                    }
                } else {
                    nShowStatus = GameLogic.SHOW_FANG_GANG;
                }
            } else if (cmd_data.cbOperateCode == GameLogic.WIK_PENG) {
                cbOperateData = [data1, data1, data1];
                cbRemoveData = [data1, data1];
                nShowStatus = GameLogic.SHOW_PENG;
                if (wOperateViewId == ccmd.MY_VIEWID) { //记录自己的绝牌
                    this._gameView._cardLayer.setJueCard(data1);
                }
            } else if (cmd_data.cbOperateCode == GameLogic.WIK_RIGHT) {
                //cbOperateData = cmd_data.cbOperateCard[1]
                cbOperateData = [data1, data3, data2];
                cbRemoveData = [data1, data2];
                nShowStatus = GameLogic.SHOW_CHI;
            } else if (cmd_data.cbOperateCode == GameLogic.WIK_CENTER) {
                //cbOperateData = cmd_data.cbOperateCard[1]
                cbOperateData = [data1, data2, data3];
                cbRemoveData = [data1, data3];
                nShowStatus = GameLogic.SHOW_CHI
            } else if (cmd_data.cbOperateCode == GameLogic.WIK_LEFT) {
                //cbOperateData = cmd_data.cbOperateCard[1]
                cbOperateData = [data2, data1, data3];
                cbRemoveData = [data2, data3];
                nShowStatus = GameLogic.SHOW_CHI;
            }
            //////////////-ysy_start//////////////////////////-
            if (cmd_data.cbOperateCode == GameLogic.WIK_WIND) {  ////东西南北 风杠
                cbOperateData = [0x31, 0x32, 0x33, 0x34];
                cbRemoveData = [0x31, 0x32, 0x33, 0x34];
                //检查杠的类型
                var cbCardCount = this._gameView._cardLayer.cbCardCount[wOperateViewId];
                if (this._gameView._cardLayer.checkBumpOrBridgeCard(wOperateViewId, data1)) {
                    nShowStatus = GameLogic.SHOW_FANG_GANG;
                } else {
                    nShowStatus = GameLogic.SHOW_FANG_GANG;
                }
            } else if (cmd_data.cbOperateCode == GameLogic.WIK_WALLTOP_O) {  ////墙头他杠
                cbOperateData = [this.m_cbQiangTouCard, this.m_cbQiangTouCard, this.m_cbQiangTouCard];
                cbRemoveData = [this.m_cbQiangTouCard, this.m_cbQiangTouCard];
                //检查杠的类型
                var cbCardCount = this._gameView._cardLayer.cbCardCount[wOperateViewId];
                if (this._gameView._cardLayer.checkBumpOrBridgeCard(wOperateViewId, data1)) {
                    nShowStatus = GameLogic.SHOW_FANG_GANG;
                } else {
                    nShowStatus = GameLogic.SHOW_FANG_GANG;
                }
            } else if (cmd_data.cbOperateCode == GameLogic.WIK_WALLTOP_S) {  ////墙头自杠
                cbOperateData = [this.m_cbQiangTouCard, this.m_cbQiangTouCard, this.m_cbQiangTouCard];
                cbRemoveData = [this.m_cbQiangTouCard, this.m_cbQiangTouCard, this.m_cbQiangTouCard];
                //检查杠的类型
                var cbCardCount = this._gameView._cardLayer.cbCardCount[wOperateViewId];
                if (this._gameView._cardLayer.checkBumpOrBridgeCard(wOperateViewId, data1)) {
                    nShowStatus = GameLogic.SHOW_FANG_GANG;
                } else {
                    nShowStatus = GameLogic.SHOW_FANG_GANG;
                }
            } else if (cmd_data.cbOperateCode == GameLogic.WIK_ARROW) { ////中发白
                cbOperateData = [0x35, 0x36, 0x37];
                cbRemoveData = [0x35, 0x36, 0x37];
                //检查杠的类型
                var cbCardCount = this._gameView._cardLayer.cbCardCount[wOperateViewId];
                if (this._gameView._cardLayer.checkBumpOrBridgeCard(wOperateViewId, data1)) {
                    nShowStatus = GameLogic.SHOW_FANG_GANG;
                } else {
                    nShowStatus = GameLogic.SHOW_FANG_GANG;
                }
                //-<////////////////////////////////////-<WW_ZhuiFengGang>////////////////////////////////////////////////
            } else if (cmd_data.cbOperateCode == GameLogic.WIK_BIRD) {		////幺鸡+中发白
                cbOperateData = [0x11, 0x35, 0x36, 0x37];
                cbRemoveData = [0x11, 0x35, 0x36, 0x37];
                //检查杠的类型
                var cbCardCount = this._gameView._cardLayer.cbCardCount[wOperateViewId];
                if (this._gameView._cardLayer.checkBumpOrBridgeCard(wOperateViewId, data1)) {
                    nShowStatus = GameLogic.SHOW_XUAN_GANG;
                } else {
                    nShowStatus = GameLogic.SHOW_XUAN_GANG;
                }
            } else if (cmd_data.cbOperateCode == GameLogic.WIK_CHASEWIND) {	////东南西北 追风杠
                //	cbOperateData = {data1}
                cbRemoveData = [data1];
                //检查杠的类型
                var cbCardCount = this._gameView._cardLayer.cbCardCount[wOperateViewId];
                nShowStatus = GameLogic.SHOW_ZHUI_GANG;
                this.AnalysisGangCardValue(wOperateViewId, data1);
            } else if (cmd_data.cbOperateCode == GameLogic.WIK_CHASEARROW) {	////中发白 追风杠
                //	cbOperateData = {data1}
                cbRemoveData = [data1];
                //检查杠的类型
                var cbCardCount = this._gameView._cardLayer.cbCardCount[wOperateViewId];
                nShowStatus = GameLogic.SHOW_ZHUI_GANG;
                this.AnalysisGangCardValue(wOperateViewId, data1);
            } else if (cmd_data.cbOperateCode == GameLogic.WIK_CHASEBIRD) {		////幺鸡 追风杠
                cbRemoveData = [data1];
                var cbCardCount = this._gameView._cardLayer.cbCardCount[wOperateViewId];
                nShowStatus = GameLogic.SHOW_ZHUI_GANG;
                this.AnalysisGangCardValue(wOperateViewId, data1);
                //->////////////////////////////////////-<WW_ZhuiFengGang>////////////////////////////////////////////////
            }
            //////////////ysy_end////////////////////-
            var bAnGang = nShowStatus == GameLogic.SHOW_AN_GANG;
            var bshow = this.AnalysisGang2(nShowStatus, cmd_data.wOperateUser, cmd_data.wProvideUser);
            this._gameView._cardLayer.bumpOrBridgeCard(wOperateViewId, cbOperateData, nShowStatus, bshow, cmd_data.cbOperateCode);
            var bRemoveSuccess = false;
            if (nShowStatus == GameLogic.SHOW_AN_GANG) {
                this._gameView._cardLayer.removeHandCard(wOperateViewId, cbOperateData, false);
            } else if (nShowStatus == GameLogic.SHOW_MING_GANG) {
                this._gameView._cardLayer.removeHandCard(wOperateViewId, [data1], false);
            } else {
                this._gameView._cardLayer.removeHandCard(wOperateViewId, cbRemoveData, false);
                //this._gameView._cardLayer.recycleDiscard(this.SwitchViewChairID(cmd_data.wProvideUser))
                cc.log("提供者不正常？", cmd_data.wProvideUser, this.GetMeChairID());
            }
            this.PlaySound(ccmd.RES_PATH + "sound/PACK_CARD.wav");
            this.playCardOperateSound(wOperateViewId, false, cmd_data.cbOperateCode);

            //记录已出现的牌
            if (wOperateViewId != ccmd.MY_VIEWID) {
                if (nShowStatus == GameLogic.SHOW_AN_GANG) {
                    if (cbOperateData.length == 0) {
                        this.insertAppearCard([data1]);
                    } else {
                        this.insertAppearCard(cbOperateData);
                    }
                } else if (nShowStatus == GameLogic.SHOW_MING_GANG
                    //-<////////////////////////////////////-<WW_ZhuiFengGang>////////////////////////////////////////////////
                    || nShowStatus == GameLogic.SHOW_ZHUI_GANG
                    || nShowStatus == GameLogic.SHOW_XUAN_GANG
                //->////////////////////////////////////-<WW_ZhuiFengGang>////////////////////////////////////////////////
                ) {
                    this.insertAppearCard([data1]);
                } else {
                    this.insertAppearCard(cbRemoveData);
                }
            }
            //提示听牌
            if (wOperateViewId == ccmd.MY_VIEWID && cmd_data.cbOperateCode == GameLogic.WIK_PENG) {
                this._gameView._cardLayer.promptListenOutCard(this.cbListenPromptOutCard);
            }
        }
        this._gameView.showOperateFlag(wOperateViewId, cmd_data.cbOperateCode);

        var cbTime = this.cbTimeOutCard - this.cbTimeOperateCard;
        this.SetGameClock(cmd_data.wOperateUser, ccmd.IDI_OUT_CARD, cbTime > 0 && cbTime || this.cbTimeOutCard);
        //碰或吃，杠等供应用户为他方时自己杠操作
        if (wOperateViewId == ccmd.MY_VIEWID) {
            //this._gameView.recognizecbActionMask(cmd_data.cbActionMask,null)
        }
        return true;
    },
	//用户听牌
	onSubListenCard : function(dataBuffer) {
        //cc.log("用户听牌")
        //var cmd_data = ExternalFun.read_netdata(ccmd.CMD_S_ListenCard, dataBuffer)
        //cc.log(cmd_data, "CMD_S_ListenCard")
        return true;
    },

	//用户托管
	onSubTrustee : function(dataBuffer) {
        cc.log("用户托管");
        var cmd_data = ExternalFun.read_netdata(ccmd.CMD_S_Trustee, dataBuffer);
        //cc.log(cmd_data, "trustee")

        var wViewChairId = this.SwitchViewChairID(cmd_data.wChairID);
        this._gameView.setUserTrustee(wViewChairId, cmd_data.bTrustee);
        if (cmd_data.wChairID == this.GetMeChairID()) {
            this.bTrustee = cmd_data.bTrustee;
        }

        if (cmd_data.bTrustee) {
            this.PlaySound(ccmd.RES_PATH + "sound/GAME_TRUSTEE.wav");
        } else {
            this.PlaySound(ccmd.RES_PATH + "sound/UNTRUSTEE.wav");
        }

        return true;
    },
	//游戏结束
	onSubGameConclude : function(dataBuffer) {
        cc.log("游戏结束");
        var cmd_data = ExternalFun.read_netdata(ccmd.CMD_S_GameConclude, dataBuffer);
        //cc.log(cmd_data, "CMD_S_GameConclude")

        var bMeWin = null;  	//null：没人赢，false：有人赢但我没赢，true：我赢
        //剩余牌
        var cbTotalCardData = GameLogic.TotalCardData;
        var cbRemainCard = GameLogic.RemoveCard(cbTotalCardData, this.cbAppearCardData);
        //提示胡牌标记
        for (i = 0; i < ccmd.GAME_PLAYER; i++) {
            var wViewChairId = this.SwitchViewChairID(i);
            if (cmd_data.cbChiHuKind[0][i] >= GameLogic.WIK_CHI_HU) {
                bMeWin = false;
                this.playCardOperateSound(ccmd.MY_VIEWID, false, GameLogic.WIK_CHI_HU);
                this._gameView.showOperateFlag(wViewChairId, GameLogic.WIK_CHI_HU);
                if (wViewChairId == ccmd.MY_VIEWID) {
                    bMeWin = true;
                }
            }
        }
        //隐藏显示按钮
        this._gameView.HideGameBtn();
        //显示结算图层
        var resultList = [];
        var cbBpBgData = this._gameView._cardLayer.getBpBgCardData();
        var cbBpAnGangList2 = this._gameView._cardLayer.getAnGangValue();
        var cbBpAnGangList = [[], [], [], []];
        for (i = 0; i < ccmd.GAME_PLAYER; i++) {
            var wViewChairId = this.SwitchViewChairID(i);
            var lScore = cmd_data.lGameScore[0][i];
            var user = this._gameFrame.getTableUserItem(this.GetMeTableID(), i);
            this.m_cbUserScore[wViewChairId] = lScore + this.m_cbUserScore[wViewChairId];
            if (user) {
                var result = [];
                result.userItem = user;
                result.lScore = lScore;
                result.cbChHuKind = cmd_data.cbChiHuKind[0][i];
                result.cbCardData = [];
                //手牌
                for (j = 0; j < cmd_data.cbCardCount[0][i]; j++) {
                    result.cbCardData[j] = cmd_data.cbHandCardData[i][j];
                }
                //碰杠牌
                result.cbBpBgCardData = cbBpBgData[wViewChairId];
                cbBpAnGangList = cbBpAnGangList2[wViewChairId];
                //奖码
                result.cbAwardCard = [];
                //for j = 1, cmd_data.cbMaCount[1][i] do
                for (j = 0; j < 3; j++) {
                    //result.cbAwardCard[j] = cmd_data.cbMaData[1][j]
                    result.cbAwardCard[j] = 1;
                }
                //插入
                resultList.push(result);
                //剩余牌里删掉对手的牌
                if (wViewChairId != ccmd.MY_VIEWID) {
                    cbRemainCard = GameLogic.RemoveCard(cbRemainCard, result.cbCardData);
                }
            }
        }
        //全部奖码
        var meIndex = this.GetMeChairID() + 1;
        var cbAwardCardTotal = [];
        for (i = 0; i < 7; i++) {
            var value = 1;
            if (value && value > 0) {
                cbAwardCardTotal.push(value);
            }
        }
        //删掉奖码
        //cbRemainCard = GameLogic.RemoveCard(cbRemainCard, cbAwardCardTotal)
        //if bMeWin == false then 			//有人赢但赢的人不是我
        //	cbRemainCard = GameLogic.RemoveCard(cbRemainCard, {cmd_data.cbProvideCard})
        //end
        //打散剩余牌
        cbRemainCard = GameLogic.RandCardList(cbRemainCard);
        //在首位插入奖码（将奖码伪装成剩余牌）
        for (i = 0; i < cbAwardCardTotal.length; i++) {
            cbRemainCard.push(cbAwardCardTotal[i],i);
        }
        cc.log("通过已显示牌统计，剩余多少张？", cbRemainCard.length);
        //显示结算框
        var self = this;
        this.runAction(new cc.Sequence(new cc.DelayTime(1), new cc.CallFunc(function (ref) {
            var cbRoomId = null;
            var cbHuiCard = null;
            if (GlobalUserItem.bPrivateRoom) {
                cbRoomId = PriRoom.getInstance().m_tabPriData.szServerID;
            }
            if (ccmd.EXISTENCE_HUI == true) {
                cbHuiCard = GameLogic.MAGIC_DATA;
            }
            self._gameView._resultLayer.showLayer(resultList, cbAwardCardTotal, cbRemainCard,
                self.wBankerUser, cmd_data.cbProvideCard, cmd_data.lGameScore[0], cmd_data.dwChiHuRight,
                self.wTimesNow, cbHuiCard, cbRoomId, cmd_data.wProvideUser, cmd_data.lHuScore[0], cbBpAnGangList);
        })));
        //播放音效
        if (bMeWin) {
            this.PlaySound(ccmd.RES_PATH + "sound/ZIMO_WIN.wav");
        } else {
            this.PlaySound(ccmd.RES_PATH + "sound/ZIMO_LOSE.wav");
        }

        this.cbPlayStatus = [0, 0, 0, 0];
        this.bTrustee = false;
        this.bSendCardFinsh = false;
        this._gameView.gameConclude();

        // if GlobalUserItem.bPrivateRoom then
        // 	//this._gameView.spClock.setVisible(false)
        // 	this._gameView.asLabTime.setString("0")
        // else
        this.SetGameClock(this.GetMeChairID(), ccmd.IDI_START_GAME, this.cbTimeStartGame);
        //end

        return true;
    },
	//游戏记录（房卡）
	onSubGameRecord : function(dataBuffer) {
        cc.log("游戏记录");
        var cmd_data = ExternalFun.read_netdata(ccmd.CMD_S_Record, dataBuffer);
        //cc.log(cmd_data, "CMD_S_Record")

        this.m_userRecord = [];
        var nInningsCount = cmd_data.nCount;
        for (i = 0; i < this.cbPlayerCount; i++) {
            this.m_userRecord[i] = [];
            this.m_userRecord[i].cbHuCount = cmd_data.cbHuCount[0][i];
            this.m_userRecord[i].cbMingGang = cmd_data.cbMingGang[0][i];
            this.m_userRecord[i].cbAnGang = cmd_data.cbAnGang[0][i];
            this.m_userRecord[i].cbMaCount = cmd_data.cbMaCount[0][i];
            //ysy——start
            this.m_userRecord[i].cbBankerCount = cmd_data.cbBankerCount[0][i];
            this.m_userRecord[i].cbZimoCount = cmd_data.cbZimoCount[0][i];
            this.m_userRecord[i].cbDianPaoCount = cmd_data.cbDianPaoCount[0][i];
            //ysy——end
            this.m_userRecord[i].lDetailScore = [];
            for (j = 0; j < nInningsCount; j++) {
                this.m_userRecord[i].lDetailScore[j] = cmd_data.lDetailScore[i][j];
            }
        }
        //cc.log(this.m_userRecord, "m_userRecord", 5)
    },
    //*****************************    普通函数     *********************************//
	//发牌完成
	sendCardFinish : function() {
        //提示操作
        if (this.cbActionMask) {
            this._gameView.recognizecbActionMask(this.cbActionMask, this.cbActionCard);
        }

        //提示听牌
        if (this.wBankerUser == this.GetMeChairID()) {
            this._gameView._cardLayer.promptListenOutCard(this.cbListenPromptOutCard);
        } else {
            //非庄家
            var cbHuCardData = this.cbListenCardList[0];
            if (cbHuCardData && cbHuCardData.length > 0) {
                this._gameView.setListeningCard(cbHuCardData);
            }
        }

        this.bSendCardFinsh = true;
        this._gameView._cardLayer.setHandCard2();
    },
	//解析筛子
	analyseSice : function(wSiceCount) {
        var cbSiceCount1 = math_mod(wSiceCount, 256);
        var cbSiceCount2 = Math.floor(wSiceCount / 256);
        return [cbSiceCount1, cbSiceCount2];
    },

	//设置操作时间
	SetGameOperateClock: function() {
        this.SetGameClock(this.GetMeChairID(), ccmd.IDI_OPERATE_CARD, this.cbTimeOperateCard);
    },
	//播放麻将数据音效（哪张）
	playCardDataSound : function(viewId, cbCardData) {
        var strGender = "";
        if (this.cbGender[viewId] == 1) {
            strGender = "BOY";
        } else {
            strGender = "GIRL";
        }
        var color = ["W_", "S_", "T_", "F_"];
        var nCardColor = Math.floor(cbCardData / 16);
        var nValue = math_mod(cbCardData, 16);
        if (ccmd.EXISTENCE_HUI == true) {
            if (cbCardData == GameLogic.MAGIC_DATA) {
                nValue = 5;
            }
        }
        var strFile = ccmd.RES_PATH + "sound/" + strGender + "/" + color[nCardColor] + nValue + ".wav";
        this.PlaySound(strFile);
    },

	//播放麻将操作音效
	playCardOperateSound : function(viewId, bTail, operateCode){
        appdf.assert(operateCode != GameLogic.WIK_NULL);

		var strGender = "";
		if (this.cbGender[viewId] == 1) {
			strGender = "BOY";
		} else {
			strGender = "GIRL";
		}
		var strName = "";
		if (bTail) {
			strName = "REPLACE.wav";
		} else {
			if (operateCode >= GameLogic.WIK_CHI_HU || GameLogic.WIK_FANG_PAO <= operateCode) {
				strName = "CHI_HU.wav";
			} else if (operateCode == GameLogic.WIK_LISTEN) {
				strName = "TING.wav";
			} else if (operateCode == GameLogic.WIK_GANG || operateCode == GameLogic.WIK_FENG_GANG ||
				operateCode == GameLogic.WIK_WALLTOP_S || operateCode == GameLogic.WIK_WALLTOP_O ||
				operateCode == GameLogic.WIK_CHASEARROW || operateCode == GameLogic.WIK_CHASEWIND ||
				operateCode == GameLogic.WIK_CHASEBIRD || operateCode == GameLogic.WIK_ARROW ||
				operateCode == GameLogic.WIK_WIND || operateCode == GameLogic.WIK_BIRD) {
				strName = "GANG.wav";
			} else if (operateCode == GameLogic.WIK_PENG) {
				strName = "PENG.wav";
			} else if (operateCode <= GameLogic.WIK_RIGHT) {
				strName = "CHI.wav";
			}
		}
		var strFile = ccmd.RES_PATH + "sound/" + strGender + "/" + strName;
		this.PlaySound(strFile);
	},
    //播放随机聊天音效
    playRandomSound : function(viewId) {
        var strGender = "";
        if (this.cbGender[viewId] == 1) {
            strGender = "BOY";
        } else {
            strGender = "GIRL";
        }
        var nRand = Math.random(25) - 1;
        if (nRand <= 6) {
            var num = 6603000 + nRand;
            var strName = num + ".wav";
            var strFile = ccmd.RES_PATH + "sound/PhraseVoice/" + strGender + "/" + strName;
            //this.PlaySound(strFile)
        }
    },
	//插入到已出现牌中
	insertAppearCard : function(cbCardData) {
        appdf.assert(cbCardData != null);
        for (i = 0; i < cbCardData.length; i++) {
            this.cbAppearCardData.push(cbCardData[i]);
        }
        this.cbAppearCardData.sort();
        var str = "";
        for (i = 0; i < this.cbAppearCardData.length; i++) {
            str = str + sprintf("%x,", this.cbAppearCardData[i]);
        }
    },

	getDetailScore : function() {
        return this.m_userRecord;
    },

	getListenPromptOutCard : function() {
        return this.cbListenPromptOutCard;
    },

	getListenPromptHuCard : function(cbOutCard) {
        if (!cbOutCard)
            return null;

        for (i = 0; i < this.cbListenPromptOutCard.length; i++) {
            if (this.cbListenPromptOutCard[i] == cbOutCard) {
                appdf.assert(this.cbListenCardList.length > 0 && this.cbListenCardList[i] && this.cbListenCardList[i].length > 0);
                return this.cbListenCardList[i];
            }
        }

        return null;
    },

	// 刷新房卡数据
	updatePriRoom : function() {
        if (PriRoom && GlobalUserItem.bPrivateRoom) {
            if (null != this._gameView._priView && null != this._gameView._priView.onRefreshInfo) {
                this._gameView._priView.onRefreshInfo();
            }
        }
    },

	//*****************************    发送消息     *********************************//
	//开始游戏
	sendGameStart : function() {
        if (PriRoom && GlobalUserItem.bPrivateRoom && PriRoom.getInstance().m_bRoomEnd) {
            //约战房结束
            return;
        }

        this.SendUserReady();
        this.OnResetGameEngine();
    },
	//出牌
	sendOutCard : function(card) {
        // body
        if (this._gameView._cardLayer.AnalysisOutCard(card) == false) {
            return false;
        }
        if (this.cbPlayer != null && this.cbPlayer != 0) {
            if (this._gameView._cardLayer.getCardCount() <= this.cbPlayer - 1) {
                return false;
            }
        }

        this._gameView.HideGameBtn();
        cc.log("发送出牌", card);

        var cmd_data = ExternalFun.create_netdata(ccmd.CMD_C_OutCard);
        cmd_data.pushbyte(card);
        return this.SendData(ccmd.SUB_C_OUT_CARD, cmd_data);
    },

	//操作扑克
	sendOperateCard : function(cbOperateCode, cbOperateCard) {
        cc.log("发送操作提示：", cbOperateCode, cbOperateCard.join());
        appdf.assert(cbOperateCard != null);

        //听牌数据置空
        this.cbListenPromptOutCard = [];
        this.cbListenCardList = [];
        this._gameView.setListeningCard(null);
        this._gameView._cardLayer.promptListenOutCard(null);

        //发送操作
        var cmd_data = new Cmd_Data(7);
        cmd_data.pushdword(cbOperateCode);
        for (i = 0; i < 3; i++) {
            cmd_data.pushbyte(cbOperateCard[i]);
        }
        this.SendData(ccmd.SUB_C_OPERATE_CARD, cmd_data);
    },
	//用户托管
	sendUserTrustee : function(isTrustee) {
        if (!this.bSendCardFinsh) {
            return;
        }

        var cmd_data = new Cmd_Data(1);
        cmd_data.pushbool(isTrustee);
        this.SendData(ccmd.SUB_C_TRUSTEE, cmd_data);
    },
	//用户补牌
	sendOperateStartCard : function(cbOperateCard,cbType) {

        if (cbType == 7) {
            var cmd_data = new Cmd_Data(136);
            for (i = 0; i < 136; i++) {
                cmd_data.pushbyte(cbOperateCard[i]);
            }
            this.SendData(cbType, cmd_data);
            this._gameView.showTextWords("已发送固定牌，从下局开始");
        } else if (cbType == 8) {
            var cmd_data = new Cmd_Data(136);
            for (i = 0; i < 136; i++) {
                cmd_data.pushbyte(0);
            }
            this.SendData(cbType, cmd_data);
            this._gameView.showTextWords("已取消固定发牌");
        }

    },

	//游戏规则
	setPlayHow : function(dataBuffer) {

        var cmd_data = ExternalFun.read_netdata(ccmd.CMD_C_CustomRule, dataBuffer);

        this.cbCustomRule = cmd_data.bCustomRule;				        //定制规则（1,0）
        this.cbTimes = cmd_data.cbTimes;						    //圈数	（4,8）
        this.cbDianPao = cmd_data.cbDianPao;						//点炮包三家（0，n）
        this.cbWindOrBird = cmd_data.cbWindOrBird;	                    //风杠或鸟杠（可赞，不可攒）
        this.cbBigWind = cmd_data.cbBigWind;					    //大风刮一吃到底(0,n)
        this.cbPlayer = cmd_data.cbPlayer;						    //(2/3/4)人麻将

        //刷新当前圈数
        this._gameView.setGameTimes(this.cbGameTimes);
        this._gameView.initPlayRule();

        ccmd.EXISTENCE_HUI = false;
        ccmd.EXISTENCE_JUE = false;

        return true;

    },

	findGameRule : function() {
        var cmd_data2 = new Cmd_Data(136);
        for (i = 0; i < 12; i++) {
            cmd_data2.pushbyte(0);
        }
        this.SendData(ccmd.SUB_C_ASK_RULE, cmd_data2);
    },

	getPlayHow : function() {
        return [this.cbDianPao, this.cbWindOrBird, this.cbBigWind];
    },

	AnalysisGang : function(cbType,user1,user2) {
        var bshow = true;
        if (GameLogic.SHOW_AN_GANG == cbType) {
            if (user1 != user2) {
                return false;
            }
        }
        return true;
    },

	AnalysisGang2 : function(cbType,user1,user2) {
        var bshow = true;
        if (GameLogic.SHOW_AN_GANG == cbType) {
            if (user1 == user2 && (this.GetMeChairID()) != user1) {
                return false;
            }
        }
        return true;
    },

	AnalysisGangCardValue : function(view,data) {
        if (data == 0x11) {
            cc.log("一条");
            this.m_cbGangZhuiFeng[view][4] = this.m_cbGangZhuiFeng[view][4] + 1;
        } else if (data == 0x31) {
            cc.log("东");
            this.m_cbGangZhuiFeng[view][0] = this.m_cbGangZhuiFeng[view][0] + 1;
        } else if (data == 0x32) {
            cc.log("南");
            this.m_cbGangZhuiFeng[view][1] = this.m_cbGangZhuiFeng[view][1] + 1;
        } else if (data == 0x33) {
            cc.log("西");
            this.m_cbGangZhuiFeng[view][2] = this.m_cbGangZhuiFeng[view][2] + 1;
        } else if (data == 0x34) {
            cc.log("北");
            this.m_cbGangZhuiFeng[view][3] = this.m_cbGangZhuiFeng[view][3] + 1;
        } else if (data == 0x35) {
            cc.log("中");
            this.m_cbGangZhuiFeng[view][5] = this.m_cbGangZhuiFeng[view][5] + 1;
        } else if (data == 0x36) {
            cc.log("发");
            this.m_cbGangZhuiFeng[view][6] = this.m_cbGangZhuiFeng[view][6] + 1;
        } else if (data == 0x37) {
            cc.log("白");
            this.m_cbGangZhuiFeng[view][7] = this.m_cbGangZhuiFeng[view][7] + 1;
        } else {
            cc.log("error");
        }
        this._gameView._cardLayer.ShowGangZhuiFeng(view, this.m_cbGangZhuiFeng[view]);
    },
	AnalysisZhuiFengNum: function(cbData) {
        var cbValue = 0;
        for (var i = 0; i < cbData.length; i++) {
            var wViewChairId = this.SwitchViewChairID(i);
            for (var j = 0; j < cbData[i].length; j++) {
                if (j > 28 && cbData[i][j] > 0) {
                    cbValue = 0x31 + (j - 27);
                    this.AnalysisGangCardValue(wViewChairId, cbValue);
                } else if (j == 9 && cbData[i][j] > 0) {
                    cbValue = 0x11;
                    this.AnalysisGangCardValue(wViewChairId, cbValue);
                }
            }
        }
    },
	////////////////////////ysy_start_合距离警告////////////////////////////////////
 	onNearUserInfo : function( event ) {
        cc.log("成功了！！！！！！！！！！！！！！");
        var nearuser = event.msg;
        cc.log(nearuser, "nearuser", 6);
        if (null != nearuser) {
            cc.log("&&成功了！！！！！" + nearuser.szNickName + " ## " + nearuser.dwDistance + "&&");
            //nearuser.dwDistance = 100
            if (null != nearuser.dwDistance && 0 != nearuser.dwDistance) {
                var cmd_data = ExternalFun.create_netdata(ccmd.CMD_C_Distance);
                cmd_data.pushdword(nearuser.dwUserID);
                cmd_data.pushscore(nearuser.dwDistance);
                this.SendData(ccmd.SUB_C_DISTANCE, cmd_data);
            }
        }
    },
	// 用户距离警告
	onSubDistanceResult : function(dataBuffer) {
        var cmd_table = ExternalFun.read_netdata(ccmd.CMD_S_Distance, dataBuffer);
        var QueryDialog = appdf.req("app.views.layer.other.QueryDialog");
        var self = this;
        this._queryDialog = new QueryDialog("有玩家距离过近您是否离开!", function (ok) {
            if (ok == true) {
                //退出
                this.onQueryExitGame();
            }
            self._queryDialog = null;
        });
        this._queryDialog.setCanTouchOutside(false);
        this.addChild(this._queryDialog);
    }
});
