/*
    author : Kil
    date :2017-12-6
 */
var GameRoomLayer = GameRoomLayerModel.extend({
   ctor: function () {
       this._super();
   },
    //获取桌子参数(背景、椅子布局)
    getTableParam : function(frameEngine) {
        var table_bg = new cc.Sprite("game/yule/sparrowcl/res/roomlist/roomtable.png");

        if (null != table_bg) {
            var bgSize = table_bg.getContentSize();
            //桌号背景
            var s = new cc.Sprite("Room/bg_tablenum.png");
            table_bg.addChild(s);
            s.setPosition(bgSize.width * 0.5, 10);
            var t = new ccui.Text("", "fonts/round_body.ttf", 16);
            table_bg.addChild(t);
            t.setColor(cc.color(255, 193, 200, 255));
            t.setTag(1);
            t.setPosition(bgSize.width * 0.5, 12);
            //状态
            s = new cc.Sprite("Room/flag_waitstatus.png");
            table_bg.addChild(s);
            s.setTag(2);
            s.setPosition(bgSize.width * 0.5, 98);
        }

        var chairCount = frameEngine.GetChairCount();
        var tabPos = [];
        if (chairCount == 2) {
            tabPos = [cc.p(107, 246), cc.p(107, -56)];
        } else if (chairCount == 3) {
            tabPos = [cc.p(-50, 100), cc.p(266, 100), cc.p(107, -56)];
        } else if (chairCount == 4) {
            tabPos = [cc.p(-50, 100), cc.p(107, 246), cc.p(266, 100), cc.p(107, -56)];
        }

        return [table_bg, tabPos];
    }
});

