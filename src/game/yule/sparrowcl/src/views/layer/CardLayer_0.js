/*
Author: Li
 */
// var ExternalFun = appdf.req(appdf.EXTERNAL_SRC+"ExternalFun");
// var GameLogic = appdf.req(appdf.GAME_SRC+"yule.sparrowcl.src.models.GameLogic");
// var ccmd = appdf.req(appdf.GAME_SRC+"yule.sparrowcl.src.models.CMD_Game");

var posTableCard = [cc.p(393, 546), cc.p(292, 179), cc.p(940, 187), cc.p(1044, 593)];
var posHandCard = [cc.p(380, 670), cc.p(224, 200), cc.p(1271, 0), cc.p(1110, 650)];
var posHandDownCard = [cc.p(295, 665), cc.p(210, 670), cc.p(295, 70), cc.p(1165, 670)];
var posDiscard = [cc.p(922, 465), cc.p(374, 535), cc.p(436, 258), cc.p(986, 204)];
var posBpBgCard = [cc.p(1080, 680), cc.p(238, 730), cc.p(0, 81), cc.p(1111, 164)];
var posBGgCard = [cc.p(100, 600), cc.p(450, 600), cc.p(770, 600), cc.p(1050, 600)];
var posBCgCard = [cc.p(248, 720), cc.p(609, 720), cc.p(970, 720), cc.p(1111, 164)];
var anchorPointHandCard = [cc.p(0, 0), cc.p(0, 0), cc.p(1, 0), cc.p(0, 1)];
var multipleTableCard = [[1, 0], [0, 1], [-1, 0], [0, -1]];
var multipleDownCard = [[1, 0], [0, -1], [1, 0], [0, -1]];
var multipleBpBgCard = [[-1, 0], [0, -1], [1, 0], [0, 1]];
var multipleGBgCard = [[0, 0], [0, 0]];

var CardLayer = cc.Layer.extend({
    TAG_BUMPORBRIDGE : 1,
    TAG_CARD_FONT : 1,
    TAG_LISTEN_FLAG : 2,
    ENUM_CARD_NORMAL : null,
    ENUM_CARD_POPUP : 1,
    ENUM_CARD_MOVING : 2,
    ENUM_CARD_OUT : 3,
    Z_ORDER_TOP : 50,

    ctor : function(scene) {
        this._super();
        this._scene = scene;
        this.onInitData();

        ExternalFun.registerTouchEvent(this, true);
        //桌牌
        //this.nodeTableCard = this.createTableCard()
        //手牌
        this.nodeHandCard = this.createHandCard();
        //铺着的手牌
        this.nodeHandDownCard = this.createHandDownCard();
        //弃牌
        this.nodeDiscard = this.createDiscard();
        //碰或杠牌
        this.nodeBpBgCard = this.createBpBgCard();
        ////////////////-**< ysy_start >**//////////////////
        //弹出杠牌
        this.nodeGBgCard = this.createBpBgCard2();
        //弹出吃牌
        this.nodeGBcCard = this.createBpBgCard2();
        //弹出吃牌
        this.nodeGQtCard = this.createBpBgCard2();
        ////////////////-**< ysy_end   >**//////////////////
    },

    onInitData : function() {
        //body
        //Math.randomseed((new Date()).getTime());
        this.cbCardData = [];
        this.cbCardCount = [ccmd.MAX_COUNT, ccmd.MAX_COUNT, ccmd.MAX_COUNT, ccmd.MAX_COUNT];
        this.nDiscardCount = [0, 0, 0, 0];
        this.nBpBgCount = [0, 0, 0, 0];
        this.bCardGray = [];
        this.cbCardStatus = [];
        this.nCurrentTouchCardTag = 0;
        this.currentTag = 0;
        this.cbListenList = [];
        this.bWin = false;
        this.currentOutCard = 0;
        this.nTableTailCardTag = 136;
        this.nRemainCardNum = 136;
        this.posTemp = cc.p(0, 0);
        this.nZOrderTemp = 0;
        this.nMovingIndex = 0;
        this.bSpreadCardEnabled = false;
        this.bSendOutCardEnabled = false;
        this.cbBpBgCardData = [[], [], [], []];
        this.cbJueCardData = [];//绝牌
        this.spHui = [];    //会标志
        this.spHuiJue = []; //绝会标志

        this.nGangScale = 1;  //杠缩放
        this.txtFengGang = [[], [], [], []];
        //东南西北，中发白一条
        for (j = 0; j < 4; j++) {
            for (i = 0; i < 8; i++) {
                this.txtFengGang[j][i] = new cc.LabelTTF("系统消息", "fonts/round_body.ttf", 18);
                this.txtFengGang[j][i].setColor(cc.color(255, 255, 255, 255));
                this.txtFengGang[j][i].setAnchorPoint(cc.p(0.5, 0.5));
                this.txtFengGang[j][i].setDimensions(26, 21);
                this.txtFengGang[j][i].setPosition(i * 60, 505);
                this.addChild(this.txtFengGang[j][i]);
                this.txtFengGang[j][i].setString("");
            }
        }
        this.cbGangParentTxt = null; //杠牌
        this.bposGang = [[true, true], [true, true], [true, true], [true, true]];//控制杠位置
    },

    onResetData : function() {
        //body
        for (i = 0; i < ccmd.GAME_PLAYER; i++) {
            this.cbCardCount[i] = ccmd.MAX_COUNT;
            this.nDiscardCount[i] = 0;
            this.nBpBgCount[i] = 0;
            this.nodeDiscard[i].removeAllChildren();
            this.nodeBpBgCard[i].removeAllChildren();
            for (j = 0; j < ccmd.MAX_COUNT; j++) {
                var card = this.nodeHandCard[i].getChildByTag(j);
                card.setColor(cc.color(255, 255, 255));
                card.setVisible(false);
                if (i == ccmd.MY_VIEWID) {
                    width = 88;
                    height = 136;
                    fSpacing = width;
                    var widthTotal = fSpacing * ccmd.MAX_COUNT;
                    var posX = widthTotal - width / 2 - fSpacing * (j - 1);
                    var posY = height / 2;
                    card.setPosition(posX, posY);
                    if (j == 1) {
                        card.setPosition(posX + 20, posY);						//每次抓的牌
                    }
                }
                this.nodeHandDownCard[i].getChildByTag(j).setVisible(false);
            }
        }
        this.cbCardData = [];
        this.cbJueCardData = [];
        this.bCardGray = [];
        this.cbCardStatus = [];
        this.nCurrentTouchCardTag = 0;
        this.nMovingIndex = 0;
        this.nZOrderTemp = 0;
        this.currentTag = 0;
        this.cbListenList = [];
        this.bWin = false;
        this.currentOutCard = 0;
        this.nTableTailCardTag = 136;
        this.nRemainCardNum = 136;
        this.posTemp = cc.p(0, 0);
        this.promptListenOutCard(null);
        this.bSpreadCardEnabled = false;
        this.bSendOutCardEnabled = false;
        this.cbBpBgCardData = [[], [], [], []];
        this.cbGangParent = null;
        this.cbChiParent = null;

        this.cbBpBgCardData = [[], [], [], []];
        this.cbJueCardData = [];
        for (i = 0; i < this.spHui.length; i++) {
            this.spHui[i].setVisible(false);
        }
        for (i = 0; i < this.spHuiJue.length; i++) {
            this.spHuiJue[i].setVisible(false);
        }
        this.spHui = [];
        this.spHuiJue = [];
        this.nGangScale = 1;

        //东南西北，中发白一条
        for (j = 0; j < 4; j++) {
            for (i = 0; i < 8; i++) {
                this.txtFengGang[j][i].setString("");
            }
        }
        if (this.cbGangParentTxt != null) {
            this.cbGangParentTxt.removeAllChildren();
        }
        this.cbGangParentTxt = null;
        this.bposGang = [[true, true], [true, true], [true, true], [true, true]];  //控制杠位置
    },
    //创建立着的手牌
    createHandCard : function() {
        var res =
            [
                ccmd.RES_PATH + "game/card_back_up.png",
                ccmd.RES_PATH + "game/card_left.png",
                ccmd.RES_PATH + "game/font_big/card_up.png",
                ccmd.RES_PATH + "game/card_right.png"
            ];

        var nodeCard = [];
        for (i = 0; i < ccmd.GAME_PLAYER; i++) {
            var bVert = i == 0 || i == ccmd.MY_VIEWID;
            var width = 0;
            var height = 0;
            var fSpacing = 0;
            if (i == 0) {
                width = 44;
                height = 67;
                fSpacing = width;
            }
            else if (i == ccmd.MY_VIEWID) {
                width = 88;
                height = 136;
                fSpacing = width;
            }
            else {
                width = 28;
                height = 75;
                fSpacing = 31;
            }

            var widthTotal = fSpacing * ccmd.MAX_COUNT;
            var heightTotal = height + fSpacing * (ccmd.MAX_COUNT - 1);
            nodeCard[i] = new cc.Node();
            nodeCard[i].setPosition(posHandCard[i]);
            nodeCard[i].setContentSize(bVert && cc.size(widthTotal, height) || cc.size(width, heightTotal));
            nodeCard[i].setAnchorPoint(anchorPointHandCard[i]);
            this.addChild(nodeCard[i], 1);
            for (j = 0; j < ccmd.MAX_COUNT; j++) {
                if (i == 0) {
                    pos = cc.p(width / 2 + fSpacing * (j), height / 2);
                }
                else if (i == 1) {
                    pos = cc.p(width / 2, height / 2 + fSpacing * (j));
                }
                else if (i == ccmd.MY_VIEWID) {
                    pos = cc.p(widthTotal - width / 2 - fSpacing * (j), height / 2);
                }
                else if (i == 3) {
                    pos = cc.p(width / 2, heightTotal - height / 2 - fSpacing * (j));
                }
                var card = new cc.Sprite(res[i]);
                card.setPosition(pos);
                card.setTag(j);
                card.setVisible(false);
                nodeCard[i].addChild(card);

                if (i == ccmd.MY_VIEWID) {
                    var cardData = GameLogic.MAGIC_DATA;
                    var nValue = math_mod(cardData, 16);
                    var nColor = Math.floor(cardData / 16);
                    var strFile = ccmd.RES_PATH + "game/font_big/font_" + nColor + "_" + nValue + ".png";
                    var font = new cc.Sprite(strFile);
                    font.setPosition(width / 2, height / 2 - 15);
                    font.setTag(1);
                    card.addChild(font);
                    if (j == 0) {
                        var x, y = card.getPosition();
                        card.setPositionX(x + 20);						//每次抓的牌
                    }
                    //提示听牌的小标记
                    var sp1 = new cc.Sprite("#sp_listenPromptFlag.png");
                    sp1.setPosition(69 / 2, 136 + 25);
                    sp1.setTag(CardLayer.TAG_LISTEN_FLAG);
                    sp1.setVisible(false);
                    card.addChild(sp1);
                }
                else if (i == 0) {
                    card.zIndex = 30 - j;	//修改了Z轴顺序，设置重叠效果   **** 需注意处理 ******
                }
            }
        }
        nodeCard[ccmd.MY_VIEWID].zIndex = 2;

        return nodeCard;
    },
    //创建铺着的手牌
    createHandDownCard : function() {
        var width = [44, 55, 88, 55];
        var height = [67, 47, 136, 47];
        var fSpacing = [width[0], 32, width[ccmd.MY_VIEWID], 32];
        var res =
            [
                ccmd.RES_PATH + "game/font_small/card_back.png",
                ccmd.RES_PATH + "game/font_small_side/card_back.png",
                ccmd.RES_PATH + "game/font_big/card_back.png",
                ccmd.RES_PATH + "game/font_small_side/card_back.png"
            ];
        var nodeCard = [];
        for (i = 0; i < ccmd.GAME_PLAYER; i++) {
            nodeCard[i] = new cc.Node();
            nodeCard[i].setPosition(posHandDownCard[i]);
            nodeCard[i].setAnchorPoint(cc.p(0.5, 0.5));
            this.addChild(nodeCard[i], 2);
            for (j = 0; j < ccmd.MAX_COUNT; j++) {
                var card = new cc.Sprite(res[i]);
                card.setVisible(false);
                card.setTag(j);
                nodeCard[i].addChild(card);
                if (i == 0 || i == 2) {
                    card.setPosition(fSpacing[i] * j, 0);
                }
                else {
                    card.setPosition(0, -fSpacing[i] * j);
                }
                if (i == 1) {
                    //card.setLocalZOrder(30 - j)
                }
            }
        }

        return nodeCard;
    },
    //创建弃牌
    createDiscard : function() {
        var nodeCard = [];
        for (i = 0; i < ccmd.GAME_PLAYER; i++) {
            nodeCard[i] = new cc.Node();
            nodeCard[i].setPosition(posDiscard[i]);
            //.setScale(0.7)        //缩小打出的牌（桌面不够用）    **** 需注意处理 ******
            this.addChild(nodeCard[i]);
        }
        //nodeCard[1].setLocalZOrder(2)

        return nodeCard;
    },
    //创建碰或杠牌
    createBpBgCard : function() {
        var nodeCard = [];
        for (i = 0; i < ccmd.GAME_PLAYER; i++) {
            nodeCard[i] = new cc.Node();
            nodeCard[i].setPosition(posBpBgCard[i]);
            this.addChild(nodeCard[i]);
        }
        nodeCard[ccmd.MY_VIEWID].setLocalZOrder(2);
        nodeCard[3].setLocalZOrder(1);

        return nodeCard;
    },
    //创建碰或杠牌,多组
    createBpBgCard2 : function() {
        var nodeCard = [];
        for (i = 0; i < ccmd.GAME_PLAYER; i++) {
            nodeCard[i] = new cc.Node();
            nodeCard[i].setPosition(posBGgCard[i]);
            this.addChild(nodeCard[i]);
            nodeCard[i].setLocalZOrder(6);
        }
        //nodeCard[ccmd.MY_VIEWID].setLocalZOrder(4)
        //nodeCard[4].setLocalZOrder(4)

        return nodeCard;
    },

    onTouchBegan : function(touch, event) {
        var pos = touch.getLocation();

        var parentPosX = this.nodeHandCard[ccmd.MY_VIEWID].getPosition().x;
        var parentPosY = this.nodeHandCard[ccmd.MY_VIEWID].getPosition().y;
        var parentSize = this.nodeHandCard[ccmd.MY_VIEWID].getContentSize();
        var posRelativeCard = cc.p(0, 0);
        posRelativeCard.x = pos.x - (parentPosX - parentSize.width);
        posRelativeCard.y = pos.y - parentPosY;

        for (i = 0; i < ccmd.MAX_COUNT; i++) {
            var card = this.nodeHandCard[ccmd.MY_VIEWID].getChildByTag(i);
            var cardRect = card.getBoundingBox();
            if (cc.rectContainsPoint(cardRect, posRelativeCard) && card.isVisible() && !this.bCardGray[i]) {
                //cc.log("touch begin!", pos.x, pos.y)
                this.nCurrentTouchCardTag = i;
                //缓存
                this.posTemp.x = card.getPosition().x;
                this.posTemp.y = card.getPosition().y;
                this.nZOrderTemp = card.getLocalZOrder();
                //将牌补满(ui与值的对齐方式)
                var nCount = 0;
                var num = math_mod(this.cbCardCount[ccmd.MY_VIEWID], 3);
                if (num == 2) {
                    nCount = this.cbCardCount[ccmd.MY_VIEWID];
                }
                else if (num == 1) {
                    nCount = this.cbCardCount[ccmd.MY_VIEWID] + 1;
                }
                else
                    appdf.assert(false);
                var index = nCount - i + 1;

                return true;
            }
        }

        return false;
    },

    onTouchMoved : function(touch, event) {
        var pos = touch.getLocation();
        //cc.log("touch move!", pos.x, pos.y)

        var parentPosX = this.nodeHandCard[ccmd.MY_VIEWID].getPosition().x;
        var parentPosY = this.nodeHandCard[ccmd.MY_VIEWID].getPosition().y;
        var parentSize = this.nodeHandCard[ccmd.MY_VIEWID].getContentSize();
        var posRelativeCard = cc.p(0, 0);
        posRelativeCard.x = pos.x - (parentPosX - parentSize.width);
        posRelativeCard.y = pos.y - parentPosY;

        //移动
        if (this.nCurrentTouchCardTag != 0) {
            //将牌补满(ui与值的对齐方式)
            var nCount = 0;
            var num = math_mod(this.cbCardCount[ccmd.MY_VIEWID], 3);
            if (num == 2) {
                nCount = this.cbCardCount[ccmd.MY_VIEWID];
            }
            else if (num == 1) {
                nCount = this.cbCardCount[ccmd.MY_VIEWID] + 1;
            }
            else
                appdf.assert(false);

            var index = nCount - this.nCurrentTouchCardTag + 1;

            this.cbCardStatus[index] = CardLayer.ENUM_CARD_MOVING;
            var card = this.nodeHandCard[ccmd.MY_VIEWID].getChildByTag(this.nCurrentTouchCardTag);
            card.setPosition(posRelativeCard);
            card.setLocalZOrder(CardLayer.Z_ORDER_TOP);
            //有则提示听牌
            var cbPromptHuCard = this._scene._scene.getListenPromptHuCard(this.cbCardData[index]);
            if (this.nMovingIndex != index && math_mod(this.cbCardCount[ccmd.MY_VIEWID], 3) == 2) {
                this._scene.setListeningCard(cbPromptHuCard);
            }
            this.nMovingIndex = index;
        }

        return true;
    },

    onTouchEnded : function(touch, event) {
        if (this.nCurrentTouchCardTag == 0) {
            return true;
        }

        var pos = touch.getLocation();
        // //cc.log("touch end!", pos.x, pos.y)
        var parentPosX = this.nodeHandCard[ccmd.MY_VIEWID].getPosition().x;
        var parentPosY = this.nodeHandCard[ccmd.MY_VIEWID].getPosition().y;
        var parentSize = this.nodeHandCard[ccmd.MY_VIEWID].getContentSize();
        var posRelativeCard = cc.p(0, 0);
        posRelativeCard.x = pos.x - (parentPosX - parentSize.width);
        posRelativeCard.y = pos.y - parentPosY;

        for (var i = 0; i < ccmd.MAX_COUNT; i++) {
            var card = this.nodeHandCard[ccmd.MY_VIEWID].getChildByTag(i);
            var cardRect = card.getBoundingBox();
            if (cc.rectContainsPoint(cardRect, posRelativeCard) && card.isVisible() && !this.bCardGray[i]) {
                //将牌补满(ui与值的对齐方式)
                var nCount = 0;
                var num = math_mod(this.cbCardCount[ccmd.MY_VIEWID], 3);
                if (num == 2) {
                    nCount = this.cbCardCount[ccmd.MY_VIEWID];
                }
                else if (num == 1) {
                    nCount = this.cbCardCount[ccmd.MY_VIEWID] + 1;
                }
                else
                    appdf.assert(false);

                var index = nCount - i + 1;		//算出这张牌对应牌值在this.cbCardData里的下标(cbCardData与cbCardStatus保持一致)
                this.text_lastCard = index;
                if (this.nCurrentTouchCardTag == i) {
                    if (this.cbCardStatus[index] == CardLayer.ENUM_CARD_NORMAL) { 		//原始状态
                        //恢复
                        this.cbCardStatus = [];
                        for (v = 1; v < ccmd.MAX_COUNT; v++) {
                            var cardTemp = this.nodeHandCard[ccmd.MY_VIEWID].getChildByTag(v);
                            cardTemp.setPositionY(136 / 2);
                        }
                        //弹出
                        this.cbCardStatus[index] = CardLayer.ENUM_CARD_POPUP;
                        card.setPositionY(136 / 2 + 30);
                        //有则提示听牌
                        if (math_mod(this.cbCardCount[ccmd.MY_VIEWID], 3) == 2) {
                            var cbPromptHuCard = this._scene._scene.getListenPromptHuCard(this.cbCardData[index]);
                            this._scene.setListeningCard(cbPromptHuCard);
                        }
                    }
                    else if (this.cbCardStatus[index] == CardLayer.ENUM_CARD_POPUP) { 		//弹出状态
                        if (math_mod(this.cbCardCount[ccmd.MY_VIEWID], 3) == 2) {
                            //出牌"
                            if (this.touchSendOutCard(this.cbCardData[index])) {
                                card.setVisible(false);
                                this.removeHandCard(ccmd.MY_VIEWID, this.cbCardData[index], true);
                            }
                        }
                        else
                        //弹回

                            this.cbCardStatus[index] = CardLayer.ENUM_CARD_NORMAL;
                        card.setPositionY(136 / 2);
                    }
                    else if (this.cbCardStatus[index] == CardLayer.ENUM_CARD_MOVING) { 		//移动状态
                        //恢复
                        this.cbCardStatus = [];
                        for (v = 0; v < ccmd.MAX_COUNT; v++) {
                            var cardTemp = this.nodeHandCard[ccmd.MY_VIEWID].getChildByTag(v);
                            cardTemp.setPositionY(136 / 2);
                        }
                        this.cbCardStatus[index] = CardLayer.ENUM_CARD_POPUP;
                        card.setPosition(this.posTemp.x, 136 / 2 + 30);
                        card.setLocalZOrder(this.nZOrderTemp);
                        //判断
                        //var rectDiscardPool = cc.rect(324, 218, 686, 283)
                        if (math_mod(this.cbCardCount[ccmd.MY_VIEWID], 3) == 2 && pos.y >= 120) {
                            //出牌"
                            if (this.touchSendOutCard(this.cbCardData[index])) {
                                card.setVisible(false);
                                card.setPosition(this.posTemp.x, 136 / 2);
                                this.removeHandCard(ccmd.MY_VIEWID, this.cbCardData[index], true);
                            }
                        }
                    }
                    else if (this.cbCardStatus[index] == CardLayer.ENUM_CARD_OUT) { 		//已出牌状态
                        appdf.assert(false);
                    }
                    break;
                }
            }
            //规避没点到牌的情况
            if (i == ccmd.MAX_COUNT - 1) {
                var cardTemp = this.nodeHandCard[ccmd.MY_VIEWID].getChildByTag(this.nCurrentTouchCardTag);
                cardTemp.setPosition(this.posTemp);
                cardTemp.setLocalZOrder(this.nZOrderTemp);
                this.cbCardStatus = [];
            }
        }
        this.nCurrentTouchCardTag = 0;
        this.nZOrderTemp = 0;
        return true;
    },

    onTouchCancelled : function(touch, event) {
        for (var j = 0; j < ccmd.MAX_COUNT; j++) {
            var card = this.nodeHandCard[ccmd.MY_VIEWID].getChildByTag(j);
            width = 88;
            height = 136;
            fSpacing = width;
            var widthTotal = fSpacing * ccmd.MAX_COUNT;
            var posX = widthTotal - width / 2 - fSpacing * (j - 1);
            var posY = height / 2;
            card.setPosition(posX, posY);
            if (j == 0) {
                card.setPosition(posX + 20, posY);						//每次抓的牌
            }
        }
    },
    //发牌
    sendCard : function(meData, cardCount) {
        appdf.assert(meData == null && cardCount == null);

        this.bSpreadCardEnabled = true;
        this.cbCardData = meData;
        this.cbCardCount = cardCount;
        var fDelayTime = 0.2;
        for (i = 0; i < ccmd.GAME_PLAYER; i++) {
            if (cardCount[i] > 0) {
                this.spreadCard(i, fDelayTime, 1);
            }
        }

        var fDelayTimeMax = fDelayTime * ccmd.MAX_COUNT + 0.3;
        this.runAction(new cc.Sequence(
            new cc.DelayTime(fDelayTimeMax),
            new cc.CallFunc(function (ref) {
                this.spreadCardFinish()
            })));
    },
    //伸展牌
    spreadCard : function(viewId, fDelayTime, nCurrentCount) {
        if (!this.bSpreadCardEnabled) {
            return false;
        }

        var nodeParent = this.nodeHandDownCard[viewId];
        if (nCurrentCount <= this.cbCardCount[viewId]) {
            //var posX, posY = nodeParent.getPosition()

            var downCard = nodeParent.getChildByTag(nCurrentCount);

            var downCardSize = downCard.getContentSize();
            downCard.setVisible(true);

            if (viewId == 0 || viewId == 2) {
                nodeParent.setPositionX(display.cx - downCardSize.width / 2 * nCurrentCount - 57);
            }
            else
                nodeParent.setPositionY(display.cy + downCardSize.height / 2 * nCurrentCount);
            this._scene._scene.PlaySound(ccmd.RES_PATH + "sound/OUT_CARD.wav");

            this.runAction(new cc.Sequence(
                new cc.DelayTime(fDelayTime),
                new cc.CallFunc(function (ref) {
                    return this.spreadCard(viewId, fDelayTime, nCurrentCount + 1);
                })));
        }
        else
            return true;
    },
    //发完牌
    spreadCardFinish : function() {
        GameLogic.SortCardList2(this.cbCardData);
        for (i = 0; i < ccmd.GAME_PLAYER; i++) {
            this.nRemainCardNum = this.nRemainCardNum - this.cbCardCount[i];
            for (j = 0; j < ccmd.MAX_COUNT; j++) {
                this.nodeHandDownCard[i].getChildByTag(j).setVisible(false);
            }
            this.setHandCard(i, this.cbCardCount[i], this.cbCardData);
        }

        this._scene.setRemainCardNum(this.nRemainCardNum);
        this._scene.sendCardFinish();
    },
    //抓牌
    catchCard : function(viewId, cardData, bTail) {
        appdf.assert(math_mod(this.cbCardCount[viewId], 3) == 1 || bTail, "Can't catch card!");
        this._scene._scene.playRandomSound(viewId);

        var HandCard = this.nodeHandCard[viewId].getChildByTag(1);
        HandCard.setVisible(true);
        this.cbCardCount[viewId] = this.cbCardCount[viewId] + 1;
        this.nRemainCardNum = this.nRemainCardNum - 1;
        this._scene.setRemainCardNum(this.nRemainCardNum);
        if (viewId == ccmd.MY_VIEWID) {
            this.cbCardData.push(cardData);
            //设置纹理
            //var rectX = this.switchToCardRectX(cardData)
            var nValue = math_mod(cardData, 16);
            var nColor = Math.floor(cardData / 16);
            var strFile = ccmd.RES_PATH + "game/font_big/font_" + nColor + "_" + nValue + ".png";
            var cardFont = HandCard.getChildByTag(CardLayer.TAG_CARD_FONT);
            cardFont.setTexture(strFile);
            //假如可以听牌
            var cbPromptCardData = this._scene._scene.getListenPromptOutCard();
            if (cbPromptCardData.length > 0) {
                this.promptListenOutCard(cbPromptCardData);
            }
        }
    },
    //设置本方牌值
    setHandCard : function(viewId, cardCount, meData) {
        appdf.assert(meData == null);
        this.cbCardData = meData;
        this.cbCardCount[viewId] = cardCount;
        this.bSendOutCardEnabled = true;

        //先全部隐藏
        for (var j = 0; j < ccmd.MAX_COUNT; j++) {
            this.nodeHandCard[viewId].getChildByTag(j).setVisible(false);
            this.nodeHandDownCard[viewId].getChildByTag(j).setVisible(false);
        }
        if (viewId == 2) {
            GameLogic.SortCardList2(this.cbCardData);
            for (i = 0; i < this.spHui.length; i++) {
                this.spHui[i].setVisible(false);
            }
            for (i = 0; i < this.spHuiJue.length; i++) {
                this.spHuiJue[i].setVisible(false);
            }
            this.spHui = [];
            this.spHuiJue = [];
        }
        //再显示
        if (this.cbCardCount[viewId] != 0) {
            var nCount = 0;
            var num = math_mod(this.cbCardCount[viewId], 3);
            if (num == 2) {
                nCount = this.cbCardCount[viewId];
            }
            else if (num == 1) {
                nCount = this.cbCardCount[viewId] + 1;
            }
            else
                appdf.assert(false);

            for (j = 0; j < this.cbCardCount[viewId]; j++) {
                var card = this.nodeHandCard[viewId].getChildByTag(nCount - j + 1);
                card.setVisible(true);
                if (viewId == ccmd.MY_VIEWID) {
                    //var rectX = this.switchToCardRectX(this.cbCardData[j])

                    var cardFont = card.getChildByTag(CardLayer.TAG_CARD_FONT);
                    var nValue = math_mod(this.cbCardData[j], 16);
                    var nColor = Math.floor(this.cbCardData[j] / 16);
                    var strFile = "game/font_big/font_" + nColor + "_" + nValue + ".png";
                    cardFont.setTexture(strFile);
                    if (ccmd.EXISTENCE_HUI == true && false) {
                        if (this.cbCardData[j] == GameLogic.MAGIC_DATA) {
                            //var hui = new cc.Sprite(ccmd.RES_PATH+"game/font_big/sp_hui.png")
                            var num = this.spHui.length;
                            this.spHui[num] = new cc.Sprite(ccmd.RES_PATH + "game/font_big/sp_hui.png");
                            this.spHui[num].setPosition(25, 55);
                            this.spHui[num].setTag(4);
                            this.spHui[num].addChild(cardFont);
                            this.spHui[num].setLocalZOrder(2);
                        }
                    }
                    if (ccmd.EXISTENCE_JUE == true && false) {
                        for (i = 0; i < this.cbJueCardData.length; i++) {
                            if (this.cbJueCardData[i] == this.cbCardData[j]) {
                                var num = this.spHuiJue.length;
                                //var hui = new cc.Sprite(ccmd.RES_PATH+"game/font_big/sp_jueHui.png")
                                this.spHuiJue[num] = new cc.Sprite(ccmd.RES_PATH + "game/font_big/sp_jueHui.png");
                                this.spHuiJue[num].setPosition(25, 60);
                                this.spHuiJue[num].setTag(5);
                                cardFont.addChild(this.spHuiJue[num]);
                                this.spHuiJue[num].setLocalZOrder(2);
                            }
                        }
                    }
                }
            }
        }
    },
    ////////////////////////////-ysy_start//////////////-
    //获取牌数量
    getCardCount : function(){
        return this.nRemainCardNum;
    },
    //重新绘画本方牌值
    setHandCard2 : function() {
        GameLogic.SortCardList2(this.cbCardData);
        viewId = 2;
        //先全部隐藏
        for (j = 0; j < ccmd.MAX_COUNT; j++) {
            this.nodeHandCard[viewId].getChildByTag(j).setVisible(false);
            this.nodeHandDownCard[viewId].getChildByTag(j).setVisible(false);
        }
        for (i = 0; i < this.spHui.length; i++) {
            this.spHui[i].setVisible(false);
        }
        for (i = 0; i < this.spHuiJue.length; i++) {
            this.spHuiJue[i].setVisible(false);
        }
        this.spHui = [];
        this.spHuiJue = [];
        //再显示
        if (this.cbCardCount[viewId] != 0) {
            var nCount = 0;
            var num = math_mod(this.cbCardCount[viewId], 3);
            if (num == 2) {
                nCount = this.cbCardCount[viewId];
            }
            else if (num == 1) {
                nCount = this.cbCardCount[viewId] + 1;
            }
            else
                appdf.assert(false);

            for (j = 0; j < this.cbCardCount[viewId]; j++) {
                var card = this.nodeHandCard[viewId].getChildByTag(nCount - j + 1);
                card.setVisible(true);
                if (viewId == ccmd.MY_VIEWID) {
                    //var rectX = this.switchToCardRectX(this.cbCardData[j])

                    var cardFont = card.getChildByTag(CardLayer.TAG_CARD_FONT);
                    var nValue = math_mod(this.cbCardData[j], 16);
                    var nColor = Math.floor(this.cbCardData[j] / 16);
                    var strFile = "game/font_big/font_" + nColor + "_" + nValue + ".png";
                    cardFont.setTexture(strFile);
                    if (ccmd.EXISTENCE_HUI == true) {
                        if (this.cbCardData[j] == GameLogic.MAGIC_DATA) {
                            //var hui = new cc.Sprite(ccmd.RES_PATH+"game/font_big/sp_hui.png")
                            var num = this.spHui.length;
                            this.spHui[num] = new cc.Sprite(ccmd.RES_PATH + "game/font_big/sp_hui.png");
                            this.spHui[num].setPosition(25, 55);
                            this.spHui[num].setTag(4);
                            cardFont.addChild(this.spHui[num]);
                            this.spHui[num].setLocalZOrder(2);
                        }
                    }
                    if (ccmd.EXISTENCE_JUE == true) {
                        for (i = 0; i < this.cbJueCardData.length; i++) {
                            if (this.cbJueCardData[i] == this.cbCardData[j]) {
                                var num = this.spHuiJue.length;
                                //var hui = new cc.Sprite(ccmd.RES_PATH+"game/font_big/sp_jueHui.png")
                                this.spHuiJue[num] = new cc.Sprite(ccmd.RES_PATH + "game/font_big/sp_jueHui.png");
                                this.spHuiJue[num].setPosition(25, 60);
                                this.spHuiJue[num].setTag(5);
                                cardFont.addChild(this.spHuiJue[num]);
                                this.spHuiJue[num].setLocalZOrder(2);
                            }
                        }
                    }
                }
            }
        }
    },

    ShowGangZhuiFeng : function(cbView,cbData) {
        for (i = 0; i < 8; i++) {
            if (cbData[i] > 0 && cbData[i] < 4) {
                this.txtFengGang[cbView][i].setString("X" + cbData[i]);
            }
            else
                this.txtFengGang[cbView][i].setString("");
        }
    },
    //删除手上的牌
    removeHandCard : function(viewId, cardData, bOutCard){
        appdf.assert(cardData == null);
        var cbRemainCount = this.cbCardCount[viewId] - cardData.length;
        if (bOutCard && math_mod(cbRemainCount, 3) != 1) {
            return false;
        }
        this.cbCardCount[viewId] = cbRemainCount;

        if (viewId == ccmd.MY_VIEWID) {
            for (i = 0; i < cardData.length; i++) {
                for (j = 0; j < this.cbCardData.length; j++) {
                    if (this.cbCardData[j] == cardData[i]) {
                        this.cbCardData.splice(j);
                        // table.remove(this.cbCardData, j);
                        break;
                    }
                    appdf.assert(j != this.cbCardData.length, "WithOut this card to remove!");
                }
            }
            GameLogic.SortCardList2(this.cbCardData);
        }
        this.setHandCard(viewId, this.cbCardCount[viewId], this.cbCardData);

        return true;
    },
    //牌打到弃牌堆
    discard : function(viewId, cardData) {
        var res1 =
            [
                ccmd.RES_PATH + "game/font_small/",
                ccmd.RES_PATH + "game/font_small_side/",
                ccmd.RES_PATH + "game/font_small/",
                ccmd.RES_PATH + "game/font_small_side/"
            ];
        var res = ccmd.RES_PATH + scc.logf("game/cardDown_%d.png", viewId);
        var width = 0;
        var height = 0;
        var fSpacing = 0;
        var posX = 0;
        var posY = 0;
        var pos = cc.p(0, 0);
        var nLimit = 0;
        var fBase = 0;
        var countTemp = this.nDiscardCount[viewId];
        var bVert = viewId == 0 || viewId == ccmd.MY_VIEWID;
        if (bVert) {
            width = 44;
            height = 67;
            fSpacing = width;
            nLimit = 12;

            while (countTemp >= nLimit * 2) {  	//超过一层
                fBase = fBase + height - 51;
                countTemp = countTemp - nLimit * 2;
            }

            while (countTemp >= nLimit) { 		//超过一行
                posY = posY - 51;
                countTemp = countTemp - nLimit;
            }

            var posX = fSpacing * countTemp;
            pos = viewId == ccmd.MY_VIEWID && cc.p(posX, posY + fBase) || cc.p(-posX, -posY + fBase);
        }
        else {
            width = 55;
            height = 47;
            fSpacing = 33;
            nLimit = 11;

            while (countTemp >= nLimit * 2) {		//超过一层
                fBase = fBase + 14;
                countTemp = countTemp - nLimit * 2;
            }

            while (countTemp >= nLimit) { 		//超过一行
                posX = posX - width;
                countTemp = countTemp - nLimit;
            }

            var posY = fSpacing * countTemp;
            pos = viewId == 3 && cc.p(-posX, posY + fBase) || cc.p(posX, -posY + fBase);
        }

        //var rectX = this.switchToCardRectX(cardData)
        var nValue = math_mod(cardData, 16);
        var nColor = Math.floor(cardData / 16);
        //牌底
        var card = new cc.Sprite(res1[viewId] + "card_down.png");
        card.setPosition(pos);
        //.setTextureRect(cc.rect(width*rectX, 0, width, height))
        card.setTag(this.nDiscardCount[viewId]);
        this.nodeDiscard[viewId].addChild(card);
        //字体
        var strFile = res1[viewId] + "font_" + nColor + "_" + nValue + ".png";
        var cardFont = new cc.Sprite(strFile);
        cardFont.setPosition(width / 2, height / 2 + 8);
        card.addChild(cardFont);
        //修改了Z轴顺序，设置重叠效果   **** 需注意处理 ******
        if (viewId == 0 || viewId == 3) {
            var nOrder = 0;
            if (this.nDiscardCount[viewId] >= nLimit * 6) {
                appdf.assert(false);
            }
            else if (this.nDiscardCount[viewId] >= nLimit * 4) {
                nOrder = 80 - (this.nDiscardCount[viewId] - nLimit * 4);
            }
            else if (this.nDiscardCount[viewId] >= nLimit * 2) {
                nOrder = 80 - (this.nDiscardCount[viewId] - nLimit * 2) * 2;
            }
            else
                nOrder = 80 - this.nDiscardCount[viewId] * 3;

            card.setLocalZOrder(nOrder);
        }
        //计数
        this.nDiscardCount[viewId] = this.nDiscardCount[viewId] + 1;
    },
    //从弃牌堆回收牌（有人要这张牌，碰或杠等）
    recycleDiscard : function(viewId) {
        this.nodeDiscard[viewId].getChildByTag(this.nDiscardCount[viewId]).removeFromParent();
        this.nDiscardCount[viewId] = this.nDiscardCount[viewId] - 1;
    },
    //碰或杠
    bumpOrBridgeCard : function(viewId, cbCardData, nShowStatus,bshow,cbCardType){
        //bshow = false
        if (cbCardData.length == 0 || cbCardData == null) {
            return;
        }
        appdf.assert(cbCardData == null);
        var resCard =
            [
                ccmd.RES_PATH+"game/font_small/card_down.png",
                ccmd.RES_PATH+"game/font_small_side/card_down.png",
                ccmd.RES_PATH+"game/font_middle/card_down.png",
                ccmd.RES_PATH+"game/font_small_side/card_down.png"
            ];
        var resFont =
            [
                ccmd.RES_PATH+"game/font_small/",
                ccmd.RES_PATH+"game/font_small_side/",
                ccmd.RES_PATH+"game/font_middle/",
                ccmd.RES_PATH+"game/font_small_side/"
            ];
        var width = 0;
        var height = 0;
        var widthTotal = 0;
        var heightTotal = 0;
        var fSpacing = 0;
        if (viewId == 0) {
            width = 44;
            height = 67;
            fSpacing = width;
        }
        else if (viewId == 2) {
            width = 80;
            height = 116;
            fSpacing = width;
        }
        else {
            width = 55;
            height = 47;
            fSpacing = 32;
        }


        var fN = [15, 15, 15, 15];
        var fParentSpacing = fSpacing*3 + fN[viewId];
        var nodeParent = new cc.Node();
        nodeParent.setPosition(this.nBpBgCount[viewId]*fParentSpacing*multipleBpBgCard[viewId][0],
            this.nBpBgCount[viewId]*fParentSpacing*multipleBpBgCard[viewId][1]);
        this.nodeBpBgCard[viewId].addChild(nodeParent);

        if (cbCardType == GameLogic.WIK_WIND || cbCardType == GameLogic.WIK_BIRD) {
            if (viewId == 0) {
                width = 33;
                height = 67;
                fSpacing = width;
                this.nGangScale = 0.77;
            }
            else if (viewId == 2) {
                width = 60;
                height = 100;
                fSpacing = width;
                this.nGangScale = 0.77;
            }
            else {
                width = 55;
                height = 47;
                fSpacing = 24;
                this.nGangScale = 0.77;
            }
        }
        else
            this.nGangScale = 1;

        if (nShowStatus != GameLogic.SHOW_CHI) {
            //明杠
            if (nShowStatus == GameLogic.SHOW_MING_GANG) {
                nodeParentMG = this.nodeBpBgCard[viewId].getChildByTag(cbCardData[0]);
                //appdf.assert(nodeParentMG, "None of this bump card!")
                if (nodeParentMG) {
                    this.nBpBgCount[viewId] = this.nBpBgCount[viewId] - 1;
                    nodeParent.removeFromParent();
                    nodeParentMG.removeAllChildren();
                    nodeParent = nodeParentMG;
                }
            }
            nodeParent.setTag(cbCardData[1]);
        }

        for (i = 0 ; i <  cbCardData.length ; j++ ) {
            //var rectX = this.switchToCardRectX(cbCardData[i])
            //牌底
            var card = new cc.Sprite(resCard[viewId]);
            card.setPosition(i * fSpacing * multipleBpBgCard[viewId][0], i * fSpacing * multipleBpBgCard[viewId][1]);
            //.setTextureRect(cc.rect(width*rectX, 0, width, height))
            nodeParent.addChild(card);
            card.setScale(this.nGangScale);
            //字体
            var nValue = math_mod(cbCardData[i], 16);
            var nColor = Math.floor(cbCardData[i] / 16);
            var strFile = resFont[viewId] + "font_" + nColor + "_" + nValue + ".png";
            if (i == 3 && bshow == false) {
                cc.log("不画");
            }
            else {
                var posX = 0;
                var posGps = 0;
                var cbWorB = 1;
                if (cbCardType == GameLogic.WIK_BIRD) {
                    posGps = 4;
                    cbWorB = 2;
                }
                if (cbCardType == GameLogic.WIK_WIND && this.bposGang[viewId][1] || (cbCardType == GameLogic.WIK_BIRD && this.bposGang[viewId][2])) {
                    posX = 8;
                    var fengGangPosX = 0;
                    var fengGangPosY = 0;
                    var fengGangRo = 1;
                    if (viewId == 0) {
                        fengGangPosX = 1080 - (this.nBpBgCount[viewId] * fParentSpacing - i * fSpacing * multipleBpBgCard[viewId][1]);
                        fengGangPosY = 640;
                        fengGangRo = 180;
                    }
                    else if (viewId == 1) {
                        fengGangPosX = 280;
                        fengGangPosY = 730 - (this.nBpBgCount[viewId] * fParentSpacing - i * fSpacing * multipleBpBgCard[viewId][2]);
                        fengGangRo = 90;
                    }
                    else if (viewId == 2) {
                        fengGangPosX = this.nBpBgCount[viewId] * fParentSpacing + i * fSpacing * multipleBpBgCard[viewId][1];
                        fengGangPosY = 140;
                        fengGangRo = 0;
                    }
                    else {
                        fengGangPosX = 1080;
                        fengGangPosY = 164 + (this.nBpBgCount[viewId] * fParentSpacing + i * fSpacing * multipleBpBgCard[viewId][2]);
                        fengGangRo = -90;
                    }

                    this.txtFengGang[viewId][posGps + i].setPosition(fengGangPosX, fengGangPosY);
                    this.txtFengGang[viewId][posGps + i].setRotation(fengGangRo);
                    if (i == 3) {
                        this.bposGang[viewId][cbWorB] = false;
                    }
                }
                var cardFont = new cc.Sprite(strFile);
                cardFont.setPosition(width / 2 + posX, height / 2 + 8);
                cardFont.setTag(1);
                card.addChild(cardFont);
                cardFont.setScale(this.nGangScale);

                // if (viewId == 1 || viewId == 4) {
                //     cardFont.setRotation(180);
                // }
            }
            if (viewId == 3) {
                card.setLocalZOrder(5 - i);
            }
            if (i == 3) { 		//杠
                var moveUp = [17, 14, 23, 14];
                if (bshow == false) {
                    card.setTexture(resFont[viewId] + "card_back.png");
                    //card.setPosition(2*fSpacing*multipleBpBgCard[viewId][1], 2*fSpacing*multipleBpBgCard[viewId][2] + moveUp[viewId])
                    card.setLocalZOrder(5);
                }
                else
                //card.setPosition(2*fSpacing*multipleBpBgCard[viewId][1], 2*fSpacing*multipleBpBgCard[viewId][2] + moveUp[viewId])
                    cc.log("正常显示");

                if (cbCardType != GameLogic.WIK_WIND && cbCardType != GameLogic.WIK_BIRD) {
                    card.setPosition(2 * fSpacing * multipleBpBgCard[viewId][1], 2 * fSpacing * multipleBpBgCard[viewId][2] + moveUp[viewId]);
                    card.setLocalZOrder(5);
                }
            }
            else if (nShowStatus == GameLogic.SHOW_AN_GANG) { 		//暗杠
                card.setTexture(resFont[viewId] + "card_back.png");
                card.removeChildByTag(1);
            }
            //添加牌到记录里
            if (nShowStatus != GameLogic.SHOW_MING_GANG || i == 3) {
                var pos = 1;
                while (pos <= this.cbBpBgCardData[viewId].length) {
                    if (this.cbBpBgCardData[viewId][pos] == cbCardData[i]) {
                        break;
                    }
                    pos = pos + 1;
                }
                this.cbBpBgCardData[viewId].splice(pos, 0, cbCardData[i]);
            }
        }
        this.nBpBgCount[viewId] = this.nBpBgCount[viewId] + 1;
    },
    //-**************< ysy_start >**************//
    //显示杠牌
    showGangCardGroup : function(wIndex,cardData) {

        var fxSpacing = 45;
        var fySpacing = 10;
        var fSpacing = 10;
        var fParentSpacing = 0;

        var nValue = math_mod(cardData[0][0], 16);
        var nColor = Math.floor(cardData[0][0] / 16);
        var strFile2 = ccmd.RES_PATH + "game/font_small/card_up.png";
        var strFile = ccmd.RES_PATH + "game/font_small/font_" + nColor + "_" + nValue + ".png";

        //this.nodeGBgCard = this.createBpBgCard()

        this.cbGangParent = new cc.Node();
        this.cbGangParent.setPosition(fParentSpacing, fParentSpacing);
        this.nodeGBgCard[wIndex].addChild(this.cbGangParent);

        for (i = 0; i < wIndex; i++) {
            for (t = 0; t < cardData[i]; t++) {
                nValue = math_mod(cardData[i][t], 16);
                nColor = Math.floor(cardData[i][t] / 16);
                strFile2 = ccmd.RES_PATH + "game/font_small/card_up.png";
                strFile = ccmd.RES_PATH + "game/font_small/font_" + nColor + "_" + nValue + ".png";

                var card = new cc.Sprite(strFile2);
                card.setPosition(-((wIndex - i) * (400 - wIndex * 20)) + fxSpacing * t, fySpacing - 260);
                this.cbGangParent.addChild(card);

                var card = new cc.Sprite(strFile);
                card.setPosition(-((wIndex - i) * (400 - wIndex * 20)) + fxSpacing * t, fySpacing - 260);
                this.cbGangParent.addChild(card);
            }
        }
    },
    //显示杠牌
    showGangCardGroup2 : function(wIndex,cardData) {
        var self = this;
        if (this.cbGangParentTxt != null) {
            this.cbGangParentTxt.removeAllChildren();
        }
        var btnGangback = function (ref, eventType) {
            if (eventType == ccui.TouchEventType.ended) {
                self.btnGangCardback(ref.getTag(), ref);
            }
        };
        var nValue = 0;
        var nColor = 0;
        var fParentSpacing = 300;
        var fSpacing = 0;
        var strFile2 = "";
        var strFile = "";

        this.cbGangParentTxt = new cc.Node();
        this.addChild(this.cbGangParentTxt);
        this.cbGangParentTxt.setLocalZOrder(10);

        var cbGangbg = new cc.Sprite("game/playRule/sp_gangBg.png");
        cbGangbg.setPosition(1334 / 2, 350);
        this.cbGangParentTxt.addChild(cbGangbg);

        var cbGangbg = new cc.Sprite("game/playRule/sp_gang.png");
        cbGangbg.setPosition(100, 350);
        this.cbGangParentTxt.addChild(cbGangbg);
        for (i = 0; i < cardData; i++) {
            for (t = 0; t < cardData[i]; t++) {

                strFile2 = ccmd.RES_PATH + "game/font_small/card_up.png";
                var btn = new ccui.Button(strFile2, strFile2);
                btn.setPosition((44 * t) + fParentSpacing, 350);
                btn.setTag(i);
                this.cbGangParentTxt.addChild(btn);
                btn.addTouchEventListener(btnGangback);

                nValue = math_mod(cardData[i][t], 16);
                nColor = Math.floor(cardData[i][t] / 16);
                strFile = ccmd.RES_PATH + "game/font_small/font_" + nColor + "_" + nValue + ".png";
                var card = new cc.Sprite(strFile);
                card.setPosition((44 * t) + fParentSpacing, 350);
                this.cbGangParentTxt.addChild(card);
                fSpacing = fSpacing + 44;
            }
            fParentSpacing = fParentSpacing + fSpacing + 44;
            fSpacing = 0;
        }
    },
    //显示吃牌
    //其它按钮响应
    btnGangCardback : function(tag, ref) {
        cc.log(tag);
        if (tag != 0) {
            this._scene.getGangCard(tag);
        }
        this.cbGangParentTxt.removeAllChildren();
    },
    //其它按钮响应
    btnChiCardback : function(tag, ref) {
        cc.log(tag);
        if (tag != 0) {
            this._scene.getChiCard(tag);
        }
        this.cbGangParentTxt.removeAllChildren();
    },

    showChiCardGroup : function(wIndex,cardData) {

        var fxSpacing = 45;
        var fySpacing = 10;
        var fSpacing = 10;
        var fParentSpacing = 0;

        var nValue = math_mod(cardData[0][0], 16);
        var nColor = Math.floor(cardData[0][0] / 16);
        var strFile2 = ccmd.RES_PATH + "game/font_small/card_up.png";
        var strFile = ccmd.RES_PATH + "game/font_small/font_" + nColor + "_" + nValue + ".png";

        //this.nodeGBcCard = this.createBpBgCard()

        this.cbChiParent = new cc.Node();
        this.cbChiParent.setPosition(fParentSpacing, fParentSpacing);
        this.nodeGBcCard[1].addChild();

        for (i = 0; i < wIndex; i++) {
            for (t = 0; t < 3; t++) {
                nValue = math_mod(cardData[i][t], 16);
                nColor = Math.floor(cardData[i][t] / 16);
                strFile2 = ccmd.RES_PATH + "game/font_small/card_up.png";
                strFile = ccmd.RES_PATH + "game/font_small/font_" + nColor + "_" + nValue + ".png";

                var card = new cc.Sprite(strFile2);
                card.setPosition(-((wIndex - i) * 300) + fxSpacing * t, fySpacing - 260);
                this.cbChiParent.addChild(card);

                var card = new cc.Sprite(strFile);
                card.setPosition(-((wIndex - i) * 300) + fxSpacing * t, fySpacing - 260);
                this.cbChiParent.addChild(card);
            }
        }
    },

    showChiCardGroup2 : function(wIndex,cardData) {
        var self = this;
        if (this.cbGangParentTxt != null) {
            this.cbGangParentTxt.removeAllChildren();
        }
        var btnGangback = function (ref, eventType) {
            if (eventType == ccui.Widget.TOUCH_ENDED) {
                self.btnChiCardback(ref.getTag(), ref);
            }
        };
        var nValue = 0;
        var nColor = 0;
        var fParentSpacing = 300;
        var fSpacing = 0;
        var strFile2 = "";
        var strFile = "";

        this.cbGangParentTxt = new cc.Node();
        this.addChild(this.cbGangParentTxt);
        this.cbGangParentTxt.setLocalZOrder(10);

        var cbGangbg = new cc.Sprite("game/playRule/sp_gangBg.png");
        cbGangbg.setPosition(1334 / 2, 350);
        this.cbGangParentTxt.addChild(cbGangbg);

        cbGangbg = new cc.Sprite("game/playRule/sp_chi.png");
        cbGangbg.setPosition(100, 350);
        this.cbGangParentTxt.addChild(cbGangbg);
        for (i = 0; i < cardData.length; i++) {
            for (t = 0; t < cardData[i].length; t++) {
                strFile2 = ccmd.RES_PATH + "game/font_small/card_up.png";
                var btn = new ccui.Button(strFile2, strFile2);
                btn.setPosition((44 * t) + fParentSpacing, 350);
                btn.setTag(i);
                this.cbGangParentTxt.addChild(btn);
                btn.addTouchEventListener(btnGangback);

                nValue = math_mod(cardData[i][t], 16);
                nColor = Math.floor(cardData[i][t] / 16);
                strFile = ccmd.RES_PATH + "game/font_small/font_" + nColor + "_" + nValue + ".png";
                var card = new cc.Sprite(strFile);
                card.setPosition((44 * t) + fParentSpacing, 350);
                this.cbGangParentTxt.addChild(card);
                fSpacing = fSpacing + 44;
            }
            fParentSpacing = fParentSpacing + fSpacing + 44;
            fSpacing = 0;
        }
    },

    isGangOrChi : function(sValue) {

        // 吃牌和杠牌为空返回
        if (this.cbGangParent == null && this.cbChiParent == null) {
            return;
        }
        //杠牌隐藏
        if (this.cbGangParent != null) {
            this.cbGangParent.setVisible(false);
        }
        //吃牌隐藏
        if (this.cbChiParent != null) {
            this.cbChiParent.setVisible(false);
        }
        //清空吃牌和杠牌精灵
        if (sValue == null) {
            this.cbGangParent = null;
            this.cbChiParent = null;
        }

        if (sValue == "CHI" && this.cbChiParent != null) {        //吃牌显示
            this.cbChiParent.setVisible(true);
        }
        else if (sValue == "GANG" && this.cbGangParent != null) {    //杠牌显示
            this.cbGangParent.setVisible(true);
        }

    },
    //显示墙头牌
    showQiangtouCard : function(cardData){

        var fxSpacing = -11;
        var fySpacing = 310;
        var fParentSpacing = 0;
        var wIndex = 0;
        var nValue = math_mod(cardData, 16);
        var nColor = Math.floor(cardData / 16);
        var strFile2 = ccmd.RES_PATH + "game/font_big/card_up.png";
        var strFile = ccmd.RES_PATH + "game/font_big/font_" + nColor + "_" + nValue + ".png";

        this.cbQiangtouParent = new cc.Node();
        this.setPosition(fParentSpacing, fParentSpacing);
        this.nodeGQtCard[wIndex].addChild(this);

        var card = new cc.Sprite(strFile2);
        card.setPosition(fxSpacing, fySpacing - 230);
        this.cbQiangtouParent.addChild(card);

        card = new cc.Sprite(strFile);
        card.setPosition(fxSpacing, fySpacing - 230);
        this.cbQiangtouParent.addChild(card);
    },
    //分析出牌
    AnalysisOutCard : function(card) {

        //判断会模式并且是绝模式
        if (ccmd.EXISTENCE_HUI == true && ccmd.EXISTENCE_JUE == true) {     //是会模式，也是绝模式
            //判断牌是否是会
            if (card != GameLogic.MAGIC_DATA) {              //-不是会
                //判断是不是绝
                var num = this.cbJueCardData.length;
                var nCount = 0;
                for (i = 0; i < num; i++) {
                    if (this.cbJueCardData[i] == card) {     //判断绝牌
                        break;
                    }
                    nCount = nCount + 1;
                }
                if (nCount >= num) {                        //不是绝
                    return true;
                }
                else {                                         //是绝
                    //判断是否手牌全是绝牌
                    var nCardDatacount = this.cbCardData.length;
                    var nHuiCount = 0;
                    for (i = 0; i < nCardDatacount; i++) {
                        for (j = 0; j < this.cbJueCardData.length; j++) {
                            if (this.cbCardData[i] == this.cbJueCardData[j]) { //判断每张牌，手牌中是否全是绝牌
                                nHuiCount = nHuiCount + 1;
                                break;
                            }
                        }
                        if (this.cbCardData[i] == GameLogic.MAGIC_DATA) {
                            nHuiCount = nHuiCount + 1;
                        }
                    }
                    if (nHuiCount >= nCardDatacount) { //手里全是绝牌
                        return true;
                    }
                    else                                 //手里不全是绝牌
                        return false;
                }
            }
            else {                                             //-是会
                //判断是否手牌全是绝牌，或是会牌
                var nCardDatacount = this.cbCardData.length;  //手牌张数
                var nHuiCount = 0;                  //会牌或绝牌张数
                for (i = 0; i < nCardDatacount; i++) {
                    for (j = 0; j < this.cbJueCardData.length; j++) {
                        if (this.cbCardData[i] == this.cbJueCardData[j]) { //判断每张牌，手牌中是否全是绝牌
                            nHuiCount = nHuiCount + 1;
                            break;
                        }
                    }
                    if (this.cbCardData[i] == GameLogic.MAGIC_DATA) {
                        nHuiCount = nHuiCount + 1;
                    }
                }
                if (nHuiCount >= nCardDatacount) { //手里全是会牌或绝牌
                    return true;
                }
                else                                 //手里不全是会牌或绝牌
                    return false;
            }
        }
        else if (ccmd.EXISTENCE_HUI == true && ccmd.EXISTENCE_JUE == false) {   //是会模式，不是绝模式
            if (card != GameLogic.MAGIC_DATA) {       //不是会牌
                return true;
            }
            else {                                       //是会牌

                var nCardDatacount = this.cbCardData.length;  //手牌张数
                var nHuiCount = 0;                      //会牌张数
                for (i = 0; i < nCardDatacount; i++) {
                    if (this.cbCardData[i] == GameLogic.MAGIC_DATA) {
                        nHuiCount = nHuiCount + 1;
                    }
                }
                if (nHuiCount >= nCardDatacount) { //手里全是会牌
                    return true;
                }
                else                                 //手里不全是会牌
                    return false;
            }
        }

        else if (ccmd.EXISTENCE_HUI == false && ccmd.EXISTENCE_JUE == true) {   //不是会模式，是绝模式
            var num = this.cbJueCardData.length;
            var nCount = 0;
            for (i = 0; i < num; i++) {
                if (this.cbJueCardData[i] == card) {
                    break;
                }
                nCount = nCount + 1;
            }
            if (nCount >= num) {   //出牌不是绝牌
                return true;
            }
            else {                    //判断是否手牌全是绝牌
                var nCardDatacount = this.cbCardData.length;  //手牌张数
                var nHuiCount = 0;                      //绝牌张数
                for (i = 0; i < nCardDatacount; i++) {
                    for (j = 0; j < this.cbJueCardData.length; j++) {
                        if (this.cbCardData[i] == this.cbJueCardData[j]) { //判断每张牌，手牌中是否是绝牌
                            nHuiCount = nHuiCount + 1;
                            break;
                        }
                    }
                }
                if (nHuiCount >= nCardDatacount) { //手里全是绝牌
                    return true;
                }
                else                                 //手里不全是绝牌
                    return false;
            }
        }
        else if (ccmd.EXISTENCE_HUI == false && ccmd.EXISTENCE_JUE == false) {  //不是会模式，不是绝模式
            return true;
        }
        return false;
    },

    setJueCard : function(cbCardData) {
        var num = this.cbJueCardData.length;
        this.cbJueCardData[num] = cbCardData;
        this.QuanHuiCard();
    },

    QuanHuiCard : function() {
        var quanHui = [];
        var num = 1;
        if (ccmd.EXISTENCE_HUI == true) {
            quanHui[num] = GameLogic.MAGIC_DATA;
            num = num + 1;
        }
        if (ccmd.EXISTENCE_JUE == true) {
            for (i = 0; i < this.cbJueCardData.length; i++) {
                quanHui[num - 1] = this.cbJueCardData[i];
                num = num + 1;
            }
        }
        if (quanHui.length() != 0) {
            GameLogic.setHuiCard(quanHui);
        }
    },
    //-**************< ysy_end   >**************//
    //检查碰、杠牌里是否有这张牌
    checkBumpOrBridgeCard : function(viewId, cbCardData) {
        var card = this.nodeBpBgCard[viewId].getChildByTag(cbCardData);
        if (card) {
            return true;
        }
        else
            return false;
    },

    getBpBgCardData : function() {
        return this.cbBpBgCardData;
    },

    gameEnded : function() {
        this.bSendOutCardEnabled = false;
    },

    switchToCardRectX : function(data) {
        appdf.assert(data, "this card is null");
        var cardIndex = GameLogic.SwitchToCardIndex(data);
        var rectX = cardIndex == GameLogic.MAGIC_INDEX && 32 || cardIndex;
        return rectX;
    },
    //使部分牌变灰（参数为不变灰的）
    makeCardGray : function(cbOutCardData) {
        //appdf.assert(#this.cbListenList > 0 && math_mod(#this.cbCardData - 2, 3) == 0)
        var cbCardCount = this.cbCardData.length;
        //先全部变灰
        for (i = ccmd.MAX_COUNT - 1; i >= 0; i--) {
            var card = this.nodeHandCard[ccmd.MY_VIEWID].getChildByTag(i);
            card.setColor(cc.color(100, 100, 100));
            this.bCardGray[i] = true;
        }
        //将牌补满(ui与值的对齐方式)
        var nCount = 0;
        var num = math_mod(cbCardCount, 3);
        if (num == 2) {
            nCount = cbCardCount;
        }
        else if (num == 1) {
            nCount = cbCardCount + 1;
        }
        else
            appdf.assert(false);
        //再恢复可打出的牌
        for (i = 0; i < cbOutCardData.length; i++) {
            for (j = 0; j < nCount; j++) {
                if (cbOutCardData[i] == this.cbCardData[j]) {
                    //appdf.assert(cbOutCardData[i] != GameLogic.MAGIC_DATA, "The magic card can't out!")
                    var nTag = nCount - j + 1;
                    var card = this.nodeHandCard[ccmd.MY_VIEWID].getChildByTag(nTag);
                    card.setColor(cc.color(255, 255, 255));
                    this.bCardGray[nTag] = false;
                    break;
                }
            }
        }
    },

    promptListeningCard : function(cardData) {
        //appdf.assert(#this.cbListenList > 0)

        if (null == cardData) {
            appdf.assert(this.currentOutCard > 0);
            cardData = this.currentOutCard;
        }

        for (i = 0; i < this.cbListenList.length; i++) {
            if (this.cbListenList[i].cbOutCard == cardData) {
                this._scene.setListeningCard(this.cbListenList[i].cbListenCard);
                break;
            }
        }
    },

    startListenCard : function() {
        for (var i = 0; i < ccmd.MAX_COUNT; i++) {
            var card = this.nodeHandCard[ccmd.MY_VIEWID].getChildByTag(i);
            if (i == 0) {
                card.setColor(cc.color(255, 255, 255));
                this.bCardGray[i] = false;
            }
            else {
                card.setColor(cc.color(100, 100, 100));
                this.bCardGray[i] = true;
            }
        }
    },

    promptListenOutCard : function(cbPromptOutData) {
        //还原
        var cbCardCount = this.cbCardData.length;
        for (var i = 0; i < ccmd.MAX_COUNT; i++) {
            var card = this.nodeHandCard[ccmd.MY_VIEWID].getChildByTag(i);
            card.getChildByTag(CardLayer.TAG_LISTEN_FLAG).setVisible(false);
        }
        if (cbPromptOutData == null) {
            return;
        }
        //校验
        appdf.assert(cbPromptOutData == null);
        appdf.assert(math_mod(cbCardCount - 2, 3) == 0, "You can't out card now!");
        //判断是否有会
        if (ccmd.EXISTENCE_HUI == true) {
            for (i = 0; i < cbPromptOutData.length; i++) {
                if (cbPromptOutData[i] != GameLogic.MAGIC_DATA) {
                    for (var j = 0; j < cbCardCount; j++) {
                        if (cbPromptOutData[i] == this.cbCardData[j]) {
                            var nTag = cbCardCount - j + 1;
                            var card = this.nodeHandCard[ccmd.MY_VIEWID].getChildByTag(nTag);
                            //card.getChildByTag(CardLayer.TAG_LISTEN_FLAG).setVisible(true)
                            card.getChildByTag(CardLayer.TAG_LISTEN_FLAG).setVisible(ccmd.SHOW_TING);
                        }
                    }
                }
            }
        }
    },

    setTableCardByHeapInfo : function(viewId, cbHeapCardInfo, wViewHead, wViewTail) {
        var nViewMinTag = (ccmd.GAME_PLAYER - viewId) * 28 + 1;
        var nViewMaxTag = (ccmd.GAME_PLAYER - viewId) * 28 + 28;
        //从右数隐藏几张牌
        var nTagStart1 = nViewMinTag;
        var nTagEnd1 = nViewMinTag + cbHeapCardInfo[0] - 1;
        for (i = nTagStart1 - 1; i < nTagEnd1; i++) {
            //this.nodeTableCard.getChildByTag(i).setVisible(false)
        }
        //从左数隐藏几张牌
        var nTagStart2 = nViewMaxTag - cbHeapCardInfo[0] + 1;
        var nTagEnd2 = nViewMaxTag;
        for (i = nTagStart2 - 1; i < nTagEnd2; i++) {
            //this.nodeTableCard.getChildByTag(i).setVisible(false)
        }

        if (viewId == wViewHead) {
            this.currentTag = (ccmd.GAME_PLAYER - viewId) * 28 + cbHeapCardInfo[0] + 1;
        }
        else if (viewId == wViewTail) {
            this.nTableTailCardTag = (ccmd.GAME_PLAYER - viewId) * 28 + (28 - cbHeapCardInfo[0]);
        }
        //牌堆总共剩余多少牌
        this.nRemainCardNum = this.nRemainCardNum - cbHeapCardInfo[0] - cbHeapCardInfo[1];
        this._scene.setRemainCardNum(this.nRemainCardNum);
    },

    touchSendOutCard : function(cbCardData) {
        if (!this.bSendOutCardEnabled) {
            return false;
        }
        this.currentOutCard = cbCardData;
        this._scene.setListeningCard(null);
        //发送消息
        return this._scene._scene.sendOutCard(cbCardData);
    },

    outCardAuto : function() {
        var nCount = this.cbCardData.length;
        if (math_mod(nCount - 2, 3) != 0) {
            return false;
        }
        var nHuiCount = 0;    //会牌计数
        for (var i = nCount - 1; i >= 0; i--) {
            var nTag = nCount - i + 1;
            var bOk = !this.bCardGray[nTag];
            //if (this.cbCardData[i] != GameLogic.MAGIC_DATA && bOk){
            //	this.touchSendOutCard(this.cbCardData[i])
            //	break
            //end
            //判断是否是托管
            //if (bOk){
            //    //如果有会
            //    if (ccmd.EXISTENCE_HUI == true ){
            //        //判断是否是会牌，会牌不可以出
            //        if (this.cbCardData[i] != GameLogic.MAGIC_DATA){
            //             this.touchSendOutCard(this.cbCardData[i])
            //	         break
            //        else
            //             nHuiCount = nHuiCount + 1
            //        end 
            //        //如果手中牌数等于会牌数（手中全是会），会牌允许出
            //        if  nHuiCount == nCount){ 
            //             this.touchSendOutCard(this.cbCardData[nHuiCount])
            //        end 
            //
            //    else
            //        this.touchSendOutCard(this.cbCardData[i])
            //    end 
            //end 
            if (bOk) {
                if (this.AnalysisOutCard(this.cbCardData[i]) == true) {
                    this.touchSendOutCard(this.cbCardData[i]);
                }
            }

        }

        return true;
    },
    //检查手里的杠
    getGangCard : function(data) {
        var cbCardCount = this.cbCardData.length;
        var num = 0;
        if (math_mod(cbCardCount, 3) == 2) {
            num = 4;
        }
        else if (math_mod(cbCardCount, 3) == 1) {
            num = 3;
        }
        else
            appdf.assert(false, "This is a not bridge card time!");

        var cbCardIndex = GameLogic.DataToCardIndex(this.cbCardData);
        if (data && data > 0) {
            var index = GameLogic.SwitchToCardIndex(data);
            if (cbCardIndex[index] == num) {
                return data;
            }
        }
        for (i = 0; i < GameLogic.NORMAL_INDEX_MAX; i++) {
            var dataTemp = GameLogic.SwitchToCardData(i);
            if (cbCardIndex[i] == num) {
                data = dataTemp;
            }
        }
        return data;
    },
    //<===============<QTC_MODIFY>================//
    //检查手里的杠是否为多方案 是多方案时返回方案数组 不是多方案时返回null
    getGangCardPlan : function(cbTybe,cbValue) {
        var planCode = [];
        var planCard = [];
        var planCount;
        var cbCardCount = this.cbCardData.length;
        var num = 0;
        if (math_mod(cbCardCount, 3) == 2) {
            num = 4;
        }
        else
            return [null, null];

        if (cbTybe == GameLogic.WIK_WALLTOP_O) {
            planCount = 0;
            planCard[planCount] = [cbValue, cbValue, cbValue];
            planCount = planCount + 1;
        }
        if (cbTybe == GameLogic.WIK_WALLTOP_S) {
            planCount = 0;
            planCard[planCount] = [cbValue, cbValue, cbValue];
            planCount = planCount + 1;
        }
        if (cbTybe == GameLogic.WIK_ARROW) {
            planCount = 0;
            planCard[planCount] = [0x35, 0x36, 0x37];
            planCount = planCount + 1;
        }
        if (cbTybe == GameLogic.WIK_WIND) {
            planCount = 0;
            planCard[planCount] = [0x31, 0x32, 0x33, 0x34];
            planCount = planCount + 1;
        }
//-<//////////////////////////<WW_ZhuiFengGang>//////////////////////////////-
        if (cbTybe == GameLogic.WIK_BIRD) {
            planCount = 0;
            planCard[planCount] = [0x11, 0x35, 0x36, 0x37];
            planCount = planCount + 1;
        }
        if (cbTybe == GameLogic.WIK_CHASEWIND) {
            var cbCardIndex = GameLogic.DataToCardIndex(this.cbCardData);
            planCount = 0;
            for (i = 27; i < 31; i++) {
                var data = GameLogic.SwitchToCardData(i);
                if (cbCardIndex[i] >= 1) {
                    planCode[planCount] = GameLogic.WIK_CHASEWIND;
                    planCard[planCount] = [data];
                    planCount = planCount + 1;
                }
            }
        }
        if (cbTybe == GameLogic.WIK_CHASEARROW) {
            var cbCardIndex = GameLogic.DataToCardIndex(this.cbCardData);
            planCount = 0;
            for (i = 31 ; i < 34; i++) {
                var data = GameLogic.SwitchToCardData(i);
                if (cbCardIndex[i] >= 1) {
                    planCode[planCount] = GameLogic.WIK_CHASEARROW;
                    planCard[planCount] = [data];
                    planCount = planCount + 1;
                }
            }
        }
        if (cbTybe == GameLogic.WIK_CHASEBIRD) {
            var cbCardIndex = GameLogic.DataToCardIndex(this.cbCardData);
            planCount = 0;
            if (cbCardIndex[9] >= 1) {
                planCode[planCount] = GameLogic.WIK_CHASEBIRD;
                planCard[planCount] = [0x11];
                planCount = planCount + 1;
            }
            for (i = 31; i < 34; i++) {
                var data = GameLogic.SwitchToCardData(i);
                if (cbCardIndex[i] >= 1) {
                    planCode[planCount] = GameLogic.WIK_CHASEBIRD;
                    planCard[planCount] = [data];
                    planCount = planCount + 1;
                }
            }
        }
//->//////////////////////////-<WW_ZhuiFengGang>//////////////////////////////-
        if (cbTybe == GameLogic.WIK_GANG) {
            var cbCardIndex = GameLogic.DataToCardIndex(this.cbCardData);
            planCount = 0;
            for (var i = 0; i < GameLogic.NORMAL_INDEX_MAX; i++) {
                var data = GameLogic.SwitchToCardData(i);
                if (cbCardIndex[i] == num) {
                    planCode[planCount] = GameLogic.WIK_GANG;
                    planCard[planCount] = [data, data, data];
                    planCount = planCount + 1;
                }
            }
            for (i = 0; i < this.cbCardData.length; i++) {
                if (this.checkBumpOrBridgeCard(2, this.cbCardData[i]) == true) {
                    planCode[planCount] = GameLogic.WIK_GANG;
                    planCard[planCount] = [this.cbCardData[i]];
                    planCount = planCount + 1;
                }
            }
        }
        if (planCount > 0) {
            return [planCode, planCard];
        }
        return [null, null];
    },
    //>===============<QTC_MODIFY>================//

    //用户必须点胡牌
    isUserMustWin : function() {
        // var cbCardCount = #this.cbCardData
        // var cbCardDataTemp = clone(this.cbCardData)
        // GameLogic.SortCardList(cbCardDataTemp)
        // //有普通牌
        // var cbNormalCardTypeCount = 0
        // var cbNormalCardTemp = 0
        // for (i = 0 ; i <  cbCardCount; i++ ){
        // 	if (cbCardDataTemp[i] == GameLogic.MAGIC_DATA){
        // 		break
        // 	end
        // 	if (cbNormalCardTemp != cbCardDataTemp[i]){
        // 		cbNormalCardTypeCount = cbNormalCardTypeCount + 1
        // 	end
        // 	cbNormalCardTemp = cbCardDataTemp[i]
        // end
        if (this.cbCardData.length == 2) {
            if (this.cbCardData[0] == this.cbCardData[1] &&
                this.cbCardData[1] == GameLogic.MAGIC_DATA) {
                return true;
            }
        }
        return false;
    },
    //用户可以碰
    isUserCanBump : function() {
        if (this.cbCardData.length == 4) {
            if (this.cbCardData[0] == this.cbCardData[1] &&
                this.cbCardData[2] == this.cbCardData[3] &&
                this.cbCardData[3] == GameLogic.MAGIC_DATA) {
                return false;
            }
        }
        return true;
    }
});