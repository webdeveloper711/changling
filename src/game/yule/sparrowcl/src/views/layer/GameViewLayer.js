// var GameBackLayer = cc.Layer.extend({
// 	ctor : function(){
// 	    this._super();
//         var layer = ccs.load(res.GameScene_json);
//
// 	}
// });

//

var anchorPointHead = [cc.p(1, 1), cc.p(0, 0.5), cc.p(0, 0), cc.p(1, 0.5)];
var posHead = [cc.p(577, 295), cc.p(165, 332), cc.p(166, 257), cc.p(724, 273)];
var posReady = [cc.p(198,13),cc.p(162,6),cc.p(338,-1),cc.p(-162,1)];
var posPlate = [cc.p(667, 589), cc.p(237, 464), cc.p(667, 174), cc.p(1093, 455)];
var posChat = [cc.p(473+50, 660+20), cc.p(229+21, 558-60), cc.p(270-20, 285+30), cc.p(1095, 528)];

var GameViewLayer = cc.Layer.extend({
    ctor: function (scene) {
        this._super();
        cc.spriteFrameCache.addSpriteFrames(res.gameScene_plist);
        var self = this;

        var bg = new cc.Sprite(ccmd.RES_PATH + "game/background_sparrowHz.png");
        bg.setAnchorPoint(0, 0);
        this.addChild(bg);

        this._cardLayer = new CardLayer(this);
        this.addChild(this._cardLayer);							//牌图层

        this._scene = scene;
        this.layer = ccs.load(res.GameScene_json).node;

        this.addChild(this.layer);
        this.onInitData();
        this.preloadUI();
        this.initButtons();

        this._resultLayer = new ResultLayer(this);
        this.addChild(this._resultLayer);
        this._resultLayer.setVisible(false);					//结算框

        //TODO
       this._chatLayer = new GameChatLayer(this._scene._gameFrame);
       this.addChild(this._chatLayer, 4);	//聊天框

        this._setLayer = new SetLayer(this);
        this.addChild(this._setLayer, 4);
        //ysy_start
        this._cardLayerGang = new CardLayer(this);
        this.addChild(this._cardLayerGang, 5);							//吃杠牌图层
        //牌库
        //TODO
        // this._textCardLayer = this.CardTest();
        // this.addChild(this._textCardLayer, 5);
        // this.CardTestInit();
        //玩法
        this._playRule = this.layer.getChildByTag(2000);
        this._playRule.setVisible(false);
        this.m_spPlayRule = [];
        for (i = 0; i < 3; i++) {
            this.m_spPlayRule[i] = null;
        }
        // 用户信息
        this.m_tabUserItem = [];
        //ysy_end
        //聊天泡泡
        this.chatBubble = [];
        for (i = 0; i < ccmd.GAME_PLAYER; i++) {
            var strFile = "";
            if (i == 3) {
                // if (i == 1 || i == 4) {
                strFile = "sp_bubble_2.png";
            }
            else {
                strFile = "sp_bubble_1.png";
            }
            var frame = cc.spriteFrameCache.getSpriteFrame(strFile);
            this.chatBubble[i] = new cc.Sprite(frame, [scale9 = true, capInsets = cc.rect(0, 0, 204, 68)]);
            this.chatBubble[i].setAnchorPoint(cc.p(0.5, 0.5));
            this.chatBubble[i].setPosition(posChat[i]);
            this.chatBubble[i].setVisible(false);
            this.addChild(this.chatBubble[i], 3);
        }

        //节点事件
        // var onNodeEvent(event) {
        //     if (event == "exit") {
        //         self.onExit();
        //     }
        // }

        //this.registerScriptHandler(onNodeEvent);

        this.nodePlayer = [];
        for (i = 0; i < ccmd.GAME_PLAYER; i++) {
            this.nodePlayer[i] = this.layer.getChildByTag(GameViewLayer.NODEPLAYER_1 + i);
            this.nodePlayer[i].setLocalZOrder(1);
            this.nodePlayer[i].setVisible(false);
            this.nodePlayer[i].getChildByTag(GameViewLayer.SP_HEADCOVER).setLocalZOrder(1);
            this.nodePlayer[i].getChildByTag(GameViewLayer.TEXT_NICKNAME).setLocalZOrder(1);
            var flagReady = this.nodePlayer[i].getChildByTag(GameViewLayer.SP_READY);
            flagReady.setPosition(posReady[i]);
            this.nodePlayer[i].setLocalZOrder(1);
            var sp_trustee = this.nodePlayer[i].getChildByTag(GameViewLayer.SP_TRUSTEE);
            sp_trustee.setVisible(false);
            var sp_banker = this.nodePlayer[i].getChildByTag(GameViewLayer.SP_BANKER);
            sp_banker.setLocalZOrder(1);
            sp_banker.setVisible(false);
            var sp_roomHost = this.nodePlayer[i].getChildByTag(GameViewLayer.SP_ROOMHOST);
            sp_roomHost.setVisible(false);
            this.nodePlayer[i].getChildByName("sp_scoreFlag").setVisible(false);

            if (i == 1 || i == ccmd.MY_VIEWID) {
                //sp_trustee.setPosition(65, -41)
                //sp_banker.setPosition(44, 55)
                //sp_roomHost.setPosition(-79, -24)
            }
        }

        this.spListenBg = this.layer.getChildByTag(GameViewLayer.SP_LISTEN);
        this.spListenBg.setLocalZOrder(3);
        this.spListenBg.setVisible(false);
        this.spListenBg.setScale(0.7);
        //庄家
        var gv = this.layer.getChildByTag(GameViewLayer.SP_ANNOUNCEMENT);
        gv.setLocalZOrder(2);
        gv.setVisible(false);
        //托管覆盖层
        this.spTrusteeCover = new cc.Layer();
        this.spTrusteeCover.setVisible(false);
        this.addChild(this.spTrusteeCover, 4);
        //阴影层
        var sp1 = new cc.Sprite(ccmd.RES_PATH + "game/sp_trusteeCover.png");
        sp1.setPosition(667, 112);
        sp1.setScaleY(1.6);
        sp1.setTag(GameViewLayer.SP_TRUSTEEBG);
        this.spTrusteeCover.addChild(sp1);
        //取消托管按钮
        // this.btTrusteeCancel = new ccui.Button("bt_trusteeCancel_1.png", "bt_trusteeCancel_2.png", "bt_trusteeCancel_1.png", ccui.TextureResType.plistType);
        // this.btTrusteeCancel.setPosition(667, 108);
        // this.btTrusteeCancel.setTag(GameViewLayer.BT_TRUSTEECANCEL);
        // this.spTrusteeCover.addChild(this.btTrusteeCancel, 1);
        // this.btTrusteeCancel.addTouchEventListener(function (ref, eventType) {
        //     if (eventType == ccui.Widget.TOUCH_ENDED) {
        //         self.onButtonClickedEvent(ref.getTag(), ref);
        //     }
        // });
        var sp2 = new cc.Sprite(ccmd.RES_PATH + "game/sp_trusteeMan.png");
        sp2.setPosition(1067, 108);
        this.spTrusteeCover.addChild(sp2);

        //this.spTrusteeCover.setTouchEnabled(true);
        // this.spTrusteeCover.registerScriptTouchHandler(function (eventType, x, y) {
        //     return this.onTrusteeTouchCallback(eventType, x, y)
        // });
        //牌盘
        this.spCardPlate = this.layer.getChildByTag(GameViewLayer.SP_PLATE);
        this.spCardPlate.setLocalZOrder(3);
        this.spCardPlate.setVisible(false);

        var sp3 = new cc.Sprite(ccmd.RES_PATH + "game/font_middle/card_down.png");
        sp3.setPosition(61, 74);
        //.setTag(this.SP_PLATECARD)
        //.setTextureRect(cc.rect(0, 0, 69, 107))
        this.spCardPlate.addChild(sp3);

        var sp4 = new cc.Sprite(ccmd.RES_PATH + "game/font_middle/font_3_5.png");
        sp4.setPosition(61, 82);
        sp4.setTag(GameViewLayer.SP_PLATECARD);
        this.spCardPlate.addChild(sp4);

        this.spClockBg = this.layer.getChildByTag(GameViewLayer.SP_CLOCK_BG);
        this.spClock = this.layer.getChildByTag(GameViewLayer.SP_CLOCK);
        this.asLabTime = this.spClock.getChildByTag(GameViewLayer.ASLAB_TIME);
        this.asLabTime.setString("0");
        //////////////-ysy_start//////////////////////-
        this.m_bGangGroup = false;   //false - 创建杠牌
        this.m_bChiGroup = false;   //false - 创建吃牌
        //等待其它玩家操作字
        this.textWaitOther = this.layer.getChildByTag(802);
        this.textWaitOther.setLocalZOrder(10);
        this.textWaitOther.setVisible(false);
        //房间号
        this.textRoomID = this.layer.getChildByTag(899);
        this.textRoomID.setLocalZOrder(10);
        this.textRoomID.setVisible(false);
        if (PriRoom.getInstance().m_tabPriData.szServerID != null) {
            this.textRoomID.setString("房间号：" + PriRoom.getInstance().m_tabPriData.szServerID);
            this.textRoomID.setVisible(true);
        }
        //会牌背景
        var spHuiBg = this.layer.getChildByName("sp_huiBg");
        spHuiBg.setVisible(false);
        // 圈数
        this.m_txtGameTimes = this.layer.getChildByName("Text_ju_num");
        //////////////ysy_end//////////////////////////

    },
    //<===============<QTC_MODIFY>================//
    __resetPlanData: function () {
        this.cbChiPlanCode = [];		//吃的方案表 存储的是cbOperateCode
        this.cbChiPlanCard = [];		//吃的方案表 存储的是cbOperateCard
        this.cbGangPlanCode = [];	//自杠的方案表 存储的是cbOperateCode
        this.cbGangPlanCard = [];	//自杠的方案表 存储的是cbOperateCard
    },
    //显示吃的二级按钮
    __showChiSub: function () {
        this.__hideGangSub();

        this.__showPlanCard(this.cbChiPlanCard[0], this.cbChiPlanCard[1], this.cbChiPlanCard[2], null);
        if (this.cbChiPlanCard[0]) {
            var bt1 = this.spGameBtn.getChildByTag(GameViewLayer.BT_EAT_SUB_1);
            bt1.setEnabled(true);
            bt1.setColor(cc.color(255, 255, 255));
        }
        if (this.cbChiPlanCard[1]) {
            var bt2 = this.spGameBtn.getChildByTag(GameViewLayer.BT_EAT_SUB_2);
            bt2.setEnabled(true);
            bt2.setColor(cc.color(255, 255, 255));
        }
        if (this.cbChiPlanCard[2]) {
            var bt3 = this.spGameBtn.getChildByTag(GameViewLayer.BT_EAT_SUB_3);
            bt3.setEnabled(true);
            bt3.setColor(cc.color(255, 255, 255));
        }
    },
    //隐藏吃的二级按钮
    __hideChiSub: function () {
        this.__hidePlanCard();
        var bt1 = this.spGameBtn.getChildByTag(GameViewLayer.BT_EAT_SUB_1);
        bt1.setEnabled(false);
        bt1.setVisible(false);
        var bt2 = this.spGameBtn.getChildByTag(GameViewLayer.BT_EAT_SUB_2);
        bt2.setEnabled(false);
        bt2.setVisible(false);
        var bt3 = this.spGameBtn.getChildByTag(GameViewLayer.BT_EAT_SUB_3);
        bt3.setEnabled(false);
        bt3.setVisible(false);
    },
    //显示杠的二级按钮
    __showGangSub: function () {
        this.__hideChiSub();

        this.__showPlanCard(this.cbGangPlanCard[0], this.cbGangPlanCard[1], this.cbGangPlanCard[2], this.cbGangPlanCard[3]);
        if (this.cbGangPlanCard[0]) {
            var bt1 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE_SUB_1);
            bt1.setEnabled(true);
            bt1.setColor(cc.color(255, 255, 255));
        }
        if (this.cbGangPlanCard[1]) {
            var bt2 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE_SUB_2);
            bt2.setEnabled(true);
            bt2.setColor(cc.color(255, 255, 255));
        }
        if (this.cbGangPlanCard[2]) {
            var bt3 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE_SUB_3);
            bt3.setEnabled(true);
            bt3.setColor(cc.color(255, 255, 255));
        }
        if (this.cbGangPlanCard[3]) {
            var bt4 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE_SUB_4);
            bt4.setEnabled(true);
            bt4.setColor(cc.color(255, 255, 255));
        }

    },
    //隐藏杠的二级按钮
    __hideGangSub: function () {
        this.__hidePlanCard();
        var btn1 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE_SUB_1);
        btn1.setEnabled(false);
        btn1.setVisible(false);
        var btn2 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE_SUB_2);
        btn2.setEnabled(false);
        btn2.setVisible(false);
        var btn3 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE_SUB_3);
        btn3.setEnabled(false);
        btn3.setVisible(false);
        var btn4 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE_SUB_4);
        btn4.setEnabled(false);
        btn4.setVisible(false);
    },
    //////////////-***********<ysy_start>******////////////////////
    //显示杠的二级按钮
    __showGangSub2: function () {
        this.__showPlanCard(this.cbGangPlanCard[0], this.cbGangPlanCard[1], this.cbGangPlanCard[2], this.cbGangPlanCard[3]);
        if (this.cbGangPlanCard[0]) {
            var btn1 = this.spGameBtnGang.getChildByTag(GameViewLayer.BT_BRIGDE_SUB_1);
            btn1.setEnabled(true);
            btn1.setVisible(true);
            btn1.setColor(cc.color(255, 255, 255));
        }
        if (this.cbGangPlanCard[1]) {
            var btn2 = this.spGameBtnGang.getChildByTag(GameViewLayer.BT_BRIGDE_SUB_2);
            btn2.setEnabled(true);
            btn2.setVisible(true);
            btn2.setColor(cc.color(255, 255, 255));
        }
        if (this.cbGangPlanCard[2]) {
            var btn3 = this.spGameBtnGang.getChildByTag(GameViewLayer.BT_BRIGDE_SUB_3);
            btn3.setEnabled(true);
            btn3.setVisible(true);
            btn3.setColor(cc.color(255, 255, 255));
        }
        if (this.cbGangPlanCard[3]) {
            var btn4 = this.spGameBtnGang.getChildByTag(GameViewLayer.BT_BRIGDE_SUB_4);
            btn4.setEnabled(true);
            btn4.setVisible(true);
            btn4.setColor(cc.color(255, 255, 255));
        }
    },
    //隐藏杠的二级按钮
    __hideGangSub2: function () {
        //this.__hidePlanCard()
        this.spGameBtnGang.setVisible(false);
        var btn1 = this.spGameBtnGang.getChildByTag(GameViewLayer.BT_BRIGDE_SUB_1);
        btn1.setEnabled(false);
        btn1.setVisible(false);
        var btn2 = this.spGameBtnGang.getChildByTag(GameViewLayer.BT_BRIGDE_SUB_2);
        btn2.setEnabled(false);
        btn2.setVisible(false);
        var btn3 = this.spGameBtnGang.getChildByTag(GameViewLayer.BT_BRIGDE_SUB_3);
        btn3.setEnabled(false);
        btn3.setVisible(false);
        var btn4 = this.spGameBtnGang.getChildByTag(GameViewLayer.BT_BRIGDE_SUB_4);
        btn4.setEnabled(false);
        btn4.setVisible(false);
    },
    //显示吃的二级按钮
    __showChiSub2: function () {
        this.__showPlanCard(this.cbChiPlanCard[0], this.cbChiPlanCard[1], this.cbChiPlanCard[2], null);
        if (this.cbChiPlanCard[0]) {
            var btn1 = this.spGameBtnChi.getChildByTag(GameViewLayer.BT_EAT_SUB_1);
            btn1.setEnabled(true);
            btn1.setVisible(true);
            btn1.setColor(cc.color(255, 255, 255));
        }
        if (this.cbChiPlanCard[1]) {
            var btn2 = this.spGameBtnChi.getChildByTag(GameViewLayer.BT_EAT_SUB_2);
            btn2.setEnabled(true);
            btn2.setVisible(true);
            btn2.setColor(cc.color(255, 255, 255));
        }
        if (this.cbChiPlanCard[2]) {
            var btn3 = this.spGameBtnChi.getChildByTag(GameViewLayer.BT_EAT_SUB_3);
            btn3.setEnabled(true);
            btn3.setVisible(true);
            btn3.setColor(cc.color(255, 255, 255));
        }
    },
    //隐藏吃的二级按钮
    __hideChiSub2: function () {
        this.spGameBtnChi.setVisible(false);
        var btn1 = this.spGameBtnChi.getChildByTag(GameViewLayer.BT_EAT_SUB_1);
        btn1.setEnabled(false);
        btn1.setVisible(false);
        var btn2 = this.spGameBtnChi.getChildByTag(GameViewLayer.BT_EAT_SUB_2);
        btn2.setEnabled(false);
        btn2.setVisible(false);
        var btn3 = this.spGameBtnChi.getChildByTag(GameViewLayer.BT_EAT_SUB_3);
        btn3.setEnabled(false);
        btn3.setVisible(false);
    },
    //分析会牌
    AnalysisHuiCard: function (cardData) {
        if (cardData == 0x09) {
            return 0x01;
        }
        else if (cardData == 0x19) {
            return 0x11;
        }
        else if (cardData == 0x29) {
            return 0x21;
        }
        else if (cardData == 0x34) {
            return 0x31;
        }
        else if (cardData == 0x37) {
            return 0x35;
        }
        else
            return cardData + 1;
    },
    //////////////-***********<ysy_end  >******////////////////////
    //显示二级菜单中的牌面
    __showPlanCard: function (planCard1, planCard2, planCard3, planCard4) {
    },
    //隐藏二级菜单中的牌面
    __hidePlanCard: function () {
    },
    //>===============<QTC_MODIFY>================//
    onInitData: function () {
        this.cbActionCard = 0;
        this.cbOutCardTemp = 0;
        this.chatDetails = [];
        this.cbAppearCardIndex = [];
        this.m_bNormalState = [];
//<===============<QTC_MODIFY>================//
        this.cbChiPlanCode = [];		//吃的方案表 存储的是cbOperateCode
        this.cbChiPlanCard = [];	//吃的方案表 存储的是cbOperateCard
        this.cbGangPlanCode = [];	//自杠的方案表 存储的是cbOperateCode
        this.cbGangPlanCard = [];	//自杠的方案表 存储的是cbOperateCard
//>===============<QTC_MODIFY>================//
        //房卡需要
        this.m_sparrowUserItem = [];
    },

    onResetData: function () {
        this._cardLayer.onResetData();

        this.spListenBg.removeAllChildren();
        this.spListenBg.setVisible(false);
        this.cbOutCardTemp = 0;
        this.cbAppearCardIndex = [];
        var spFlag = this.layer.getChildByTag(GameViewLayer.SP_OPERATFLAG);
        if (spFlag) {
            spFlag.removeFromParent();
        }
        this.spCardPlate.setVisible(false);
        this.spTrusteeCover.setVisible(false);
        for (i = 0; i < ccmd.GAME_PLAYER; i++) {
            var node1 = this.nodePlayer[i].getChildByTag(GameViewLayer.SP_TRUSTEE);
            node1.setVisible(false);
            var node2 = this.nodePlayer[i].getChildByTag(GameViewLayer.SP_BANKER);
            node2.setVisible(false);
        }
        this.setRemainCardNum(136);
        var btn = this.spGameBtn.getChildByTag(GameViewLayer.BT_PASS);
        btn.setEnabled(true);
        btn.setColor(cc.color(255, 255, 255));
    },

    onExit: function () {
        cc.log("GameViewLayer onExit");
        this.m_tabUserItem = [];
        this._scene.KillGameClock();
        cc.spriteFrameCache.removeSpriteFramesFromFile(res.gameScene_plist);
       // cc.director.getTextureCache().removeTextureForKey("gameScene.png");
        cc.spriteFrameCache.removeSpriteFramesFromFile(res.chooseCard_plist);
      //  cc.director.getTextureCache().removeTextureForKey("chooseCard.png");
      //  cc.director.getTextureCache().removeUnusedTextures();
       // cc.spriteFrameCache.removeUnusedSpriteFrames();
      //  AnimationMgr.removeCachedAnimation(ccmd.VOICE_ANIMATION_KEY);
    },
    //////////////ysy_start////////////////////////
    setGameTimes: function (cbValue) {
        if (this._scene.cbTimes == null) {
            return;
        }
        strValue = cbValue + "/" + this._scene.cbTimes + "圈";
        this.m_txtGameTimes.setString(strValue);
    },
    showWaitOtherOperation: function (bVisible) {
        return;
    },
    //局号
    showTimes: function (cbValue) {
        var sValue = "局号：" + cbValue;
        this.textWaitOther.setString(sValue);
        this.textWaitOther.setVisible(true);
    },
    //////////////ysy_end//////////////////////////
    preloadUI: function () {
        cc.log("欢迎来到我的酒馆！");
        //导入动画
//        var animationCache = cc.animationCache();
        for (i = 0; i < 12; i++) {
            var strColor = "";
            var index = 0;
            if (i < 6) {
                strColor = "white";
                index = i;
            }
            else {
                strColor = "red";
                index = i - 6;
            }
            var animation = new cc.Animation();
            animation.setDelayPerUnit(0.1);
            animation.setLoops(1);
            for (var j = 0; j < 9; j++) {
                var strFile = ccmd.RES_PATH + "Animate_sice_" + strColor + sprintf("/sice_%d.png", index + 1);
                var spFrame = cc.SpriteFrame.create(strFile, cc.rect(133 * (j), 0, 133, 207));
                animation.addSpriteFrame(spFrame);
            }

            var strName = "sice_" + strColor + sprintf("_%d", index + 1);
       //     animationCache.addAnimation(animation, strName);
        }

        // 语音动画
    //    AnimationMgr.loadAnimationFromFrame("record_play_ani_%d.png", 1, 3, ccmd.VOICE_ANIMATION_KEY);
    },

    initButtons : function() {
        var self = this;
        //按钮回调
        var btnCallback = function (ref, eventType) {
            if (eventType == ccui.Widget.TOUCH_ENDED) {
                self.onButtonClickedEvent(ref.getTag(), ref)
            }
        };

        //桌子操作按钮屏蔽层
        var callbackShield = function (ref) {
            var pos = ref.getTouchEndPosition();
            var rectBg = self.spTableBtBg.getBoundingBox();
            if (cc.rectContainsPoint(rectBg, pos) != false) {
                self.showTableBt(false);
            }
        };
        this.layoutShield = new ccui.Layout();
        this.layoutShield.setContentSize(cc.size(1334, 750));
        this.layoutShield.setTouchEnabled(false);
        this.addChild(this.layoutShield, 5);
        this.layoutShield.addClickEventListener(callbackShield);
        //桌子操作按钮
        this.spTableBtBg = this.layer.getChildByTag(GameViewLayer.SP_TABLE_BT_BG);
        this.spTableBtBg.setLocalZOrder(1);
        this.spTableBtBg.setPosition(1800, 703);
        this.spTableBtBg.setVisible(false);
        var btSet = this.spTableBtBg.getChildByTag(GameViewLayer.BT_SET);
        //btSet.setSelected(!bAble)
        btSet.addTouchEventListener(btnCallback);
        var btChat = this.spTableBtBg.getChildByTag(GameViewLayer.BT_CHAT);	//聊天
        btChat.addTouchEventListener(btnCallback);
        var btExit = this.spTableBtBg.getChildByTag(GameViewLayer.BT_EXIT);	//退出
        btExit.addTouchEventListener(btnCallback);
        var btTrustee = this.spTableBtBg.getChildByTag(GameViewLayer.BT_TRUSTEE);	//托管
        btTrustee.addTouchEventListener(btnCallback);
        if (GlobalUserItem.bPrivateRoom) {
            btTrustee.setEnabled(false);
            btTrustee.setColor(cc.color(158, 112, 8));
        }
        var btHowPlay = this.spTableBtBg.getChildByTag(GameViewLayer.BT_HOWPLAY);	//玩法
        btHowPlay.addTouchEventListener(btnCallback);
        //ysy_start
        var btTextCardData = this.layer.getChildByTag(803);	//摆牌
        btTextCardData.setVisible(true);
        btTextCardData.addTouchEventListener(btnCallback);
        //ysy_end
        //桌子按钮开关
        this.btSwitch = this.layer.getChildByTag(GameViewLayer.BT_SWITCH);
        this.btSwitch.setLocalZOrder(2);
        this.btSwitch.addTouchEventListener(btnCallback);
        this.btSwitch.setVisible(false);
        //开始
        this.btStart = this.layer.getChildByTag(GameViewLayer.BT_START);
        this.btStart.setLocalZOrder(2);
        this.btStart.setVisible(false);
        this.btStart.addTouchEventListener(btnCallback);

        //游戏操作按钮
        this.spGameBtn = this.layer.getChildByTag(GameViewLayer.SP_GAMEBTN);
        this.spGameBtn.setLocalZOrder(10);
        this.spGameBtn.setVisible(false);
        this.spGameBtnChi = this.layer.getChildByTag(GameViewLayer.SP_GAMEBTN_CHI);
        this.spGameBtnChi.setLocalZOrder(3);
        this.spGameBtnChi.setVisible(false);
        this.spGameBtnGang = this.layer.getChildByTag(GameViewLayer.SP_GAMEBTN_GANG);
        this.spGameBtnGang.setLocalZOrder(3);
        this.spGameBtnGang.setVisible(false);
        //<===============<QTC_MODIFY>================//

        var btEat = this.spGameBtn.getChildByTag(GameViewLayer.BT_EAT); 	//吃
        btEat.setEnabled(false);
        btEat.setColor(cc.color(158, 112, 8));
        btEat.addTouchEventListener(btnCallback);

        var btEatGroup = this.spGameBtn.getChildByTag(GameViewLayer.BT_EAT_GROUP); //吃 多方案
        btEatGroup.setEnabled(false);
        btEatGroup.setColor(cc.color(158, 112, 8));
        btEatGroup.addTouchEventListener(btnCallback);

        var btEatSub1 = this.spGameBtnChi.getChildByTag(GameViewLayer.BT_EAT_SUB_1); 	//吃 方案1
        btEatSub1.setEnabled(false);
        btEatSub1.setColor(cc.color(158, 112, 8));
        btEatSub1.addTouchEventListener(btnCallback);

        var btEatSub2 = this.spGameBtnChi.getChildByTag(GameViewLayer.BT_EAT_SUB_2);	//吃 方案2
        btEatSub2.setEnabled(false);
        btEatSub2.setColor(cc.color(158, 112, 8));
        btEatSub2.addTouchEventListener(btnCallback);

        var btEatSub3 = this.spGameBtnChi.getChildByTag(GameViewLayer.BT_EAT_SUB_3); 	//吃 方案3
        btEatSub3.setEnabled(false);
        btEatSub3.setColor(cc.color(158, 112, 8));
        btEatSub3.addTouchEventListener(btnCallback);
        //>===============<QTC_MODIFY>================//
        var btBump = this.spGameBtn.getChildByTag(GameViewLayer.BT_BUMP);	//碰
        btBump.setEnabled(false);
        btBump.setColor(cc.color(158, 112, 8));
        btBump.addTouchEventListener(btnCallback);
        var btBrigde = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE);		//杠
        btBrigde.setEnabled(false);
        btBrigde.setColor(cc.color(158, 112, 8));
        btBrigde.addTouchEventListener(btnCallback);
        //<===============<QTC_MODIFY>================//
        var btBrigdeGroup = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE_GROUP); 		//杠 多方案
        btBrigdeGroup.setEnabled(false);
        btBrigdeGroup.setColor(cc.color(158, 112, 8));
        btBrigdeGroup.addTouchEventListener(btnCallback);

        var btBrigdeSub1 = this.spGameBtnGang.getChildByTag(GameViewLayer.BT_BRIGDE_SUB_1); 		//杠 方案1
        btBrigdeSub1.setEnabled(false);
        btBrigdeSub1.setColor(cc.color(158, 112, 8));
        btBrigdeSub1.addTouchEventListener(btnCallback);

        var btBrigdeSub2 = this.spGameBtnGang.getChildByTag(GameViewLayer.BT_BRIGDE_SUB_2); 		//杠 方案2
        btBrigdeSub2.setEnabled(false);
        btBrigdeSub2.setColor(cc.color(158, 112, 8));
        btBrigdeSub2.addTouchEventListener(btnCallback);

        var btBrigdeSub3 = this.spGameBtnGang.getChildByTag(GameViewLayer.BT_BRIGDE_SUB_3);		//杠 方案3
        btBrigdeSub3.setEnabled(false);
        btBrigdeSub3.setColor(cc.color(158, 112, 8));
        btBrigdeSub3.addTouchEventListener(btnCallback);

        var btBrigdeSub4 = this.spGameBtnGang.getChildByTag(GameViewLayer.BT_BRIGDE_SUB_4); 		//杠 方案4
        btBrigdeSub4.setEnabled(false);
        btBrigdeSub4.setColor(cc.color(158, 112, 8));
        btBrigdeSub4.addTouchEventListener(btnCallback);
        //>===============<QTC_MODIFY>================//
        var btWin = this.spGameBtn.getChildByTag(GameViewLayer.BT_WIN);		//胡
        btWin.setEnabled(false);
        btWin.setColor(cc.color(158, 112, 8));
        btWin.addTouchEventListener(btnCallback);
        var btPass = this.spGameBtn.getChildByTag(GameViewLayer.BT_PASS);		//过
        btPass.addTouchEventListener(btnCallback);

        //////-ysy——start//////
        //TODO
        // var btChatNew = this.layer.getChildByTag(GameViewLayer.BT_CHAT_NEW);	//聊天新
        // btChatNew.addTouchEventListener(btnCallback);

        //TODO
        // var btSetNew = this.layer.getChildByTag(GameViewLayer.BT_SET_NEW);   //设置新
        // btSetNew.addTouchEventListener(btnCallback);
        //////-ysy——end////////
         var btSetNew1 = this.layer.getChildByTag(GameViewLayer.BT_SET_NEW1);   //设置新1
         btSetNew1.addTouchEventListener(btnCallback);
         btSetNew1.setLocalZOrder(2);
        var btChatNew1 = this.layer.getChildByTag(GameViewLayer.BT_CHAT_NEW1);   //聊天新1
        btChatNew1.addTouchEventListener(btnCallback);
        btChatNew1.setLocalZOrder(2);

        //语音
        var btVoice = this.layer.getChildByTag(GameViewLayer.BT_VOICE);
        btVoice.setLocalZOrder(3);
        //btVoice.setVisible(false)
        btVoice.addTouchEventListener(function (ref, eventType) {
            if (eventType == ccui.ccui.Widget.TOUCH_ENDED) {
                self._scene._scene.startVoiceRecord();
            }
            else if (eventType == ccui.Widget.TOUCH_ENDED
                || eventType == ccui.Widget.TOUCH_CANCELED) {
                self._scene._scene.stopVoiceRecord();
            }
        });
    },

    showTableBt : function(bVisible) {
        var self = this;
        if (this.spTableBtBg.isVisible() == bVisible) {
            return false;
        }

        var fSpacing = 550;
        if (bVisible == true) {
            this.layoutShield.setTouchEnabled(true);
            this.btSwitch.setVisible(false);
            this.spTableBtBg.setVisible(true);
            this.spTableBtBg.runAction(new cc.MoveBy(0.3, cc.p(-fSpacing, 0)));
        }
        else {
            this.layoutShield.setTouchEnabled(false);
            this.showPlayRule(false);
            this._playRule.setVisible(false);
            this.spTableBtBg.runAction(new cc.Sequence(
                new cc.MoveBy(0.5, cc.p(fSpacing, 0)),
                new cc.CallFunc(function (ref) {
                    self.btSwitch.setVisible(true);
                    self.spTableBtBg.setVisible(false)
                })));
        }
        return true;
    },
	//更新用户显示
    OnUpdateUser : function(viewId, userItem) {
        cc.log("OnUpdateUser : viewId = " + viewId);
        if (viewId == null || viewId == yl.INVALID_CHAIR) {
            cc.log("OnUpdateUser viewId is null");
            return;
        }

        this.m_sparrowUserItem[viewId] = userItem;
        //头像
        var head = this.nodePlayer[viewId].getChildByTag(GameViewLayer.SP_HEAD);
        if (userItem == null) {
            this.nodePlayer[viewId].setVisible(false);
            if (head != null) {
                head.setVisible(false);
            }
        }
        else {
            this.nodePlayer[viewId].setVisible(true);
            var spready = this.nodePlayer[viewId].getChildByTag(GameViewLayer.SP_READY);
            spready.setVisible(userItem.cbUserStatus == yl.US_READY);
            //头像
            if (head == null) {
                var tempHead = new PopupInfoHead().createNormal(userItem, 75);
                head = tempHead[0];
                head.setPosition(0, 0);			//初始位置
                head.enableHeadFrame(false);
                head.enableInfoPop(true, posHead[viewId], anchorPointHead[viewId]);			//点击弹出的位置0
                head.setTag(GameViewLayer.SP_HEAD);
                this.nodePlayer[viewId].addChild(head);

                this.m_bNormalState[viewId] = true;
            }
            else {
                head.updateHead(userItem);
                //掉线头像变灰
                if (userItem.cbUserStatus == yl.US_OFFLINE) {
                    if (this.m_bNormalState[viewId]) {
                      // cc.convertToGraySprite(head.m_head.m_spRender);
                    }
                    this.m_bNormalState[viewId] = false;
                }
                else {
                    if (!this.m_bNormalState[viewId]) {
                        //convertToNormalSprite(head.m_head.m_spRender);
                    }
                    this.m_bNormalState[viewId] = true;
                }
            }
            head.setVisible(true);
            //金币
            //var score = userItem.lScore
            //var score = -10
            //if (userItem.lScore < 0){
            //	score = -score
            //end
            //var strScore = this.numInsertPoint(score)
            //if (userItem.lScore < 0){
            //	strScore = "."+strScore
            //end
            var score2 = this._scene.m_cbUserScore[viewId];
            var score = score2;
            if (score2 < 0) {
                score = -score;
            }
            var strScore = this.numInsertPoint(score);
            if (score2 < 0) {
                strScore = "." + strScore;
            }
            var asa = this.nodePlayer[viewId].getChildByTag(GameViewLayer.ASLAB_SCORE);
            asa.setString(strScore);
            asa.setVisible(false);

            //昵称
            //var strNickname = string.EllipsisByConfig(userItem.szNickName, 90, string.getConfig(res.round_body_ttf, 14));
            var strNickname = new cc.LabelTTF(userItem.szNickName, res.round_body_ttf, 20);
            //var strNickname = userItem.szNickName;
            var txt = this.nodePlayer[viewId].getChildByTag(GameViewLayer.TEXT_NICKNAME);
            txt.setString("");
            txt.addChild(strNickname);
        }
        if (userItem != null) {
            this.m_tabUserItem[viewId] = userItem;
        }
    },
	//用户聊天
    userChat : function(wViewChairId, chatString) {
        var self = this;
        if (chatString && chatString.length > 0) {
            this._chatLayer.showGameChat(false);
            //取消上次
            if (this.chatDetails[wViewChairId]) {
                this.chatDetails[wViewChairId].stopAllActions();
                this.chatDetails[wViewChairId].removeFromParent();
                this.chatDetails[wViewChairId] = null;
            }

            //创建label
            var limWidth = 24 * 12;
            var labCountLength = new cc.LabelTTF(chatString, res.round_body_ttf, 24);
            // if (labCountLength.getContentSize().width > limWidth) {
            //     this.chatDetails[wViewChairId] = new cc.LabelTTF(chatString, res.round_body_ttf, 24, cc.size(limWidth, 0));
            // }
            // else
            // var xScale = chatString.length<15?chatString.length/10:1.5;
            // var yScale = chatString.length<15?chatString.length/10:1.7;
            var xScale = 1.6;
            var yScale = 1.7;
                this.chatDetails[wViewChairId] = new cc.LabelTTF(chatString, res.round_body_ttf, 24,cc.size(204*xScale-20,50), cc.TEXT_ALIGNMENT_LEFT);

            this.chatDetails[wViewChairId].setColor(cc.color(0, 0, 0));
            this.chatDetails[wViewChairId].setPosition(posChat[wViewChairId].x, posChat[wViewChairId].y + 15);
            this.chatDetails[wViewChairId].setAnchorPoint(cc.p(0.5, 0.5));
            this.addChild(this.chatDetails[wViewChairId], 3);

            //改变气泡大小
            // this.chatBubble[wViewChairId].setContentSize(this.chatDetails[wViewChairId].getContentSize().width + 38, this.chatDetails[wViewChairId].getContentSize().height + 54);
            this.chatBubble[wViewChairId].setScale(xScale,yScale);
            this.chatBubble[wViewChairId].setVisible(true);
            //动作
            this.chatDetails[wViewChairId].runAction(new cc.Sequence(
                new cc.DelayTime(3),
                new cc.CallFunc(function (ref) {
                    self.chatDetails[wViewChairId].removeFromParent();
                    self.chatDetails[wViewChairId] = null;
                    self.chatBubble[wViewChairId].setVisible(false);
                })));
        }
    },
	//用户表情
    userExpression : function(wViewChairId, wItemIndex) {
        var self = this;
        if (wItemIndex && wItemIndex >= 0) {
            this._chatLayer.showGameChat(false);
            //取消上次
            if (this.chatDetails[wViewChairId]) {
                this.chatDetails[wViewChairId].stopAllActions();
                this.chatDetails[wViewChairId].removeFromParent();
                this.chatDetails[wViewChairId] = null;
            }

            var strName = sprintf("e(%d).png", wItemIndex);
            this.chatDetails[wViewChairId] = cc.spriteFrameCache(strName);
            this.chatDetails[wViewChairId].setPosition(posChat[wViewChairId].x, posChat[wViewChairId].y + 15);
            this.chatDetails[wViewChairId].setAnchorPoint(cc.p(0.5, 0.5));
            this.addChild(this.chatDetails[wViewChairId], 3);
            //改变气泡大小
            this.chatBubble[wViewChairId].setContentSize(90, 100);
            this.chatBubble[wViewChairId].setVisible(true);

            this.chatDetails[wViewChairId].runAction(new cc.Sequence(
                new cc.DelayTime(3),
                new cc.CallFunc(function (ref) {
                    self.chatDetails[wViewChairId].removeFromParent();
                    self.chatDetails[wViewChairId] = null;
                    self.chatBubble[wViewChairId].setVisible(false);
                })));
        }
    },

    onUserVoiceStart : function(viewId) {
        //取消上次
        if (this.chatDetails[viewId]) {
            this.chatDetails[viewId].stopAllActions();
            this.chatDetails[viewId].removeFromParent();
            this.chatDetails[viewId] = null;
        }
        // 语音动画
        var param = AnimationMgr.getAnimationParam();
        param.m_fDelay = 0.1;
        param.m_strName = ccmd.VOICE_ANIMATION_KEY;
        var animate = AnimationMgr.getAnimate(param);
        this.m_actVoiceAni = new cc.RepeatForever(animate);

        this.chatDetails[viewId] = new cc.Sprite("blank.png");
        this.chatDetails[viewId].setPosition(posChat[viewId].x, posChat[viewId].y + 15);
        this.chatDetails[viewId].setAnchorPoint(cc.p(0.5, 0.5));
        this.addChild(this.chatDetails[viewId], 3);
        if (viewId == 2 || viewId == 3) {
            this.chatDetails[viewId].setRotation(180);
        }
        this.chatDetails[viewId].runAction(this.m_actVoiceAni);

        //改变气泡大小
        this.chatBubble[viewId].setContentSize(90, 100);
        this.chatBubble[viewId].setVisible(true);
    },

    onUserVoiceEnded : function(viewId) {
        if (this.chatDetails[viewId]) {
            this.chatDetails[viewId].removeFromParent();
            this.chatDetails[viewId] = null;
            this.chatBubble[viewId].setVisible(false);
        }
    },

    onButtonClickedEvent : function(tag, ref) {
        if (tag == GameViewLayer.BT_START) {
            cc.log("长岭麻将开始！");
            this.btStart.setVisible(false);
            this.showTableBt(false);
            this._scene.sendGameStart();
        }
        //ysy_start
        else if (tag == 803) {
            this._textCardLayer.setVisible(true);
        }
        //ysy_end
        else if (tag == GameViewLayer.BT_SWITCH) {
            cc.log("按钮开关");
            this.showTableBt(true);
        }
        else if (tag == GameViewLayer.BT_CHAT || tag == GameViewLayer.BT_CHAT_NEW || tag == GameViewLayer.BT_CHAT_NEW1) {
            cc.log("聊天！");
            this.showTableBt(false);
            this._chatLayer.showGameChat(true);
            // this._chatLayer.setLocalZOrder(yl.MAX_INT)
        }
        else if (tag == GameViewLayer.BT_SET || tag == GameViewLayer.BT_SET_NEW || tag == GameViewLayer.BT_SET_NEW1) {
            cc.log("设置开关");
            this.showTableBt(false);
            this._setLayer.showLayer();
            //this._setLayer.setLocalZOrder(yl.MAX_INT)
            // var data2 = [0x02, 0x03, 0x04, 0x04, 0x05, 0x06, 0x11, 0x12, 0x14, 0x17, 0x19, 0x19, 0x25,
            // 			0x02, 0x03, 0x04, 0x04, 0x05, 0x06, 0x11, 0x12, 0x14, 0x17, 0x19, 0x19, 0x25]
            // this.setListeningCard(data2)
        }
        else if (tag == GameViewLayer.BT_HOWPLAY) {
            cc.log("玩法！");
            this.showPlayRule(true);
            //this.showTableBt(false)
            //this._scene._scene.popHelpLayer2(ccmd.KIND_ID, 0)

            //this._scene._scene.popHelpLayer(yl.HTTP_URL + "/Mobile/Introduce.aspx?kindid=389&typeid=0")
            // var data1 = [0x11, 0x08, 0x06, 0x09, 0x08, 0x02, 0x02, 0x07]
            // var data2 = [0x02, 0x03, 0x04, 0x04, 0x05, 0x06, 0x11, 0x12, 0x14, 0x17, 0x19, 0x19, 0x25, 0x36]
            // var data3 = [0x22, 0x22, 0x22, 0x19, 0x19]
            // var data4 = [0x01, 0x03, 0x05, 0x15, 0x16, 0x17, 0x24, 0x24, 0x25, 0x25, 0x25, 0x27, 0x36, 0x29]
            // var data5 = [1, 1, 1, 6, 7, 8, 9, 18, 19, 20, 33, 34, 35, 53]
            // for ( i = 1 ; i < ccmd.GAME_PLAYER ; i++){
            // 	this._cardLayer.setHandCard(i, 14, data5)
            // end
        }
        else if (tag == GameViewLayer.BT_EXIT) {
            cc.log("退出！");
            // this._cardLayer.bumpOrBridgeCard(1, [1, 1, 1], GameLogic.SHOW_PENG)
            // this._cardLayer.bumpOrBridgeCard(2, [1, 1, 1, 1], GameLogic.SHOW_PENG)
            //this._cardLayer.bumpOrBridgeCard(3, [1, 1, 1, 1], GameLogic.SHOW_AN_GANG)
            // this._cardLayer.bumpOrBridgeCard(4, [1, 1, 1, 1], GameLogic.SHOW_FANG_GANG)
            this._scene.onQueryExitGame();
        }
        else if (tag == GameViewLayer.BT_TRUSTEE) {
            cc.log("托管");
            this.showTableBt(false);
            this._scene.sendUserTrustee(true);
        }
        else if (tag == GameViewLayer.BT_TRUSTEECANCEL) {
            cc.log("取消托管");
            this._scene.sendUserTrustee(false);
            // else if (tag == this.BT_VOICE){
            // 	cc.log("语音关闭！")
            // 	this._scene._scene.startVoiceRecord()
            // else if (tag == this.BT_VOICEOPEN){
            // 	cc.log("语音开启！")
            // 	this._scene._scene.stopVoiceRecord()
//<===============<QTC_MODIFY>================//
        }
        else if (tag == GameViewLayer.BT_EAT) {
            cc.log("吃！");
            //发送吃牌
            this._scene.sendOperateCard(this.cbChiPlanCode[0], this.cbChiPlanCard[0]);
            this.showWaitOtherOperation(true);
            this.HideGameBtn();
        }
        else if (tag == GameViewLayer.BT_EAT_GROUP) {
            cc.log("吃 多方案！");

            //显示吃按钮
            //this.OnBtnChiOrGang("CHI")
            //显示吃牌
            //this.OnCardChiOrGang("CHI")
            //吃牌为按钮
            this.setChiCard();
        }
        else if (tag == GameViewLayer.BT_EAT_SUB_1) {
            cc.log("吃 方案1！");

            //发送吃牌
            this._scene.sendOperateCard(this.cbChiPlanCode[0], this.cbChiPlanCard[0]);
            this.showWaitOtherOperation(true);
            this.HideGameBtn();
        }
        else if (tag == GameViewLayer.BT_EAT_SUB_2) {
            cc.log("吃 方案2！");

            //发送吃牌
            this._scene.sendOperateCard(this.cbChiPlanCode[1], this.cbChiPlanCard[1]);
            this.showWaitOtherOperation(true);
            this.HideGameBtn();
        }
        else if (tag == GameViewLayer.BT_EAT_SUB_3) {
            cc.log("吃 方案3！");

            //发送吃牌
            this._scene.sendOperateCard(this.cbChiPlanCode[2], this.cbChiPlanCard[2]);
            this.showWaitOtherOperation(true);
            this.HideGameBtn();
//>===============<QTC_MODIFY>================//
        }
        else if (tag == GameViewLayer.BT_BUMP) {
            cc.log("碰！");
            //发送碰牌
            var cbOperateCard = [this.cbActionCard, this.cbActionCard, this.cbActionCard];
            this._scene.sendOperateCard(GameLogic.WIK_PENG, cbOperateCard);

            this.HideGameBtn();
        }
        else if (tag == GameViewLayer.BT_BRIGDE) {
            cc.log("杠！");
            var cbGangCard = this._cardLayer.getGangCard(this.cbActionCard);
            var cbOperateCard = [cbGangCard, cbGangCard, cbGangCard];
            //this._scene.sendOperateCard(GameLogic.WIK_GANG, cbOperateCard)
            this._scene.sendOperateCard(this.cbGangPlanCode[0], this.cbGangPlanCard[0]);

            this.HideGameBtn();
        }
//*******************<ysy_start>******************//
        else if (tag == GameViewLayer.BT_BRIGDE_GROUP) {
            cc.log("杠多方案！");

            //显示杠按钮
            //this.OnBtnChiOrGang("GANG")
            //显示杠牌
            //this.OnCardChiOrGang("GANG")
            //杠牌为按钮
            this.setGangCard();
        }
        else if (tag == GameViewLayer.BT_BRIGDE_SUB_1) {
            cc.log("杠 方案1！");

            //发送杠牌

            //this._scene.sendOperateCard(GameLogic.WIK_GANG, this.cbGangPlanCard[1])
            this._scene.sendOperateCard(this.cbGangPlanCode[0], this.cbGangPlanCard[0]);
            this.HideGameBtn();
        }
        else if (tag == GameViewLayer.BT_BRIGDE_SUB_2) {
            cc.log("杠 方案2！");

            //发送杠牌

            //this._scene.sendOperateCard(GameLogic.WIK_GANG, this.cbGangPlanCard[2])
            this._scene.sendOperateCard(this.cbGangPlanCode[1], this.cbGangPlanCard[1]);
            this.HideGameBtn();
        }
        else if (tag == GameViewLayer.BT_BRIGDE_SUB_3) {
            cc.log("杠 方案3！");

            //发送杠牌
            //this._scene.sendOperateCard(GameLogic.WIK_GANG, this.cbGangPlanCard[3])
            this._scene.sendOperateCard(this.cbGangPlanCode[2], this.cbGangPlanCard[2]);
            this.HideGameBtn();
        }
        else if (tag == GameViewLayer.BT_BRIGDE_SUB_4) {
            cc.log("杠 方案4！");

            //发送杠牌
            //this._scene.sendOperateCard(GameLogic.WIK_GANG, this.cbGangPlanCard[4])
            this._scene.sendOperateCard(this.cbGangPlanCode[3], this.cbGangPlanCard[3]);
            this.HideGameBtn();
        }
//*******************<ysy_end>******************//
        else if (tag == GameViewLayer.BT_WIN) {
            cc.log("胡！");

            var cbOperateCard = [this.cbActionCard, 0, 0];
            this._scene.sendOperateCard(GameLogic.WIK_CHI_HU, cbOperateCard);

            this.HideGameBtn();
        }
        else if (tag == GameViewLayer.BT_PASS) {
            cc.log("过！");
            var cbOperateCard = [0, 0, 0];
            this._scene.sendOperateCard(GameLogic.WIK_NULL, cbOperateCard);
            this.HideGameBtn();
        }
        else
            cc.log("default");
    },
	//-**********************< ysy_start >**************************//-
	//吃多方案，杠多方案按钮显示
    setGangCard : function() {
        this._cardLayerGang.showGangCardGroup2(null, this.cbGangPlanCard);
    },

    getGangCard : function(sValueID) {
        this._scene.sendOperateCard(this.cbGangPlanCode[sValueID], this.cbGangPlanCard[sValueID]);
        this.HideGameBtn();
    },
	//吃多方案，杠多方案按钮显示
    setChiCard : function() {
        this._cardLayerGang.showChiCardGroup2(null, this.cbChiPlanCard);
    },

    getChiCard : function(sValueID) {
        this._scene.sendOperateCard(this.cbChiPlanCode[sValueID], this.cbChiPlanCard[sValueID]);
        this.HideGameBtn();
    },
	//吃多方案，杠多方案按钮显示
    OnBtnChiOrGang : function(sValue) {

        this.spGameBtnGang.setVisible(false);
        this.spGameBtnChi.setVisible(false);

        if (sValue == "GANG") {
            var wNum = this.cbGangPlanCard.length;
            this.spGameBtnGang.setVisible(true);
            this.__showGangSub2();
            this._cardLayer.isGangOrChi("GANG");
        }
        else if (sValue == "CHI") {
            this.__showChiSub2();
            this.spGameBtnChi.setVisible(true);
            this._cardLayer.isGangOrChi("CHI");
        }
    },
	//吃多方案，杠多方案牌显示
    OnCardChiOrGang : function(sValue) {

        //var a = [[0x01,0x01,0x01,0x01],[0x02,0x02,0x02,0x02],[0x03,0x03,0x03,0x03]]
        //this.cbChiPlanCard = [[0x11,0x12,0x13],[0x12,0x13,0x14],[0x13,0x14,0x15]]

        if (sValue == "GANG") {        //杠
            //显示杠牌
            if (this.m_bGangGroup == false) {  //创建新牌
                var wIndex = this.cbGangPlanCard.length;
                if (wIndex >= 4) {
                    wIndex = 4;
                }
                this._cardLayer.showGangCardGroup(wIndex, this.cbGangPlanCard);
                this.m_bGangGroup = true;
            }
            else
                this._cardLayer.isGangOrChi("GANG");
        }
        else if (sValue == "CHI") {     //吃
            //显示吃牌
            if (this.m_bChiGroup == false) {  //创建新牌
                var wIndex = this.cbChiPlanCard.length;
                this._cardLayer.showChiCardGroup(wIndex, this.cbChiPlanCard);
                this.m_bChiGroup = true;
            }
            else
                this._cardLayer.isGangOrChi("CHI");
        }

    },
	//显示墙头牌
    OnQiangTouCard : function(cbValue) {
        this._cardLayer.showQiangtouCard(cbValue);
        this.m_cbQiangtouCard = cbValue;
    },
	//-**********************< ysy_end >**************************//-
	//计时器刷新
    OnUpdataClockView : function(viewId, time) {
        if (null == viewId || viewId == yl.INVALID_CHAIR || null == time) {
            this.asLabTime.setString("0");
        }
        else {
            var cbValue = this.m_cbBanker;
            if (this.m_cbBanker == -1 || this.m_cbBanker == null) {
                cbValue = 2;
            }
            cbValue = math_mod((viewId + 4 - cbValue), 4) + 1;
            var res = sprintf("sp_clock_%d_%d.png", cbValue, viewId + 1);
            var frame = cc.spriteFrameCache.getSpriteFrame(res);
            this.spClock.setSpriteFrame(frame);
            this.asLabTime.setString(time);
        }
        if (GlobalUserItem.bPrivateRoom) {
            this.asLabTime.setVisible(false);
        }
    },
	//开始
    gameStart : function(startViewId, wHeapHead, cbCardData, cbCardCount, cbSiceCount1, cbSiceCount2) {
        //this.runSiceAnimate(cbSiceCount1, cbSiceCount2, function()
        this._cardLayer.sendCard(cbCardData, cbCardCount);
        //end)
    },
	//用户出牌
    gameOutCard : function(viewId, card) {
        this.showCardPlate(viewId, card);
        this._cardLayer.removeHandCard(viewId, [card], true);

        this.cbOutCardTemp = card;
        this.cbOutUserTemp = viewId;
        //this._cardLayer.discard(viewId, card)
    },
	//用户抓牌
    gameSendCard : function(viewId, card, bTail) {
        //把上一个人打出的牌丢入弃牌堆
        if (this.cbOutCardTemp != 0 && !bTail) {
            this._cardLayer.discard(this.cbOutUserTemp, this.cbOutCardTemp);
            this.cbOutUserTemp = null;
            this.cbOutCardTemp = 0;
        }
        var self = this;
        //清理之前的出牌
        this.runAction(new cc.Sequence(
            new cc.DelayTime(0.5),
            new cc.CallFunc(function () {
                self.showCardPlate(null);
                self.showOperateFlag(null);
            })));

        //当前的人抓牌
        this._cardLayer.catchCard(viewId, card, bTail);
    },
	//摇骰子
    runSiceAnimate : function(cbSiceCount1, cbSiceCount2, callback) {
        var str1 = sprintf("sice_red_%d", cbSiceCount1);
        var str2 = sprintf("sice_white_%d", cbSiceCount2);
        var siceX1 = 667 - 320 + Math.random(640) - 35;
        var siceY1 = 375 - 120 + Math.random(240) + 43;
        var siceX2 = 667 - 320 + Math.random(640) - 35;
        var siceY2 = 375 - 120 + Math.random(240) + 43;
        var sp1 = new cc.Sprite();
        sp1.setPosition(siceX1, siceY1);
        sp1.setTag(GameViewLayer.SP_SICE1);
        this.addChild(sp1, 0);
        sp1.runAction(new cc.Sequence(
            this.getAnimate(str1),
            new cc.DelayTime(1),
            new cc.CallFunc(function (ref) {
                //ref.removeFromParent()
            })));
        var sp2 = new cc.Sprite();
        sp2.setPosition(siceX2, siceY2);
        sp2.setTag(GameViewLayer.SP_SICE2);
        this.addChild(sp2, 0);
        sp2.runAction(new cc.Sequence(
            this.getAnimate(str2),
            new cc.DelayTime(1),
            new cc.CallFunc(function (ref) {
                //ref.removeFromParent()
                if (callback) {
                    callback()
                }
            })));
        this._scene.PlaySound(ccmd.RES_PATH + "sound/DRAW_SICE.wav");
    },

    sendCardFinish : function() {
        var spSice1 = this.layer.getChildByTag(GameViewLayer.SP_SICE1);
        if (spSice1) {
            spSice1.removeFromParent();
        }
        var spSice2 = this.layer.getChildByTag(GameViewLayer.SP_SICE2);
        if (spSice2) {
            spSice2.removeFromParent();
        }
        this._scene.sendCardFinish();
    },

    gameConclude : function() {
        for (i = 0; i < ccmd.GAME_PLAYER; i++) {
            this.setUserTrustee(i, false);
        }
        this._cardLayer.gameEnded();
    },

    HideGameBtn : function() {
//****************<ysy_start>**********************//-
        this._cardLayer.isGangOrChi(null);
        this.__hideGangSub2();
        this.__hideChiSub2();
        this.m_bGangGroup = false;
        this.m_bChiGroup = false;
        if (this._cardLayerGang.cbGangParentTxt != null) {
            this._cardLayerGang.cbGangParentTxt.removeAllChildren();
        }
//****************<ysy_end>************************//-
//<===============<QTC_MODIFY>================//
        for (i = GameViewLayer.BT_EAT; i <= GameViewLayer.BT_WIN; i++) {
//>===============<QTC_MODIFY>================//
            var bt = this.spGameBtn.getChildByTag(i);
            if (bt) {
                bt.setEnabled(false);
                bt.setColor(cc.color(158, 112, 8));
            }
        }
        this.spGameBtn.setVisible(false);
    },
    //识别动作掩码
    recognizecbActionMask : function(cbActionMask, cbCardData) {
        cc.log("收到提示操作：", cbActionMask, cbCardData);
        if (cbActionMask == GameLogic.WIK_NULL || cbActionMask == 32) {
            appdf.assert("false");
            return false;
        }

        if (this._cardLayer.isUserMustWin()) {
            //必须胡牌的情况
            var btn = this.spGameBtn.getChildByTag(GameViewLayer.BT_PASS);
            btn.setEnabled(false);
            btn.setColor(cc.color(158, 112, 8));
            // this.spGameBtn.getChildByTag(this.BT_WIN)
            // 	.setEnabled(true)
            // 	.setColor(cc.color(255, 255, 255))
            // this.spGameBtn.setVisible(true)
            // this._scene.SetGameOperateClock()
            // return true
        }

        if (cbCardData) {
            this.cbActionCard = cbCardData;
        }
//////////////-***<ysy_start>***//////////////////////-
        //if (cbActionMask >= 144){ 				//风杠
        //	cbActionMask = cbActionMask - 144
        //    var operationCard = [cbCardData,0,0]
        //    this._scene.sendOperateCard(GameLogic.WIK_FENG_GANG,operationCard)
        //	//this.spGameBtn.setVisible(true)
        //    //this._scene.SetGameOperateClock()
        //    return
        //end
//////////////-***<ysy_end  >***//////////////////////-

        var cbGangIndex = 0;
        //清空数据
        this.cbGangPlanCard = [];
        this.cbGangPlanCode = [];
//-<////////////////////////////////////-<WW_ZhuiFengGang>//////////////////////////////////////////////////////-
        if (cbActionMask >= GameLogic.WIK_BIRD) {   //【中发白+幺鸡】组成的杠
            cbActionMask = cbActionMask - GameLogic.WIK_BIRD;
            //todo
            var result = this._cardLayer.getGangCardPlan(GameLogic.WIK_BIRD, null);
            var cardCode = result[0];
            var cardData = result[1];
            this.cbGangPlanCode[cbGangIndex] = GameLogic.WIK_BIRD;
            this.cbGangPlanCard[cbGangIndex] = cardData[0];
            cbGangIndex = cbGangIndex + 1;

            //杠按钮
            var btn1 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE_GROUP);
            btn1.setVisible(false);
            var btn2 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE);
            btn2.setEnabled(true);
            btn2.setVisible(true);
            btn2.setColor(cc.color(255, 255, 255));
        }
//->////////////////////////////////////-<WW_ZhuiFengGang>//////////////////////////////////////////////////////-
        if (cbActionMask >= GameLogic.WIK_WIND) {   //【东南西北】四张【风牌】在一起组成的杠
            cbActionMask = cbActionMask - GameLogic.WIK_WIND;
            var result = this._cardLayer.getGangCardPlan(GameLogic.WIK_WIND, null);
            var cardCode = result[0];
            var cardData = result[1];
            this.cbGangPlanCode[cbGangIndex] = GameLogic.WIK_WIND;
            this.cbGangPlanCard[cbGangIndex] = cardData[0];
            cbGangIndex = cbGangIndex + 1;
            //杠按钮
//-<////////////////////////////////////-<WW_ZhuiFengGang>//////////////////////////////////////////////////////-
            if (cbGangIndex > 1) {
                var btn3 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE);
                btn3.setVisible(false);
                var btn4 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE_GROUP);
                btn4.setVisible(true);
                btn4.setEnabled(true);
                btn4.setColor(cc.color(255, 255, 255));
            } else {
//->////////////////////////////////////-<WW_ZhuiFengGang>//////////////////////////////////////////////////////-
                var btn5 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE_GROUP);
                btn5.setVisible(false);
                var btn6 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE);
                btn6.setEnabled(true);
                btn6.setVisible(true);
                btn6.setColor(cc.color(255, 255, 255));
            }
        }
        if (cbActionMask >= GameLogic.WIK_ARROW) {   //【中发白】三张【箭牌】在一起组成的杠
            cbActionMask = cbActionMask - GameLogic.WIK_ARROW;
            result = this._cardLayer.getGangCardPlan(GameLogic.WIK_ARROW, null);
            cardCode = result[0];
            cardData = result[1];
            this.cbGangPlanCode[cbGangIndex + 1] = GameLogic.WIK_ARROW;
            this.cbGangPlanCard[cbGangIndex + 1] = cardData[1];
            cbGangIndex = cbGangIndex + 1;

            //杠按钮
//-<////////////////////////////////////-<WW_ZhuiFengGang>//////////////////////////////////////////////////////-
            if (cbGangIndex > 1) {
                var btn7 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE);
                btn7.setVisible(false);
                var btn8 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE_GROUP);
                btn8.setVisible(true);
                btn8.setEnabled(true);
                btn8.setColor(cc.color(255, 255, 255));
            }
            else {
//->////////////////////////////////////-<WW_ZhuiFengGang>//////////////////////////////////////////////////////-
                var btn9 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE_GROUP);
                btn9.setVisible(false);
                var btn10 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE);
                btn10.setEnabled(true);
                btn10.setVisible(true);
                btn10.setColor(cc.color(255, 255, 255));
            }
        }
        //-<////////////////////////////////////-<WW_ZhuiFengGang>//////////////////////////////////////////////////////-
        if (cbActionMask >= GameLogic.WIK_CHASEBIRD) {   //【中发白+幺鸡】杠后追加的【中发白+幺鸡】牌
            cbActionMask = cbActionMask - GameLogic.WIK_CHASEBIRD;
            result = this._cardLayer.getGangCardPlan(GameLogic.WIK_CHASEBIRD, null);
            cardCode = result[0];
            cardData = result[1];
            if (cardData != null) {
                for (i = 0; i < cardData.length; i++) {
                    this.cbGangPlanCard[cbGangIndex] = cardData[i];
                    this.cbGangPlanCode[cbGangIndex] = cardCode[i];
                    cbGangIndex = cbGangIndex + 1;
                }
            }
            if (cbGangIndex > 0) {
                btn1 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE);
                btn1.setVisible(false);
                btn2 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE_GROUP);
                btn2.setVisible(true);
                btn2.setEnabled(true);
                btn2.setColor(cc.color(255, 255, 255));
            }
            else {
                if (this.cbGangPlanCard.length == 0) {
                    this.cbGangPlanCard[0] = [cardData];
                    this.cbGangPlanCode[0] = GameLogic.WIK_CHASEBIRD;
                }
                btn1 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE_GROUP);
                btn1.setVisible(false);
                btn2 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE);
                btn2.setEnabled(true);
                btn2.setVisible(true);
                btn2.setColor(cc.color(255, 255, 255));
            }

        }
        //->////////////////////////////////////-<WW_ZhuiFengGang>//////////////////////////////////////////////////////-
        if (cbActionMask >= GameLogic.WIK_WALLTOP_O) {   //【墙头他杠】一张来自【他人】,其实是碰
            cbActionMask = cbActionMask - GameLogic.WIK_WALLTOP_O;
            this.cbGangPlanCard[cbGangIndex] = [this.m_cbQiangtouCard, this.m_cbQiangtouCard, this.m_cbQiangtouCard];
            this.cbGangPlanCode[cbGangIndex] = GameLogic.WIK_WALLTOP_O;
            cbGangIndex = cbGangIndex + 1;

            //杠按钮
            btn1 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE_GROUP);
            btn1.setVisible(false);
            btn2 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE);
            btn2.setEnabled(true);
            btn2.setVisible(true);
            btn2.setColor(cc.color(255, 255, 255));
        }
        if (cbActionMask >= GameLogic.WIK_WALLTOP_S) {   //【墙头自杠】三张都来自【自己】,属于暗杠
            cbActionMask = cbActionMask - GameLogic.WIK_WALLTOP_S;
            result = this._cardLayer.getGangCardPlan(GameLogic.WIK_WALLTOP_S, this.m_cbQiangtouCard);
            cardCode = result[0];
            cardData = result[1];
            this.cbGangPlanCode[cbGangIndex] = GameLogic.WIK_WALLTOP_S;
            this.cbGangPlanCard[cbGangIndex] = cardData[cbGangIndex];
            cbGangIndex = cbGangIndex + 1;

            //杠按钮
            btn1 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE_GROUP);
            btn1.setVisible(false);
            btn2 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE);
            btn2.setEnabled(true);
            btn2.setVisible(true);
            btn2.setColor(cc.color(255, 255, 255));
        }
//-<////////////////////////////////////<WW_ZhuiFengGang>////////////////////////////////////////
        if (cbActionMask >= GameLogic.WIK_CHASEWIND) {   //【东南西北】杠后追加的风牌
            cbActionMask = cbActionMask - GameLogic.WIK_CHASEWIND;
            result = this._cardLayer.getGangCardPlan(GameLogic.WIK_CHASEWIND, null);
            cardCode = result[0];
            cardData = result[1];
            if (cardData != null) {
                for (i = 0; i < cardData.length; i++) {
                    this.cbGangPlanCard[cbGangIndex] = cardData[i];
                    this.cbGangPlanCode[cbGangIndex] = cardCode[i];
                    cbGangIndex = cbGangIndex + 1;
                }
            }
            if (cbGangIndex > 0) {
                btn1 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE);
                btn1.setVisible(false);
                btn2 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE_GROUP);
                btn2.setVisible(true);
                btn2.setEnabled(true);
                btn2.setColor(cc.color(255, 255, 255));
            }
            else {
                if (this.cbGangPlanCard.length == 0) {
                    this.cbGangPlanCard[0] = [cardData];
                    this.cbGangPlanCode[0] = GameLogic.WIK_CHASEWIND;
                }
                btn1 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE_GROUP);
                btn1.setVisible(false);
                btn2 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE);
                btn2.setEnabled(true);
                btn2.setVisible(true);
                btn2.setColor(cc.color(255, 255, 255));
            }
        }
        if (cbActionMask >= GameLogic.WIK_CHASEARROW) {   //【中发白】杠后追加的箭牌
            cbActionMask = cbActionMask - GameLogic.WIK_CHASEARROW;
            result = this._cardLayer.getGangCardPlan(GameLogic.WIK_CHASEARROW, null);
            cardCode = result[0];
            cardData = result[1];
            if (cardData != null) {
                for (i = 0; i < cardData.length; i++) {
                    this.cbGangPlanCard[cbGangIndex + 1] = cardData[i];
                    this.cbGangPlanCode[cbGangIndex + 1] = cardCode[i];
                    cbGangIndex = cbGangIndex + 1;
                }
            }
            if (cbGangIndex > 1) {
                btn1 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE);
                btn1.setVisible(false);
                btn2 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE_GROUP);
                btn2.setVisible(true);
                btn2.setEnabled(true);
                btn2.setColor(cc.color(255, 255, 255));
            }
            else {
                if (this.cbGangPlanCard.length == 0) {
                    this.cbGangPlanCard[1] = [cardData];
                    this.cbGangPlanCode[1] = GameLogic.WIK_CHASEARROW;
                }
                btn1 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE_GROUP);
                btn1.setVisible(false);
                btn2 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE);
                btn2.setEnabled(true);
                btn2.setVisible(true);
                btn2.setColor(cc.color(255, 255, 255));
            }
        }
        //->////////////////////////////////////<WW_ZhuiFengGang>////////////////////////////////////////
        if (cbActionMask >= 128) { 				//放炮
            cbActionMask = cbActionMask - 128;
            btn1 = this.spGameBtn.getChildByTag(GameViewLayer.BT_WIN);
            btn1.setEnabled(true);
            btn1.setColor(cc.color(255, 255, 255));
        }
        if (cbActionMask >= 64) { 					//胡
            cbActionMask = cbActionMask - 64;
            btn1 = this.spGameBtn.getChildByTag(GameViewLayer.BT_WIN);
            btn1.setEnabled(true);
            btn1.setColor(cc.color(255, 255, 255));
        }
        if (cbActionMask >= 32) { 					//听
            cbActionMask = cbActionMask - 32;
        }
        if (cbActionMask >= 16) { 					//杠
            cbActionMask = cbActionMask - 16;
            //<===============<QTC_MODIFY>================//
            result = this._cardLayer.getGangCardPlan(GameLogic.WIK_GANG, null);
            cardCode = result[0];
            cardData = result[1];
            if (cardData != null) {
                for (i = 0; i < cardData.length; i++) {
                    this.cbGangPlanCard[cbGangIndex] = cardData[i];
                    this.cbGangPlanCode[cbGangIndex] = cardCode[i];
                    cbGangIndex = cbGangIndex + 1;
                }
            }

            //if (this.cbGangPlanCode && this.cbGangPlanCard){
            if (cbGangIndex > 0) {
                btn1 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE);
                btn1.setVisible(false);
                btn2 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE_GROUP);
                btn2.setVisible(true);
                btn2.setEnabled(true);
                btn2.setColor(cc.color(255, 255, 255));
            }
            else {
                if (this.cbGangPlanCard.length == 0) {
                    this.cbGangPlanCard[0] = [cbCardData, cbCardData, cbCardData];
                    this.cbGangPlanCode[0] = GameLogic.WIK_GANG;
                }
                btn1 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE_GROUP);
                btn1.setVisible(false);
                btn2 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BRIGDE);
                btn2.setEnabled(true);
                btn2.setVisible(true);
                btn2.setColor(cc.color(255, 255, 255));
            }
            //>===============<QTC_MODIFY>================//
        }
        if (cbActionMask >= 8) { 					//碰
            cbActionMask = cbActionMask - 8;
            if (this._cardLayer.isUserCanBump()) {
                btn1 = this.spGameBtn.getChildByTag(GameViewLayer.BT_BUMP);
                btn1.setEnabled(true);
                btn1.setColor(cc.color(255, 255, 255));
            }
        }
//<===============<QTC_MODIFY>================//
        if (cbActionMask > 0) { 					//吃
            var cmd_data;
            var planCount;
            this.cbChiPlanCode = [];
            this.cbChiPlanCard = [];
            planCount = 0;
            if (cbActionMask >= 4) { 					//右吃
                cbActionMask = cbActionMask - 4;
                this.cbChiPlanCode[planCount] = GameLogic.WIK_RIGHT;
                this.cbChiPlanCard[planCount] = [this.cbActionCard - 2, this.cbActionCard - 1, this.cbActionCard];
                planCount = planCount + 1;
            }
            if (cbActionMask >= 2) { 					//中吃
                cbActionMask = cbActionMask - 2;
                this.cbChiPlanCode[planCount] = GameLogic.WIK_CENTER;
                this.cbChiPlanCard[planCount] = [this.cbActionCard - 1, this.cbActionCard, this.cbActionCard + 1];
                planCount = planCount + 1;
            }
            if (cbActionMask >= 1) { 					//左吃
                cbActionMask = cbActionMask - 1;
                this.cbChiPlanCode[planCount] = GameLogic.WIK_LEFT;
                this.cbChiPlanCard[planCount] = [this.cbActionCard, this.cbActionCard + 1, this.cbActionCard + 2];
                planCount = planCount + 1;
            }
            if (planCount > 1) {
                btn1 = this.spGameBtn.getChildByTag(GameViewLayer.BT_EAT_GROUP);
                btn1.setVisible(true);
                btn1.setEnabled(true);
                btn1.setColor(cc.color(255, 255, 255));
            }
            else {
                btn1 = this.spGameBtn.getChildByTag(GameViewLayer.BT_EAT_GROUP);
                btn1.setVisible(false);
                btn2 = this.spGameBtn.getChildByTag(GameViewLayer.BT_EAT);
                btn2.setEnabled(true);
                btn2.setColor(cc.color(255, 255, 255));
            }
        }
//>===============<QTC_MODIFY>================//
        this.spGameBtn.setVisible(true);
        this._scene.SetGameOperateClock();

        return true;
    },

    getAnimate : function(name, bEndRemove) {
        var animation = cc.animationCache().getAnimation(name);
        var animate = new cc.Animate(animation);
        if (bEndRemove) {
            animate = new cc.Sequence(animate, new cc.CallFunc(function (ref) {
                ref.removeFromParent();
            }));
        }
        return animate;
    },
    //设置听牌提示
    setListeningCard : function(cbCardData) {
        if (cbCardData == null || ccmd.SHOW_TING == false) {
            this.spListenBg.setVisible(false);
            return;
        }
        appdf.assert(cbCardData == null);
        this.spListenBg.removeAllChildren();
        if (ccmd.SHOW_TING) {
            this.spListenBg.setVisible(true);
        }

        var cbCardCount = cbCardData.length;
        var bTooMany = (cbCardCount >= 16);
        //拼接块
        var width = 44;
        var height = 67;
        var posX = 327;
        var fSpacing = 100;
        if (!bTooMany) {
            for (i = 0; i < fSpacing * cbCardCount; i++) {
                var sp1 = new cc.Sprite("sp_listenBg_2.png");
                sp1.setPosition(posX, 46.5);
                sp1.setAnchorPoint(cc.p(0, 0.5));
                sp1.addChild(this.spListenBg);
                posX = posX + 1;
                if (i > 700) {
                    break;
                }
            }
        }
        //尾块
        var sp2 = new cc.Sprite("sp_listenBg_3.png");
        sp2.setPosition(posX, 46.5);
        sp2.setAnchorPoint(cc.p(0, 0.5));
        sp2.addChild(GameViewLayer.spListenBg);
        //可胡牌过多，屏幕摆不下
        if (bTooMany) {
            var cardBack = new cc.Sprite(ccmd.RES_PATH + "game/font_small/card_down.png");
            cardBack.setPosition(183 + 40, 46);
            this.spListenBg.addChild(cardBack);
            var cardFont = new cc.Sprite(ccmd.RES_PATH + "game/font_small/font_3_5.png");
            cardFont.setPosition(width / 2, height / 2 + 8);
            cardBack.addChild(cardFont);

            var strFilePrompt = "";
            var spListenCount = null;
            if (cbCardCount == 28) { 		//所有牌
                strFilePrompt = "389_sp_listen_anyCard.png";
            }
            else {
                strFilePrompt = "389_sp_listen_manyCard.png";
                spListenCount = new cc.LabelTTF(cbCardCount + "", res.round_body_ttf, 30);
            }

            var spPrompt = new cc.Sprite(strFilePrompt);
            spPrompt.setPosition(183 + 110, 46);
            spPrompt.setAnchorPoint(cc.p(0, 0.5));
            this.spListenBg.addChild(spPrompt);
            if (spListenCount) {
                spListenCount.setPosition(70, 12).addChild(spPrompt);
            }
            // new cc.LabelTTF("厉害了word哥！你可以胡的牌太多，摆不下了++", res.round_body_ttf, 50)
            // 	.setPosition(260, 40)
            // 	.setAnchorPoint(cc.p(0, 0.5))
            // 	.setColor(cc.color(0, 0, 0))
            // 	.addChild(GameViewLayer.spListenBg, 1)
        }
        //牌、番、数
        this.cbAppearCardIndex = GameLogic.DataToCardIndex(this._scene.cbAppearCardData);
        for (i = 0; i < cbCardCount; i++) {
            if (bTooMany) {
                break;
            }
            var tempX = fSpacing * (i - 1);
            //var rectX = this._cardLayer.switchToCardRectX(cbCardData[i])
            var cbCardIndex = GameLogic.SwitchToCardIndex(cbCardData[i]);
            var nLeaveCardNum = 4 - this.cbAppearCardIndex[cbCardIndex];
            //牌底
            var card = new cc.Sprite(ccmd.RES_PATH + "game/font_small/card_down.png");
            //.setTextureRect(cc.rect(width*rectX, 0, width, height))
            card.setPosition(183 + tempX, 46);
            this.spListenBg.addChild(card);
            //字体
            var nValue = math_mod(cbCardData[i], 16);
            var nColor = Math.floor(cbCardData[i] / 16);
            var strFile = ccmd.RES_PATH + "game/font_small/font_" + nColor + "_" + nValue + ".png";
            var cardFont = new cc.Sprite(strFile);
            cardFont.setPosition(width / 2, height / 2 + 8);
            card.addChild(cardFont);
            var la1 = new cc.LabelTTF("1", res.round_body_ttf, 16);		//番数
            la1.setPosition(220 + tempX, 61);
            la1.setColor(cc.color(254, 246, 165));
            this.spListenBg.addChild(la1);

            var sp3 = new cc.Sprite("sp_listenTimes.png");
            sp3.setPosition(244 + tempX, 61);
            this.spListenBg.addChild(sp3);
            var la2 = new cc.LabelTTF(nLeaveCardNum + "", res.round_body_ttf, 16);		//剩几张
            la2.setPosition(220 + tempX, 31);

            la2.setColor(cc.color(254, 246, 165));
            la2.setTag(cbCardIndex);
            this.spListenBg.addChild(la2);
            var sp4 = new cc.Sprite("sp_listenNum.png");
            sp4.setPosition(244 + tempX, 31);
            sp4.addChild(this.spListenBg);
        }
    },
    //减少可听牌数
    reduceListenCardNum : function(cbCardData) {
        var cbCardIndex = GameLogic.SwitchToCardIndex(cbCardData);
        if (this.cbAppearCardIndex.length == 0) {
            this.cbAppearCardIndex = GameLogic.DataToCardIndex(this._scene.cbAppearCardData);
        }
        this.cbAppearCardIndex[cbCardIndex] = this.cbAppearCardIndex[cbCardIndex] + 1;
        var labelLeaveNum = this.spListenBg.getChildByTag(cbCardIndex);
        if (labelLeaveNum) {
            var nLeaveCardNum = 4 - this.cbAppearCardIndex[cbCardIndex];
            labelLeaveNum.setString(nLeaveCardNum + "");
        }
    },

    setBanker : function(viewId) {
        if (viewId < 0 || viewId >= ccmd.GAME_PLAYER) {
            cc.log("chair id is error!");
            return false;
        }
        this.m_cbBanker = viewId;
        var spBanker = this.nodePlayer[viewId].getChildByTag(GameViewLayer.SP_BANKER);
        spBanker.setVisible(true);

        this.spClockBg.setRotation(rotationAngle[viewId]);

        return true;
    },

    setUserTrustee : function(viewId, bTrustee) {
        this.nodePlayer[viewId].getChildByTag(GameViewLayer.SP_TRUSTEE).setVisible(bTrustee);
        if (viewId == ccmd.MY_VIEWID) {
            this.spTrusteeCover.setVisible(bTrustee);
        }
    },
    //设置房间信息
    setRoomInfo : function(tableId, chairId) {
    },

    onTrusteeTouchCallback : function(event, x, y) {
        if (!this.spTrusteeCover.isVisible()) {
            return false;
        }

        var rect = this.spTrusteeCover.getChildByTag(GameViewLayer.SP_TRUSTEEBG).getBoundingBox();
        if (cc.rectContainsPoint(rect, cc.p(x, y))) {
            return true;
        }
        else
            return false;
    },
    //设置剩余牌
    setRemainCardNum : function(num) {
        var strRemianNum = sprintf("剩%d张", num);
        var textNum = this.layer.getChildByTag(GameViewLayer.TEXT_REMAINNUM);
        textNum.setString(strRemianNum);
    },
    //牌托
    showCardPlate : function(viewId, cbCardData) {
        if (null == viewId) {
            this.spCardPlate.setVisible(false);
            return;
        }
        //var rectX = this._cardLayer.switchToCardRectX(cbCardData)
        var nValue = math_mod(cbCardData, 16);
        var nColor = Math.floor(cbCardData / 16);
        var strFile = ccmd.RES_PATH + "game/font_middle/font_" + nColor + "_" + nValue + ".png";
        this.spCardPlate.getChildByTag(GameViewLayer.SP_PLATECARD).setTexture(strFile);
        this.spCardPlate.setPosition(posPlate[viewId]);
        this.spCardPlate.setVisible(true);
    },
    //操作效果
    showOperateFlag : function(viewId, operateCode) {
        var spFlag = this.layer.getChildByTag(GameViewLayer.SP_OPERATFLAG);
        if (spFlag) {
            spFlag.removeFromParent();
        }
        if (null == viewId) {
            return false;
        }
        var strFile = "";
        if (operateCode == GameLogic.WIK_NULL) {
            return false;
        }
        else if (operateCode == GameLogic.WIK_CHI_HU) {
            // strFile = "sp_flag_win.png";
            strFile = cc.spriteFrameCache.getSpriteFrame("sp_flag_win.png");
        }
        else if (operateCode == GameLogic.WIK_LISTEN) {
            // strFile = "sp_flag_listen.png";
            strFile = cc.spriteFrameCache.getSpriteFrame("sp_flag_listen.png");
        }
        else if (operateCode == GameLogic.WIK_GANG || operateCode == GameLogic.WIK_WALLTOP_O
            || operateCode == GameLogic.WIK_WALLTOP_S || operateCode == GameLogic.WIK_ARROW
            || operateCode == GameLogic.WIK_WIND
//-<////////////////////////////-<WW_ZhuiFengGang>////////////////////////////////////////////
            || operateCode == GameLogic.WIK_CHASEWIND || operateCode == GameLogic.WIK_CHASEARROW
            || operateCode == GameLogic.WIK_BIRD || operateCode == GameLogic.WIK_CHASEBIRD
//->////////////////////////////-<WW_ZhuiFengGang>////////////////////////////////////////////
        ) {
            // strFile = "sp_flag_bridge.png";
            strFile = cc.spriteFrameCache.getSpriteFrame("sp_flag_bridge.png");
        }
        else if (operateCode == GameLogic.WIK_PENG) {
            // strFile = "sp_flag_bump.png";
            strFile = cc.spriteFrameCache.getSpriteFrame("sp_flag_bump.png");
        }
        else if (operateCode <= GameLogic.WIK_RIGHT) {
            // strFile = "sp_flag_eat.png";
            strFile = cc.spriteFrameCache.getSpriteFrame("sp_flag_eat.png");
        }
        var sp = new cc.Sprite(strFile);
        sp.setTag(GameViewLayer.SP_OPERATFLAG);
        sp.setPosition(posPlate[viewId]);
        this.layer.addChild(sp, 2);

        return true;
    },
    //数字中插入点
    numInsertPoint : function(lScore) {
        appdf.assert(lScore >= 0);
        var strRes = "";
        var str = sprintf("%d", lScore);
        var len = str.length;

        var times = Math.floor(len / 3);
        var remain = math_mod(len, 3);
        strRes = strRes + str.substring(1, remain);
        for (i = 0; i < times; i++) {
            if (strRes != "") {
                strRes = strRes + "/";
            }
            var index = (i - 1) * 3 + remain + 1;	//截取起始位置
            strRes = strRes + str.substring(index, index + 2);
        }
        return strRes;
    },

    setRoomHost : function(viewId) {
        for (i = 0; i < ccmd.GAME_PLAYER; i++) {
            this.nodePlayer[i].getChildByTag(GameViewLayer.SP_ROOMHOST).setVisible(false);
        }
        this.nodePlayer[viewId].getChildByTag(GameViewLayer.SP_ROOMHOST).setVisible(true);
    },
    //-ysy_start
    //卡牌层
    CardTest : function() {
        var textCardLayer = ccs.load(ccmd.RES_PATH + "gameResult/GameCardText.csb");
        textCardLayer.setVisible(false);
        return textCardLayer;
    },
    //初始化
    CardTestInit : function(){
        this.t_cbCardData = [];              //byte 卡牌存储
        this.t_cbSendCardData = [];        //发送牌值
        this.t_spriteData = [];           //sprite 精灵显示卡值
        this.t_btCard = [];                   //按钮牌
        this.t_spriteCard = [];               //按钮精灵
        this.t_btCardData = [];               //牌库按钮
        this.t_cbSurplusCard = [];                //剩余牌
        this.t_cbAutoSurplusCard = [];                //自动补全剩余牌

        this.t_cbIndexNow = 0;               //记录当前在哪张牌上-索引
        this.t_btIndexNow = null;                 //记录当前在哪张牌上-牌

        //初始化牌库
        for ( i = 0 ; i < 136 ; i++) {
            this.t_cbCardData[i] = 0;
        }
        //初始化每张牌有四张
        for ( i = 0 ; i < 37 ; i++) {
            this.t_cbSurplusCard[i] = 4;
            this.t_cbAutoSurplusCard[i] = 4;
        }
        //按钮回调
        var btnCallback2 = function(ref, eventType) {
            //其它按钮
            if (ref.getTag() >= 51 && ref.getTag() <= 55) {
                if (eventType == ccui.Widget.TOUCH_ENDED) {
                    this.OnButtonTestCard(ref.getTag(), ref);
                }
            }
            //牌按钮
            if (ref.getTag() >= 101 && ref.getTag() <= 137) {
                if (eventType == ccui.Widget.TOUCH_ENDED) {
                    this.OnButtonCardOne(ref.getTag(), ref);
                }
            }
            //牌库按钮
            if (ref.getTag() >= 1001 && ref.getTag() <= 1136) {
                if (eventType == ccui.Widget.TOUCH_ENDED) {
                    this.OnButtonCardData(ref.getTag(), ref);
                }
            }
        };

        //文字——start
        this.t_textWarning = this._textCardLayer.getChildByTag(25);
        this.t_textWarning.setVisible(false);
        //文字——end

        ////-按钮——start
        //全部清除
        this.t_btAllClear = this._textCardLayer.getChildByTag(51);
        this.t_btAllClear.addTouchEventListener(btnCallback2);
        //自动补全
        this.t_btAuto = this._textCardLayer.getChildByTag(52);
        this.t_btAuto.addTouchEventListener(btnCallback2);
        //发送开始
        this.t_btSendStart = this._textCardLayer.getChildByTag(53);
        this.t_btSendStart.addTouchEventListener(btnCallback2);
        //发送结束
        this.t_btSendEnd = this._textCardLayer.getChildByTag(54);
        this.t_btSendEnd.addTouchEventListener(btnCallback2);
        //退出编辑
        this.t_btExit = this._textCardLayer.getChildByTag(55);
        this.t_btExit.addTouchEventListener(btnCallback2);

        //牌按钮
        var tag = 101;
        for ( i = 0 ; i < 37 ; i++) {
            if (math_mod(i, 10) != 0) {
                //牌按钮
                var btn = this._textCardLayer.getChildByTag(tag);
                btn.addTouchEventListener(btnCallback2);
                this.t_btCard[i] = btn;
                //牌按钮精灵
                var sprite = this._textCardLayer.getChildByTag(tag + 100);
                this.t_spriteCard[i] = sprite;
            }
            tag = tag + 1;
        }
        //牌库按钮
        tag = 1001;
        for ( i = 0 ; i < 136 ; i++) {
            var btn = this._textCardLayer.getChildByTag(tag);
            btn.addTouchEventListener(btnCallback2);
            this.t_btCardData[i] = btn;
            tag = tag + 1;
        }
        //////按钮——end
    },
    //其它按钮响应
    OnButtonTestCard : function(tag, ref) {
        if (tag == 51) {               //全部清除
            this.CleanCard();
        }
        else if (tag == 52) {            //自动补足
            this.AutoCard();
        }
        else if (tag == 53) {            //发送开始
            if (this.isAutoCard() == true) {
                this._scene.sendOperateStartCard(this.t_cbSendCardData, 7);
            }
            else
                appdf.assert(true == true);
        }
        else if (tag == 54) {             //发送停止
            this._scene.sendOperateStartCard(null, 8);
        }
        else if (tag == 55) {            //关闭
            this.showCardText(false);
        }
    },
    //牌按钮响应
    OnButtonCardOne : function(tag, ref) {
        //判断是否是开始
        if (this.t_cbIndexNow == 0) {
            this.t_cbIndexNow = 1;
        }
        //超出136张返回
        if (this.t_cbIndexNow > 136) {
            return;
        }
        //获取值
        tag = tag - 100;
        var nValue = math_mod(tag, 10);
        var nColor = Math.floor(tag / 10);
        var num = nColor * 16 + nValue;//牌值

        //判断当前牌是否已有四张
        if (this.t_cbSurplusCard[tag] != 0) { //不足四张
            if (this.t_btIndexNow != null) {
                this.t_btIndexNow.setColor(cc.color(255, 255, 255));
            }
            this.t_cbSurplusCard[tag] = this.t_cbSurplusCard[tag] - 1;
            //判断当前牌是否为空,若不是空，查找空位置
            if (this.t_spriteData[this.t_cbIndexNow] != null) {
                var cbIndex = this.FindDataCard();
                if (cbIndex == 0) {
                    return;
                }
                else
                    this.t_cbIndexNow = cbIndex;
            }
            this.t_cbCardData[this.t_cbIndexNow] = num;
            //判断当前按钮牌是否已经有四张，有隐藏按钮精灵
            if (this.t_cbSurplusCard[tag] == 0) {
                this.t_spriteCard[tag].setVisible(false);
            }
            //绘画当前值
            this.showCardOne(this.t_cbIndexNow, num);
            this.t_cbIndexNow = this.t_cbIndexNow + 1;
            var cbIndex = this.FindDataCard();
            //判断下一张牌是否为空，或是否超过牌库大小（牌库大小136）；若不为空或超过，查找空库位置；若为空则直接使用索引
            if (this.t_spriteData[this.t_cbIndexNow] != null || this.t_cbIndexNow + 1 >= 136) {
                if (cbIndex == 0) {
                    return;
                }
                else {
                    this.t_cbIndexNow = cbIndex;
                    this.t_btIndexNow = this.t_btCardData[cbIndex];
                    this.t_btIndexNow.setColor(cc.color(176, 204, 243, 0));
                }
            }
            else {
                if (this.t_cbIndexNow + 1 >= 136) return;
                this.t_btIndexNow = this.t_btCardData[this.t_cbIndexNow];
                this.t_btIndexNow.setColor(cc.color(176, 204, 243, 0));
            }
        }
        else {
        }
    },
    //牌库按钮响应
    OnButtonCardData : function(tag, ref) {

        //判断之前按钮是否为空，不为空取消颜色
        if (this.t_btIndexNow != null) {
            this.t_btIndexNow.setColor(cc.color(255, 255, 255));
        }
        //为当前按钮变色
        tag = tag - 1000;
        ref.setColor(cc.color(176, 204, 243, 0));
        this.t_btIndexNow = ref;
        //判断牌库当前是否有牌，有则清除，无则选中
        if (this.t_cbCardData[tag] != 0) {
            var nValue = math_mod(this.t_cbCardData[tag], 16);
            var nColor = Math.floor(this.t_cbCardData[tag] / 16);
            var num = nColor * 10 + nValue;
            this.t_cbSurplusCard[num] = this.t_cbSurplusCard[num] + 1;
            this.t_spriteCard[num].setVisible(true);
            this.t_cbCardData[tag] = 0;
            this.CleanCard(null, tag);
            this.t_cbIndexNow = tag;
        }
        else
            this.t_cbIndexNow = tag;
    },
    //显示或影藏卡层
    showCardText : function(bVisible) {
        //this.CleanCard()
        this._textCardLayer.setVisible(bVisible);
    },
    //绘画全卡牌显示
    showCardData : function(wData) {
        for (i = 0; i < 136; i++) {
            if (wData[i] != 0 && this.t_spriteData[i] == null) {
                this.showCardOne(i, wData[i]);
            }
        }
    },
    //绘画卡牌显示
    showCardOne : function(cbNumber,cbValue) {
        var nValue = math_mod(cbValue, 16);
        var nColor = Math.floor(cbValue / 16);
        var str = ccmd.RES_PATH + "game/font_small/font_" + nColor + "_" + nValue + ".png";
        this.t_spriteData[cbNumber] = cc.Sprite.create(str);
        //this._textCardLayer.addChild(this.t_spriteData[cbNumber], 6); TODO
        if (cbNumber >= 137) {
            return;
        }
        else if (cbNumber >= 136) {
            this.t_spriteData[cbNumber].setPosition(192, 535);
        }
        else if (cbNumber >= 119) {
            var num = cbNumber - 119;
            this.t_spriteData[cbNumber].setPosition(1050 - ((num) * 47), 535);
        }
        else if (cbNumber >= 97) {
            var num = cbNumber - 97;
            this.t_spriteData[cbNumber].setPosition(1050 - ((num) * 47), 460);
        }
        else if (cbNumber >= 75) {
            var num = cbNumber - 75;
            this.t_spriteData[cbNumber].setPosition(1050 - ((num) * 47), 389);
        }
        else if (cbNumber >= 53) {
            var num = cbNumber - 53;
            this.t_spriteData[cbNumber].setPosition(1050 - ((num) * 47), 318);
        }
        else if (cbNumber >= 40) {
            var num = cbNumber - 40;
            this.t_spriteData[cbNumber].setPosition(623 - ((num) * 47), 239);
        }
        else if (cbNumber >= 27) {
            var num = cbNumber - 27;
            this.t_spriteData[cbNumber].setPosition(1283 - ((num) * 47), 239);
        }
        else if (cbNumber >= 14) {
            var num = cbNumber - 14;
            this.t_spriteData[cbNumber].setPosition(623 - ((num) * 47), 166);
        }
        else if (cbNumber >= 1) {
            this.t_spriteData[cbNumber].setPosition(1283 - ((cbNumber - 1) * 47), 166);
        }
        else
            return;
    },
    //清空绘画卡牌
    CleanCard : function(cbValue,cbNum) {
        //清空单张
        if (cbValue == null && cbNum != null) {
            this.t_spriteData[cbNum].setVisible(false);
            this.t_spriteData[cbNum] = null;
            return;
        }
        //清空全部
        for (i = 0; i < 136; i++) {
            if (this.t_spriteData[i] != null) {
                this.t_spriteData[i].setVisible(false);
                this.t_spriteData[i] = null;
            }
            this.t_spriteData[i] = null;
            this.t_cbCardData[i] = 0;
            if (i < 37) {
                this.t_cbSurplusCard[i] = 4;
                this.t_cbAutoSurplusCard[i] = 4;
            }
        }
        //显示全部按钮牌精灵
        this.t_cbIndexNow = 0;
        for (i = 0; i < 37; i++) {
            if (math_mod(i, 10) != 0) {
                this.t_spriteCard[i].setVisible(true);
            }
        }
    },
    //自动补全牌值
    AutoCard : function() {
        cc.log("自动补齐");
        if (this.t_btIndexNow != null) {
            this.t_btIndexNow.setColor(cc.color(255, 255, 255));
        }
        //判断现在缺少哪些牌
        for (i = 0; i < 136; i++) {
            if (this.t_cbCardData[i] != 0) {
                for (j = 0; j < 37; j++) {
                    var nValue = math_mod(this.t_cbCardData[i], 16);
                    var nColor = Math.floor(this.t_cbCardData[i] / 16);
                    var num = nColor * 10 + nValue;
                    if (num == j) {
                        this.t_cbAutoSurplusCard[num] = this.t_cbAutoSurplusCard[num] - 1;
                        break;
                    }
                }
            }
        }
        //补齐缺少牌
        var count = 1;
        for (i = 0; i < 136; i++) {
            if (this.t_cbCardData[i] == 0) {
                for (j = 0; j < 37; j++) {
                    if (this.t_cbAutoSurplusCard[j] != 0 && math_mod(j, 10) != 0) {
                        this.t_cbCardData[i] = math_mod(j, 10) + Math.floor(j / 10) * 16;
                        this.t_cbAutoSurplusCard[j] = this.t_cbAutoSurplusCard[j] - 1;
                        break;
                    }
                }
            }
        }
        //绘画牌
        this.showCardData(this.t_cbCardData);
        //清空记录
        for (i = 0; i < 37; i++) {
            this.t_cbSurplusCard[i] = 0;
            this.t_cbAutoSurplusCard[i] = 4;
            //隐藏按钮牌精灵
            if (math_mod(i, 10) != 0) {
                this.t_spriteCard[i].setVisible(false);
            }
        }
        cc.log("11111");
    },
    //判断牌值是否有空
    isAutoCard : function() {

        var data = [];
        for (i = 0; i < 136; i++) {
            if (this.t_cbCardData[i] == 0) {
                return false;
            }
            data[i] = this.t_cbCardData[i];
        }

        for (i = 0; i < 136; i++) {
            this.t_cbSendCardData[i] = data[136 - i - 1];
        }
        return true;
    },
    //分析牌值
    AnalysisCard : function(nValue,nColor) {

        var str = null;
        if (nColor == 0) {
            str = nValue + "万";
        }
        else if (nColor == 1) {
            str = nValue + "条";
        }
        else if (nColor == 2) {
            str = nValue + "筒";
        }
        else if (nColor == 3) {
            if (nValue == 1) {
                str = "东";
            }
            else if (nValue == 2) {
                str = "西";
            }
            else if (nValue == 3) {
                str = "南";
            }
            else if (nValue == 4) {
                str = "北";
            }
            else if (nValue == 5) {
                str = "中";
            }
            else if (nValue == 6) {
                str = "发";
            }
            else if (nValue == 7) {
                str = "白";
            }
        }
        return str;
    },
    //显示字
    showTextWords : function(str) {
        this.t_textWarning.setVisible(true);
        this.t_textWarning.setString(str);
        var del = new cc.DelayTime(1);
        var call = new cc.CallFunc(function () {
            this.t_textWarning.setVisible(false);
        });
        var seq = new cc.Sequence(del, call);
        this.t_textWarning.runAction(seq);
    },
    //查找空牌位置
    FindDataCard : function(str) {
        for (i = 0; i < 136; i++) {
            if (this.t_cbCardData[i] == 0) {
                return i;
            }
        }
        return 0;
    },
    //显示规则
    showPlayRule : function(bShow) {

        this._playRule.setVisible(false);
        if (this.m_bShowPlayRule != true) {
            if (bShow == true) {
                this.initPlayRule();
            }
        }
        else {
            for (i = 0; i < 3; i++) {
                if (this.m_spPlayRule[i] != null) {
                    this.m_spPlayRule[i].setVisible(bShow);
                }
            }
        }
    },
    //创建规则
    initPlayRule : function() {
        if (this.m_bShowPlayRule == true) {
            return;
        }
        this._playRule.setVisible(false);
        this._scene.findGameRule();
        var cbDianPao, cbWindOrBird, cbBigWind = this._scene.getPlayHow();
        for (i = 0; i < 3; i++) {
            if (this.m_spPlayRule[i] != null) {
                this.m_spPlayRule[i].setVisible(false);
            }
            this.m_spPlayRule[i] = null;
        }

        var posX = 27;
        var posY = 650;
        var strFile = ccmd.RES_PATH + "game/playRule/sp_play_fenggang_0.png";
        var num = 1;
        //带不带攒风杠鸟杠
        if (cbWindOrBird == 0) {
            strFile = ccmd.RES_PATH + "game/playRule/sp_play_fenggang_0.png";
        }
        else {
            strFile = ccmd.RES_PATH + "game/playRule/sp_play_fenggang_2.png";
        }

        this.m_spPlayRule[num] = new cc.Sprite(strFile);
        this.m_spPlayRule[num].setPosition(cc.p(posX, posY));
        this.m_spPlayRule[num].setAnchorPoint(0, 0.5);
        this.addChild(this.m_spPlayRule[num], 7);
        this.m_spPlayRule[num].setVisible(true);
        num = num + 1;
        //点炮包三家
        if (cbDianPao != 0) {
            strFile = ccmd.RES_PATH + "game/playRule/sp_play_fenggang_3.png";
            this.m_spPlayRule[num] = cc.Sprite.create(strFile);
            posY = posY - 30;
            this.m_spPlayRule[num].setPosition(cc.p(posX, posY));
            this.m_spPlayRule[num].setAnchorPoint(0, 0.5);
            this.addChild(this.m_spPlayRule[num], 7);
            this.m_spPlayRule[num].setVisible(true);
            num = num + 1;
        }

        //大风刮一吃到底
        if (cbBigWind != 0) {
            strFile = ccmd.RES_PATH + "game/playRule/sp_play_fenggang_1.png";
            this.m_spPlayRule[num] = new cc.Sprite(strFile);
            posY = posY - 30;
            this.m_spPlayRule[num].setPosition(cc.p(posX, posY));
            this.m_spPlayRule[num].setAnchorPoint(0, 0.5);
            this.addChild(this.m_spPlayRule[num], 7);
            this.m_spPlayRule[num].setVisible(true);
            num = num + 1;
        }

        this.m_bShowPlayRule = true;
    },
    //刷新玩家分数
    upDataScore : function(viewId,cbScore) {
        if (viewId > 4 || viewId <= 0) {
            return;
        }
        var score = cbScore;
        if (cbScore < 0) {
            score = -score;
        }
        var strScore = this.numInsertPoint(score);
        if (cbScore < 0) {
            strScore = "." + strScore;
        }
        var m_Node = this.nodePlayer[viewId].getChildByTag(GameViewLayer.ASLAB_SCORE);
        m_Node.setString(strScore);
    },
    upDataGameTimes : function(cbCount,cbLimit) {
        this.m_txtGameTimes.setString(cbCount + 1 + "/" + cbLimit);
    }
});

GameViewLayer.SP_TABLE_BT_BG = 1;					//桌子按钮背景
GameViewLayer.BT_CHAT = 41;				//聊天按钮
GameViewLayer.BT_SET = 42;				//设置
GameViewLayer.BT_EXIT = 43;				//退出按钮
GameViewLayer.BT_TRUSTEE = 44;				//托管按钮
GameViewLayer.BT_HOWPLAY = 45;				//玩法按钮

GameViewLayer.BT_SWITCH = 2; 				//按钮开关按钮
GameViewLayer.BT_START = 3;				//开始按钮

GameViewLayer.BT_VOICE = 5;					//语音按钮（语音关闭）
// BT_VOICEOPEN 			= 55				//语音按钮（语音开启）

GameViewLayer.SP_GAMEBTN = 6;					//游戏操作按钮
GameViewLayer.SP_GAMEBTN_CHI = 801;				//游戏吃多按钮
GameViewLayer.SP_GAMEBTN_GANG = 800;				//游戏杠多按钮
//<===============<QTC_MODIFY>================//
GameViewLayer.BT_EAT = 962;				//游戏操作按钮吃(只有一种方案)
GameViewLayer.BT_EAT_GROUP = 963;				//游戏操作按钮吃(有多于一种方案)
GameViewLayer.BT_EAT_SUB_1 = 964;				//游戏操作按钮吃(二级按钮 方案1)
GameViewLayer.BT_EAT_SUB_2 = 965;				//游戏操作按钮吃(二级按钮 方案2)
GameViewLayer.BT_EAT_SUB_3 = 966;				//游戏操作按钮吃(二级按钮 方案3)
GameViewLayer.BT_BUMP = 967;				//游戏操作按钮碰
GameViewLayer.BT_BRIGDE = 968;				//游戏操作按钮杠(只有一种方案)
GameViewLayer.BT_BRIGDE_GROUP = 969;				//游戏操作按钮杠(有多于一种方案)
GameViewLayer.BT_BRIGDE_SUB_1 = 970;				//游戏操作按钮杠(二级按钮 方案1)
GameViewLayer.BT_BRIGDE_SUB_2 = 971; 				//游戏操作按钮杠(二级按钮 方案2)
GameViewLayer.BT_BRIGDE_SUB_3 = 972;				//游戏操作按钮杠(二级按钮 方案3)
GameViewLayer.BT_BRIGDE_SUB_4 = 973;				//游戏操作按钮杠(二级按钮 方案4)
GameViewLayer.BT_LISTEN = 974;				//游戏操作按钮听
GameViewLayer.BT_WIN = 975;				//游戏操作按钮胡
GameViewLayer.BT_PASS = 976;				//游戏操作按钮过
//>===============<QTC_MODIFY>================//

GameViewLayer.SP_ROOMINFO = 7;					//房间信息
GameViewLayer.TEXT_ROOMNUM = 1;				//房间信息房号
GameViewLayer.TEXT_ROOMNAME = 2;				//房间信息房名
GameViewLayer.TEXT_INDEX = 3;				//房间信息局数
GameViewLayer.TEXT_INNINGS = 4;				//房间信息剩多少局

GameViewLayer.SP_ANNOUNCEMENT = 8;					//公告

GameViewLayer.SP_CLOCK = 9;					//计时器
GameViewLayer.ASLAB_TIME = 1;					//计时器时间

GameViewLayer.SP_LISTEN = 10;				//听牌提示

GameViewLayer.NODEPLAYER_1 = 11;				//玩家节点1
GameViewLayer.NODEPLAYER_2 = 12;				//玩家节点2
GameViewLayer.NODEPLAYER_3 = 13;				//玩家节点3
GameViewLayer.NODEPLAYER_4 = 14;				//玩家节点4
GameViewLayer.SP_HEAD = 1;				//玩家头像
GameViewLayer.SP_HEADCOVER = 2;					//玩家头像覆盖层
GameViewLayer.TEXT_NICKNAME = 3;				//玩家昵称
GameViewLayer.ASLAB_SCORE = 4;				//玩家金币
GameViewLayer.SP_READY = 5;				//玩家准备标志
GameViewLayer.SP_TRUSTEE = 6;					//玩家托管标志
GameViewLayer.SP_BANKER = 7;				//庄家
GameViewLayer.SP_ROOMHOST = 8;				//房主

GameViewLayer.SP_PLATE = 19;				//牌盘
GameViewLayer.SP_PLATECARD = 1;					//排盘中的牌

GameViewLayer.TEXT_REMAINNUM = 20;				//牌堆剩多少张

GameViewLayer.SP_SICE1 = 27;				//筛子1
GameViewLayer.SP_SICE2 = 28;				//筛子2
GameViewLayer.SP_OPERATFLAG = 29;				//操作标志

GameViewLayer.SP_TRUSTEEBG = 1;				//托管底图
GameViewLayer.BT_TRUSTEECANCEL = 30; 				//取消托管

GameViewLayer.SHOW_TING =  false;
GameViewLayer.PLAY_RLUE = 2000;				//游戏玩法
GameViewLayer.SP_CLOCK_BG = 2001;              //时间背景
GameViewLayer.BT_SET_NEW = 3001;				//游戏设置
GameViewLayer.BT_CHAT_NEW = 3002;				//游戏聊天

GameViewLayer.BT_SET_NEW1 = 362;  // 游戏设置
GameViewLayer.BT_CHAT_NEW1 = 363;  //游戏聊天

var maxScore = 0;
var rotationAngle = [180,90,0,270];
