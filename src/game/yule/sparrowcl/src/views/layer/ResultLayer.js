/*
Author: Li
*/

var posBanker = [cc.p(95, 559), cc.p(95, 395), cc.p(95, 281), cc.p(95, 170)];
var posScoreS = [cc.p(1115, 555), cc.p(920, 420), cc.p(920, 315), cc.p(920, 200)];
var posScoreY = [cc.p(1115, 510), cc.p(920, 380), cc.p(920, 270), cc.p(920, 155)];
var posDianPao = [cc.p(0, 0), cc.p(515, 437), cc.p(515, 324), cc.p(515, 212)];

var ResultLayer = cc.Layer.extend({

////////-***< ysy _ end  >***//////////////////-

    ctor : function(scene) {
        this._super();
        this._scene = scene;
        var self = this;
        var layer = ccs.load(res.GameResultLayer_json).node;
        this.addChild(layer);
        this.onInitData();
        //ExternalFun.registerTouchEvent(this, true);

        var btRecodeShow = layer.getChildByTag(ResultLayer.TAG_BT_RECODESHOW);
        btRecodeShow.setVisible(false);
        btRecodeShow.addClickEventListener(function (ref) {
            self.recodeShow();
        });

        var btContinue = layer.getChildByTag(ResultLayer.TAG_BT_CONTINUE);
        btContinue.setPositionX(1334/2);
        btContinue.addClickEventListener(function (ref) {
            self.hideLayer();
            self._scene.onButtonClickedEvent(GameViewLayer.BT_START);
        });

        this.nodeUser = [];
        for (i = 0; i < ccmd.GAME_PLAYER; i++) {
            this.nodeUser[i] = layer.getChildByTag(ResultLayer.TAG_NODE_USER_1 + i);
            this.nodeUser[i].setLocalZOrder(1);
            var headcover = this.nodeUser[i].getChildByTag(ResultLayer.TAG_SP_HEADCOVER);
            headcover.setLocalZOrder(1);
            //个人麻将
            var nodeUserCard = new cc.Node();
            nodeUserCard.setTag(ResultLayer.TAG_NODE_CARD);
            this.nodeUser[i].addChild(nodeUserCard);
            //个人头像
            var head = new cc.Node();
            head.setTag(ResultLayer.TAG_HEAD);
            this.nodeUser[i].addChild(head);
        }
        //奖码
        this.nodeAwardCard = new cc.Node();
        this.addChild(this.nodeAwardCard);
        //剩余麻将
        this.nodeRemainCard = new cc.Node();
        this.addChild(this.nodeRemainCard);
        //庄标志
        this.spBanker = layer.getChildByTag(ResultLayer.TAG_SP_BANKER);
        this.spBanker.setLocalZOrder(1);
    //////////////////////////////-ysy_start////////////////////////////////////////////////////////////////////////-
        //-点炮
        this.textDianpao = layer.getChildByTag(840);
        this.textDianpao.setLocalZOrder(2);
        this.textDianpao.setVisible(false);
        //-胡型
        this.textHuiType = layer.getChildByTag(112);
        this.textHuiType.setLocalZOrder(2);
        this.textHuiType.setString("0000000000000000000");
        //局号
        this.textTimesNow = layer.getChildByTag(841);
        this.textTimesNow.setLocalZOrder(2);
        this.textTimesNow.setString("0000000000000000000");
        //房间号码
        this.textRoomID = layer.getChildByTag(842);
        this.textRoomID.setLocalZOrder(2);
        //胡牌类型
        this.spHuiType = [];
        var nHuiType = 800;
        this.txtSscore = [];
        this.txtYscore = [];
        for (i = 0; i < 11; i++) {
            this.spHuiType[i] = layer.getChildByTag(800 + i + 1);
            this.spHuiType[i].setLocalZOrder(2);
            this.spHuiType[i].setVisible(false);
        }
        for (i = 0; i < 4; i++) {
            this.txtSscore[i] = this.nodeUser[i].getChildByTag(820 + i + 1);
            this.txtSscore[i].setAnchorPoint(cc.p(0, 0.5));
            this.txtSscore[i].setTag(820 + i + 1);

            this.txtYscore[i] = this.nodeUser[i].getChildByTag(830 + i + 1);
            this.txtYscore[i].setAnchorPoint(cc.p(0, 0.5));
            this.txtYscore[i].setTag(830 + i + 1);
        }
    //////////////////////////////ysy_end////////////////////////////////////////////////////////////////////////////
    },

    onInitData : function() {
        this.winnerIndex = null;
        this.bShield = false;
    },

    onResetData : function() {
        //body
        this.winnerIndex = null;
        this.bShield = false;
        this.nodeAwardCard.removeAllChildren();
        this.nodeRemainCard.removeAllChildren();
        for (i = 0; i < ccmd.GAME_PLAYER; i++) {
            this.nodeUser[i].getChildByTag(ResultLayer.TAG_NODE_CARD).removeAllChildren();
            this.nodeUser[i].getChildByTag(ResultLayer.TAG_HEAD).removeAllChildren();
            var score = this.nodeUser[i].getChildByTag(ResultLayer.TAG_ASLAB_SCORE);
            if (score) {
                score.removeFromParent();
            }
        }
    },

    onTouchBegan : function(touch, event) {
        var pos = touch.getLocation();
        //cc.log(pos.x, pos.y)
        var rect = cc.rect(17, 25, 1330, 750);
        if (!cc.rectContainsPoint(rect, pos)) {
            this.hideLayer();
        }
        return this.bShield;
    },

    showLayer : function(resultList, cbAwardCard, cbRemainCard, wBankerChairId, cbHuCard,cbSocre,cbHuiType,cbTimesNow,cbHuiCard,cbRoomID,cbDianpao,cbHuScore,cbBpAnGangList) {
        appdf.assert(resultList == null && cbAwardCard == null && cbRemainCard == null);
        var width = 44;
        var height = 67;
        for (i = 0; i < resultList.length; i++) {
            if (resultList[i].cbChHuKind >= GameLogic.WIK_CHI_HU) {
                this.winnerIndex = i;
                break;
            }
        }
        ////////-ysy_start
        //房间号
        if (cbRoomID != null) {
            this.textRoomID.setString("" + cbRoomID);
            this.textRoomID.setVisible(true);
        }
        else
            this.textRoomID.setVisible(false);

        //点炮
        this.textDianpao.setString("点炮");
        var nDianPao = 0;
        if (nDianPao != 0) {
            nDianPao = this.switchToOrder(cbDianpao);
        }
        this.textDianpao.setVisible(false);
        if (nDianPao > 0 && nDianPao < 4) {
            this.textDianpao.setVisible(true);
            this.textDianpao.setPosition(posDianPao[nDianPao]);
        }
        //局号
        this.textTimesNow.setString("" + cbTimesNow);
        //胡牌类型
        var cbWinner = -1;
        var cbWinnerScore = 0;
        for (i = 0; i < cbSocre.length; i++) {
            if (cbSocre[i] > 0) {
                if (cbWinnerScore < cbSocre[i]) {
                    cbWinnerScore = cbSocre[i];
                    cbWinner = i;
                }
            }
        }
        var sHuiType = [];
        if (cbWinner != -1) {
            var dHuiType = cbHuiType[cbWinner][0];

            if (dHuiType >= ResultLayer.CHR_YOU_YU) {      //有鱼
                dHuiType = dHuiType - ResultLayer.CHR_YOU_YU;
                var num = sHuiType.length;
                sHuiType[num] = 101;
            }
            if (dHuiType >= ResultLayer.CHR_GANG_KAI) {      //杠开
                dHuiType = dHuiType - ResultLayer.CHR_GANG_KAI;
                var num = sHuiType.length;
                sHuiType[num] = 102;
            }
            if (dHuiType >= ResultLayer.CHR_ZHAN_LI) {      //站立
                dHuiType = dHuiType - ResultLayer.CHR_ZHAN_LI;
                var num = sHuiType.length;
                sHuiType[num] = 103;
            }
            if (dHuiType >= ResultLayer.CHR_SHUANG_3) {      //三双
                dHuiType = dHuiType - ResultLayer.CHR_SHUANG_3;
                var num = sHuiType.length;
                sHuiType[num] = 104;
            }
            if (dHuiType >= ResultLayer.CHR_SHUANG_4) {      //四双
                dHuiType = dHuiType - ResultLayer.CHR_SHUANG_4;
                var num = sHuiType.length;
                sHuiType[num] = 105;
            }
            if (dHuiType >= ResultLayer.CHR_SHUANG_6) {      //六双
                dHuiType = dHuiType - ResultLayer.CHR_SHUANG_6;
                var num = sHuiType.length;
                sHuiType[num] = 106;
            }
            if (dHuiType >= ResultLayer.CHR_SHUANG_7) {      //七双
                dHuiType = dHuiType - ResultLayer.CHR_SHUANG_7;
                var num = sHuiType.length;
                sHuiType[num] = 107;
            }
            if (dHuiType >= ResultLayer.CHR_SHUANG_8) {      //八双
                dHuiType = dHuiType - ResultLayer.CHR_SHUANG_8;
                var num = sHuiType.length;
                sHuiType[num] = 108;
            }
            if (dHuiType >= ResultLayer.CHR_SHUANG_9) {      //九双
                dHuiType = dHuiType - ResultLayer.CHR_SHUANG_9;
                var num = sHuiType.length;
                sHuiType[num] = 109;
            }
            if (dHuiType >= ResultLayer.CHR_ZI_MO) {      //自摸
                dHuiType = dHuiType - ResultLayer.CHR_ZI_MO;
                var num = sHuiType.length;
                sHuiType[num] = 110;
            }
            if (dHuiType >= ResultLayer.CHR_ZHUANG_HU) {      //庄户
                dHuiType = dHuiType - ResultLayer.CHR_ZHUANG_HU;
                var num = sHuiType.length;
                sHuiType[num] = 111;
            }
        }
        this.textHuiType.setString("");
        //绘画胡型
        var nHuiType = sHuiType.length;
        var posX = 535;
        var posY = 606;
        for (i = 0; i < nHuiType; i++) {
            this.spHuiType[sHuiType[i] - 100].setPosition(posX, posY);
            this.spHuiType[sHuiType[i] - 100].setVisible(true);
            posX = posX + 75;
        }
        //绘画会牌
        if (ccmd.EXISTENCE_HUI == true) {
            var nValue = math_mod(cbHuiCard, 16);
            var nColor = Math.floor(cbHuiCard / 16);
            var cardHui = new cc.Sprite(ccmd.RES_PATH + "game/font_big/card_up.png");
            cardHui.setPosition(1194, 300);
            cardHui.addChild(this);
            var cardHuiValue = new cc.Sprite(ccmd.RES_PATH + "game/font_big/font_" + nColor + "_" + nValue + ".png");
            cardHuiValue.setPosition(1194, 300);
            this.addChild(cardHuiValue);
        }
        ////////-ysy_end
        var nBankerOrder = 1;
        for (i = 0; i < ccmd.GAME_PLAYER; i++) {
            var order = this.switchToOrder(i);
            if (i < resultList.length) {
                this.nodeUser[order].setVisible(true);
                //头像
                var tempHead = (new PopupInfoHead()).createClipHead(resultList[i].userItem, 65);
                var head = tempHead[0];
                head.setPosition(0, 2);		//初始位置
                head.enableHeadFrame(false);
                head.enableInfoPop(false);
                head.setTag(ResultLayer.TAG_HEAD);
                this.nodeUser[order].addChild(head);
                var strFile = null;
                if (resultList[i].lScore >= 0) {
                    //strFile = ccmd.RES_PATH+"gameResult/num_win.png"
                    strFile = "+ ";
                }
                else {
                    //strFile = ccmd.RES_PATH+"gameResult/num_lose.png"
                    resultList[i].lScore = -resultList[i].lScore;
                    strFile = "- ";
                }
                if (cbHuScore[i] >= 0) {
                    //strFile2 = ccmd.RES_PATH+"gameResult/num_win.png"
                    strFile2 = "+ ";
                }
                else {
                    //strFile = ccmd.RES_PATH+"gameResult/num_lose.png"
                    cbHuScore[i] = -cbHuScore[i];
                    strFile2 = "- ";
                }
                var strNum = strFile + resultList[i].lScore;
                var strNum2 = strFile2 + cbHuScore[i];
                this.txtSscore[order].setString(strNum);
                this.txtYscore[order].setString(strNum2);
                //昵称
                var textNickname = this.nodeUser[order].getChildByTag(ResultLayer.TAG_TEXT_NICKNAME);
                textNickname.setString("");
                szNickName = resultList[i].userItem.szNickName;
                //var strNickname = string.EllipsisByConfig(szNickName, 150, string.getConfig("fonts/round_body.ttf", 32)); //TODO
                var strNickname = new cc.LabelTTF(szNickName, res.round_body_ttf, 28);
                textNickname.removeAllChildren();
                textNickname.addChild(strNickname);
                //个人麻将
                var nodeUserCard = this.nodeUser[order].getChildByTag(ResultLayer.TAG_NODE_CARD);
                var fX = 82;
                for (j = 0; j < resultList[i].cbBpBgCardData.length; j++) { 											//碰杠牌
                    //牌底
                    if (this.setAnGangValue(cbBpAnGangList[i], resultList[i].cbBpBgCardData[j]) == false) {
                        //var rectX = CardLayer.switchToCardRectX(resultList[i].cbBpBgCardData[j])
                        var card = new cc.Sprite(ccmd.RES_PATH + "game/font_small/card_down.png");
                        //.setTextureRect(cc.rect(width*rectX, 0, width, height))
                        card.setPosition(fX, -7);
                        nodeUserCard.addChild(card);
                        //字体
                        var nValue = math_mod(resultList[i].cbBpBgCardData[j], 16);
                        var nColor = Math.floor(resultList[i].cbBpBgCardData[j] / 16);
                        var sp1 = new cc.Sprite(ccmd.RES_PATH + "game/font_small/font_" + nColor + "_" + nValue + ".png");
                        sp1.setPosition(width / 2, height / 2 + 8);
                        card.addChild(sp1);
                    }
                    else {
                        var card = new cc.Sprite(ccmd.RES_PATH + "game/font_small/card_back.png");
                        //.setTextureRect(cc.rect(width*rectX, 0, width, height))
                        card.setPosition(fX, -7);
                        nodeUserCard.addChild(card);
                    }

                    if (resultList[i].cbBpBgCardData[j] == resultList[i].cbBpBgCardData[j + 1] || this.setGangShow(resultList[i].cbBpBgCardData, j)) {
                        fX = fX + width;
                    }
                    else
                        fX = fX + 52;
                    //末尾
                    if (j == resultList[i].cbBpBgCardData.length) {
                        fX = fX + 20;
                    }
                }
                for (j = 0; j < resultList[i].cbCardData.length; j++) {  											 	//剩余手牌
                    //牌底
                    //var rectX = CardLayer.switchToCardRectX(resultList[i].cbCardData[j])
                    var card = new cc.Sprite(ccmd.RES_PATH + "game/font_small/card_down.png");
                    //.setTextureRect(cc.rect(width*rectX, 0, width, height))
                    card.setPosition(fX, -7);
                    nodeUserCard.addChild(card);
                    //字体
                    var nValue = math_mod(resultList[i].cbCardData[j], 16);
                    var nColor = Math.floor(resultList[i].cbCardData[j] / 16);
                    var sp2 = new cc.Sprite(ccmd.RES_PATH + "game/font_small/font_" + nColor + "_" + nValue + ".png");
                    sp2.setPosition(width / 2, height / 2 + 8);
                    card.addChild(sp2);

                    fX = fX + width;
                }
                //胡的那张牌
                if (resultList[i].cbChHuKind >= GameLogic.WIK_CHI_HU) {
                    fX = fX + 20;
                    //牌底
                    //var rectX = CardLayer.switchToCardRectX(cbHuCard)
                    var huCard = new cc.Sprite(ccmd.RES_PATH + "game/font_small/card_down.png");
                    //.setTextureRect(cc.rect(width*rectX, 0, width, height))
                    huCard.setPosition(fX, -7);
                    nodeUserCard.addChild(huCard);
                    //字体
                    var nValue = math_mod(cbHuCard, 16);
                    var nColor = Math.floor(cbHuCard / 16);
                    var sp3 = new cc.Sprite(ccmd.RES_PATH + "game/font_small/font_" + nColor + "_" + nValue + ".png");
                    sp3.setPosition(width / 2, height / 2 + 8);
                    huCard.addChild(sp3);
                    //自摸或放炮标记
                    // var sp4 = new cc.Sprite("sp_ziMo.png");
                    var sp4 = new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("sp_ziMo.png"));
                    sp4.setPosition(fX + 21, -7 + 32);
                    nodeUserCard.addChild(sp4);
                    sp4.setVisible(false);
                }
                //奖码
                fX = fX + 110;
                for (j = 0; j < resultList[i].cbAwardCard.length; j++) {
                    //var rectX = CardLayer.switchToCardRectX(resultList[i].cbAwardCard[j])
                    //var x = 788 + 52*j
                    var y = -7;
                    //牌底
                    var card = new cc.Sprite(ccmd.RES_PATH + "game/font_small/card_down.png");
                    //.setTextureRect(cc.rect(width*rectX, 0, width, height))
                    card.setPosition(fX, y);
                    nodeUserCard.addChild(card);
                    card.setVisible(false);
                    //字体
                    var nValue = math_mod(resultList[i].cbAwardCard[j], 16);
                    var nColor = Math.floor(resultList[i].cbAwardCard[j] / 16);
                    var sp5 = new cc.Sprite(ccmd.RES_PATH + "game/font_small/font_" + nColor + "_" + nValue + ".png");
                    sp5.setPosition(width / 2, height / 2 + 8);
                    card.addChild(sp5);
                    sp5.setVisible(false);
                    if (null != this.winnerIndex && (
                            nValue == 1 ||
                            nValue == 5 ||
                            nValue == 9 ||
                            resultList[i].cbAwardCard[j] == GameLogic.MAGIC_DATA)) {
                        // var sp6 = new cc.Sprite("sp_chooseFlag.png");
                        var sp6 = new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("sp_chooseFlag.png"));
                        sp6.setPosition(fX + 5, y - 30);
                        nodeUserCard.addChild(sp6);
                        sp6.setVisible(false);
                    }
                    fX = fX + 52;
                }
                //庄家
                if (wBankerChairId == resultList[i].userItem.wChairID) {
                    nBankerOrder = order;
                }
            }
            else
                this.nodeUser[order].setVisible(false);
        }
        //庄家
        this.setBanker(nBankerOrder);

        this.bShield = true;
        this.setVisible(true);
        this.setLocalZOrder(yl.MAX_INT);
    },

    hideLayer : function() {
        if (!this.isVisible()) {
            return;
        }
        this.onResetData();
        this.setVisible(false);
        this._scene.btStart.setVisible(true);
        //隐藏胡型
        for (i = 0; i < 10; i++) {
            this.spHuiType[i].setVisible(false);
        }
    },
    //0~3转换到0~3
    switchToOrder : function(index) {
        appdf.assert(index >= 0 && index < ccmd.GAME_PLAYER);
        if (this.winnerIndex == null) {
            return index;
        }
        var nDifference = ResultLayer.WINNER_ORDER - this.winnerIndex;
        var order = math_mod(index + nDifference, ccmd.GAME_PLAYER);
        return order;
    },

    setBanker : function(order) {
        appdf.assert(order != 0);
        this.spBanker.setPosition(posBanker[order]);
        this.spBanker.setVisible(true);
    },

    setGangShow : function(data,index) {
        var value = 0;
        if (data[index] != null && data[index] != 0) {
            value = data[index];
            if (value == 0x31 || value == 0x32 || value == 0x33) {
                if (data[index] != null || data[index] != 0) {
                    if (data[index] == value + 1) {
                        return true;
                    }
                }
            }
            if (value == 0x11) {
                if (data[index] != null && data[index] != 0) {
                    if (data[index] == 0x35) {
                        return true;
                    }
                }
            }
            if (value == 0x35 || value == 0x36) {
                if (data[index] != null && data[index] != 0) {
                    if (data[index] == value + 1) {
                        return true;
                    }
                }
            }
            if (data[index] != null && data[index] != 0 && data[index + 1] != null || data[index + 1] != 0) {
                if (data[index] == value + 1) {
                    if (data[index + 1] == value + 2) {
                        return true;
                    }
                }
            }
            if (data[index - 2] != null && data[index - 2] != 0 && data[index] != null || data[index] != 0) {
                if (data[index] == value + 1) {
                    if (data[index - 2] == value - 1) {
                        return true;
                    }
                }
            }
        }

        return false;
    },

    setAnGangValue : function(cbBpAnGangList,value) {
        if (cbBpAnGangList == null) {
            return false;
        }
        for (i = 0; i < cbBpAnGangList.length; i++) {
            if (cbBpAnGangList[0] == value) {
                return true;
            }
        }
        return false;
    },

    recodeShow : function() {
        var self = this;
        cc.log("战绩炫耀");
        if (!PriRoom) {
            return;
        }

        //GlobalUserItem.bAutoConnect = false
        PriRoom.getInstance().getPlazaScene().popTargetShare(function (target, bMyFriend) {
            bMyFriend = bMyFriend || false;

            function sharecall(isok) {
                if (typeof(isok) == "string" && isok == "true") {
                    showToast(self, "战绩炫耀成功", 2);
                }
            }

            var url = GlobalUserItem.szWXSpreaderURL || yl.HTTP_URL;
            // 截图分享
            var framesize = cc.director.getOpenGLView().getFrameSize();
            var area = cc.rect(0, 0, framesize.width, framesize.height);
            var imagename = "grade_share.jpg";
            if (bMyFriend) {
                imagename = "grade_share_" + (new Date()).getTime() + ".jpg";
            }
            ExternalFun.popupTouchFilter(0, false);
            // captureScreenWithArea(area, imagename, function (ok, savepath) {
            //     ExternalFun.dismissTouchFilter();
            //     if (ok) {
            //         if (bMyFriend) {
            //             PriRoom.getInstance().getTagLayer(PriRoom.LAYTAG.LAYER_FRIENDLIST, function (frienddata) {
            //                 PriRoom.getInstance().imageShareToFriend(frienddata, savepath, "分享我的战绩");
            //             });
            //         }
            //         else if (null != target) {
            //             MultiPlatform.getInstance().shareToTarget(target, sharecall, "我的战绩", "分享我的战绩", url, savepath, "true");
            //         }
            //     }
            // });
        });
    }
});

ResultLayer.TAG_NODE_USER_1 = 1;
ResultLayer.TAG_NODE_USER_2 = 2;
ResultLayer.TAG_NODE_USER_3 = 3;
ResultLayer.TAG_NODE_USER_4 = 4;
ResultLayer.TAG_SP_ROOMHOST = 5;
ResultLayer.TAG_SP_BANKER = 6;
ResultLayer.TAG_BT_RECODESHOW = 8;
ResultLayer.TAG_BT_CONTINUE = 9;

ResultLayer.TAG_SP_HEADCOVER = 1;
ResultLayer.TAG_TEXT_NICKNAME = 2;
ResultLayer.TAG_ASLAB_SCORE = 3;
ResultLayer.TAG_HEAD = 4;
ResultLayer.TAG_NODE_CARD = 5;

ResultLayer.WINNER_ORDER = 0;

////////-***< ysy _ start>***//////////////////-

ResultLayer.CHR_QIONG_HU = 0x00000001;		//穷胡
ResultLayer.CHR_PIAO_HU = 0x00000002;		//飘胡
ResultLayer.CHR_QING_YI_SE = 0x00000004;		//清一色
ResultLayer.CHR_QI_DUI = 0x00000008;		//七对
ResultLayer.CHR_SHI_SAN_YAO = 0x00000010;		//十三幺
ResultLayer.CHR_ZHUANG_HU = 0x00000020;		//庄胡
ResultLayer.CHR_ZI_MO = 0x00000040;		//自摸
ResultLayer.CHR_TIAN_HU = 0x00000080;		//天和(自摸胡 手中第一次够14张牌时 尚未有人吃碰杠过 庄家)
ResultLayer.CHR_DI_HU = 0x00000100;		//地和(自摸胡 手中第一次够14张牌时 尚未有人吃碰杠过 非庄家)
ResultLayer.CHR_REN_HU = 0x00000200;		//人和(点炮胡 手中从未够过14张牌时 尚未有人吃碰杠过 非庄家)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ResultLayer.CHR_SHUANG_9 = 0x00000400;		//9双(清一色)
ResultLayer.CHR_SHUANG_8 = 0x00000800;		//8双(绝张)
ResultLayer.CHR_SHUANG_7 = 0x00001000;		//7双(1叫飘)
ResultLayer.CHR_SHUANG_6 = 0x00002000;		//6双(多叫飘)
ResultLayer.CHR_SHUANG_4 = 0x00004000;		//4双(1叫)
ResultLayer.CHR_SHUANG_3 = 0x00008000;		//3双(多叫)
ResultLayer.CHR_ZHAN_LI = 0x00010000;		//站立(没有吃碰杠过)
ResultLayer.CHR_GANG_KAI = 0x00020000;		//杠开(岭上开花)
ResultLayer.CHR_YOU_YU = 0x00040000;		//有鱼(手里有幺鸡)