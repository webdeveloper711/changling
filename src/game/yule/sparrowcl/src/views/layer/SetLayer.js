/*
	author : Kil
	date : 2017-12-6
 */
var TAG_BT_MUSICON 		= 1;
var TAG_BT_MUSICOFF 	= 2;
var TAG_BT_EFFECTON 	= 3;
var TAG_BT_EFFECTOFF 	= 4;
var TAG_BT_EXIT 		= 5;

var SetLayer = cc.Layer.extend({
	ctor : function (scene) {
		this._super();
		var self = this;
		this._scene = scene;
		this.onInitData();	
	
		this.colorLayer = new cc.LayerColor(cc.color(0, 0, 0, 125));
        this.colorLayer.setContentSize(1334, 750);
        this.addChild(this.colorLayer);

        var customListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: false,
            onTouchBegan: function (touch, event) {
                cc.log("SetLayer : onTouchBegan");
                return true;
            },
            onTouchMoved: function (touch, event) {
                cc.log("SetLayer : onTouchMoved");
                return true;
            },
            onTouchEnded : function (touch, event) {
                cc.log("SetLayer : onTouchEnded");
                self.onTouchEnded(touch, event);
                return true;
            }
        });
        cc.eventManager.addListener(customListener,self);
	
		var funCallback = function(ref) {
            self.onButtonCallback(ref.getTag(), ref);
        };
		//UI
        this._csbNode = ccs.load(res.SetLayer_json).node;
        this.addChild(this._csbNode, 1);
		this.btMusicOn = this._csbNode.getChildByName("bt_music_on");
        this.btMusicOn.setTag(TAG_BT_MUSICON);
		this.btMusicOn.addClickEventListener(funCallback);
		this.btMusicOff = this._csbNode.getChildByName("bt_music_off");
        this.btMusicOff.setTag(TAG_BT_MUSICOFF);
		this.btMusicOff.addClickEventListener(funCallback);
		this.btEffectOn = this._csbNode.getChildByName("bt_effect_on");
        this.btEffectOn.setTag(TAG_BT_EFFECTON);
		this.btEffectOn.addClickEventListener(funCallback);
		this.btEffectOff = this._csbNode.getChildByName("bt_effect_off");
        this.btEffectOff.setTag(TAG_BT_EFFECTOFF);
		this.btEffectOff.addClickEventListener(funCallback);
		var btnClose = this._csbNode.getChildByName("bt_close");
        btnClose.setTag(TAG_BT_EXIT);
		btnClose.addClickEventListener(funCallback);
		this.sp_layerBg = this._csbNode.getChildByName("sp_setLayer_bg");
		//声音
		this.btMusicOn.setVisible(GlobalUserItem.bSoundAble);
		this.btMusicOff.setVisible(! GlobalUserItem.bSoundAble);
		this.btEffectOn.setVisible(GlobalUserItem.bVoiceAble);
		this.btEffectOff.setVisible(! GlobalUserItem.bVoiceAble);
		if (GlobalUserItem.bSoundAble) {
            cc.audioEngine.playMusic(res.audio_background01_mp3, true);
        }


		//版本号
		this.setVisible(false);
    },
	onInitData: function () {

    },
	onResetData: function () {

    },
	
	onButtonCallback : function(tag, ref) {
        if (tag == TAG_BT_MUSICON) {
            cc.log("音乐状态本开");
            GlobalUserItem.setSoundAble(false);
            this.btMusicOn.setVisible(false);
            this.btMusicOff.setVisible(true);
        } else if (tag == TAG_BT_MUSICOFF) {
            cc.log("音乐状态本关");
            GlobalUserItem.setSoundAble(true);
            this.btMusicOn.setVisible(true);
            this.btMusicOff.setVisible(false);
            cc.audioEngine.playMusic(res.audio_background01_mp3, true);
        } else if (tag == TAG_BT_EFFECTON) {
            cc.log("音效状态本开");
            GlobalUserItem.setVoiceAble(false);
            this.btEffectOn.setVisible(false);
            this.btEffectOff.setVisible(true);
        } else if (tag == TAG_BT_EFFECTOFF) {
            cc.log("音效状态本关");
            GlobalUserItem.setVoiceAble(true);
            this.btEffectOn.setVisible(true);
            this.btEffectOff.setVisible(false);
        } else if (tag == TAG_BT_EXIT) {
            cc.log("离开");
            this.hideLayer();
        }
    },
	onTouchEnded : function(touch, event) {
	    var x = touch.getLocation().x;
	    var y = touch.getLocation().y;
        var pos = cc.p(x, y);
        var rectLayerBg = this.sp_layerBg.getBoundingBox();
        if (!cc.rectContainsPoint(rectLayerBg, pos)) {
            this.hideLayer();
        }
        return true;
    },
	
	showLayer : function() {
        //this.colorLayer.setTouchEnabled = true;
        this.setVisible(true);
    },
	
	hideLayer : function() {
        //this.colorLayer.setTouchEnabled = false;
        this.setVisible(false);
        this.onResetData();
    }
});
