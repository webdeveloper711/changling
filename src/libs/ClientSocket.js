/*
* auther: Jong
* use WebSocket
 */

var WebSocket = WebSocket || window.WebSocket || window.MozWebSocket;

ClientSocket = cc.Node.extend({
    _socket: null,
    _buffer: null,
    handler: null, /* function(Cmd_Data) */

    _timerId: 0,

    ctor: function(handler) {
        cc.Node.prototype.ctor.call(this);

        this._socket = null;
        this._buffer = null;
        this.handler = handler;
    },

    connectSocket: function (host, port, pValidate) {
        var socket = new WebSocket("ws://" + host + ":" + port);
        socket.binaryType = "arraybuffer";

        var self = this;

        socket.onopen = function (event) {
            // socket is opened
            cc.log("socket is opened ");
            self.setheartbeatkeep(true);

            var cmdData = new Cmd_Data(0);
            cmdData.setcmdinfo(yl.MAIN_SOCKET_INFO, yl.SUB_SOCKET_CONNECT);
            self.handler(cmdData);
        };

        socket.onclose = function (p1) {
            // socket is closed
            cc.log("socket is closed. the code : " + p1.code);
            self.setheartbeatkeep(false);

            var cmdData = new Cmd_Data(0);
            cmdData.setcmdinfo(yl.MAIN_SOCKET_INFO, yl.SUB_SOCKET_CLOSE);
            self.handler(cmdData);
        };

        socket.onmessage = function (event) {
            if (self._buffer != null) {
                var cache = new ArrayBuffer(self._buffer.byteLength + event.data.byteLength);
                memcpy(cache, self._buffer, 0, self._buffer.byteLength);
                memcpy(cache, event.data, self._buffer.byteLength, event.data.byteLength);
                self._buffer = cache;
            }
            else {
                self._buffer = new ArrayBuffer(event.data.byteLength);
                memcpy(self._buffer, event.data, 0, event.data.byteLength);
            }

            while (self._buffer.byteLength > 0) {
                var pData = self.receiveCmdData();
                if (pData) {
                    cc.log("- Received " + pData.getlen() + "bytes");
                    if (pData.getmain() != 0 && pData.getsub() != 1) self.handler(pData);
                }
                else
                    break;
            }
        };

        socket.onerror = function (event) {
            cc.log('socket error: ' + event.code);

            var cmdData = new Cmd_Data(0);
            cmdData.setcmdinfo(yl.MAIN_SOCKET_INFO, yl.SUB_SOCKET_ERROR);
            self.handler(cmdData);
        };

        this._socket = socket;

        return true;
    },

    /**
     *
     * @param {Cmd_Data} cmdData
     */
    sendData: function (cmdData) {
        return this.sendSocketData(cmdData.getmain(), cmdData.getsub(), cmdData.m_pBuffer, cmdData.getlen());
    },

    releaseSocket: function () {
        this.closeSocket();
        this._socket = null;
    },

    closeSocket: function () {
        this._socket.close();
    },

    setheartbeatkeep: function (bKeep) {
        if (this._socket) {
            var self = this;
            var keepAlive = function() {
                var timeout = 2000;
                if (self._socket && self._socket.readyState == WebSocket.OPEN) {
                    self._socket.send('');
                }
                self._timerId = setTimeout(keepAlive, timeout);
            };
            var cancelKeepAlive = function() {
                if (self._timerId) {
                    clearTimeout(self._timerId);
                }
            };

            if (bKeep) {
                keepAlive();
            }
            else {
                cancelKeepAlive();
            }
        }
    },

    setdelaytime: function (t) {

    },

    setwaittime: function (t) {
        if (this._socket) {

        }
    }
});

ClientSocket.createSocket = function(handler) {
    return new ClientSocket(handler);
};

ClientSocket.prototype.sendSocketData = function(wMain, wSub, pData, dataSize) {
    if (this._socket.readyState !== WebSocket.OPEN) {
        cc.log("websocket instance wasn't ready...");
        return false;
    }

    var packInfo = new PacketInfo();

    var socketData = new SocketData();
    socketData.command.wMainCmdID = wMain;
    socketData.command.wSubCmdID = wSub;

    if (dataSize > 0) {
        socketData.dataBuffer = new ArrayBuffer(dataSize);
        memcpy(socketData.dataBuffer, pData, 0, dataSize);
        socketData.dataSize = dataSize;
    }

    var buf = socketData.toArrayBuffer();
    var dataView = new Uint8Array(buf);

    // buffer mapping
    var checkCode = 0;
    for (var i = 0; i < buf.byteLength; i++) {
        checkCode += dataView[i];
        dataView[i] = g_SendByteMap[dataView[i]];
    }
    packInfo.cbDataKind |= Packet.DK_MAPPED;
    packInfo.cbCheckCode = ~checkCode + 1;

    // encrypt
    //

    // compress
    /*var deflate = new Zlib.Deflate(buf);
    buf = deflate.compress();
    packInfo.cbDataKind |= Packet.DK_COMPRESS;*/

    // convert to byte array and send data
    packInfo.wPacketSize = PacketInfo.size + buf.byteLength;

    var packBuffer = new PacketBuffer(packInfo, buf);

    this._socket.send(packBuffer.toArrayBuffer());

    return true;
};

/**
 *
 * @returns {Cmd_Data}
 */
ClientSocket.prototype.receiveCmdData = function() {
    var packet = PacketBuffer.fromArrayBuffer(this._buffer, true);

    if (!packet) {
        return false;
    }
    //ExternalFun.dump(packet.info, "ClientSocket.receiveCmdData - Packet Info", 3);
    appdf.assert(this._buffer.byteLength >= packet.info.wPacketSize);

    // take data out of socket buffer
    var cache = new ArrayBuffer(this._buffer.byteLength - packet.info.wPacketSize);
    var cacheView = new Uint8Array(cache);
    var dataView = new Uint8Array(this._buffer);

    for (i = 0; i < cache.byteLength; i ++ ) {
        cacheView[i] = dataView[i + packet.info.wPacketSize];
    }

    this._buffer = cache;


    var buf = packet.buffer;

    // decompress
    if ((packet.info.cbDataKind & Packet.DK_COMPRESS) != 0) {
        var inflate = new Zlib.Inflate(buf);
        buf = inflate.decompress();
    }

    // decrypt
    //

    dataView = new Uint8Array(buf);

    // buffer un-mapping
    if ((packet.info.cbDataKind & Packet.DK_MAPPED) != 0) {
        var checkCode = packet.info.cbCheckCode;
        for (var i = 0; i < dataView.length; i++) {
            checkCode += g_RecvByteMap[dataView[i]];
            dataView[i] = g_RecvByteMap[dataView[i]];
        }

        if (checkCode != 0) {
            //cc.log("Checksum invalid");
            //return false;
        }
    }

    // construct Cmd_Data
    var sockData = SocketData.fromArrayBuffer(packet.buffer);

    if (!sockData) {
        cc.log('invalid socket data');
        return false;
    }
    //ExternalFun.dump(sockData, "ClientSocket.receiveCmdData - Socket Data", 3);

    var cmdData = new Cmd_Data(sockData.dataSize);
    cmdData.setcmdinfo(sockData.command.wMainCmdID, sockData.command.wSubCmdID);
    cmdData.setbuffer(sockData.dataBuffer);

    return cmdData;
};