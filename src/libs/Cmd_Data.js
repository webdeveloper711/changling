/*
* author: Jong
 */

function Cmd_Data(length) {
    this.m_wMain = 0;   //主命令
    this.m_wSub = 0;    //子命令

    this.m_wMaxLength = 512;		//数据长度
    this.m_wCurIndex = 0;		//操作游标

    this.m_bAutoLen = true;

    if (length > 0) {
        this.m_bAutoLen = false;
        this.m_wMaxLength = length;
    }

    this.m_pBuffer = new ArrayBuffer(this.m_wMaxLength);
    memset(this.m_pBuffer, 0, this.m_wMaxLength);
}

Cmd_Data.prototype.setcmdinfo = function(wMain, wSub) {
    this.m_wMain = wMain;
    this.m_wSub = wSub;
};

Cmd_Data.prototype.setbuffer = function(buf) {
    this.m_pBuffer = buf;
    this.m_wMaxLength = buf.byteLength;
};

Cmd_Data.prototype.getmain = function() {
    return this.m_wMain;
};

Cmd_Data.prototype.getsub = function() {
    return this.m_wSub;
};

Cmd_Data.prototype.getlen = function() {
    return this.m_bAutoLen ? this.m_wCurIndex : this.m_pBuffer.byteLength;
};

Cmd_Data.prototype.getcurlen = function() {
    return this.m_wCurIndex;
};

Cmd_Data.prototype.pushbool = function(val) {
    var cbData = new Uint8Array(1);
    cbData[0] = !!val;
    this.PushByteData(cbData, 1);
};

Cmd_Data.prototype.pushbyte = function(val) {
    var cbData = new Uint8Array(1);
    cbData[0] = val;
    this.PushByteData(cbData, 1);
};

Cmd_Data.prototype.pushword = function(val) {
    var cbData = new ArrayBuffer(2);
    var word = new Uint16Array(cbData);
    word[0] = val;
    this.PushByteData(cbData, 2);
};

Cmd_Data.prototype.pushshort = function(val) {
    var cbData = new ArrayBuffer(2);
    var short = new Int16Array(cbData);
    short[0] = val;
    this.PushByteData(cbData, 2);
};

Cmd_Data.prototype.pushint = function(val) {
    var cbData = new ArrayBuffer(4);
    var word = new Int32Array(cbData);
    word[0] = val;
    this.PushByteData(cbData, 4);
};

Cmd_Data.prototype.pushdword = function(val) {
    var cbData = new ArrayBuffer(4);
    var word = new Uint32Array(cbData);
    word[0] = val;
    this.PushByteData(cbData, 4);
};

Cmd_Data.prototype.pushfloat = function(val) {
    var cbData = new ArrayBuffer(4);
    var word = new Float32Array(cbData);
    word[0] = val;
    this.PushByteData(cbData, 4);
};

Cmd_Data.prototype.pushdouble = function(val) {
    var cbData = new ArrayBuffer(8);
    var word = new Float64Array(cbData);
    word[0] = val;
    this.PushByteData(cbData, 8);
};

Cmd_Data.prototype.pushscore = function(score) {
    var cbData = new ArrayBuffer(8);
    var word = new Uint32Array(cbData);
    word[0] = score & 0xffffffff;
    word[1] = (score >>> 32) & 0xffffffff00000000;
    this.PushByteData(cbData, 8);
};

Cmd_Data.prototype.pushstring = function(str, wDstLen) {
    //appdf.assert(false, "Cmd_Data.pushstring");
    if (str.length > 0) {
        var uint8array = new TextEncoder('utf-16le', { NONSTANDARD_allowLegacyEncoding: true }).encode(str);

        var cbData = new ArrayBuffer(wDstLen * 2);
        memset(cbData, 0, wDstLen * 2);

        var intData = new Uint8Array(cbData);
        for (var i = 0; i < wDstLen * 2; i ++) {
            if (i == uint8array.byteLength) break;

            intData[i] = uint8array[i];
        }

        this.PushByteData(cbData, wDstLen * 2);
    }
    else {
        this.PushByteData(null, wDstLen * 2);
    }
};

Cmd_Data.prototype.resetread = function() {
    this.m_wCurIndex = 0;
};

Cmd_Data.prototype.readbool = function() {
    if (this.m_wCurIndex < this.getlen()) {
        var arr = new Uint8Array(this.m_pBuffer);
        var ret = arr[this.m_wCurIndex] != 0;
        this.m_wCurIndex ++;
        return ret;
    }
    cc.log("exception - current index out of bounds");
    return 0;
};

Cmd_Data.prototype.readbyte = function() {
    if (this.m_wCurIndex < this.getlen()) {
        var arr = new Uint8Array(this.m_pBuffer);
        var ret = arr[this.m_wCurIndex];
        this.m_wCurIndex ++;
        return ret;
    }
    cc.log("exception - current index out of bounds");
    return 0;
};

Cmd_Data.prototype.readword = function() {
    if (this.m_wCurIndex + 1 < this.getlen()) {
        var arr = new ArrayBuffer(2);
        memcpy2(arr, this.m_pBuffer, 0, this.m_wCurIndex, 2);

        var data = new Uint16Array(arr);
        this.m_wCurIndex += 2;
        return data[0];
    }
    cc.log("exception - current index out of bounds");
    return 0;
};

Cmd_Data.prototype.readshort = function() {
    if (this.m_wCurIndex + 1 < this.getlen()) {
        var arr = new ArrayBuffer(2);
        memcpy2(arr, this.m_pBuffer, 0, this.m_wCurIndex, 2);

        var data = new Int16Array(arr);
        this.m_wCurIndex += 2;
        return data[0];
    }
    cc.log("exception - current index out of bounds");
    return 0;
};

Cmd_Data.prototype.readint = function() {
    if (this.m_wCurIndex + 3 < this.getlen()) {
        var arr = new ArrayBuffer(4);
        memcpy2(arr, this.m_pBuffer, 0, this.m_wCurIndex, 4);

        var data = new Int32Array(arr);
        this.m_wCurIndex += 4;
        return data[0];
    }
    cc.log("exception - current index out of bounds");
    return 0;
};

Cmd_Data.prototype.readdword = function() {
    if (this.m_wCurIndex + 3 < this.getlen()) {
        var arr = new ArrayBuffer(4);
        memcpy2(arr, this.m_pBuffer, 0, this.m_wCurIndex, 4);

        var data = new Uint32Array(arr);
        this.m_wCurIndex += 4;
        return data[0];
    }
    cc.log("exception - current index out of bounds");
    return 0;
};

Cmd_Data.prototype.readfloat = function() {
    if (this.m_wCurIndex + 3 < this.getlen()) {
        var arr = new ArrayBuffer(4);
        memcpy2(arr, this.m_pBuffer, 0, this.m_wCurIndex, 4);

        var data = new Float32Array(arr);
        this.m_wCurIndex += 4;
        return data[0];
    }
    cc.log("exception - current index out of bounds");
    return 0;
};

Cmd_Data.prototype.readdouble = function() {
    if (this.m_wCurIndex + 7 < this.getlen()) {
        var arr = new ArrayBuffer(8);
        memcpy2(arr, this.m_pBuffer, 0, this.m_wCurIndex, 8);

        var data = new Float64Array(arr);
        this.m_wCurIndex += 8;
        return data[0];
    }
    cc.log("exception - current index out of bounds");
    return 0;
};

// deal as normal number
Cmd_Data.prototype.readscore = function() {
    if (this.m_wCurIndex + 7 < this.getlen()) {
        var arr = new ArrayBuffer(8);
        memcpy2(arr, this.m_pBuffer, 0, this.m_wCurIndex, 8);

        var data = new Uint32Array(arr);
        this.m_wCurIndex += 8;
        return data[0] + data[1] << 32;
    }
    cc.log("exception - current index out of bounds");
    return 0;
};

Cmd_Data.prototype.readstring = function(maxLen) {
    var wMaxLen;
    if (maxLen === undefined) {
        if (this.m_wCurIndex < this.getlen()) {
            wMaxLen = this.getlen() - this.m_wCurIndex;

        }
    } else {
        wMaxLen = this.getlen() - this.m_wCurIndex;
        if (maxLen * 2 < wMaxLen) wMaxLen = maxLen * 2;
    }

    var ret = "";
    if (wMaxLen > 0) {
        var arr = new ArrayBuffer(wMaxLen);
        memcpy2(arr, this.m_pBuffer, 0, this.m_wCurIndex, wMaxLen);

        var u16arr = new Uint16Array(arr);
        var strLen = 0;
        for (var i = 0; i < u16arr.length; i++) {
            if (u16arr[i] === 0) break;
        }
        strLen = i;


        ret = new TextDecoder('utf-16le').decode(arr);
        ret = ret.substring(0, strLen);

        this.m_wCurIndex += wMaxLen;
    }

    return ret.trim();
};

/**
 *
 * @param {ArrayBuffer} cbData
 * @param wLength
 * @returns {number|*}
 */
Cmd_Data.prototype.PushByteData = function(cbData, wLength) {
    do {
        // 非法过滤
        if(wLength == 0 && cbData == null)
        {
            cc.log("[_DEBUG] pushByteData-error-null-input");
            break;
        }

        if(this.m_wCurIndex + wLength > this.m_wMaxLength) {
            if(!this.m_bAutoLen) {
                cc.log("[_DEBUG] pushByteData-error:[cur:%d][max:%d][add:%d]", this.m_wCurIndex, this.m_wMaxLength, wLength);
                break;
            }
            else{
                var wNewLen = (this.m_wMaxLength + wLength) * 2; // WORD
                var pNewData = new ArrayBuffer(wNewLen); // BYTE *
                memset(pNewData,0, wNewLen);
                memcpy(pNewData, this.m_pBuffer, 0, this.m_wMaxLength);
                this.m_wMaxLength = wNewLen;

                this.m_pBuffer = pNewData;
            }
        }
        //填充数据
        if(cbData != null) {
            memcpy(this.m_pBuffer, cbData, this.m_wCurIndex, wLength);
        }
        //游标更新
        this.m_wCurIndex += wLength;
    } while (false);

    return this.m_wCurIndex;
};