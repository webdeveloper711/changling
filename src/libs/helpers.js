/*
* @author: Jong
 */

/**
 * memcpy
 *
 * @param {ArrayBuffer} arData
 * @param val
 * @param nLen
 */
function memset(arData, val, nLen) {
    var u8array = new Uint8Array(arData);
    for (var i = 0; i < nLen; i++) {
        u8array[i] = val;
    }
}

/**
 * memcpy
 *
 * @param {ArrayBuffer} arrDest
 * @param {ArrayBuffer} arrSrc
 * @param nStart  start position of destination array
 * @param nLen
 */
function memcpy(arrDest, arrSrc, nStart, nLen) {
    var i8dest = new Uint8Array(arrDest);
    var i8src  = new Uint8Array(arrSrc);

    for (var i = 0; i < nLen; i++) {
        if (i + nStart >= arrDest.byteLength) break;
        if (i >= arrSrc.byteLength) break;
        i8dest[i + nStart] = i8src[i];
    }

    return i;
}

/**
 * memcpy2
 *
 * @param {ArrayBuffer} arrDest
 * @param {ArrayBuffer} arrSrc
 * @param {Int} nDestStart start position of destination array
 * @param {Int} nSrcStart start position of source array
 * @param nLen
 */
function memcpy2(arrDest, arrSrc, nDestStart, nSrcStart, nLen) {
    var i8dest = new Uint8Array(arrDest);
    var i8src  = new Uint8Array(arrSrc);

    for (var i = 0; i < nLen; i++) {
        if (i + nDestStart >= arrDest.byteLength) break;
        if (i + nSrcStart >= arrSrc.byteLength) break;
        i8dest[i + nDestStart] = i8src[i + nSrcStart];
    }

    return i;
}